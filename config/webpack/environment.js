const { config, environment } = require('@rails/webpacker');
const erb                     = require('./loaders/erb');
const { env } = require('process');
const webpack = require('webpack');

const __DEV__  = env.NODE_ENV === 'development';
const __TEST__ = env.NODE_ENV === 'test';
const __PROD__ = env.NODE_ENV === 'production' || env.NODE_ENV === 'staging' || env.NODE_ENV === 'qa'
  || env.NODE_ENV === 'dev';

environment.plugins.insert(
  'DefinePlugin',
  new webpack.DefinePlugin(Object.assign({
    'process.env': { NODE_ENV: JSON.stringify(env.NODE_ENV) },
    __DEV__,
    __TEST__,
    __PROD__,
  }))
);

environment.config.merge({
  module: {
    rules: [
      {
        test: require.resolve('jquery'),
        use:  [
          {
            loader:  'expose-loader',
            options: 'jQuery',
          },
          {
            loader:  'expose-loader',
            options: '$',
          }
        ],
      }
    ],
  },
});
environment.loaders.append('erb', erb);
// environment.loaders.prepend('erb', erb)
module.exports = environment;
