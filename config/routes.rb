# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  authenticate :user do
    mount Sidekiq::Web => '/admin/sidekiq'
    mount Flipper::UI.app(Flipper) => '/admin/flipper'
  end

  mount ActionCable.server => '/cable'
  mount SwaggerUiEngine::Engine, at: '/api_docs'

  devise_for :users, skip: %i[sessions registrations passwords confirmations]
  as :user do
    get '(:locale)/users/sign_in' => 'users/sessions#new', as: :new_user_session
    post '(:locale)/users/sign_in' => 'users/sessions#create', as: :user_session
    delete '(:locale)/users/sign_out' => 'users/sessions#destroy', as: :destroy_user_session
    get '(:locale)/users/sign_up' => 'users/registrations#new', as: :new_user_registration
    post '(:locale)/users/sign_up' => 'users/registrations#create', as: :user_registration
    get '(:locale)/users/passwords/new' => 'users/passwords#new', as: :new_user_password
    post '(:locale)/users/passwords/new' => 'users/passwords#create', as: :user_password
    put '(:locale)/users/passwords/new' => 'users/passwords#update'
    get '(:locale)/users/passwords/edit' => 'users/passwords#edit', as: :edit_user_password
    put '(:locale)/users/passwords/edit' => 'users/passwords#update'
    get '(:locale)/users/confirmation/new' => 'users/confirmations#new', as: :new_user_confirmation
    get '(:locale)/users/confirmation' => 'users/confirmations#show', as: :show_user_confirmation
    post '(:locale)/users/confirmation' => 'users/confirmations#create', as: :user_confirmation
  end

  scope '(:locale)', locale: /en|ar/ do
    root to: 'home#index'
    resource :home, only: :index

    devise_scope :user do
      get '/users/registrations/notification' => 'users/registrations#notification'
      get '/users/password/notification' => 'users/passwords#notification'
    end
  end

  namespace :admin do
    root to: 'dashboards#index'
    resources :dashboards

    resources :hotels
    resources :audits do
      member do
        post :initiate
        post :force_run
      end
    end

    resource :generator do
      # get :new_hotel
      post :create_hotel
      # get :new_reservation
      post :create_reservation
      # get :new_reservations
      post :create_reservations
    end

    resources :reservations
    resources :invoices
    resources :users

    resources :serviceworkers, only: :index
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :docs, only: :index

      scope '(:locale)', locale: /en|ar/ do
        resources :docs, only: :index
        resources :countries, only: %i[index show], param: :alpha3

        namespace :rooms do
          resources :amenities, only: %i[index create update destroy show]
          resources :room_types, only: %i[index create update destroy show]
          resources :rooms, only: %i[index create update destroy show]
          resources :available_rooms, only: %i[index]
        end

        namespace :hotels do
          resource :logo, except: %i[new edit]
          resource :offline_mode, only: %i[show update]
          resource :address, only: %i[show update]
          resource :check_time, only: %i[show update]
          resource :configuration, only: %i[show update]
        end

        scope module: :hotels do
          resource :hotel, only: %i[show update]
        end

        namespace :reservations do
          resources :dnr
          resources :adults, only: %i[create update show destroy]
          resources :invoices, module: :invoices, only: %i[show]
        end

        namespace :audits do
          resources :confirmations, only: [:index] do
            post :confirm, on: :member
          end
          resources :audits, only: [:show] do
            get :last, on: :collection
            get :last_completed, on: :collection
          end
        end

        resources :reservations, module: :reservations, except: %i[new edit] do
          member do
            patch :cancel
            patch :check_in
            patch :check_out
          end
        end

        resources :invoices, only: %i[show], module: :invoices do
          resources :payments, only: %i[index create update show destroy]
          resources :refunds, only: %i[create update show destroy]
          resources :discounts, only: %i[create update show destroy]
          resources :charges, only: %i[index create update show destroy]
        end

        scope module: :guests do
          resources :guests, only: [:index] do
            post :search, on: :collection
          end
        end

        resources :notifications, module: :notifications, only: %i[index show create] do
          post :preview, on: :collection
          patch :read, on: :member
        end

        resources :profiles, module: :profiles, only: [] do
          put :update, on: :collection
          get :show, on: :collection
        end

        namespace :reports do
          resources :reservations, only: %i[index]
        end

        namespace :rates do
          resources :rates, only: %i[index]
          resources :periods, only: %i[index show update create destroy] do
            get :standard, on: :collection
          end
        end
      end
    end
  end

  get 'serviceworker.js', to: 'serviceworkers#index'
  get '*path', to: 'home#index', constraints: ->(req) { !(%r{/^\/uploads\/.*/} =~ req.fullpath) }
end
