# frozen_string_literal: true

lock '3.11.0'
set :application, 'zaire'
set :repo_url, 'git@gitlab-ssh.sumatosoft.com:zaire/zaire.git'
set :linked_files, ['config/database.yml', 'config/secrets.yml']
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

after 'deploy:normalize_assets', '_compress_assets' do
  Rake::Task['deploy:compress_assets'].invoke([])
end
after 'deploy:normalize_assets', 'deploy:compress_png'

namespace :notify_slack do
  desc 'Notify slack about starting deploy to server'
  task :started do
    on roles(:app), in: :parallel do |server|
      stage  = fetch(:stage).to_s.upcase
      branch = fetch(:branch).to_s.upcase
      domain = server.hostname
      slack_notifer.ping(
        ":bangbang: branch *#{branch}* is deploying to *#{stage}* #{domain} by @#{`whoami`}"
      )
    end
  end

  desc 'Notify slack about success deploy operation'
  task :finished do
    on roles(:app), in: :parallel do |server|
      stage  = fetch(:stage).to_s.upcase
      domain = server.hostname
      branch = fetch(:branch).to_s.upcase
      slack_notifer.ping(
        ":white_check_mark: branch *#{branch}* successfully deployed to *#{stage}* #{domain} by @#{`whoami`}"
      )
    end
  end

  def slack_notifer
    Slack::Notifier.new 'https://hooks.slack.com/services/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' do
      defaults(channel: '#zaire_development', username: 'zaire', icon_emoji: ':shipit:')
    end
  end
end

before 'deploy:starting', 'notify_slack:started'
after 'deploy:finished', 'notify_slack:finished'
