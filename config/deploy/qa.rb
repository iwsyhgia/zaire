# frozen_string_literal: true

set :deploy_to,             '/var/www/apps/zaire_qa'
set :rails_env,             'qa'
set :branch,                ENV['BRANCH'] || 'qa'

server 'qa.zaire.demo.sumatosoft.com', user: 'app', roles: %i[app db web]
