set :deploy_to,             '/var/www/apps/zaire_dev'
set :rails_env,             'dev'
set :branch,                ENV['BRANCH'] || 'dev'

server 'dev.zaire.demo.sumatosoft.com', user: 'app', roles: %i[app db web]
