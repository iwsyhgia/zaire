# frozen_string_literal: true

set :deploy_to,             '/var/www/apps/zaire_production'
set :rails_env,             'production'
set :branch,                ENV['BRANCH'] || 'master'

server 'server_ip', user: 'app', roles: %i[app db web]
