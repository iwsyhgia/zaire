# frozen_string_literal: true

set :deploy_to,             '/var/www/apps/zaire_staging'
set :rails_env,             'staging'
set :branch,                ENV['BRANCH'] || 'staging'

server 'zaire.demo.sumatosoft.com', user: 'app', roles: %i[app db web]
