{
  "devise": { 
    "sidebar_welcome": "The World's Easiest Hotel System",
    "footer_about": "About",
    "footer_faq": "FAQ",
    "footer_contact_us": "Contact us",
    "sign_out": "Sign Out"
  },
  "rates": {
    "copy_period": "Copy period",
    "frontdesk_period": "Frontdesk Period"
  },
  "amenities": {
    "errors": {
      "cannot_be_blank": "Amenity can not be blank"
    }
  },
  "rooms": {
    "room_management": "Rooms management",
    "new_room_type": "New room type",
    "add_new_room_type": "Add new room type",
    "edit_type": "Edit type",
    "edit_room_type": "Edit room type",
    "delete_room_type": "Delete room type",
    "room_number_is_duplicated": "Room number is duplicated",
    "standard_price_per_night": "Standard price per night",
    "fixed_price": "Fixed price",
    "dynamic_price": "Dynamic price",
    "rate_management": "Rate management",

    "make_custom_amenity": "Make custom amenity",
    "enter_room_number": "Enter room number",

    "no_room_types": "No room types available",
    "are_you_sure_you_want_to_delete_room_type": "Are you sure to delete {{name}} room type?",
    "are_you_sure_you_want_to_delete_amenity": "Are you sure you want to delete amenity {{name}}?",
    "invalid_room_number": "Invalid room number",

    "errors": {
      "room_number_presence": "Room number can not be blank",
      "room_number_is_duplicated": "Room number is duplicated",
      "does_not_exist": "Room with the passed id does not exist",
      "not_associated": "Room is not associated with a room type",
      "capacity_exceeded": "Room capacity has been exceeded",
      "taken": "Room {{value}} has already been taken",
      "cannot_be_destroyed": "Room cannot be destroyed because it associated with a reservation"
    },

    "room_type": {
      "max_occupancy": "Max occupancy",
      "name_of_type": "Name of room type",
      "size_measurement": "Size measurement",
      "quantity": "Room quantity"
    },
    "value_less_than_10000": "Value should be less than 10000",
    "image_format_invalid": "Image format should be PNG, JPG",
    "image_size_invalid": "Image size must be greater than 1 KB and less than 10 MB",
    "image_resolution_invalid":"Image must be more than {{resolution}} pixels",  
    "delete_adult_by_cross": "Please choose what adult to delete by clicking cross near the particular one"
  },
  "hotel": {
    "hotel_settings": "Hotel settings",
    "check_time_subtitle": "Check-in/check-out",
    "name_and_logo_subtitle": "Name and logo",

    "night_audit": "Night Audit",
    "edit_night_audit": "Edit night audit time",
    "configuration": {
      "night_audit_confirmation_required": "Manual Confirmation"
    },
    "audit_time_saved": "Night audit has been successfully saved.",

    "offline_storage": "Reservations offline storage",
    "edit_check_in": "Edit check-in time",
    "edit_check_out": "Edit check-out time",
    "fixed_time": "Fixed time",
    "period": "Period",
    "check_time_saved": "Check in and check out time saved",
    "fail_check_in_time": "Fail to save check in time",
    "fail_check_out_time": "Fail to save check out time",
    "wrong_time_period": "Wrong time period",
    "from": "From",
    "to": "To",
    "logo_of_hotel": "Hotel logo",
    "logo_formats_support": "PNG, SVG, JPG, GIF, BMP formats are supported",
    "logo_size_invalid": "Image size must be greater than 1 KB and less than 10 MB",
    "logo_load_success": "Hotel logo is loaded successfully.",
    "logo_load_error": "Fail to load hotel logo.",
    "logo_destroyed": "Logo deleted.",
    "logo_destroy_error": "Fail to destroy logo.",
    "hotel_name": "Name of hotel",
    "name": "Name",
    "name_success": "Hotel name changed.",
    "address": "Address",
    "address_2": "Address line 2",
    "state": "State / Province",
    "phone_number": "Phone number",
    "fax": "Fax",
    "email": "Email",
    "field_required": "Field is required",
    "field_too_long": "Field is too long.(255 max length)",
    "address_success": "Address changed.",
    "delete_logo_confirmation": "Are you sure to delete the hotel logo?",

    "days_before": "Days before a current date",
    "days_after": "Days after a current date(including a current date)"
  },
  "reservation": {
    "reservations": "Frontdesk",
    "reservation": "Reservation",
    "guest": "Guest Information",
    "adult_details": "Adult guests details",

    "new_reservation": "New reservation",
    "edit_reservation": "Edit reservation",
    "add_cancelation": "To add cancelation fee?",
    "cancel_reservation": "Cancel reservation",
    "send_email_sms": "Send Email/SMS",
    "check_out": "Check out",
    "dnr": "DNR",
    "edit_dnr": "Edit DNR",
    "add_charge": "Add charge",
    "add_discount": "Add discount",
    "add_refund": "Add refund",
    "add_payment": "Add payment",
    "edit_discount": "Edit discount",
    "edit_refund": "Edit refund",
    "edit_payment": "Edit payment",
    "view_invoice": "View invoice",
    "hide_details": "Hide details",
    "invoice": "Invoice",

    "title": "Title",
    "first_name": "First name",
    "last_name": "Last name",
    "code": "Code",
    "phone": "Phone",
    "email": "Email",
    "birth_date": "Birth date",
    "country": "Country",
    "city": "City",
    "check_in": "Check in",
    "check_out": "Check out",
    "stay_details": "Stay details",
    "refundable": "refundable",
    "non_refundable": "non-refundable",
    "refundable_upper": "Refundable",
    "non_refundable_upper": "Non-refundable",
    "first_night": "First-night",
    "full_stay": "Full-stay",
    "avr_price_per_night": "Average price per night",
    "total_price_for_nights": "Total price for {{night_count}} nights",
    "total_price_for_night": "Total price for 1 night",
    "price_for_nights": "{{total_price}} for {{night_count}} nights",
    "price_for_night": "{{total_price}} for 1 night",
    "price_per_night": "{{per_night}} per night",
    "reserved_date": "Reserved {{created_date}} at {{created_time}} directly",
    "guest_checkout_info": "The guest checked out {{night_count}} days earlier",
    "ID": "ID",
    "adults": "Adults",
    "adult": "Adult",
    "children": "Children",
    "child": "Child",

    "added": "Added",
    "edited": "Edited",
    "description": "Description",
    "added_last_edited": "Added/last edited",
    "charges": "Charges",
    "discount": "Discount",
    "payment": "Payment",
    "total": "Total",
    "SAR": "SAR",
    "number": "Number",
    "total_booking": "Total booking",
    "total_charges": "Total charges",
    "vat_tax": "VAT",
    "municipality_tax": "Municipality Tax",
    "total_cahrges_with_taxes": "Total charges with taxes",
    "total_paid": "Total paid",
    "balance": "Balance",
    "reason_for_refund": "Reason for refund",
    "paid_by_card": "(paid by card)",
    "paid_in_cash": "(paid in cash)",
    "payment_for_refund": "(Payment {{payment}})",

    "reservation_number": "Reservation #{{number}}",
    "guest_number": "#{{number}}",

    "no_rooms_available": "No rooms available",
    "no_balance_activity": "There is no balance activity yet",
    "no_adults_checked_in": "No adults checked in",
    "change_room_error": "The room {{room}} is not available for these dates. Please, choose another room.",
    "delete_dnr_confirmation": "Are you sure you want to delete this DNR reservation?",
    "only_current_date_check_in": "You can check in only for current date",
    "refresh_frontdesk": "Please refresh page",
    "submit_cancellation": "Are you sure you want to cancel this reservation?"
  },

  "charges": {
    "no_show_fee": "No-Show",
    "cancellation_fee": "Cancellation",
    "stay_fee": "Stay Fee",
    "charge_for": "Charge for"
  },

  "payments": {
    "delete_payment": "Are you sure you want to delete this item?",
    "delete_payment_forbidden": "This payment is already linked to the refund. You can't delete it"
  },
  
  "audit": {
    "night_audit": "night audit",
    "night_audit_title": "night audit",
    "night_audit_subtitle": "time",
    "night_audit_date": "for date: {{date}}",
    "night_audit_description": "Night audit is running. Usually it takes up to 3 minutes.",
    "successfull_audit": "Night audit report is successfully completed",
    "failed_audit": "Night audit report is failed"
  },

  "profile": {
    "page_title": "Personal account",
    "access_info": "Access information",
    "photo": "Photo",
    "avatar": "Avatar",
    "details": "Details",
    "other": "Other",
    "updated_successfuly": "Data was successfully updated",

    "password": {
      "confirmation": "Password confirmation", 
      "current": "Current password",
      "new": "New password"
    },

    "errors": {
      "required": "Field is required",
      "length_invalid": "Field is too long (maximum is {{value}})",
      "phone_number_invalid": "Phone number is invalid",
      "new_password_not_match": "New password does not match with confirmation",
      "confirmation_not_match": "Password confirmation does not match with new password",
      "complexity": "Password must contain at least 1 lowercase character, 1 uppercase character and 1 number character",
      "no_payments": "There are no payments"
    }
  },

  "image": {
    "supported_formats": "PNG, SVG, JPG, GIF, BMP formats are supported"
  },

  "images": {
    "errors": {
      "base64_invalid": "Image is invalid",
      "format_invalid": "Image format should be PNG, JPG or SVG",
      "resolution_invalid": "Image must be more than {{resolution}} pixels",
      "size_invalid": "Image size must be greater than {{min}} KB and less than {{max}} MB"
    }
  }
}
