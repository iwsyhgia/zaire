# frozen_string_literal: true

if Rails.env.development? || Rails.env.test?
  CarrierWave.configure do |config|
    config.storage = :file
    config.asset_host = ActionController::Base.asset_host
    config.cache_dir = '/tmp'
  end
else
  region    = Rails.application.secrets[:aws][:region]
  directory = Rails.application.secrets[:aws][:directory]

  CarrierWave.configure do |config|
    config.fog_provider    = 'fog/aws'
    config.fog_credentials = {
      provider: 'AWS',
      use_iam_profile: true,
      region: region
    }
    config.asset_host     = "//#{directory}.s3.dualstack.#{region}.amazonaws.com"
    config.fog_directory  = directory
    config.fog_attributes = { cache_control: "max-age=#{365.day.to_i}" } # optional, defaults to {}
    config.storage        = :fog
  end
end
