# frozen_string_literal: true

# rubocop:disable Style/ClassAndModuleChildren
module ActiveSupport::JSON::Encoding
  class Oj < JSONGemEncoder
    def encode(value)
      ::Oj.dump(value.as_json)
    end
  end
end
# rubocop:enable Style/ClassAndModuleChildren

ActiveSupport.json_encoder = ActiveSupport::JSON::Encoding::Oj

Oj.default_options = { mode: :compat }
