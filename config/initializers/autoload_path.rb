# frozen_string_literal: true

Dir['lib/**/*.rb'].each { |file| require File.expand_path(file) }
