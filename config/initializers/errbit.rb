# frozen_string_literal: true

Airbrake.configure do |config|
  config.environment = Rails.env
  config.ignore_environments = %i[test development]
  config.project_id = 1
  config.host = "https://#{Rails.application.secrets.errbit[:host]}"
  config.project_key = Rails.application.secrets.errbit[:project_key]
end
