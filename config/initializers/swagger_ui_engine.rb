# frozen_string_literal: true

SwaggerUiEngine.configure do |config|
  config.swagger_url = {
    v1: Rails.application.secrets[:swagger][:url]
  }

  config.doc_expansion = 'none'
  config.model_rendering = 'model'
end
