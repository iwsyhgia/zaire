# frozen_string_literal: true

require 'sidekiq/api'
redis_config = Rails.application.config.redis_config

Sidekiq.configure_server do |config|
  config.redis = redis_config

  config.server_middleware do |chain|
    # chain.add Sidekiq::Middleware::Server::RetryJobs, max_retries: 5
  end
end

Sidekiq.default_worker_options = { 'backtrace' => true }

Sidekiq.configure_client do |config|
  config.redis = redis_config
end
