# frozen_string_literal: true

Date::DATE_FORMATS[:default] = '%d/%m/%Y'
Timeliness.use_euro_formats
DATE_FORMATTER = '^([0-9]|[0-2][0-9]|(3)[0-1])(\/)(([0-9]|(0)[0-9])|((1)[0-2]))(\/)\d{4}$' # = '%d/%m/%Y'
