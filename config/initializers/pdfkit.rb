# frozen_string_literal: true

# WickedPDF Global Configuration
#
# Use this to set up shared configuration options for your entire application.
# Any of the configuration options shown here can also be applied to single
# models by passing arguments to the `render :pdf` call.
#
# To learn more, check out the README:
#
# https://github.com/mileszs/wicked_pdf/blob/master/README.md

# WickedPdf.config = {
#   use_puppeteer: false,
#   layout: 'pdf/application'
# }

PDFKit.configure do |config|
  config.default_options = {
    page_size:              'A4',
    dpi:                     300,
    margin_top:              '0.2in',
    margin_right:            '0.2in',
    margin_bottom:           '0.2in',
    margin_left:             '0.2in',
    print_media_type:        false,
    disable_smart_shrinking: true
  }
end
