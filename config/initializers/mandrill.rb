# frozen_string_literal: true

require 'sidekiq/extensions/action_mailer'

MandrillMailer.configure do |config|
  config.default_url_options      = { host: Rails.application.secrets.domain }
  config.api_key                  = Rails.application.secrets[:mandrill][:api_key]
  config.deliver_later_queue_name = :default
end

::MandrillMailer::TemplateMailer.extend(::Sidekiq::Extensions::ActionMailer)
