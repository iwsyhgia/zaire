# frozen_string_literal: true

module ISO3166
  class Country
    include ActiveModel::Serialization

    def self.policy_class
      CountryPolicy
    end
  end
end

ISO3166.configure do |config|
  config.locales = %i[en ar]
end
