# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'
require 'base64'
require 'singleton'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Zaire
  class Application < Rails::Application
    config.generators do |g|
      g.orm             :active_record
      g.template_engine :slim
      g.assets          false
      g.helper          false
    end
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    %w[services serializers swagger validators].each do |folder|
      config.paths.add(
        File.join('app', 'lib/' + folder),
        glob: File.join('**', '*.rb'),
        autoload: true,
        with: folder
      )
    end

    config.middleware.use BatchLoader::Middleware

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**/*.{rb,yml}').to_s]
    config.i18n.available_locales = %i[en ar]
    config.i18n.default_locale = :en
    config.i18n.fallbacks = true
    config.assets.unknown_asset_fallback = true
    config.active_record.default_timezone = :local
    config.time_zone = 'Asia/Riyadh'
    config.active_record.time_zone_aware_attributes = false
    config.active_record.belongs_to_required_by_default = false

    config.i18n.fallbacks = { en: %i[en ar], ar: %i[ar en] }

    config.active_job.queue_adapter = :sidekiq

    config.redis_config = YAML.load(File.read(Rails.root.join('config', 'redis.yml')))[Rails.env] || {}
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
