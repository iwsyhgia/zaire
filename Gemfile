source 'https://rubygems.org'

ruby '2.6.2'

gem 'rails', '5.2.0'
# Ruby interface to the PostgreSQL RDBMS
gem 'pg', '1.1.3'
# A Ruby/Rack web server built for concurrency
gem 'puma', '3.12.0'
# Ruby on Rails stylesheet engine for Sass
gem 'sass-rails', '5.0.7'
# Ruby wrapper for UglifyJS JavaScript compressor
gem 'uglifier', '4.1.20'
# Manage app-like JavaScript modules in Rails
gem 'webpacker', '4.0.2'

gem 'sass', '3.7.2'
gem 'turbolinks', '5.2.0'

# A template language whose goal is to reduce the view syntax
gem 'slim', '4.0.1'
gem 'slim-rails', '3.2.0'

# A library for adding finite state machines to Ruby classes
gem 'aasm', '5.0.2'
# A Scope & Engine based, clean, powerful and customizable paginator for Ruby webapps
gem 'kaminari', '1.1.1'
# Records soft delete
gem 'paranoia', '2.4.1'
# Simple, efficient background processing for Ruby
gem 'sidekiq', '5.2.5'
# Allows to limit number of workers per sidekiq queue
gem 'sidekiq-limit_fetch', '3.4.0'
# Powerful form builder
gem 'simple_form', '4.1.0'
# An attributes on Steroids for Plain Old Ruby Objects
gem 'virtus', '1.0.5'

# Speed Up JSON encoding
gem 'oj', '3.7.10'
gem 'dry-schema', '~> 0.4.0' # Schema coercion & validation

# Validators
gem 'validates_timeliness', '5.0.0.alpha3'

# API (serializers, etc)
gem 'active_model_serializers', '0.10.9'
# ActiveModel Serializers addon for eliminating N+1 queries problem from the serializers.
gem 'ams_lazy_relationships', '0.1.2'
# This gem provides a generic lazy batching mechanism to avoid N+1 DB queries, HTTP queries, etc.
gem 'batch-loader', '1.3.0'
gem 'swagger-blocks', '2.0.2'
gem 'swagger_ui_engine', '1.1.2'

# Images
gem 'carrierwave', '1.3.1'
gem 'fog-aws', '3.4.0'
gem 'rmagick', '3.0.0'
gem 'mini_magick', '4.9.3'

# Settings
gem 'rails-settings-cached', '0.7.2'

# PDF generation tools
gem 'wkhtmltopdf-binary', '~> 0.12.3'
gem 'wicked_pdf', '~>  1.2.2'
gem 'pdfkit', '~> 0.8.2'

# Translations
gem 'i18n-js', '3.2.0'
gem 'globalize', '5.2.0'
gem 'globalize-accessors', '0.2.1'

# Seacrh
gem 'ransack', git: 'https://github.com/activerecord-hackery/ransack', ref: 'd44bfe6fe21ab374ceea9d060267d0d38b09ef28' # 2.1.1

# Authentication && Authorization
gem 'devise', '4.6.1'
gem 'pundit', '2.0.1'

# Emails
gem 'mandrill_mailer', '1.7.1'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '1.4.1', require: false

# A slim ruby wrapper for posting to slack webhooks
gem 'slack-notifier', '2.3.2'
# All sorts of useful information about every country
gem 'countries', '3.0.0'
# A library for generating fake data (names, addresses, phone numbers)
gem 'faker', '1.9.3'

gem 'paper_trail', '10.2.1'

gem 'faraday', '~> 0.15.4'

# An exception reporting service
gem 'airbrake', '8.3.2'

# Gem for switching on\off functionality
gem 'flipper-active_record', '~> 0.16.1'
gem 'flipper-ui', '~> 0.16.1'

# Coloring console output
gem 'colorize', '~> 0.8.1'

gem 'seed_migration', '1.2.2' # Data migration library, similar to rails built-in schema migration

group :staging, :qa, :development do
  # Restrict email sent by your application to only approved domains or accounts
  gem 'safety_mailer', '0.0.10'
end

group :development do
  gem 'magic_frozen_string_literal', '1.0.2'
  # Annotates ActiveRecord Models, routes, fixtures, and others based on the database schema.
  gem 'annotate', '2.7.4'
  # A framework for building automated deployment scripts
  gem 'capistrano', '3.11.0'
  # Rails specific tasks for Capistrano
  gem 'capistrano-rails', '1.4.0', require: false
  # Bundler specific tasks for Capistrano
  gem 'capistrano-bundler', '1.5.0', require: false
  # Adds a task to restart your application after deployment via Capistrano
  gem 'capistrano-passenger', '0.2.0', require: false
  # RVM support for Capistrano
  gem 'capistrano-rvm', '0.1.2', require: false
  # Sidekiq integration for Capistrano
  gem 'capistrano-sidekiq', '1.0.2'
  # Optimized assets compressor for Capistrano
  gem 'capistrano-the-best-compression', git: 'https://github.com/SumatoSoft/capistrano-the-best-compression', ref: '07b3dc7bfdedf689a14e663cc99d2bcd6cedeb89'
  # Preview mail in the browser instead of sending
  gem 'letter_opener', '1.7.0'
  # Listens to file modifications and notifies you about the changes
  gem 'listen', '3.1.5'
  # A Ruby static code analyzer and formatter
  gem 'rubocop', '0.66.0', require: false
  # Static code analyzer for rails. Helps to find common rails issues.
  gem 'rails_best_practices', '1.19.4'
  # Checks all project gems for known vulnerabilities and recommends how to fix the issues found
  # gem 'bundler-audit', '0.6'
  # Help to kill N+1 queries and unused eager loading
  gem 'bullet', '5.9.0'

end

group :development, :test do
  # A library for setting up objects as test data
  gem 'factory_bot_rails', '5.0.1'
  # Ectracting 'assigns' to your controller tests
  gem 'rails-controller-testing', '1.0.4'
  # Testing utils for Action Cable
  gem 'action-cable-testing', '0.5.0'
  # A simple one-liner tests for common Rails functionality
  gem 'shoulda-matchers', '4.0.1'
  # A set of RSpec matchers for testing Pundit authorisation policies
  gem 'pundit-matchers', '1.6.0'
  # A test-double framework for RSpec with support for method stubs, fakes
  gem 'rspec-mocks', '3.8.0'
  # A testing framework for Rails
  gem 'rspec-rails', '3.8.2'
  # Splits tests into even groups and runs each group in a single process with its own database.
  gem 'parallel_tests', '2.28.0'
  # A runtime developer console
  gem 'pry', '0.12.2'
  gem 'pry-rails', '0.3.9'
  # A code style checking for RSpec files
  gem 'rubocop-rspec', '1.32.0'
  gem 'database_cleaner', '1.7.0'
  #Middleware that displays speed badge for every html page.
  gem 'rack-mini-profiler', '1.0.2'
end

group :test do
  # A code coverage analysis tool
  gem 'simplecov', '0.16.1', require: false
  # Record your test suite's HTTP
  gem 'vcr', '~> 4.0'
end
