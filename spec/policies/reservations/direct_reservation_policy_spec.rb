# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Reservations::DirectReservationPolicy do
  subject               { described_class.new(user, reservation) }

  let(:user)            { create(:hotel_with_reservation).user }
  let(:resolved_scope)  { described_class::Scope.new(user, ::Reservations::DirectReservation).resolve }

  context 'when tries to access his own direct reservations' do
    let(:reservation)   { user.hotel.reservations.first }

    it 'allows the user to see reservations of hotel' do
      expect(resolved_scope).to eq(user.hotel.reservations)
    end

    it 'includes reservation in resolved scope' do
      expect(resolved_scope).to include(reservation)
    end

    it { is_expected.to permit_actions(%i[create update destroy show cancel check_in check_out]) }
  end

  context 'when tries to access foreign reservations' do
    let(:hotel)         { create(:hotel_with_reservation) }
    let(:reservation)   { hotel.reservations.first }

    it 'not include reservation in resolved scope' do
      expect(resolved_scope).not_to include(reservation)
    end

    it { is_expected.to forbid_actions(%i[create update destroy show cancel check_in check_out]) }
  end
end
