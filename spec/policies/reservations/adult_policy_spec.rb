# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Reservations::AdultPolicy do
  subject             { described_class.new(user, adult) }

  let(:user)          { create(:hotel_with_reservation).user }

  context 'when tries to access his own adults' do
    let(:adult)       { user.hotel.reservations.first.adults.create(attributes_for(:adult)) }

    it { is_expected.to permit_actions(%i[create update destroy show]) }
  end

  context 'when tries to access foreign adults' do
    let(:hotel)       { create(:hotel_with_reservation) }
    let(:adult)       { hotel.reservations.first.adults.create(attributes_for(:adult)) }

    it { is_expected.to forbid_actions(%i[create update destroy show]) }
  end
end
