# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Reservations::DNRPolicy do
  subject              { described_class.new(user, dnr) }

  let(:user)           { create(:hotel_with_reservation).user }
  let(:resolved_scope) { described_class::Scope.new(user, ::Reservations::DNR).resolve }

  context 'when tries to access his own dnr reservations' do
    let(:dnr)  { user.hotel.rooms.first.dnr.create(attributes_for(:dnr)) }

    it 'allows the user to see dnr reservations of hotel' do
      expect(resolved_scope).to eq(user.hotel.dnr)
    end

    it 'includes dnr reservation in resolved scope' do
      expect(resolved_scope).to include(dnr)
    end

    it { is_expected.to permit_actions(%i[create update destroy show]) }
  end

  context 'when tries to access foreign dnr reservations' do
    let(:hotel)        { create(:hotel_with_reservation) }
    let(:dnr)          { hotel.rooms.first.dnr.create(attributes_for(:dnr)) }

    it 'not include dnr reservation in resolved scope' do
      expect(resolved_scope).not_to include(dnr)
    end

    it { is_expected.to forbid_actions(%i[create update destroy show]) }
  end
end
