# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Invoices::RefundPolicy do
  subject              { described_class.new(user, refund) }

  let(:user)           { create(:hotel_with_reservation).user }
  let(:invoice)        { create(:invoice_with_documents, to: user.hotel.reservations.first) }
  let(:refunds)        { invoice.refunds }
  let(:resolved_scope) { described_class::Scope.new(user, ::Invoices::Refund).resolve }

  context 'when tries to access his own refunds' do
    let(:refund) { create(:refund, invoice: invoice, payment: invoice.payments.first) }

    it 'allows the user to see refunds' do
      expect(resolved_scope).to eq(refunds)
    end

    it 'includes refund in resolved scope' do
      expect(resolved_scope).to include(refund)
    end

    it { is_expected.to permit_actions(%i[create update destroy show]) }
  end

  context 'when tries to access foreign refunds' do
    let(:hotel)        { create(:hotel_with_reservation) }
    let(:invoice)      { create(:invoice_with_documents, to: hotel.reservations.first) }
    let(:refund)       { invoice.refunds.first }

    it 'not include refund in resolved scope' do
      expect(resolved_scope).not_to include(refund)
    end

    it { is_expected.to forbid_actions(%i[create update destroy show]) }
  end
end
