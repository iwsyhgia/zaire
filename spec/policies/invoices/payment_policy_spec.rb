# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Invoices::PaymentPolicy do
  subject              { described_class.new(user, payment) }

  let(:user)           { create(:hotel_with_reservation).user }
  let(:invoice)        { create(:invoice_with_documents, to: user.hotel.reservations.first) }
  let(:payments)       { invoice.payments }
  let(:resolved_scope) { described_class::Scope.new(user, ::Invoices::Payment).resolve }

  context 'when tries to access his own payments' do
    let(:payment) { create(:payment, invoice: invoice) }

    it 'allows the user to see payments' do
      expect(resolved_scope).to eq(payments)
    end

    it 'includes payment in resolved scope' do
      expect(resolved_scope).to include(payment)
    end

    it { is_expected.to permit_actions(%i[create update destroy show]) }
  end

  context 'when tries to access foreign payments' do
    let(:hotel)        { create(:hotel_with_reservation) }
    let(:invoice)      { create(:invoice_with_documents, to: hotel.reservations.first) }
    let(:payment)      { invoice.payments.first }

    it 'not include payment in resolved scope' do
      expect(resolved_scope).not_to include(payment)
    end

    it { is_expected.to forbid_actions(%i[create update destroy show]) }
  end
end
