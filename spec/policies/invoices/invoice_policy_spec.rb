# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Invoices::InvoicePolicy do
  subject             { described_class.new(user, invoice) }

  let(:user)          { create(:hotel_with_reservation).user }

  context 'when tries to access his own invoices' do
    let(:invoice)     { create(:invoice_with_documents, to: user.hotel.reservations.first) }

    it { is_expected.to permit_actions(%i[show update create destroy]) }
  end

  context 'when tries to access foreign invoices' do
    let(:hotel)       { create(:hotel_with_reservation) }
    let(:invoice)     { create(:invoice_with_documents, to: hotel.reservations.first) }

    it { is_expected.to forbid_actions(%i[show update create destroy]) }
  end
end
