# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Invoices::DiscountPolicy do
  subject              { described_class.new(user, discount) }

  let(:user)           { create(:hotel_with_reservation).user }
  let(:invoice)        { create(:invoice_with_documents, to: user.hotel.reservations.first) }
  let(:discounts)      { invoice.discounts }
  let(:resolved_scope) { described_class::Scope.new(user, ::Invoices::Discount).resolve }

  context 'when tries to access his own discounts' do
    let(:discount) { create(:discount, invoice: invoice) }

    it 'allows the user to see discounts' do
      expect(resolved_scope).to eq(discounts)
    end

    it 'includes discount in resolved scope' do
      expect(resolved_scope).to include(discount)
    end

    it { is_expected.to permit_actions(%i[create update destroy show]) }
  end

  context 'when tries to access foreign discounts' do
    let(:hotel)        { create(:hotel_with_reservation) }
    let(:invoice)      { create(:invoice_with_documents, to: hotel.reservations.first) }
    let(:discount)     { invoice.discounts.first }

    it 'not include discount in resolved scope' do
      expect(resolved_scope).not_to include(discount)
    end

    it { is_expected.to forbid_actions(%i[create update destroy show]) }
  end
end
