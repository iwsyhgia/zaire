# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Hotels::CheckTimePolicy do
  subject              { described_class.new(user, check_time) }

  let(:user)           { create(:user, :with_hotel) }

  context 'when tries to access his own check times' do
    describe '#check_in_time' do
      let(:check_time) { user.hotel.check_in_time }

      it { is_expected.to permit_actions(%i[update]) }
    end

    describe '#check_out_time' do
      let(:check_time) { user.hotel.check_out_time }

      it { is_expected.to permit_actions(%i[update]) }
    end
  end

  context 'when tries to access foreign check times' do
    describe '#check_in_time' do
      let(:check_time) { create(:hotel).check_in_time }

      it { is_expected.to forbid_actions(%i[update]) }
    end

    describe '#check_out_time' do
      let(:check_time) { create(:hotel).check_out_time }

      it { is_expected.to forbid_actions(%i[update]) }
    end
  end
end
