# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Hotels::OfflineModePolicy do
  subject                { described_class.new(user, offline_mode) }

  let(:user)             { create(:user, :with_hotel) }

  context 'when tries to access his own offline mode' do
    let(:offline_mode)  { user.hotel.offline_mode }

    it { is_expected.to permit_actions(%i[show update]) }
  end

  context 'when tries to access foreign offline mode' do
    let(:offline_mode)  { create(:hotel).offline_mode }

    it { is_expected.to forbid_actions(%i[show update]) }
  end
end
