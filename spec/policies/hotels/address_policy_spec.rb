# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Hotels::AddressPolicy do
  subject             { described_class.new(user, address) }

  let(:user)          { create(:user, :with_hotel) }

  context 'when tries to access his own adults' do
    let(:address)     { user.hotel.address }

    it { is_expected.to permit_actions(%i[show update]) }
  end

  context 'when tries to access foreign adults' do
    let(:hotel)       { create(:hotel) }
    let(:address)     { hotel.address }

    it { is_expected.to forbid_actions(%i[show update]) }
  end
end
