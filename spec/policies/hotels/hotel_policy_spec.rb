# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Hotels::HotelPolicy do
  subject             { described_class.new(user, hotel) }

  let(:user)          { create(:user, :with_hotel) }

  context 'when tries to access his own hotel' do
    let(:hotel)       { user.hotel }

    it { is_expected.to permit_actions(%i[show update]) }
  end

  context 'when tries to access foreign hotel' do
    let(:hotel)       { create(:hotel) }

    it { is_expected.to forbid_actions(%i[show update]) }
  end
end
