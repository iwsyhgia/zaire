# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Guests::GuestPolicy do
  subject              { described_class.new(user, guest) }

  let(:user)           { create(:hotel_with_reservation).user }
  let(:guests)         { user.hotel.guests }
  let(:resolved_scope) { described_class::Scope.new(user, ::Guests::Guest).resolve }

  context 'when tries to access his own guests' do
    let(:room_type)    { user.hotel.room_types.create(attributes_for(:room_type)) }
    let(:room)         { room_type.rooms.create(attributes_for(:room)) }
    let(:guest)        { room.reservations.create(guest_attributes: attributes_for(:guest).merge(hotel_id: user.hotel.id)).guest }

    it 'allows the user to see guests of hotel' do
      expect(resolved_scope).to eq(user.hotel.guests)
    end

    it 'includes guest in resolved scope' do
      expect(resolved_scope).to include(guest)
    end

    it { is_expected.to permit_actions(%i[index]) }
  end

  context 'when tries to access foreign guests' do
    let(:hotel)        { create(:hotel_with_reservation) }
    let(:guest)        { hotel.reservations.first.guest }

    it 'not include guest in resolved scope' do
      expect(resolved_scope).not_to include(guest)
    end

    # it { is_expected.to forbid_actions(%i[index]) }
  end
end
