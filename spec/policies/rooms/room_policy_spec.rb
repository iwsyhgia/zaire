# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Rooms::RoomPolicy do
  subject               { described_class.new(user, room) }

  let(:user)            { create(:hotel_with_reservation).user }
  let(:rooms)           { user.hotel.rooms }
  let(:resolved_scope)  { described_class::Scope.new(user, ::Rooms::Room).resolve }

  context 'when tries to access his own rooms' do
    let(:room)          { user.hotel.room_types.first.rooms.create(attributes_for(:room)) }

    it 'allows the user to see rooms of hotel' do
      expect(resolved_scope).to eq(rooms)
    end

    it 'includes room in resolved scope' do
      expect(resolved_scope).to include(room)
    end

    it { is_expected.to permit_actions(%i[show create update destroy]) }
  end

  context 'when tries to access foreign rooms' do
    let(:room)          { create(:hotel_with_reservation).room_types.first.rooms.first }

    it 'not include room in resolved scope' do
      expect(resolved_scope).not_to include(room)
    end

    it { is_expected.to forbid_actions(%i[show create update destroy]) }
  end
end
