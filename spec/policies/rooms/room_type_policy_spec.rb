# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Rooms::RoomTypePolicy do
  subject               { described_class.new(user, room_type) }

  let(:user)            { create(:hotel_with_reservation).user }
  let(:room_types)      { user.hotel.room_types }
  let(:resolved_scope)  { described_class::Scope.new(user, ::Rooms::RoomType).resolve }

  context 'when tries to access his own room types' do
    let(:room_type)     { user.hotel.room_types.create(attributes_for(:room_type)) }

    it 'allows the user to see room types of hotel' do
      expect(resolved_scope).to eq(room_types)
    end

    it 'includes room type in resolved scope' do
      expect(resolved_scope).to include(room_type)
    end

    it { is_expected.to permit_actions(%i[show create update destroy]) }
  end

  context 'when tries to access foreign room types' do
    let(:room_type) { create(:hotel_with_reservation).room_types.first }

    it 'not include room type type in resolved scope' do
      expect(resolved_scope).not_to include(room_type)
    end

    it { is_expected.to forbid_actions(%i[show create update destroy]) }
  end
end
