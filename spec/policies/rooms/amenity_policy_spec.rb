# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Rooms::AmenityPolicy do
  subject              { described_class.new(user, amenity) }

  let(:user)           { create(:hotel, :with_amenities).user }
  let(:amenities)      { user.hotel.amenities }
  let(:resolved_scope) { described_class::Scope.new(user, ::Rooms::Amenity).resolve }

  context 'when tries to access his own amenities' do
    let(:amenity)       { create(:amenity, hotel_id: user.hotel.id) }

    it 'allows the user to see amenities of hotel' do
      expect(resolved_scope).to eq(user.hotel.amenities)
    end

    it 'includes amenity in resolved scope' do
      expect(resolved_scope).to include(amenity)
    end

    it { is_expected.to permit_actions(%i[show create update destroy]) }
  end

  context 'when tries to access foreign amenities' do
    let(:amenity) { create(:amenity, hotel_id: create(:hotel).id) }

    it 'not include amenity in resolved scope' do
      expect(resolved_scope).not_to include(amenity)
    end

    it { is_expected.to forbid_actions(%i[show create update destroy]) }
  end

  context 'when tries to access default amenities' do
    let(:amenity) { create(:amenity, hotel_id: nil) }

    it 'includes amenity in resolved scope' do
      expect(resolved_scope).to include(amenity)
    end

    it { is_expected.to permit_action(:show) }
  end
end
