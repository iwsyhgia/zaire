# frozen_string_literal: true

require 'rails_helper'

def session
  @session ||= ActionDispatch::Integration::Session.new(Rails.application)
end

describe 'Night audit start', type: :feature do
  let(:hotel) { create(:hotel) }
  # Enable when confirmations turn on
  # it 'night audit with confirmation' do
  #   perform_enqueued_jobs do
  #     sign_in(hotel.user)
  #     Services::Audits::Initiate.call(audit: hotel.current_audit)
  #     session.get('/api/v1/audits/confirmations.json')
  #     confirmation_id = Oj.load(session.response.body)['response'].last['id']
  #     session.patch("/api/v1/audits/confirmations/#{confirmation_id}/confirm.json")
  #   end
  # end
end
