module ApiHelper
  def error_base
    json_body.dig(:error, :details, :base)&.first
  end

  def error_details
    json_body.dig(:error, :details)
  end

  def json_body
    JSON(response.body, symbolize_names: true)
  end

  def json_response
    json_body.dig(:response)
  end
end
