# frozen_string_literal: true

RSpec.shared_examples 'response with success status' do
  it { expect(response).to have_http_status(:success) }
end

RSpec.shared_examples 'response with bad request status' do
  it { expect(response).to have_http_status(400) }
end

RSpec.shared_examples 'successfully assigns' do |parameter|
  it { expect(assigns(parameter)).to eq(subject) }
end
