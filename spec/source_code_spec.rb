require 'rails_helper'

describe 'SourceCode' do
  PATH = File.join(Rails.root, 'app/assets/javascripts')
  DIRS_TO_EXCLUDE = ['--exclude-dir "ckeditor"',
                     '--exclude-dir "vendor"'].freeze

  RUBY_SOURCE_PATHS = Dir.entries('app') - ['assets', '..']

  it 'not have any console.log in the javascript. What? You made this fail? How embarrassing...' do
    command = "grep -R #{DIRS_TO_EXCLUDE.join(' ')} 'console\\.log' --include *.js #{PATH}"
    output = `#{command}`
    matches = output.count("\n")
    expect(matches).to eq(0), "Found in:\n#{output}"
  end

  it 'not have any debugger in the javascript. What? You made this fail? How embarrassing...' do
    output = `grep -R 'debugger' --include *.js #{PATH}`
    matches = output.count("\n")
    expect(matches).to eq(0), "Found in:\n#{output}"
  end

  it 'not have any alert in the javascript. What? You made this fail? How embarrassing...' do
    command = "grep -R #{DIRS_TO_EXCLUDE.join(' ')} 'alert(' --include *.js #{PATH}"
    output = `#{command}`
    matches = output.count("\n")
    expect(matches).to eq(0), "Found in:\n#{output}"
  end

  it 'not have any binding.pry in the ruby code. What? You made this fail? How embarrassing...' do
    includes = RUBY_SOURCE_PATHS.inject('') do |result, path|
      result << " --include *.rb app/#{path} --include *.haml app/#{path}"
    end
    output = `grep -R 'binding\\.pry' #{includes}`
    puts output
    matches = output.count("\n")
    expect(matches).to eq(0), "Found in:\n#{output}"
  end
end
