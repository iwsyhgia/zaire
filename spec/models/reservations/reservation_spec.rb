# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reservations::Reservation, type: :model do
  subject(:reservation) { hotel.reservations.first }

  let(:hotel) { create(:hotel_with_reservation) }

  describe '#scope :overlaps' do
    context 'with dates that overlaps reservation' do
      let(:options) { { check_in_date: reservation.check_in_date, check_out_date: reservation.check_out_date } }

      it 'includes reservation in overlaps scope' do
        expect(Reservations::Reservation.overlaps(options)).to include(reservation)
      end
    end

    context 'with dates that does not overlap reservation' do
      let(:options) do
        { check_in_date: reservation.check_in_date + 30.days, check_out_date: reservation.check_out_date + 30.days }
      end

      it 'does not include reservation in overlaps scope' do
        expect(Reservations::Reservation.overlaps(options)).not_to include(reservation)
      end
    end
  end

  describe '#scope :in_datetime_range' do
    context 'with date range which overlaps check in and check out dates' do
      let(:date_start) { (reservation.check_in_date - 5.hours).in_time_zone('Asia/Riyadh') }
      let(:date_end) { (reservation.check_in_date + 1.day).in_time_zone('Asia/Riyadh') }

      it 'includes reservation in in_datetime_range scope' do
        expect(Reservations::Reservation.in_datetime_range(date_start, date_end)).to include(reservation)
      end
    end

    context 'with date range which is before check out date' do
      let(:date_start) { reservation.check_in_date - 2.days }
      let(:date_end) { reservation.check_in_date - 1.days }

      it 'does not include reservation in in_datetime_range scope' do
        expect(Reservations::Reservation.in_datetime_range(date_start, date_end)).not_to include(reservation)
      end
    end
  end

  describe '#scope :waiting_for_new_charge' do
    before do
      reservation.status = 'checked_in'
      reservation.save
    end

    context 'with date which is between check in date and check out date' do
      let(:audit) { reservation.hotel.audits.first }

      it 'includes reservation in waiting_for_new_charge scope' do
        audit.time_frame_begin = reservation.check_in_date
        audit.time_frame_end = reservation.check_out_date + 1.day
        expect(Reservations::Reservation.waiting_for_new_charge(audit)).to include(reservation)
      end
    end

    context 'with date which is after check out date' do
      let(:audit) { reservation.hotel.audits.first }

      it 'does not include reservation in waiting_for_new_charge scope' do
        audit.time_frame_begin = reservation.check_out_date + 1.day
        audit.time_frame_end = reservation.check_out_date + 2.days
        expect(Reservations::Reservation.waiting_for_new_charge(audit)).not_to include(reservation)
      end
    end
  end

  describe '#scope :must_be_marked_as_no_show' do
    context 'with confirmed, refundable reservation and date which is after check_in_date' do
      let(:date) { reservation.check_in_date + 1.day }

      it 'includes reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'confirmed'
        reservation.rate = 'refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).to include(reservation)
      end
    end

    context 'with confirmed, refundable reservation and date which is before check_in_date' do
      let(:date) { reservation.check_in_date - 1.day }

      it 'does not include reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'confirmed'
        reservation.rate = 'refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).not_to include(reservation)
      end
    end

    context 'with unconfirmed, refundable reservation and date which is after check_in_date' do
      let(:date) { reservation.check_in_date + 1.day }

      it 'includes reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'unconfirmed'
        reservation.rate = 'refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).to include(reservation)
      end
    end

    context 'with unconfirmed, refundable reservation and date which is before check_in_date' do
      let(:date) { reservation.check_in_date - 1.day }

      it 'does not include reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'unconfirmed'
        reservation.rate = 'refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).not_to include(reservation)
      end
    end

    context 'with unconfirmed, non_refundable reservation and date which is after check_in_date' do
      let(:date) { reservation.check_in_date + 1.day }

      it 'includes reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'unconfirmed'
        reservation.rate = 'non_refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).to include(reservation)
      end
    end

    context 'with unconfirmed, non_refundable reservation and date which is before check_in_date' do
      let(:date) { reservation.check_in_date - 1.day }

      it 'does not include reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'unconfirmed'
        reservation.rate = 'non_refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).not_to include(reservation)
      end
    end

    context 'with confirmed, non_refundable reservation and date which is after check_out_date' do
      let(:date) { reservation.check_out_date + 1.day }

      it 'includes reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'confirmed'
        reservation.rate = 'non_refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).to include(reservation)
      end
    end

    context 'with confirmed, non_refundable reservation and date which is before check_out_date' do
      let(:date) { reservation.check_out_date - 1.day }

      it 'does not include reservation in must_be_marked_as_no_show scope' do
        reservation.status = 'confirmed'
        reservation.rate = 'non_refundable'
        reservation.save

        expect(Reservations::Reservation.must_be_marked_as_no_show(date)).not_to include(reservation)
      end
    end
  end
end
