# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Rates::Period, type: :model do
  subject(:period) { build_stubbed(:period) }

  describe '#covered_day_names' do
    it do
      period.assign_attributes(start_date: '2019-04-15', end_date: '2019-04-17')
      expect(period.covered_day_names).to eq(%i[monday tuesday])
    end

    it do
      period.assign_attributes(start_date: '2019-04-15', end_date: '2019-05-17')
      expect(period.covered_day_names).to eq(%i[monday tuesday wednesday thursday friday saturday sunday])
    end

    it do
      period.assign_attributes(start_date: '2019-04-19', end_date: '2019-04-23')
      expect(period.covered_day_names).to eq(%i[monday friday saturday sunday])
    end

    it do
      period.assign_attributes(start_date: '2019-04-19', end_date: '2019-04-19')
      expect(period.covered_day_names).to be_empty
    end

    it do
      period.assign_attributes(start_date: nil, end_date: nil, period_kind: :standard)
      expect(period.covered_day_names).to eq(%i[monday tuesday wednesday thursday friday saturday sunday])
    end

    it do
      period.assign_attributes(start_date: '2019-04-19', end_date: '2019-05-22', period_kind: :standard)
      expect(period.covered_day_names).to eq(%i[monday tuesday wednesday thursday friday saturday sunday])
    end
  end
end
