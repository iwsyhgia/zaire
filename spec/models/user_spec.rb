# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject(:user) { create(:user) }

  # A password must have at least 8 characters,
  # including at least 1 number, at least 1 lowercase letter and at least 1 uppercase letter.
  describe '#password' do
    it { is_expected.to validate_presence_of(:password) }
    it { is_expected.not_to allow_value(nil).for(:password) }
    it { is_expected.not_to allow_value('').for(:password) }
    it { is_expected.not_to allow_value('1234567').for(:password) }
    it { is_expected.not_to allow_value('Q12345678').for(:password) }
    it { is_expected.not_to allow_value('w12345678').for(:password) }
    it { is_expected.not_to allow_value('qw12345678').for(:password) }
    it { is_expected.not_to allow_value('wwwwwwwwwwww').for(:password) }
    it { is_expected.not_to allow_value('QQQQQQQQQQQ').for(:password) }
    it { is_expected.not_to allow_value('QwQwQwQwQwQw').for(:password) }
    it { is_expected.to allow_value('Qw12345678').for(:password) }
  end
end
