# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Rooms::Room, type: :model do
  subject(:room) { hotel.rooms.first }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:reservation) { hotel.reservations.first }
  let(:reservation_room) { reservation.room }

  describe '#scope :available_for' do
    context 'with dates that overlaps an exisiting reservation' do
      let(:options) { [reservation.check_in_date, reservation.check_out_date] }

      it 'does not include room in the list of available rooms' do
        expect(::Rooms::Room.available_for(*options)).not_to include(reservation_room)
        expect(::Rooms::Room.available_for(*options)).to include(room)
      end
    end

    context 'with dates that not overlaps an exisiting reservation' do
      let(:options) { [reservation.check_in_date - 2.month, reservation.check_out_date - 1.month] }

      it 'includes room in the list of available rooms' do
        expect(::Rooms::Room.available_for(*options)).to include(reservation_room)
        expect(::Rooms::Room.available_for(*options)).to include(room)
      end
    end

    context 'with dates that overlaps a cancelled reservation' do
      let(:options) { [reservation.check_in_date, reservation.check_out_date] }

      it 'includes rooms in the list of available rooms' do
        reservation.update(status: 'cancelled')
        expect(::Rooms::Room.available_for(*options)).to include(reservation_room)
        expect(::Rooms::Room.available_for(*options)).to include(room)
      end
    end

    context 'with dates that overlaps 2 exisiting reservations' do
      let(:reservation_2) { create(:reservation, room: reservation_room, date_range: 3..5) }
      let(:options) { [reservation.check_in_date, reservation_2.check_out_date] }

      it 'does not include room in the list of available rooms' do
        expect(::Rooms::Room.available_for(*options)).not_to include(reservation_room)
        expect(::Rooms::Room.available_for(*options)).to include(room)
      end
    end
  end
end
