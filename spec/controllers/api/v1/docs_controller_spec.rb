# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::DocsController, type: :controller do
  let(:user) { create(:user, :with_hotel) }

  before do
    sign_in user
  end

  describe 'GET #index' do
    it 'returns 200 status' do
      get(:index)
      expect(response.status).to eq(200)
    end
  end
end
