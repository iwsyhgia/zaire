# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Profiles::ProfilesController, type: :controller do
  subject(:profile) { create(:profile, avatar: create(:new_image), user: user) }

  let(:user) do
    create(:user, password: 'Zaire123', profile: nil, password_confirmation: 'Zaire123', hotel: create(:hotel))
  end

  before { sign_in profile.user }

  describe 'GET show' do
    before { get :show, params: { id: profile } }

    include_examples 'successfully assigns', :profile
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      it 'changes profile attributes' do
        profile_attributes = { first_name: 'John', last_name: 'Doe', phone_number: '9999' }
        put :update, params: { id: profile, profile: profile_attributes }

        profile.reload
        profile_attributes.each { |key, value| expect(profile[key]).to eq(value) }
        expect(response).to have_http_status(:success)
      end

      it 'destroys avatar successfully' do
        expect do
          put :update, params: { id: profile, profile: { avatar_attributes: { _destroy: true } } }
        end.to change(Image, :count).by(-1)

        expect(response).to have_http_status(:success)
        expect(profile.reload.avatar).to be_nil
      end

      it 'changes password successfully' do
        current_password = 'Zaire123'
        new_password = 'Zaire456'

        put :update, params: {
          id: profile,
          profile: {
            user_attributes: {
              password: new_password,
              password_confirmation: new_password,
              current_password: current_password
            }
          }
        }

        expect(response).to have_http_status(:success)
        expect(user.reload.valid_password?(new_password)).to be(true)
      end

      it 'returns success with empty attributes' do
        put :update, params: { id: profile, profile: { avatar_attributes: {}, user_attributes: {} } }
        expect(response).to have_http_status(:success)
      end
    end

    context 'with invalid params' do
      it 'returns success with empty attributes' do
        put :update, params: { id: profile, profile: { phone_number: 'asdasd' } }

        message = JSON.parse(response.body).dig('error', 'details', 'phone_number').first
        expect(message).to eq(I18n.t('profiles.errors.phone_format_invalid'))
        expect(response).to have_http_status(400)
      end
    end
  end
end
