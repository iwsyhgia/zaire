# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Guests::GuestsController, type: :controller do
  subject!(:needed_guest) do
    room.reservations.create(guest_attributes: attributes_for(:guest_for_search).merge(hotel_id: hotel.id)).guest
  end

  let(:hotel) { create(:hotel) }
  let(:room_type) { hotel.room_types.create(attributes_for(:room_type)) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:search_attributes) { attributes_for(:guest_search) }
  let!(:another_guest) do
    room.reservations.create(guest_attributes: attributes_for(:guest).merge(hotel_id: hotel.id)).guest
  end

  before do
    sign_in hotel.user
  end

  describe 'GET index' do
    before do
      get :index, params: { q: search_attributes }
    end

    it 'returns needed guest' do
      expect(JSON.parse(response.body)['response'].first['id']).to eq(needed_guest.id)
    end

    it 'returns only one guest' do
      expect(JSON.parse(response.body)['response'].count). to eq(1)
    end

    it 'does not return guest, who is not fit the search request' do
      JSON.parse(response.body)['response'].each do |guest|
        expect(guest['id']).not_to eq(another_guest.id)
      end
    end

    include_examples 'response with success status'
  end
end
