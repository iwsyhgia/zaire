# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Invoices::ChargesController do
  subject(:charge) { create(:charge, invoice_id: invoice.id) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:invoice) { create(:invoice, to: hotel.reservations.first) }
  let(:charge_attributes) { attributes_for(:charge, invoice_id: invoice.id, kind: 'sellable_items') }

  before do
    sign_in hotel.user
  end

  describe 'POST #create' do
    it 'creates a new charge' do
      expect do
        post :create, params: { charge: charge_attributes, invoice_id: invoice.id }
      end.to change(::Invoices::Charge, :count).by(1)
    end

    include_examples 'response with success status'
  end

  describe 'GET #show' do
    before { get :show, params: { id: charge, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :charge
    include_examples 'response with success status'
  end

  describe 'PUT #update' do
    before do
      put :update, params: { id: charge, charge: charge_attributes, invoice_id: invoice.id }
    end

    it 'changes charge description attribute' do
      expect(charge.reload.description).to eq(charge_attributes[:description])
    end

    it 'changes charge kind attribute' do
      expect(charge.reload.kind).to eq(charge_attributes[:kind])
    end

    it 'changes charge amount attribute' do
      expect(charge.reload.amount).to eq(charge_attributes[:amount])
    end

    include_examples 'successfully assigns', :charge
    include_examples 'response with success status'
  end

  describe 'DELETE #destroy' do
    before { delete :destroy, params: { id: charge, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :charge
    include_examples 'response with success status'
  end
end
