# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Invoices::InvoicesController, type: :controller do
  subject(:invoice) { create(:invoice_with_documents, to: hotel.reservations.first) }

  let(:hotel) { create(:hotel_with_reservation) }

  before do
    sign_in hotel.user
  end

  describe 'GET show' do
    before do
      get :show, params: { id: invoice }
    end

    include_examples 'successfully assigns', :invoice
    include_examples 'response with success status'
  end
end
