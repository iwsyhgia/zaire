# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Invoices::PaymentsController do
  subject(:payment) { create(:payment, invoice_id: invoice.id) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:reservation) { hotel.reservations.first }
  let(:invoice) { create(:invoice, to: reservation) }
  let(:payment_attributes) { attributes_for(:payment, invoice_id: invoice.id) }
  let(:invalid_payment_attributes) { attributes_for(:invalid_payment, invoice_id: invoice.id) }

  before { sign_in hotel.user }

  describe 'GET #index' do
    before { get :index, params: { invoice_id: invoice.id } }

    include_examples 'response with success status'
  end

  describe 'POST #create' do
    context 'when payment attributes presents' do
      it 'creates a new payment and changes status of reservation' do
        expect do
          post :create, params: { payment: payment_attributes, invoice_id: invoice.id }
          reservation.reload
        end.
          to  change(::Invoices::Payment, :count).by(1).
          and change(reservation, :status).from('unconfirmed').to('confirmed')
      end

      it 'creates a new payment and does not change status of reservation' do
        reservation.confirm!
        expect do
          post :create, params: { payment: payment_attributes, invoice_id: invoice.id }
          reservation.reload
        end.
          to  change(::Invoices::Payment, :count).by(1).
          and not_change(reservation, :status)
      end

      include_examples 'response with success status'
    end

    context 'when payment attributes is missing' do
      it 'does not create a new payment' do
        expect do
          post :create, params: { payment: invalid_payment_attributes, invoice_id: invoice.id }
        end.not_to change(::Invoices::Payment, :count)
      end
    end
  end

  describe 'GET #show' do
    before { get :show, params: { id: payment, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :payment
    include_examples 'response with success status'
  end

  describe 'PUT #update' do
    context 'with valid params' do
      before do
        put :update, params: { id: payment, payment: payment_attributes, invoice_id: invoice.id }
      end

      it 'changes payment payment_method attribute' do
        expect(payment.reload.payment_method).to eq(payment_attributes[:payment_method])
      end

      it 'changes payment amount attribute' do
        expect(payment.reload.amount).to eq(payment_attributes[:amount])
      end

      include_examples 'successfully assigns', :payment
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: payment, payment: invalid_payment_attributes, invoice_id: invoice.id }
      end

      include_examples 'successfully assigns', :payment
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE #destroy' do
    before { delete :destroy, params: { id: payment, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :payment
    include_examples 'response with success status'
  end
end
