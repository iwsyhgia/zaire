# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Invoices::RefundsController do
  subject(:refund) { create(:refund, invoice_id: invoice.id, payment: payment) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:invoice) { create(:invoice, to: hotel.reservations.first) }
  let(:payment) { create(:payment, invoice_id: invoice.id, amount: 1000) }
  let(:refund_attributes) { attributes_for(:refund, invoice_id: invoice.id, payment_id: payment.id) }
  let(:invalid_refund_attributes) { attributes_for(:invalid_refund, invoice_id: invoice.id, payment_id: payment.id) }

  before { sign_in hotel.user }

  describe 'POST #create' do
    context 'when refund attributes presents' do
      it 'creates a new refund' do
        expect do
          post :create, params: { refund: refund_attributes, invoice_id: invoice.id }
        end.to change(::Invoices::Refund, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when refund attributes is missing' do
      it 'does not create a new refund' do
        expect do
          post :create, params: { refund: invalid_refund_attributes, invoice_id: invoice.id }
        end.not_to change(::Invoices::Refund, :count)
      end
    end
  end

  describe 'GET #show' do
    before { get :show, params: { id: refund, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :refund
    include_examples 'response with success status'
  end

  describe 'PUT #update' do
    context 'with valid params' do
      before do
        put :update, params: { id: refund, refund: refund_attributes, invoice_id: invoice.id }
      end

      it 'changes refund description attribute' do
        expect(refund.reload.description).to eq(refund_attributes[:description])
      end

      it 'changes refund amount attribute' do
        expect(refund.reload.amount).to eq(refund_attributes[:amount])
      end

      include_examples 'successfully assigns', :refund
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: refund, refund: invalid_refund_attributes, invoice_id: invoice.id }
      end

      include_examples 'successfully assigns', :refund
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE #destroy' do
    before { delete :destroy, params: { id: refund, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :refund
    include_examples 'response with success status'
  end
end
