# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Invoices::DiscountsController do
  subject(:discount) { create(:discount, invoice_id: invoice.id) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:invoice) { create(:invoice, to: hotel.reservations.first) }
  let(:discount_attributes) { attributes_for(:discount, invoice_id: invoice.id) }
  let(:invalid_discount_attributes) { attributes_for(:invalid_discount, invoice_id: invoice.id) }

  before do
    sign_in hotel.user
  end

  describe 'POST #create' do
    context 'when discount attributes presents' do
      it 'creates a new discount' do
        expect do
          post :create, params: { discount: discount_attributes, invoice_id: invoice.id }
        end.to change(::Invoices::Discount, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when discount attributes is missing' do
      it 'does not create a new discount' do
        expect do
          post :create, params: { discount: invalid_discount_attributes, invoice_id: invoice.id }
        end.not_to change(::Invoices::Discount, :count)
      end
    end
  end

  describe 'GET #show' do
    before { get :show, params: { id: discount, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :discount
    include_examples 'response with success status'
  end

  describe 'PUT #update' do
    context 'with valid params' do
      before do
        put :update, params: {
          id: discount,
          discount: discount_attributes.slice(:value),
          invoice_id: invoice.id
        }
      end

      it 'changes discount value attribute' do
        expect(discount.reload.value).to eq(discount_attributes[:value])
      end

      include_examples 'successfully assigns', :discount
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: {
          id: discount,
          discount: invalid_discount_attributes.slice(:value),
          invoice_id: invoice.id
        }
      end

      include_examples 'successfully assigns', :discount
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE #destroy' do
    before { delete :destroy, params: { id: discount, invoice_id: invoice.id } }

    include_examples 'successfully assigns', :discount
    include_examples 'response with success status'
  end
end
