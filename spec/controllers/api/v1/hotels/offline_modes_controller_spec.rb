# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Hotels::OfflineModesController, type: :controller do
  subject(:offline_mode) { user.hotel.offline_mode }

  let(:user) { create(:user, :with_hotel) }
  let(:new_offline_mode_attributes) { attributes_for(:new_hotel_offline_mode) }
  let(:invalid_offline_mode_attributes) { attributes_for(:invalid_hotel_offline_mode) }

  before do
    sign_in user
  end

  describe 'GET show' do
    before do
      get :show, params: { id: offline_mode }
    end

    include_examples 'successfully assigns', :offline_mode
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    describe 'response locale when invalid params' do
      it 'valid message about mistake for both locale' do
        request.headers['X-LANGUAGE'] = 'ar'
        put :update, params: { id: offline_mode, offline_mode: invalid_offline_mode_attributes }

        json = response&.body.present? ? JSON.parse(response.body) : {}
        expect(json['error']['message']).to eq('فشل التحقّق من: يجب أن يكون عدد Days before today أصغر أو  يساوي 15')

        request.headers['X-LANGUAGE'] = 'en'
        put :update, params: { id: offline_mode, offline_mode: invalid_offline_mode_attributes }

        json = response&.body.present? ? JSON.parse(response.body) : {}
        expect(json['error']['message']).to eq('Validation failed: Days before today must be less than or equal to 15')
      end
    end

    context 'with valid params' do
      before do
        put :update, params: { id: offline_mode, offline_mode: new_offline_mode_attributes }
      end

      it 'changes offline mode attributes' do
        offline_mode.reload
        new_offline_mode_attributes.each do |key, value|
          expect(offline_mode[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :offline_mode
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: offline_mode, offline_mode: invalid_offline_mode_attributes }
      end

      include_examples 'successfully assigns', :offline_mode
      include_examples 'response with bad request status'
    end
  end
end
