# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Hotels::AddressesController, type: :controller do
  subject(:address) { user.hotel.address }

  let(:user) { create(:user, :with_hotel) }
  let(:new_address_attributes) { attributes_for(:new_hotel_address) }
  let(:invalid_address_attributes) { attributes_for(:invalid_hotel_address) }

  before do
    sign_in user
  end

  describe 'GET show' do
    before do
      get :show, params: { id: address }
    end

    include_examples 'successfully assigns', :address
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: address, address: new_address_attributes }
        address.reload
      end

      it 'changes address attributes' do
        new_address_attributes.each do |key, value|
          expect(address[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :address
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: address, address: invalid_address_attributes }
      end

      include_examples 'successfully assigns', :address
      include_examples 'response with bad request status'
    end
  end
end
