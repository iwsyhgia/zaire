# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Hotels::CheckTimesController, type: :controller do
  let(:check_in_time) { user.hotel.check_in_time }
  let(:check_out_time) { user.hotel.check_out_time }

  let(:user) { create(:user, :with_hotel) }

  let(:new_check_time_attributes) do
    {
      check_in_time_attributes: attributes_for(:new_check_in_time),
      check_out_time_attributes: attributes_for(:new_check_out_time)
    }
  end
  let(:invalid_check_time_attributes) do
    {
      check_in_time_attributes: attributes_for(:invalid_check_in_time),
      check_out_time_attributes: attributes_for(:invalid_check_out_time)
    }
  end

  before do
    sign_in user
  end

  describe 'GET show' do
    before do
      get :show
    end

    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { check_time: new_check_time_attributes }, as: :json
      end

      it 'changes check in time attributes' do
        new_check_time_attributes[:check_in_time_attributes].each do |key, value|
          expect(check_in_time[key]).to eq(value)
        end
      end

      it 'changes check out time attributes' do
        new_check_time_attributes[:check_out_time_attributes].each do |key, value|
          expect(check_out_time[key]).to eq(value)
        end
      end

      include_examples 'response with success status'
    end

    context 'with invalid check time params' do
      before do
        put :update, params: { check_time: invalid_check_time_attributes }, as: :json
      end

      include_examples 'response with bad request status'
    end
  end
end
