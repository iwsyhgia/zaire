# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Hotels::LogosController, type: :controller do
  subject(:logo) { user.hotel.logo }

  let(:user) { create(:user, :with_hotel) }
  let(:valid_logo_attributes) { attributes_for(:new_image) }
  let(:new_logo_attributes) { attributes_for(:new_image) }
  let(:invalid_logo_attributes) { attributes_for(:invalid_image_format, format: 'bmp') }

  before do
    sign_in user
  end

  describe 'POST create' do
    context 'when logo file presents' do
      before do
        user.hotel.logo.destroy
      end

      it 'creates a new logo' do
        expect do
          post :create, params: { logo: valid_logo_attributes }
        end.to change(Image, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when logo file is missing' do
      it 'does not save the new logo' do
        expect do
          post :create, params: { logo: invalid_logo_attributes }
        end.not_to change(Image, :count)
      end
    end
  end

  describe 'GET show' do
    before do
      get :show, params: { id: logo }
    end

    include_examples 'successfully assigns', :logo
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: logo, logo: new_logo_attributes }
        logo.reload
      end

      it 'changes the logo' do
        expect('data:image/png;base64,' + Base64.encode64(File.open(logo.file.file.file).read)).
          to eq(new_logo_attributes[:base64])
      end

      include_examples 'successfully assigns', :logo
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: logo, logo: invalid_logo_attributes }
      end

      include_examples 'successfully assigns', :logo
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: logo }
    end

    include_examples 'successfully assigns', :logo
    include_examples 'response with success status'
  end
end
