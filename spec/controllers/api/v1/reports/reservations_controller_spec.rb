# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Reports::ReservationsController, type: :controller do
  let(:hotel) { create(:hotel) }
  let(:room_type) { create(:room_type, hotel: hotel) }
  let(:room) { create(:room, room_type: room_type) }

  before do
    sign_in hotel.user
  end

  describe 'GET index' do
    context 'when a hotel does not have a reservation' do
      it 'returns json response and successful status' do
        get :index

        json = JSON.parse(response.body)['response']
        totals = json['totals']

        expect(totals['stay_fees'].to_f).to eq(0.0)
        expect(totals['stay_fees_discount'].to_f).to eq(0.0)
        expect(totals['sellable_items'].to_f).to eq(0.0)
        expect(totals['sellable_items_discount'].to_f).to eq(0.0)
        expect(totals['tax_total'].to_f).to eq(0.0)
        expect(totals['charges_with_taxes_total'].to_f).to eq(0.0)
        expect(totals['paid_total'].to_f).to eq(0.0)
        expect(totals['unpaid_total'].to_f).to eq(0.0)

        expect(response).to have_http_status(:success)
      end
    end

    context 'when a hotel has reservations' do
      let!(:reservations) do
        default_taxes = { vat_tax: 5, municipal_tax: 5 }
        invoices_attributes = [
          { charges_with_taxes_total: 100, payments_total: 200, charges_total: 300, paid_total: 200 },
          { charges_with_taxes_total: 100, payments_total: 200, charges_total: 300, paid_total: 200 },
          { charges_with_taxes_total: 100, payments_total: 200, charges_total: 300, paid_total: 200 },
          { charges_with_taxes_total: 100, payments_total: 200, charges_total: 300, paid_total: 200 }
        ]
        invoices_attributes.each do |attributes|
          create(:reservation, invoice: create(:invoice, **attributes.merge!(default_taxes)))
        end
      end

      it 'returns json response and successful status' do
        get :index

        json = JSON.parse(response.body)['response']
        totals = json['totals']

        expect(json['reservations'].size). to eq(4)
        expect(totals['items_number'].to_i).to eq(4)
        # expect(totals['sellable_items'].to_f).to eq(0.0)
        # expect(totals['sellable_items_discount'].to_f).to eq(0.0)
        # expect(totals['unpaid_total'].to_f).to eq(4200.0)
        expect(totals['paid_total'].to_f).to eq(800.0)
        expect(totals['stay_fees'].to_f).to eq(1200.0)
        expect(totals['stay_fees_discount'].to_f).to eq(0.0)
        expect(totals['tax_total'].to_f).to eq(120.0)
        expect(totals['charges_with_taxes_total'].to_f).to eq(400.0)

        expect(response).to have_http_status(:success)
      end
    end

    context 'when a hotel has reservations and a query contains search params' do
      let(:today) { Date.current }
      let!(:reservations) do
        create(:reservation, status: :checked_in,  check_in_date: today + 1.days, check_out_date: today + 3.days)
        create(:reservation, status: :checked_out, check_in_date: today + 1.days, check_out_date: today + 3.days)
        create(:reservation, status: :cancelled,   check_in_date: today + 1.days, check_out_date: today + 3.days)
        create(:reservation, status: :confirmed,   check_in_date: today + 1.days, check_out_date: today + 3.days)
        create(:reservation, status: :unconfirmed, check_in_date: today + 1.days, check_out_date: today + 3.days)

        create(:reservation, status: :checked_in,  check_in_date: today + 5.days, check_out_date: today + 9.days)
        create(:reservation, status: :checked_out, check_in_date: today + 5.days, check_out_date: today + 9.days)
        create(:reservation, status: :cancelled,   check_in_date: today + 5.days, check_out_date: today + 9.days)
        create(:reservation, status: :confirmed,   check_in_date: today + 5.days, check_out_date: today + 9.days)
        create(:reservation, status: :unconfirmed, check_in_date: today + 5.days, check_out_date: today + 9.days)

        create(:reservation, status: :confirmed,   check_in_date: today + 10.days, check_out_date: today + 20.days)
        create(:reservation, status: :unconfirmed, check_in_date: today + 10.days, check_out_date: today + 20.days)
      end

      it 'returns successful response and a correct reservation collection' do
        query_params = {
          status_in: %w[unconfirmed confirmed],
          source_in: %w[direct],
          'check_in_date_gteq' => (today + 1.days),
          'check_out_date_lteq' => (today + 10.days)
        }
        get :index, params: { q: query_params }

        json = JSON.parse(response.body)['response']

        expect(json['reservations'].count). to eq(4)
        expect(response).to have_http_status(:success)
      end

      it 'returns an empty collection status_in: no_show' do
        query_params = { status_in: %w[no_show] }
        get :index, params: { q: query_params }

        expect(JSON.parse(response.body).dig('response', 'reservations').count). to eq(0)
        expect(response).to have_http_status(:success)
      end

      it 'returns an empty collection when status_in: confirmed unconfirmed checked_in' do
        query_params = { status_in: %w[confirmed unconfirmed checked_in] }
        get :index, params: { q: query_params }

        expect(JSON.parse(response.body).dig('response', 'reservations').count). to eq(8)
        expect(response).to have_http_status(:success)
      end
    end
  end
end
