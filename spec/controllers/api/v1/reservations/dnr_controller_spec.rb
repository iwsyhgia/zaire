# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Reservations::DNRController, type: :controller do
  subject(:dnr) { create(:dnr, room_id: room.id) }

  let(:hotel) { create(:audit, :completed).hotel }
  let(:room_type) { hotel.room_types.create(attributes_for(:room_type)) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:reservation) { room.reservations.create(attributes_for(:reservation)) }
  let(:valid_dnr_attributes) { attributes_for(:dnr).merge(room_id: room.id) }
  let(:invalid_dnr_attributes) do
    attributes_for(:dnr).merge(
      room_id: room.id,
      check_in_date: reservation.check_in_date,
      check_out_date: reservation.check_out_date,
      description: 'new'
    )
  end

  before do
    sign_in hotel.user
  end

  describe 'POST create' do
    context 'when there are no reservations' do
      it 'creates a new DNR reservation' do
        expect do
          post :create, params: { dnr: valid_dnr_attributes }
        end.to change(::Reservations::DNR, :count).by(1).
          and not_change(::Invoices::Invoice, :count)
      end

      include_examples 'response with success status'
    end

    context 'when reservation presents' do
      it 'does not save the new DNR reservation' do
        expect do
          post :create, params: { dnr: invalid_dnr_attributes }
        end.not_to change(::Reservations::DNR, :count)
      end
    end
  end

  describe 'GET show' do
    before do
      get :show, params: { id: dnr }
    end

    include_examples 'successfully assigns', :dnr
    include_examples 'response with success status'
  end

  describe 'GET index' do
    before do
      get :index
    end

    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: dnr, dnr: valid_dnr_attributes }
        dnr.reload
      end

      it 'changes reservation attributes' do
        valid_dnr_attributes.each do |key, value|
          expect(dnr[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :dnr
      include_examples 'response with success status'
    end

    # context 'with invalid params' do
    #   before do
    #     put :update, params: { id: dnr, dnr: invalid_dnr_attributes }
    #     dnr.reload
    #   end

    #   include_examples 'successfully assigns', :dnr
    #   include_examples 'response with bad request status'
    # end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: dnr }
    end

    include_examples 'successfully assigns', :dnr
    include_examples 'response with success status'
  end
end
