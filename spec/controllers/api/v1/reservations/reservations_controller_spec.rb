# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Reservations::ReservationsController, type: :controller do
  subject(:reservation) { hotel.reservations.first }

  let(:hotel) do
    create(:hotel_with_reservation, :with_standard_period)
  end
  let(:room_type) do
    create(:room_type, hotel: hotel) do |room_type|
      create(:periods_room_type, period: hotel.periods.standard.first, room_type_id: room_type.id)
    end
  end
  let(:room) { room_type.rooms.create(attributes_for(:room, room_type_id: room_type.id)) }
  let(:valid_reservation_attributes) do
    attributes_for(:reservation).merge(
      room_id: room.id,
      number_of_adults: 1,
      guest_attributes: attributes_for(:guest).merge(hotel_id: hotel.id)
    )
  end

  let(:valid_update_reservation_attributes) do
    attributes_for(:reservation).merge(
      room_id: room.id,
      guest_id: reservation.guest_id,
      guest_attributes: attributes_for(:guest).merge(id: reservation.guest_id, hotel_id: hotel.id)
    )
  end

  let(:valid_check_in_reservation_attributes) do
    attributes_for(:reservation).merge(
      room_id: room.id,
      adults_attributes: [attributes_for(:adult)]
    ).without(:payment_kind, :rate, :status, :type)
  end

  let(:invalid_reservation_attributes) do
    attributes_for(:invalid_reservation).merge(
      room_id: room_type.rooms.create(attributes_for(:room)).id,
      guest_attributes: attributes_for(:guest).merge(hotel_id: hotel.id)
    )
  end

  let(:new_reservation_attributes) do
    attributes_for(:new_reservation).merge(
      room_id: room.id,
      guest_attributes: attributes_for(:guest).merge(hotel_id: hotel.id)
    )
  end

  before do
    sign_in hotel.user
  end

  describe 'POST create' do
    context 'when reservation attributes presents' do
      it 'creates a new reservation' do
        expect do
          post :create, params: { reservation: valid_reservation_attributes }
        end.to change(::Reservations::DirectReservation, :count).by(1).
          and change(::Invoices::Invoice, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when reservation phone number is not unique' do
      let(:guest_attributes) { attributes_for(:guest, :with_not_unique_phone).merge(hotel_id: hotel.id) }
      let(:reservation_attributes) do
        attributes_for(:reservation, room_id: room.id, guest_attributes: guest_attributes)
      end

      before { create(:reservation, room_id: room.id, guest_attributes: guest_attributes) }

      it 'does not create a new reservation' do
        expect do
          post :create, params: { reservation: reservation_attributes }
        end.not_to change(::Reservations::DirectReservation, :count)
      end

      it 'response message equal' do
        post :create, params: { reservation: reservation_attributes }
        message = JSON(response.body).dig('error', 'details', 'guest')&.first
        expect(message).to eq(I18n.t('guests.errors.phone_number_taken', value: guest_attributes[:phone_number]))
      end
    end

    context 'when reservation email is not unique' do
      let(:guest_attributes) { attributes_for(:guest, :with_not_unique_email).merge(hotel_id: hotel.id) }
      let(:reservation_attributes) do
        attributes_for(:reservation, room_id: room.id, guest_attributes: guest_attributes)
      end

      before { create(:reservation, room_id: room.id, guest_attributes: guest_attributes) }

      it 'returns response message that equal' do
        post :create, params: { reservation: reservation_attributes }
        message = JSON(response.body).dig('error', 'details', 'guest')&.first
        expect(message).to eq(I18n.t('guests.errors.email_taken', value: guest_attributes[:email]))
      end
    end

    context 'when reservation attributes is missing' do
      it 'does not save the new reservation' do
        expect do
          post :create, params: { reservation: invalid_reservation_attributes }
        end.not_to change(::Reservations::DirectReservation, :count)
      end
    end
  end

  describe 'GET index' do
    before do
      get :index
    end

    include_examples 'response with success status'
  end

  describe 'GET show' do
    before do
      get :show, params: { id: reservation }
    end

    include_examples 'successfully assigns', :reservation
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: reservation, reservation: valid_update_reservation_attributes }
      end

      it 'changes reservation attributes' do
        reservation.reload
        valid_update_reservation_attributes.without(:guest_attributes).each do |key, value|
          expect(reservation[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :reservation
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: reservation, reservation: invalid_reservation_attributes }
      end

      include_examples 'successfully assigns', :reservation
      include_examples 'response with bad request status'
    end

    context 'when reservation has status confirmed and refundable rate it allows to change dates' do
      let(:reservation_attributes) do
        attributes_for(:reservation,
          room: room,
          status: :confirmed,
          rate: :refundable,
          check_out_date: reservation.check_out_date + 5.days)
      end

      before do
        put :update, params: { id: reservation, reservation: reservation.attributes }
      end

      include_examples 'successfully assigns', :reservation
      include_examples 'response with bad request status'
    end

    context 'with reservation that has status confirmed and non-refundable rate' do
      let(:reservation_attributes) do
        attributes_for(:reservation,
          status: :confirmed,
          rate: :non_refundable,
          check_out_date: reservation.check_out_date - 5.days).merge(
            room_id: room.id,
            guest_id: reservation.guest_id,
            guest_attributes: attributes_for(:guest).merge(id: reservation.guest_id, hotel_id: hotel.id)
          ).except(:payment_kind)
      end

      before do
        put :update, params: { id: reservation, reservation: reservation_attributes }
      end

      it 'does not allow to change dates' do
        expect(response).to have_http_status(400)
        message = JSON(response.body).dig('error', 'message')
        expect(message).to eq(I18n.t('reservations.errors.dates_cannot_be_narrowed'))
      end
    end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: reservation }
    end

    include_examples 'successfully assigns', :reservation
    include_examples 'response with success status'
  end

  describe 'PATCH cancel' do
    context 'without cancellation fee' do
      before do
        patch :cancel, params: { id: reservation, reservation: { cancellation_fee: false } }, as: :json
      end

      include_examples 'successfully assigns', :reservation
      include_examples 'response with success status'
    end

    context 'with cancellation fee' do
      before do
        patch :cancel, params: { id: reservation, reservation: { cancellation_fee: true } }, as: :json
      end

      include_examples 'successfully assigns', :reservation
      include_examples 'response with success status'
    end
  end

  describe 'PATCH check_in' do
    before do
      patch :check_in, params: { id: reservation, reservation: valid_check_in_reservation_attributes }
    end

    context 'when night audit is not running' do
      include_examples 'successfully assigns', :reservation
      include_examples 'response with success status'

      it 'does not allow to check in if night audit is initiated' do
        hotel.audits.last.waiting_for_confirmation!
        patch :check_in, params: { id: reservation, reservation: valid_check_in_reservation_attributes }
        expect(response).to have_http_status(400)
      end
    end

    include_examples 'successfully assigns', :reservation
    include_examples 'response with success status'

    it 'switches reservation to checked_in status' do
      expect(reservation.reload.status).to eq('checked_in')
    end
  end

  describe 'PATCH check_out' do
    before do
      reservation.status = 'checked_in'
      reservation.save
      patch :check_out, params: { id: reservation }
    end

    include_examples 'successfully assigns', :reservation
    include_examples 'response with success status'
  end
end
