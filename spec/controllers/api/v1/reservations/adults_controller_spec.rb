# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Reservations::AdultsController, type: :controller do
  subject(:adult) { hotel.reservations.first.adults.create(attributes_for(:adult)) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:valid_adult_attributes) { attributes_for(:adult).merge(reservation_id: adult.reservation_id) }
  let(:invalid_adult_attributes) { attributes_for(:invalid_adult) }
  let(:new_adult_attributes) { attributes_for(:new_adult).merge(reservation_id: adult.reservation_id) }

  before do
    sign_in hotel.user
  end

  describe 'POST create' do
    context 'when adult first name presents' do
      it 'creates a new adult' do
        expect do
          post :create, params: { adult: valid_adult_attributes }
        end.to change(::Reservations::Adult, :count).by(1)
      end

      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end
    end

    context 'when adult first name is missing' do
      it 'does not save the new adult' do
        expect do
          post :create, params: { adult: invalid_adult_attributes }
        end.not_to change(::Reservations::Adult, :count)
      end
    end
  end

  describe 'GET show' do
    before do
      get :show, params: { id: adult }
    end

    include_examples 'successfully assigns', :adult
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: adult, adult: new_adult_attributes }
      end

      it 'changes adults attributes' do
        adult.reload
        new_adult_attributes.each do |key, value|
          expect(adult[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :adult
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: adult, adult: invalid_adult_attributes }
      end

      include_examples 'successfully assigns', :adult
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: adult }
    end

    include_examples 'successfully assigns', :adult
    include_examples 'response with success status'
  end
end
