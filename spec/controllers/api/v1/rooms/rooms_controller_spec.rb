# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Rooms::RoomsController, type: :controller do
  subject(:room) { room_type.rooms.create(attributes_for(:room)) }

  let(:user) { create(:user, :with_hotel) }
  let(:room_type) { user.hotel.room_types.create(attributes_for(:room_type)) }
  let(:valid_room_attributes) { attributes_for(:room).merge(room_type_id: room_type.id) }
  let(:invalid_room_attributes) { attributes_for(:invalid_room).merge(room_type_id: room_type.id) }
  let(:new_room_attributes) { attributes_for(:new_room).merge(room_type_id: room_type.id) }

  before do
    sign_in user
  end

  describe 'POST create' do
    context 'when room number presents' do
      it 'creates a new room' do
        expect do
          post :create, params: { room: valid_room_attributes }
        end.to change(::Rooms::Room, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when room number is missing' do
      it 'does not save the new room' do
        expect do
          post :create, params: { room: invalid_room_attributes }
        end.not_to change(::Rooms::Room, :count)
      end
    end
  end

  describe 'GET index' do
    before do
      get :index
    end

    include_examples 'response with success status'
  end

  describe 'GET show' do
    before do
      get :show, params: { id: room }
    end

    include_examples 'successfully assigns', :room
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: room, room: new_room_attributes }
      end

      it 'changes room attributes' do
        room.reload
        new_room_attributes.each do |key, value|
          expect(room[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :room
      include_examples 'response with success status'
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: room, room: invalid_room_attributes }
      end

      include_examples 'successfully assigns', :room
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: room }
    end

    include_examples 'response with success status'
    include_examples 'successfully assigns', :room
  end
end
