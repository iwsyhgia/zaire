# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Rooms::AmenitiesController, type: :controller do
  subject(:amenity) { user.hotel.amenities.create(attributes_for(:amenity)) }

  let(:user) { create(:user, :with_hotel) }
  let(:valid_amenity_attributes) { attributes_for(:amenity) }
  let(:invalid_amenity_attributes) { attributes_for(:invalid_amenity) }
  let(:new_amenity_attributes) { attributes_for(:new_amenity) }

  before do
    sign_in user
  end

  describe 'POST create' do
    context 'when amenity name presents' do
      it 'creates a new amenity' do
        expect do
          post :create, params: { amenity: valid_amenity_attributes }
        end.to change(::Rooms::Amenity, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when amenity name is missing' do
      it 'does not save the new amenity' do
        expect do
          post :create, params: { amenity: invalid_amenity_attributes }
        end.not_to change(::Rooms::Amenity, :count)
      end
    end
  end

  describe 'GET index' do
    before do
      get :index
    end

    include_examples 'response with success status'
  end

  describe 'GET show' do
    before do
      get :show, params: { id: amenity }
    end

    include_examples 'successfully assigns', :amenity
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: amenity, amenity: new_amenity_attributes }
      end

      it 'changes amenities attributes' do
        amenity.reload
        new_amenity_attributes.each do |key, value|
          expect(amenity[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :amenity
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: amenity, amenity: invalid_amenity_attributes }
      end

      include_examples 'successfully assigns', :amenity
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE destroy' do
    before do
      delete :destroy, params: { id: amenity }
    end

    include_examples 'successfully assigns', :amenity
    include_examples 'response with success status'
  end
end
