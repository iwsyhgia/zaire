# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::Rooms::RoomTypesController, type: :controller do
  subject(:room_type) { hotel.room_types.create(attributes_for(:room_type)) }

  let(:hotel) { create(:hotel, :with_standard_period) }
  let(:valid_room_type_attributes) { attributes_for(:room_type) }
  let(:invalid_room_type_attributes) { attributes_for(:invalid_room_type) }
  let(:new_room_type_attributes) { attributes_for(:new_room_type) }

  before do
    sign_in hotel.user
  end

  describe 'POST create' do
    context 'when room type name presents' do
      it 'creates a new room type' do
        expect { post :create, params: { room_type: valid_room_type_attributes } }.
          to change(hotel.room_types, :count).by(1).and change(hotel.periods_room_types, :count).by(1)
      end

      include_examples 'response with success status'
    end

    context 'when room type name is missing' do
      it 'does not save the new room type' do
        expect do
          post :create, params: { room_type: invalid_room_type_attributes }
        end.not_to change(::Rooms::RoomType, :count)
      end
    end
  end

  describe 'GET index' do
    before do
      get :index
    end

    include_examples 'response with success status'
  end

  describe 'GET show' do
    before do
      get :show, params: { id: room_type }
    end

    include_examples 'successfully assigns', :room_type
    include_examples 'response with success status'
  end

  describe 'PUT update' do
    context 'with valid params' do
      before do
        put :update, params: { id: room_type, room_type: new_room_type_attributes }
      end

      it 'changes room type attributes' do
        room_type.reload
        new_room_type_attributes.each do |key, value|
          expect(room_type[key]).to eq(value)
        end
      end

      include_examples 'successfully assigns', :room_type
    end

    context 'with rate attributes' do
      let(:room_type_attributes) { attributes_for(:new_room_type) }

      it 'changes rate to fixed' do
        rate_attributes = { kind: 'fixed', lower_bound: 111.to_d }
        attributes = room_type_attributes.merge!(rate_attributes: rate_attributes)
        put :update, params: { id: room_type, room_type: attributes }

        expect(response).to have_http_status(:success)
        expect(room_type.reload.rate).to have_attributes(rate_attributes)
      end

      it 'changes rate to dynamic' do
        rate_attributes = { kind: 'dynamic', lower_bound: 111.to_d, upper_bound: 222.to_d }
        attributes = room_type_attributes.merge!(rate_attributes: rate_attributes)
        put :update, params: { id: room_type, room_type: attributes }

        expect(room_type.reload.rate).to have_attributes(rate_attributes)
      end
    end

    context 'with an invalid rate attributes' do
      let(:room_type_attributes) { attributes_for(:new_room_type) }

      it 'returns an error message that upper bound must be absent' do
        rate_attributes = { kind: :fixed, lower_bound: 111, upper_bound: 333 }
        attributes = room_type_attributes.merge!(rate_attributes: rate_attributes)
        put :update, params: { id: room_type, room_type: attributes }

        expect(response).to have_http_status(400)
        expect(JSON(response.body).dig('error', 'details', 'base')&.first).to include(I18n.t('errors.messages.present'))
      end

      it 'returns an error message that upper bound grater than lower bound' do
        rate_attributes = { kind: :dynamic, lower_bound: 77, upper_bound: 42 }
        attributes = room_type_attributes.merge!(rate_attributes: rate_attributes)
        put :update, params: { id: room_type, room_type: attributes }

        expect(response).to have_http_status(400)
        message = JSON(response.body).dig('error', 'details', 'base')&.first
        expect(message).to include(I18n.t('errors.messages.greater_than_or_equal_to', count: 77))
      end

      it 'returns an error message that kind is invalid' do
        rate_attributes = { kind: :invalid, lower_bound: 77, upper_bound: 42 }
        attributes = room_type_attributes.merge!(rate_attributes: rate_attributes)
        put :update, params: { id: room_type, room_type: attributes }

        expect(response).to have_http_status(400)
        message = JSON(response.body).dig('error', 'details', 'base')&.first
        expect(message).to include(I18n.t('rates.errors.kind_invalid', values: ::Rooms::Rate.kinds.keys.join(', ')))
      end
    end

    context 'with invalid params' do
      before do
        put :update, params: { id: room_type, room_type: invalid_room_type_attributes }
      end

      include_examples 'successfully assigns', :room_type
      include_examples 'response with bad request status'
    end
  end

  describe 'DELETE destroy' do
    context 'with valid params' do
      before do
        delete :destroy, params: { id: room_type }
      end

      include_examples 'successfully assigns', :room_type
      include_examples 'response with success status'
    end

    context 'when trying to destroy room type with an upcoming reservation' do
      let(:room) { room_type.rooms.create(attributes_for(:room)) }

      it 'returns a response message that equal' do
        create(:reservation, room_id: room.id, guest_attributes: attributes_for(:guest))
        delete :destroy, params: { id: room_type }

        message = JSON(response.body).dig('error', 'details', 'base')&.first
        expect(message).to eq(I18n.t('room_types.errors.cannot_be_destroyed'))
      end
    end
  end
end
