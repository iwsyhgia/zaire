require 'rails_helper'

RSpec.describe Api::V1::Rates::RatesController, type: :controller do
  let(:hotel) { create(:hotel) }

  before { sign_in hotel.user }

  describe 'GET #index' do
    it 'returns rates for start and end date' do
      get :index, params: { start_date: Date.current, end_date: Date.current + 10.days }
      expect(response.status).to eq(200)
    end

    it 'returns rates for default dates' do
      get :index
      expect(response.status).to eq(200)
    end
  end
end
