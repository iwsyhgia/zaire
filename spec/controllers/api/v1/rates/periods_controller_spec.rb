require 'rails_helper'

RSpec.describe Api::V1::Rates::PeriodsController, type: :controller do
  let(:hotel) { create(:hotel) }
  let(:room_types) { create_list(:room_type, 3, hotel: hotel) }
  let!(:standard_period) { create(:period, :standard, hotel: hotel, room_types: room_types) }
  let!(:specific_period) { create(:period, :special, hotel: hotel, room_types: room_types) }

  before do
    allow(Date).to receive(:current) { Date.parse('2019-04-01') }
    sign_in hotel.user
  end

  describe 'GET #index' do
    it 'returns periods' do
      get :index

      expect(json_response.map { |v| v[:id] }).to include(*[standard_period, specific_period].map(&:id))
      expect(response.status).to eq(200)
    end
  end

  describe 'GET #show' do
    it 'returns a standard period' do
      get :standard

      expect(response.status).to eq(200)
      expect(json_response.dig(:id)).to eq(standard_period.id)
    end

    it 'returns a specific period' do
      get :show, params: { id: specific_period.id }

      expect(response.status).to eq(200)
      expect(json_response.dig(:id)).to eq(specific_period.id)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a specific period' do
        attributes = attributes_for(:period, :with_nested_attributes, room_types: room_types)
        expect { post :create, params: attributes, as: :json }.to change(hotel.periods, :count).by(1)
        expect(response.status).to eq(201)
      end
    end

    context 'with invalid params' do
      it 'does not create a specific period without rates' do
        attributes = attributes_for(:period, :with_nested_attributes,
          nested_attributes: { rates: {} }, room_types: room_types)
        expect { post :create, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        message = "#{::Rates::PeriodsRoomType.human_attribute_name(:rates)} #{I18n.t('errors.messages.blank')}"
        expect(error_base).to include(message)
        expect(response.status).to eq(400)
      end

      it 'does not create a specific period with invalid day names in rates' do
        options = {
          nested_attributes: { day_names: %i[monday tuesday] }, room_types: room_types,
          start_date: '2019-04-20', end_date: '2019-04-22'
        }
        attributes = attributes_for(:period, :with_nested_attributes, **options)
        expect { post :create, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        message = I18n.t('periods.errors.day_names_invalid', values: %i[saturday sunday].join(', '))
        expect(error_base).to include(message)
        expect(response.status).to eq(400)
      end

      it 'does not create a specific period with invalid fixed rates' do
        attributes = attributes_for(:period, :with_nested_attributes, :fixed, room_types: room_types)
        expect { post :create, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        message = "#{::Rooms::Rate.human_attribute_name(:upper_bound)} #{I18n.t('errors.messages.present')}"
        expect(error_base).to eq(message)
        expect(response.status).to eq(400)
      end

      it 'does not create a specific period with invalid dynamic rates' do
        attributes = attributes_for(:period, :dynamic, :with_nested_attributes,
          room_types: room_types, nested_attributes: { rate: { lower_bound: '100' } })

        expect { post :create, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        message = "#{::Rooms::Rate.human_attribute_name(:upper_bound)} #{I18n.t('errors.messages.blank')}"
        expect(error_base).to eq(message)
        expect(response.status).to eq(400)
      end

      it 'does not create a specific period that ovelaps another period' do
        attributes = attributes_for(:period, :with_nested_attributes,
          room_types: room_types, offset: 1.month, duration: 6.days, hotel: hotel)

        expect { post :create, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        expect(error_base).to eq(I18n.t('periods.errors.cannot_be_overlapped'))
        expect(response.status).to eq(400)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'updates a specific period' do
        attributes = { id: specific_period.id, name: 'specific', price_kind: 'fixed' }
        expect { put :update, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        expect(response.status).to eq(200)
      end

      it 'updates a standard period' do
        attributes = { id: standard_period.id, name: 'standard', price_kind: :fixed }
        expect { put :update, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        expect(response.status).to eq(200)
      end

      it 'updates a specific period with rates' do
        nested_attrs = specific_period.periods_room_types.map do |record|
          attributes_for(:periods_room_type, record.attributes.slice('room_type_id', 'id'))
        end
        period_attrs = attributes_for(:period, periods_room_types_attributes: nested_attrs)
        attributes = { id: specific_period.id, **period_attrs }.except(:period_kind)
        expect { put :update, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        expect(response.status).to eq(200)
      end
    end

    context 'with invalid params' do
      it 'does not update a specific period with dynamic price and with invalid rates' do
        nested_attrs = specific_period.periods_room_types.map do |record|
          attrs = { day_names: %i[monday] }.merge!(record.attributes.slice('room_type_id', 'id'))
          attributes_for(:periods_room_type, **attrs.symbolize_keys)
        end
        period_attrs = attributes_for(:period, periods_room_types_attributes: nested_attrs)
        attributes = { id: specific_period.id, **period_attrs }.except(:period_kind)
        expect { put :update, params: attributes, as: :json }.not_to change(hotel.periods, :count)

        message = I18n.t('periods.errors.day_names_invalid', values: DateTime::DAYS_INTO_WEEK.keys.join(', '))
        expect(error_base).to eq(message)
        expect(response.status).to eq(400)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'with a valid params' do
      it 'destroys a specific period with successful status' do
        expect { delete :destroy,  params: { id: specific_period.id } }.to change(hotel.periods, :count).by(-1)

        expect(response.status).to eq(200)
      end
    end

    context 'with a invalid params' do
      it 'does not allow to destroy a standard period' do
        expect { delete :destroy,  params: { id: standard_period.id } }.to change(hotel.periods, :count).by(0)

        expect(error_base).to eq(I18n.t('periods.errors.cannot_be_destroyed'))
        expect(response.status).to eq(400)
      end
    end
  end
end
