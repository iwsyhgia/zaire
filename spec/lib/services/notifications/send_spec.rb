# frozen_string_literal: true

require 'rails_helper'

describe Services::Notifications::Send do
  subject(:notification_sender) do
    described_class.new(recipients: users, message_params: { message: message }, kind: kind)
  end

  let(:users) { create_list(:user, 2) }
  let(:message) { 'message text' }
  let(:kind) { 'audit_initiated' }

  before do
    allow(Services::Notifications::Notifiers::ActionCable).to receive(:call)
    notification_sender.call
  end

  it 'creates notification object for every recipient' do
    users.each do |user|
      expect(user.notifications.last).to have_attributes(message_params: { 'message' => message }, kind: kind)
    end
  end

  it 'sends the message for every user using web sockets notifier' do
    users.each do |user|
      expect(Services::Notifications::Notifiers::ActionCable).to have_received(:call).once.ordered.
        with(notification: user.notifications.last)
    end
  end
end
