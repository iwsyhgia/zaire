# frozen_string_literal: true

require 'rails_helper'

describe Services::Notifications::Notifiers::ActionCable do
  subject(:notifier) { described_class.new(notification: notification) }

  let(:notification) { create(:notification, kind: 'audit_initiated') }

  it 'sends the message for user into WebNotificationsChannel' do
    expect { notifier.call }.to have_broadcasted_to(notification.recipient).
      from_channel(WebNotificationsChannel).
      with(message: notification.message_params['message'], kind: 'audit_initiated')
  end
end
