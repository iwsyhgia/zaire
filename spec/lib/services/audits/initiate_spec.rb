# frozen_string_literal: true

require 'rails_helper'

describe Services::Audits::Initiate do
  subject(:audit_initiator) { described_class.new(audit: audit) }

  let(:audit) { create(:audit) }

  context 'when confirmation is required' do
    before do
      allow(Services::Notifications::Send).to receive(:call)
      audit_initiator.call
    end

    it 'sets correct status to audit' do
      audit_initiator.call
      expect(audit.status).to eq('waiting_for_confirmation')
    end

    it 'creates audit confirmation model for every receptionist and hotelier' do
      expect(audit.hotel.user.audit_confirmations.last).to have_attributes(audit: audit).and be_persisted
    end
  end

  context 'when confirmation is not required' do
    before do
      # rubocop:disable RSpec/SubjectStub
      allow(audit_initiator).to receive(:confirmation_required?).and_return(false)
      # rubocop:enable RSpec/SubjectStub
      allow(Services::Audits::Perform).to receive(:call_later)
      audit_initiator.call
    end

    it 'creates audit confirmation objects for every receptionist and hotelier' do
      expect(Services::Audits::Perform).to have_received(:call_later)
    end
  end
end
