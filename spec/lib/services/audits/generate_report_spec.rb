# frozen_string_literal: true

require 'rails_helper'

describe Services::Audits::GenerateReport do
  subject(:generate_report) { described_class.new(audit: audit) }

  let(:hotel) { create(:hotel_with_room_type, room_type_count: 7) }
  let(:rooms) { hotel.rooms }

  describe '#generate_report' do
    before do
      ::Reservations::Reservation.destroy_all
      create(:reservation, :unconfirmed, :refundable,     :two_days, room: rooms[3])
      create(:reservation, :unconfirmed, :non_refundable, :two_days, room: rooms[4])
      create(:reservation, :confirmed,   :refundable,     :two_days, room: rooms[5])
      create(:reservation, :confirmed,   :non_refundable, :two_days, room: rooms[6])
    end

    context 'with valid audit' do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now - 1.day, time_frame_end: DateTime.now)
      end

      let(:result) { generate_report.call }

      it 'generates night audit report pdf' do
        expect(result).to be_success
        expect(audit.pdf_report.file.file.content_type).to eq('application/pdf')
        expect(audit.pdf_report.file.file.size).to be > 0
      end

      it 'creates one attachment and add him to audit' do
        expect { result }.to change(::Attachment, :count).by(1)
        expect(audit.pdf_report).to eq(Attachment.first)
      end
    end

    context 'with invalid audit' do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: nil, time_frame_end: nil)
      end

      let(:result) { generate_report.call }

      it 'returns service error object and does not saving the attachment' do
        expect(result).not_to be_success
        expect(result).to be_instance_of(::Services::Error)
        expect(audit.pdf_report).to eq(nil)
      end
    end
  end
end
