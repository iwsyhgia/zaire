# frozen_string_literal: true

require 'rails_helper'

describe Services::Audits::Confirm do
  subject(:service_runner) { described_class.new(confirmation: confirmation) }

  let(:audit) { create(:audit, :waiting_for_confirmation) }
  let(:result) { service_runner.call }

  context 'with confirmation' do
    let(:confirmation) { audit.confirmations.first }

    it 'succeeds' do
      expect(result.success?).to be(true)
    end

    it 'confirms the audit' do
      expect { service_runner.call }.to change(confirmation, :confirmed_at)
    end
  end

  context 'without confirmation' do
    let(:confirmation) { nil }

    it 'returns error' do
      expect(result.value).to include(I18n.t('general.errors.record_not_found'))
    end
  end
end
