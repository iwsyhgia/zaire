# frozen_string_literal: true

require 'rails_helper'

describe Services::Audits::ScheduleNext do
  subject(:service_runner) { described_class.new(hotel: hotel) }

  let(:hotel) { create(:hotel_with_room_type, room_type_count: 1) }
  let(:audit) { hotel.audits.last }

  describe 'when has no audits' do
    before { hotel.audits.destroy_all }

    it 'creates the first audit' do
      expect { service_runner.call }.to change { ::Audits::Audit.count }.by(1)
    end

    it 'creates the first audit with correct timing' do
      result = service_runner.call

      expect(result.value.time_frame_begin.to_date).to eq Date.current
      expect(result.value.time_frame_end.to_date).to eq Date.tomorrow
      audit_time_end_hours = result.value.time_frame_end.strftime('%H:%M')
      expect(audit_time_end_hours).to eq hotel.configuration.night_audit_time
      expect(result.value.hours_coverage).to be_in 21..28
    end
  end

  describe 'creates the second audit for a hotel' do
    it 'creates the first audit' do
      expect { service_runner.call }.to change { ::Audits::Audit.count }.by(2)
    end

    it 'creates the first audit with correct timing' do
      result = service_runner.call

      expect(result.value.time_frame_begin).to eq hotel.audits.first.time_frame_end
      expect(result.value.time_frame_end.to_date).to eq((hotel.audits.first.time_frame_end + 1.day).to_date)
      expect(result.value.time_frame_end.strftime('%H:%M')).to eq hotel.configuration.night_audit_time
      expect(result.value.hours_coverage).to be_in 21..28
    end
  end

  describe 'errors handling' do
    before { hotel.audits.destroy_all }

    it 'returns service error object and does not saving the charge if the recalucaltion service failed' do
      allow(::Services::Audits::Initiate).to receive(:call_at).and_return(::Services::Error.new('error', 400))

      result = service_runner.call
      expect(result).not_to be_success
      expect(hotel.audits.count).to eq 0
    end
  end
end
