# frozen_string_literal: true

require 'rails_helper'
require 'mandrill_mailer/offline'

describe Services::Audits::Perform do
  subject(:audit_runner) { described_class.new(audit: audit) }

  let(:hotel) { create(:hotel_with_room_type, room_type_count: 7) }
  let(:audit) { hotel.audits.last }
  let(:rooms) { hotel.rooms }

  before do
    audit.confirmed!
    create(:reservation, :checked_in,  :two_days, room: rooms[0])
    create(:reservation, :checked_in,  :two_days, room: rooms[1])
    create(:reservation, :checked_out, :two_days, room: rooms[2])
  end

  describe '#update_reservations_status' do
    before do
      ::Reservations::Reservation.destroy_all
      create(:reservation, :unconfirmed, :refundable,     :two_days, room: rooms[3])
      create(:reservation, :unconfirmed, :non_refundable, :two_days, room: rooms[4])
      create(:reservation, :confirmed,   :refundable,     :two_days, room: rooms[5])
      create(:reservation, :confirmed,   :non_refundable, :two_days, room: rooms[6])
    end

    context "when audit is #{(DateTime.now - 1.day).to_date} - #{DateTime.now.to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now - 1.day, time_frame_end: DateTime.now)
      end

      it 'does not mark reservation as no_show' do
        audit_runner.call
        expect(hotel.charges.count).to eq 0
        expect(hotel.reservations.no_show.count).to eq 0
      end
    end

    context "when audit is #{DateTime.now.to_date} - #{(DateTime.now + 1.day).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now, time_frame_end: DateTime.now + 1.day)
      end

      it 'marks reservations as no_show all except the confirmed-non-refundable' do
        audit_runner.call
        expect(hotel.charges.count).to eq 3
        expect(hotel.reservations.no_show.count).to eq 3
      end

      it 'sets 1 night for refundable reservations and keeps 2 nights for non_refundable confirmed' do
        audit_runner.call
        expect(hotel.reservations.refundable.map(&:total_nights).uniq).to eq [1]
        expect(hotel.reservations.non_refundable.confirmed.map(&:total_nights).uniq).to eq [2]
      end
    end

    context "when audit is #{DateTime.now.to_date + 2.days} - #{(DateTime.now + 3.days).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel,
                                   time_frame_begin: DateTime.now + 2.days,
                                   time_frame_end: DateTime.now + 3.days)
      end

      it 'marks reservations as no_show all except the confirmed-non-refundable' do
        audit_runner.call
        expect(hotel.charges.count).to eq 4
        expect(hotel.reservations.no_show.count).to eq 4
      end
    end
  end

  describe '#create_stay_charges step' do
    context "when audit is #{(DateTime.now - 1.day).to_date} - #{DateTime.now.to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now - 1.day, time_frame_end: DateTime.now)
      end

      it 'does not creates charges if there are no check_ins' do
        expect { audit_runner.call }.to change { ::Invoices::Charge.count }.by(0)
      end
    end

    context "when audit is #{DateTime.now.to_date} - #{(DateTime.now + 1.day).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now, time_frame_end: DateTime.now + 1.day)
      end

      it 'creates charges for checked_in reservations when next audit came' do
        expect { audit_runner.call }.to change { ::Invoices::Charge.count }.by(2)
      end

      it 'does not create excessive charges if run for the second time' do
        audit_runner.call
        expect { audit_runner.call }.to change { ::Invoices::Charge.count }.by(0)
      end
    end

    context "when audit is #{(DateTime.now + 1.day).to_date} - #{(DateTime.now + 2.days).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel,
                                   time_frame_begin: DateTime.now + 1.day,
                                   time_frame_end: DateTime.now + 2.days)
      end

      it 'creates charges for checked_in reservations when next audit came' do
        expect { audit_runner.call }.to change { ::Invoices::Charge.count }.by(2)
      end
    end

    context "when audit is #{(DateTime.now + 2.days).to_date} - #{(DateTime.now + 3.days).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel,
                                   time_frame_begin: DateTime.now + 2.days,
                                   time_frame_end: DateTime.now + 3.days)
      end

      it 'does not creates charges if there are no check_ins' do
        expect { audit_runner.call }.to change { ::Invoices::Charge.count }.by(0)
      end
    end
  end

  describe 'errors handling' do
    it 'fails the audit if there is a ruby exception in the code' do
      audit.update(time_frame_end: nil)

      expect { audit_runner.call }.to raise_error(StandardError)
      expect(audit.reload.status).to eq 'failed'
    end
  end

  describe '#send_report step' do
    it 'sends an email' do
      expect { audit_runner.call }.to change { MandrillMailer.deliveries.size }.by(1)
    end

    it 'sends an email to the correct user' do
      audit_runner.call

      expect(MandrillMailer.deliveries.last.message['to'].first[:email]).to eq audit.hotel.user.email
    end
  end

  describe '#schedule_next_audit step' do
    context 'when audit is the last audit of the hotel' do
      it 'creates a new audit object with correct timing' do
        expect { audit_runner.call }.to change { ::Audits::Audit.count }.by(1)
      end
    end

    context 'when audit is not the last audit of the hotel' do
      before do
        create(:audit, hotel: hotel)
      end

      it 'does not create a new audit object' do
        expect { audit_runner.call }.not_to change { ::Audits::Audit.count }
      end
    end
  end
end
