# frozen_string_literal: true

require 'rails_helper'

describe Services::Sms::RoomChanged do
  subject(:send_service) do
    described_class.new(phone_number: '375291283795',
                        reservation_id: create(:reservation).id)
  end

  let(:call_service) { send_service.call }

  context 'with valid attributes' do
    before do
      allow(::Services::Sms::Send).to receive(:call_later)
    end

    context 'with success status', :vcr do
      it 'succeed with valid creds' do
        call_service

        expect(::Services::Sms::Send).to have_received(:call_later)
      end
    end
  end

  context 'with invalid attibutes', :vcr do
    subject(:send_service) do
      described_class.new(phone_number:   nil,
                          reservation_id: nil)
    end

    it 'does not send sms' do
      expect(call_service).to eq(nil)
    end
  end
end
