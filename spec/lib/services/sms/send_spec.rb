# frozen_string_literal: true

require 'rails_helper'

describe Services::Sms::Send do
  subject(:send_service) do
    described_class.new(text: 'If you going to San Francisco...',
                        phone_number: '375293969579')
  end

  let(:result) { send_service.call }

  context 'with enabled sms feature' do
    before do
      allow(Flipper).to receive(:enabled?).and_return(true)
    end

    context 'with success status', :vcr do
      it 'succeed with valid creds' do
        expect(result).to be_success
      end
    end

    context 'with error status', :vcr do
      it 'fails with incorrect credentials' do
        Rails.application.secrets.sms[:username] = 'qwe'

        expect(result).not_to be_success
      end
    end
  end

  context 'with disabled sms feature', :vcr do
    before do
      allow(Flipper).to receive(:enabled?).and_return(false)
    end

    it 'does not send sms' do
      expect(result).to eq(nil)
    end
  end
end
