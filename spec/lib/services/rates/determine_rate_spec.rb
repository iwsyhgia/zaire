require 'rails_helper'

RSpec.describe Services::Rates::DetermineRate, type: :service do
  subject(:service) { described_class.new(date: date, room_type: room_type) }

  let(:date) { Date.parse('2019-04-10') }
  let(:hotel) { create(:hotel) }
  let(:room_type) { create(:room_type, hotel: hotel, rooms_count: 0) }
  let(:rooms) { create_list(:room, 5, room_type: room_type) }
  let(:rate) { { lower_bound: 100.0, upper_bound: 300.0 } }
  let(:discount_value) { 0 }
  let(:discount_kind) { :percent }
  let!(:standard_period) do
    create(:period, :standard, :with_nested_attributes, hotel: hotel, room_types: [room_type],
      price_kind: price_kind, discount_kind: discount_kind,
      nested_attributes: { rates: { wednesday: rate }, discount_value: discount_value })
  end

  before do
    allow(Date).to receive(:current).and_return(date)
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  context 'with reservation and amount discount' do
    let(:discount_kind) { :amount }
    let(:price_kind) { :fixed }
    let(:discount_value) { 10 }

    it 'returns the lowest rate' do
      reservation = create(:reservation, :non_refundable, date_range: (0..5), room: rooms[0])

      expect(described_class.call(reservation: reservation).value).to eq(rate[:lower_bound] - discount_value)
    end
  end

  context 'with reservation and percent discount' do
    let(:discount_kind) { :percent }
    let(:price_kind) { :fixed }
    let(:discount_value) { 15 }

    it 'returns the lowest rate' do
      reservation = create(:reservation, :non_refundable, date_range: (0..5), room: rooms[0])

      expect(described_class.call(reservation: reservation).value).to eq(rate[:lower_bound] * (1 - discount_value / 100.0))
    end
  end

  context 'with a fixed price' do
    let(:price_kind) { :fixed }

    it 'returns the lowest rate' do
      create(:reservation, check_in_date: date, check_out_date: date + 5.days, room: rooms[0])

      expect(service.call.value).to eq(rate[:lower_bound])
    end
  end

  context 'with a dynamic price' do
    let(:price_kind) { :dynamic }

    it 'returns lowest rate' do
      expect(service.call.value).to eq(rate[:lower_bound])
    end

    it 'returns an intermediate rate' do
      2.times { |i| create(:reservation, check_in_date: date, check_out_date: date + 5.days, room: rooms[i]) }

      number_available_rooms = 3
      value = (rate[:upper_bound] - rate[:lower_bound]) / number_available_rooms + rate[:lower_bound]

      expect(service.call.value.round(2)).to eq(value.round(2))
    end

    it 'returns the highest rate' do
      4.times { |i| create(:reservation, check_in_date: date, check_out_date: date + 5.days, room: rooms[i]) }

      expect(service.call.value).to eq(rate[:upper_bound])
    end
  end
end
