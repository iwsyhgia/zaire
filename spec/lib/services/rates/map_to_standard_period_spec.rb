require 'rails_helper'

RSpec.describe Services::Rates::MapToStandardPeriod, type: :service do
  subject(:service) { described_class }

  let(:hotel) { create(:hotel, :with_standard_period) }
  let(:room_type) { create(:room_type, hotel: hotel) }
  let(:service_call) { service.call(room_type: room_type, hotel: hotel) }

  it 'creates a periods room type for a standard period' do
    expect { service_call }.to change(hotel.periods_room_types, :count).by(1)
    expect(service_call.value.rates.keys).to eq(DateTime::DAYS_INTO_WEEK.keys.map(&:to_s))
  end
end
