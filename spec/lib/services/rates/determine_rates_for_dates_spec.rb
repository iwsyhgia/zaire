require 'rails_helper'

RSpec.describe Services::Rates::DetermineRatesForDates, type: :service do
  subject(:service) { described_class.new(start_date: start_date, end_date: end_date) }

  let(:hotel) { create(:hotel) }
  let(:hotel_double) { instance_double('controller', current_hotel: hotel) }
  let(:room_types) { create_list(:room_type, 3, hotel: hotel) }
  let(:standard_period) { create(:period, :standard, start_date: nil, end_date: nil, hotel: hotel) }
  let(:periods_room_types) do
    room_types.each { |room_type| create(:periods_room_type, period: standard_period, room_type: room_type) }
  end

  before do
    periods_room_types
    allow(::Services::CurrentControllerService).to receive(:get) { hotel_double }
  end

  context 'with valid start and end date' do
    let(:start_date) { Date.current }
    let(:end_date) { Date.current + 10.days }

    it 'returns rates for dates' do
      result = service.call
      expect(result).to be_success
      expect(result.value.values.first).not_to be_empty
    end
  end
end
