require 'rails_helper'

RSpec.describe Services::Rates::InitStandardPeriod, type: :service do
  subject(:service) { described_class }

  let(:hotel) { create(:hotel, room_types: create_list(:room_type, 3, hotel: nil)) }
  let(:service_call) { service.call(hotel: hotel) }

  it 'creates a standard period with dynamic rates' do
    expect { service.call(hotel: hotel) }.to change(::Rates::Period, :count).by(1)
    expect(service_call.value).to be_standard
  end
end
