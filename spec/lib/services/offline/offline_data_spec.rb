# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Services::Offline::FetchData do
  subject(:fetch_date_service) { described_class.new(hotel: hotel) }

  let(:hotel) { create(:hotel_with_room_type) }
  let(:result) { fetch_date_service.call }

  context 'with enabled offline mode' do
    it 'returns saved data' do
      expect(result.value[:offline_storage][:invoices]).to eq([])
      expect(result.value[:offline_storage][:reservations]).to eq([])
      expect(result.value[:offline_storage][:room_types].map { |item| item[:id] }).to eq(hotel.room_types.map(&:id))
      expect(result.value[:offline_storage][:rooms].map { |item| item[:id] }).to eq(hotel.rooms.map(&:id))
    end

    it 'returns updated data with reservation which included in offline mode range' do
      create(:reservation, :two_days, room: hotel.rooms.first)

      expect(result.value[:offline_storage][:invoices].map { |item| item[:id] }).to eq(hotel.invoices.map(&:id))
      expect(result.value[:offline_storage][:reservations].map { |item| item[:id] }).to eq(hotel.reservations.map(&:id))
      expect(result.value[:offline_storage][:room_types].map { |item| item[:id] }).to eq(hotel.room_types.map(&:id))
      expect(result.value[:offline_storage][:rooms].map { |item| item[:id] }).to eq(hotel.rooms.map(&:id))
    end

    it 'returns saved data without reservation which not included in offline mode range' do
      create(:reservation, check_in_date: Date.current + 10.days,
                           check_out_date: Date.current + 15.days,
                           room: hotel.rooms.first)

      expect(result.value[:offline_storage][:invoices]).to eq([])
      expect(result.value[:offline_storage][:reservations]).to eq([])
      expect(result.value[:offline_storage][:room_types].map { |item| item[:id] }).to eq(hotel.room_types.map(&:id))
      expect(result.value[:offline_storage][:rooms].map { |item| item[:id] }).to eq(hotel.rooms.map(&:id))
    end
  end

  context 'with disabled offline mode' do
    before do
      hotel.offline_mode.active = false
      hotel.save
    end

    it 'returns empty data' do
      expect(result.value).to eq({})
    end
  end
end
