# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::CheckOutService do
  subject(:check_out_service) { described_class.new(reservation: reservation) }

  let(:reservation) { create(:reservation, :checked_in) }
  let(:result) { check_out_service.call }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to checked-out' do
    reservation.status = 'checked_in'

    expect { check_out_service.call }.to change(reservation, :status).to 'checked_out'
  end

  context 'with reservation, that can not be checked-out' do
    it 'fails if reservation was already checked-out' do
      reservation.status = 'checked_out'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already checked-out' do
      reservation.status = 'checked_out'

      expect(result.value).to include(I18n.t('reservations.errors.already_checked_out'))
    end

    it 'can not be checked-out if there is no transition to this state' do
      reservation.status = 'unconfirmed'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_checked_out'))
    end
  end
end
