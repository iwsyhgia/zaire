# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::ConfirmService do
  subject(:confirm_service) { described_class.new(reservation: reservation) }

  let(:reservation) { create(:reservation) }
  let(:result) { confirm_service.call }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to confirmed' do
    reservation.status = 'unconfirmed'

    expect { confirm_service.call }.to change(reservation, :status).to 'confirmed'
  end

  context 'with reservation, that can not be confirmed' do
    it 'fails if reservation was already confirmed' do
      reservation.status = 'confirmed'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already confirmed' do
      reservation.status = 'confirmed'

      expect(result.value).to include(I18n.t('reservations.errors.already_confirmed'))
    end

    it 'can not be confirmed if there is no transition to this state' do
      reservation.status = 'checked_out'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_confirmed'))
    end
  end
end
