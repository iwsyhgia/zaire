# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::CheckInService do
  subject(:check_in_service) { described_class.new(reservation: reservation) }

  let(:reservation) { create(:reservation) }
  let(:result) { check_in_service.call }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to checked_in' do
    reservation.status = 'unconfirmed'
    reservation.check_in_date = Date.current

    expect { check_in_service.call }.to change(reservation, :status).to 'checked_in'
  end

  context 'with reservation, that can not be checked in' do
    it 'fails if reservation was already checked in' do
      reservation.status = 'checked_in'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already checked in' do
      reservation.status = 'checked_in'

      expect(result.value).to include(I18n.t('reservations.errors.already_checked_in'))
    end

    it 'can not be checked in if there is no transition to this state' do
      reservation.status = 'cancelled'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_checked_in'))
    end

    it 'can not be checked in if reservation check in date after today' do
      reservation.check_in_date = Date.current + 1.day

      expect(result.value).to include(I18n.t('reservations.errors.only_current_date_check_in'))
    end
  end
end
