# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::CancelService do
  subject(:cancel_service) { described_class.new(reservation: reservation, cancellation_fee: false) }

  let(:reservation) { create(:reservation) }
  let(:result) { cancel_service.call }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to cancelled' do
    reservation.status = 'unconfirmed'

    expect { cancel_service.call }.to change(reservation, :status).to 'cancelled'
  end

  context 'with reservation, that can not be cancelled' do
    it 'fails if reservation was already canceled' do
      reservation.status = 'cancelled'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already canceled' do
      reservation.status = 'cancelled'

      expect(result.value).to include(I18n.t('reservations.errors.already_cancelled'))
    end

    it 'can not be cancelled if type is "DNR"' do
      reservation.type = 'Reservations::DNR'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_cancelled'))
    end

    it 'can not be cancelled if there is no transition to this state' do
      reservation.status = 'checked_in'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_cancelled'))
    end
  end
end
