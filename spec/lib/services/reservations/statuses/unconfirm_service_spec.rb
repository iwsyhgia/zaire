# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::UnconfirmService do
  subject(:unconfirm_service) { described_class.new(reservation: reservation) }

  let(:reservation) { create(:reservation, :confirmed) }
  let(:result) { unconfirm_service.call }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to unconfirmed' do
    reservation.status = 'confirmed'

    expect { unconfirm_service.call }.to change(reservation, :status).to 'unconfirmed'
  end

  context 'with reservation, that can not be unconfirmed' do
    it 'fails if reservation was already unconfirmed' do
      reservation.status = 'unconfirmed'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already unconfirmed' do
      reservation.status = 'unconfirmed'

      expect(result.value).to include(I18n.t('reservations.errors.already_unconfirmed'))
    end

    it 'can not be unconfirmed if there is no transition to this state' do
      reservation.status = 'checked_out'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_unconfirmed'))
    end
  end
end
