describe Services::Reservations::Statuses::DetermineStatus do
  subject(:service) { described_class.new(reservation: reservation) }

  let(:service_call) { service.call }
  let(:night_count) { 5 }
  let(:taxes_multiplier) { 1 + (invoice.vat_tax + invoice.municipal_tax) / 100 }
  let(:first_night_price) { 100 * taxes_multiplier }
  let(:full_stay_price) { first_night_price * night_count }
  let(:invoice) { create(:invoice) }
  let(:reservation) { create(:reservation, **reservation_attributes) }
  let(:reservation_attributes) { { invoice: invoice }.merge!(rate_attributes).merge!(status_attributes) }

  before do
    allow(reservation).to receive(:price_total_with_taxes).and_return(full_stay_price)
    allow(reservation).to receive(:price_per_night_with_taxes).and_return(first_night_price)
  end

  context 'with an unconfirmed reservation' do
    let(:status_attributes) { { status: :unconfirmed } }

    context 'with a refundable rate and a first night payment kind' do
      let(:rate_attributes) { { rate: :refundable, payment_kind: :first_night } }

      it 'changes status to confirmed' do
        allow(invoice).to receive(:amount).and_return(first_night_price - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('unconfirmed').to('confirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end

    context 'with a refundable rate and a full stay payment kind' do
      let(:rate_attributes) { { rate: :refundable, payment_kind: :full_stay } }

      it 'changes status to confirmed' do
        allow(invoice).to receive(:amount).and_return(full_stay_price - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('unconfirmed').to('confirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end

    context 'with a non-refundable reservation' do
      let(:rate_attributes) { { rate: :non_refundable } }

      it 'changes status to confirmed' do
        allow(invoice).to receive(:amount).and_return(full_stay_price - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('unconfirmed').to('confirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end
  end

  context 'with an confirmed reservation' do
    let(:status_attributes) { { status: :confirmed } }

    context 'with a refundable reservation and a first night payment kind' do
      let(:rate_attributes) { { rate: :refundable, payment_kind: :first_night } }

      it 'changes status to unconfirmed' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('confirmed').to('unconfirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(first_night_price - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end

    context 'with a refundable rate and a full stay payment kind' do
      let(:rate_attributes) { { rate: :refundable, payment_kind: :full_stay } }

      it 'changes status to unconfirmed' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('confirmed').to('unconfirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(full_stay_price - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end

    context 'with a non-refundable reservation' do
      let(:rate_attributes) { { rate: :non_refundable } }

      it 'changes status to unconfirmed' do
        allow(invoice).to receive(:amount).and_return(0 - full_stay_price)
        expect { service_call }.to change(reservation, :status).from('confirmed').to('unconfirmed')
      end

      it 'does not change status' do
        allow(invoice).to receive(:amount).and_return(full_stay_price - full_stay_price)
        expect { service_call }.not_to change(reservation, :status)
      end
    end
  end
end
