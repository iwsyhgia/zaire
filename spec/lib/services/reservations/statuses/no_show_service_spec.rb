# frozen_string_literal: true

require 'rails_helper'

describe Services::Reservations::Statuses::NoShowService do
  subject(:no_show_service) { described_class.new(reservation: reservation) }

  let(:reservation) { create(:hotel_with_reservation).reservations.first }
  let(:result) { no_show_service.call }

  before { allow(reservation.invoice).to receive(:current_rate).and_return 100 }

  it 'succeeds' do
    expect(result.success?).to be(true)
  end

  it 'changes reservation status to No-show' do
    reservation.status = 'unconfirmed'

    expect { no_show_service.call }.to change(reservation, :status).to 'no_show'
  end

  context 'with confirmed, non-refundable reservation' do
    it 'generates charge with no_show_fee kind and correct amount' do
      reservation.status = 'confirmed'
      reservation.rate = 'non_refundable'

      expect { no_show_service.call }.to change(Invoices::Charge, :count).by(1).
        and not_change(reservation, :check_out_date)
      expect(reservation.invoice.charges.first.kind).to eq('no_show_fee')
      expect(reservation.invoice.charges.first.amount).to eq((25 * 100) + (25 * 100) * (5.0 / 100))
    end
  end

  context 'with confirmed, refundable reservation' do
    it 'generates charge with no_show_fee kind and correct amount' do
      reservation.status = 'confirmed'
      reservation.rate = 'refundable'

      expect { no_show_service.call }.to change(Invoices::Charge, :count).by(1)
      expect(reservation.invoice.charges.first.kind).to eq('no_show_fee')
      expect(reservation.invoice.charges.first.amount).to eq((1 * 100) + (1 * 100) * (5.0 / 100))
    end
  end

  context 'with reservation, that can not be switched to No-show status' do
    it 'fails if reservation was already No-show' do
      reservation.status = 'no_show'

      expect(result).not_to be_success
    end

    it 'contains error if reservation was already No-show' do
      reservation.status = 'no_show'

      expect(result.value).to include(I18n.t('reservations.errors.already_no_show'))
    end

    it 'can not be switched to No-show status if there is no transition to this state' do
      reservation.status = 'checked_in'

      expect(result.value).to include(I18n.t('reservations.errors.cannot_be_no_showed'))
    end

    it 'does not generate a charge if reservation was already No-show' do
      reservation.status = 'no_show'

      expect { no_show_service.call }.not_to change(Invoices::Charge, :count)
    end
  end
end
