require 'rails_helper'
require 'mandrill_mailer/offline'

RSpec.describe Services::Reservations::DestroyDNR do
  subject(:destroy_dnr) { described_class.new(dnr: dnr) }

  let(:dnr) { create(:dnr, room_id: room.id) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:room_type) { audit.hotel.room_types.create(attributes_for(:room_type)) }
  let(:audit) { create(:audit, :completed) }
  let(:call_service) { destroy_dnr.call }

  context 'when a check-in date of dnr reservation is after audit date' do
    before do
      dnr.check_in_date = audit.time_frame_begin + 10.days
      dnr.check_out_date = audit.time_frame_begin + 15.days
    end

    it 'returns success' do
      expect(call_service.success?).to be(true)
    end

    it 'destroys dnr reservation' do
      expect { call_service }.to change(::Reservations::DNR, :count).by(-1)
    end
  end

  context 'when a check-in date of dnr reservation is before or the same with audit date' do
    before do
      dnr.check_in_date = audit.time_frame_begin.to_date
      dnr.check_out_date = audit.time_frame_begin.to_date + 5.days
    end

    it 'returns success' do
      expect(call_service.success?).to be(true)
    end

    it 'change check-out date of dnr reservation' do
      expect { call_service }.to change(dnr, :check_out_date).to audit.time_frame_end.to_date
    end

    it 'does not destroy dnr reservation' do
      expect { call_service }.to change(::Reservations::DNR, :count).by(0)
    end
  end

  context 'when a check-out date of dnr reservation is before current date' do
    before do
      dnr.check_in_date = Date.current - 5.days
      dnr.check_out_date = Date.current - 3.days
    end

    it 'returns error' do
      expect(call_service.value).to include(I18n.t('dnr.errors.cannot_be_deleted'))
    end

    it 'does not destroy dnr reservation' do
      expect { call_service }.to change(::Reservations::DNR, :count).by(0)
    end
  end

  context 'when a dnr reservation has already been deleted' do
    before do
      dnr.destroy
    end

    it 'returns error' do
      expect(call_service.value).to include(I18n.t('dnr.errors.already_deleted'))
    end
  end
end
