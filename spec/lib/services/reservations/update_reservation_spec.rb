# frozen_string_literal: true

require 'rails_helper'
require 'mandrill_mailer/offline'

RSpec.describe Services::Reservations::UpdateReservation do
  subject(:update_direct_reservation) { described_class.new(reservation: reservation) }

  let(:hotel) { create(:hotel) }
  let!(:standard_period) do
    period = create(:period, :standard, :fixed, hotel: hotel)
    create(:periods_room_type, period: period, room_type: room_type)
    create(:periods_room_type, period: period, room_type: another_room_type)
  end
  let(:another_room_type) { create(:room_type, hotel: hotel) }
  let(:room_type) { create(:room_type, hotel: hotel) }
  let(:room) { create(:room, room_type: room_type) }
  let(:another_room) { create(:room, room_type: another_room_type) }
  let(:reservation) { create(:reservation, room: room, status: :checked_in, invoice: invoice) }
  let(:invoice) { create(:invoice, current_rate: 100) }
  let(:call_service) do
    standard_period
    update_direct_reservation.call
  end

  before do
    ::Services::Invoices::RecalculateInvoice.call(invoice: reservation.invoice)
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
    allow(::Services::Sms::RoomChanged).to receive(:call)
    allow(::Services::Sms::RoomChanged).to receive(:call)
    allow(::Services::Sms::DatesChanged).to receive(:call)
    allow(Flipper).to receive(:enabled?).and_return(true)
  end

  context 'when a reservation changes room of another type' do
    let(:call_service) do
      reservation.assign_attributes(room_id: another_room.id)
      update_direct_reservation.call
    end

    it 'changes total price of the reservation' do
      # TODO: uncomment when rate management will be added
      # expect { call_service }.to change(reservation, :price_total)
    end

    it 'returns success' do
      expect(call_service.success?).to be(true)
    end

    it 'does not require a refund' do
      call_service
      expect(update_direct_reservation.send(:refund_needed?)).to be(false)
    end

    it 'sends notification sms to a guest who is associated with a reservation' do
      call_service

      expect(::Services::Sms::RoomChanged).to have_received(:call)
    end

    it 'sends notification email to a guest who is associated with a reservation' do
      expect { call_service }.to have_enqueued_job(MandrillMailer::MandrillTemplateJob)
    end
  end

  context 'when a reservation changes dates' do
    let(:call_service) do
      reservation.assign_attributes(check_out_date: reservation.check_out_date + 1.day)
      update_direct_reservation.call
    end

    it 'changes total price of the reservation' do
      expect { call_service }.to change(reservation.invoice, :price_total).and change(reservation.invoice, :updated_at)
    end

    it 'does require a refund' do
      reservation.assign_attributes(check_out_date: reservation.check_out_date - 3.day)
      call_service
      expect(update_direct_reservation.send(:refund_needed?)).to be(true)
    end

    it 'returns success' do
      expect(call_service.success?).to be(true)
    end

    it 'sends notification sms to a guest who is associated with a reservation' do
      call_service

      expect(::Services::Sms::DatesChanged).to have_received(:call)
    end

    it 'sends notification email to a guest who is associated with a reservation' do
      expect { call_service }.to have_enqueued_job(MandrillMailer::MandrillTemplateJob)
    end
  end

  context 'when a reservation changes room of same type' do
    let(:another_room) { room }

    it 'returns success' do
      expect(call_service.success?).to be(true)
    end

    it 'does not change total price of the reservation' do
      expect { call_service }.not_to change(reservation, :price_total)
    end
  end
end
