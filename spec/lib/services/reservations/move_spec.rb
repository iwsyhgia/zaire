# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ::Services::Reservations::Move do
  subject(:service) { described_class }

  let(:call_service) { service.call(dnr: dnr) }
  let(:api_controller) { instance_double(Api::ApiController, current_hotel: hotel) }
  let(:today) { Date.current }
  let(:hotel) { create(:hotel_with_reservation, reservation_count: 10) }
  let!(:rooms) { create(:room_type, rooms_count: 2, hotel: hotel).rooms }

  before { allow(::Services::CurrentControllerService).to receive(:get) { api_controller } }

  context 'when a new DNR reservation does not overlap reservations' do
    let(:dnr) { build(:dnr, room: rooms[0], check_in_date: today + 10.day, check_out_date: today + 11.day) }

    it 'returns success and does not move reservations' do
      expect { call_service }.not_to change(::Reservations::DirectReservation.order(:updated_at).last, :updated_at)
      expect(call_service).to be_success
      expect(call_service.value).to be_empty
    end
  end

  context 'when a persisted DNR reservation does not overlap another reservation(s)' do
    let(:dnr) { create(:dnr, room: rooms[1], check_in_date: today + 1.day, check_out_date: today + 2.days) }

    it 'returns success and does not move reservations' do
      dnr.assign_attributes(check_in_date: today + 10.day, check_out_date: today + 11.days)

      expect { call_service }.not_to change(::Reservations::DirectReservation.order(:updated_at).last, :updated_at)
      expect(call_service).to be_success
      expect(call_service.value).to be_empty
    end
  end

  context 'when a new DNR reservation overlaps with another reservation(s)' do
    let!(:reservations_cancelled) do
      create(:reservation, :cancelled, room: rooms[0], date_range: 0..5)
      create(:reservation, :cancelled, room: rooms[1], date_range: 0..5)
    end

    context 'when overlaps reservation with status checked-in' do
      let(:dnr) { build(:dnr, room: rooms[0], check_in_date: today, check_out_date: today + 1.day) }

      it 'returns an error with the message' do
        create(:reservation, :checked_in, room: rooms[0], check_in_date: today, check_out_date: today + 1.day)

        expect { call_service }.to raise_error(::Services::Exception, I18n.t('dnr.errors.checked_in_exists'))
      end
    end

    context 'when overlaps with another DNR reservation' do
      let(:dnr) { build(:dnr, room: rooms[0], check_in_date: today + 1.day, check_out_date: today + 4.day) }

      it 'returns an error with the message' do
        create(:dnr, room: rooms[0], check_in_date: today + 3.day, check_out_date: today + 4.day)

        expect { call_service }.to raise_error(::Services::Exception, I18n.t('dnr.errors.dnr_affected'))
      end
    end

    context 'when affected reservations cannot be moved' do
      let(:dnr) { build(:dnr, room: rooms[0], date_range: 1..3) }

      it 'returns an error with the message' do
        create(:reservation, :confirmed, room: rooms[0], date_range: 1..2)
        create(:reservation, :confirmed, room: rooms[0], date_range: 2..3)
        create(:reservation, :confirmed, room: rooms[1], date_range: 1..2)
        create(:reservation, :confirmed, room: rooms[1], date_range: 2..3)

        expect { call_service }.to raise_error(::Services::Exception, I18n.t('dnr.errors.cannot_be_moved'))
      end

      it 'returns an error message' do
        allow(Date).to receive(:current) { Date.parse('2019-04-16') }
        create(:reservation, :unconfirmed, room: rooms[0], check_in_date: '2019-04-16', check_out_date: '2019-04-20')
        create(:reservation, :checked_in, room: rooms[1], check_in_date: '2019-04-16', check_out_date: '2019-04-18')
        dnr = create(:dnr, room: rooms[1], check_in_date: '2019-04-18', check_out_date: '2019-04-20')

        dnr.assign_attributes(room_id: rooms[0].id)
        expect { service.call(dnr: dnr) }.to raise_error(::Services::Exception, I18n.t('dnr.errors.cannot_be_moved'))
      end
    end

    context 'when affected reservations can be moved' do
      let(:dnr) { build(:dnr, room: rooms[0], date_range: 1..5) }
      let(:moved_room_ids) { [rooms.second.id] * 3 }
      let(:moved_reservations_ids) { [] }

      it 'moves three reservations' do
        create(:reservation, :confirmed, room: rooms[1], date_range: 3..4)
        moved_reservations_ids << create(:reservation, :confirmed, room: rooms[0], date_range: 1..2).id
        moved_reservations_ids << create(:reservation, :confirmed, room: rooms[0], date_range: 2..3).id
        moved_reservations_ids << create(:reservation, :confirmed, room: rooms[0], date_range: 4..5).id

        expect(call_service).to be_success
        expect(call_service.value.map(&:room_id)).to contain_exactly(*moved_room_ids)
        expect(call_service.value.map(&:id)).to contain_exactly(*moved_reservations_ids)
      end
    end
  end

  context 'when a persisted DNR reservation overlaps another reservation(s)' do
    let!(:reservations) do
      [
        create(:reservation, :confirmed, room: rooms[0], date_range: 1..2),
        create(:reservation, :confirmed, room: rooms[0], date_range: 2..3)
      ]
    end

    context 'with a new room' do
      let(:dnr) { create(:dnr, room: rooms[1], date_range: 1..2) }

      it 'moves one affected reservation' do
        dnr.assign_attributes(room: rooms[0])

        expect(call_service).to be_success
        expect(call_service.value.map(&:room_id)).to contain_exactly(rooms[1].id)
      end

      it 'moves two affected reservations' do
        dnr.assign_attributes(room: rooms[0], check_in_date: today + 1.day, check_out_date: today + 3.day)

        expect(call_service).to be_success
        expect(call_service.value.map(&:room_id)).to contain_exactly(rooms[1].id, rooms[1].id)
        expect(call_service.value.map(&:id)).to contain_exactly(*reservations[0..1].map(&:id))
      end
    end

    context 'with new check-in/out dates' do
      let(:dnr) { create(:dnr, room: rooms[0], date_range: 1..2) }
      let!(:reservations) do
        [
          create(:reservation, :confirmed, room: rooms[0], date_range: 2..3),
          create(:reservation, :confirmed, room: rooms[0], date_range: 3..4)
        ]
      end

      it 'updates dates and moves one affected reservation' do
        dnr.assign_attributes(check_in_date: today + 3.day, check_out_date: today + 4.day)

        expect(call_service).to be_success
        expect(call_service.value.map(&:room_id)).to contain_exactly(rooms[1].id)
        expect(call_service.value.map(&:id)).to contain_exactly(reservations.second.id)
      end

      it 'updates dates and moves two affected reservations' do
        dnr.assign_attributes(check_out_date: today + 4.day)

        expect(call_service).to be_success
        expect(call_service.value.map(&:room_id)).to contain_exactly(rooms[1].id, rooms[1].id)
        expect(call_service.value.map(&:id)).to contain_exactly(*reservations.map(&:id))
      end
    end
  end
end
