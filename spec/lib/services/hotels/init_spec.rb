# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Services::Hotels::Init do
  subject(:init) { described_class.new(user: user) }

  let(:result) { init.call }
  let(:user) { create(:user) }

  before { result }

  it 'initializes hotel' do
    expect(result.success?).to be(true)
  end

  it 'creates only 1 hotel' do
    expect(::Hotels::Hotel.all.count).to eq(1)
  end

  it 'creates onfline mode that belogns to hotel' do
    expect(::Hotels::OfflineMode.first.hotel).to eq(user.hotel)
  end

  it 'creates only 1 offline mode' do
    expect(::Hotels::OfflineMode.all.count).to eq(1)
  end

  it 'creates address that belogns to hotel' do
    expect(::Hotels::Address.first.hotel).to eq(user.hotel)
  end

  it 'creates only 1 address' do
    expect(::Hotels::Address.all.count).to eq(1)
  end

  it 'creates configuration that belogns to hotel' do
    expect(::Hotels::Configuration.first.hotel).to eq(user.hotel)
  end

  it 'creates only 1 configuration' do
    expect(::Hotels::Configuration.all.count).to eq(1)
  end

  it 'creates check_in time that belogns to hotel' do
    expect(::Hotels::CheckTime.check_in.first.hotel).to eq(user.hotel)
  end

  it 'creates only 1 check_in time' do
    expect(::Hotels::CheckTime.check_in.count).to eq(1)
  end

  it 'creates check_in with given attributes' do
    expect(::Hotels::CheckTime.check_in.first).
      to have_attributes(fixed: true, start_time: '14:00')
  end

  it 'creates check_out time that belogns to hotel' do
    expect(::Hotels::CheckTime.check_out.first.hotel).to eq(user.hotel)
  end

  it 'creates only 1 check_out time' do
    expect(::Hotels::CheckTime.check_out.count).to eq(1)
  end

  it 'creates check_out with given attributes' do
    expect(::Hotels::CheckTime.check_out.first).
      to have_attributes(fixed: false, start_time: '12:00', end_time: '14:00')
  end
end
