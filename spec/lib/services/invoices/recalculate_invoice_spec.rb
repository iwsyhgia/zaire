# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable Metrics/LineLength

RSpec.describe Services::Invoices::RecalculateInvoice do
  let(:recalculate_totals) { described_class.new(invoice: invoice) }
  let(:taxes_multiplier) { 1 + (invoice.vat_tax + invoice.municipal_tax) / 100 }
  let(:price_total) { 30_000 }
  let(:payments_total) { 500 }
  let(:refunds_total) { 100 }
  let(:charges_total) { 15_000 }
  let(:charge1) { 100 }
  let(:charge2) { 1000 }

  let(:invoice) do
    create(:invoice, :with_default_totals, to: create(:reservation)) do |i|
      payments = create_list(:payment, 5, invoice: i, amount: 100)
      create_list(:refund, 5, payment: payments.first, invoice: i, amount: 20)
      create_list(:charge, 5, invoice: i, amount: 3000, kind: :sellable_items)
    end
  end
  let(:result) { recalculate_totals.call }

  before do
    allow(invoice.reservation).to receive(:price_total).and_return(price_total)
    result
  end

  context 'when a statement is a payment' do
    it 'recalculates successfully' do
      expect(result.success?).to be(true)
    end
    it 'recalculates payments total' do
      expect(invoice.payments_total).to eq(payments_total)
    end
  end

  context 'when a statement is a refund' do
    it 'recalculates successfully' do
      expect(result.success?).to be(true)
    end
    it 'recalculate a refund total' do
      expect(invoice.refunds_total).to eq(refunds_total)
    end
  end

  context 'when a statement is a charge' do
    it 'recalculates successfully' do
      expect(result.success?).to be(true)
    end
    it 'recalculates charges total' do
      expect(invoice.charges_total).to eq(charges_total)
    end
    it 'recalculates amount' do
      expect(invoice.amount).to eq(((- price_total - charges_total) * taxes_multiplier + payments_total - refunds_total).round)
    end
  end

  context 'with test case: charge-1/charge-2/amount-discount/percent-discount' do
    let(:discount) { [50, 0.8] }
    let(:amount) { ((price_total + charge1 + charge2) * taxes_multiplier - discount[0]) * discount[1] }

    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :amount, invoice: invoice, value: discount[0])
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[1]) * 100)
      end
    end

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq((price_total * taxes_multiplier - discount[0]) * discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 + charge2) * taxes_multiplier * discount[1])
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end

  context 'with test case: charge-1/charge-2/percent-discount/amount-discount' do
    let(:discount) { [0.8, 50] }

    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[0]) * 100)
        create(:discount, kind: :amount, invoice: invoice, value: discount[1])
      end
    end

    let(:amount) { (price_total + charge1 + charge2) * taxes_multiplier * discount[0] - discount[1] }

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq(price_total * taxes_multiplier * discount[0] - discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 + charge2) * taxes_multiplier * discount[0])
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end

  context 'with test case: charge-1/amount-discount/charge-2/percent-discount' do
    let(:discount) { [50, 0.8] }

    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:discount, kind: :amount, invoice: invoice, value: discount[0])
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[1]) * 100)
      end
    end

    let(:amount) { ((price_total + charge1 + charge2) * taxes_multiplier - discount[0]) * discount[1] }

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq((price_total * taxes_multiplier - discount[0]) * discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 + charge2) * taxes_multiplier * discount[1])
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end

  context 'with test case: charge-1/percent-discount/charge-2/amount-discount' do
    let(:discount) { [0.8, 50] }

    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[0]) * 100)
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :amount, invoice: invoice, value: discount[1])
      end
    end

    let(:amount) { (price_total + charge1) * taxes_multiplier * discount[0] + charge2 * taxes_multiplier - discount[1] }

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq((price_total * taxes_multiplier * discount[0]) - discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 * discount[0] + charge2) * taxes_multiplier)
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end

  context 'with test case: charge-1/charge-2/percent-discount/percent-discount' do
    let(:discount) { [0.8, 0.7] }
    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[0]) * 100)
        create(:discount, kind: :percent, invoice: invoice, value: (1 - discount[1]) * 100)
      end
    end

    let(:amount) { (price_total + charge1 + charge2) * taxes_multiplier * discount[0] * discount[1] }

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq(price_total * taxes_multiplier * discount[0] * discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 + charge2) * taxes_multiplier * discount[0] * discount[1])
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end

  context 'with test case: charge-1/charge-2/amount-discount/amount-discount' do
    let(:discount) { [50, 100] }
    let(:invoice) do
      create(:invoice, :with_default_totals, to: create(:reservation)) do |invoice|
        create(:charge, invoice: invoice, amount: charge1, kind: :sellable_items)
        create(:charge, invoice: invoice, amount: charge2, kind: :sellable_items)
        create(:discount, kind: :amount, invoice: invoice, value: discount[0])
        create(:discount, kind: :amount, invoice: invoice, value: discount[1])
      end
    end

    let(:amount) { (price_total + charge1 + charge2) * taxes_multiplier - discount[0] - discount[1] }

    it 'recalculates successfully and correctly' do
      expect(result.success?).to be(true)
      expect(invoice.amount).to eq(- amount)
      expect(invoice.discounts_total).to eq((price_total + charge1 + charge2) * taxes_multiplier - amount)
      expect(invoice.booking_total).to eq(price_total * taxes_multiplier - discount[0] - discount[1])
      expect(invoice.charges_with_taxes_total).to eq((charge1 + charge2) * taxes_multiplier)
      expect(invoice.refunds_total).to eq(0)
      expect(invoice.payments_total).to eq(0)
      expect(invoice.charges_total).to eq(charge1 + charge2)
    end
  end
end

# rubocop:enable Metrics/LineLength
