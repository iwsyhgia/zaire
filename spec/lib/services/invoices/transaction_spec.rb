# frozen_string_literal: true

require 'rails_helper'

# rubocop:disable RSpec/InstanceVariable
RSpec.describe Services::Invoices::Transaction, type: :model do
  describe 'POST create' do
    let(:source) { create(:guest).tap { |obj| obj.create_balance(amount: 10_001) } }
    let(:target) { create(:reservation).tap { |obj| obj.create_balance(amount: 1) } }
    let(:payment) { create(:payment, amount: 9.99) }
    let(:transaction) do
      ::Services::Invoices::Transaction.new(
        source: source,
        target: target,
        document: payment,
        kind: :offline_payment
      )
    end

    before do
      @result = transaction.call { {} }
      @source_transaction = ::Invoices::Transaction.find_by(source: source)
      @target_transaction = ::Invoices::Transaction.find_by(source: target)
    end

    it 'generates successful transaction' do
      expect(@result.success?).to be true
    end

    it 'reduces source balance by invoice amount' do
      expect(::Invoices::Balance.for(source).amount).to eq(9991.01)
    end

    it 'increases target balance by invoice amount' do
      expect(::Invoices::Balance.for(target).amount).to eq(10.99)
    end

    it 'creates only 2 transactions' do
      expect(::Invoices::Transaction.all.count).to eq(2)
    end

    it 'creates source transaction with amount equal to what is paid with minus sign' do
      expect(@source_transaction.amount).to eq(-9.99)
    end

    it 'creates successful source transaction' do
      expect(@source_transaction.successful?).to be true
    end

    it 'creates target transaction with amount equal to what is paid' do
      expect(@target_transaction.amount).to eq(9.99)
    end

    it 'creates successful target transaction' do
      expect(@target_transaction.successful?).to be true
    end
  end
end
# rubocop:enable RSpec/InstanceVariable
