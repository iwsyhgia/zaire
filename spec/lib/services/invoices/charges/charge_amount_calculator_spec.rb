# frozen_string_literal: true

require 'rails_helper'

describe Services::Invoices::Charges::ChargeAmountCalculator do
  let(:hotel) { create(:hotel_with_room_type, room_type_count: 1) }
  let(:rooms) { hotel.rooms }

  before { allow(reservation.invoice).to receive(:current_rate).and_return 100 }

  context 'with no_show_fee kind' do
    subject(:service_runner) { described_class.new(reservation: reservation, kind: 'no_show_fee') }

    describe '#unconfirmed_reservation' do
      let(:reservation) do
        create(:reservation, :unconfirmed, room: rooms[0],
                                          check_in_date: Date.current,
                                          check_out_date: Date.current + 2.days)
      end

      it 'correctly calculates amounts' do
        result = service_runner.call

        expect(result).to be_success
        expect(result.value[:base_price]).to eq(100)
        expect(result.value[:amount]).to eq(105)
        expect(result.value[:municipal_tax_amount]).to eq(0)
        expect(result.value[:vat_tax_amount]).to eq(5)
      end
    end

    describe '#confirmed_reservation' do
      let(:reservation) do
        create(:reservation, :confirmed, :non_refundable, room: rooms[0],
                                                          check_in_date: Date.current,
                                                          check_out_date: Date.current + 2.days)
      end

      it 'correctly calculates amounts' do
        result = service_runner.call

        expect(result).to be_success
        expect(result.value[:base_price]).to eq(200)
        expect(result.value[:amount]).to eq(210)
        expect(result.value[:municipal_tax_amount]).to eq(0)
        expect(result.value[:vat_tax_amount]).to eq(10)
      end
    end
  end

  context 'with stay_fee kind' do
    subject(:service_runner) { described_class.new(reservation: reservation, kind: 'stay_fee') }

    let(:reservation) do
      create(:reservation, :unconfirmed, room: rooms[0],
                                         check_in_date: Date.current,
                                         check_out_date: Date.current + 2.days)
    end

    it 'correctly calculates amounts' do
      result = service_runner.call

      expect(result).to be_success
      expect(result.value[:base_price]).to eq(100)
      expect(result.value[:amount]).to eq(107.5)
      expect(result.value[:municipal_tax_amount]).to eq(2.5)
      expect(result.value[:vat_tax_amount]).to eq(5)
    end
  end

  context 'with cancellation_fee kind' do
    subject(:service_runner) { described_class.new(reservation: reservation, kind: 'cancellation_fee') }

    let(:reservation) do
      create(:reservation, :unconfirmed, room: rooms[0],
                                         check_in_date: Date.current,
                                         check_out_date: Date.current + 2.days)
    end

    it 'correctly calculates amounts' do
      result = service_runner.call

      expect(result).to be_success
      expect(result.value[:base_price]).to eq(100)
      expect(result.value[:amount]).to eq(105.0)
      expect(result.value[:municipal_tax_amount]).to eq(0)
      expect(result.value[:vat_tax_amount]).to eq(5)
    end
  end
end
