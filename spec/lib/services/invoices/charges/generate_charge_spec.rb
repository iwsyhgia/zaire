# frozen_string_literal: true

require 'rails_helper'

describe Services::Invoices::Charges::GenerateCharge do
  let(:hotel) { create(:hotel_with_room_type, room_type_count: 1) }
  let(:rooms) { hotel.rooms }
  let(:reservation) do
    create(:reservation, :checked_in, room: rooms[0],
                                      check_in_date: Date.current,
                                      check_out_date: Date.current + 2.days)
  end

  before { allow(reservation.invoice).to receive(:current_rate).and_return 100 }

  context 'without audit:' do
    subject(:service_runner) { described_class.new(reservation: reservation, kind: 'cancellation_fee') }

    it 'creates cancellation_fee charge' do
      result = service_runner.call

      expect(result).to be_success
      expect(reservation.invoice.charges.cancellation_fee.count).to eq 1
    end
  end

  context 'with audit:' do
    subject(:service_runner) { described_class.new(reservation: reservation, audit: audit, kind: 'stay_fee') }

    let(:audit) { hotel.audits.last }

    context "#create_stay_charges step when audit is #{(DateTime.now - 1.day).to_date} - #{DateTime.now.to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now - 1.day, time_frame_end: DateTime.now)
      end

      it 'does not create charges if there are no check_ins' do
        expect { service_runner.call }.to change { reservation.invoice.charges.count }.by(0)
      end
    end

    context "when audit is #{DateTime.now.to_date} - #{(DateTime.now + 1.day).to_date}" do
      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now, time_frame_end: DateTime.now + 1.day)
      end

      it 'creates charges for checked_in reservations when next audit came' do
        expect { service_runner.call }.to change { reservation.invoice.charges.count }.by(1)
      end

      it 'successfully updates invoice totals' do
        price_with_taxes = 107.5
        expect { service_runner.call }.to change { reservation.invoice.charges_total }.by(price_with_taxes)
      end

      it 'does not create excessive charges if run for the second time' do
        service_runner.call
        expect { service_runner.call }.to change { reservation.invoice.charges.count }.by(0)
      end

      it 'creates charges for each nights of checked_in reservations' do
        audit.time_frame_begin = DateTime.now + 1.day
        audit.time_frame_end = DateTime.now + 2.day

        expect { service_runner.call }.to change { reservation.invoice.charges.count }.by(1)
      end
    end

    describe 'errors handling' do
      subject(:charge_for_no_show) { described_class.new(reservation: reservation, audit: audit, kind: 'no_show_fee') }

      let(:audit) do
        create(:audit, :confirmed, hotel: hotel, time_frame_begin: DateTime.now, time_frame_end: DateTime.now + 1.day)
      end

      it 'does not check night existance with no_show_fee kind' do
        reservation.check_in_date = Date.current - 2.days
        reservation.check_out_date = Date.current - 1.day

        expect { charge_for_no_show.call }.to change { reservation.invoice.charges.count }.by(1)
      end

      it 'does not create charge if audit night is out of booked nights' do
        reservation.check_in_date = Date.current - 2.days
        reservation.check_out_date = Date.current - 1.day

        result = service_runner.call
        expect(result).not_to be_success
        expect(result.value).to eq('Passed night is out of booked nights.')
      end

      it 'returns service error object with error log' do
        audit.update(time_frame_end: nil)

        result = service_runner.call
        expect(result).not_to be_success
        expect(result.value).to be_instance_of(::Audits::ErrorLog)
      end

      it 'returns service error object and does not saving the charge if the recalucaltion service failed' do
        recalculate_invoice_class = ::Services::Invoices::RecalculateInvoice
        allow(recalculate_invoice_class).to receive(:call).and_return(::Services::Error.new('error', 400))

        result = service_runner.call

        expect(result).not_to be_success
        expect(reservation.invoice.charges.count).to eq 0
        expect(reservation.invoice.charges_total).to eq 0
      end
    end
  end
end
