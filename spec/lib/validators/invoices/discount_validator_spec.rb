# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Invoices::DiscountValidator, type: :model do
  subject(:validator) { described_class.new(discount_attributes, discount) }

  let(:discount) { create(:discount, invoice_id: invoice.id) }
  let(:hotel) { create(:hotel_with_reservation) }
  let(:invoice) { create(:invoice, to: hotel.reservations.first) }
  let(:creation_ways) { ::Invoices::Discount.creation_ways.keys }
  let(:kinds) { ::Invoices::Discount.kinds.keys }
  let(:discount_attributes) do
    attributes_for(:discount, user_id: hotel.user_id)
  end

  describe '#kind' do
    context 'when id presents' do
      it { is_expected.not_to validate_presence_of(:kind) }
      it { is_expected.to allow_value(nil).for(:kind) }
    end

    context 'when id is missing' do
      before do
        validator.id = nil
        validator.valid?
      end

      it { is_expected.to validate_presence_of(:kind) }
      it { is_expected.not_to validate_absence_of(:kind) }
      it { is_expected.not_to allow_value(nil).for(:kind) }
      it { is_expected.to allow_value('amount').for(:kind) }
      it { is_expected.not_to allow_value('not a kind').for(:kind) }
      it do
        expect(validator).to validate_inclusion_of(:kind).
          in_array(kinds).
          with_message(I18n.t('discounts.errors.kind_invalid', valid_values: kinds))
      end
    end
  end

  describe '#user_id' do
    before do
      validator.id = nil
      validator.valid?
    end

    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.not_to allow_value(nil).for(:user_id) }
  end

  describe '#description' do
    context 'when id is missing' do
      before do
        validator.id = nil
        validator.valid?
      end

      it { is_expected.to validate_presence_of(:description) }
      it { is_expected.not_to validate_absence_of(:description) }
      it { is_expected.to allow_value('description').for(:description) }
      it { is_expected.not_to allow_value('v' * 256).for(:description) }
      it { is_expected.not_to allow_value(nil).for(:description) }
    end
  end

  describe '#value' do
    before do
      validator.id = nil
      validator.valid?
    end

    it { is_expected.to validate_presence_of(:value) }
    it { is_expected.not_to allow_value(nil).for(:value) }

    context 'with amount kind' do
      let(:discount) { create(:discount, kind: 'amount', invoice_id: invoice.id) }

      before do
        validator.kind = 'amount'
        validator.valid?
      end

      it { is_expected.to validate_numericality_of(:value).is_greater_than(0).is_less_than(1_000_000) }
      it { is_expected.to allow_value(10_000).for(:value) }
      it { is_expected.not_to allow_value(-1).for(:value) }
      it { is_expected.not_to allow_value(1_000_001).for(:value) }
    end

    context 'with percent kind' do
      let(:discount) { create(:discount, kind: 'percent', invoice_id: invoice.id) }

      before do
        validator.kind = 'percent'
        validator.valid?
      end

      it { is_expected.to validate_numericality_of(:value).is_greater_than(0).is_less_than(100) }
      it { is_expected.to allow_value(10).for(:value) }
      it { is_expected.not_to allow_value(101).for(:value) }
      it { is_expected.not_to allow_value(-1).for(:value) }
    end
  end

  describe '#creation_way' do
    it { is_expected.to allow_value('manual').for(:creation_way) }
    it { is_expected.not_to allow_value('not a creation way').for(:creation_way) }
    it do
      expect(validator).to validate_inclusion_of(:creation_way).
        in_array(creation_ways).
        with_message(I18n.t('discounts.errors.creation_way_invalid', valid_values: creation_ways))
    end
  end
end
