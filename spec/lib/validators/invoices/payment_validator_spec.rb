# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Invoices::PaymentValidator, type: :model do
  subject(:validator) { described_class.new(payment_attributes, payment) }

  let(:payment) { create(:payment, invoice_id: invoice.id) }
  let(:hotel) { create(:hotel_with_reservation) }
  let(:invoice) { create(:invoice, to: hotel.reservations.first) }
  let(:creation_ways) { ::Invoices::Payment.creation_ways.keys }
  let(:payment_methods) { ::Invoices::Payment.payment_methods.keys }
  let(:payment_attributes) do
    attributes_for(
      :payment,
      user_id: hotel.user_id
    ).without(:status)
  end

  describe '#payment_method' do
    it { is_expected.to validate_presence_of(:payment_method) }
    it { is_expected.not_to allow_value(nil).for(:payment_method) }
    it { is_expected.to allow_value('card').for(:payment_method) }
    it { is_expected.not_to allow_value('not a payment method').for(:payment_method) }
    it do
      expect(validator).to validate_inclusion_of(:payment_method).
        in_array(payment_methods).
        with_message(I18n.t('payments.errors.payment_method_invalid', valid_values: payment_methods))
    end
  end

  describe '#creation_way' do
    it { is_expected.to allow_value('manual').for(:creation_way) }
    it { is_expected.not_to allow_value('not a creation way').for(:creation_way) }
    it do
      expect(validator).to validate_inclusion_of(:creation_way).
        in_array(creation_ways).
        with_message(I18n.t('payments.errors.creation_way_invalid', valid_values: creation_ways))
    end
  end

  describe '#user_id' do
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.not_to allow_value(nil).for(:user_id) }
  end

  describe '#amount' do
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than(0).is_less_than(1_000_000) }
    it { is_expected.to allow_value(5).for(:amount) }
    it { is_expected.not_to allow_value(0).for(:amount) }
    it { is_expected.not_to allow_value(1_000_001).for(:amount) }
    it { is_expected.not_to allow_value(nil).for(:amount) }
  end
end
