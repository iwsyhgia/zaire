# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Rooms::DestroyRoomTypeValidator, type: :model do
  subject(:validator) { described_class.new(destroy_room_type_attributes) }

  let(:destroy_room_type_attributes) { { id: hotel.room_types.first.id } }

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  context 'with valid attributes' do
    let(:hotel) { create(:hotel_with_room_type) }

    before do
      validator.valid?
    end

    describe '#validate_ability_be_destroyed' do
      it 'validates ability to be destroyed' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    let(:hotel) { create(:hotel_with_reservation) }

    before do
      validator.valid?
    end

    describe '#validate_ability_be_destroyed' do
      it 'validates invalid amenities_attributes' do
        expect(validator.errors[:base]).
          to include('Room Type cannot be destroyed because it associated with a reservation')
      end
    end
  end
end
