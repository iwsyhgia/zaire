# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Rooms::RoomTypeValidator, type: :model do
  subject(:validator) { described_class.new(room_type_attributes, room_type) }

  let(:hotel) { create(:hotel) }
  let(:room_type) { create(:room_type, hotel_id: hotel.id) }
  let(:room_type_attributes) do
    attributes_for(:room_type).merge(
      images_attributes: [attributes_for(:image, :with_base64)],
      rooms_attributes: [attributes_for(:room)],
      amenities_attributes: [attributes_for(:amenity)]
    )
  end

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  describe '#name' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to allow_value('Room type name').for(:name) }
    it { is_expected.not_to allow_value(nil).for(:name) }
    it { is_expected.not_to allow_value('').for(:name) }
    it { is_expected.not_to allow_value('v' * 31).for(:name) }
  end

  describe '#max_occupancy' do
    it { is_expected.to validate_presence_of(:max_occupancy) }
    it { is_expected.to allow_value(123).for(:max_occupancy) }
    it { is_expected.not_to allow_value(nil).for(:max_occupancy) }
    it { is_expected.not_to allow_value(0).for(:max_occupancy) }
    it { is_expected.not_to allow_value(1001).for(:max_occupancy) }
    it { is_expected.not_to allow_value(100.01).for(:max_occupancy) }
  end

  describe '#quantity' do
    it { is_expected.to validate_presence_of(:quantity) }
    it { is_expected.to allow_value(123).for(:quantity) }
    it { is_expected.not_to allow_value(nil).for(:quantity) }
    it { is_expected.not_to allow_value(0).for(:quantity) }
    it { is_expected.not_to allow_value(1001).for(:quantity) }
    it { is_expected.not_to allow_value(100.01).for(:quantity) }
  end

  describe '#unit' do
    it { is_expected.to allow_value('Room type unit').for(:unit) }
    it { is_expected.to allow_value('').for(:unit) }
    it { is_expected.not_to allow_value('v' * 31).for(:unit) }
  end

  describe '#size' do
    it { is_expected.to allow_value('Room type size').for(:size) }
    it { is_expected.to allow_value('').for(:size) }
    it { is_expected.not_to allow_value('v' * 31).for(:size) }
  end

  context 'with valid attributes' do
    before do
      validator.valid?
    end

    describe '#images_attributes' do
      it 'validates valid images_attributes' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#rooms_attributes' do
      it 'validates valid rooms_attributes' do
        expect(validator.errors[:rooms]).to be_empty
      end
    end

    describe '#amenities_attributes' do
      it 'validates valid amenities_attributes' do
        expect(validator.errors[:amenity]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    let(:room_type_attributes) do
      attributes_for(:room_type).merge(
        images_attributes: [attributes_for(:image)],
        rooms_attributes: [attributes_for(:invalid_room)],
        amenities_attributes: [attributes_for(:invalid_amenity)]
      )
    end

    before do
      validator.valid?
    end

    describe '#images_attributes' do
      it 'validates invalid images_attributes' do
        expect(validator.errors[:base]).to include("Base64 can't be blank")
      end
    end

    describe '#rooms_attributes' do
      it 'validates invalid rooms_attributes' do
        expect(validator.errors[:base]).to include("Number can't be blank")
      end
    end

    describe '#amenities_attributes' do
      it 'validates invalid amenities_attributes' do
        expect(validator.errors[:amenity]).to include("Name can't be blank")
      end
    end
  end
end
