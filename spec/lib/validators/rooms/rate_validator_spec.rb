require 'rails_helper'

RSpec.describe Validators::Rooms::RateValidator, type: :model do
  subject(:validator) { described_class.new(attributes, nil) }

  let(:kind_message) { I18n.t('rates.errors.kind_invalid', values: ::Rooms::Rate.kinds.keys.join(', ')) }
  let(:attributes) { attributes_for(:rate) }

  describe '#kind' do
    it { is_expected.to validate_presence_of(:kind) }
    it { is_expected.to validate_inclusion_of(:kind).in_array(::Rooms::Rate.kinds.keys).with_message(kind_message) }
    it { is_expected.to allow_value('fixed').for(:kind) }
    it { is_expected.to allow_value('dynamic').for(:kind) }
    it { is_expected.not_to allow_value(nil).for(:kind) }
    it { is_expected.not_to allow_value('not valid').for(:kind) }
  end

  context 'with a fixed kind' do
    let(:attributes) { attributes_for(:rate, kind: 'fixed', upper_bound: nil) }

    describe '#lower_bound' do
      it { is_expected.to validate_presence_of(:lower_bound) }
      it { is_expected.to validate_numericality_of(:lower_bound).is_less_than(100_000).is_greater_than(0) }
    end

    describe '#upper_bound' do
      it { is_expected.to validate_absence_of(:upper_bound) }
    end
  end

  context 'with a dynamic rate' do
    let(:attributes) { attributes_for(:rate, kind: 'dynamic') }

    describe '#lower_bound' do
      it { is_expected.to validate_presence_of(:lower_bound) }
      it { is_expected.to validate_numericality_of(:lower_bound).is_less_than(100_000).is_greater_than(0) }
    end

    describe '#upper_bound' do
      it { is_expected.to validate_presence_of(:upper_bound) }
      it { is_expected.to validate_numericality_of(:upper_bound).is_greater_than_or_equal_to(attributes[:lower_bound]) }
    end
  end
end
