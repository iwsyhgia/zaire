# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Rooms::RoomValidator, type: :model do
  subject { Validators::Rooms::RoomValidator.new(room_attributes, room) }

  let(:hotel) { create(:hotel_with_room_type) }
  let(:room) { create(:room, room_type_id: hotel.room_types.first.id) }
  let(:room_attributes) { attributes_for(:room) }

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  describe '#number' do
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to allow_value(123).for(:number) }
    it { is_expected.not_to allow_value(nil).for(:number) }
    it { is_expected.not_to allow_value(0).for(:number) }
    it { is_expected.not_to allow_value(10_001).for(:number) }
    it { is_expected.not_to allow_value(100.01).for(:number) }
  end
end
