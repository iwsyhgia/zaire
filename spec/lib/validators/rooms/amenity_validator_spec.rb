# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Rooms::AmenityValidator, type: :model do
  subject { Validators::Rooms::AmenityValidator.new(amenity_attributes, amenity) }

  let(:hotel) { create(:hotel_with_room_type) }
  let(:amenity) { create(:amenity) }
  let(:amenity_attributes) { attributes_for(:amenity) }

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  describe '#name' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to allow_value('amenity name').for(:name) }
    it { is_expected.not_to allow_value(nil).for(:name) }
    it { is_expected.not_to allow_value('').for(:name) }
    it { is_expected.not_to allow_value('v' * 51).for(:name) }
  end
end
