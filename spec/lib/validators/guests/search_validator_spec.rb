# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Guests::SearchValidator, type: :model do
  subject(:validator) { described_class.new(search_attributes) }

  let(:search_attributes) { attributes_for(:guest_search) }

  describe '#email_cont' do
    it { is_expected.to validate_length_of(:email_cont) }
    it { is_expected.to allow_value('example').for(:email_cont) }
    it { is_expected.to allow_value('').for(:email_cont) }
    it { is_expected.not_to allow_value('v' * 101).for(:email_cont) }
  end

  describe '#phone_number_cont' do
    it { is_expected.to validate_length_of(:phone_number_cont) }
    it { is_expected.to allow_value('example').for(:phone_number_cont) }
    it { is_expected.to allow_value('').for(:phone_number_cont) }
    it { is_expected.not_to allow_value('v' * 101).for(:phone_number_cont) }
  end

  context 'with valid attributes' do
    before do
      validator.valid?
    end

    describe '#first_name_cont' do
      it 'validates valid first name length' do
        expect(validator.errors[:guests]).to be_empty
      end
    end

    describe '#last_name_cont' do
      it 'validates valid last name length' do
        expect(validator.errors[:guests]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    before do
      validator.g = [first_name_cont: 'v' * 101, last_name_cont: 'v' * 101]
      validator.valid?
    end

    describe '#first_name_cont' do
      it 'validates invalid first name length' do
        expect(validator.errors[:first_name_cont]).to include('is the wrong length')
      end
    end

    describe '#last_name_cont' do
      it 'validates invalid last name length' do
        expect(validator.errors[:last_name_cont]).to include('is the wrong length')
      end
    end
  end
end
