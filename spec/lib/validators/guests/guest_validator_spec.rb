# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Guests::GuestValidator, type: :model do
  subject(:validator) { described_class.new(guest_attributes) }

  let(:hotel) { create(:hotel_with_reservation) }
  let(:guest_attributes) { attributes_for(:guest) }

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
    validator.valid?
  end

  describe '#first_name' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to allow_value('Something').for(:first_name) }
    it { is_expected.not_to allow_value(nil).for(:first_name) }
    it { is_expected.not_to allow_value('').for(:first_name) }
    it { is_expected.not_to allow_value('v' * 121).for(:first_name) }
  end

  describe '#last_name' do
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to allow_value('Something').for(:last_name) }
    it { is_expected.not_to allow_value(nil).for(:last_name) }
    it { is_expected.not_to allow_value('').for(:last_name) }
    it { is_expected.not_to allow_value('v' * 121).for(:last_name) }
  end

  describe '#email' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to allow_value('example@test.com').for(:email) }
    it { is_expected.not_to allow_value('').for(:email) }
    it { is_expected.not_to allow_value(nil).for(:email) }
  end

  describe '#birth_date' do
    it { is_expected.to allow_value(1.day.ago).for(:birth_date) }
    it { is_expected.not_to allow_value(Date.current).for(:birth_date) }
  end

  describe '#phone_code' do
    before do
      validator.email = nil
      validator.valid?
    end

    it { is_expected.to validate_numericality_of(:phone_code).only_integer }
    it { is_expected.to allow_value(375).for(:phone_code) }
    it { is_expected.not_to allow_value(nil).for(:phone_code) }
    it { is_expected.not_to allow_value(0).for(:phone_code) }
  end

  describe '#phone_number' do
    before do
      validator.email = nil
      validator.valid?
    end

    it { is_expected.to validate_presence_of(:phone_number) }
    it { is_expected.to allow_value('1234567').for(:phone_number) }
    it { is_expected.not_to allow_value(nil).for(:phone_number) }
    it { is_expected.not_to allow_value('').for(:phone_number) }
  end

  describe '#country' do
    it { is_expected.to validate_presence_of(:country) }
    it { is_expected.to allow_value('Country').for(:country) }
    it { is_expected.not_to allow_value(nil).for(:country) }
  end

  describe '#city' do
    it { is_expected.to allow_value('City').for(:city) }
  end
end
