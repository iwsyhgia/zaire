# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Profiles::ProfileValidator, type: :model do
  subject(:validator) { described_class.new(profile_attributes) }

  let(:hotel) { create(:hotel) }
  let(:profile_attributes) do
    attributes_for(:profile).merge(
      user_attributes: { current_password: 'Qw123123' }
    )
  end

  before do
    allow(::Services::CurrentControllerService).to receive(:get) do
      instance_double('controller', current_hotel: hotel)
    end
    validator.valid?
  end

  describe '#first_name' do
    it { is_expected.to validate_length_of(:first_name) }
    it { is_expected.to allow_value(nil).for(:first_name) }
    it { is_expected.to allow_value('First name').for(:first_name) }
    it { is_expected.to allow_value('').for(:first_name) }
    it { is_expected.not_to allow_value('v' * 43).for(:first_name) }
  end

  describe '#last_name' do
    it { is_expected.to validate_length_of(:last_name) }
    it { is_expected.to allow_value(nil).for(:last_name) }
    it { is_expected.to allow_value('First name').for(:last_name) }
    it { is_expected.to allow_value('').for(:last_name) }
    it { is_expected.not_to allow_value('v' * 43).for(:last_name) }
  end

  describe '#phone_number' do
    it { is_expected.to validate_length_of(:phone_number) }
    it { is_expected.to allow_value(nil).for(:phone_number) }
    it { is_expected.not_to allow_value('-123145235').for(:phone_number) }
    it { is_expected.to allow_value('+123145235').for(:phone_number) }
    it { is_expected.to allow_value('').for(:phone_number) }
    it { is_expected.not_to allow_value('2' * 43).for(:phone_number) }
  end

  describe '#notifications' do
    it { is_expected.to allow_value('off').for(:notifications) }
    it { is_expected.to allow_value('on').for(:notifications) }
    it { is_expected.not_to allow_value('invalid').for(:notifications) }
  end

  context 'with valid attributes' do
    describe '#validate_user_attributes' do
      it 'validates user attributes' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#validate_user_attributes' do
      before do
        validator.user_attributes[:current_password] = 'invalid'
        validator.valid?
      end

      it 'validates password matching' do
        expect(validator.errors[:base]).to include('Current password does not match')
      end
    end
  end
end
