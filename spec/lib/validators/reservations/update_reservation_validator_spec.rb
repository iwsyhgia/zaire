# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Reservations::UpdateReservationValidator, type: :model do
  subject(:validator) { described_class.new(reservation_attributes, reservation) }

  let(:room_type) { create(:hotel).room_types.create(attributes_for(:room_type, max_occupancy: 10)) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:reservation) { create(:reservation, room: room) }
  let(:reservation_attributes) do
    attributes_for(:reservation).merge(
      room_id: room.id,
      adults_attributes: [attributes_for(:adult), attributes_for(:adult)],
      number_of_adults: 2
    ).without(:type, :rate, :status, :payment_kind)
  end

  context 'with valid attributes' do
    describe '#validate_adults_uniqueness' do
      before do
        validator.valid?
      end

      it 'validates adults id uniqueness' do
        expect(validator.errors[:adults]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#validate_adults_uniqueness' do
      before do
        validator.adults_attributes.first[:personal_id] = validator.adults_attributes.last[:personal_id]
        validator.valid?
      end

      it 'validates that adult id is not unique' do
        expect(validator.errors[:adults]).to include('Personal ID must be unique within a reservation.')
      end
    end
  end

  context 'with confirmed reservation' do
    let(:reservation) do
      create(:reservation, room: room, status: :confirmed,
        check_in_date: Date.current + 10.days,
        check_out_date: Date.current + 20.days)
    end

    it 'allows to extend dates' do
      validator.check_in_date = reservation.check_in_date - 5.days
      validator.check_out_date = reservation.check_out_date + 5.days
      expect(validator).to be_valid
    end

    it 'forbids to extend check-in date after check-in day' do
      error_message = I18n.t('errors.messages.is_at', restriction: reservation.check_in_date)
      reservation.check_in_date = Date.current - 1.day
      validator.check_in_date = reservation.check_in_date + 5.days

      expect(validator).to be_invalid
      expect(validator.errors[:check_in_date]).to include(error_message)
    end
  end

  context 'with checked-in reservation' do
    let(:reservation) do
      create(:reservation, room: room, status: :checked_in,
        check_in_date: Date.current + 10.days,
        check_out_date: Date.current + 20.days)
    end

    it 'allows to extend check-out date' do
      validator.check_in_date = reservation.check_in_date
      validator.check_out_date = reservation.check_out_date + 1.day

      expect(validator).to be_valid
    end

    it 'forbids to extend check-in date' do
      error_message = I18n.t('errors.messages.is_at', restriction: reservation.check_in_date)
      reservation.check_in_date = Date.current
      validator.check_in_date = reservation.check_in_date - 1.day
      validator.check_out_date = reservation.check_out_date

      expect(validator).to be_invalid
      expect(validator.errors[:check_in_date]).to include(error_message)
    end
  end
end
