# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Reservations::AdultValidator, type: :model do
  subject(:validator) { described_class.new(adult_attributes, adult) }

  let(:adult) { create(:adult) }
  let(:adult_attributes) { attributes_for(:adult).merge(reservation_id: adult.reservation_id) }

  describe '#first_name' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_length_of(:first_name) }
    it { is_expected.to allow_value('Something').for(:first_name) }
    it { is_expected.not_to allow_value(nil).for(:first_name) }
    it { is_expected.not_to allow_value('').for(:first_name) }
    it { is_expected.not_to allow_value('v' * 101).for(:first_name) }
  end

  describe '#last_name' do
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_length_of(:last_name) }
    it { is_expected.to allow_value('Something').for(:last_name) }
    it { is_expected.not_to allow_value(nil).for(:last_name) }
    it { is_expected.not_to allow_value('').for(:last_name) }
    it { is_expected.not_to allow_value('v' * 101).for(:last_name) }
  end

  describe '#personal_id' do
    it { is_expected.to validate_presence_of(:personal_id) }
    it { is_expected.to validate_length_of(:personal_id) }
    it { is_expected.to allow_value(412_419_890).for(:personal_id) }
    it { is_expected.to allow_value('awd33435fs').for(:personal_id) }
    it { is_expected.not_to allow_value('').for(:personal_id) }
    it { is_expected.not_to allow_value(nil).for(:personal_id) }
  end

  describe '#reservation_id' do
    let(:one_more_adult) { create(:adult, reservation_id: adult.reservation_id, personal_id: adult.personal_id) }

    it { is_expected.to validate_numericality_of(:reservation_id).only_integer }
    it { is_expected.to allow_value(1000).for(:reservation_id) }
    it { is_expected.not_to allow_value(nil).for(:reservation_id) }

    describe '#reservation_id' do
      before do
        validator.id = nil
        validator.personal_id = one_more_adult.personal_id
        validator.valid?
      end

      it 'does not allow two adults with the same personal id within one reservation' do
        expect(validator.errors[:base]).to include('Personal ID must be unique within a reservation.')
      end
    end
  end

  context 'with valid attributes' do
    describe '#number_adults' do
      it 'validates valid number of adults' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#reservation_existance' do
      it 'validates reservation existance' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#direct_only' do
      it 'validates valid type of reservation' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#number_adults' do
      it 'validates invalid number of adults' do
        validator.id = nil
        validator.valid?
        expect(validator.errors[:base]).to include('Number of adults is more than indicated in the reservation')
      end

      it 'does not validate #number_adults when saving as nested attributes' do
        validator.record = nil
        validator.id = nil
        validator.valid?
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#reservation_existance' do
      before do
        validator.reservation_id = nil
        validator.valid?
      end

      it 'validates that reservation is missing' do
        expect(validator.errors[:base]).to include('Reservation with the passed id does not exist')
      end
    end

    describe '#direct_only' do
      let(:adult) { create(:invalid_adult) }

      before do
        validator.valid?
      end

      it 'validates invalid type of reservation' do
        expect(validator.errors[:base]).
          to include('The reservation affects DNR. Please change the room number or dates.')
      end
    end
  end
end
