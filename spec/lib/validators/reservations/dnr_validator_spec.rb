# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Reservations::DNRValidator, type: :model do
  subject(:validator) { described_class.new(dnr_attributes) }

  let(:room_type) { create(:hotel).room_types.create(attributes_for(:room_type)) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:dnr_attributes) { attributes_for(:dnr).merge(room_id: room.id) }

  before do
    validator.valid?
  end

  describe '#check_in_date' do
    it { is_expected.to validate_presence_of(:check_in_date) }
    it { is_expected.to allow_value(Date.current).for(:check_in_date) }
    it { is_expected.not_to allow_value(nil).for(:check_in_date) }
    it { is_expected.not_to allow_value(Date.current - 10).for(:check_in_date) }
  end

  describe '#check_out_date' do
    it { is_expected.to validate_presence_of(:check_out_date) }
    it { is_expected.to allow_value(validator.check_in_date + 2).for(:check_out_date) }
    it { is_expected.not_to allow_value(nil).for(:check_out_date) }
    it { is_expected.not_to allow_value(validator.check_in_date).for(:check_out_date) }
  end

  describe '#room_id' do
    it { is_expected.to validate_presence_of(:room_id) }
    it { is_expected.to validate_numericality_of(:room_id).only_integer }
    it { is_expected.to allow_value(375).for(:room_id) }
    it { is_expected.not_to allow_value(375.5).for(:room_id) }
    it { is_expected.not_to allow_value(nil).for(:room_id) }
  end

  describe '#decription' do
    it { is_expected.to allow_value('description').for(:description) }
    it { is_expected.to allow_value('').for(:description) }
    it { is_expected.not_to allow_value('v' * 251).for(:description) }
  end

  context 'with valid attributes' do
    describe '#validate_room_existence' do
      it 'validates room existance' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#validate_room_existence' do
      before do
        validator.room_id = 100_000_000
        validator.valid?
      end

      it 'validates that room is missing' do
        expect(validator.errors[:base]).to include('Room with the passed id does not exist')
      end
    end
  end
end
