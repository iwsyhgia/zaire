# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Reservations::ReservationValidator, type: :model do
  subject(:validator) { described_class.new(reservation_attributes) }

  let(:room_type) { create(:hotel).room_types.create(attributes_for(:room_type)) }
  let(:room) { room_type.rooms.create(attributes_for(:room)) }
  let(:reservation_attributes) do
    attributes_for(:reservation).merge(
      room_id: room.id,
      guest_attributes: attributes_for(:guest)
    )
  end

  before do
    allow(::Services::CurrentControllerService).to receive(:get) do
      instance_double('controller', current_hotel: room_type.hotel)
    end
    validator.valid?
  end

  describe '#check_in_date' do
    it { is_expected.to validate_presence_of(:check_in_date) }
    it { is_expected.to allow_value(Date.current).for(:check_in_date) }
    it { is_expected.not_to allow_value(nil).for(:check_in_date) }
    it { is_expected.not_to allow_value(Date.current - 10).for(:check_in_date) }
  end

  describe '#check_out_date' do
    it { is_expected.to validate_presence_of(:check_out_date) }
    it { is_expected.to allow_value(validator.check_in_date + 2).for(:check_out_date) }
    it { is_expected.not_to allow_value(nil).for(:check_out_date) }
    it { is_expected.not_to allow_value(validator.check_in_date).for(:check_out_date) }
  end

  describe '#number_of_adults' do
    it { is_expected.to validate_presence_of(:number_of_adults) }
    it { is_expected.to validate_numericality_of(:number_of_adults).only_integer }
    it { is_expected.to allow_value(1).for(:number_of_adults) }
    it { is_expected.not_to allow_value(1.5).for(:number_of_adults) }
    it { is_expected.not_to allow_value(0).for(:number_of_adults) }
    it { is_expected.not_to allow_value(nil).for(:number_of_adults) }
    it { is_expected.not_to allow_value(42).for(:number_of_adults) }
  end

  describe '#number_of_children' do
    it { is_expected.to validate_presence_of(:number_of_children) }
    it { is_expected.to validate_numericality_of(:number_of_children).only_integer }
    it { is_expected.to allow_value(1).for(:number_of_children) }
    it { is_expected.to allow_value(0).for(:number_of_children) }
    it { is_expected.not_to allow_value(1.5).for(:number_of_children) }
    it { is_expected.not_to allow_value(nil).for(:number_of_children) }
    it { is_expected.not_to allow_value(42).for(:number_of_children) }
  end

  describe '#room_id' do
    it { is_expected.to validate_presence_of(:room_id) }
    it { is_expected.to validate_numericality_of(:room_id).only_integer }
    it { is_expected.to allow_value(375).for(:room_id) }
    it { is_expected.not_to allow_value(375.5).for(:room_id) }
    it { is_expected.not_to allow_value(nil).for(:room_id) }
  end

  describe '#type' do
    it { is_expected.to validate_presence_of(:type) }
    it { is_expected.to allow_value('type').for(:type) }
    it { is_expected.not_to allow_value(nil).for(:type) }
  end

  describe '#rate' do
    it { is_expected.to validate_presence_of(:rate) }
    it { is_expected.to validate_inclusion_of(:rate).in_array(::Reservations::Reservation.rates.keys) }
    it { is_expected.to allow_value('refundable').for(:rate) }
    it { is_expected.not_to allow_value(nil).for(:rate) }
    it { is_expected.not_to allow_value('not rate').for(:rate) }
  end

  describe '#payment_kind' do
    context 'with refundable rate' do
      before do
        validator.rate = 'refundable'
        validator.valid?
      end

      it { is_expected.to validate_presence_of(:payment_kind) }
      it { is_expected.to allow_value('first_night').for(:payment_kind) }
      it { is_expected.not_to allow_value(nil).for(:payment_kind) }
      it { is_expected.not_to allow_value('not payment kind').for(:payment_kind) }
      it do
        expect(validator).
          to validate_inclusion_of(:payment_kind).in_array(::Reservations::Reservation.payment_kinds.keys)
      end
    end

    context 'with non_refundable rate' do
      before do
        validator.rate = 'non_refundable'
        validator.valid?
      end

      it { is_expected.to validate_absence_of(:payment_kind) }
    end
  end

  context 'with valid attributes' do
    describe '#validate_guest_attributes' do
      it 'validates valid guest attributes' do
        expect(validator.errors[:guest]).to be_empty
      end
    end

    describe '#validate_reservation_intersection' do
      it 'validates that reservation does not overlap another reservation' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#validate_room_existence' do
      it 'validates room existance' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#validate_room_type' do
      it 'validates valid room type' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#validate_room_occupancy' do
      it 'validates valid room occupancy' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#validate_guest_attributes' do
      before do
        validator.guest_attributes = nil
        validator.valid?
      end

      it 'validates invalid guest attributes' do
        expect(validator.errors[:guest]).to include("First name can't be blank")
      end
    end

    describe '#validate_reservation_intersection' do
      let(:another_reservation) { room.reservations.create(attributes_for(:reservation)) }

      before do
        another_reservation.check_in_date = reservation_attributes[:check_in_date]
        another_reservation.check_out_date = reservation_attributes[:check_out_date]
        validator.valid?
      end

      it 'validates that reservation overlaps another reservation' do
        expect(validator.errors[:base]).
          to include('Reservation cannot be created because it overlaps another reservation')
      end
    end

    describe '#validate_room_existence' do
      before do
        validator.room_id = 100_000_000
        validator.valid?
      end

      it 'validates that room is missing' do
        expect(validator.errors[:base]).to include('Room with the passed id does not exist')
      end
    end

    describe '#validate_room_type' do
      let(:room_without_room_type) { create(:room, room_type_id: nil) }

      before do
        validator.room_id = room_without_room_type.id
        validator.valid?
      end

      it 'validates valid room type' do
        expect(validator.errors[:base]).to include('Room is not associated with a room type')
      end
    end

    describe '#validate_room_occupancy' do
      before do
        validator.number_of_adults = room_type.max_occupancy + 1
        validator.valid?
      end

      it 'validates valid room occupancy' do
        expect(validator.errors[:base]).to include('Room capacity has been exceeded')
      end
    end
  end
end
