# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Reservations::CancelReservationValidator, type: :model do
  subject(:validator) { described_class.new(cancel_reservation_attributes) }

  let(:cancel_reservation_attributes) { { cancellation_fee: true } }

  describe '#cancellation_fee' do
    it { is_expected.to allow_value(true).for(:cancellation_fee) }
    it { is_expected.to allow_value(false).for(:cancellation_fee) }
    it { is_expected.to allow_value('true').for(:cancellation_fee) }
    it { is_expected.to allow_value('false').for(:cancellation_fee) }
    it { is_expected.not_to allow_value('invalid').for(:cancellation_fee) }
  end
end
