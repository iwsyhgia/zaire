require 'rails_helper'

RSpec.describe Validators::Rates::PeriodValidator, type: :model do
  NAME_LENGTH_RANGE = Validators::Rates::PeriodValidator::NAME_LENGTH_RANGE
  subject(:validator) { described_class.new(period_attributes, period, scope) }

  let(:price_kinds) { ::Rates::Period.price_kinds.keys.map(&:to_s) }
  let(:discount_kinds) { ::Rates::Period.discount_kinds.keys(&:to_s) }
  let(:hotel) { create(:hotel) }
  let(:room_type) { build_stubbed(:room_type, hotel: hotel) }
  let(:period) { build(:period, **attributes_for(:period).merge!(price_kind: :dynamic)) }
  let(:periods_room_types_attributes) do
    { periods_room_types_attributes: [attributes_for(:periods_room_type).merge!(room_type_id: room_type.id)] }
  end
  let(:period_attributes) { attributes_for(:period).merge!(periods_room_types_attributes) }

  before do
    allow(::Services::CurrentControllerService).to receive(:get) { instance_double('controller', current_hotel: hotel) }
  end

  context 'when a scope is a create' do
    let(:scope) { :create }

    before { validator.valid?(scope) }

    describe '#name' do
      it { is_expected.to validate_presence_of(:name).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:name).on(scope) }
    end

    describe '#start_date' do
      it { is_expected.to validate_presence_of(:start_date).on(scope) }
      it { is_expected.to allow_value(Date.current).for(:start_date).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:start_date).on(scope) }
      it { is_expected.not_to allow_value(Date.current - 10).for(:start_date).on(scope) }
    end

    describe '#end_date' do
      it { is_expected.to validate_presence_of(:end_date).on(scope) }
      it { is_expected.to allow_value(validator.start_date + 2).for(:end_date).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:end_date).on(scope) }
      it { is_expected.not_to allow_value(validator.start_date).for(:end_date).on(scope) }
    end

    describe '#period_kind' do
      let(:message) { I18n.t('periods.errors.period_kind_invalid') }

      it { is_expected.to validate_presence_of(:period_kind).on(scope) }
      it { is_expected.to validate_inclusion_of(:period_kind).in_array(['special']).with_message(message).on(scope) }
      it { is_expected.not_to allow_value('standard').for(:period_kind).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:period_kind).on(scope) }
    end
  end

  context 'when a scope is an update or a create' do
    let(:scope) { :update }

    before { validator.valid?(scope) }

    describe '#name' do
      let(:name_max) { NAME_LENGTH_RANGE.last }
      let(:name_min) { NAME_LENGTH_RANGE.first }

      it { is_expected.to validate_length_of(:name).is_at_most(name_max).is_at_least(name_min).on(scope) }
    end

    describe '#discount_kind' do
      let(:msg) { I18n.t('periods.errors.discount_kind_invalid', values: discount_kinds.join(', ')) }

      it { is_expected.to validate_inclusion_of(:discount_kind).in_array(discount_kinds).with_message(msg).on(scope) }
      it { is_expected.to allow_value('amount').for(:discount_kind).on(scope) }
      it { is_expected.not_to allow_value('not a kind of price').for(:discount_kind).on(scope) }
    end

    describe '#price_kind' do
      let(:message) { I18n.t('periods.errors.price_kind_invalid', values: price_kinds.join(', ')) }

      it { is_expected.to validate_inclusion_of(:price_kind).in_array(price_kinds).with_message(message).on(scope) }
      it { is_expected.to allow_value('fixed').for(:price_kind).on(scope) }
      it { is_expected.not_to allow_value('not a kind of price').for(:price_kind).on(scope) }
    end
  end

  context 'when a scope is a destroy' do
    let(:scope) { :destroy }

    before { validator.valid?(scope) }

    describe '#id' do
      it { is_expected.to validate_presence_of(:id).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:id).on(scope) }
      it { is_expected.not_to allow_value('').for(:id).on(scope) }
    end

    describe '#validate_standard_period_cannot_be_destroyed' do
      let(:message) { I18n.t('periods.errors.cannot_be_destroyed') }

      context 'with an invalid attributes' do # rubocop:disable RSpec/NestedGroups
        let(:period) { build(:period, **attributes_for(:period, period_kind: :standard)) }

        it 'validates that a standard period cannot be destroyed' do
          expect(validator.errors[:base]).to include(message)
        end
      end

      context 'with a valid attributes' do # rubocop:disable RSpec/NestedGroups
        let(:period) { build_stubbed(:period) }

        it 'validates that specific period can be destroyed' do
          expect(validator.errors[:base]).to be_empty
        end
      end
    end
  end
end
