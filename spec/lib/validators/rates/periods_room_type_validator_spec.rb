require 'rails_helper'

# rubocop:disable RSpec/NestedGroups, Metrics/LineLength
RSpec.describe Validators::Rates::PeriodsRoomTypeValidator, type: :model do
  subject(:validator) { described_class.new(attributes, periods_room_type.tap { |_r| period }, scope) }

  let(:period) { periods_room_type.period }
  let(:attributes) { attributes_for(:periods_room_type) }
  let(:periods_room_type) { build_stubbed(:periods_room_type, **attributes) }

  before { validator.valid?(scope) }

  context 'when a scope is a create' do
    let(:scope) { :create }

    describe '#room_type_id' do
      it { is_expected.to validate_presence_of(:room_type_id).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:room_type_id).on(scope) }
    end

    describe '#discount_value' do
      it { is_expected.to validate_presence_of(:discount_value).on(scope) }
      it { is_expected.to allow_value(1).for(:discount_value).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:discount_value).on(scope) }
      it { is_expected.not_to allow_value(-1).for(:discount_value).on(scope) }
    end

    describe '#rates' do
      it { is_expected.to validate_presence_of(:rates).on(scope) }
      it { is_expected.not_to allow_value(nil).for(:rates).on(scope) }
    end
  end

  context 'when a scope is an update or a create' do
    let(:scope) { %i[update create] }

    context 'when a discount kind is an amount' do
      let(:periods_room_type) { build_stubbed(:periods_room_type, **attributes).tap { |r| r.period.discount_kind = 'amount' } }

      describe '#discount_value' do
        it { is_expected.to validate_numericality_of(:discount_value).is_greater_than_or_equal_to(0).is_less_than(1_000_000).on(scope) }
      end
    end

    context 'when a discount kind is an percent' do
      let(:periods_room_type) { build_stubbed(:periods_room_type, **attributes).tap { |r| r.period.discount_kind = 'percent' } }

      describe '#discount_value' do
        it { is_expected.to validate_numericality_of(:discount_value).is_greater_than_or_equal_to(0).is_less_than(100).on(scope) }
      end
    end

    describe '#validate_day_names_correspond_dates' do
      let(:day_names) { %i[wednesday thursday] }
      let(:period) { periods_room_type.period.assign_attributes(start_date: '10-04-2019', end_date: '12-04-2019') }

      context 'with valid attributes' do
        let(:attributes) { attributes_for(:periods_room_type, day_names: day_names, room_type_id: create(:room_type).id) }

        it do
          expect(validator.errors[:base]).to be_empty
        end
      end

      context 'with invalid attributes' do
        let(:attributes) do
          attributes_for(:periods_room_type, day_names: %i[saturday sunday], room_type_id: create(:room_type).id)
        end
        let(:error_message) { I18n.t('periods.errors.day_names_invalid', values: day_names.join(', ')) }

        it { expect(validator.errors[:base]).to include(error_message) }
      end
    end

    describe '#validate_discount_exceeded' do
      let(:attributes) do
        attributes_for(:periods_room_type, discount_value: discount_value, room_type_id: create(:room_type).id)
      end

      context 'with valid attributes' do
        let(:discount_value) { 42 }

        it { expect(validator.errors[:base]).to be_empty }
      end

      context 'with invalid attributes' do
        let(:discount_value) { 1000 }
        let(:error_message) { I18n.t('periods.errors.discount_exceeded') }

        it { expect(validator.errors[:base]).to include(error_message) }
      end
    end

    describe '#validate_rates' do
      context 'with valid attributes' do
        it { expect(validator.errors[:base]).to be_empty }
      end
    end
  end
end
# rubocop:enable RSpec/NestedGroups, Metrics/LineLength
