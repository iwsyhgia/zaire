require 'rails_helper'

RSpec.describe Validators::Rates::RateValidator, type: :model do
  RATE_KINDS = ::Rooms::Rate.kinds.keys

  subject(:validator) { described_class.new(attributes, nil) }

  let(:kind_message) { I18n.t('rates.errors.kind_invalid', values: RATE_KINDS.join(', ')) }
  let(:attributes) { attributes_for(:rate) }

  context 'with a fixed kind' do
    let(:attributes) { attributes_for(:rate, kind: 'fixed', upper_bound: nil) }

    describe '#lower_bound' do
      it { is_expected.to validate_presence_of(:lower_bound) }
      it { is_expected.to validate_numericality_of(:lower_bound).is_less_than(100_000).is_greater_than(0) }
    end

    describe '#upper_bound' do
      it { is_expected.to validate_absence_of(:upper_bound) }
    end
  end

  context 'with a dynamic rate' do
    let(:attributes) { attributes_for(:rate, kind: 'dynamic') }

    describe '#lower_bound' do
      it { is_expected.to validate_presence_of(:lower_bound) }
      it { is_expected.to validate_numericality_of(:lower_bound).is_less_than(100_000).is_greater_than(0) }
    end

    describe '#upper_bound' do
      it { is_expected.to validate_presence_of(:upper_bound) }
      it { is_expected.to validate_numericality_of(:upper_bound).is_greater_than_or_equal_to(attributes[:lower_bound]) }
    end
  end
end
