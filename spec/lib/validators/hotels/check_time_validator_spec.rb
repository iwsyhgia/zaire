# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Hotels::CheckTimeValidator, type: :model do
  subject(:validator) { described_class.new(check_time_attributes) }

  let(:check_time_attributes) do
    {
      check_in_time_attributes: attributes_for(:new_check_in_time),
      check_out_time_attributes: attributes_for(:new_check_out_time)
    }
  end

  let(:invalid_check_time_attributes) do
    {
      check_in_time_attributes: attributes_for(:invalid_check_in_time),
      check_out_time_attributes: attributes_for(:invalid_check_out_time)
    }
  end

  context 'with valid attributes' do
    it 'valid with valid attributes' do
      validator.valid?

      expect(validator).to be_valid
    end
  end

  context 'with invalid attributes' do
    let(:check_time_attributes) { invalid_check_time_attributes }

    it 'fails if check out time came after check in time' do
      validator.valid?

      expect(validator).not_to be_valid
    end

    it 'fails if check_in time fixed and check_out_time fixed and check_in earlier then check_out' do
      validator.check_in_time_attributes[:fixed] = true
      validator.check_out_time_attributes[:fixed] = true
      validator.check_in_time_attributes[:start_time] = '09:00'
      validator.check_out_time_attributes[:start_time] = '10:00'

      validator.valid?

      expect(validator.errors[:base]).to include(I18n.t('hotels.errors.check_out_time_earlier'))
    end

    it 'fails if check_in time fixed and check_out_time period and check_in earlier then check_out' do
      validator.check_in_time_attributes[:fixed] = true
      validator.check_out_time_attributes[:fixed] = false
      validator.check_in_time_attributes[:start_time] = '09:00'
      validator.check_out_time_attributes[:end_time] = '10:00'

      validator.valid?

      expect(validator.errors[:base]).to include(I18n.t('hotels.errors.check_out_time_earlier'))
    end

    it 'fails if time period and check_in earlier then check_out' do
      validator.check_in_time_attributes[:fixed] = false
      validator.check_out_time_attributes[:fixed] = false
      validator.check_in_time_attributes[:start_time] = '09:00'
      validator.check_out_time_attributes[:end_time] = '10:00'

      validator.valid?

      expect(validator.errors[:base]).to include(I18n.t('hotels.errors.check_out_time_earlier'))
    end
  end
end
