# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Hotels::HotelValidator, type: :model do
  subject(:validator) { described_class.new(hotel_attributes) }

  let(:hotel_attributes) { attributes_for(:hotel) }

  describe '#name' do
    it { is_expected.to validate_length_of(:name) }
    it { is_expected.to allow_value('').for(:name) }
    it { is_expected.not_to allow_value('v' * 256).for(:name) }
  end

  describe '#name_ar' do
    it { is_expected.to validate_length_of(:name_ar) }
    it { is_expected.to allow_value('').for(:name_ar) }
    it { is_expected.not_to allow_value('v' * 256).for(:name_ar) }
  end
end
