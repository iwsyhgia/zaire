# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Hotels::OfflineModeValidator, type: :model do
  subject(:validator) { described_class.new(offline_mode_attributes, offline_mode) }

  let(:offline_mode) { create(:hotel).offline_mode }
  let(:offline_mode_attributes) { attributes_for(:hotel_offline_mode) }

  describe '#days_before_today' do
    it { is_expected.to validate_presence_of(:days_before_today) }
    it { is_expected.not_to allow_value(nil).for(:days_before_today) }
    it { is_expected.to validate_numericality_of(:days_before_today).only_integer }
    it { is_expected.to validate_numericality_of(:days_before_today).is_greater_than(0) }
    it { is_expected.to validate_numericality_of(:days_before_today).is_less_than_or_equal_to(15) }
    it { is_expected.to allow_value(7).for(:days_before_today) }
    it { is_expected.not_to allow_value(7.5).for(:days_before_today) }
    it { is_expected.not_to allow_value(0).for(:days_before_today) }
    it { is_expected.not_to allow_value(16).for(:days_before_today) }
  end

  describe '#days_after_today' do
    it { is_expected.to validate_presence_of(:days_after_today) }
    it { is_expected.not_to allow_value(nil).for(:days_after_today) }
    it { is_expected.to validate_numericality_of(:days_after_today).only_integer }
    it { is_expected.to validate_numericality_of(:days_after_today).is_greater_than(0) }
    it { is_expected.to validate_numericality_of(:days_after_today).is_less_than_or_equal_to(15) }
    it { is_expected.to allow_value(7).for(:days_after_today) }
    it { is_expected.not_to allow_value(7.5).for(:days_after_today) }
    it { is_expected.not_to allow_value(0).for(:days_after_today) }
    it { is_expected.not_to allow_value(16).for(:days_after_today) }
  end
end
