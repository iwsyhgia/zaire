# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Hotels::AddressValidator, type: :model do
  subject(:validator) { described_class.new(address_attributes) }

  let(:address_attributes) { attributes_for(:hotel_address) }

  describe '#country' do
    it { is_expected.to validate_presence_of(:country) }
    it { is_expected.not_to allow_value(nil).for(:country) }
    it { is_expected.to allow_value('Country').for(:country) }
    it { is_expected.not_to allow_value('').for(:country) }
    it { is_expected.not_to allow_value('v' * 256).for(:country) }
  end

  describe '#city' do
    it { is_expected.to validate_presence_of(:city) }
    it { is_expected.not_to allow_value(nil).for(:city) }
    it { is_expected.to allow_value('City').for(:city) }
    it { is_expected.not_to allow_value('').for(:city) }
    it { is_expected.not_to allow_value('v' * 256).for(:city) }
  end

  describe '#state' do
    it { is_expected.to allow_value(nil).for(:state) }
    it { is_expected.to allow_value('State').for(:state) }
    it { is_expected.to allow_value('').for(:state) }
    it { is_expected.not_to allow_value('v' * 256).for(:state) }
  end

  describe '#address_1' do
    it { is_expected.to validate_presence_of(:address_1) }
    it { is_expected.not_to allow_value(nil).for(:address_1) }
    it { is_expected.to allow_value('Address 1').for(:address_1) }
    it { is_expected.not_to allow_value('').for(:address_1) }
    it { is_expected.not_to allow_value('v' * 256).for(:address_1) }
  end

  describe '#address_2' do
    it { is_expected.to allow_value(nil).for(:address_2) }
    it { is_expected.to allow_value('Address 2').for(:address_2) }
    it { is_expected.to allow_value('').for(:address_2) }
    it { is_expected.not_to allow_value('v' * 256).for(:address_2) }
  end
end
