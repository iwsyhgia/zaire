# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::Hotels::CheckInOutTimeValidator, type: :model do
  let(:check_in_time) { create(:hotel).check_in_time }
  let(:check_in_time_attributes) { attributes_for(:check_in_time) }

  let(:check_out_time) { create(:hotel).check_out_time }
  let(:check_out_time_attributes) { attributes_for(:check_out_time) }

  describe 'check_in' do
    let(:validator) { described_class.new(check_in_time_attributes) }

    context 'with valid attributes' do
      it 'valid with valid attributes' do
        validator.valid?

        expect(validator).to be_valid
      end
    end

    context 'with invalid attributes' do
      let(:check_in_time_attributes) { attributes_for(:invalid_check_in_time) }

      it 'validates start_time goes earlier than end_time' do
        validator.valid?

        expect(validator.errors[:base]).to include(I18n.t('hotels.errors.wrong_time_period'))
      end

      it '#start_time validates invalid format' do
        validator.start_time = '9:00'
        validator.valid?

        expect(validator.errors[:start_time]).to include(I18n.t('hotels.errors.invalid_format'))
      end

      it '#end_time validates invalid format' do
        validator.end_time = '9:00'
        validator.valid?

        expect(validator.errors[:end_time]).to include(I18n.t('hotels.errors.invalid_format'))
      end
    end
  end

  describe 'check_out' do
    let(:validator) { described_class.new(check_out_time_attributes) }

    context 'with valid attributes' do
      it 'valid with valid attributes' do
        validator.valid?

        expect(validator).to be_valid
      end
    end

    context 'with invalid attributes' do
      let(:check_out_time_attributes) { attributes_for(:invalid_check_out_time) }

      it 'validates start_time goes earlier than end_time' do
        validator.valid?

        expect(validator.errors[:base]).to include(I18n.t('hotels.errors.wrong_time_period'))
      end

      it '#start_time validates invalid format' do
        validator.start_time = '9:00'
        validator.valid?

        expect(validator.errors[:start_time]).to include(I18n.t('hotels.errors.invalid_format'))
      end

      it '#end_time validates invalid format' do
        validator.end_time = '9:00'
        validator.valid?

        expect(validator.errors[:end_time]).to include(I18n.t('hotels.errors.invalid_format'))
      end
    end
  end
end
