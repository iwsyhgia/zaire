# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Validators::ImageValidator, type: :model do
  subject(:validator) { described_class.new(image_attributes) }

  let(:image_attributes) { attributes_for(:image, :with_base64) }

  before do
    validator.valid?
  end

  describe '#base64' do
    it { is_expected.to validate_presence_of(:base64) }
    it { is_expected.not_to allow_value(nil).for(:base64) }
    it { is_expected.to validate_length_of(:base64).is_at_least(100) }
    it { is_expected.to allow_value(image_attributes[:base64]).for(:base64) }
    it { is_expected.not_to allow_value('not a base64').for(:base64) }
  end

  context 'with valid attributes' do
    describe '#validate_image_resolution' do
      it 'validates valid image resolution' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#validate_image_format' do
      it 'validates valid image format' do
        expect(validator.errors[:base]).to be_empty
      end
    end

    describe '#validate_image_size' do
      it 'validates valid image size' do
        expect(validator.errors[:base]).to be_empty
      end
    end
  end

  context 'with invalid attributes' do
    describe '#validate_image_resolution' do
      let(:image_attributes) { attributes_for(:invalid_image_resolution, format: 'jpg') }

      before do
        validator.tag = 'not a logo'
        validator.valid?
      end

      it 'validates invalid image resolution' do
        expect(validator.errors[:base]).to include('Image must be more than 300x300 pixels')
      end
    end

    describe '#validate_image_format' do
      let(:image_attributes) { attributes_for(:invalid_image_format, format: 'bmp') }

      before do
        validator.valid?
      end

      it 'validates invalid image format' do
        expect(validator.errors[:base]).to include('Image format should be PNG, JPG or SVG')
      end
    end

    describe '#validate_image_size' do
      let(:image_attributes) { attributes_for(:invalid_image_size, format: 'png') }

      before do
        validator.valid?
      end

      it 'validates invalid image size' do
        expect(validator.errors[:base]).to include('Image size must be greater than 1 KB and less than 10 MB')
      end
    end
  end
end
