# frozen_string_literal: true

FactoryBot.define do
  factory :payment, class: '::Invoices::Payment' do
    amount { Faker::Number.decimal(5, 2).to_d }
    status { :pending }
    creation_way { %w[manual automated].sample }
    payment_method { %w[card cash].sample }

    association :user, factory: :user, strategy: :build_stubbed
    association :invoice, factory: :invoice, strategy: :build

    factory :invalid_payment do
      amount { nil }
    end
  end
end
