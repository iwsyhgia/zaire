# frozen_string_literal: true

FactoryBot.define do
  factory :invoice, class: '::Invoices::Invoice' do
    trait :with_default_totals do
      payments_total { 0 }
      refunds_total { 0 }
      charges_total { 0 }
      vat_tax { 5 }
      municipal_tax { 5 }
    end

    factory :invoice_with_documents do
      transient do
        refund_count { 3 }
        payment_count { 3 }
        discount_count { 3 }
      end

      after(:create) do |invoice, evaluator|
        create_list(:payment, evaluator.payment_count, invoice: invoice)
        create_list(:refund, evaluator.refund_count, invoice: invoice)
        create_list(:discount, evaluator.discount_count, invoice: invoice)
      end
    end
  end
end
