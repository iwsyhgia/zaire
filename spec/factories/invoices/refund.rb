# frozen_string_literal: true

FactoryBot.define do
  factory :refund, class: '::Invoices::Refund' do
    amount { Faker::Number.decimal(1, 2).to_d }
    description { Faker::Lorem.sentence(3) }
    status { :pending }
    creation_way { %w[manual automated].sample }

    association :user, factory: :user, strategy: :build_stubbed
    association :payment, factory: :payment, strategy: :build_stubbed

    factory :invalid_refund do
      amount { nil }
    end

    factory :refund_with_payment do
      transient do
        payment_amount { 42 }
      end

      before(:create) do |refund, evaluator|
        create(:payment, amount: evaluator.payment_amount, invoice: refund.invoice, refunds: [refund])
      end
    end
  end
end
