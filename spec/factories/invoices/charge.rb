# frozen_string_literal: true

FactoryBot.define do
  factory :charge, class: '::Invoices::Charge' do
    amount { Faker::Number.decimal(3, 2).to_d }
    description { Faker::Lorem.sentence(3) }
    kind { %w[stay_fee sellable_items].sample }

    association :user, factory: :user, strategy: :build_stubbed
  end
end
