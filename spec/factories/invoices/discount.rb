# frozen_string_literal: true

FactoryBot.define do
  factory :discount, class: '::Invoices::Discount' do
    value { Faker::Number.decimal(0, 2).to_d }
    kind { %i[percent amount].first }
    creation_way { %w[manual automated].sample }
    description { Faker::Lorem.sentence(3) }

    association :user, factory: :user, strategy: :build_stubbed

    trait :with_percent do
      kind { :percent }
      value { 42 }
    end

    factory :invalid_discount do
      value { -1 }
    end
  end
end
