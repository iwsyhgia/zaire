# frozen_string_literal: true

FactoryBot.define do
  factory :audit, class: 'Audits::Audit' do
    hotel
    status { :scheduled }
    time_frame_begin { DateTime.now.beginning_of_day - 1.day }
    time_frame_end { DateTime.now.beginning_of_day + 3.hours }

    trait :confirmed do
      status { :confirmed }
      started_at { 5.minutes.ago }

      after(:create) do |audit|
        create_list(:audit_confirmation, 1, audit: audit, user: audit.hotel.user)
      end
    end

    trait :waiting_for_confirmation do
      status { :waiting_for_confirmation }

      after(:create) do |audit|
        create_list(:audit_confirmation, 1, audit: audit, user: audit.hotel.user)
      end
    end

    trait :completed do
      status { :completed }
      started_at { 5.minutes.ago }
    end
  end
end
