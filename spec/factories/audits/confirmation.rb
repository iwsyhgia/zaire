# frozen_string_literal: true

FactoryBot.define do
  factory :audit_confirmation, class: 'Audits::Confirmation' do
    user
  end
end
