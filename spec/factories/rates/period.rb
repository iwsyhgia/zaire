# frozen_string_literal: true

FactoryBot.define do
  factory :period, class: '::Rates::Period' do
    name { Faker::Name.unique.name }
    period_kind { 'special' }
    price_kind { 'dynamic' }
    discount_kind { 'amount' }

    start_date { Date.current + offset }
    end_date { Date.current + offset + duration }

    association :hotel, strategy: :build

    trait(:standard) do
      period_kind { :standard }
      start_date { nil }
      end_date { nil }
    end
    trait(:special) { period_kind { :special } }
    trait(:fixed) { price_kind { :fixed } }
    trait(:dynamic) { price_kind { :dynamic } }
    trait(:amount) { discount_kind { :amount } }
    trait(:percent) { price_kind { :percent } }

    trait(:with_nested_attributes) do
      transient { nested_attributes { {} } }

      periods_room_types_attributes do
        room_types.map do |room_type|
          attributes_for(:periods_room_type, room_type_id: room_type.id, **nested_attributes)
        end
      end
    end

    transient { room_types { [] } }
    transient { offset { 0.days } }
    transient { duration { 10.days } }

    after(:create) do |period, evaluator|
      evaluator.room_types.each do |room_type|
        create(:periods_room_type, period: period, room_type: room_type)
      end
    end
  end
end
