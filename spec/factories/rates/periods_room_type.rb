# frozen_string_literal: true

FactoryBot.define do
  factory :periods_room_type, class: '::Rates::PeriodsRoomType' do
    discount_value { 42.to_d }

    rates do
      day_names.each_with_object({}) { |day_name, memo| memo[day_name] = rate }
    end

    association :room_type, strategy: :build_stubbed
    association :period, strategy: :build_stubbed

    trait :without_rates do
      rates { {} }
    end

    transient do
      rate { { lower_bound: '100', upper_bound: '200' } }
    end

    transient do
      day_names { DateTime::DAYS_INTO_WEEK.keys }
    end
  end
end
