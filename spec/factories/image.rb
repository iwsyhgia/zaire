# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
FactoryBot.define do
  factory :image do
    tag { 'logo' }
    file do
      Rack::Test::UploadedFile.new(File.open(File.join(Rails.root,
        '/spec/images/valid_test_image.jpg')), 'image/jpg')
    end

    trait :with_base64 do
      base64 do
        'data:image/jpg;base64,' +
          Base64.encode64(File.open(File.join(Rails.root, '/spec/images/valid_test_image.jpg')).read)
      end
    end

    trait :with_file_and_base64 do
      transient do
        format { nil }
      end

      file do
        Rack::Test::UploadedFile.new(File.open(File.join(Rails.root,
          '/spec/images/invalid_test_image.' + format)), 'image/' + format)
      end

      base64 do
        'data:image/' + format + ';base64,' +
          Base64.encode64(File.open(File.join(Rails.root, '/spec/images/invalid_test_image.' + format)).read)
      end
    end

    factory :new_image do
      file do
        Rack::Test::UploadedFile.new(File.open(File.join(Rails.root,
          '/spec/images/valid_test_image.png')), 'image/png')
      end

      base64 do
        'data:image/png;base64,' +
          Base64.encode64(File.open(File.join(Rails.root, '/spec/images/valid_test_image.png')).read)
      end
    end

    factory :invalid_image_size,       traits: [:with_file_and_base64]
    factory :invalid_image_format,     traits: [:with_file_and_base64]
    factory :invalid_image_resolution, traits: [:with_file_and_base64]
  end
end
# rubocop:enable Metrics/BlockLength
