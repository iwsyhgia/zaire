# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations_adults
#
#  id             :bigint(8)        not null, primary key
#  deleted_at     :datetime
#  first_name     :string
#  last_name      :string
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  personal_id    :string
#  reservation_id :bigint(8)
#

FactoryBot.define do
  factory :adult, class: '::Reservations::Adult' do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    title { 'Mr.' }
    personal_id { Faker::Number.number(10) }

    association :reservation, factory: :reservation, strategy: :build

    factory :invalid_adult do
      association :reservation, factory: :dnr, strategy: :build
    end

    factory :new_adult do
      first_name { Faker::Name.first_name }
      last_name { Faker::Name.last_name }
    end
  end
end
