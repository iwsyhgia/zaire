# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id                 :bigint(8)        not null, primary key
#  guest_id           :bigint(8)
#  room_id            :bigint(8)
#  check_in_date      :date
#  check_out_date     :date
#  status             :integer          default("unconfirmed")
#  number_of_adults   :integer
#  number_of_children :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  type               :string
#  description        :string
#  deleted_at         :datetime
#  vat_percent        :decimal(5, 2)    default(0.0)
#

FactoryBot.define do
  factory :dnr, class: '::Reservations::DNR' do
    type { 'Reservations::DNR' }
    description { 'Description' }

    transient do
      date_range { (10..11) }
    end

    check_in_date  { Date.current + date_range.first.days }
    check_out_date { Date.current + date_range.last.days }

    association :room, factory: :room, strategy: :create
  end
end
