# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id                 :bigint(8)        not null, primary key
#  guest_id           :bigint(8)
#  room_id            :bigint(8)
#  check_in_date      :date
#  check_out_date     :date
#  status             :integer          default("unconfirmed")
#  number_of_adults   :integer
#  number_of_children :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  type               :string
#  description        :string
#  deleted_at         :datetime
#  vat_percent        :decimal(5, 2)    default(0.0)
#

FactoryBot.define do
  factory :reservation, class: 'Reservations::Reservation' do
    status { 'unconfirmed' }
    number_of_adults { 1 }
    number_of_children { 0 }
    rate { 'refundable' }
    type { 'Reservations::DirectReservation' }
    payment_kind { 'first_night' }

    transient do
      date_range { (0..25) }
    end

    check_in_date  { Date.current + date_range.first.days }
    check_out_date { Date.current + date_range.last.days }

    association :guest, factory: :guest, strategy: :create
    association :room, factory: :room, strategy: :create
    association :invoice, factory: :invoice, strategy: :create

    factory :invalid_reservation do
      status { 'unconfirmed' }
      check_in_date { nil }
      check_out_date { nil }
    end

    factory :new_reservation do
      status { 'confirmed' }
      check_out_date { Date.current + 3.days }
    end

    trait(:two_days)       { check_out_date { Date.current + 2.days } }
    trait(:refundable)     { rate { 'refundable' } }
    trait(:non_refundable) { rate { 'non_refundable' } }
    trait(:first_night)    { payment_kind { 'first_night' } }
    trait(:full_stay)      { payment_kind { 'full_stay' } }
    trait(:confirmed)      { status { 'confirmed' } }
    trait(:unconfirmed)    { status { 'unconfirmed' } }
    trait(:checked_in)     { status { 'checked_in' } }
    trait(:checked_out)    { status { 'checked_out' } }
    trait(:no_show)        { status { 'no_show' } }
    trait(:cancelled)      { status { 'cancelled' } }
  end
end
