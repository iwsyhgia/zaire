# frozen_string_literal: true

FactoryBot.define do
  factory :notification, class: 'Notifications::Notification' do
    association :recipient, factory: :user, strategy: :build
    message_params { { message: Faker::Lorem.sentence } }
    kind { 'audit_initiated' }
  end
end
