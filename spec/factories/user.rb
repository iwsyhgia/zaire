# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "email#{n}#{rand(1..9999)}@example.com" }
    password { 'Qw123123' }
    password_confirmation { 'Qw123123' }
    confirmed_at { DateTime.now }

    trait :with_hotel do
      association :hotel, factory: :hotel, strategy: :build
    end
  end
end
