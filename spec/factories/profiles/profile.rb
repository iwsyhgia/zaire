# frozen_string_literal: true

FactoryBot.define do
  factory :profile, class: '::Profiles::Profile' do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone_number { '1231233' }
    notifications { 'off' }

    association :user, factory: :user, strategy: :create
  end
end
