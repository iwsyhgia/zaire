# frozen_string_literal: true

FactoryBot.define do
  factory :configuration, class: 'Hotels::Configuration' do
    night_audit_confirmation_required { true }
    night_audit_time { '03:00' }
    time_zone { 'UTC' }
  end
end
