# frozen_string_literal: true

FactoryBot.define do
  factory :hotel_offline_mode, class: 'Hotels::OfflineMode' do
    active { true }
    days_before_today { 7 }
    days_after_today { 7 }

    factory :invalid_hotel_offline_mode do
      active { true }
      days_before_today { 100 }
      days_after_today { 100 }
    end

    factory :new_hotel_offline_mode do
      active { true }
      days_before_today { 9 }
      days_after_today { 9 }
    end
  end
end
