# frozen_string_literal: true

FactoryBot.define do
  factory :check_time, aliases: [:check_in_time], class: 'Hotels::CheckTime' do
    fixed { false }
    check_type { 'check_in' }
    start_time { '14:00' }
    end_time { '15:00' }

    trait :check_out do
      check_type { 'check_out' }
      start_time { '11:00' }
      end_time { '12:00' }
    end

    trait :start_time_after_end_time do
      start_time { '16:00' }
      end_time { '10:00' }
    end

    factory :new_check_in_time
    factory :check_out_time,         traits: %i[check_out]
    factory :new_check_out_time,     traits: %i[check_out]
    factory :invalid_check_out_time, traits: %i[check_out start_time_after_end_time]
    factory :invalid_check_in_time,  traits: %i[start_time_after_end_time]
  end
end
