# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels
#
#  id         :bigint(8)        not null, primary key
#  deleted_at :datetime
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#
# Indexes
#
#  index_hotels_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#  deleted_at :datetime
#  logo       :string
#
# rubocop:disable Metrics/BlockLength
FactoryBot.define do
  factory :hotel, class: 'Hotels::Hotel' do
    name { Faker::Name.name }
    name_ar { Faker::Name.name }

    user
    configuration
    association :offline_mode, factory: :hotel_offline_mode, strategy: :build
    association :check_in_time, factory: :check_in_time, strategy: :build
    association :check_out_time, factory: :check_out_time, strategy: :build
    association :address, factory: :hotel_address, strategy: :build
    association :logo, factory: :image, strategy: :build

    transient do
      amenities_count { 5 }
    end

    after(:create) do |hotel|
      create_list(:audit, 1, hotel: hotel)
    end

    trait :with_standard_period do
      after(:create) do |hotel|
        create(:period, :standard, start_date: nil, end_date: nil, hotel: hotel)
      end
    end

    trait :with_amenities do
      after(:create) do |hotel, evaluator|
        create_list(:amenity, evaluator.amenities_count, hotel: hotel)
      end
    end

    factory :hotel_with_room_type do
      transient do
        room_type_count { 1 }
      end

      after(:create) do |hotel, evaluator|
        create_list(:room_type, evaluator.room_type_count, hotel: hotel)
      end
    end

    factory :hotel_with_reservation do
      transient do
        reservation_count { 1 }
        room_type_count { 1 }
        room_count { 1 }
      end

      after(:create) do |hotel, evaluator|
        room_types = create_list(:room_type, evaluator.room_type_count, hotel: hotel)
        rooms = create_list(:room, evaluator.room_count, room_type: room_types.sample)
        create_list(:reservation, evaluator.reservation_count, room: rooms.sample)
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
