# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels_addresses
#
#  id         :bigint(8)        not null, primary key
#  address_1  :string
#  address_2  :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#  state      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  hotel_id   :bigint(8)
#

FactoryBot.define do
  factory :hotel_address, class: 'Hotels::Address' do
    country { Faker::Address.country }
    city { Faker::Address.city }
    state { Faker::Address.state }
    address_1 { Faker::Address.street_address }
    address_2 { Faker::Address.secondary_address }

    factory :invalid_hotel_address do
      country { nil }
      city { nil }
    end

    factory :new_hotel_address do
      country { Faker::Address.country }
      city { Faker::Address.city }
      state { Faker::Address.state }
      address_1 { Faker::Address.street_address }
      address_2 { Faker::Address.secondary_address }

      sequence(:email) { |n| "hotel#{n}@example.com" }
      phone_number { '42' }
      fax { 'fax' }
    end
  end
end
