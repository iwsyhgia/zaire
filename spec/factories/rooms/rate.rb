# frozen_string_literal: true

FactoryBot.define do
  factory :rate, class: '::Rooms::Rate' do
    kind { 'dynamic' }
    lower_bound { 100.to_d }
    upper_bound { 200.to_d }
  end
end
