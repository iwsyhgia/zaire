# frozen_string_literal: true

# == Schema Information
#
# Table name: rooms
#
#  id           :bigint(8)        not null, primary key
#  number       :integer          not null
#  room_type_id :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

FactoryBot.define do
  factory :room, class: '::Rooms::Room' do
    number { Faker::Number.unique.number(4).to_i }

    association :room_type, factory: :room_type
  end

  factory :invalid_room, class: '::Rooms::Room' do
    number { nil }
  end

  factory :new_room, class: '::Rooms::Room' do
    number { 123 }
  end
end
