# frozen_string_literal: true

# == Schema Information
#
# Table name: room_types
#
#  id            :bigint(8)        not null, primary key
#  name          :string           not null
#  size          :string
#  unit          :string
#  max_occupancy :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  hotel_id      :bigint(8)
#  quantity      :integer
#  deleted_at    :datetime
#

FactoryBot.define do
  factory :room_type, class: '::Rooms::RoomType' do
    name { Faker::Name.unique.name }
    name_ar { Faker::Name.unique.name }
    max_occupancy { rand(1..30) }
    quantity { Faker::Number.number(2).to_i }

    association :hotel, factory: :hotel
    association :rate, strategy: :build

    transient do
      rooms_count { 1 }
    end

    after(:create) do |room_type, evaluator|
      create_list(:room, evaluator.rooms_count, room_type: room_type)
    end
  end

  factory :invalid_room_type, class: '::Rooms::RoomType' do
    name { nil }
    max_occupancy { nil }
    quantity { nil }
  end

  factory :new_room_type, class: '::Rooms::RoomType' do
    name { 'New room type' }
    name_ar { 'New room type ar' }
    max_occupancy { Faker::Number.number(2).to_i }
    quantity { Faker::Number.number(2).to_i }
  end
end
