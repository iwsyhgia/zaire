# frozen_string_literal: true

# == Schema Information
#
# Table name: amenities
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  hotel_id   :bigint(8)
#

FactoryBot.define do
  factory :amenity, class: '::Rooms::Amenity' do
    name { Faker::Name.unique.name }
    name_ar { Faker::Name.unique.name }
    association :hotel, factory: :hotel
  end

  factory :invalid_amenity, class: '::Rooms::Amenity' do
    name { nil }
  end

  factory :new_amenity, class: '::Rooms::Amenity' do
    name { 'New amenity' }
    name_ar { 'New amenity ar' }
  end
end
