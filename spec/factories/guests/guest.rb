# frozen_string_literal: true

# == Schema Information
#
# Table name: guests
#
#  id           :bigint(8)        not null, primary key
#  title        :string
#  first_name   :string
#  last_name    :string
#  email        :string
#  phone_code   :string
#  phone_number :string
#  country      :string
#  city         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

FactoryBot.define do
  factory :guest, class: 'Guests::Guest' do
    title { 'Mr.' }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    birth_date { Date.current - 25.years }
    sequence(:email) { |n| "email#{n}@example.com" }
    country { Faker::Address.country }
    city { Faker::Address.city }

    trait :with_not_unique_email do
      email { 'notunique@email.com' }
      phone_number { nil }
    end

    trait :with_not_unique_phone do
      email { nil }
      phone_number { '5555555555' }
      phone_code { '1' }
    end
  end

  factory :guest_for_search, class: 'Guests::Guest' do
    first_name { 'First name' }
    last_name { 'Last name' }
    email { 'guest_search@example.com' }
    country { 'Country' }
    city { 'City' }
    phone_code { '+123' }
    phone_number { '123456789' }
  end
end
