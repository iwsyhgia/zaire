# frozen_string_literal: true

class GuestSearch
  attr_accessor :m, :email_cont, :phone_number_cont, :g
end

FactoryBot.define do
  factory :guest_search do
    m { 'or' }
    email_cont { 'guest_search@example.com' }
    phone_number_cont { '123456789' }
    g { [first_name_cont: 'First name', last_name_cont: 'Last name'] }
  end
end
