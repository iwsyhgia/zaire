class InitSeedMigration < SeedMigration::Migration
  def up
    # Update amenities
    amenities = YAML.load_file Rails.root.join('db', 'data', 'amenities.yml')
    amenities['amenities_categories'].each do |category, items|
      category['amenities'].each do |item|
        amenity = ::Rooms::Amenity.find_by(name: item['en'])
        if amenity.present?
          amenity.update(name_ar: item['ar'], category: category['name'])
        else
          ::Rooms::Amenity.create!(name: item['en'], name_ar: item['ar'], category: category['name'])
        end
      end
    end

    # Initiate night audits for old Hotels
    Hotels::Hotel.left_outer_joins(:audits).where('audits.id IS NULL').find_each do |hotel|
      ::Services::Audits::ScheduleNext.call(hotel: hotel)
    end
    Audits::Confirmation.destroy_all

    # Attach guests to hotels
    Guests::Guest.joins(:reservations).find_each do |guest|
      guest.update(hotel_id: guest.reservations.last.hotel&.id)
    end

    # Init standard periods
    Hotels::Hotel.find_each do |hotel|
      hotel.periods.destroy_all
      ::Rooms::Rate.update(active: true)
      ::Services::Rates::InitStandardPeriod.call(hotel: hotel)
    end
  end

  def down
    Rates::Periods.periods.destroy_all
  end
end
