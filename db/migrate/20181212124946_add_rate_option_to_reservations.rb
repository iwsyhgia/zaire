class AddRateOptionToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :rate, :integer, default: 0, null: false
    add_column :reservations, :payment_kind, :integer
  end
end
