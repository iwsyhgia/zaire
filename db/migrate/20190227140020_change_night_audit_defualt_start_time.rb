class ChangeNightAuditDefualtStartTime < ActiveRecord::Migration[5.2]
  def change
    change_column :hotels_configurations, :night_audit_time, :string, default: '05:00'
  end
end
