class UpgradeInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices_refunds, :payment_method, :integer
    add_column :invoices, :discounts_total, :decimal, precision: 10, scale: 2, default: 0
    rename_column :invoices, :tax_total, :charges_with_taxes_total
  end
end
