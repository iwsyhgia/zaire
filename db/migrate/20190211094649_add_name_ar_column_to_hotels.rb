class AddNameArColumnToHotels < ActiveRecord::Migration[5.2]
  def change
    add_column :hotels, :name_ar, :string, null: true
  end
end
