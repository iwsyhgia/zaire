class AddRatesForOldRoomTypes < ActiveRecord::Migration[5.2]
  def change
    execute <<-SQL
      -- Create rates for old room_types
      INSERT INTO rates (room_type_id, kind, lower_bound, upper_bound, updated_at, created_at)
      SELECT room_types.id, 0, 100, 200, '2019-03-05 00:00:00', '2019-03-05 00:00:00'
      FROM room_types
      LEFT OUTER JOIN rates ON room_types.id = rates.room_type_id
      WHERE (rates.room_type_id IS NULL);
    SQL
  end
end
