class AddPaymentMethodsToInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices_charges, :kind, :integer
    add_column :invoices_charges, :creation_way, :integer
    add_column :invoices_discounts, :creation_way, :integer
    add_column :invoices_refunds, :creation_way, :integer
    add_column :invoices_refunds, :fullness_type, :integer
    add_column :invoices_payments, :payment_method, :integer
    rename_column :invoices_payments, :kind, :creation_way
    add_index :invoices_charges, :kind
    add_index :invoices_charges, :creation_way
    add_index :invoices_discounts, :creation_way
    add_index :invoices_refunds, :creation_way
    add_index :invoices_refunds, :fullness_type
    add_index :invoices_payments, :creation_way
    add_index :invoices_payments, :payment_method
  end
end
