class CreateRatesPeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :rates_periods do |t|
      t.references :hotel

      t.date :start_date, null: true
      t.date :end_date, null: true
      t.string :name, null: false, default: ''
      t.integer :period_kind, null: false, default: 0
      t.integer :price_kind, null: false, default: 0
      t.integer :discount_kind, null: false, default: 0

      t.timestamps
    end

    create_table :rates_periods_room_types do |t|
      t.references :period
      t.references :room_type

      t.decimal :discount_amount, precision: 10, scale: 2, default: 0
      t.jsonb :rates

      t.timestamps
    end
  end
end
