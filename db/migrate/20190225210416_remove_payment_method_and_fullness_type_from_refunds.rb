class RemovePaymentMethodAndFullnessTypeFromRefunds < ActiveRecord::Migration[5.2]
  def change
    remove_column :invoices_refunds, :fullness_type, :integer, default: 0
    remove_column :invoices_refunds, :payment_method, :integer, default: 0
  end
end
