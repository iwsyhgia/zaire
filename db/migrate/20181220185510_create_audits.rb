class CreateAudits < ActiveRecord::Migration[5.2]
  def change
    create_table :audits do |t|
      t.belongs_to :hotel
      t.integer :status
      t.datetime :started_at

      t.timestamps
    end

    create_table :audits_confirmations do |t|
      t.belongs_to :user
      t.belongs_to :audit
      t.datetime :confirmed_at

      t.timestamps
    end
  end
end
