class AddNameArToRoomTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :room_types, :name_ar, :string, unique: true
  end
end
