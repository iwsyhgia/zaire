class AddQuantityColumnToRoomType < ActiveRecord::Migration[5.2]
  def change
    add_column :room_types, :quantity, :integer
  end
end
