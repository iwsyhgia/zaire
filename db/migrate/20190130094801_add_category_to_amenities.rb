class AddCategoryToAmenities < ActiveRecord::Migration[5.2]
  def change
    add_column :amenities, :category, :string
  end
end
