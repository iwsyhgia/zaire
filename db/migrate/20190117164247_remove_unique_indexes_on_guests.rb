class RemoveUniqueIndexesOnGuests < ActiveRecord::Migration[5.2]
  def up
    remove_index :guests, :email
    remove_index :guests, :phone_number
  end

  def down
    add_index :guests, :email, unique: true
    add_index :guests, :phone_number, unique: true
  end
end
