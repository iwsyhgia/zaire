class CreateInvoicesForOldReservations < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        execute <<-SQL
          -- Create Invoices for old reservations
          INSERT INTO invoices (to_id, to_type, updated_at, created_at)
          SELECT reservations.id, 'Reservations::Reservation', '2019-01-30 00:00:00', '2019-01-30 00:00:00'
          FROM reservations
          LEFT OUTER JOIN invoices
          ON invoices.to_id = reservations.id AND invoices.to_type = 'Reservations::Reservation'
          WHERE reservations.type IN ('Reservations::DirectReservation') AND (invoices.to_id IS NULL);
        SQL
      end
    end
  end
end
