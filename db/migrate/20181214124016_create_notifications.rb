class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.text :message
      t.integer :kind
      t.belongs_to :recipient, polymorphic: true

      t.timestamps
    end
  end
end
