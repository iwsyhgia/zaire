class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.references :user, foreign_key: true, index: true # hotelier
      t.string :name

      t.timestamps
    end

    create_table :hotels_offline_modes do |t|
      t.references :hotel, foreign_key: true, index: true

      t.boolean :active, default: false
      t.integer :days_before_today
      t.integer :days_after_today

      t.timestamps
    end

    change_table :room_types do |t|
      t.references :hotel, foreign_key: true, index: true
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL
          -- Create Hotels for already existing hoteliers
          INSERT INTO hotels (user_id, updated_at, created_at)
          SELECT id, '2018-11-23 00:00:00', '2017-11-23 00:00:00'
          FROM users;

          -- Create Offline Mode settings for already existing hotels
          INSERT INTO hotels_offline_modes (hotel_id, updated_at, created_at)
          SELECT id, '2018-11-23 00:00:00', '2017-11-23 00:00:00'
          FROM hotels;
        SQL
      end
    end
  end
end
