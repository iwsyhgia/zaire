class AddReferenceHotelToAmenities < ActiveRecord::Migration[5.2]
  def change
    add_reference :amenities, :hotel, foreign_key: true, null: true
  end
end
