class RemoveSourceColumn < ActiveRecord::Migration[5.2]
  def change
    remove_column :reservations, :source, :string

    reversible do |dir|
      dir.up do
        execute <<-SQL
          UPDATE reservations SET type='Reservations::DirectReservation' WHERE type is null
        SQL
      end
    end
  end
end
