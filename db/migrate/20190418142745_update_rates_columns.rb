class UpdateRatesColumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :rates, :rateable_type, :string
    rename_column :rates, :rateable_id, :room_type_id

    add_column :rates_periods_room_types, :deleted_at, :datetime
    add_column :rates_periods, :deleted_at, :datetime
  end
end
