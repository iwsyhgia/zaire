class AddTimeframeToNightAudits < ActiveRecord::Migration[5.2]
  def change
    add_column :audits, :time_frame_begin, :datetime
    add_column :audits, :time_frame_end, :datetime
  end
end
