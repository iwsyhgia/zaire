class CreateTransactionsAndBalances < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.belongs_to :from, polymorphic: true, index: true
      t.belongs_to :to, polymorphic: true, index: true
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.decimal :charges_total, precision: 10, scale: 2, default: 0
      t.decimal :refunds_total, precision: 10, scale: 2, default: 0
      t.decimal :payments_total, precision: 10, scale: 2, default: 0
      t.integer :status
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :payments_transactions do |t|
      t.decimal :amount, default: 0
      t.decimal :balance, default: 0
      t.integer :status
      t.integer :transfer_kind
      t.jsonb :meta
      t.belongs_to :payment, index: true
      t.belongs_to :source, polymorphic: true, index: true
      t.belongs_to :target, polymorphic: true, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :payments_refunds do |t|
      t.text :description
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.belongs_to :payment, index: true
      t.belongs_to :user, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :payments_discounts do |t|
      t.text :description
      t.decimal :value, precision: 10, scale: 2, default: 0
      t.integer :kind
      t.belongs_to :user, index: true
      t.belongs_to :payment, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :payments_charges do |t|
      t.text :description
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.belongs_to :user, index: true
      t.belongs_to :payment, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :payments_balances do |t|
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.integer :department
      t.belongs_to :owner, polymorphic: true, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_column :reservations, :vat_percent, :decimal, precision: 5, scale: 2, default: 0
    add_column :reservations, :municipal_tax, :decimal, precision: 5, scale: 2, default: 0
  end
end
