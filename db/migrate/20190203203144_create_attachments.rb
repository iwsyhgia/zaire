class CreateAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :attachments do |t|
      t.string :file
      t.references :attachable, polymorphic: true, index: true, null: true

      t.timestamps null: false
      t.datetime :deleted_at
    end
  end
end
