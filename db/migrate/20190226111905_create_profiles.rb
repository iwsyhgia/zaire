class CreateProfiles < ActiveRecord::Migration[5.2]
  def up
    create_table :profiles do |t|
      t.string :first_name, null: true
      t.string :last_name, null: true
      t.string :phone_number, null: true
      t.integer :notifications, null: false, default: 0
      t.references :user, foreign_key: true
      t.datetime :deleted_at, index: true

      t.timestamps
    end

    execute <<-SQL
      -- Create profiles for old users
      INSERT INTO profiles (user_id, updated_at, created_at)
      SELECT users.id, '2019-03-04 00:00:00', '2019-03-04 00:00:00'
      FROM users
      LEFT OUTER JOIN profiles ON users.id = profiles.user_id 
      WHERE (profiles.user_id IS NULL);
    SQL
  end

  def down
    drop_table :profiles
  end
end
