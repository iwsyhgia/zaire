class UpgradeNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :read_at, :datetime
    add_column :notifications, :message_params, :jsonb, default: '{}', null: false
    remove_column :notifications, :message, :text
    add_index :notifications, :message_params, using: :gin

    create_table :notifications_logs do |t|
      t.references :notification
      t.integer :status
      t.text :meta
      t.timestamps
    end
  end
end
