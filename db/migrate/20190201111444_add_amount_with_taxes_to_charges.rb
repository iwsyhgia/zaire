class AddAmountWithTaxesToCharges < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices_charges,  :municipal_tax_amount, :decimal, precision: 10, scale: 2, default: 0.0
    add_column :invoices_charges,  :vat_tax_amount,       :decimal, precision: 10, scale: 2, default: 0.0
    add_column :invoices_charges,  :base_price,           :decimal, precision: 10, scale: 2, default: 0.0
    add_column :invoices_payments, :amount_without_taxes, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
