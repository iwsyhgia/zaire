class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    drop_table :payments, if_exists: true
    drop_table :payments_refunds, if_exists: true
    drop_table :payments_discounts, if_exists: true
    drop_table :payments_charges, if_exists: true
    drop_table :payments_transactions, if_exists: true
    drop_table :payments_balances, if_exists: true
    remove_column :reservations, :vat_percent, :decimal
    remove_column :reservations, :municipal_tax, :decimal

    create_table :invoices do |t|
      t.belongs_to :from, polymorphic: true, index: true
      t.belongs_to :to, polymorphic: true, index: true

      t.decimal :vat_tax, precision: 5, scale: 2, default: 0
      t.decimal :municipal_tax, precision: 5, scale: 2, default: 0
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.decimal :charges_total, precision: 10, scale: 2, default: 0
      t.decimal :refunds_total, precision: 10, scale: 2, default: 0
      t.decimal :payments_total, precision: 10, scale: 2, default: 0
      t.decimal :tax_total, precision: 10, scale: 2, default: 0
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_transactions do |t|
      t.belongs_to :document, polymorphic: true, index: true
      t.belongs_to :source, polymorphic: true, index: true
      t.belongs_to :target, polymorphic: true, index: true
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.decimal :balance, precision: 10, scale: 2, default: 0
      t.integer :status, default: 0
      t.integer :kind, default: 0
      t.jsonb :meta
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_refunds do |t|
      t.belongs_to :user, index: true
      t.belongs_to :invoice, index: true
      t.belongs_to :payment, index: true # necessary to retrieve an id of successful transaction for refund in a payment system
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.integer :status, default: 0
      t.text :description
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_payments do |t|
      t.belongs_to :user, index: true
      t.belongs_to :invoice, index: true
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.integer :status, default: 0
      t.integer :kind, default: 0 # cash or card
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_charges do |t|
      t.belongs_to :user, index: true
      t.belongs_to :invoice, index: true
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.text :description
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_discounts do |t|
      t.belongs_to :user, index: true
      t.belongs_to :invoice, index: true
      t.decimal :value, precision: 10, scale: 2, default: 0
      t.integer :kind, default: 0
      t.text :description
      t.datetime :deleted_at

      t.timestamps
    end

    create_table :invoices_balances do |t|
      t.belongs_to :owner, polymorphic: true, index: true
      t.decimal :amount, precision: 10, scale: 2, default: 0
      t.integer :department
      t.datetime :deleted_at

      t.timestamps
    end

  end
end
