class CreateAmenitiesRoomTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :amenities_room_types do |t|
      t.references :room_type, foreign_key: true
      t.references :amenity, foreign_key: true

      t.timestamps
    end
  end
end
