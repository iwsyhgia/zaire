class DropUnusedTables < ActiveRecord::Migration[5.2]
  def up
    drop_table :amenities_users if ActiveRecord::Base.connection.data_source_exists? 'amenities_users'
    drop_table :room_types_users if ActiveRecord::Base.connection.data_source_exists? 'room_types_users'
    drop_table :rooms_users if ActiveRecord::Base.connection.data_source_exists? 'rooms_users'
  end
end
