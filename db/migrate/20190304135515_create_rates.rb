class CreateRates < ActiveRecord::Migration[5.2]
  def up
    create_table :rates do |t|
      t.references :rateable, index: true, polymorphic: true
      t.decimal :lower_bound, precision: 10, scale: 2, default: 0
      t.decimal :upper_bound, precision: 10, scale: 2, default: 0
      t.integer :kind, precision: 10, scale: 2, default: 0
      t.datetime :deleted_at

      t.timestamps
    end

    execute <<-SQL
      -- Create rates for old room_types
      INSERT INTO rates (rateable_id, rateable_type, kind, lower_bound, upper_bound, updated_at, created_at)
      SELECT room_types.id, 'Rooms::RoomType', 0, 100, 200, '2019-03-05 00:00:00', '2019-03-05 00:00:00'
      FROM room_types
      LEFT OUTER JOIN rates ON room_types.id = rates.rateable_id AND rates.rateable_type = 'Rooms::RoomType'
      WHERE (rates.rateable_id IS NULL);
    SQL
  end

  def down
    drop_table :rates
  end
end
