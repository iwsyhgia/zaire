class AddNameArToAmenities < ActiveRecord::Migration[5.2]
  def change
    add_column :amenities, :name_ar, :string, unique: true
  end
end
