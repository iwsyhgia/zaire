class AddColumnsToInvoices < ActiveRecord::Migration[5.2]
  def change
    change_table :invoices do |t|
      t.decimal :stay_fees_total, precision: 10, scale: 2, default: 0
      t.decimal :unpaid_total, precision: 10, scale: 2, default: 0
      t.decimal :paid_total, precision: 10, scale: 2, default: 0
      t.decimal :price_total, precision: 10, scale: 2, default: 0
      t.decimal :price_total_with_taxes, precision: 10, scale: 2, default: 0
      t.decimal :price_per_night_with_taxes, precision: 10, scale: 2, default: 0
      t.decimal :price_per_night, precision: 10, scale: 2, default: 0
      t.decimal :vat_total, precision: 10, scale: 2, default: 0
      t.decimal :municipal_total, precision: 10, scale: 2, default: 0
      t.decimal :sellable_items_discount_total, precision: 10, scale: 2, default: 0
      t.decimal :current_rate, precision: 10, scale: 2, default: 0
    end

    rename_table :rates, :rooms_rates
    add_column :rooms_rates, :active, :boolean, default: true

    rename_column :rates_periods_room_types, :discount_amount, :discount_value
  end
end
