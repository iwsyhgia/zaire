class CreateAasmLogsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :aasm_logs do |t|
      t.references :user, null: true
      t.references :aasm_loggable, polymorphic: true, null: true,
                                   index: { name: :index_aasm_logs_tables_on_aasm_loggable }
      t.string     :from_state, null: true
      t.string     :to_state, null: true
      t.jsonb      :meta
      t.timestamps null: false
    end
  end
end
