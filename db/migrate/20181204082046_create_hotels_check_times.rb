class CreateHotelsCheckTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels_check_times do |t|
      t.references :hotel, foreign_key: true
      t.integer :check_type, default: 0
      t.boolean :fixed, default: true
      t.string :start_time, null: false
      t.string :end_time

      t.timestamps
    end
  end
end
