class AddAddressTablesToHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels_addresses do |t|
      t.references :hotel, foreign_key: true, index: true

      t.string :country
      t.string :city
      t.string :state
      t.string :address_1
      t.string :address_2

      t.datetime :deleted_at
      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL
          -- Create Address setting for already existing hotels
          INSERT INTO hotels_addresses (hotel_id, updated_at, created_at)
          SELECT id, '2018-12-28 00:00:00', '2018-12-28 00:00:00'
          FROM hotels;
        SQL
      end
    end
  end
end
