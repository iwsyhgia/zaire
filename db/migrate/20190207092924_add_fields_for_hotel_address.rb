class AddFieldsForHotelAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :hotels_addresses, :phone_number, :string, null: true
    add_column :hotels_addresses, :email, :string, null: true
    add_column :hotels_addresses, :fax, :string, null: true
  end
end
