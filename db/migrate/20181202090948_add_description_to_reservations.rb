class AddDescriptionToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :type, :string
    add_column :reservations, :description, :string
  end
end
