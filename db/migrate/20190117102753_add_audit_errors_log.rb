class AddAuditErrorsLog < ActiveRecord::Migration[5.2]
  def change
    create_table :audits_error_logs do |t|
      t.references :audit, foreign_key: true, index: true
      t.references :auditable, polymorphic: true, index: true
      t.integer :step # enum
      t.jsonb :meta

      t.timestamps
    end

    add_belongs_to :invoices_charges, :audit, foreign_key: true, index: true
  end
end
