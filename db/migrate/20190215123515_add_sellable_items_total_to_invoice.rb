class AddSellableItemsTotalToInvoice < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :sellable_items_total, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
