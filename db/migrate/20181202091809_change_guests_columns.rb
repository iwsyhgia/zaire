class ChangeGuestsColumns < ActiveRecord::Migration[5.2]
  def change
    add_index :guests, :email, unique: true
    add_index :guests, :phone_number, unique: true
  end
end
