class UpdateInvoices < ActiveRecord::Migration[5.2]
  def change
    add_column :invoices, :booking_total, :decimal, precision: 10, scale: 2, default: 0, null: false
  end
end
