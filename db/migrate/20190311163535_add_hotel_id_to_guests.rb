class AddHotelIdToGuests < ActiveRecord::Migration[5.2]
  def change
    add_reference :guests, :hotel, index: true
  end
end
