class RemoveNullConstraintOnRoomTypes < ActiveRecord::Migration[5.2]
  def change
    change_column :room_types, :size, :string, null: true
    change_column :room_types, :unit, :string, null: true
  end
end
