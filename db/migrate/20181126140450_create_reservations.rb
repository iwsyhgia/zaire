class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :guests do |t|
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone_code
      t.string :phone_number
      t.string :country
      t.string :city

      t.timestamps
    end

    create_table :reservations do |t|
      t.references :guest, foreign_key: true, index: true
      t.references :room,  foreign_key: true, index: true

      t.date    :check_in_date
      t.date    :check_out_date
      t.integer :status, default: 0
      t.integer :number_of_adults
      t.integer :number_of_children
      t.string  :source

      t.timestamps
    end

    create_table :reservations_adults do |t|
      t.references :reservation, foreign_key: true, index: true

      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :personal_id

      t.timestamps
    end
  end
end
