class AddSoftDelete < ActiveRecord::Migration[5.2]
  def change
    add_column :hotels,               :deleted_at, :datetime
    add_column :hotels_offline_modes, :deleted_at, :datetime
    add_column :users,                :deleted_at, :datetime
    add_column :rooms,                :deleted_at, :datetime
    add_column :room_types,           :deleted_at, :datetime
    add_column :reservations,         :deleted_at, :datetime
    add_column :reservations_adults,  :deleted_at, :datetime
    add_column :guests,               :deleted_at, :datetime
    add_column :images,               :deleted_at, :datetime
  end
end
