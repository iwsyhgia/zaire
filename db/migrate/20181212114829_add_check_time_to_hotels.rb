class AddCheckTimeToHotels < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
      -- Create check_in_time for already existing hotels
      INSERT INTO hotels_check_times (hotel_id, check_type, fixed, start_time, updated_at, created_at)
      SELECT id, 0, true, '14:00', '2018-12-12 00:00:00', '2018-12-12 00:00:00'
      FROM hotels;

      -- Create check_out_time for already existing hotels
      INSERT INTO hotels_check_times (hotel_id, check_type, fixed, start_time, end_time, updated_at, created_at)
      SELECT id, 1, false, '12:00', '14:00', '2018-12-12 00:00:00', '2018-12-12 00:00:00'
      FROM hotels;
    SQL
  end
end
