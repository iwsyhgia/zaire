class CreateHotelsConfigurations < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels_configurations do |t|
      t.references :hotel, foreign_key: true, index: true

      t.integer :night_audit_time, default: 0
      t.boolean :night_audit_confirmation_required, default: true
      t.string  :time_zone, default: 'UTC'

      t.datetime :deleted_at
      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL
          -- Create Address setting for already existing hotels
          INSERT INTO hotels_configurations (hotel_id, updated_at, created_at)
          SELECT id, '2018-12-28 00:00:00', '2018-12-28 00:00:00'
          FROM hotels;
        SQL
      end
    end
  end
end
