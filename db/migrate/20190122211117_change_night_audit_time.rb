class ChangeNightAuditTime < ActiveRecord::Migration[5.2]
  def change
    remove_column :hotels_configurations, :night_audit_time, :integer, default: 0
    add_column :hotels_configurations, :night_audit_time, :string, default: '03:00'
  end
end
