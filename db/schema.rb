# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_01_130806) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aasm_logs", force: :cascade do |t|
    t.bigint "user_id"
    t.string "aasm_loggable_type"
    t.bigint "aasm_loggable_id"
    t.string "from_state"
    t.string "to_state"
    t.jsonb "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aasm_loggable_type", "aasm_loggable_id"], name: "index_aasm_logs_tables_on_aasm_loggable"
    t.index ["user_id"], name: "index_aasm_logs_on_user_id"
  end

  create_table "amenities", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "hotel_id"
    t.string "name_ar"
    t.string "category"
    t.string "category_ar"
    t.index ["hotel_id"], name: "index_amenities_on_hotel_id"
  end

  create_table "amenities_room_types", force: :cascade do |t|
    t.bigint "room_type_id"
    t.bigint "amenity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["amenity_id"], name: "index_amenities_room_types_on_amenity_id"
    t.index ["room_type_id"], name: "index_amenities_room_types_on_room_type_id"
  end

  create_table "attachments", force: :cascade do |t|
    t.string "file"
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id"
  end

  create_table "audits", force: :cascade do |t|
    t.bigint "hotel_id"
    t.integer "status"
    t.datetime "started_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "time_frame_begin"
    t.datetime "time_frame_end"
    t.index ["hotel_id"], name: "index_audits_on_hotel_id"
  end

  create_table "audits_confirmations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "audit_id"
    t.datetime "confirmed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["audit_id"], name: "index_audits_confirmations_on_audit_id"
    t.index ["user_id"], name: "index_audits_confirmations_on_user_id"
  end

  create_table "audits_error_logs", force: :cascade do |t|
    t.bigint "audit_id"
    t.string "auditable_type"
    t.bigint "auditable_id"
    t.integer "step"
    t.jsonb "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["audit_id"], name: "index_audits_error_logs_on_audit_id"
    t.index ["auditable_type", "auditable_id"], name: "index_audits_error_logs_on_auditable_type_and_auditable_id"
  end

  create_table "flipper_features", force: :cascade do |t|
    t.string "key", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_flipper_features_on_key", unique: true
  end

  create_table "flipper_gates", force: :cascade do |t|
    t.string "feature_key", null: false
    t.string "key", null: false
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feature_key", "key", "value"], name: "index_flipper_gates_on_feature_key_and_key_and_value", unique: true
  end

  create_table "guests", force: :cascade do |t|
    t.string "title"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone_code"
    t.string "phone_number"
    t.string "country"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.date "birth_date"
    t.bigint "hotel_id"
    t.index ["hotel_id"], name: "index_guests_on_hotel_id"
  end

  create_table "hotels", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "name_ar"
    t.index ["user_id"], name: "index_hotels_on_user_id"
  end

  create_table "hotels_addresses", force: :cascade do |t|
    t.bigint "hotel_id"
    t.string "country"
    t.string "city"
    t.string "state"
    t.string "address_1"
    t.string "address_2"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone_number"
    t.string "email"
    t.string "fax"
    t.index ["hotel_id"], name: "index_hotels_addresses_on_hotel_id"
  end

  create_table "hotels_check_times", force: :cascade do |t|
    t.bigint "hotel_id"
    t.integer "check_type", default: 0
    t.boolean "fixed", default: true
    t.string "start_time", null: false
    t.string "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_id"], name: "index_hotels_check_times_on_hotel_id"
  end

  create_table "hotels_configurations", force: :cascade do |t|
    t.bigint "hotel_id"
    t.boolean "night_audit_confirmation_required", default: true
    t.string "time_zone", default: "UTC"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "night_audit_time", default: "05:00"
    t.index ["hotel_id"], name: "index_hotels_configurations_on_hotel_id"
  end

  create_table "hotels_offline_modes", force: :cascade do |t|
    t.bigint "hotel_id"
    t.boolean "active", default: false
    t.integer "days_before_today"
    t.integer "days_after_today"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["hotel_id"], name: "index_hotels_offline_modes_on_hotel_id"
  end

  create_table "images", force: :cascade do |t|
    t.string "file"
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.string "tag"
    t.datetime "optimized_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.string "from_type"
    t.bigint "from_id"
    t.string "to_type"
    t.bigint "to_id"
    t.decimal "vat_tax", precision: 5, scale: 2, default: "0.0"
    t.decimal "municipal_tax", precision: 5, scale: 2, default: "0.0"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "charges_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "refunds_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "payments_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "charges_with_taxes_total", precision: 10, scale: 2, default: "0.0"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "discounts_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "booking_total", precision: 10, scale: 2, default: "0.0", null: false
    t.decimal "sellable_items_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "stay_fees_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "unpaid_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "paid_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "price_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "price_total_with_taxes", precision: 10, scale: 2, default: "0.0"
    t.decimal "price_per_night_with_taxes", precision: 10, scale: 2, default: "0.0"
    t.decimal "price_per_night", precision: 10, scale: 2, default: "0.0"
    t.decimal "vat_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "municipal_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "sellable_items_discount_total", precision: 10, scale: 2, default: "0.0"
    t.decimal "current_rate", precision: 10, scale: 2, default: "0.0"
    t.index ["from_type", "from_id"], name: "index_invoices_on_from_type_and_from_id"
    t.index ["to_type", "to_id"], name: "index_invoices_on_to_type_and_to_id"
  end

  create_table "invoices_balances", force: :cascade do |t|
    t.string "owner_type"
    t.bigint "owner_id"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.integer "department"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_invoices_balances_on_owner_type_and_owner_id"
  end

  create_table "invoices_charges", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "invoice_id"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.text "description"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "kind"
    t.integer "creation_way"
    t.bigint "audit_id"
    t.decimal "municipal_tax_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "vat_tax_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "base_price", precision: 10, scale: 2, default: "0.0"
    t.index ["audit_id"], name: "index_invoices_charges_on_audit_id"
    t.index ["creation_way"], name: "index_invoices_charges_on_creation_way"
    t.index ["invoice_id"], name: "index_invoices_charges_on_invoice_id"
    t.index ["kind"], name: "index_invoices_charges_on_kind"
    t.index ["user_id"], name: "index_invoices_charges_on_user_id"
  end

  create_table "invoices_discounts", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "invoice_id"
    t.decimal "value", precision: 10, scale: 2, default: "0.0"
    t.integer "kind", default: 0
    t.text "description"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "creation_way"
    t.index ["creation_way"], name: "index_invoices_discounts_on_creation_way"
    t.index ["invoice_id"], name: "index_invoices_discounts_on_invoice_id"
    t.index ["user_id"], name: "index_invoices_discounts_on_user_id"
  end

  create_table "invoices_payments", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "invoice_id"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.integer "status", default: 0
    t.integer "creation_way", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "payment_method"
    t.decimal "amount_without_taxes", precision: 10, scale: 2, default: "0.0"
    t.index ["creation_way"], name: "index_invoices_payments_on_creation_way"
    t.index ["invoice_id"], name: "index_invoices_payments_on_invoice_id"
    t.index ["payment_method"], name: "index_invoices_payments_on_payment_method"
    t.index ["user_id"], name: "index_invoices_payments_on_user_id"
  end

  create_table "invoices_refunds", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "invoice_id"
    t.bigint "payment_id"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.integer "status", default: 0
    t.text "description"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "creation_way"
    t.index ["creation_way"], name: "index_invoices_refunds_on_creation_way"
    t.index ["invoice_id"], name: "index_invoices_refunds_on_invoice_id"
    t.index ["payment_id"], name: "index_invoices_refunds_on_payment_id"
    t.index ["user_id"], name: "index_invoices_refunds_on_user_id"
  end

  create_table "invoices_transactions", force: :cascade do |t|
    t.string "document_type"
    t.bigint "document_id"
    t.string "source_type"
    t.bigint "source_id"
    t.string "target_type"
    t.bigint "target_id"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "balance", precision: 10, scale: 2, default: "0.0"
    t.integer "status", default: 0
    t.integer "kind", default: 0
    t.jsonb "meta"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["document_type", "document_id"], name: "index_invoices_transactions_on_document_type_and_document_id"
    t.index ["source_type", "source_id"], name: "index_invoices_transactions_on_source_type_and_source_id"
    t.index ["target_type", "target_id"], name: "index_invoices_transactions_on_target_type_and_target_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "kind"
    t.string "recipient_type"
    t.bigint "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "read_at"
    t.jsonb "message_params", default: "{}", null: false
    t.index ["message_params"], name: "index_notifications_on_message_params", using: :gin
    t.index ["recipient_type", "recipient_id"], name: "index_notifications_on_recipient_type_and_recipient_id"
  end

  create_table "notifications_logs", force: :cascade do |t|
    t.bigint "notification_id"
    t.integer "status"
    t.text "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["notification_id"], name: "index_notifications_logs_on_notification_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone_number"
    t.integer "notifications", default: 0, null: false
    t.bigint "user_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_profiles_on_deleted_at"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "rates_periods", force: :cascade do |t|
    t.bigint "hotel_id"
    t.date "start_date"
    t.date "end_date"
    t.string "name", default: "", null: false
    t.integer "period_kind", default: 0, null: false
    t.integer "price_kind", default: 0, null: false
    t.integer "discount_kind", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["hotel_id"], name: "index_rates_periods_on_hotel_id"
  end

  create_table "rates_periods_room_types", force: :cascade do |t|
    t.bigint "period_id"
    t.bigint "room_type_id"
    t.decimal "discount_value", precision: 10, scale: 2, default: "0.0"
    t.jsonb "rates"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["period_id"], name: "index_rates_periods_room_types_on_period_id"
    t.index ["room_type_id"], name: "index_rates_periods_room_types_on_room_type_id"
  end

  create_table "reservations", force: :cascade do |t|
    t.bigint "guest_id"
    t.bigint "room_id"
    t.date "check_in_date"
    t.date "check_out_date"
    t.integer "status", default: 0
    t.integer "number_of_adults"
    t.integer "number_of_children"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.string "description"
    t.datetime "deleted_at"
    t.integer "rate", default: 0, null: false
    t.integer "payment_kind"
    t.index ["guest_id"], name: "index_reservations_on_guest_id"
    t.index ["room_id"], name: "index_reservations_on_room_id"
  end

  create_table "reservations_adults", force: :cascade do |t|
    t.bigint "reservation_id"
    t.string "title"
    t.string "first_name"
    t.string "last_name"
    t.string "personal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["reservation_id"], name: "index_reservations_adults_on_reservation_id"
  end

  create_table "room_types", force: :cascade do |t|
    t.string "name", null: false
    t.string "size"
    t.string "unit"
    t.integer "max_occupancy", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.bigint "hotel_id"
    t.datetime "deleted_at"
    t.string "name_ar"
    t.index ["hotel_id"], name: "index_room_types_on_hotel_id"
  end

  create_table "rooms", force: :cascade do |t|
    t.integer "number", null: false
    t.bigint "room_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["room_type_id"], name: "index_rooms_on_room_type_id"
  end

  create_table "rooms_rates", force: :cascade do |t|
    t.bigint "room_type_id"
    t.decimal "lower_bound", precision: 10, scale: 2, default: "0.0"
    t.decimal "upper_bound", precision: 10, scale: 2, default: "0.0"
    t.integer "kind", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
  end

  create_table "seed_migration_data_migrations", force: :cascade do |t|
    t.string "version"
    t.integer "runtime"
    t.datetime "migrated_on"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 0, null: false
    t.datetime "deleted_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "amenities", "hotels"
  add_foreign_key "amenities_room_types", "amenities"
  add_foreign_key "amenities_room_types", "room_types"
  add_foreign_key "audits_error_logs", "audits"
  add_foreign_key "hotels", "users"
  add_foreign_key "hotels_addresses", "hotels"
  add_foreign_key "hotels_check_times", "hotels"
  add_foreign_key "hotels_configurations", "hotels"
  add_foreign_key "hotels_offline_modes", "hotels"
  add_foreign_key "invoices_charges", "audits"
  add_foreign_key "profiles", "users"
  add_foreign_key "reservations", "guests"
  add_foreign_key "reservations", "rooms"
  add_foreign_key "reservations_adults", "reservations"
  add_foreign_key "room_types", "hotels"
  add_foreign_key "rooms", "room_types"
end
