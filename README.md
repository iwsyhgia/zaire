[![Ruby Version](https://img.shields.io/badge/ruby-v2.6.2-blue.svg?longCache=true&style=flat-square)](https://www.ruby-lang.org/en/downloads/) [![Rails Version](https://img.shields.io/badge/rails-v5.2.0-blue.svg?longCache=true&style=flat-square)](https://rubygems.org/gems/rails/versions/5.2.0) [![CI dev](https://gitlab.sumatosoft.com/zaire/zaire/badges/dev/pipeline.svg)](https://gitlab.sumatosoft.com/zaire/zaire/commits/dev)

# Zaire

[![JIRA Board](https://img.shields.io/badge/Jira%20Board-link-green.svg?longCache=true&style=for-the-badge)](https://jira.sumatosoft.com/jira/secure/RapidBoard.jspa?projectKey=ZAIR&rapidView=138) [![Errbit Exceptions](https://img.shields.io/badge/Errbit%20Exceptions-link-green.svg?longCache=true&style=for-the-badge)](https://errbit.sumatosoft.com/apps/5bd6ece890e34303fb99c0c7) [![Confluence Space](https://img.shields.io/badge/Confluence%20space-link-green.svg?longCache=true&style=for-the-badge)](https://jira.sumatosoft.com/confluence/display/ZAIR/Zaire) [![SLACK Chat](https://img.shields.io/badge/Slack%20Chat-link-green.svg?longCache=true&style=for-the-badge)](https://sumatosoftteam.slack.com/messages/GDRVC2962/) [![Designs](https://img.shields.io/badge/designs-link-green.svg?longCache=true&style=for-the-badge)](https://xd.adobe.com/spec/d453c74e-6f2b-4c86-54ce-b66e33af3951-a262/) [![User Stories](https://img.shields.io/badge/user%20stories-link-green.svg?longCache=true&style=for-the-badge)](https://docs.google.com/document/d/1CPcFiEfGROU2LRUmKTu9NqTUaSSN659T5ROLaOvf-Sc/edit) [![Timesheet](https://img.shields.io/badge/timesheet-link-green.svg?longCache=true&style=for-the-badge)](https://docs.google.com/spreadsheets/d/1cECo-jP3Ybu7i2NWYes2rvyowx1YaBYWF08cdRww8Aw/edit#gid=0) [![Translations](https://img.shields.io/badge/translations-link-green.svg?longCache=true&style=for-the-badge)](https://docs.google.com/spreadsheets/d/1VP2SLdBnAr3DwyEpjuoS2wAOJJh441COJpcmWQIL1wE/edit#gid=566055345)

## Description

Zaire is a property management system, which is developed to be integrated for the client's hotels network management and sold as SaaS service. The audience for SaaS is hoteliers, who owns not to big hotels. [more details](https://jira.sumatosoft.com/confluence/display/ZAIR/Project+Goal+and+Description)

## Links
+ **DEV** (for some devs branches like Offline)- [dev.zaire.demo.sumatosoft.com](http://dev.zaire.demo.sumatosoft.com)
+ **QA** (for testing currently developing features)- [qa.zaire.demo.sumatosoft.com](http://qa.zaire.demo.sumatosoft.com)
+ **Staging** (for final testing = pre-prod) - [zaire.demo.sumatosoft.com](http://zaire.demo.sumatosoft.com)
+ **Production** - <CHANGE_ME_TO_PRODUCTION_LINK>

*for now staging and qa are both installed on one EC2 intance on Sumatosoft AWS*

## System\app dependencies
* [Ruby - v2.5.1](https://www.ruby-lang.org/en/downloads/)
* [Rails - v5.2.0](https://rubygems.org/gems/rails/versions/5.2.0)
* [PostgreSQL - v10](https://www.postgresql.org/download/)
* [ImageMagic](https://www.imagemagick.org/)
* [Sidekiq - v5.2](https://github.com/mperham/sidekiq)
* [Redis](https://redis.io/topics/quickstart)
* [Rubocop - v0.66.0](https://github.com/rubocop-hq/rubocop)
* [Webpacker - '>= 4.0.x'](https://github.com/rails/webpacker)
* [Node - v11.0.0](https://nodesource.com/blog/installing-node-js-tutorial-using-nvm-on-mac-os-x-and-ubuntu/)
* React, Redux

+ Deafult system timezone is **UTC**
+ To make PDF generation work (works using headless chrome) on Ubuntu run
`sudo apt-get install gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget`


## API \ Other important libraries:

### Our Api
- docs Swagger path: `/api_docs/swagger_docs/v1`
- Postman is available for **development** and **qa** environments. Use our common server login\password for basic authorization. All requests and responses will be on behalf first user: *admin/admin*.

## Deployment Process [read more in the guideline](https://jira.sumatosoft.com/confluence/pages/viewpage.action?spaceKey=TEAM&title=GitFlow)

+ Your ssh key must be added to the server. (if it's not ask g.melesh@sumatosoft.com)
+ In short to deploy: `cap qa deploy` or `BRANCH='features/my-branch' cap qa deploy`

* All current sprint work goes to `dev` branch [can be deployed to QA]
* When sprint is fully ready for QA we merge it to `qa` branch [can be deployed to QA]
* When Sprint is tested by QA team we create `releases/<release_number>` branch and merge it to `staging` branch [can be deployed to Staging]
* If there any fixes to staging or qa create `fixes/<JIRA-ID>-<short-descr>`
  * switch to `qa` branch: `git checkout qa && git pull origin qa`
  * create fix branch `git checkout -b fixes/<JIRA-ID>-<short-desc>`
  * add needed fix
  * create PR to `qa` branch
  * merge to QA after approve
  * also merge it to dev `git checkout dev && git pull origin dev && git merge origin qa`
* If there any urgent fixes to release or production create `hotfixes/<JIRA-ID>-<release_number>-<hotfix-version>`


### Deploy current sprint work without code freezing
* run `BRANCH=dev cap qa deploy`

### Deploy to QA = code freeze
* create PR from `dev` branch to `qa` branch. name it <Sprint Name> - [dev to qa]
* merge it.
* run `cap qa deploy`

### Deploy to Staging = deploy to prod for now
* switch to `qa` branch: `git checkout qa && git pull origin qa`
* create release branch: `git checkout -b releases/<sprint_number>-<version_number>`
* create tags: `git tag release.YYYY-MM-dd.v && git push origin --tags`
* merge to staging: `git checkout staging && git pull origin staging && git merge origin releases/<release_number>`
* run `cap staging deploy`


### Data migration
* You can use the generator `rails g seed_migration AddFoo`
* To run all pending migrations, simply use `rake seed:migrate`

## IMPORTANT:
Deploy doesn't automatically apply changes to the next files:

* 'config/database.yml'
* 'config/secrets.yml'
* 'config/sidekiq.yml'
* 'config/webpacker.yml'

To change them manually ssh to the server and go to `cd /var/www/apps/zaire_qa/shared/config`

### Restart

* If Websockets messages are not transmitted(restart nginx): `sudo service nginx restart`

### SSH to staging example
* `ssh app@zaire.demo.sumatosoft.com`
* `cd /var/www/apps/zaire_staging/current`

view logs: `tail -f /var/log/zaire_staging/staging.log` or `tail -f /var/log/nginx/error.log`
run console: `RAILS_ENV=staging bundle exec rails c`
restart server: `touch tmp/restart`

### Download Dump from staging example
* `ssh app@qa.zaire.demo.sumatosoft.com -C 'pg_dump zaire_qa' > dump.txt` - download dump
* `bundle exec rake db:drop & bundle exec rake db:create` -  drop old data
* `cat dump.txt | bundle exec rails db` - apply dump
* `bundle exec rake db:migrate` - appy migrations
* `bundle exec rake db:remove_pii` - Reset passwords to `Zaire123` and add .blocked to non @sumatosoft emails

## Code Checklists:

### Migration: if there are any migrations

- [ ] creates all needed Indexes
- [ ] the scheme.rb is updated
- [ ] it doesn't break old records and related staff. (In case of adding a new table\column)
- [ ] it doesn't contain references to Model classes. If you need to update something use raw SQL or Rake task.
- [ ] add 'null: false' if there is a default value. Set default value in the model callback
- [ ] it is reversible

### Checklist for Swagger docs.

The goal here is to make our API readable and usable for QA and FE, so we can save time on discussions how to use it.

- [ ] All namespaces are sorted by alphabet]
- [ ] If there are more than one namespace use template <namespace> - [Another namespace]. ex: Hotels - [Offline Modes]
- [ ] Endpoint description says what id does.
- [ ] Endpoint description contains restrictions if there any
- [ ] Endpoint description contains default behaviour if there is any specific
- [ ] Endpoint description contains any other information QA/FE must know to use it
- [ ] Update request should return an **updated object**
- [ ] Delete request should return an **id** of destroyed object
- [ ] Must have possible responses
- [ ] Request parameters have descriptions if it's not obvious what does it mean
- [ ] Validates association id if there any (to prevent throwing 500 error if Not found)

### Versioning

- [ ] gems in Gemfile have strict version (means no '~>')
- [ ] each gem must have a version (exception: gems with githab link)
- [ ] make sure that bundler updates only needed gems.
- [ ] never do `bundle update`

### Overall requirement for PR

- [ ] contains Jira issue ID in the title
- [ ] has working code
- [ ] has tests
- [ ] has at least one test if it was a BE bug
- [ ] passed tests
- [ ] passed CI
- [ ] has at least 2 Approves if it's a story PR
- [ ] has at least 1 Approve if it's a bug PR


## Guides
+ Ruby Style Guide: https://gitlab.sumatosoft.com/employees/ruby-style-guide
+ Rails Style Guide: https://gitlab.sumatosoft.com/employees/rails-style-guide
+ JS Style Guide: https://gitlab.sumatosoft.com/employees/javascript-style-guide, https://gist.github.com/Fabel/23d450d19949847042bafb6563142c99
+ Git Flow Guide: https://jira.sumatosoft.com/confluence/pages/viewpage.action?spaceKey=TEAM&title=GitFlow
+ CI Setup Guide: https://jira.sumatosoft.com/confluence/display/TEAM/How+to+setup+GitLab+CI

------------------------
## Contacts

Current team:
+ PM: [@y.makarevich](https://gitlab.sumatosoft.com/y.makarevich) - y.makarevich@sumatosoft.com
+ Lead: [@k.kasinskaya](https://gitlab.sumatosoft.com/k.kasinskaya) - k.kasinskaya@suamtosoft.com
+ BE: [@v.borozna](https://gitlab.sumatosoft.com/v.borozna) - v.borozna@suamtosoft.com
+ BE: [@a.hrynevich](https://gitlab.sumatosoft.com/a.hrynevich) - a.hrynevich@suamtosoft.com
+ FE: [@s.krasnovskaya](https://gitlab.sumatosoft.com/s.krasnovskaya) - s.krasnovskaya@suamtosoft.com
+ FE: [@u.sysoyeu](https://gitlab.sumatosoft.com/u.sysoyeu) - u.sysoyeu@suamtosoft.com
+ QA: [@t.haurylavets](https://gitlab.sumatosoft.com/t.haurylavets) - t.haurylavets@sumatosoft.com
+ QA: [@d.lomeyko](https://gitlab.sumatosoft.com/d.lomeyko) - d.lomeyko@sumatosoft.com

Contributors:
+ BE: [@v.khvistik](https://gitlab.sumatosoft.com/v.khvistik) - v.khvistik@suamtosoft.com
+ BE: [@a.andrade](https://gitlab.sumatosoft.com/a.andrade) - a.andrade@suamtosoft.com
+ FE: [@d.viazhevich](https://gitlab.sumatosoft.com/d.viazhevich) - d.viazhevich@suamtosoft.com
+ Design: [@s.gavris](https://gitlab.sumatosoft.com/s.gavris) - s.gavris@suamtosoft.com
