# frozen_string_literal: true

class WelcomeMailer < BaseMandrillMailer
  def welcome_after_sign_up(user)
    return unless user.email.present?

    template_vars = {
      subject: I18n.t('welcome_mailer.welcome_after_sign_up.subject'),
      message: I18n.t('welcome_mailer.welcome_after_sign_up.message', email: user.email)
    }

    mail_with_mandrill('account-welcome-email-after-sign-up', user.email, template_vars)
  end

  def welcome_after_check_in(reservation)
    return unless reservation.guest.email.present?

    template_vars = {
      subject: I18n.t('welcome_mailer.welcome_after_check_in.subject', hotel_name: reservation.hotel.name),
      message: I18n.t('welcome_mailer.welcome_after_check_in.message',
        first_name: reservation.guest.first_name,
        hotel_name: reservation.hotel.name)
    }

    mail_with_mandrill('account-welcome-email-after-check-in', reservation.guest.email, template_vars)
  end
end
