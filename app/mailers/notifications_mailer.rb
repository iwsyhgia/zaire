# frozen_string_literal: true

class NotificationsMailer < ApplicationMailer
  default from: 'do-not-reply@zaire.com'

  def default_notification(email:, message_params:)
    @message_params = message_params
    mail(to: email, subject: 'Notification')
  end
end
