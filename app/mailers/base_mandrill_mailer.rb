# frozen_string_literal: true

class BaseMandrillMailer < MandrillMailer::TemplateMailer
  include ApplicationHelper
  include ::ActionView::Helpers::AssetTagHelper

  default from:              'noreply@zaiier.com'
  default from_name:         'Zaiier'
  default view_content_link: true

  def default_attrs
    {
      merge: true,
      important: true,
      inline_css: true,
      merge_language: 'mailchimp'
    }
  end

  def mail_with_mandrill(template, recipients_emails, template_vars, images = [], merge_vars = [])
    mail_data = {
      to: recepient_data(recipients_emails),
      vars: template_vars.merge!(default_attrs).stringify_keys,
      images: images,
      subject: template_vars[:subject],
      template: template,
      merge_vars: merge_vars
    }

    mandrill_mail(mail_data)
  end

  def recepient_data(recipients_emails)
    recipients_emails = [recipients_emails] unless recipients_emails.is_a?(Array)
    return {} if recipients_emails.empty?

    recipients_emails.map { |email| { email: email } }
  end
end
