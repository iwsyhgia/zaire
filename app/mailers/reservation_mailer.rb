# frozen_string_literal: true

class ReservationMailer < BaseMandrillMailer
  def room_changed(email:, message_params:)
    reservation = ::Reservations::Reservation.find(message_params['reservation_id'])
    return unless reservation && email.present?

    template_vars = {
      subject: I18n.t('reservation_mailer.room_changed.subject'),
      message: I18n.t('reservation_mailer.room_changed.message',
        first_name: reservation.guest.first_name,
        id: reservation.id,
        new_room_type: reservation.room.room_type.name,
        total_stay_fee_amount: reservation.price_total_with_taxes)
    }

    mail_with_mandrill('reservation-room-changed', email, template_vars)
  end

  def dates_changed(email:, message_params:)
    reservation = ::Reservations::Reservation.find(message_params['reservation_id'])
    return unless reservation && email.present?

    template_vars = {
      subject: I18n.t('reservation_mailer.dates_changed.subject'),
      message: I18n.t('reservation_mailer.dates_changed.message',
        first_name: reservation.guest.first_name,
        check_in_date: reservation.check_in_date,
        check_out_date: reservation.check_out_date,
        total_stay_fee_amount: reservation.price_total_with_taxes)
    }

    mail_with_mandrill('reservation-dates-changed', email, template_vars)
  end

  def reservation_cancelled(email:, message_params:)
    reservation = ::Reservations::Reservation.find(message_params['reservation_id'])
    return unless reservation && email.present?

    template_vars = {
      subject: I18n.t('reservation_mailer.reservation_cancelled.subject'),
      message: I18n.t('reservation_mailer.reservation_cancelled.message',
        first_name: reservation.guest.first_name,
        id: reservation.id)
    }

    mail_with_mandrill('reservation-cancelled', email, template_vars)
  end

  def reservation_invoice(email:, message_params:)
    reservation = ::Reservations::Reservation.find(message_params['reservation_id'])
    return unless reservation && email.present?

    template_vars = {
      subject: I18n.t('reservation_mailer.reservation_invoice.subject'),
      message: I18n.t('reservation_mailer.reservation_invoice.message',
        first_name: reservation.guest.first_name,
        check_in_date: reservation.check_in_date,
        check_out_date: reservation.check_out_date,
        hotel_name: reservation.hotel.name)
    }

    # TODO: add the invoice screen

    mail_with_mandrill('reservation-invoice', email, template_vars)
  end
end
