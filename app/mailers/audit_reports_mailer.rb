# frozen_string_literal: true

class AuditReportsMailer < BaseMandrillMailer
  def audit_report(email:, audit:, report_title:)
    template_vars = {
      subject: report_title,
      message_first: I18n.t('audit_reports_mailer.audit_report.message_first',
        email: email,
        start_date: audit.time_frame_begin.to_date,
        end_date: audit.time_frame_end.to_date),
      message_second: I18n.t('audit_reports_mailer.audit_report.message_second'),
      link_text:      I18n.t('audit_reports_mailer.audit_report.link_text'),
      audit_report:   (Rails.env.development? ? '' : 'https:') + audit.pdf_report.file.url
    }

    mail_with_mandrill('audit-report', email, template_vars)
  end
end
