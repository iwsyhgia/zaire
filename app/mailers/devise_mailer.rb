# frozen_string_literal: true

class DeviseMailer < BaseMandrillMailer
  include ::Devise::Mailers::Helpers
  include ::Devise::Controllers::UrlHelpers

  def confirmation_instructions(record, token, _opts = {})
    template_vars = {
      confirm_url: user_confirmation_url(confirmation_token: token, locale: I18n.locale),
      subject: I18n.t('devise_mailer.confirmation_instructions.subject'),
      message: I18n.t('devise_mailer.confirmation_instructions.message'),
      link_text: I18n.t('devise_mailer.confirmation_instructions.link-text')
    }

    mail_with_mandrill('account-confirm-email-user', record.email, template_vars)
  end

  def reset_password_instructions(record, token, _opts = {})
    template_vars = {
      proceed_url: edit_user_password_url(reset_password_token: token, locale: I18n.locale),
      subject: I18n.t('devise_mailer.reset_password_instructions.subject'),
      message_first: I18n.t('devise_mailer.reset_password_instructions.message-first', email: record.email),
      message_second: I18n.t('devise_mailer.reset_password_instructions.message-second'),
      link_text: I18n.t('devise_mailer.reset_password_instructions.link-text')
    }

    mail_with_mandrill('account-change-password-user', record.email, template_vars)
  end
end
