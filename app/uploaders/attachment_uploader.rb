# frozen_string_literal: true

class AttachmentUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  IMAGE_EXTENSIONS    = %w[jpg jpeg gif png tiff].freeze
  DOCUMENT_EXTENSIONS = %w[doc docm docx rtf xls xlsx ppt pptx pps ppsx pot txt xml].freeze
  PDF_EXTENSIONS      = %w[pdf].freeze

  #  Processes the file in case of an image
  process resize_to_limit: [1024, 1024], if: :image?

  version :thumb, if: :previewable? do
    # process :cover, if: :pdf?
    # process convert: :png, if: :pdf?

    #  Considers different extension for the preview file properly
    #
    def full_filename(for_file = model.file)
      filename = super
      extension = File.extname(filename)

      return "#{filename.chomp(extension)}.png" if extension.include?('pdf')

      filename
    end
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    IMAGE_EXTENSIONS + DOCUMENT_EXTENSIONS + PDF_EXTENSIONS
  end

  #  Redefines default rmagick behaviour and takes only the first page of the PDF document
  #
  def cover
    manipulate! do |frame, index, _options|
      if index.zero?
        frame.border!(0, 0, 'white')
        frame.alpha Magick::DeactivateAlphaChannel
        frame
      end
    end
  end

  protected

  def previewable?(for_file)
    pdf?(for_file) || image?(for_file)
  end

  def pdf?(for_file)
    extension = File.extname(for_file.try(:filename) || original_filename || '').downcase.split('.').last
    PDF_EXTENSIONS.include?(extension)
  end

  def image?(for_file)
    extension = File.extname(for_file.try(:filename) || original_filename || '').downcase.split('.').last
    IMAGE_EXTENSIONS.include?(extension)
  end
end
