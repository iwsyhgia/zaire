# frozen_string_literal: true

class ImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  # storage :fog
  process :compress_zopflipng, if: :need_optimization?

  version :medium do
    process resize_to_fit: [nil, 300], if: :cover_image?
    process resize_to_fit: [nil, 180], if: :logo?
    process :compress_zopflipng, if: :need_optimization?
  end

  version :small do
    process resize_to_fit: [nil, 150], if: :cover_image?
    process resize_to_fit: [nil, 100], if: :logo?
    process :compress_zopflipng, if: :need_optimization?
  end

  def need_optimization?(new_file)
    png?(new_file) && model.try(:need_to_optimize)
  end

  def compress_zopflipng
    system("zopflipng -y '#{current_path}' '#{current_path}'")
  end

  def png?(for_file)
    extension = File.extname(for_file.try(:filename) || original_filename || '').downcase.split('.').last
    extension == 'png'
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  private

  def cover_image?(_picture)
    model.tag == 'cover_image'
  end

  def logo?(_picture)
    model.tag == 'logo'
  end
end
