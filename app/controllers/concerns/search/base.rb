# frozen_string_literal: true

module Search
  module Base
    extend ActiveSupport::Concern

    protected

    def default_ransack_params
      raise 'Please override this method'
    end

    def query_params
      @query_params ||= begin
        request_params = params[:q] ? validated_search_params : {}
        default_ransack_params.with_indifferent_access.merge(request_params)
      end
    end

    def validated_search_params
      raise 'Please override this method'
    end

    def search_params
      raise 'Please override this method'
    end
  end
end
