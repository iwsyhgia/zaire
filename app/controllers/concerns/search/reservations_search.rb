# frozen_string_literal: true

module Search
  module ReservationsSearch
    extend ActiveSupport::Concern
    include Search::Base

    private

    def default_ransack_params
      {
        s: 'created_at DESC',
        status_in: ::Reservations::Reservation.statuses.except(:cancelled).keys + ::Reservations::DNR.statuses.keys
      }
    end

    def search_reservations
      @q = @reservations.ransack(query_params)

      from_date = query_params[:from_date] ? Date.parse(query_params[:from_date]) : Date.current - 15.days
      to_date   = query_params[:to_date] ? Date.parse(query_params[:to_date]) : Date.current + 15.days

      condition = '(check_in_date, check_out_date) OVERLAPS (?, ?)'
      @reservations = @q.result.where(condition, from_date - 1.day, to_date + 1.day)
    end

    def validated_search_params
      ::Validators::Reservations::SearchParamsValidator.new(search_params).to_hash
    end

    def search_params
      keys = %i[
        from_date
        to_date
        status_in
      ]
      params.require(:q).permit(*keys)
    end
  end
end
