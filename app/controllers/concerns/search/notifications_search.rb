# frozen_string_literal: true

module Search
  module NotificationsSearch
    extend ActiveSupport::Concern
    include Search::Base

    private

    def default_ransack_params
      {
        s: 'created_at DESC',
        kind_in: ::Notifications::Notification.kinds.keys
      }
    end

    def search_notifications(notifications)
      notifications.ransack(validated_search_params).result
    end

    def validated_search_params
      validated_search_params = ::Validators::Notifications::SearchValidator.new(search_params).to_hash
      {
        kind_in: validated_search_params[:kind_in],
        read_at_not_null: validated_search_params[:is_read],
        recipient_type_eq: validated_search_params[:recipient_type] || 'User',
        recipient_id_eq: validated_search_params[:recipient_id] || current_user.id
      }.compact
    end

    def search_params
      params.fetch(:q, {}).permit(:kind_in, :is_read, :recipient_type, :recipient_id)
    end
  end
end
