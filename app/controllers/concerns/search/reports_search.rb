# frozen_string_literal: true

module Search
  module ReportsSearch
    extend ActiveSupport::Concern
    include Search::Base

    private

    def default_ransack_params
      {
        s: 'created_at DESC',
        source_in: ['direct']
      }
    end

    def search(reservations)
      reservations.ransack(query_params).result
    end

    def validated_search_params
      ::Validators::Reports::SearchParamsValidator.new(search_params).to_hash
    end

    def search_params
      keys = [
        :room_type_name_eq,
        :check_in_date_gteq,
        :check_out_date_lteq,
        status_in: [],
        source_in: []
      ]
      params.fetch(:q, {}).permit(*keys)
    end
  end
end
