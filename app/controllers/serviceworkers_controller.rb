# frozen_string_literal: true

class ServiceworkersController < ApplicationController
  respond_to :js

  def index
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate max-age=0'
    response.headers['Content-Type']  = 'application/javascript'

    respond_to do |format|
      format.js { render 'index', layout: false }
    end
  end
end
