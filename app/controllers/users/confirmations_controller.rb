# frozen_string_literal: true

module Users
  class ConfirmationsController < Devise::ConfirmationsController
    def create
      resource_class.find_by(resource_params.permit(:email))&.update!(confirmation_token: nil)
      super
    end

    def show
      self.resource = resource_class.confirm_by_token(params[:confirmation_token])
      yield resource if block_given?

      if resource.errors.empty?
        WelcomeMailer.welcome_after_sign_up(resource)&.deliver_later
        sign_in(resource)
        respond_with_navigational(resource) { redirect_to after_confirmation_path_for(resource_name, resource) }
      else
        respond_with_navigational(resource.errors, status: :unprocessable_entity) { render :new }
      end
    end
  end
end
