# frozen_string_literal: true

class ApplicationController < ActionController::Base
  helper Webpacker::Helper

  helper_method :current_hotel

  before_action :authenticate_user!
  before_action :set_locale
  before_action :remember_current_controller

  def set_locale
    locale = request.env['HTTP_X_LANGUAGE'] if %w[en ar].include?(request.env['HTTP_X_LANGUAGE'])
    locale ||= params[:locale] || I18n.default_locale

    I18n.locale = locale
  end

  def self.default_url_options(options = {})
    options.merge(locale: I18n.locale)
  end

  # Each user will be connected to one hotel. for now we consider only hoteliers
  # TODO: later don't forget to update it for other roles (get it from memberships)
  def current_hotel
    @current_hotel = current_user&.hotel
  end

  #  Stores current controller to be accessible in models, etc later
  #
  def remember_current_controller
    ::Services::CurrentControllerService.set(self)
  end

  def after_sign_in_path_for(_resource)
    root_path(locale: params[:locale])
  end
end
