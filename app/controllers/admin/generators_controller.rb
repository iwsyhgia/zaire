# frozen_string_literal: true

module Admin
  class GeneratorsController < Admin::BaseController
    before_action :init_data

    def create_reservation
      result = ::Services::Generators::DirectReservationGenerator.call(
        params: params.permit![:reservations_reservation].merge(hotel_id: current_hotel.id)
      )

      if result.success?
        flash[:notice] = 'A new reservation has been successfully created.'
        redirect_to admin_reservation_path(result.value)
      else
        flash[:alert] = result.value
        render :show
      end
    end

    def create_reservations
      result = ::Services::Generators::ReservationsGenerator.call(
        params: params.permit![:reservations].merge(hotel_id: current_hotel.id)
      )

      if result.success?
        flash[:notice] = 'A new reservation has been successfully created.'
        redirect_to admin_reservations_path
      else
        flash[:alert] = result.value
        render :show
      end
    end

    def create_hotel
      result = ::Services::Generators::HotelGenerator.call(params: params)

      if result.success?
        flash[:notice] = 'Audit has been successfully finished'
        redirect_to admin_hotel_path(result.value)
      else
        @user = User.new(email: params[:user][:email])
        flash[:alert] = result.value
        render :show
      end
    end

    private

    def init_data
      @reservation = ::Reservations::Reservation.new
      @user        = User.new
    end
  end
end
