# frozen_string_literal: true

module Admin
  class UsersController < Admin::BaseController
    before_action :fetch_user, except: :index

    def index
      @q = User.order('users.created_at DESC').ransack(params[:q])
      @users = @q.result.includes(:hotel).page(params[:page]).per(10)
    end

    def show; end

    def destroy
      @user.destroy
      redirect_to action: :index
    end

    private

    def fetch_user
      @audit = User.find(params[:id])
    end
  end
end
