# frozen_string_literal: true

module Admin
  class ReservationsController < Admin::BaseController
    before_action :fetch_reservation, except: :index

    def index
      @q = ::Reservations::Reservation.order('reservations.created_at DESC').ransack(params[:q])
      @reservations = @q.result.includes(:hotel, :guest).page(params[:page]).per(10)
    end

    def show; end

    def destroy
      @reservation.destroy
      redirect_to action: :index
    end

    private

    def fetch_reservation
      @reservation = ::Reservations::Reservation.find(params[:id])
    end
  end
end
