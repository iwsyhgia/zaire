# frozen_string_literal: true

module Admin
  class BaseController < ApplicationController
    layout 'admin'

    before_action :authenticate_admin!

    private

    def authenticate_admin!
      raise Pundit::NotAuthorizedError unless current_user
    end
  end
end
