# frozen_string_literal: true

module Admin
  class HotelsController < Admin::BaseController
    before_action :fetch_hotel, except: :index

    def index
      @q = ::Hotels::Hotel.order('hotels.created_at DESC').ransack(params[:q])
      @hotels = @q.result.includes(:user).page(params[:page]).per(10)
    end

    def show; end

    def destroy
      @hotel.destroy
      redirect_to action: :index
    end

    private

    def fetch_hotel
      @hotel = ::Hotels::Hotel.find(params[:id])
    end
  end
end
