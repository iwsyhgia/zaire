# frozen_string_literal: true

module Admin
  class AuditsController < Admin::BaseController
    before_action :fetch_audit, except: :index

    def index
      @q = ::Audits::Audit.order('audits.created_at DESC').ransack(params[:q])
      @audits = @q.result.page(params[:page]).per(10)
    end

    def show; end

    # runs audit without confirmations
    def force_run
      result = ::Services::Audits::Perform.call(audit: @audit)

      flash[:notice] = 'Audit has been successfully finished' if result&.success?
      flash[:alert] = 'Audit has been failed' unless result&.success?

      redirect_to admin_audit_path(@audit)
    end

    # runs audit with confrimations
    def initiate
      result = ::Services::Audits::Initiate.call(audit: @audit)

      flash[:notice] = 'Audit has been successfully finished' if result&.success?
      flash[:alert] = 'Audit has been failed' unless result&.success?

      redirect_to admin_audit_path(@audit)
    end

    def destroy
      @audit.destroy
      redirect_to action: :index
    end

    private

    def fetch_audit
      @audit = ::Audits::Audit.find(params[:id])
    end
  end
end
