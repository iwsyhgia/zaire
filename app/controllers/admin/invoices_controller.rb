# frozen_string_literal: true

module Admin
  class InvoicesController < Admin::BaseController
    before_action :fetch_invoice, except: :index

    def index
      @q = ::Invoices::Invoice.order('invoices.created_at DESC').ransack(params[:q])
      @invoices = @q.result.page(params[:page]).per(10)
    end

    def show; end

    def destroy
      @invoice.destroy
      redirect_to action: :index
    end

    private

    def fetch_invoice
      @invoice = ::Invoices::Invoice.find(params[:id])
    end
  end
end
