# frozen_string_literal: true

module Api
  module V1
    module Reservations
      class ReservationsController < ApiController
        include Search::ReservationsSearch

        ReservationSerializer = ::Serializers::Reservations::ReservationSerializer

        before_action :fetch_reservation, except: %i[index create]

        def index
          @reservations = current_hotel.reservations
          search_reservations
          success(response: serialize(@reservations, ReservationSerializer))
        end

        def create
          @reservation = authorize(::Reservations::Reservation.new(validated_create_params))
          ::Services::Reservations::CreateReservation.call(reservation: @reservation)
          success(status: 201, response: serialize(@reservation, ReservationSerializer))
        end

        def update
          authorize(@reservation).assign_attributes(validated_params)
          ::Services::Reservations::UpdateReservation.call(reservation: @reservation)
          success(response: serialize(@reservation, ReservationSerializer))
        end

        def destroy
          @reservation.destroy!
          success(response: { id: @reservation.id })
        end

        def show
          success(response: serialize(@reservation, ReservationSerializer))
        end

        def cancel
          handle_service(
            ::Services::Reservations::Statuses::CancelService.new(
              reservation: @reservation,
              cancellation_fee: cancel_validated_params[:cancellation_fee]
            )
          )
        end

        def check_out
          handle_service(::Services::Reservations::Statuses::CheckOutService.new(reservation: @reservation))
        end

        def check_in
          @reservation.assign_attributes(validated_update_params)
          handle_service(::Services::Reservations::Statuses::CheckInService.new(reservation: @reservation))
        end

        private

        def validated_params
          @reservation.unconfirmed? ? validated_create_params : validated_update_params
        end

        def validated_create_params
          ::Validators::Reservations::ReservationValidator.new(reservation_params, @reservation).to_hash
        end

        def validated_update_params
          reservation_params = params.require(:reservation)
          ::Validators::Reservations::UpdateReservationValidator.new(reservation_params, @reservation).to_hash
        end

        def fetch_reservation
          @reservation = policy_scope(::Reservations::Reservation).find(params[:id])
        end

        def reservation_params
          keys = [
            :id,
            :check_in_date,
            :check_out_date,
            :status,
            :number_of_adults,
            :number_of_children,
            :room_id,
            :guest_id,
            :rate,
            :payment_kind,
            guest_attributes: %i[
              id
              title
              first_name
              last_name
              email
              birth_date
              phone_code
              phone_number
              country
              city
            ]
          ]
          attrs = params.require(:reservation).permit(*keys).merge!(type: 'Reservations::DirectReservation')
          attrs[:guest_attributes][:hotel_id] = current_hotel.id if attrs[:guest_attributes]
          attrs
        end

        def cancel_params
          keys = %i[cancellation_fee]
          params.require(:reservation).permit(*keys)
        end

        def cancel_validated_params
          ::Validators::Reservations::CancelReservationValidator.new(cancel_params).to_hash
        end
      end
    end
  end
end
