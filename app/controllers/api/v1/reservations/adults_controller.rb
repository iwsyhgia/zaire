# frozen_string_literal: true

module Api
  module V1
    module Reservations
      class AdultsController < ApiController
        AdultSerializer = ::Serializers::Reservations::AdultSerializer

        before_action :set_adult, except: :create

        def create
          @adult = ::Reservations::Adult.new
          @adult.assign_attributes(validated_params)
          authorize @adult
          @adult.save!
          success(status: 201, response: serialize(@adult, AdultSerializer))
        end

        def update
          @adult.update!(validated_params)
          success(response: serialize(@adult, AdultSerializer))
        end

        def destroy
          @adult.destroy!
          success(response: { id: @adult.id })
        end

        def show
          success(response: serialize(@adult, AdultSerializer))
        end

        private

        def validated_params
          ::Validators::Reservations::AdultValidator.new(adult_params, @adult).to_hash
        end

        def set_adult
          @adult ||= ::Reservations::Adult.find(params[:id])
          authorize @adult
        end

        def adult_params
          keys = %i[
            id
            reservation_id
            title
            first_name
            last_name
            personal_id
          ]
          params.require(:adult).permit(*keys)
        end
      end
    end
  end
end
