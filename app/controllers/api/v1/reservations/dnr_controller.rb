# frozen_string_literal: true

module Api
  module V1
    module Reservations
      class DNRController < ApiController
        DNRSerializer = ::Serializers::Reservations::DNRSerializer
        ReservationSerializer = ::Serializers::Reservations::ReservationSerializer

        before_action :fetch_dnr, except: %i[index create]

        def index
          @dnr_records = policy_scope(::Reservations::DNR)
          success(response: serialize(@dnr_records, DNRSerializer))
        end

        def create
          @dnr = ::Reservations::DNR.new(validated_params)
          authorize @dnr
          @dnr.transaction do
            @moved_reservations = ::Services::Reservations::Move.call(dnr: @dnr).value
            @dnr.save!
          end
          success(response: response_with_moved_reservations)
        end

        def update
          authorize @dnr
          @dnr.assign_attributes(validated_params)
          @dnr.transaction do
            @moved_reservations = ::Services::Reservations::Move.call(dnr: @dnr).value
            @dnr.save!
          end
          success(response: response_with_moved_reservations)
        end

        def destroy
          authorize @dnr
          @dnr.transaction do
            @result = ::Services::Reservations::DestroyDNR.call(dnr: @dnr).value
          end
          success(response: { dnr: @result })
        end

        def show
          authorize @dnr
          success(response: serialize(@dnr, DNRSerializer))
        end

        private

        def response_with_moved_reservations
          { dnr: serialize(@dnr, DNRSerializer), reservations: serialize(@moved_reservations, ReservationSerializer) }
        end

        def validated_params
          ::Validators::Reservations::DNRValidator.new(dnr_params, @dnr).to_hash
        end

        def fetch_dnr
          @dnr = ::Reservations::DNR.find(params[:id])
        end

        def dnr_params
          keys = %i[id check_in_date check_out_date room_id description type]
          params.require(:dnr).permit(*keys).merge!(type: 'Reservations::DNR')
        end
      end
    end
  end
end
