# frozen_string_literal: true

module Api
  module V1
    module Audits
      class AuditsController < ApiController
        AuditSerializer = ::Serializers::Audits::CurrentAuditSerializer

        before_action :fetch_audit, only: %i[show]

        def show
          success(response: serialize(authorize(@audit), AuditSerializer))
        end

        def last
          success(response: serialize(audits.last, AuditSerializer))
        end

        def last_completed
          success(response: serialize(audits.completed.order(time_frame_begin: :desc).first, AuditSerializer))
        end

        private

        def audits
          policy_scope(::Audits::Audit)
        end

        def fetch_audit
          @audit = audits.find(params[:id])
        end
      end
    end
  end
end
