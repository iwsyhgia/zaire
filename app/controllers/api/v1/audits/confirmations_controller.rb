# frozen_string_literal: true

module Api
  module V1
    module Audits
      class ConfirmationsController < ApiController
        ConfirmationSerializer = ::Serializers::Audits::ConfirmationSerializer

        before_action :fetch_confirmation, only: [:confirm]

        def index
          success(response: serialize(policy_scope(::Audits::Confirmation).pending, ConfirmationSerializer))
        end

        def confirm
          authorize @confirmation
          success(response: serialize(
            ::Services::Audits::Confirm.call(confirmation: @confirmation).value,
            ConfirmationSerializer
          ))
        end

        private

        def fetch_confirmation
          @confirmation = ::Audits::Confirmation.find(params[:id])
        end
      end
    end
  end
end
