# frozen_string_literal: true

module Api
  module V1
    module Rooms
      class AmenitiesController < ApiController
        AmenitySerializer = ::Serializers::Rooms::AmenitySerializer

        before_action :set_amenity, except: %i[index]

        def index
          @amenities = policy_scope(::Rooms::Amenity)
          success(response: serialize(@amenities, AmenitySerializer))
        end

        def create
          @amenity.save!
          success(status: 201, response: serialize(@amenity, AmenitySerializer))
        end

        def update
          @amenity.update!(validated_params)
          success(response: serialize(@amenity, AmenitySerializer))
        end

        def destroy
          @amenity.amenities_room_types.where(room_type_id: params[:room_type_id]).destroy_all if params[:room_type_id]
          if @amenity.reload.amenities_room_types.empty?
            @amenity.destroy!
            success(response: { id: @amenity.id })
          else
            error(status: 400, message: I18n.t('.amenities.errors.cannot_be_destroyed'))
          end
        end

        def show
          success(response: serialize(@amenity, AmenitySerializer))
        end

        private

        def set_amenity
          @amenity = ::Rooms::Amenity.new(validated_params) if action_name == 'create'
          @amenity ||= ::Rooms::Amenity.find(params[:id])

          authorize @amenity
        end

        def validated_params
          ::Validators::Rooms::AmenityValidator.new(amenity_params, @amenity).to_hash
        end

        def amenity_params
          keys = %i[name name_ar]
          amenity_params = params.require(:amenity).permit(*keys)
          return amenity_params.merge(hotel_id: current_hotel&.id) if params[:action] == 'create'

          amenity_params
        end
      end
    end
  end
end
