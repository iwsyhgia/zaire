# frozen_string_literal: true

module Api
  module V1
    module Rooms
      class RoomTypesController < ApiController
        RoomTypeSerializer = ::Serializers::Rooms::RoomTypeSerializer

        before_action :set_room_type, except: %i[index]

        def index
          @room_types = policy_scope(::Rooms::RoomType)
          success(response: serialize(@room_types, RoomTypeSerializer))
        end

        def create
          @room_type.save!
          ::Services::Rates::MapToStandardPeriod.call(room_type: @room_type)
          success(status: 201, response: serialize(@room_type, RoomTypeSerializer))
        end

        def update
          @room_type.update!(validated_params)
          ::Services::Rates::MapToStandardPeriod.call(room_type: @room_type)
          success(response: serialize(@room_type, RoomTypeSerializer))
        end

        def destroy
          ::Validators::Rooms::DestroyRoomTypeValidator.new(room_type_params, @room_type).to_hash
          success(response: { id: @room_type.id })
        end

        def show
          success(response: serialize(@room_type, RoomTypeSerializer))
        end

        private

        def validated_params
          validator = ::Validators::Rooms::RoomTypeValidator
          validator.new(room_type_params, @room_type).to_hash
        end

        def set_room_type
          @room_type = ::Rooms::RoomType.new(validated_params) if action_name == 'create'
          @room_type ||= ::Rooms::RoomType.find(params[:id])
          authorize @room_type
        end

        def room_type_params
          keys = [
            :name,
            :name_ar,
            :size,
            :unit,
            :max_occupancy,
            :quantity,
            room_ids: [],
            image_ids: [],
            amenity_ids: [],
            images_attributes: %i[id base64 _destroy],
            rooms_attributes: %i[id number _destroy],
            rate_attributes: %i[id lower_bound upper_bound kind]
          ]
          params.fetch(:room_type, {}).permit(*keys).merge(hotel_id: current_hotel.id)
        end
      end
    end
  end
end
