# frozen_string_literal: true

module Api
  module V1
    module Rooms
      class AvailableRoomsController < ApiController
        def index
          @rooms = policy_scope(::Rooms::Room).
                   available_for(*search_params).
                   includes(:room_type).
                   order('rooms.room_type_id')

          success(response: serialize(@rooms, ::Serializers::Rooms::RoomSerializer))
        end

        private

        def search_params
          ::Validators::Rooms::AvailableRoomsSearchParamsValidator.new(params.permit(:check_in_date, :check_out_date)).
            to_hash.values
        end
      end
    end
  end
end
