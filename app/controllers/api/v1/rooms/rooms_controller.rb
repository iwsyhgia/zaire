# frozen_string_literal: true

module Api
  module V1
    module Rooms
      class RoomsController < ApiController
        RoomSerializer = ::Serializers::Rooms::RoomSerializer

        before_action :set_room, except: %i[index]

        def index
          @rooms = policy_scope(::Rooms::Room)
          success(response: serialize(@rooms, RoomSerializer))
        end

        def create
          @room.save!
          success(status: 201, response: { id: @room.id })
        end

        def update
          @room.update!(validated_params)
          success(response: serialize(@room, RoomSerializer))
        end

        def destroy
          @room.destroy!
          success(response: { id: @room.id })
        end

        def show
          success(response: serialize(@room, RoomSerializer))
        end

        private

        def validated_params
          ::Validators::Rooms::RoomValidator.new(room_params, @room).to_hash
        end

        def set_room
          @room = ::Rooms::Room.new(validated_params) if action_name == 'create'
          @room ||= ::Rooms::Room.find(params[:id])
          authorize @room
        end

        def room_params
          keys = %i[number room_type_id]
          params.require(:room).permit(*keys)
        end
      end
    end
  end
end
