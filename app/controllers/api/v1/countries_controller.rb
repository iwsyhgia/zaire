# frozen_string_literal: true

module Api
  module V1
    class CountriesController < ApiController
      CountrySerializer = ::Serializers::Countries::CountrySerializer

      before_action :set_country, only: %i[show]

      def index
        @countries = policy_scope(ISO3166::Country)
        success(response: serialize(@countries, CountrySerializer))
      end

      def show
        success(response: serialize(@country, CountrySerializer))
      end

      private

      def set_country
        @country = ISO3166::Country.find_country_by_alpha3(validated_params[:alpha3])
        authorize @country

        raise ActiveRecord::RecordNotFound unless @country
      end

      def validated_params
        ::Validators::Countries::CountryValidator.new(country_params).to_hash
      end

      def country_params
        params.permit(:alpha3)
      end
    end
  end
end
