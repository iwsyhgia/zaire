# frozen_string_literal: true

module Api
  module V1
    module Guests
      class GuestsController < ApiController
        GuestSerializer = ::Serializers::Guests::GuestSerializer

        def index
          result = policy_scope(::Guests::Guest).ransack(validated_params).result
          success(response: serialize(result, GuestSerializer))
        end

        private

        def validated_params
          ::Validators::Guests::SearchValidator.new(search_params).to_hash
        end

        def search_params
          keys = [
            :m,
            :email_cont,
            :phone_number_cont,
            g: %i[first_name_cont last_name_cont]
          ]
          params[:q]&.merge!(m: 'or')
          params.fetch(:q, {}).permit(*keys)
        end
      end
    end
  end
end
