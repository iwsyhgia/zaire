module Api
  module V1
    module Rates
      class RatesController < ApiController
        def index
          from_date = params[:from_date] ? Date.parse(params[:from_date]) : Date.current - 15.days
          to_date   = params[:to_date] ? Date.parse(params[:to_date]) : Date.current + 15.days

          success(response: ::Services::Rates::DetermineRatesForDates.call(start_date: from_date, end_date: to_date))
        end
      end
    end
  end
end
