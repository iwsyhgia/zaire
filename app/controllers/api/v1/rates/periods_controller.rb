module Api
  module V1
    module Rates
      class PeriodsController < ApiController
        PeriodSerializer = ::Serializers::Rates::PeriodSerializer

        before_action :fetch_period, only: %i[destroy update show]

        def index
          success(response: serialize(periods, PeriodSerializer))
        end

        def standard
          success(response: serialize(periods.standard.first, PeriodSerializer))
        end

        def show
          success(response: serialize(authorize(@period), PeriodSerializer))
        end

        def update
          @period.transaction do
            @period.assign_attributes(validated_params)
            ::Services::Rates::DeactivateRoomTypesRates.call(period: @period)
            authorize(@period).save!
          end
          success(response: serialize(@period, PeriodSerializer))
        end

        def create
          @period = ::Rates::Period.new(validated_params)
          authorize(@period).save!
          success(status: 201, response: serialize(@period, PeriodSerializer))
        end

        def destroy
          period_validator.validate!
          authorize(@period).destroy!
          success(response: { id: @period.id })
        end

        private

        def periods
          @periods ||= policy_scope(::Rates::Period).joins(periods_room_types: :room_type).distinct
        end

        def fetch_period
          @period = periods.find(params[:id])
        end

        def validated_params
          default_params = { hotel_id: current_hotel.id }
          period_validator.to_hash.merge!(default_params)
        end

        def period_validator
          ::Validators::Rates::PeriodValidator.new(permitted_params, @period, action_name.to_sym)
        end

        def permitted_params
          Schemas::Rates::Period.call!(params).to_h
        end
      end
    end
  end
end
