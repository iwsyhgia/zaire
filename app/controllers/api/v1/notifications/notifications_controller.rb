# frozen_string_literal: true

module Api
  module V1
    module Notifications
      class NotificationsController < ApiController
        include ::Notifications::NotificationsHelper
        include ::Search::NotificationsSearch

        NotificationSerializer = ::Serializers::Notifications::NotificationSerializer

        def index
          filtered_notifications = search_notifications(notifications)
          success(response: serialize(filtered_notifications, NotificationSerializer))
        end

        def show
          notification = notifications.find(params[:id])
          success(response: serialize(notification, NotificationSerializer))
        end

        def create
          notification = ::Services::Notifications::Send.call(
            validated_create_params.slice(
              :recipients,
              :kind,
              :notifiers
            ).merge(message_params: { reservation_id: validated_create_params[:reservation_id] })
          )
          success(status: 201)
        end

        def preview
          success(response: { notification_text: notification_text })
        end

        def read
          notification = notifications.find(params[:id])
          ::Services::Notifications::Read.call(notification: notification)
          success(response: serialize(notification, NotificationSerializer))
        end

        private

        def notifications
          policy_scope(::Notifications::Notification)
        end

        def validated_create_params
          validated_create_params = ::Validators::Notifications::NotificationValidator.new(create_params).to_hash
          {
            kind: validated_create_params[:kind],
            recipients: [::Guests::Guest.find(validated_create_params[:recipient_id])],
            reservation_id: validated_create_params[:reservation_id],
            notifiers: [validated_create_params[:notifier]]
          }.compact
        end

        def create_params
          keys = %i[
            kind
            recipient_id
            reservation_id
            notifier
          ]
          params.require(:notification).permit(*keys)
        end
      end
    end
  end
end
