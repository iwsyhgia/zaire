# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class PaymentsController < BaseController
        before_action :set_payment, only: %i[update destroy show]

        def index
          authorize ::Invoices::Payment
          success(response: serialize(policy_scope(@invoice.payments), ::Serializers::Invoices::PaymentSerializer))
        end

        def create
          payment = @invoice.payments.new(validated_params)
          authorize payment
          @invoice.transaction do
            payment.save!
            ::Services::Invoices::RecalculateInvoice.call(statement: payment)
            ::Services::Reservations::Statuses::DetermineStatus.call(reservation: reservation)
          end
          success(status: 201, response: ::Serializers::Invoices::PaymentSerializer.new(payment))
        end

        def update
          authorize @payment
          @payment.update!(validated_params)
          ::Services::Invoices::RecalculateInvoice.call(statement: @payment)
          ::Services::Reservations::Statuses::DetermineStatus.call(reservation: reservation)
          success(response: ::Serializers::Invoices::PaymentSerializer.new(@payment))
        end

        def destroy
          authorize @payment
          @payment.destroy!
          ::Services::Invoices::RecalculateInvoice.call(statement: @payment)
          ::Services::Reservations::Statuses::DetermineStatus.call(reservation: reservation)
          success(response: ::Serializers::Invoices::PaymentSerializer.new(@payment))
        end

        def show
          authorize @payment
          success(response: serialize(@payment, ::Serializers::Invoices::PaymentSerializer))
        end

        private

        def reservation
          @reservation ||= @invoice.reservation
        end

        def set_payment
          @payment = @invoice.payments.find(params[:id])
        end

        def validated_params
          @validated_params ||= ::Validators::Invoices::PaymentValidator.new(payment_params, @payment).to_hash
        end

        def payment_params
          default_values = { user_id: current_user.id, creation_way: 'manual' }
          keys = %i[id amount user_id creation_way payment_method]
          params.require(:payment).permit(*keys).merge!(default_values)
        end
      end
    end
  end
end
