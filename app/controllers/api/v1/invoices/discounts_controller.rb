# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class DiscountsController < BaseController
        before_action :set_discount, only: %i[update destroy show]

        def create
          discount = @invoice.discounts.new(validated_params)
          authorize discount

          @invoice.transaction do
            discount.save!
            result = ::Services::Invoices::RecalculateInvoice.call(statement: discount)

            if result.success?
              ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
              success(status: 201, response: ::Serializers::Invoices::DiscountSerializer.new(discount))
            else
              error(message: result.value)
              raise ActiveRecord::Rollback
            end
          end
        end

        def update
          authorize @discount

          @invoice.transaction do
            @discount.update!(validated_params)
            result = ::Services::Invoices::RecalculateInvoice.call(statement: @discount)

            if result.success?
              ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
              success(response: ::Serializers::Invoices::DiscountSerializer.new(@discount))
            else
              error(message: result.value)
              raise ActiveRecord::Rollback
            end
          end
        end

        def destroy
          authorize @discount

          @invoice.transaction do
            @discount.destroy!
            result = ::Services::Invoices::RecalculateInvoice.call(statement: @discount)

            if result.success?
              ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
              success(response: ::Serializers::Invoices::DiscountSerializer.new(@discount))
            else
              error(message: result.value)
              raise ActiveRecord::Rollback
            end
          end
        end

        def show
          authorize @discount
          success(response: serialize(@discount, ::Serializers::Invoices::DiscountSerializer))
        end

        private

        def set_discount
          @discount = @invoice.discounts.find(params[:id])
        end

        def validated_params
          @validated_params ||=
            ::Validators::Invoices::DiscountValidator.new(discount_params, @discount).to_hash
        end

        def discount_params
          default_values = { user_id: current_user.id, creation_way: 'manual' }
          keys = %i[id value kind description creation_way]
          params.require(:discount).permit(*keys).merge!(default_values)
        end
      end
    end
  end
end
