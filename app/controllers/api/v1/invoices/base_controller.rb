# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class BaseController < ApiController
        before_action :fetch_invoice

        private

        def fetch_invoice
          @invoice = policy_scope(::Invoices::Invoice).find(params[:invoice_id])
        end
      end
    end
  end
end
