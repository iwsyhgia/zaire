# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class ChargesController < BaseController
        before_action :set_charge, only: %i[update destroy show]
        before_action -> { authorize @invoice }, except: %i[index]

        def index
          success(response: serialize(@invoice.charges, ::Serializers::Invoices::ChargeSerializer))
        end

        def create
          charge = @invoice.charges.create!(validated_params)
          ::Services::Invoices::RecalculateInvoice.call(statement: charge)
          success(status: 201, response: ::Serializers::Invoices::ChargeSerializer.new(charge))
        end

        def update
          @charge.update!(validated_params)
          ::Services::Invoices::RecalculateInvoice.call(statement: @charge)
          success(response: ::Serializers::Invoices::ChargeSerializer.new(@charge))
        end

        def destroy
          @charge.destroy!
          ::Services::Invoices::RecalculateInvoice.call(statement: @charge)
          success(response: { id: @charge.id })
        end

        def show
          success(response: serialize(@charge, ::Serializers::Invoices::ChargeSerializer))
        end

        private

        def set_charge
          @charge = @invoice.charges.find(params[:id])
        end

        def validated_params
          @validated_params ||= charge_params
        end

        def charge_params
          keys = %i[id amount description user_id kind creation_way]
          params.require(:charge).permit(*keys).merge!(user_id: current_user.id, kind: :sellable_items)
        end
      end
    end
  end
end
