# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class InvoicesController < ApiController
        def show
          @invoice = ::Invoices::Invoice.find(params[:id])
          authorize @invoice

          success(response: serialize(@invoice, ::Serializers::Invoices::InvoiceSerializer))
        end
      end
    end
  end
end
