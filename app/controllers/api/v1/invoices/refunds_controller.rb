# frozen_string_literal: true

module Api
  module V1
    module Invoices
      class RefundsController < BaseController
        before_action :set_refund, only: %i[update destroy show]

        def create
          refund = @invoice.refunds.new(validated_params)
          authorize refund
          refund.save!
          ::Services::Invoices::RecalculateInvoice.call(statement: refund)
          ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
          success(status: 201, response: ::Serializers::Invoices::RefundSerializer.new(refund))
        end

        def update
          authorize @refund
          @refund.update!(validated_params)
          ::Services::Invoices::RecalculateInvoice.call(statement: @refund)
          ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
          success(response: ::Serializers::Invoices::RefundSerializer.new(@refund))
        end

        def destroy
          authorize @refund
          @refund.destroy!
          ::Services::Invoices::RecalculateInvoice.call(statement: @refund)
          ::Services::Reservations::Statuses::DetermineStatus.call(reservation: @invoice.reservation)
          success(response: ::Serializers::Invoices::RefundSerializer.new(@refund))
        end

        def show
          authorize @refund
          success(response: serialize(@refund, ::Serializers::Invoices::RefundSerializer))
        end

        private

        def set_refund
          @refund = @invoice.refunds.find(params[:id])
        end

        def validated_params
          @validated_params ||=
            ::Validators::Invoices::RefundValidator.new(refund_params, @refund).to_hash
        end

        def refund_params
          default_values = { user_id: current_user.id, creation_way: 'manual' }
          keys = %i[id amount payment_id description creation_way]
          params.require(:refund).permit(*keys).merge!(default_values)
        end
      end
    end
  end
end
