# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class ConfigurationsController < ApiController
        before_action :fetch_configuration, only: %i[show update]
        before_action -> { authorize @configuration }

        def show
          success(response: serialize(@configuration, ::Serializers::Hotels::ConfigurationSerializer))
        end

        def update
          @configuration.update!(validated_params)
          success(response: serialize(@configuration, ::Serializers::Hotels::ConfigurationSerializer))
        end

        private

        def fetch_configuration
          @configuration = current_hotel.configuration
        end

        def validated_params
          ::Validators::Hotels::ConfigurationValidator.new(configuration_params).to_hash
        end

        def configuration_params
          keys = %i[night_audit_time night_audit_confirmation_required time_zone]
          params.require(:configuration).permit(*keys)
        end
      end
    end
  end
end
