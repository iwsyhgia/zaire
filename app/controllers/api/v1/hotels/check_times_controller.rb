# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class CheckTimesController < ApiController
        def show
          success(response: serialize(check_times, ::Serializers::Hotels::CheckTimeSerializer))
        end

        def update
          current_hotel.update!(validated_check_time_params)

          success(response: serialize(check_times, ::Serializers::Hotels::CheckTimeSerializer))
        end

        private

        def check_times
          [current_hotel.check_in_time, current_hotel.check_out_time]
        end

        def validated_check_time_params
          ::Validators::Hotels::CheckTimeValidator.new(check_time_params).to_hash
        end

        def check_time_params
          check_time_keys = %i[fixed check_type start_time end_time]
          keys = [
            check_in_time_attributes: [*check_time_keys],
            check_out_time_attributes: [*check_time_keys]
          ]

          params.require(:check_time).permit(*keys)
        end
      end
    end
  end
end
