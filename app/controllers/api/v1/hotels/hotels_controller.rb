# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class HotelsController < ApiController
        before_action -> { authorize current_hotel }

        def show
          success(response: serialize(current_hotel, ::Serializers::Hotels::HotelSerializer))
        end

        def update
          current_hotel.update!(validated_params)
          success(response: serialize(current_hotel, ::Serializers::Hotels::HotelSerializer))
        end

        private

        def validated_params
          ::Validators::Hotels::HotelValidator.new(hotel_params).to_hash
        end

        def hotel_params
          keys = %i[name name_ar]
          params.require(:hotel).permit(*keys)
        end
      end
    end
  end
end
