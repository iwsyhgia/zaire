# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class LogosController < ApiController
        before_action :fetch_logo, except: %i[index create]
        before_action -> { authorize current_hotel }

        def create
          current_hotel.logo = Image.create!(validated_params)

          success(status: 201, response: serialize(current_hotel.logo, ::Serializers::ImageSerializer))
        end

        def show
          success(response: serialize(@logo, ::Serializers::ImageSerializer))
        end

        def update
          @logo.update!(validated_params)

          success(response: serialize(@logo, ::Serializers::ImageSerializer))
        end

        def destroy
          @logo.destroy
          success(response: { id: @logo.id })
        end

        private

        def fetch_logo
          raise ActiveRecord::RecordNotFound if current_hotel.logo.nil?

          @logo = current_hotel.logo
        end

        def logo_params
          keys = %i[id base64]
          params.require(:logo).permit(*keys)
        end

        def validated_params
          ::Validators::ImageValidator.new(logo_params.merge(tag: 'logo', imageable: current_hotel)).to_hash
        end
      end
    end
  end
end
