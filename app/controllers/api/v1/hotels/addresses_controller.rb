# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class AddressesController < ApiController
        before_action :fetch_address, only: %i[show update]
        before_action -> { authorize @address }

        def show
          success(response: serialize(@address, ::Serializers::Hotels::AddressSerializer))
        end

        def update
          @address.update!(validated_params)
          success(response: serialize(@address, ::Serializers::Hotels::AddressSerializer))
        end

        private

        def fetch_address
          @address = current_hotel.address
        end

        def validated_params
          ::Validators::Hotels::AddressValidator.new(address_params).to_hash
        end

        def address_params
          keys = %i[country city state address_1 address_2 fax phone_number email]
          params.require(:address).permit(*keys)
        end
      end
    end
  end
end
