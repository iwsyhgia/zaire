# frozen_string_literal: true

module Api
  module V1
    module Hotels
      class OfflineModesController < ApiController
        before_action :set_offline_mode, only: %i[show update]

        def show
          success(response: serialize(@offline_mode, ::Serializers::Hotels::OfflineModeSerializer))
        end

        def update
          @offline_mode.update!(validated_params)
          success(response: serialize(@offline_mode, ::Serializers::Hotels::OfflineModeSerializer))
        end

        private

        def set_offline_mode
          @offline_mode = current_hotel.offline_mode
          authorize @offline_mode
        end

        def validated_params
          ::Validators::Hotels::OfflineModeValidator.new(offline_mode_params).to_hash
        end

        def offline_mode_params
          keys = %i[id active days_before_today days_after_today]
          params.require(:offline_mode).permit(*keys)
        end
      end
    end
  end
end
