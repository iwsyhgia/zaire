# frozen_string_literal: true

module Api
  module V1
    class DocsController < ApiController # rubocop:disable Metrics/ClassLength
      include Swagger::Blocks

      before_action -> { authorize :swagger }, only: %i[index]

      swagger_root do
        key :swagger, '2.0'
        info do
          key :version, '1.0.0'
          key :title, 'Swagger Zaire'
          key :description, 'A sample API of Hotel management'
          contact do
            key :name, 'Zaire API Team'
          end
          license do
            key :name, 'MIT'
          end
        end

        security_definition :authorization do
          key :type, :apiKey
          key :name, :authorization
          key :in, :header
        end

        key :basePath, '/api/v1'
        key :consumes, %w[application/json multipart/form-data]
        key :produces, ['application/json']
      end

      # A list of all classes that have swagger_* declarations.
      SWAGGERED_CLASSES = [
        Swagger::V1::Models::RoomShow,
        Swagger::V1::Models::RoomCreateQuery,
        Swagger::V1::Models::RoomIndex,
        Swagger::V1::Models::Rooms::AmenityIndex,
        Swagger::V1::Models::Rooms::AmenityCreateQuery,
        Swagger::V1::Models::Rooms::RoomTypeIndex,
        Swagger::V1::Models::Rooms::RoomTypeCreateQuery,
        Swagger::V1::Models::Rooms::RoomTypeUpdateQuery,
        Swagger::V1::Models::Rooms::RoomTypeShow,
        Swagger::V1::Models::Hotels::AddressShow,
        Swagger::V1::Models::Hotels::ConfigurationShow,
        Swagger::V1::Models::Hotels::HotelShow,
        Swagger::V1::Models::Hotels::OfflineModeShow,
        Swagger::V1::Models::Hotels::CheckTimeShow,
        Swagger::V1::Models::Hotels::CheckTimeIndex,
        Swagger::V1::Models::Hotels::LogoShow,
        Swagger::V1::Models::Hotels::LogoCreateQuery,
        Swagger::V1::Models::Invoices::RefundShow,
        Swagger::V1::Models::Invoices::RefundCreateQuery,
        Swagger::V1::Models::Invoices::RefundUpdateQuery,
        Swagger::V1::Models::Invoices::DiscountShow,
        Swagger::V1::Models::Invoices::DiscountCreateQuery,
        Swagger::V1::Models::Invoices::DiscountUpdateQuery,
        Swagger::V1::Models::Invoices::PaymentShow,
        Swagger::V1::Models::Invoices::PaymentCreateQuery,
        Swagger::V1::Models::Invoices::PaymentUpdateQuery,
        Swagger::V1::Models::Invoices::ChargeShow,
        Swagger::V1::Models::Invoices::ChargeCreateQuery,
        Swagger::V1::Models::Invoices::ChargeUpdateQuery,
        Swagger::V1::Models::Invoices::InvoiceShow,
        Swagger::V1::Models::Hotels::OfflineModeShow,
        Swagger::V1::Models::Profiles::ProfileShow,
        Swagger::V1::Models::Profiles::ProfileUpdateQuery,
        Swagger::V1::Models::Profiles::UserUpdateQuery,
        Swagger::V1::Models::Profiles::AvatarUpdateQuery,
        Swagger::V1::Models::Reservations::AdultShow,
        Swagger::V1::Models::Reservations::AdultCreateQuery,
        Swagger::V1::Models::Reservations::AdultCreateFromReservationQuery,
        Swagger::V1::Models::Reservations::AffectedReservation,
        Swagger::V1::Models::Reservations::DNRShow,
        Swagger::V1::Models::Reservations::DNRIndex,
        Swagger::V1::Models::Reservations::DNRCreateQuery,
        Swagger::V1::Models::Reservations::ReservationShow,
        Swagger::V1::Models::Reservations::ReservationIndex,
        Swagger::V1::Models::Reservations::ReservationCancelQuery,
        Swagger::V1::Models::Reservations::ReservationCreateQuery,
        Swagger::V1::Models::Reservations::ReservationUpdateQuery,
        Swagger::V1::Models::Guests::GuestIndex,
        Swagger::V1::Models::Guests::GuestCreateQuery,
        Swagger::V1::Models::Guests::GuestUpdateQuery,
        Swagger::V1::Models::Guests::GuestSearchQuery,
        Swagger::V1::Models::Countries::CountryShow,
        Swagger::V1::Models::Countries::CountryIndex,
        Swagger::V1::Models::Notifications::NotificationIndex,
        Swagger::V1::Models::Notifications::NotificationShow,
        Swagger::V1::Models::Notifications::NotificationCreateQuery,
        Swagger::V1::Models::Reservations::ReservationIndex,
        Swagger::V1::Models::Reports::InvoiceShow,
        Swagger::V1::Models::Reports::RoomTypeShow,
        Swagger::V1::Models::Reports::TotalsIndex,
        Swagger::V1::Models::Reports::ReservationIndex,
        Swagger::V1::Models::Rooms::RoomTypeRateShow,
        Swagger::V1::Models::Rooms::RoomTypeRateUpdate,
        Swagger::V1::Models::Rates::PeriodCreate,
        Swagger::V1::Models::Rates::PeriodUpdate,
        Swagger::V1::Models::Rates::PeriodShow,
        Swagger::V1::Models::Rates::RatesIndex,
        Swagger::V1::Models::Rates::PeriodsRoomTypeShow,
        Swagger::V1::Models::Rates::PeriodsRoomTypeCreate,
        Swagger::V1::Models::Rates::PeriodsRoomTypeUpdate,
        Swagger::V1::Models::Rates::PeriodsRoomTypeRates,
        Swagger::V1::Models::Rates::PeriodsRoomTypeRate,

        Swagger::V1::Controllers::Countries::CountriesController,
        Swagger::V1::Controllers::Guests::GuestsController,
        Swagger::V1::Controllers::Hotels::HotelsController,
        Swagger::V1::Controllers::Hotels::AddressesController,
        Swagger::V1::Controllers::Hotels::CheckTimeController,
        Swagger::V1::Controllers::Hotels::ConfigurationsController,
        Swagger::V1::Controllers::Hotels::OfflineModesController,
        Swagger::V1::Controllers::Hotels::LogosController,
        Swagger::V1::Controllers::Notifications::NotificationsController,
        Swagger::V1::Controllers::Profiles::ProfilesController,
        Swagger::V1::Controllers::Rates::RatesController,
        Swagger::V1::Controllers::Rates::PeriodsController,
        Swagger::V1::Controllers::Reports::ReservationsController,
        Swagger::V1::Controllers::Reservations::ReservationsController,
        Swagger::V1::Controllers::Reservations::AdultsController,
        Swagger::V1::Controllers::Reservations::DNRController,
        Swagger::V1::Controllers::Rooms::RoomsController,
        Swagger::V1::Controllers::Rooms::AvailableRoomsController,
        Swagger::V1::Controllers::Rooms::AmenitiesController,
        Swagger::V1::Controllers::Rooms::RoomTypesController,
        Swagger::V1::Controllers::Invoices::InvoicesController,
        Swagger::V1::Controllers::Invoices::ChargesController,
        Swagger::V1::Controllers::Invoices::DiscountsController,
        Swagger::V1::Controllers::Invoices::PaymentsController,
        Swagger::V1::Controllers::Invoices::RefundsController,

        self
      ].freeze

      def index
        authorize :swagger

        render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
      end
    end
  end
end
