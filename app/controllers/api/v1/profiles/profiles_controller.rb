# frozen_string_literal: true

module Api
  module V1
    module Profiles
      class ProfilesController < ApiController
        def show
          authorize profile
          success(response: serialize(profile, ::Serializers::Profiles::ProfileSerializer))
        end

        def update
          authorize profile

          update_user
          profile.update!(profile_params)

          success(response: serialize(profile.reload, ::Serializers::Profiles::ProfileSerializer))
        end

        protected

        def update_user
          return unless user_params

          current_user.update!(user_params)
          bypass_sign_in(current_user)
        end

        def user_params
          validated_params[:user_attributes]&.except(:current_password)
        end

        def profile_params
          validated_params.except(:user_attributes)
        end

        def validated_params
          permitted_params[:user_attributes]&.merge!(id: current_user.id)
          permitted_params[:avatar_attributes]&.merge!(id: profile&.avatar&.id, tag: 'avatar')
          @validated_params ||= ::Validators::Profiles::ProfileValidator.new(permitted_params, profile).to_hash
        end

        def permitted_params
          @permitted_params ||= params.fetch(:profile, {}).permit(*permitted_attributes)
        end

        def permitted_attributes
          [
            :id,
            :first_name,
            :last_name,
            :phone_number,
            :notifications,
            avatar_attributes: %i[base64 _destroy],
            user_attributes: %i[password password_confirmation current_password]
          ]
        end

        def profile
          @profile ||= current_user.profile
        end
      end
    end
  end
end
