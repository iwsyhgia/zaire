# frozen_string_literal: true

module Api
  module V1
    module Reports
      class ReservationsController < ApiController
        include Search::ReportsSearch

        ReservationSerializer = ::Serializers::Reports::ReservationSerializer
        PER_PAGE = 100

        def index
          # Totals are shown for all the results, that are shown for all the pages
          totals = ::Services::Reports::CalculateTotals.call(reservations: reservations).value
          reservations_filtered = search(reservations).page(params[:page]).per(PER_PAGE)
          serialized_reservations = serialize(reservations_filtered, ReservationSerializer)

          success(response: { reservations: serialized_reservations, totals: totals })
        end

        private

        def reservations
          @reservations ||=
            policy_scope(::Reservations::Reservation).
            includes(:guest, :room_type, :room, :invoice).
            where.not(status: :cancelled)
        end
      end
    end
  end
end
