# frozen_string_literal: true

module Api
  class ApiController < ActionController::API
    include Devise::Controllers::Helpers
    include Pundit

    before_action :set_locale
    before_action :remember_current_controller
    before_action :set_paper_trail_whodunnit
    around_action :handle_request

    def set_locale
      locale = request.env['HTTP_X_LANGUAGE'] if %w[en ar].include?(request.env['HTTP_X_LANGUAGE'])
      locale ||= params[:locale] || I18n.default_locale

      I18n.locale = locale
    end

    def current_user
      if request.env['HTTP_POSTMAN_TOKEN'] && (Rails.env.development? || Rails.env.qa?)
        User.first
      else
        super
      end
    end

    #  Stores current controller to be accessible in models, etc later
    #
    def remember_current_controller
      ::Services::CurrentControllerService.set(self)
    end

    # Each user will be connected to one hotel. for now we consider only hoteliers
    # TODO: later don't forget to update it for other roles (get it from memberships)
    def current_hotel
      @current_hotel = current_user&.hotel
    end

    # For now we going to send updated data with each request
    # TODO: leter response with this data only to receptionists
    def offline_store_data
      @offline_store_data ||= ::Services::Offline::FetchData.new(hotel: current_hotel).call.value
    end

    def success(status: 200, response: {})
      render json: { offline_store_data: offline_store_data, response: response, status: status.to_i }, status: status
    end

    def error(status: 400, message: '', details: {})
      error = { message: message }
      error[:details] = details unless details.blank?
      render json: { error: error, status: status.to_i }, status: status
    end

    def serialize(resources, each_serializer)
      return resources.to_json if resources.nil?

      options = { each_serializer: each_serializer }
      ActiveModelSerializers::SerializableResource.new(resources, options).as_json
    end

    def handle_service(service)
      result = service.perform
      result.success? ? success(response: result.value) : error(message: result.value)
    end

    def authenticate_user
      raise Pundit::NotAuthorizedError unless current_user
    end

    def handle_request
      authenticate_user
      yield
    rescue ::Pundit::NotAuthorizedError
      response.set_header('WWW-Authenticate', 'Bearer')
      error(status: 401, message: I18n.t('pundit.default'))
    rescue ActiveRecord::RecordNotFound
      error(status: 404, message: I18n.t('.general.errors.record_not_found'))
    rescue ActiveRecord::RecordInvalid => e
      error(status: 422, message: e.message, details: e.record.errors)
    rescue ActionController::UnpermittedParameters => e
      error(message: e.message, details: e.params)
    rescue Validators::InvalidParameters => e
      error(message: e.message, details: e.errors)
    rescue Schemas::InvalidError => e
      error(message: e.message, details: e.errors)
    rescue Services::Exception => e
      error(message: e.message, details: Rails.env.develoment? ? e.backtrace : nil)
    end
  end
end
