import axios from 'axios';
import { AJAX, LOG_ERROR, SET_OFFLINE_STATE, SET_ONLINE_STATE } from '../constants/actionConstants';

const { saveState, loadState } = require('../store/syncStoreWithLocalStorage');

const ajax = ({ dispatch }) => next => (action) => {
  // Only process API actions
  if (!action || action.type !== AJAX) {
    return next(action);
  }

  const { payload } = action;

  // Setup Default Fetch Options
  const { url, meta = {} } = payload;
  const method  = payload.method || 'GET';
  const locale  = window.localStorage.getItem('locale');
  let data      = payload.data || {};
  let params    = null;
  let headers   = {
    'Content-Type': 'application/json',
    Accept:         'application/json',
  };

  if (payload.formData) {
    data = payload.formData;
    headers = {};
  }

  if (locale) { headers['X-LANGUAGE'] = locale; }
  if (method === 'GET') { params = data; }
  if (method !== 'GET') {
    headers['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  }

  const warn = (error, errorData) => {
    if (process && process.env && process.env.BABEL_ENV == 'jest') return; //eslint-disable-line
    console.warn(error, errorData); //eslint-disable-line
  };

  const requestParams = () => ({
    method,
    url,
    data,
    params,
    ...payload.config,
    headers,
    withCredentials: true,
  });

  // Functions to handle various response states
  const handleStart = () => dispatch({ type: payload.PENDING });

  const handleError = (error, response = {}, metaData) => {
    warn(error, response.data || '');

    if (metaData.log && metaData.log.includes('error')) {
      dispatch({
        type:    LOG_ERROR,
        payload: response.data,
      });
    }

    dispatch({
      type:    payload.ERROR, error, payload: response.data, response,
    });
    return next(action);
  };
  const handleSuccess = (response) => {
    dispatch({ type: payload.SUCCESS, payload: response, parentAjaxAction: action });
    return next(action);
  };

  handleStart();

  const handleOfflineData = (response) => {
    saveState('offline-storage', response.data.offline_store_data);
  };

  const handleRequestForOffline = () => {

    const requestedParams          = requestParams();
    const isApiGetReservationRequest = requestedParams.url.match(/(\/api\/v1\/reservations\/\d*)/) !== null;
    const isApiGetInvoiceRequest     = requestedParams.url.match(/(\/api\/v1\/invoices\/\d*)/) !== null;
    const offlineStorage             = loadState('offline-storage').offline_storage;
    const onlineState  = loadState('redux-store');
    let offlineState   = onlineState;

    if (offlineStorage) {
      offlineState = {
        ...onlineState,
        reservations:   { ...onlineState.reservations, list: offlineStorage.reservations },
        room_types:     { ...onlineState.room_types, list: offlineStorage.room_types },
        rooms:          { ...onlineState.rooms, list: offlineStorage.rooms },
        hotel_settings: { ...onlineState.hotel_settings, offline: offlineStorage.hotel.offline },
      };
    }

    dispatch({ type: SET_OFFLINE_STATE, payload: offlineState, parentAjaxAction: action });

    if (isApiGetReservationRequest || isApiGetInvoiceRequest) {
      dispatch({
        type:             payload.OFFLINE,
        payload:          offlineState,
        parentAjaxAction: action,
        offlineStorage,
      });
    }
  };

  return new Promise((resolve, reject) => (
    axios.request(requestParams()).then((response) => {
      if (response.status === 401) {
        dispatch({ type: SET_ONLINE_STATE, parentAjaxAction: action });
        window.location.reload();
        return;
      }
      if (response.status >= 300) {
        const contentType = response.headers.get('content-type');
        const error       = `Error ${response.status}: ${response.statusText}`;
        const errorData   = contentType && contentType.indexOf('application/json') !== -1 ? response.data : response;

        handleError(error, errorData, meta);
        reject(error, response);
        dispatch({ type: SET_ONLINE_STATE, parentAjaxAction: action });
      } else {
        const successData = (response.status === 204) ? {} : response.data;
        if (response.data.offline_store_data) { handleOfflineData(response); }

        handleSuccess(successData);
        resolve(successData);
        dispatch({ type: SET_ONLINE_STATE, parentAjaxAction: action });
      }
    }).catch((error) => {
      if (!error.response) {
        handleRequestForOffline(error);
        return;
      }

      handleError(`Fetch Error: ${error.message}`, error.response, meta);
      reject(error);
    })));
};

export default ajax;
