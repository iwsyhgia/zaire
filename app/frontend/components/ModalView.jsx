import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import {
  resetForm as resetFormAction,
  showReservationPopup as showReservationPopupAction,
} from '../actions/reservations';
import IconCloseModal from './icons/IconCloseModal';

class ModalView extends Component {

  static propTypes = {
    resetForm:            PropTypes.func.isRequired,
    showReservationPopup: PropTypes.func.isRequired,
    showedComponent:      PropTypes.element.isRequired,
    isShow:               PropTypes.bool,
  };

  static defaultProps = {
    isShow: false,
  }

  componentDidMount() {
  }

  onCancel = () => {
    const { resetForm, showReservationPopup } = this.props;
    showReservationPopup({ isShow: false });
    resetForm();
  }

  render() {
    const {
      isShow,
      showedComponent,
    } = this.props;

    return (
      <div
        className={cx('reservation-popup modal fade', {
          'd-block show': isShow,
        })}
        tabIndex="-1"
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <button type="button" className="close close-modal" data-dismiss="modal" aria-label="Close" onClick={() => this.onCancel()}>
              <IconCloseModal width="14" height="14" className="icon icon-close" />
            </button>
            <div className="modal-body">
              {showedComponent}
            </div>
          </div>
        </div>
      </div>
    );
  }

}

const mapStateToProps = state => ({
  isShow:          state.reservations.form.isShow,
  isEdit:          state.reservations.form.isEdit,
  showedComponent: state.reservations.form.showedComponent,
});

const mapDispatchToProps = dispatch => ({
  resetForm:            () => { dispatch(resetFormAction()); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalView);
