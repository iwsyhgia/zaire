import React from 'react';
import i18n from 'i18next';
import IconQuestion from '../icons/IconQuestion';

const ImageInfo = () => (
  <div className="mb-3 supported_formats">
    <div className="row no-gutters">
      <div className="">
        <IconQuestion className="question-icon" />
      </div>
      <div className="col mx-1">
        {` ${i18n.t('hotel.logo_formats_support')}`}
        <br />
        <span>{i18n.t('hotel.logo_size_invalid', { min: '1', max: '10' })}</span>
      </div>
    </div>
  </div>
);

export default ImageInfo;
