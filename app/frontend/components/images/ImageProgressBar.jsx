import React from 'react';
import ImageModal from './ImageModal';

const ImageProgressBar = ({ show, progress }) => (
  <ImageModal
    show={show}
  >
    <div className="progress">
      <div
        style={{ width: `${progress}%` }}
        className="progress-bar progress-green"
        role="progressbar"
        aria-valuenow={progress}
        aria-valuemin="0"
        aria-valuemax="100"
      />
    </div>
  </ImageModal>
);

export default ImageProgressBar;
