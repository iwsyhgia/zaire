import React from 'react';
import Cropper from 'react-cropper';
import ImageModal from './ImageModal';

const ImageCropper = React.forwardRef((props, ref) => (
  <ImageModal
    isOffline={props.isOffline}
    show={props.show}
    onCancel={props.onCancel}
    onSave={props.onSave}
  >
    <Cropper
      src={props.blob}
      ref={ref}
      aspectRatio={props.aspectRatio}
      className="cropper"
    />
  </ImageModal>
));

export default ImageCropper;
