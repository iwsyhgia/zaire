import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import i18n from 'i18next';
import { SecondaryButton, PrimaryButton } from '../controls/Buttons';

const ImageModal = ({
  onSave,
  onCancel,
  saveName = i18n.t('common.save'),
  cancelName = i18n.t('common.cancel'),
  show,
  children,
}) => {
  const footerPresent = _.isFunction(onCancel) || _.isFunction(onSave);

  const cancelButton = _.isFunction(onCancel)
    && (<SecondaryButton className="mx-3" onClick={onCancel}>{cancelName}</SecondaryButton>);

  const saveButton = _.isFunction(onSave)
    && (<PrimaryButton onClick={onSave}>{saveName}</PrimaryButton>);

  return (
    <div
      className={cx('error-popup modal fade', { 'show d-block': show })}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content image-modal">
          <div className="d-flex flex-column justify-content-center modal-body">
            {children}
          </div>
          { footerPresent
            && (
              <div className="modal-footer d-flex justify-content-flex-end">
                { cancelButton }
                { saveButton }
              </div>
            )
          }
        </div>
      </div>
    </div>
  );
};

export default ImageModal;
