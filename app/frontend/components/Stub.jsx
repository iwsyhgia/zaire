import React from "react";
import i18n from 'i18next';


const Stub = props => (
  <div className="jumbotron">
    <h1 className="text-center display-3">{i18n.t('common.coming_soon')}</h1>
  </div>
);

export default Stub;



