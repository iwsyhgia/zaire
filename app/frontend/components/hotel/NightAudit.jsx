import React              from 'react';
import _                  from 'lodash';
import { Form, Checkbox } from 'informed';
import { connect }        from 'react-redux';
import Select             from 'react-select';
import i18n from 'i18next';
import { NITGH_AUDIT_TIME_OPTIONS }                          from '../../constants/constants';
import { PageBlock, ContentBlock, ContentGreyBlock } from '../Wrappers';
import { SecondaryButton, PrimaryButton } from '../controls/Buttons';
import {
  getAuditTime as getAuditTimeAction,
  storeAuditTime as storeAuditTimeAction,
  updateAuditTime as updateAuditTimeAction,
} from '../../actions/hotelSettings/nightAudit';

class NightAudit extends React.Component {

  componentDidMount() {
    const { getAuditTime } = this.props;

    getAuditTime();
  }

  onSave = () => {
    const { nightAudit, updateAuditTime } = this.props;
    const params = { configuration: { ...nightAudit.model } };

    updateAuditTime(params);
  };

  onCancel = () => {
    const { nightAudit, storeAuditTime } = this.props;
    storeAuditTime({
      model: { ...nightAudit.real },
    });
  };

  isChanged = () => {
    const { nightAudit: { model, real } } = this.props;
    return (model.night_audit_time !== real.night_audit_time
      || model.night_audit_confirmation_required !== real.night_audit_confirmation_required);
  }

  render() {
    const { nightAudit: { model, real }, storeAuditTime } = this.props;

    if (_.isEmpty(model)) return null;
    const initialTime = NITGH_AUDIT_TIME_OPTIONS.find(item => item.value === model.night_audit_time);
    const initialManualConfirmation = model.night_audit_confirmation_required;

    return (
      <PageBlock
        title={i18n.t('hotel.hotel_settings')}
        subtitle={i18n.t('hotel.night_audit')}
        className="page-hotel-settings"
      >
        <div className="zr-content-block zr-content-block-checkin">
          <ContentBlock className="box-shadow border-box flex-column">
            <div className="d-flex flex-column p-3">
              <h3 className="mb-2">{i18n.t('hotel.edit_night_audit')}</h3>
            </div>
            <Form name="form-hotel-night-audit" className="form-hotel-offline w-100">
              {({ formApi }) => (
                <ContentGreyBlock className="pl-3 pr-3 pt-4 pb-4 justify-content-between align-items-end">
                  <div className="">
                    <div className="form-group d-flex">
                      <Select
                        value={initialTime || NITGH_AUDIT_TIME_OPTIONS[0]}
                        onChange={(value) => {
                          storeAuditTime({
                            model: { ...model, ...{ night_audit_time: value.value } },
                          });
                        }}
                        options={NITGH_AUDIT_TIME_OPTIONS}
                        className="check-time-select"
                        classNamePrefix="check-time-select"
                      />
                    </div>
                    <div className="form-group d-flex">
                      <label htmlFor="input_night_audit_confirmation_required">
                        <Checkbox
                          field="night_audit_confirmation_required"
                          id="input_night_audit_confirmation_required"
                          initialValue={initialManualConfirmation}
                          checked={initialManualConfirmation}
                          value={initialManualConfirmation}
                          onChange={(event) => {
                            storeAuditTime({
                              model: {
                                ...model, ...{ night_audit_confirmation_required: event.target.checked },
                              },
                            });
                          }}
                        />
                        <span className="ml-1 mr-1">
                          {i18n.t('hotel.configuration.night_audit_confirmation_required')}
                        </span>
                      </label>
                    </div>
                  </div>
                  <div className="d-flex justify-content-end">
                    <div className="zr-content-check-actions pb-2">
                      <ul className="zr-content-check-actions-list">
                        <li className="zr-content-check-action">
                          <SecondaryButton
                            disabled={!this.isChanged()}
                            className="btn-fz-initial"
                            onClick={() => {
                              this.onCancel();
                              formApi.setValue('night_audit_confirmation_required',
                                real.night_audit_confirmation_required);
                            }}
                          >
                            {i18n.t('common.cancel')}
                          </SecondaryButton>
                        </li>
                        <li className="zr-content-check-action btn-fz-initial">
                          <PrimaryButton disabled={!this.isChanged()} onClick={() => this.onSave()}>
                            {i18n.t('common.save')}
                          </PrimaryButton>
                        </li>
                      </ul>
                    </div>
                  </div>
                </ContentGreyBlock>
              )}
            </Form>
          </ContentBlock>
        </div>
      </PageBlock>
    );
  }

}

const mapStateToProps = state => ({
  nightAudit: state.hotel_settings.nightAudit,
});

const mapDispatchToProps = dispatch => ({
  getAuditTime:    () => dispatch(getAuditTimeAction()),
  updateAuditTime: data => dispatch(updateAuditTimeAction(data)),
  storeAuditTime:  data => dispatch(storeAuditTimeAction(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(NightAudit);
