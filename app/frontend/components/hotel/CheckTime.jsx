import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import { Form } from 'informed';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { PageBlock, ContentBlock, Button, ContentGreyBlock } from '../Wrappers';
import { PrimaryButton, SecondaryButton } from '../controls/Buttons';
import {
  getCheckTime as getCheckTimeAction,
  storeCheckTime as storeCheckTimeAction,
  updateCheckTime as updateCheckTimeAction,
} from '../../actions/hotelSettings/checkTime';
import TimeSelect from '../controls/TimeSelect';
import { FormValidator } from '../validations';
import ValidationTooltip from '../notifications/ValidationTooltip';


class CheckTime extends React.Component {

  static validationRules(checkTime) {
    return (
      [
        {
          field:  'model[0].end_time',
          method: (field) => {
            if (!checkTime[0].fixed) {
              return checkTime[0].start_time < field;
            }
            return true;
          },
          validWhen: true,
          message:   i18n.t('hotel.wrong_time_period'),
        },
        {
          field:  'model[1].end_time',
          method: (field) => {
            if (!checkTime[1].fixed) {
              return checkTime[1].start_time < field;
            }
            return true;
          },
          validWhen: true,
          message:   i18n.t('hotel.wrong_time_period'),
        }
      ]
    );
  }

  constructor(props) {
    super(props);

    this.checkTimeRefs = [{}, {}];
  }

  componentDidMount() {
    const { getCheckTime } = this.props;

    getCheckTime();
  }

  onSave = () => {
    const { checkTime, storeCheckTime, updateCheckTime } = this.props;

    const rules = CheckTime.validationRules(checkTime.model);
    const validator = new FormValidator(rules);

    const validation = validator.validate(checkTime);

    if (!validation.isValid) {
      storeCheckTime({ validation });
      return;
    }

    const params = {
      check_time: {
        check_in_time_attributes:  _.find(checkTime.model, { check_type: 'check_in' }),
        check_out_time_attributes: _.find(checkTime.model, { check_type: 'check_out' }),
      },
    };

    updateCheckTime(params);
  };

  onCancel = () => {
    const { checkTime, storeCheckTime } = this.props;

    const checkTimeModel = checkTime.model.map((checkInOrOut, index) => {
      const checkInOrOutOld = _.cloneDeep(checkInOrOut);
      checkInOrOutOld.start_time = checkTime.real[index].start_time;
      checkInOrOutOld.end_time = checkTime.real[index].end_time;

      return checkInOrOutOld;
    });

    storeCheckTime({ model: checkTimeModel });
    setTimeout(() => {
      this.checkTimeRefs.forEach((ref) => {
        ref.start_time.resetToDefault();
        if (ref.end_time) ref.end_time.resetToDefault();
      });
    });
  };

  changeCheckTimeFields = (path, value) => {
    const { checkTime, storeCheckTime } = this.props;
    const checkTimeModel = _.cloneDeep(checkTime.model);
    _.set(checkTimeModel, path, value);

    storeCheckTime({ model: checkTimeModel, validation: null });
  };

  renderCheckPanel = (checkTime, title, index, showSubmit = false) => {
    const selectValidation = _.get(checkTime.validation, `model[${index}].end_time`, null);
    const validationMsg = selectValidation ? selectValidation.message : '';

    return (
      <ContentBlock className="zr-content-check-section">
        <Form className="plain-form zr-content-check-tabs">
          <h3 className="mb-3">{i18n.t(title)}</h3>
          <div className="btn-group btn-group-fixed" role="group" aria-label="Basic example">
            <Button
              type="button"
              className={cx('btn-secondary', { 'focused-secondary-button': checkTime.model[index].fixed })}
              onClick={() => this.changeCheckTimeFields(`[${index}].fixed`, true)}
            >
              {i18n.t('hotel.fixed_time')}
            </Button>
            <Button
              type="button"
              className={cx('btn-secondary', { 'focused-secondary-button': !checkTime.model[index].fixed })}
              onClick={() => this.changeCheckTimeFields(`[${index}].fixed`, false)}
            >
              {i18n.t('hotel.period')}
            </Button>
          </div>
        </Form>
        <div className="zr-content-check-time">
          {
            checkTime.model[index].fixed
              ? (
                <TimeSelect
                  ref={ref => _.extend(this.checkTimeRefs[index], { start_time: ref })}
                  defaultValue={checkTime.model[index].start_time || '00:00'}
                  onChange={value => this.changeCheckTimeFields(`[${index}].start_time`, value.value)}
                  styles="check-time-select"
                />
              )
              : (
                <div className="d-flex flex-row">
                  <div className="form-group mb-0">
                    <label className="check-time-label" htmlFor="check_out_from">{i18n.t('hotel.from')}</label>
                    <TimeSelect
                      ref={ref => _.extend(this.checkTimeRefs[index], { start_time: ref })}
                      defaultValue={checkTime.model[index].start_time || '00:00'}
                      id="check_out_from"
                      styles="check-time-select"
                      onChange={value => this.changeCheckTimeFields(`[${index}].start_time`, value.value)}
                    />
                  </div>

                  <div className="form-group mb-0 mx-3">
                    <label className="check-time-label" htmlFor="check_out_from">{i18n.t('hotel.to')}</label>
                    <TimeSelect
                      ref={ref => _.extend(this.checkTimeRefs[index], { end_time: ref })}
                      defaultValue={checkTime.model[index].end_time || '00:00'}
                      styles="check-time-select"
                      onChange={value => this.changeCheckTimeFields(`[${index}].end_time`, value.value)}
                    />
                    {
                      validationMsg !== ''
                      && <ValidationTooltip>{validationMsg}</ValidationTooltip>
                    }
                  </div>
                </div>
              )
          }
        </div>
        {
          showSubmit
          && (
            <ContentGreyBlock className="zr-content-check-actions">
              <ul className="zr-content-check-actions-list">
                <li className="zr-content-check-action">
                  <SecondaryButton className="btn-fz-initial" onClick={() => this.onCancel()}>
                    {i18n.t('common.cancel')}
                  </SecondaryButton>
                </li>
                <li className="zr-content-check-action btn-fz-initial">
                  <PrimaryButton onClick={() => this.onSave()}>
                    {i18n.t('common.save')}
                  </PrimaryButton>
                </li>
              </ul>
            </ContentGreyBlock>
          )
        }
      </ContentBlock>
    );
  }

  render() {
    const { checkTime } = this.props;

    if (_.isEmpty(checkTime.model)) return null;

    return (
      <PageBlock
        title={i18n.t('hotel.hotel_settings')}
        subtitle={i18n.t('hotel.check_time_subtitle')}
        className="page-hotel-settings"
      >
        <div className="zr-content-block zr-content-block-checkin">
          { this.renderCheckPanel(checkTime, 'hotel.edit_check_in', 0, false) }
          { this.renderCheckPanel(checkTime, 'hotel.edit_check_out', 1, true) }
        </div>
      </PageBlock>
    );
  }

}

const mapStateToProps = state => ({
  checkTime: state.hotel_settings.checkTime,
});

const mapDispatchToProps = dispatch => ({
  getCheckTime:    () => dispatch(getCheckTimeAction()),
  updateCheckTime: data => dispatch(updateCheckTimeAction(data)),
  storeCheckTime:  data => dispatch(storeCheckTimeAction(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(CheckTime);
