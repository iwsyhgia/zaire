import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Text } from 'informed';
import i18n from 'i18next';

import { PageBlock, ContentBlock, ContentGreyBlock } from '../Wrappers';
import Switcher from '../controls/Switcher';

import {
  updateOfflineMode as updateOfflineModeAction,
  getOfflineMode as getOfflineModeAction,
} from '../../actions/hotelSettings/offlineMode';
import { SubmitButton, SecondaryButton } from '../controls/Buttons';

class OfflineMode extends React.Component {

  static propTypes = {
    isOffline:         PropTypes.bool.isRequired,
    updateOfflineMode: PropTypes.func.isRequired,
    getOfflineMode:    PropTypes.func.isRequired,
    offline:           PropTypes.instanceOf(Object).isRequired,
  };

  componentDidMount() {
    const { getOfflineMode } = this.props;
    getOfflineMode();
  }

  onSubmit = (data) => {
    const { updateOfflineMode, offline: { active } } = this.props;

    const payload = {
      active,
      days_before_today: data.days_before_today,
      days_after_today:  data.days_after_today,
    };

    updateOfflineMode(payload);
  }

  validDays = (value = '') => {
    const arr = '0123456789';
    return (value.length === 0 || (value.length > 0 && arr.indexOf(value[value.length - 1]) > -1));
  }

  areDaysChanged = (values) => {
    const { offline: { days_before_today: daysBeforeToday, days_after_today: daysAfterToday } } = this.props;
    return +values.days_before_today !== daysBeforeToday || +values.days_after_today !== daysAfterToday;
  }

  turnOn = () => {
    const {
      updateOfflineMode,
      offline: { days_before_today: daysBeforeToday, days_after_today: daysAfterToday },
    } = this.props;

    updateOfflineMode({
      active:            true,
      days_before_today: daysBeforeToday,
      days_after_today:  daysAfterToday,
    });
  }

  turnOff = () => {
    const {
      updateOfflineMode,
      offline: { days_before_today: daysBeforeToday, days_after_today: daysAfterToday },
    } = this.props;

    updateOfflineMode({
      active:            false,
      days_before_today: daysBeforeToday,
      days_after_today:  daysAfterToday,
    });
  }

  render() {
    const {
      isOffline, offline: { days_before_today: daysBeforeToday, days_after_today: daysAfterToday, active: isOn },
    } = this.props;

    return (
      <PageBlock
        title={i18n.t('hotel.hotel_settings')}
        subtitle={i18n.t('hotel.offline_storage')}
        className="page-hotel-settings"
      >
        <ContentBlock className="box-shadow border-box flex-column">
          <div className="d-flex p-3">
            <Switcher name="active" isOn={isOn} onTurnOn={this.turnOn} onTurnOff={this.turnOff} disabled={isOffline} />
            <h2 className="pl-3 pr-3 mb-0">{i18n.t('hotel.offline_storage')}</h2>
          </div>
          {!!isOn && (
            <Form name="form-hotel-offline" className="form-hotel-offline w-100" onSubmit={this.onSubmit}>
              {({ formApi, formState }) => (
                <ContentGreyBlock className="pl-3 pr-3 pt-4 pb-4 justify-content-between align-items-end">
                  <div>
                    <div className="form-group d-flex">
                      <Text
                        field="days_before_today"
                        key={`days_before_today-${daysBeforeToday}`}
                        type="number"
                        min="0"
                        id="days_before_today"
                        className="form-control box-shadow"
                        initialValue={daysBeforeToday}
                      />
                      <label htmlFor="days_before_today" className="px-3">{i18n.t('hotel.days_before')}</label>
                    </div>
                    <div className="form-group d-flex">
                      <Text
                        field="days_after_today"
                        key={`days_after_today-${daysAfterToday}`}
                        type="number"
                        min="0"
                        id="days_after_today"
                        className="form-control box-shadow"
                        initialValue={daysAfterToday}
                      />
                      <label htmlFor="days_after_today" className="px-3">{i18n.t('hotel.days_after')}</label>
                    </div>
                  </div>
                  <div className="d-flex justify-content-end mt-3">
                    <SecondaryButton
                      className="ml-3 mr-3"
                      onClick={
                        () => {
                          formApi.setValue('days_before_today', daysBeforeToday);
                          formApi.setValue('days_after_today', daysAfterToday);
                        }
                      }
                      disabled={!this.areDaysChanged(formState.values)}
                    >
                      {i18n.t('common.cancel')}
                    </SecondaryButton>
                    <SubmitButton disabled={!this.areDaysChanged(formState.values)}>
                      {i18n.t('common.save')}
                    </SubmitButton>
                  </div>
                </ContentGreyBlock>
              )}
            </Form>
          )}
        </ContentBlock>
      </PageBlock>
    );
  }

}

const mapStateToProps = state => ({
  offline:   state.hotel_settings.offline,
  success:   state.hotel_settings.success,
  isOffline: state.common.isOffline,
});

const mapDispatchToProps = dispatch => ({
  updateOfflineMode: (data) => { dispatch(updateOfflineModeAction(data)); },
  getOfflineMode:    () => { dispatch(getOfflineModeAction()); },
});


export default connect(mapStateToProps, mapDispatchToProps)(OfflineMode);
