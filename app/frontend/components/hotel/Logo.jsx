import React from 'react';
import _ from 'lodash';
import connect from 'react-redux/es/connect/connect';
import Cropper from 'react-cropper';
// eslint-disable-next-line
import 'cropperjs/dist/cropper.css';
import i18n from 'i18next';
import {
  createLogo as createLogoAction,
  deleteLogo as deleteLogoAction,
  getLogo as getLogoAction,
  storeLogo as storeLogoAction,
  updateLogo as updateLogoAction,
} from '../../actions/hotelSettings/logo';
import { ContentBlock, ContentGreyBlock } from '../Wrappers';
import DefaultLogo from '../icons/DefaultLogo';
import ConfirmationPopup from '../notifications/ConfirmationPopup';

import ImageCropper from '../images/ImageCropper';
import ImageProgressBar from '../images/ImageProgressBar';
import ImageInfo from '../images/ImageInfo';
import { PrimaryButton } from '../controls/Buttons';


class Logo extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      logoDeleteConfirmation: false,
    };
  }

  componentDidMount() {
    const { getLogo } = this.props;
    getLogo();
  }

  onLogoSelected = (files) => {
    const { storeLogo } = this.props;

    if (files && files[0]) {
      const [file] = files;
      file.blob = URL.createObjectURL(files[0]);
      storeLogo({ file, cropShow: true });
    }
  };

  createOrUpdateLogo = (base64, config) => {
    const { logo, createLogo, updateLogo } = this.props;
    const logoParams = { logo: { base64 } };

    return _.isEmpty(logo.model)
      ? createLogo(logoParams, config)
      : updateLogo(logoParams, config);
  };

  onCropperSave = () => {
    const { storeLogo } = this.props;

    this.cropperRef.current.getCroppedCanvas().toBlob((blob) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onload = () => {
        const config = {
          onUploadProgress: (progressEvent) => {
            const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
            storeLogo({ progress: percentCompleted });
          },
        };

        storeLogo({ progress: 0, progressShow: true, cropShow: false });

        this.createOrUpdateLogo(reader.result, config);
        this.fileInput.value = '';
      };
    }, 'image/jpeg');
  };

  noCropperCancel = () => {
    const { storeLogo } = this.props;
    document.getElementById('logo').value = '';
    storeLogo({ cropShow: false, file: null });
  };

  renderProgress() {
    const { logo } = this.props;

    return <ImageProgressBar show={logo.progressShow} progress={logo.progress} />;
  }

  renderCropperModal() {
    const { logo, isOffline } = this.props;
    this.cropperRef = React.createRef();

    return (
      <ImageCropper
        isOffline={isOffline}
        show={logo.cropShow}
        onSave={this.onCropperSave}
        onCancel={this.noCropperCancel}
        blob={_.get(logo.file, 'blob')}
        ref={this.cropperRef}
      />
    );
  }

  renderConfirmationPopup = () => {
    const { logoDeleteConfirmation }  = this.state;
    const { deleteLogo } = this.props;

    return (
      <ConfirmationPopup
        show={logoDeleteConfirmation}
        submitText={i18n.t('common.delete')}
        onSubmit={() => {
          deleteLogo();
          this.setState({ logoDeleteConfirmation: false });
        }}
        onHide={() => this.setState({ logoDeleteConfirmation: false })}
      >
        <p>{i18n.t('hotel.delete_logo_confirmation')}</p>
      </ConfirmationPopup>
    );
  };

  renderLogo = () => {
    const { logo, isOffline } = this.props;

    return (
      <ContentBlock className="box-shadow border-box flex-column mb-4">
        <div className="d-flex flex-column p-3">
          <h3 className="mb-2">{i18n.t('hotel.logo_of_hotel')}</h3>
        </div>
        <ContentGreyBlock className="pl-3 pr-3 pt-4 pb-4 justify-content-between align-items-end">
          <div className="d-flex flex-row p-3">
            <div className="d-flex w-5 p-2 logo-container rounded align-items-center">
              <div className="rounded logo">
                {
                  _.has(logo, 'model.path') ? (
                    <img src={logo.model.path} width="180" className="rounded" alt="logo" />
                  ) : (
                    <DefaultLogo width="180" />
                  )
                }
              </div>
            </div>
            <div className="flex-column mr-5 ml-5">
              <ImageInfo />
              <div className="mb-3 logo-file-input">
                <label htmlFor="logo" className={`btn btn-primary ${isOffline ? 'disabled' : ''}`} disabled={isOffline}>
                  <input
                    id="logo"
                    type="file"
                    disabled={isOffline}
                    onChange={e => this.onLogoSelected(e.target.files)}
                    accept="image/*"
                    // eslint-disable-next-line no-return-assign
                    ref={ref => (this.fileInput = ref)}
                  />
                  { _.has(logo, 'model.path') ? i18n.t('common.change') : i18n.t('common.add') }
                </label>
              </div>
              <div className="">
                <PrimaryButton
                  disabled={logo.model === null}
                  onClick={() => { this.setState({ logoDeleteConfirmation: true }); }}
                >
                  {i18n.t('common.delete')}
                </PrimaryButton>
              </div>
            </div>
          </div>
        </ContentGreyBlock>
      </ContentBlock>
    );
  };


  render() {
    return (
      <div>
        {this.renderLogo()}
        {this.renderCropperModal()}
        {this.renderProgress()}
        {this.renderConfirmationPopup()}
      </div>
    );
  }

}

const mapStateToProps = state => ({
  isOffline: state.common.isOffline,
  logo:      state.hotel_settings.logo,
});

const mapDispatchToProps = dispatch => ({
  getLogo:    () => dispatch(getLogoAction()),
  updateLogo: (data, config) => dispatch(updateLogoAction(data, config)),
  createLogo: (data, config) => dispatch(createLogoAction(data, config)),
  deleteLogo: () => dispatch(deleteLogoAction()),
  storeLogo:  data => dispatch(storeLogoAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Logo);
