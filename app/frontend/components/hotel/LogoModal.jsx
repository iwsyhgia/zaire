import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import i18n from 'i18next';


const LogoModal = ({ onSave, onCancel, show, children }) => {

  const footerPresent = _.isFunction(onCancel)
    || _.isFunction(onSave);

  return (
    <div
      className={cx('error-popup modal fade', { 'show d-block': show })}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content" style={{ height: '600px' }}>
          <div className="d-flex flex-column justify-content-center modal-body">
            {children}
          </div>
          {footerPresent
          && (
            <div className="modal-footer d-flex justify-content-flex-end">
              {
                _.isFunction(onCancel)
                && (
                <button
                  type="button"
                  className="btn btn-secondary mx-3"
                  onClick={onCancel}
                >
                  {i18n.t('common.cancel')}
                </button>
                )
              }
              {
                _.isFunction(onSave)
                && <button type="button" className="btn btn-primary" onClick={onSave}>{i18n.t('common.save')}</button>
              }
            </div>
          )
          }
        </div>
      </div>
    </div>
  );
};

export default LogoModal;
