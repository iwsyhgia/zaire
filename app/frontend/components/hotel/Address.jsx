import React from 'react';
import validator from 'validator';
import _ from 'lodash';
import cx from 'classnames';
import Select from 'react-select';
import connect from 'react-redux/es/connect/connect';
import i18n from 'i18next';
import FormValidator from '../validations/FormValidator';
import { ContentBlock, ContentGreyBlock } from '../Wrappers';
import { PrimaryButton, SecondaryButton } from '../controls/Buttons';
import {
  getAddress as getAddressAction,
  storeAddress as storeAddressAction,
  storeHotelSettings as storeHotelSettingsAction,
  updateAddress as updateAddressAction,
} from '../../actions/hotelSettings/nameAddress';

const PHONE_FORMAT = /^[+]?[0-9]+$/;

class Address extends React.Component {

  static validationRulesForAddress() {
    return (
      [
        {
          field:     'city',
          method:    field => validator.isEmpty((field || '').trim()),
          validWhen: false,
          message:   i18n.t('hotel.field_required'),
        },
        {
          field:     'city',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'address_1',
          method:    field => validator.isEmpty((field || '').trim()),
          validWhen: false,
          message:   i18n.t('hotel.field_required'),
        },
        {
          field:     'address_1',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'address_2',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'state',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'phone_number',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'phone_number',
          method:    field => validator.isEmpty((field || '').trim()),
          validWhen: false,
          message:   i18n.t('hotel.field_required'),
        },
        {
          field:     'phone_number',
          method:    field => PHONE_FORMAT.test((field || '').trim()),
          validWhen: true,
          message:   i18n.t('hotels.errors.phone_number_invalid'),
        },
        {
          field:     'fax',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'email',
          method:    field => validator.isLength(field || '', 0, 255),
          validWhen: true,
          message:   i18n.t('hotel.field_too_long'),
        },
        {
          field:     'email',
          method:    field => validator.isEmpty((field || '').trim()),
          validWhen: false,
          message:   i18n.t('hotel.field_required'),
        },
        {
          field:     'email',
          method:    field => validator.isEmail(field || ''),
          validWhen: true,
          message:   i18n.t('hotels.errors.email_invalid'),
        }
      ]
    );
  }

  componentDidMount() {
    const { getAddress } = this.props;

    getAddress();
  }

  onAddressCancel = () => {
    const { address, storeAddress } = this.props;
    storeAddress({
      model:         { ...address.real },
      country_input: { value: address.real.country, label: address.real.country },
    });
  };

  onAddressSave = () => {
    const { address, updateAddress, storeAddress, countries } = this.props;

    const formValidator = new FormValidator(Address.validationRulesForAddress());
    const validation = formValidator.validate(address.model);

    if (!validation.isValid) {
      storeAddress({ validation });
      return;
    }

    updateAddress({
      address: {
        country:      _.isEmpty(address.model.country) ? _.get(countries, '[0].name') : address.model.country,
        city:         address.model.city,
        state:        address.model.state,
        address_1:    address.model.address_1,
        address_2:    address.model.address_2,
        fax:          address.model.fax,
        email:        address.model.email,
        phone_number: address.model.phone_number,
      },
    });
  };

  countriesSelectOption = () => {
    const { countries } = this.props;
    return countries.map(country => ({ value: country.name, label: country.name }));
  };

  renderAddress = () => {
    const { address, storeAddress } = this.props;
    const countriesOptions = this.countriesSelectOption();

    if (_.isEmpty(countriesOptions) || _.isEmpty(address)) return null;

    return (
      <ContentBlock className="box-shadow border-box flex-column mb-4 address">
        <div className="d-flex flex-column p-3">
          <h3 className="mb-2">{i18n.t('hotel.address')}</h3>
        </div>
        <ContentGreyBlock className="pl-3 pr-3 pt-4 pb-4 justify-content-between align-items-end">
          <div className="plain-form address-form">
            <div className="form-row mt-2">
              <div className="col">
                <div className="w-100">
                  <div className="form-group live-placeholder country-select">
                    <Select
                      value={_.isEmpty(address.country_input.value)
                        ? countriesOptions[0] : address.country_input}
                      onChange={(value) => {
                        storeAddress({
                          country_input: value,
                          model:         { ...address.model, ...{ country: value.label } },
                        });
                      }}
                      options={countriesOptions}
                      className="plain-select"
                      classNamePrefix="plain-select"
                    />
                    <label htmlFor="ruf-name" className="required">{i18n.t('reservation.country')}</label>
                  </div>
                </div>
              </div>

              <div className="col mr-4 ml-4">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.phone_number}
                    onChange={(e) => {
                      storeAddress({ model: { ...address.model, phone_number: e.target.value } });
                    }}
                    id="phone_number"
                    className={
                      cx('form-control', { 'is-invalid': _.get(address, 'validation.phone_number.isInvalid') })
                    }
                  />
                  <label htmlFor="phone_number" className="required">
                    { i18n.t('hotel.phone_number')}
                  </label>
                  {
                    _.get(address, 'validation.phone_number.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.phone_number.message')}</span>
                  }
                </div>
              </div>
            </div>

            <div className="form-row mt-5">
              <div className="col">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.city}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, city: e.target.value },
                      });
                    }}
                    id="city"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.city.isInvalid') })}
                  />
                  <label htmlFor="city" className="required">{i18n.t('reservation.city')}</label>
                  {
                    _.get(address, 'validation.city.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.city.message')}</span>
                  }
                </div>
              </div>
              <div className="col mr-4 ml-4">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.fax}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, fax: e.target.value },
                      });
                    }}
                    id="fax"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.fax.isInvalid') })}
                  />
                  <label htmlFor="fax">{i18n.t('hotel.fax')}</label>
                  {
                    _.get(address, 'validation.fax.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.fax.message')}</span>
                  }
                </div>
              </div>
            </div>

            <div className="form-row mt-5">
              <div className="col">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.state}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, state: e.target.value },
                      });
                    }}
                    id="state"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.state.isInvalid') })}
                  />
                  <label htmlFor="state">{i18n.t('hotel.state')}</label>
                  {
                    _.get(address, 'validation.state.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.state.message')}</span>
                  }
                </div>
              </div>
              <div className="col mr-4 ml-4">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.email}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, email: e.target.value },
                      });
                    }}
                    id="email"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.email.isInvalid') })}
                  />
                  <label htmlFor="email" className="required">
                    {i18n.t('hotel.email')}
                  </label>
                  {
                    _.get(address, 'validation.email.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.email.message')}</span>
                  }
                </div>
              </div>
            </div>

            <div className="form-row mt-5">
              <div className="col">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.address_1}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, address_1: e.target.value },
                      });
                    }}
                    id="address_1"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.address_1.isInvalid') })}
                  />
                  <label htmlFor="address_1" className="required">{i18n.t('hotel.address')}</label>
                  {
                    _.get(address, 'validation.address_1.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.address_1.message')}</span>
                  }
                </div>
              </div>
              <div className="col mr-4 ml-4" />
            </div>

            <div className="form-row mt-5">
              <div className="col">
                <div className="form-group live-placeholder">
                  <input
                    value={address.model.address_2}
                    onChange={(e) => {
                      storeAddress({
                        model: { ...address.model, address_2: e.target.value },
                      });
                    }}
                    id="address_2"
                    className={cx('form-control', { 'is-invalid': _.get(address, 'validation.address_2.isInvalid') })}
                  />
                  <label htmlFor="address_2">{i18n.t('hotel.address_2')}</label>
                  {
                    _.get(address, 'validation.address_2.isInvalid')
                    && <span className="invalid-feedback">{_.get(address, 'validation.address_2.message')}</span>
                  }
                </div>
              </div>
              <div className="col mr-4 ml-4" />
            </div>
          </div>

          <div className="d-flex flex-row">
            <SecondaryButton
              className="mx-3"
              disabled={_.isEqual(address.model, address.real)}
              onClick={() => this.onAddressCancel()}
            >
              {i18n.t('common.cancel')}
            </SecondaryButton>
            <PrimaryButton onClick={() => this.onAddressSave()}>
              {i18n.t('common.save')}
            </PrimaryButton>
          </div>
        </ContentGreyBlock>
      </ContentBlock>
    );
  };

  render() {
    return (
      <div>
        {this.renderAddress()}
      </div>
    );
  }

}

const mapStateToProps = state => ({
  hotel_settings: state.hotel_settings,
  address:        state.hotel_settings.address,
  countries:      state.common.countries,
});

const mapDispatchToProps = dispatch => ({
  getAddress:         () => dispatch(getAddressAction()),
  updateAddress:      data => dispatch(updateAddressAction(data)),
  storeAddress:       data => dispatch(storeAddressAction(data)),
  storeHotelSettings: data => dispatch(storeHotelSettingsAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Address);
