import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import { Form, Text } from 'informed';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { ContentBlock, ContentGreyBlock } from '../Wrappers';
import { SubmitButton, SecondaryButton } from '../controls/Buttons';
import BiField from '../controls/BiField';
import {
  getHotel as getHotelAction,
  storeHotel as storeHotelAction,
  updateHotel as updateHotelAction,
} from '../../actions/hotelSettings/nameAddress';

class Hotel extends React.Component {

  componentDidMount() {
    const { getHotel } = this.props;
    getHotel();
  }

  storeHotelName = (options) => {
    const { storeHotel } = this.props;

    storeHotel({ model: { ...options } });
  }

  onSubmit = (data) => {
    const { hotel, updateHotel } = this.props;

    updateHotel({ hotel: { ...hotel, ...data } });
  }

  validatePresence = value => (
    !value || value.trim().length === 0 ? i18n.t('common.english_field_required') : null
  );

  validateOnlySpaces = value => (
    !!value && value.trim().length === 0 ? i18n.t('common.incorrect_field_value') : null
  );

  render() {
    const { hotel: { name, name_ar: nameAr, loading } } = this.props;

    if (loading) return null;

    return (
      <ContentBlock className="zr-hotel-setting__name box-shadow border-box flex-column mb-4">
        <div className="d-flex flex-column p-3">
          <h3 className="mb-2">{i18n.t('hotel.hotel_name')}</h3>
        </div>
        <ContentGreyBlock className="justify-content-between align-items-end">
          <Form name="hotel-name-form" className="hotel-name-form w-100" onSubmit={this.onSubmit}>
            {({ formState, formApi }) => (
              <div className="d-flex justify-content-between">
                <div className="zr-hotel-name-field">
                  <BiField
                    isFirstEmpty={_.get(formState, 'values.name', '').length === 0}
                    FirstField={(
                      <Text
                        field="name"
                        id="roomtype-name"
                        dir="ltr"
                        className={cx('form-control', {
                          'is-invalid': _.has(formState, 'errors.name'),
                          empty:        _.get(formState, 'values.name', '').length === 0,
                        })}
                        initialValue={name}
                        maxLength="30"
                        validate={this.validatePresence}
                        placeholder={i18n.t('hotel.name')}
                      />
                    )}
                    isSecondEmpty={_.get(formState, 'values.name_ar', '').length === 0}
                    SecondField={(
                      <Text
                        field="name_ar"
                        id="roomtype-name_ar"
                        dir="rtl"
                        className={cx('form-control', {
                          empty: _.get(formState, 'values.name_ar', '').length === 0,
                        })}
                        initialValue={nameAr}
                        maxLength="30"
                        validate={this.validateOnlySpaces}
                        placeholder={i18n.t('hotel.name')}
                      />
                    )}
                  />
                  {
                    <div className="invalid-feedback">
                      {_.has(formState, 'errors.name') && `${formState.errors.name} `}
                      {_.has(formState, 'errors.name_ar') && `${formState.errors.name_ar} `}
                    </div>
                  }
                </div>
                <div className="d-flex flex-row">
                  <SecondaryButton
                    className="mx-3"
                    disabled={formApi.getValue('name') === name && formApi.getValue('name_ar') === nameAr}
                    onClick={() => {
                      formApi.setValue('name', name);
                      formApi.setValue('name_ar', nameAr);
                    }}
                  >
                    {i18n.t('common.cancel')}
                  </SecondaryButton>
                  <SubmitButton onClick={() => this.onAddressSave()}>
                    {i18n.t('common.save')}
                  </SubmitButton>
                </div>
              </div>
            )}
          </Form>
        </ContentGreyBlock>
      </ContentBlock>
    );
  }

}

const mapStateToProps = state => ({
  hotel: state.hotel_settings.hotel,
});

const mapDispatchToProps = dispatch => ({
  getHotel:    () => dispatch(getHotelAction()),
  updateHotel: data => dispatch(updateHotelAction(data)),
  storeHotel:  data => dispatch(storeHotelAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Hotel);
