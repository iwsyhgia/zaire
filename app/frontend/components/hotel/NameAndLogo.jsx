import React from 'react';
import i18n from 'i18next';

import { PageBlock } from '../Wrappers';
/* eslint-disable no-return-assign */
import Logo from './Logo';
import Hotel from './Hotel';
import Address from './Address';


const NameAndLogo = () => (
  <PageBlock
    title={i18n.t('hotel.hotel_settings')}
    subtitle={i18n.t('hotel.name_and_logo_subtitle')}
    className="page-hotel-settings"
  >
    <div className="zr-hotel-settings">
      <Hotel />
      <Logo />
      <Address />
    </div>
  </PageBlock>
);

export default NameAndLogo;
