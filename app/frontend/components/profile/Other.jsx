import React from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import Switcher from '../controls/Switcher';
import { ContentBlock } from '../Wrappers';
import { storeProfileFormAction } from '../../actions/profiles';

const Other = ({ notifications, storeProfileForm }) => (
  <ContentBlock className="zr-account__details  flex-column mb-2">
    <div className="d-flex border-bottom flex-column py-2 px-3">
      <h3 className="mb-2">{i18n.t('profile.other')}</h3>
    </div>
    <div className="justify-content-start align-items-end">
      <div className="plain-form form-section">
        <div className="border-bottom not-expandeable form-row mx-0 pl-2 mb-3 pt-3 pb-3">
          <div className="col-2">
            <span htmlFor="">{i18n.t('profiles.notifications')}</span>
          </div>
          <div className="col-1">
            <Switcher
              name="active"
              isOn={notifications.value === 'on'}
              onTurnOn={() => storeProfileForm({ notifications: { value: 'on' } })}
              onTurnOff={() => storeProfileForm({ notifications: { value: 'off' } })}
            />
          </div>
        </div>
      </div>
    </div>
  </ContentBlock>
);

const mapStateToProps = state => ({
  notifications: state.profile.form.notifications,
});

const mapDispatchToProps = dispatch => ({
  storeProfileForm: data => dispatch(storeProfileFormAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Other);
