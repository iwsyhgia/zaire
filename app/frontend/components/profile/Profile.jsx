import React from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { PageBlock } from '../Wrappers';

import ProgressCircle from '../icons/ProgressCircle';
import FormValidator from '../validations/FormValidator';
import ValidationRules from './validation/rules';
import PasswordChange from './PasswordChange';
import DetailsPersonal from './DetailsPersonal';
import Avatar from './Avatar';
import Other from './Other';
import {
  getProfileAction,
  updateProfileAction,
  storeProfileAction,
  resetProfileFormAction,
} from '../../actions/profiles';
import { SecondaryButton, SubmitButton } from '../controls/Buttons';

class Profile extends React.Component {

  componentDidMount() {
    const { getProfile, profile } = this.props;
    if (!profile.model.avatar) getProfile();
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { storeProfile, updateProfile, form, resetForm } = this.props;

    const formValidator = new FormValidator(ValidationRules());
    const validation = formValidator.validate(form);

    if (!validation.isValid) {
      storeProfile({ validation });
      return;
    }

    updateProfile(this.getTransformedFormData());
    resetForm();
  }

  /* eslint-disable camelcase */
  getTransformedFormData = () => {
    const {
      form: {
        notifications,
        last_name,
        first_name,
        phone_number,
        password,
        avatar,
      },
    } = this.props;

    return {
      profile: {
        notifications: notifications.value,
        ...(first_name.expanded && { first_name: first_name.value }),
        ...(last_name.expanded && { last_name: last_name.value }),
        ...(phone_number.expanded && { phone_number: phone_number.value }),
        ...(password.expanded && {
          user_attributes: {
            password:              password.new,
            current_password:      password.current,
            password_confirmation: password.confirmation,
          },
        }),
        ...(avatar.expanded && { avatar_attributes: { ...avatar } }),
      },
    };
  }

  onCancel = () => {
    const { resetForm } = this.props;
    resetForm();
  }

  render() {
    const { loading, profile } = this.props;
    if (loading && !profile.model) return null;

    return (
      <PageBlock title={i18n.t('profile.page_title')} className="page-account">
        <form onSubmit={this.onSubmit} className="personal-account-form">
          <PasswordChange />
          <Avatar />
          <DetailsPersonal />
          <Other />
          <div className="d-flex justify-content-end flex-row">
            <SecondaryButton className="mx-3" onClick={this.onCancel}>
              {i18n.t('common.cancel')}
            </SecondaryButton>
            <SubmitButton>
              <div className="progress-circle-container">
                <ProgressCircle className={cx('progress-circle', { show: loading })} />
                { i18n.t('common.save') }
              </div>
            </SubmitButton>
          </div>
        </form>
      </PageBlock>
    );
  }

}

const mapStateToProps = state => ({
  form:    state.profile.form,
  profile: state.profile,
  loading: state.profile.loading,
});

const mapDispatchToProps = dispatch => ({
  resetForm:     () => dispatch(resetProfileFormAction()),
  getProfile:    () => dispatch(getProfileAction()),
  updateProfile: data => dispatch(updateProfileAction(data)),
  storeProfile:  data => dispatch(storeProfileAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
