import React from 'react';
import _ from 'lodash';

const ValidationFeedback = ({ model, name }) => {
  const result = _.get(model, `validation.${name}.isInvalid`);
  const message = _.get(model, `validation.${name}.message`);

  if (!result) return null;

  return (<span className="invalid-feedback">{message}</span>);
};

export default ValidationFeedback;
