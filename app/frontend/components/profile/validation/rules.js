import validator from 'validator';
import i18n from 'i18next';
import { PHONE_FORMAT } from '../../../constants/constants';

const MAX_LENGTH = 42;
const MIN_PASSWORD_LENGTH = 8;
const PASSWORD_REGEXP = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;

const required = ({ value, expanded }) => !expanded || !validator.isEmpty((value || '').trim());
const length = ({ value, expanded }) => !expanded || validator.isLength(value || '', 0, MAX_LENGTH);
const phoneFormat = ({ value, expanded }) => !expanded || PHONE_FORMAT.test((value || '').trim());

const passwordLength = (_stub, { name }, state) => {
  const { password } = state;
  return !password.expanded || validator.isLength(password[name] || '', MIN_PASSWORD_LENGTH, MAX_LENGTH);
};
const passwordComplexity = (_stub, { name }, state) => {
  const { password } = state;
  return !password.expanded || PASSWORD_REGEXP.test((password[name] || '').trim());
};
const passwordRequired = (_stub, { name }, state) => {
  const { password } = state;
  return !password.expanded || !validator.isEmpty((password[name] || '').trim());
};
const passwordMatch = (_stub, state) => {
  const { password } = state;
  return !password.expanded || password.confirmation === password.new;
};

const phoneFormatInvalidMessage = () => (i18n.t('profile.errors.phone_number_invalid'));
const lengthInvalidMessage = () => (i18n.t('profile.errors.length_invalid', { value: MAX_LENGTH }));
const requireMessage = () => (i18n.t('profile.errors.required'));
const passwordNotMatchMessage = () => (i18n.t('profile.errors.new_password_not_match'));
const confirmationNotMatchMessage = () => (i18n.t('profile.errors.confirmation_not_match'));
const complexityMessage = () => (i18n.t('profile.errors.complexity'));

const messageOptions = { count: MIN_PASSWORD_LENGTH };
const passwordLengthMessage = () => (
  `${i18n.t('users.password')} ${i18n.t('errors.messages.too_short', messageOptions)}`
);

const ValidationRules = () => ([
  {
    field:     'first_name',
    method:    required,
    validWhen: true,
    message:   requireMessage(),
  },
  {
    field:     'last_name',
    method:    required,
    validWhen: true,
    message:   requireMessage(),
  },
  {
    field:     'phone_number',
    method:    required,
    validWhen: true,
    message:   requireMessage(),
  },
  {
    field:     'password_confirmation',
    method:    passwordRequired,
    args:      [{ name: 'confirmation' }],
    message:   requireMessage(),
    validWhen: true,
  },
  {
    field:     'password_current',
    method:    passwordRequired,
    args:      [{ name: 'current' }],
    message:   requireMessage(),
    validWhen: true,
  },
  {
    field:     'password_new',
    method:    passwordRequired,
    args:      [{ name: 'new' }],
    message:   requireMessage(),
    validWhen: true,
  },
  {
    field:     'password_new',
    method:    passwordLength,
    validWhen: true,
    args:      [{ name: 'new' }],
    message:   passwordLengthMessage(),
  },
  {
    field:     'password_current',
    method:    passwordLength,
    validWhen: true,
    args:      [{ name: 'current' }],
    message:   passwordLengthMessage(),
  },
  {
    field:     'password_confirmation',
    method:    passwordLength,
    args:      [{ name: 'confirmation' }],
    validWhen: true,
    message:   passwordLengthMessage(),
  },
  {
    field:     'first_name',
    method:    length,
    validWhen: true,
    message:   lengthInvalidMessage(),
  },
  {
    field:     'last_name',
    method:    length,
    validWhen: true,
    message:   lengthInvalidMessage(),
  },
  {
    field:     'phone_number',
    method:    length,
    validWhen: true,
    message:   lengthInvalidMessage(),
  },
  {
    field:     'phone_number',
    method:    phoneFormat,
    validWhen: true,
    message:   phoneFormatInvalidMessage(),
  },
  {
    field:     'password_new',
    method:    passwordMatch,
    validWhen: true,
    message:   passwordNotMatchMessage(),
  },
  {
    field:     'password_confirmation',
    method:    passwordMatch,
    validWhen: true,
    message:   confirmationNotMatchMessage(),
  },
  {
    field:     'password_confirmation',
    method:    passwordComplexity,
    args:      [{ name: 'confirmation' }],
    message:   complexityMessage(),
    validWhen: true,
  },
  {
    field:     'password_current',
    method:    passwordComplexity,
    args:      [{ name: 'current' }],
    message:   complexityMessage(),
    validWhen: true,
  },
  {
    field:     'password_new',
    method:    passwordComplexity,
    args:      [{ name: 'new' }],
    message:   complexityMessage(),
    validWhen: true,
  }
]);

export default ValidationRules;
