import React from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { cxInvalid } from '../../utils/helpers';
import { ContentBlock } from '../Wrappers';
import Feedback from './validation/Feedback';
import { storeProfileFormAction, toggleProfileFieldAction } from '../../actions/profiles';
import IconEyeClosed from '../icons/IconEyeClosed';
import IconEyeOpened from '../icons/IconEyeOpened';

class PasswordChange extends React.Component {

  renderPasswordRecord = (name) => {
    const { form, profile, storeProfileForm, form: { password } } = this.props;

    return (
      <div className="">
        <div className="form-group">
          <div
            className="zr-form-control-group zr-form-control-group-has-status zr-form-control-group-has-append focus"
          >
            <input
              value={password[name]}
              onChange={e => storeProfileForm({ password: { ...password, [name]: e.target.value } })}
              className={cxInvalid('form-control', profile, `password_${name}`)}
              type={form[`opened_${name}`] ? 'text' : 'password'}
              placeholder={i18n.t(`profile.password.${name}`)}
            />
            <button
              className={`zr-btn-icon zr-btn-icon-eye zr-eye ${form[`opened_${name}`] && 'opened'}`}
              type="button"
              onClick={() => storeProfileForm({ ...form.password, [`opened_${name}`]: !form[`opened_${name}`] })}
            >
              <IconEyeClosed className="icon icon-eye-closed" />
              <IconEyeOpened className="icon icon-eye-opened" />
            </button>
          </div>
          <Feedback model={profile} name={`password_${name}`} />
        </div>
      </div>
    );
  }

  renderPassword = () => {
    const { form: { password } } = this.props;

    if (!password.expanded) return <span className="password-dots">●●●●●●●●</span>;

    return (
      <div
        role="button"
        tabIndex="0"
        onKeyPress={() => {}}
        onClick={e => e.stopPropagation()}
      >
        { this.renderPasswordRecord('current') }
        { this.renderPasswordRecord('new') }
        { this.renderPasswordRecord('confirmation') }
      </div>
    );
  }


  render() {
    const { model: { email }, form: { password }, toggleProfileField } = this.props;
    const expandedClass = password.expanded ? 'expanded-background' : '';

    return (
      <ContentBlock className="zr-account_access_info flex-column password">
        <div className="border-bottom d-flex flex-column px-3 pb-2">
          <h3 className="mb-2 mt-2">{i18n.t('profile.access_info')}</h3>
        </div>
        <div className="justify-content-start align-items-end">
          <div className="form-section">
            <div className="border-bottom form-row not-expandeable mx-0 px-2 pt-3 pb-3">
              <div className="col-2">
                <span>{i18n.t('users.email')}</span>
              </div>
              <div className="col-1">
                <span>{email}</span>
              </div>
            </div>
            <div
              tabIndex="0"
              role="button"
              onKeyPress={() => {}}
              onClick={() => toggleProfileField('password')}
              className={`border-bottom form-row mx-0 px-2 pt-3 pb-3 ${expandedClass}`}
            >
              <div className="col-2">
                <span>{i18n.t('users.password')}</span>
              </div>
              <div className="col-2 password-form">
                { this.renderPassword() }
              </div>
            </div>
          </div>
        </div>
      </ContentBlock>
    );
  }

}

const mapStateToProps = state => ({
  form:    state.profile.form,
  model:   state.profile.model,
  profile: state.profile,
});

const mapDispatchToProps = dispatch => ({
  storeProfileForm:   data => dispatch(storeProfileFormAction(data)),
  toggleProfileField: data => dispatch(toggleProfileFieldAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChange);
