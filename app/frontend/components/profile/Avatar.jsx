// eslint-disable-next-line
import 'cropperjs/dist/cropper.css';
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { ContentBlock } from '../Wrappers';
import { storeProfileFormAction, storeProfileAction, toggleProfileFieldAction } from '../../actions/profiles';
import AvatarControls from '../avatar/AvatarControls';
import AvatarWithSizes from '../avatar/AvatarWithSizes';
import ImageCropper from '../images/ImageCropper';
import ImageInfo from '../images/ImageInfo';


class Avatar extends React.Component {

  onAvatarSelected = (files) => {
    const { storeProfile, form } = this.props;

    if (files && files[0]) {
      const [file] = files;
      file.blob = URL.createObjectURL(files[0]);
      storeProfile({ cropShow: true, form: { ...form, avatar: { ...form.avatar, file } } });
    }
  };

  onDelete = () => {
    const { storeProfileForm, form: { avatar } } = this.props;
    storeProfileForm({ avatar: { expanded: avatar.expanded, _destroy: true } });
  }

  onCropperSave = () => {
    const { form, storeProfile } = this.props;

    this.cropperRef.current.getCroppedCanvas().toBlob((blob) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onload = () => {

        storeProfile({
          cropShow: false,
          form:     { ...form, avatar: { ...form.avatar, base64: reader.result } },
        });
      };
    }, 'image/jpeg');
  };

  noCropperCancel = () => {
    const { storeProfile, form } = this.props;
    document.getElementById('avatar').value = '';
    storeProfile({ cropShow: false, form: { ...form, avatar: { ...form.avatar, file: null } } });
  };

  renderCropperModal = () => {
    const { form, profile } = this.props;
    this.cropperRef = React.createRef();

    if (!form.avatar) return null;

    return (
      <ImageCropper
        blob={_.get(form.avatar.file, 'blob')}
        show={profile.cropShow}
        onSave={this.onCropperSave}
        onCancel={this.noCropperCancel}
        ref={this.cropperRef}
        aspectRatio={1}
      />
    );
  }

  isExpanded = () => {
    const { form: { avatar } } = this.props;
    return avatar.expanded;
  }

  expandedClass = () => (this.isExpanded() ? 'expanded-background' : '')

  getAvatarSrc = () => {
    const { profile: { model }, profile: { form: { avatar } } } = this.props;

    const src = avatar && avatar.base64;
    return src || (!(!avatar || avatar._destroy) && (model.avatar && model.avatar.path));
  }

  renderAvatarForm = () => {
    if (!this.isExpanded()) return <AvatarWithSizes height="35" weight="35" src={this.getAvatarSrc()} />;
    const { isOffline } = this.props;

    return (
      <div
        role="button"
        tabIndex="0"
        onKeyPress={() => {}}
        onClick={e => e.stopPropagation()}
        className="d-flex flex-row p-3"
      >
        { <AvatarWithSizes height="100" weight="100" src={this.getAvatarSrc()} />}
        <div className="flex-column mr-5 ml-5">
          <ImageInfo />
          <AvatarControls
            isOffline={isOffline}
            onSelected={this.onAvatarSelected}
            src={this.getAvatarSrc()}
            onDelete={this.onDelete}
          />
        </div>
      </div>
    );
  }

  renderAvatar = () => {
    const { toggleProfileField } = this.props;

    return (
      <ContentBlock className="flex-column">
        <div className="d-flex border-bottom flex-column px-3 pb-2">
          <h3 className="mb-2">{i18n.t('profile.photo')}</h3>
        </div>
        <div className="justify-content-start align-items-end">
          <div className="plain-form form-section">
            <div
              role="button"
              tabIndex="0"
              onKeyPress={() => {}}
              onClick={() => toggleProfileField('avatar')}
              className={`border-bottom form-row mx-0 pt-3 px-2 py-2 ${this.expandedClass()}`}
            >
              <div className="col-2">
                <span>{i18n.t('profiles.avatar')}</span>
              </div>
              <div className="col-7">
                { this.renderAvatarForm() }
              </div>
            </div>
          </div>
        </div>
      </ContentBlock>
    );
  };

  render() {
    return (
      <div>
        {this.renderAvatar()}
        {this.renderCropperModal()}
      </div>
    );
  }

}

const mapStateToProps = state => ({
  isOffline: state.common.isOffline,
  form:      state.profile.form,
  profile:   state.profile,
});

const mapDispatchToProps = dispatch => ({
  storeProfile:       data => dispatch(storeProfileAction(data)),
  storeProfileForm:   data => dispatch(storeProfileFormAction(data)),
  toggleProfileField: data => dispatch(toggleProfileFieldAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Avatar);
