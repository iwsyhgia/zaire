import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import { ContentBlock } from '../Wrappers';
import Feedback from './validation/Feedback';
import { cxInvalid } from '../../utils/helpers';
import {
  storeProfileFormAction,
  toggleProfileFieldAction,
} from '../../actions/profiles';

class DetailsPersonal extends React.Component {

  isExpanded = (name) => {
    const { form } = this.props;
    return form[name].expanded;
  }

  expandedClass = name => (this.isExpanded(name) ? 'expanded-background' : '')

  renderRecord = (name) => {
    const { profile, form, storeProfileForm, toggleProfileField } = this.props;

    let record;
    if (form[name].expanded) {
      record = (
        <Fragment>
          <input
            value={form[name].value || ''}
            onClick={e => e.stopPropagation()}
            placeholder={i18n.t(`profiles.${name}`)}
            className={cxInvalid('form-control details', profile, name)}
            onChange={e => storeProfileForm({ [name]: { ...(form[name]), value: e.target.value } })}
          />
          <Feedback model={profile} name={name} />
        </Fragment>
      );
    } else {
      record = (<span>{profile.model[name]}</span>);
    }

    const divWrapperClass = `border-bottom form-row mx-0 pl-2 pt-3 pb-3 ${this.expandedClass(name)}`;

    return (
      <div
        role="button"
        tabIndex="0"
        onKeyPress={() => {}}
        onClick={() => toggleProfileField(name)}
        className={divWrapperClass}
      >
        <div className="col-2"><span>{i18n.t(`profiles.${name}`)}</span></div>
        <div className="col-3">{ record }</div>
      </div>
    );
  }

  render() {
    return (
      <ContentBlock className="zr-account__details flex-column">
        <div className="border-bottom d-flex flex-column py-2 px-3">
          <h3 className="mb-2">{i18n.t('profile.details')}</h3>
        </div>
        <div className="justify-content-start align-items-end">
          <div className="plain-form form-section">
            { this.renderRecord('first_name') }
            { this.renderRecord('last_name') }
            { this.renderRecord('phone_number') }
          </div>
        </div>
      </ContentBlock>
    );
  }

}

const mapStateToProps = state => ({
  profile: state.profile,
  form:    state.profile.form,
});

const mapDispatchToProps = dispatch => ({
  toggleProfileField: name => dispatch(toggleProfileFieldAction(name)),
  storeProfileForm:   data => dispatch(storeProfileFormAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPersonal);
