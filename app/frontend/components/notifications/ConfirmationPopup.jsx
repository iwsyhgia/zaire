import React, { Fragment } from 'react';  // eslint-disable-line
import cx from 'classnames';
import i18n from 'i18next';
import { PrimaryButton, SecondaryButton } from '../controls/Buttons';


const ConfirmationPopup = ({
  submitText = i18n.t('common.ok'),
  cancelText = i18n.t('common.cancel'),
  onSubmit = () => { },
  onHide = () => { },
  isSubmit = true,
  show,
  children,
}) => (
  <div
    className={cx('confirmation-popup modal fade', { 'show d-block': show })}
    tabIndex="-1"
    role="dialog"
    aria-hidden="true"
  >
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">
            {i18n.t('common.confirm')}
          </h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={onHide}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          {children}
        </div>
        <div className="modal-footer d-flex justify-content-end">
          {
            isSubmit
              ? (
                <Fragment>
                  <PrimaryButton className="mx-3" onClick={onSubmit}>{submitText}</PrimaryButton>
                  <SecondaryButton onClick={onHide}>{cancelText}</SecondaryButton>
                </Fragment>
              )
              : (
                <PrimaryButton onClick={onHide}>{cancelText}</PrimaryButton>
              )
          }
        </div>
      </div>
    </div>
  </div>
);

export default ConfirmationPopup;
