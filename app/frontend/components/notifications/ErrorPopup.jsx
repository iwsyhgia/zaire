import React from 'react';
import cx from 'classnames';
import i18n from 'i18next';
import IconError from '../icons/IconError';
import IconClose from '../icons/IconClose';


const ErrorPopup = ({ onHide, show, children }) => (
  <div
    className={cx('error-popup modal fade', { 'show d-block': show })}
    tabIndex="-1"
    role="dialog"
    aria-hidden="true"
  >
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">
            <IconError width="32" />
            <span className="px-3">
              {i18n.t('common.error')}
            </span>
          </h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={onHide}>
            <span aria-hidden="true">
              <IconClose width="12" color="#B8BDCF" />
            </span>
          </button>
        </div>
        <div className="modal-body">
          {children}
        </div>
        <div className="modal-footer d-flex justify-content-center">
          <button type="button" className="btn btn-primary" onClick={onHide}>
            {i18n.t('common.ok')}
          </button>
        </div>
      </div>
    </div>
  </div>
);

export default ErrorPopup;
