import React from 'react';
import cx from 'classnames';

const Notification = ({ className, style, children }) => (
  <div
    className={cx('zr-float-notification', className)}
    style={{
      ...style,
    }}
  >
    {children}
  </div>
);

export default Notification;
