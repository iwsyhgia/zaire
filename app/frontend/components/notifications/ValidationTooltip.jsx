import React from "react";
import cx from "classnames";


const ValidationTooltip = ({ className, style, children }) => {
    return (
      <div
        className="validation-tooltip"
        style={{
          ...style          
        }}
      >
        {children}
      </div>
    );
  }

  export default ValidationTooltip;