import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';

class ImageLoader extends React.PureComponent {

  static propTypes = {
    photos: PropTypes.arrayOf.isRequired,
    closeImage: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired,
  };

  render() {
    const { photos, closeImage, show } = this.props;

    return (
      <div className={cx('modal fade', { 'd-block show': show })} tabIndex="-1" role="dialog" aria-hidden="true" >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="d-flex justify-content-end">
              <button type="button" className="close mr-3 mt-2 ml-3" data-dismiss="modal" aria-label="Close" onClick={closeImage}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div id="carouselExampleControls" className="carousel slide" data-ride="carousel" data-interval="false">
                <div className="carousel-inner">
                  {photos}
                </div>
                <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true" />
                </a>
                <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div >
    );
  }
}

export default ImageLoader;
