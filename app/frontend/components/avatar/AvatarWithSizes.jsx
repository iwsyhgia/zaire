import React from 'react';
import DefaultAvatar from '../icons/DefaultAvatar';

const AvatarWithSizes = ({ width, height, src }) => (
  <div className="d-flex w-5 pb-2 align-items-center">
    <div className="rounded">
      { src
        ? <img src={src} width={width} height={height} alt="avatar" />
        : <DefaultAvatar width={width} height={height} />}
    </div>
  </div>
);

export default AvatarWithSizes;
