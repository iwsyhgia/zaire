import React from 'react';
import i18n from 'i18next';

const AvatarControls = ({ isOffline, src, onSelected, onDelete }) => (
  <div className="d-flex flex-column pt-2 avatar-file-input">
    <label htmlFor="avatar" className="">
      <input
        onChange={e => onSelected(e.target.files)}
        id="avatar"
        type="file"
        accept="image/*"
        disabled={isOffline}
      />
      <span className={`btn btn-primary ${isOffline ? 'disabled' : ''}`}>
        {` ${src ? i18n.t('common.change') : i18n.t('common.add')} `}
      </span>
    </label>
    {
      src
        && (
        <span
          tabIndex="0"
          role="button"
          className={`btn btn-primary ${isOffline ? 'disabled' : ''}`}
          disabled={isOffline}
          onClick={onDelete}
          onKeyPress={onDelete}
        >
          {i18n.t('common.delete')}
        </span>
        )
    }
  </div>
);

export default AvatarControls;
