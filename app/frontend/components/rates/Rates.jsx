import React from 'react';
import i18n from 'i18next';

import { PageBlock } from '../Wrappers';
import RateTabs from './RateTabs';
import RatePeriods from './RatePeriods';

const Rates = () => (
  <PageBlock title={i18n.t('rooms.rate_management')}>
    <RateTabs />
    <RatePeriods />
  </PageBlock>
);

export default Rates;
