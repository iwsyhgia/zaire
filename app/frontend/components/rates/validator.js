import i18n from 'i18next';

export const validateTextField = (errorValue) => {
  if (!errorValue || errorValue === '') return i18n.t('common.field_required');
  if (+errorValue <= 0) return i18n.t('common.field_positive');
  return null;
};


export default validateTextField;
