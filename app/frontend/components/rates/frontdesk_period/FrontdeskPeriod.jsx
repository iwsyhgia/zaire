import React from 'react';
import moment from 'moment';
import i18n from 'i18next';
import cx from 'classnames';
import { withFormApi } from 'informed';

import IconArrowDown from '../../icons/IconArrowDown';
import { PrimaryButton, LinkButton, SecondaryButton, SubmitButton } from '../../controls/Buttons';
import PriceTable from './PriceTable';

const FrontdeskPeriodComponent = ({
  isOpen,
  isEdit,
  toggleEdit,
  toggleCollapse,
  rooms,
  standartPeriod,
  isHover,
  onCollapseOver,
  onCollapseOut,
  formApi,
}) => (
  <div className="card">
    <div
      className="card-header zr-rate-collapse"
      onMouseOver={onCollapseOver}
      onMouseOut={onCollapseOut}
      onFocus={() => { }}
      onBlur={() => { }}
    >
      <div className="zr-arrow zr-width-flex-30" onClick={toggleCollapse} aria-hidden>
        <IconArrowDown width="10" height="10" className={cx({ 'zr-arrow-rotate': isOpen })} />
        <h3 className="mx-1">{i18n.t('rates.frontdesk_period')}</h3>
      </div>
      <div className="zr-cursor zr-width-flex-48" onClick={toggleCollapse} aria-hidden>
        <h3>
          {`${moment().startOf('year').format('DD MMM YYYY')}
            - ${moment().endOf('year').format('DD MMM YYYY')}`}
        </h3>
      </div>
      {
        isEdit
          ? (
            <div className="zr-button-group zr-width-flex-22">
              <SecondaryButton onClick={() => {
                formApi.reset();
                toggleEdit();
              }}
              >
                {i18n.t('common.cancel')}
              </SecondaryButton>
              <SubmitButton className="btn-primary">
                <span className="zr-inner-header-btn-text">{i18n.t('common.save')}</span>
              </SubmitButton>
            </div>
          )
          : (
            <div className={cx('zr-button-group zr-width-flex-22', { 'zr-hide-buttons': !isHover && !isOpen })}>
              <LinkButton to="#" className="copy-link-button" onClick={() => {}}>
                <h3>{i18n.t('rates.copy_period')}</h3>
              </LinkButton>
              <PrimaryButton className="btn-primary" onClick={toggleEdit}>
                <span className="zr-inner-header-btn-text">{i18n.t('common.edit')}</span>
              </PrimaryButton>
            </div>
          )
        }
    </div>

    <div id="collapseOne" className={cx('collapse', { show: isOpen })}>
      <div className="card-body">
        <PriceTable rooms={rooms} standartPeriod={standartPeriod} isEdit={isEdit} />
      </div>
    </div>
  </div>
);

const FrontdeskPeriod = withFormApi(FrontdeskPeriodComponent);

export default FrontdeskPeriod;
