import React from 'react';
import i18n from 'i18next';
import { CustomSelect } from '../../../controls/FormControls';
import { WEEK_DAYS, KIND_DISCOUNT_OPTIONS } from '../../../../constants/constants';

const PeriodTitle = ({ isEdit, standartPeriod }) => {
  const initialNRRate = KIND_DISCOUNT_OPTIONS().find(item => item.value === standartPeriod.discount_kind) || '';
  return (
    <div className="zr-reservation-thead-line">
      <div className="zr-reservation-thead-line-header zr-rates-frontdesk-thead-row">
        <div className="zr-rates-frontdesk-thead-col zr-width-flex-10" />
        <div className="zr-rates-frontdesk-thead-col zr-width-flex-70 zr-week-line">
          {
            WEEK_DAYS.map(day => (
              <div className="zr-rates-frontdesk-thead-col zr-width-flex-10" key={day.short_name}>
                {i18n.t(`common.${day.short_name}`).toUpperCase()}
              </div>
            ))
          }
        </div>
        <div className="zr-rates-frontdesk-thead-col zr-width-flex-20 zr-nr-col-text">
          {`${i18n.t('reservation.non_refundable').toUpperCase()} ${i18n.t('common.rate').toUpperCase()}`}
          {
            isEdit
              && (
                <CustomSelect
                  field="discount_kind"
                  className="plain-select zr-rates-dropdown"
                  classNamePrefix="plain-select"
                  initialValue={initialNRRate}
                  options={KIND_DISCOUNT_OPTIONS()}
                  placeholder={i18n.t('reservation.title')}
                />
              )
          }
        </div>
      </div>
    </div>
  );

};
export default PeriodTitle;
