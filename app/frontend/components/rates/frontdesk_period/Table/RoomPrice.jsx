import React from 'react';
import { Text, Scope, withFormState } from 'informed';
import i18n from 'i18next';
import cx from 'classnames';
import { WEEK_DAYS } from '../../../../constants/constants';
import { validateTextField } from '../../validator';

const RoomPriceComponent = ({
  room,
  standartPeriod,
  isEdit,
  formState,
}) => {
  const isErrorExist = formState.errors && formState.errors.rooms && formState.errors.rooms[room.name];
  const roomLine = standartPeriod.periods_room_types && standartPeriod.periods_room_types.find(
    currentRoom => currentRoom.room_type_id === room.id
  );
  if (!roomLine) return null;
  return (
    <div className="zr-rates-frontdesk-tbody-line">
      <div className="zr-rates-frontdesk-tbody-line-header zr-rates-frontdesk-tbody-row">
        <div className="zr-rates-frontdesk-tbody-col zr-width-flex-10">
          <p>{room.name}</p>
        </div>
        <Scope scope="rooms">
          <Scope scope={room.name}>
            {
              WEEK_DAYS.map(day => (
                <div className="zr-rates-frontdesk-tbody-col zr-width-flex-10" key={day.field}>
                  <Scope scope={day.field}>
                    <Text
                      type="number"
                      autoComplete="off"
                      field="lower_bound"
                      initialValue={
                        roomLine.rates[day.field]
                          ? roomLine.rates[day.field].lower_bound
                          : room.rate.lower_bound
                      }
                      className={cx('form-control', {
                        'is-invalid': isErrorExist && (formState.errors.rooms[room.name][day.field] || {}).lower_bound,
                      })}
                      disabled={!isEdit}
                      validate={validateTextField}
                    />
                  </Scope>
                </div>
              ))
            }
          </Scope>
        </Scope>
        <div className="zr-rates-frontdesk-tbody-col-currency-name zr-width-flex-5">{i18n.t('reservation.SAR')}</div>
        <Scope scope="discounts">
          <div className="zr-rates-frontdesk-tbody-col zr-width-flex-10">
            <Text
              field={room.name}
              className="form-control"
              initialValue={roomLine.discount_value}
              disabled={!isEdit}
            />
          </div>
        </Scope>
        <div className="zr-rates-frontdesk-tbody-col-currency-name zr-width-flex-5">
          {standartPeriod.discount_kind === 'amount' ? i18n.t('reservation.SAR') : '%'}
        </div>
      </div>
    </div>
  );

};

const RoomPrice = withFormState(RoomPriceComponent);
export default RoomPrice;
