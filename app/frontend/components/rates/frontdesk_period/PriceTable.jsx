import React from 'react';
import { withFormState } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';

import PeriodTitle from './Table/PeriodTitle';
import RoomPrice from './Table/RoomPrice';

const PriceTableComponent = ({ rooms, standartPeriod, isEdit, formState }) => (
  <div>
    <div className={cx('invalid-feedback', { 'zr-show-error': formState.errors.rooms })}>
      <h3>{i18n.t('common.field_required')}</h3>
    </div>
    <div>
      <PeriodTitle isEdit={isEdit} standartPeriod={standartPeriod} />
    </div>
    <div>
      {
        rooms && rooms.map(room => (
          <RoomPrice isEdit={isEdit} standartPeriod={standartPeriod} room={room} />
        ))
      }
    </div>
  </div>
);

const PriceTable = withFormState(PriceTableComponent);
export default PriceTable;
