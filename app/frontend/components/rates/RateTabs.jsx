import React from 'react';
import i18n from 'i18next';
import cx from 'classnames';
import { PrimaryButton } from '../controls/Buttons';

class RateTabs extends React.Component {

  state = {
    tabName: 'frontdesk',
  }

  checkTab = (tab) => {
    this.setState({ tabName: tab });
  };

  render() {
    const { tabName } = this.state;

    return (
      <div>
        <ul className="nav nav-tabs tabs">
          <li className="nav-item" onClick={() => this.checkTab('frontdesk')} aria-hidden>
            <p
              className={cx('tabs-navlink-custom', { 'tabs-custom-active': tabName === 'frontdesk' })}
            >
              <b>{i18n.t('common.reservations')}</b>
            </p>
          </li>
          <li className="nav-item" onClick={() => this.checkTab('otas')} aria-hidden>
            <p
              className={cx('tabs-navlink-custom', { 'tabs-custom-active': tabName === 'otas' })}
            >
              <b>{i18n.t('common.otas')}</b>
            </p>
          </li>
          <li className="nav-item" onClick={() => this.checkTab('website')} aria-hidden>
            <p
              className={cx('tabs-navlink-custom', { 'tabs-custom-active': tabName === 'website' })}
            >
              <b>{i18n.t('common.website')}</b>
            </p>
          </li>
        </ul>
        <div className="tab-content button-header">
          <PrimaryButton className="zr-inner-header-btn">
            <span className="zr-inner-header-btn-text">{i18n.t('common.add_special_period')}</span>
          </PrimaryButton>
        </div>
      </div>
    );
  }

}

export default RateTabs;
