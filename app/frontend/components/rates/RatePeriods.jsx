import React from 'react';
import { connect } from 'react-redux';
import { Form } from 'informed';
import { Transition } from 'react-transition-group';

import FrontdeskPeriod from './frontdesk_period/FrontdeskPeriod';
import Spinner from '../icons/Spinner';
import { getRoomTypes as getRoomTypesAction } from '../../actions/roomTypes';
import {
  getStandartPeriod as getStandartPeriodAction,
  updatePeriod as updatePeriodAction,
} from '../../actions/rates';

class RatePeriods extends React.Component {

  state = {
    isOpen:  false,
    isEdit:  false,
    isHover: false,
  }

  componentDidMount() {
    const { getRoomTypes, getStandartPeriod } = this.props;
    getRoomTypes();
    getStandartPeriod();
  }

  onSubmit = (data) => {
    const { updatePeriod, standart_period: standartPeriod } = this.props;
    const roomNames = Object.keys(data.rooms);
    const periodsAttrs = standartPeriod.periods_room_types && standartPeriod.periods_room_types.map((room, i) => (
      {
        id:              room.id,
        room_type_id:    room.room_type_id,
        discount_value: data.discounts[roomNames[i]] || null,
        rates:           data.rooms[roomNames[i]],
      }
    ));

    const requestForPeriod = {
      period_id:                     standartPeriod.id,
      name:                          standartPeriod.name,
      price_kind:                    'fixed',
      discount_kind:                 data.discount_kind.value || data.discount_kind,
      periods_room_types_attributes: [
        ...periodsAttrs
      ],
    };

    updatePeriod(requestForPeriod);
    this.toggleEdit();
  };

  toggleCollapse = () => {
    const { isOpen } = this.state;

    this.setState({ isOpen: !isOpen });
  }

  toggleEdit = () => {
    const { isEdit } = this.state;

    this.setState({ isEdit: !isEdit, isOpen: true });
  }

  onCollapseOver = () => {
    this.setState({ isHover: true });
  }

  onCollapseOut = () => {
    this.setState({ isHover: false });
  }

  render() {
    const { list, standart_period: standartPeriod } = this.props;
    const { isOpen, isEdit, isHover } = this.state;

    return (
      <Transition
        in={standartPeriod}
        timeout={500}
        classNames="fade"
      >
        { (state) => {
          switch (state) {
            case 'entered':
              return (
                <Form onSubmit={this.onSubmit}>
                  <FrontdeskPeriod
                    isEdit={isEdit}
                    isOpen={isOpen}
                    toggleEdit={this.toggleEdit}
                    toggleCollapse={this.toggleCollapse}
                    rooms={list}
                    standartPeriod={standartPeriod}
                    isHover={isHover}
                    onCollapseOver={this.onCollapseOver}
                    onCollapseOut={this.onCollapseOut}
                  />
                </Form>
              );
            default:
              return <Spinner className="zr-spinner-grey" />;
          }
        }}
      </Transition>
    );
  }

}

const mapStateToProps = state => ({
  list:            state.room_types.list,
  standart_period: state.rates.standart_period,
});

const mapDispatchToProps = dispatch => ({
  getRoomTypes:      () => { dispatch(getRoomTypesAction()); },
  updatePeriod:      (data) => { dispatch(updatePeriodAction(data)); },
  getStandartPeriod: () => { dispatch(getStandartPeriodAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(RatePeriods);
