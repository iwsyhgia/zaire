import React from 'react'; // eslint-disable-line
import { connect } from 'react-redux'; // eslint-disable-line

const OfflineSensitiveButton = ({
  children,
  disabled = false,
  isOffline,
  dispatch,
  ...props
}) => (
  // eslint-disable-next-line react/button-has-type
  <button {...props} disabled={isOffline || disabled}>
    {children}
  </button>
);

export default connect(state => ({ isOffline: state.common.isOffline }), null)(OfflineSensitiveButton);
