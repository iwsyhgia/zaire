import React from 'react';
import PropTypes   from 'prop-types';
import _ from 'lodash';
import Select from 'react-select';
import moment from 'moment';
import { AM_PM_HOURS_FORMAT, HOURS_24_FORMAT } from  '../../constants/constants';


class TimeSelect extends React.Component {

  static hoursAmPm(time) {
    const locale = moment.locale();
    moment.locale('en');
    const format = moment(time, 'HH:mm').format(AM_PM_HOURS_FORMAT);
    moment.locale(locale);
    return format;
  }

  static buildTimeOptions() {
    const timeOptions = [];
    const locale = moment.locale();
    moment.locale('en');
    _.range(24).forEach((hour) => {
      const time24 = moment({ hour, minute: '00' }).format(HOURS_24_FORMAT);
      const haltTime24 = moment({ hour, minute: '30' }).format(HOURS_24_FORMAT);

      timeOptions.push({ value: time24, label: TimeSelect.hoursAmPm(time24) });
      timeOptions.push({ value: haltTime24, label: TimeSelect.hoursAmPm(haltTime24) });
    });
    moment.locale(locale);

    return timeOptions;
  }

  constructor(props) {
    super(props);

    this.state = {
      value: {
        label: TimeSelect.hoursAmPm(this.props.defaultValue),
        value: this.props.defaultValue,
      },
    };

    this.timeOptions = TimeSelect.buildTimeOptions();
  }

  onChange = (value) => {
    this.props.onChange(value);
    this.setState({ value });
  };

  resetToDefault = () => {
    this.setState({
      value: {
        label: TimeSelect.hoursAmPm(this.props.defaultValue),
        value: this.props.defaultValue,
      },
    });
  };

  render() {
    return (
      <Select
        value={this.state.value}
        onChange={this.onChange}
        options={this.timeOptions}
        className={this.props.styles}
        styles={{ borderColor: 'red' }}
      />
    );
  }
}

TimeSelect.propTypes = {
  defaultValue: PropTypes.string.isRequired,
  onChange:     PropTypes.func.isRequired,
  styles:       PropTypes.string,
};

export default TimeSelect;
