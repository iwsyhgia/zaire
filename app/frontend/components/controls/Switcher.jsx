import React from 'react'; // eslint-disable-line
import PropTypes from 'prop-types';

class Switcher extends React.Component {

  static propTypes = {
    disabled:  PropTypes.bool,
    isOn:      PropTypes.bool,
    name:      PropTypes.string,
    onTurnOn:  PropTypes.func,
    onTurnOff: PropTypes.func,
  };

  static defaultProps = {
    disabled:  false,
    isOn:      false,
    name:      'switcher_default',
    onTurnOn:  () => { },
    onTurnOff: () => { },
  };

  // Event handlers
  onChange = (event) => {
    const isChecked = event.target.checked;
    const { onTurnOff, onTurnOn } = this.props;
    return isChecked ? onTurnOn() : onTurnOff();
  }

  render() {
    const { name, isOn, disabled } = this.props;
    return (
      <div className="switcher-control">
        <input name={name} type="checkbox" id="switcher" onChange={this.onChange} checked={isOn} disabled={disabled} />
        <label nesting="switcher" htmlFor="switcher" className="mb-0" disabled={disabled} />
      </div>
    );
  }

}

export default Switcher;
