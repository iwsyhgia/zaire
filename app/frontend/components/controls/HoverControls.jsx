import React from 'react';
import IconClose from '../icons/IconClose';
import IconEdit from '../icons/IconEdit';
import IconEye from '../icons/IconEye';

export const AmenityControls = ({ onRemove = () => { } }) => (
  <span className="controls-on-hover h-100">
    <span className="cursor-pointer" onClick={() => onRemove()} role="presentation">
      <IconClose width="14" height="14" />
    </span>
  </span>
);

export const RoomControls = ({ onRemove = () => { }, onEdit = () => { } }) => (
  <ul className="controls-on-hover room-controls list-inline h-100">
    <li className="list-inline-item cursor-pointer" onClick={onEdit} role="presentation">
      <IconEdit width="14" height="14" />
    </li>
    <li className="list-inline-item cursor-pointer" onClick={onRemove} role="presentation">
      <IconClose width="14" height="14" />
    </li>
  </ul>
);

export const ImageControlsEdit = ({ onRemove = () => { } }) => (
  <span className="controls-on-hover">
    <span className="cursor-pointer" onClick={onRemove} role="presentation">
      <IconClose width="14" height="14" />
    </span>
  </span>
);

export const ImageView = ({ dataToggle, dataTarget, dataId }) => (
  <div className="view-on-hover w-100 h-100 justify-content-center align-items-center">
    <div className="eye-icon" data-toggle={dataToggle} data-target={dataTarget} data-id={dataId}>
      <IconEye width="14" height="14" />
    </div>
  </div>
);
