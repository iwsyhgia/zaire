import React from 'react';
import cx from 'classnames';
import moment from 'moment';
import { Text, asField } from 'informed';
import Select, { AsyncCreatable } from 'react-select';
import Datetime from 'react-datetime';
import i18n from 'i18next';
import SelectSingleValue from './SelectSingleValue';
import IconCalendar from '../icons/IconCalendar';

export const FormGroupLabel = ({
  type = 'text',
  name,
  label = `label_${name}`,
  children,
  ...props
}) => (
  <div className="form-group">
    <label htmlFor={`input_${name}`}>{label}</label>
    {children || (
      <Text
        type={type}
        field={name}
        id={`input_${name}`}
        className="form-control"
        {...props}
      />
    )}
  </div>
);

export const CustomSelect = asField(({ fieldState, fieldApi, ...props }) => {
  const { setValue, setTouched } = fieldApi;
  const {
    onChange,
    onBlur,
    initialValue,
    optionValue,
    forwardedRef,
    placeholder,
    ...rest
  } = props;

  return (
    <Select
      {...rest}
      placeholder={placeholder || i18n.t('common.select')}
      ref={forwardedRef}
      value={optionValue}
      defaultValue={initialValue}
      noOptionsMessage={() => i18n.t('common.no_options')}
      onChange={(option) => {
        setValue(option.value);
        if (onChange) {
          onChange(option);
        }
      }}
      onBlur={(e) => {
        setTouched();
        if (onBlur) {
          onBlur(e);
        }
      }}
    />
  );
});

export const AsyncSelect = asField(({ fieldState, fieldApi, ...props }) => {
  const { setValue, setTouched } = fieldApi;
  const {
    onChange,
    onBlur,
    optionValue,
    noOptionsMessage = () => i18n.t('common.no_options'),
    loadingMessage = () => i18n.t('common.loading'),
    ...rest
  } = props;

  return (
    <AsyncCreatable
      {...rest}
      value={optionValue}
      noOptionsMessage={noOptionsMessage}
      loadingMessage={loadingMessage}
      formatCreateLabel={inputValue => (
        <span>
          {i18n.t('common.add')}
          {' '}
          <strong>{inputValue}</strong>
        </span>
      )}
      onChange={(option) => {
        setValue(option.value);
        if (onChange) {
          onChange(option);
        }
      }}
      onBlur={(e) => {
        setTouched();
        if (onBlur) {
          onBlur(e);
        }
      }}
    />
  );
});

export const CustomAsyncSelect = asField(({ fieldState, fieldApi, ...props }) => {
  const { setValue, setTouched } = fieldApi;
  const {
    onChange,
    onBlur,
    optionValue,
    ...rest
  } = props;

  return (
    <AsyncCreatable
      {...rest}
      value={optionValue}
      noOptionsMessage={() => i18n.t('common.no_options')}
      loadingMessage={() => i18n.t('common.loading')}
      formatCreateLabel={inputValue => (
        <span>
          {i18n.t('common.add')}
          {' '}
          <strong>{inputValue}</strong>
        </span>
      )}
      onChange={(option) => {
        setValue(option.value);
        if (onChange) {
          onChange(option);
        }
      }}
      onBlur={(e) => {
        setTouched();
        if (onBlur) {
          onBlur(e);
        }
      }}
      components={{
        SingleValue: SelectSingleValue,
      }}
    />
  );
});

export const CustomDatePicker = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue } = fieldApi;
  const {
    onChange,
    onBlur,
    initialValue,
    forwardedRef,
    dateFormat = 'DD MMM',
    isValidDate,
    label,
    ...rest
  } = props;

  // Fix date format. Add DD/MM/YYYY validation
  let currentValue = value || initialValue || '';

  if (Number.isInteger(currentValue)) {
    currentValue = new Date(currentValue);
  }

  const yesterday = Datetime.moment().subtract(1, 'day');
  const valid = current => current.isAfter(yesterday);
  const closeOnSelect = true;

  return (
    <div className="custom-datepicker">
      <IconCalendar width={20} />
      { label && <label htmlFor={label}>{label}</label> }
      <Datetime
        {...rest}
        ref={forwardedRef}
        defaultValue={currentValue}
        viewDate={currentValue}
        timeFormat={false}
        dateFormat={dateFormat}
        isValidDate={isValidDate || valid}
        closeOnSelect={closeOnSelect}
        onChange={(data) => {
          if (moment.isMoment(data) && data.isValid()) { // Prevent manual input edit
            setValue(data);
            if (onChange) {
              onChange(data);
            }
          }
        }}
      />
    </div>
  );
});

export const IncDec = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue } = fieldApi;
  const {
    onChange,
    onBlur,
    initialValue,
    forwardedRef,
    label = '',
    DecButton = {},
    IncButton = {},
    place,
    ...rest
  } = props;
  const decButtonClass = DecButton.class || '';
  const incButtonClass = IncButton.class || '';
  return (
    <div className="inc-dec-control d-flex">
      <button
        type="button"
        disabled={DecButton.disabled}
        className={cx('btn btn-small-icon', decButtonClass)}
        onClick={(e) => {
          const newValue = value - 1;
          setValue(newValue);
          if (DecButton.onClick) {
            DecButton.onClick(e, newValue);
          }
        }}
      >
        {DecButton.content || '-'}
      </button>
      <input
        {...rest}
        type="hidden"
        ref={forwardedRef}
        value={value || initialValue || 0}
        onChange={(e) => {
          setValue(e.target.value);
          if (onChange) {
            onChange(e);
          }
        }}
      />
      <span className="inc-dec-control__value fixed-width-90 text-center">{`${place === 'edit-form' ? initialValue : value} ${label}`}</span>
      <button
        type="button"
        className={cx('btn btn-small-icon', incButtonClass)}
        onClick={(e) => {
          const newValue = value + 1;
          setValue(newValue);
          if (IncButton.onClick) {
            IncButton.onClick(e, newValue);
          }
        }}
      >
        {IncButton.content || '+'}
      </button>
    </div>
  );
});

export const PositiveNumber = asField(({ fieldState, fieldApi, ...props }) => {
  const { value } = fieldState;
  const { setValue } = fieldApi;
  const {
    onChange,
    forwardedRef,
    ...rest
  } = props;

  return (
    <input
      {...rest}
      ref={forwardedRef}
      type="number"
      value={!value && value !== 0 ? '' : value}
      onChange={(e) => {
        if (Number(e.target.value) < 0) return;
        setValue(e.target.value);
        if (onChange) {
          onChange(e);
        }
      }}
    />
  );
});
