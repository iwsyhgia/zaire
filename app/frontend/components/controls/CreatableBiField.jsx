import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import i18n from 'i18next';
import IconPlus from '../icons/IconPlus';
import { Button } from './Buttons';
import BiField from './BiField';
import forwardRefWrapper from '../../containers/forwardRefWrapper';

const inputRef = React.createRef();

class CreatableBiField extends React.Component {

  static propTypes = {
    onCreateItem: PropTypes.func,
    onUpdate:     PropTypes.func,
    validValue:   PropTypes.func,
    placeholder:  PropTypes.string,
    type:         PropTypes.string,
    maxValue:     PropTypes.number,
  };

  static defaultProps = {
    onCreateItem: () => {
    },
    onUpdate: () => {
    },
    validValue:  () => true,
    placeholder: '',
    type:        'text',
    maxValue:    10000,
  };

  state = {
    customValue:   '',
    customValueBi: '',
    initValue:     '',
    isEdit:        false,
    isInvalid:     false,
  };

  onKeyDown = (e) => {
    const { isEdit } = this.state;
    if (e.key === 'Enter') {
      e.preventDefault();
      e.stopPropagation();
      if (isEdit) {
        this.onUpdate();
      } else {
        this.onSubmit();
      }
    }
  };

  // Event handlers
  handleChange = (event) => {
    const { validValue, maxValue } = this.props;
    const val = event.target.value;
    if (validValue(val)) this.setState({ customValue: event.target.value });
    if (val <= maxValue) {
      this.setState({ isInvalid: false });
    } else {
      this.setState({ isInvalid: true });
    }
  };

  handleChangeBi = (event) => {
    const { validValue, maxValue } = this.props;
    const val = event.target.value;
    if (validValue(val)) this.setState({ customValueBi: event.target.value });
    if (val <= maxValue) {
      this.setState({ isInvalid: false });
    } else {
      this.setState({ isInvalid: true });
    }
  };

  onSubmit = () => {
    const { customValue, customValueBi } = this.state;
    const { onCreateItem } = this.props;
    this.setState({ customValue: '', customValueBi: '' });
    onCreateItem(customValue, customValueBi);
  };

  onUpdate = () => {
    const { initValue, customValue } = this.state;
    const { onUpdate } = this.props;
    const oldValue = initValue;
    this.setState({ initValue: '', customValue: '', isEdit: false });
    onUpdate(oldValue, customValue);
  };

  onCancel = () => {
    this.setState({ customValue: '', customValueBi: '', isEdit: false });
  };

  focus = (value = '', isEdit = true) => {
    this.setState({ initValue: value, customValue: value, isEdit });
    inputRef.current.focus();
  };

  render() {
    const { type, placeholder } = this.props;
    const { customValue, customValueBi, isEdit, isInvalid } = this.state;

    const field = (
      <input
        ref={inputRef}
        type={type}
        dir="ltr"
        className={cx('form-control', {
          'is-invalid': isInvalid && type === 'number',
        })}
        placeholder={placeholder}
        value={customValue}
        onChange={this.handleChange}
        onKeyDown={this.onKeyDown}
      />
    );
    const arField = (
      <input
        type={type}
        dir="rtl"
        className={cx('form-control', {
          'is-invalid': isInvalid && type === 'number',
        })}
        placeholder={placeholder}
        value={customValueBi}
        onChange={this.handleChangeBi}
        onKeyDown={this.onKeyDown}
      />
    );
    const inputGroup = (
      <BiField
        isFirstEmpty={customValue.length === 0}
        FirstField={field}
        isSecondEmpty={customValueBi.length === 0}
        SecondField={arField}
      />
    );

    return (
      <div className={cx('creatable-item', {
        'number-field-input': isInvalid && type === 'number',
      })}
      >
        <div className="input-group">
          {inputGroup}
          <div className="invalid-feedback number-field-validation-text">
            {i18n.t('rooms.value_less_than_10000')}
          </div>
          <div className="input-group-append">
            {(isEdit)
              ? (
                <Fragment>
                  <Button className="btn-link" type="button" disabled={isInvalid} onClick={this.onUpdate}>
                    {i18n.t('common.save')}
                  </Button>
                  <Button className="btn-link" type="button" onClick={this.onCancel}>
                    {i18n.t('common.close')}
                  </Button>
                </Fragment>
              )
              : (
                <Button type="button" disabled={type === 'number' && isInvalid} onClick={this.onSubmit}>
                  <IconPlus width="16" height="16" />
                </Button>
              )
            }
          </div>
        </div>
      </div>
    );
  }

}

export default forwardRefWrapper(CreatableBiField);
