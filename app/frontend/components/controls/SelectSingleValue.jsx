import React, { Component } from 'react';
import { components } from 'react-select';

class SelectSingleValue extends Component {

  constructor(props) {
    super(props);

    this.state = {
      createdOption: null,
      value:         props.data.value,
    };
  }

  onEditChange = (e) => {
    const { createdOption } = this.state;

    e.stopPropagation();

    const newValue = e.target.value;
    const newOption = {
      ...createdOption,
      label: newValue,
      value: newValue,
    };
    this.setState({
      createdOption: newOption,
      value:         newValue,
    });
  }

  onKeyDown = (e) => {
    if (e.target.value.length > 0) {
      // important: this will allow the use of backspace in the input
      e.stopPropagation();
    } else if (e.key === 'Enter') {
      // if value is empty, press enter to delete custom option
      e.stopPropagation();
      const { setValue } = this.props;
      setValue(null);
    }
  }

  onBlur = (e) => {
    const { setValue } = this.props;
    setValue(e.target.value, 'create-option');
  }

  render() {
    const props = this.props;
    const { value } = this.state;

    // eslint-disable-next-line
    return (props.data.__isNew__ || !!props.selectProps.isNewOption) ? (
      <input
        type="text"
        value={value}
        onChange={this.onEditChange}
        onBlur={this.onBlur}
        onKeyDown={this.onKeyDown}
      />
    ) : (
      <components.SingleValue {...props} />
    );
  }

}

export default SelectSingleValue;
