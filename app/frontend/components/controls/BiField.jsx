import React from 'react';
import cx from 'classnames';
import { Nav, Tab } from 'react-bootstrap';

const BiField = ({
  isFirstEmpty = true,
  FirstField = null,
  isSecondEmpty = true,
  SecondField = null,
  isPlain = false,
}) => (
  <div className={cx('zr-blang-container', { 'zr-blang-container-plain': isPlain })}>
    <Tab.Container defaultActiveKey="en" id="b-lang-name">
      <Nav variant="tabs" className="zr-blang-nav">
        <Nav.Item>
          <Nav.Link eventKey="en" className="zr-blang-nav-item zr-blang-nav-en">
            <span className={cx({ 'has-value': !isFirstEmpty })}>En</span>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="ar" className="zr-blang-nav-item zr-blang-nav-ar">
            <span className={cx({ 'has-value': !isSecondEmpty })}>Ar</span>
          </Nav.Link>
        </Nav.Item>
      </Nav>
      <Tab.Content className="zr-blang-tab-content">
        <Tab.Pane eventKey="en" className="zr-blang-field zr-blang-field-en">
          {FirstField}
        </Tab.Pane>
        <Tab.Pane eventKey="ar" className="zr-blang-field zr-blang-field-ar">
          {SecondField}
        </Tab.Pane>
      </Tab.Content>
    </Tab.Container>
  </div>
);

export default BiField;
