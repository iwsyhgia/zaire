import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'; // eslint-disable-line
import OfflineSensitiveButton from './OfflineSensitiveButton';

export const Button = ({
  className,
  children,
  ...props
}) => (
  <button type="button" className={cx('btn', className)} {...props}>
    {children}
  </button>
);

export const PrimaryButton = ({
  className,
  children,
  ...props
}) => (
  <OfflineSensitiveButton type="button" className={cx('btn btn-primary', className)} {...props}>
    {children}
  </OfflineSensitiveButton>
);

export const SecondaryButton = ({
  className,
  children,
  ...props
}) => (
  <OfflineSensitiveButton type="button" className={cx('btn btn-secondary', className)} {...props}>
    {children}
  </OfflineSensitiveButton>
);

export const CancelButton = ({
  className,
  children,
  ...props
}) => (
  <button type="button" className={cx('btn', className)} {...props}>
    {children}
  </button>
);

export const SubmitButton = ({
  className,
  children,
  ...props
}) => (
  <OfflineSensitiveButton type="submit" className={cx('btn btn-primary', className)} {...props}>
    {children}
  </OfflineSensitiveButton>
);

export const LinkButton = connect(state => ({ isOffline: state.common.isOffline }), null)(({
  className,
  children,
  disabled = false,
  isOffline,
  ...props
}) => (
  <Link className={cx('btn', className, { disabled: isOffline })} disabled={isOffline || disabled} {...props}>
    {children}
  </Link>
));
