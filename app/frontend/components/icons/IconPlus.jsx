import React from 'react';

const IconPlus = props => (
  <svg viewBox="0 0 16 16" {...props}>
    <g transform="translate(-748 -222)">
      <g fill="#fff" stroke="#4ba0e1" transform="translate(755 222)">
        <rect stroke="none" width="2" height="16" />
        <rect fill="none" x="0.5" y="0.5" width="1" height="15" />
      </g>
      <g fill="#fff" stroke="#4ba0e1" transform="translate(764 229) rotate(90)">
        <rect stroke="none" width="2" height="16" />
        <rect fill="none" x="0.5" y="0.5" width="1" height="15" />
      </g>
    </g>
  </svg>
);

export default IconPlus;
