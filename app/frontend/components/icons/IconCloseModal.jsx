import React from 'react';

const IconCloseModal = props => (
  <svg viewBox="0 0 512 512" {...props}>
    <path d="M511.478 451.401l-192.777-192.777 189.668-189.668-65.296-65.296-189.668 189.668-192.777-192.777-61.15 61.15 192.777 192.777-188.631 188.631 65.296 65.296 188.632-188.632 192.777 192.777z" />
  </svg>
);

export default IconCloseModal;
