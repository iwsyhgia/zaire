import React from 'react';

const IconArrowDown = props => (
  <svg viewBox="0 0 11 7" {...props}>
    <path d="M8.1,11.6,2.6,6.041,4.026,4.6,8.1,8.718,12.174,4.6,13.6,6.041Z" transform="translate(-2.6 -4.6)" />
  </svg>
);

export default IconArrowDown;
