import React from 'react';

const IconEye = props => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.833 11.653" {...props}>
    <g fill='#fff' transform="translate(-1079.423 -549.969)">
      <path className="a" d="M8.917,161.525A9.791,9.791,0,0,0,.036,167.19a.382.382,0,0,0,0,.323,9.794,9.794,0,0,0,17.762,0,.382.382,0,0,0,0-.323A9.791,9.791,0,0,0,8.917,161.525Zm0,9.847a4.02,4.02,0,1,1,4.02-4.02A4.019,4.019,0,0,1,8.917,171.372Z" transform="translate(1079.423 388.444)" />
      <circle className="a" cx="2.118" cy="2.118" r="2.118" transform="translate(1086.221 553.677)" />
    </g>
  </svg>
);

export default IconEye;

