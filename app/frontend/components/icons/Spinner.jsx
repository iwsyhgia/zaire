import React from 'react';
import cx from 'classnames';

const Spinner = ({ className }) => (
  <div className={cx('zr-spinner-box', className)}>
    <div className="zr-spinner" />
  </div>
);

export default Spinner;
