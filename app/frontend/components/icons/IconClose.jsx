import React from 'react';

const IconClose = (props) => {
  const color = props.color || '#fff';
  return (
    <svg viewBox="0 0 12.728 12.728" {...props}>
      <g transform="translate(-619.636 -293.636)">
        <g fill={color} stroke={color} transform="translate(630.95 293.636) rotate(45)">
          <rect stroke="none" width="2" height="16" />
          <rect fill="none" x="0.5" y="0.5" width="1" height="15" />
        </g>
        <g fill={color} stroke={color} transform="translate(632.364 304.95) rotate(135)">
          <rect stroke="none" width="2" height="16" />
          <rect fill="none" x="0.5" y="0.5" width="1" height="15" />
        </g>
      </g>
    </svg>
  );
};

export default IconClose;
