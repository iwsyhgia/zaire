import React from 'react';

export const IconReservations = props => (
  <svg viewBox="0 0 22 22" {...props}>
    <path className="menu-icon" d="M4,1V3H3A2,2,0,0,0,1,5V21a2,2,0,0,0,2,2H21a2,2,0,0,0,2-2V5a2,2,0,0,0-2-2H20V1H18V3H6V1ZM3,5H21V7H3ZM3,9H21l0,12H3Zm11.8,2.3-3.3,3.3L9.7,12.8,8.3,14.2l3.2,3.2,4.7-4.7Z" transform="translate(-1 -1)" />
  </svg>
);
export const IconInventory = props => (
  <svg viewBox="0 0 16 28.999" {...props}>
    <path className="menu-icon" d="M15.731,1.006A8.014,8.014,0,0,0,8.621,5.9a2.994,2.994,0,0,0,3.266,4.047A3.32,3.32,0,0,0,14.215,8.1a1.984,1.984,0,0,1,2.18-1.059A2.084,2.084,0,0,1,18,9.115V12H11a3.015,3.015,0,0,0-3,3V27a3.015,3.015,0,0,0,3,3H21a3.015,3.015,0,0,0,3-3V9.281A8.185,8.185,0,0,0,16.375,1.01Q16.05.995,15.731,1.006Zm.553,2A6.185,6.185,0,0,1,22,9.281V27a.984.984,0,0,1-1,1H11a.984.984,0,0,1-1-1V15a.984.984,0,0,1,1-1h9V9.115a4,4,0,0,0-7.566-1.926,1.382,1.382,0,0,1-.893.793.925.925,0,0,1-1.076-1.3A5.991,5.991,0,0,1,16.283,3.008ZM12,17v2h6V17Zm0,4v2h4V21Z" transform="translate(-8 -1.001)" />
  </svg>
);
export const IconRates = props => (
  <svg viewBox="0 0 26 22" {...props}>
    <path className="menu-icon" d="M14,5a11,11,0,0,0,0,22,10.882,10.882,0,0,0,2-.187A10.85,10.85,0,0,0,18,27,11,11,0,0,0,18,5a10.851,10.851,0,0,0-2,.188A10.883,10.883,0,0,0,14,5Zm0,2a9,9,0,1,1-9,9A8.987,8.987,0,0,1,14,7Zm7,.531a8.977,8.977,0,0,1,0,16.938A10.965,10.965,0,0,0,21,7.531ZM13,11a1.984,1.984,0,0,1-2,2v2a3.928,3.928,0,0,0,2-.562V19H11v2h6V19H15V11Z" transform="translate(-3 -5)" />
  </svg>
);
export const IconReport = props => (
  <svg viewBox="0 0 22 20" {...props}>
    <path className="menu-icon" d="M4,6V26H26V6ZM6,8H24V24H6Zm15.281,2.281L19,12.563l-1.281-1.281-1.437,1.438,2,2,.719.688.719-.687,3-3ZM8,12v2h6V12Zm13.281,4.281L19,18.563l-1.281-1.281-1.437,1.438,2,2,.719.688.719-.687,3-3ZM8,18v2h6V18Z" transform="translate(-4 -6)" />
  </svg>
);
export const IconGuests = props => (
  <svg viewBox="0 0 22 14.143" {...props}>
    <path className="menu-icon" d="M8.071,3a3.913,3.913,0,0,0-1.878,7.354A7.069,7.069,0,0,0,1,17.143H15.143A7.065,7.065,0,0,0,14.59,14.4a4.008,4.008,0,0,1,2.836-1.185,3.956,3.956,0,0,1,4,3.929H23a5.506,5.506,0,0,0-3.72-5.162,3.931,3.931,0,1,0-3.6-.025,5.62,5.62,0,0,0-1.9,1.05,7.078,7.078,0,0,0-3.83-2.652A3.913,3.913,0,0,0,8.071,3ZM17.5,6.143A2.357,2.357,0,1,1,15.143,8.5,2.342,2.342,0,0,1,17.5,6.143Z" transform="translate(-1 -3)" />
  </svg>
);
export const IconReviews = props => (
  <svg viewBox="0 0 23 22.043" {...props}>
    <path className="menu-icon" d="M13,4C6.94,4,2,8.246,2,13.563a9.122,9.122,0,0,0,4.286,7.525,2.769,2.769,0,0,1-.131.772,8.045,8.045,0,0,1-1.2,2.483l-.489.7h.856a6.616,6.616,0,0,0,4.938-2.24A11.878,11.878,0,0,0,13,23.123c6.059,0,11-4.246,11-9.561S19.06,4,13,4Zm0,.957c5.537,0,10.043,3.86,10.043,8.607s-4.506,8.6-10.043,8.6a10.943,10.943,0,0,1-2.518-.3l-.568-.133-.377.441A5.64,5.64,0,0,1,6.3,24a8.313,8.313,0,0,0,.779-1.9,3.8,3.8,0,0,0,.164-.981l.022-.532-.443-.3a8.189,8.189,0,0,1-3.863-6.733C2.957,8.816,7.463,4.957,13,4.957ZM12.963,8.3a.48.48,0,0,0-.447.306L11.431,11.43l-3.019.164a.478.478,0,0,0-.277.848l2.346,1.906L9.7,17.268a.477.477,0,0,0,.461.6.484.484,0,0,0,.26-.077l2.537-1.642L15.5,17.793a.477.477,0,0,0,.719-.525l-.777-2.92,2.346-1.906a.478.478,0,0,0-.276-.85L14.5,11.43,13.409,8.611A.478.478,0,0,0,12.963,8.3Z" transform="translate(-1.5 -3.5)" />
  </svg>
);
export const IconHotelSettings = props => (
  <svg viewBox="0 0 22 20" {...props}>
    <path className="menu-icon" d="M3,4A1,1,0,0,0,2,5V23a1,1,0,0,0,1,1H23a1,1,0,0,0,1-1V5a1,1,0,0,0-1-1ZM4,6H22V22H16V17a1,1,0,0,0-1-1H11a1,1,0,0,0-1,1v5H4ZM6,8v2H8V8Zm4,0v2h2V8Zm4,0v2h2V8Zm4,0v2h2V8ZM6,12v2H8V12Zm4,0v2h2V12Zm4,0v2h2V12Zm4,0v2h2V12ZM6,16v2H8V16Zm12,0v2h2V16Zm-6,2h2v4H12Z" transform="translate(-2 -4)" />
  </svg>
);
export const IconRooms = props => (
  <svg viewBox="0 0 22 19" {...props}>
    <path className="menu-icon" d="M3,2A2,2,0,0,0,1,4V18a2,2,0,0,0,2,2v1H5V20H19v1h2V20a2,2,0,0,0,2-2V4a2,2,0,0,0-2-2ZM3,4H21v6h-.906V9.813A1.787,1.787,0,0,0,18.313,8h-3.5A1.814,1.814,0,0,0,13,9.813V10H11.094V9.813A1.787,1.787,0,0,0,9.313,8h-3.5A1.814,1.814,0,0,0,4,9.813V10H3Zm0,8H21v6H3Z" transform="translate(-1 -2)" />
  </svg>
);
export const IconUsers = props => (
  <svg viewBox="0 0 22 14.143" {...props}>
    <path className="menu-icon" d="M15.929,3a3.913,3.913,0,0,1,1.878,7.354A7.069,7.069,0,0,1,23,17.143H8.857A7.065,7.065,0,0,1,9.41,14.4a4.008,4.008,0,0,0-2.836-1.185,3.956,3.956,0,0,0-4,3.929H1A5.506,5.506,0,0,1,4.72,11.98a3.931,3.931,0,1,1,3.6-.025,5.62,5.62,0,0,1,1.9,1.05,7.078,7.078,0,0,1,3.83-2.652A3.913,3.913,0,0,1,15.929,3ZM6.5,6.143A2.357,2.357,0,1,0,8.857,8.5,2.342,2.342,0,0,0,6.5,6.143Z" transform="translate(-1 -3)" />
  </svg>
);
export const IconAccount = props => (
  <svg viewBox="0 0 20 22" {...props}>
    <path className="menu-icon" d="M16,5a7,7,0,0,0-3.906,12.813A10.029,10.029,0,0,0,6,27H8a7.988,7.988,0,0,1,3-6.25l4.313,3.969.688.656.688-.656L21,20.75A7.988,7.988,0,0,1,24,27h2a10.029,10.029,0,0,0-6.094-9.187A7,7,0,0,0,16,5Zm0,2a5,5,0,1,1-5,5A4.985,4.985,0,0,1,16,7Zm0,12a8.009,8.009,0,0,1,3.219.656l-3.219,3-3.219-3A8.009,8.009,0,0,1,16,19Z" transform="translate(-6 -5)" />
  </svg>
);
export const IconArrowDown = props => (
  <svg viewBox="0 0 11 7" {...props}>
    <path d="M8.1,11.6,2.6,6.041,4.026,4.6,8.1,8.718,12.174,4.6,13.6,6.041Z" transform="translate(-2.6 -4.6)" />
  </svg>
);
