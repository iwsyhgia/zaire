import React from 'react';

const IconLogout = props => (
  <svg viewBox="0 0 14.273 15" {...props}>
    <g transform="translate(0 37)">
      <path d="M10.235,2.033V3.948a5.392,5.392,0,1,1-5.354.243V2.2A7.171,7.171,0,1,0,14.9,8.765,7.179,7.179,0,0,0,10.235,2.033Z" transform="translate(-0.63 -37.949)" />
      <path d="M11.3,8.12a.975.975,0,0,0,.994-.954V.957a.995.995,0,0,0-1.988,0V7.166A.975.975,0,0,0,11.3,8.12Z" transform="translate(-4.306 -37)" />
    </g>
  </svg>
);

export default IconLogout;
