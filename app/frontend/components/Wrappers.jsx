import React from 'react';
import cx from 'classnames';

export const PageBlock = ({
  title = '',
  subtitle = null,
  className,
  controls = null,
  children,
}) => (
  <div className={cx('zr-inner', className)}>
    <div className="zr-inner-header">
      <h2 className="zr-inner-header-title">
        {title}
        {
          subtitle && (
          <span className="zr-inner-header-subtitle" style={{ fontWeight: 'normal' }}>
            {` — ${subtitle}`}
          </span>
          )
        }
      </h2>
      {!!controls && <ul className="zr-inner-header-tools">{controls}</ul>}
    </div>
    <div className="zr-inner-content">
      {children}
    </div>
  </div>
);

export const ContentBlock = ({ className, children }) => (
  <div className={cx('content-block d-flex', className)}>
    {children}
  </div>
);

export const ContentGreyBlock = ({ className, children }) => (
  <div className={cx('content-block grey-block d-flex', className)}>
    {children}
  </div>
);

export const SectionBlock = ({ className, title, children }) => (
  <div className={cx('zr-reservation-widget', className)}>
    <div className="zr-reservation-widget-inner">
      <div className="zr-reservation-widget-header">{title}</div>
      <div className="zr-reservation-widget-content">{children}</div>
    </div>
  </div>
);

export const Button = ({
  loading,
  type,
  className,
  children,
  ...props
}) => (
  <button
    type={type}
    className={cx('btn', className)}
    {...props}
  >
    {children}
  </button>
);
