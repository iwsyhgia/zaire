import React from 'react';
import i18n from 'i18next';
import cx from 'classnames';
import TableCell from '../columns/Column';

const TableTitle = ({ isViewInvoice }) => (
  <div className="zr-reservation-thead-row">
    <TableCell
      isTitle
      text={i18n.t('reservation.description')}
      columnClasses={cx('zr-col-description', {
        'zr-width-flex-39 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    />
    <TableCell
      isTitle
      text={i18n.t('reservation.added_last_edited')}
      columnClasses={cx('zr-col-date', {
        'zr-width-flex-19 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    />
    <TableCell
      isTitle
      text={i18n.t('reservation.charges')}
      columnClasses={cx('zr-col-charge', {
        'zr-width-flex-10 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    />
    <TableCell
      isTitle
      text={i18n.t('reservation.discount')}
      columnClasses={cx('zr-col-discount', {
        'zr-width-flex-11 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    />
    <TableCell
      isTitle
      text={i18n.t('reservation.payment')}
      columnClasses={cx({
        'zr-col-payment zr-width-flex-21 zr-width-flex-md-28': !isViewInvoice,
        'zr-col zr-width-flex-20':                             isViewInvoice,
      })}
    />
  </div>
);

export default TableTitle;
