import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import i18n from 'i18next';
import cx from 'classnames';

import TableCell from '../columns/Column';
import TableRowActions from '../columns/ButtonsColumn';
import HistoryList from '../History';
import { howDidWePay, generateDescription } from '../../../../../utils/payments';
import { INVOICE_TYPES } from '../../../../../constants/constants';


const TableRow = ({
  payment = {},
  currentReservation,
  userName,
  nightCount,
  handleResetForm,
  handleShow,
  tableRowShowed,
  paymentsID,
  refunds,
  lastCompletedAudit,
  isViewInvoice,
}) => {
  if (!lastCompletedAudit.created_at) return null;
  return (
    <div className={cx({
      'zr-invoice-tbody-line':     isViewInvoice,
      'zr-reservation-tbody-line': !isViewInvoice,
    })}
    >
      <div className="zr-reservation-tbody-line-header zr-reservation-tbody-row">
        <TableCell
          tableRowShowed={tableRowShowed}
          handleShow={handleShow}
          payment={payment}
          isViewInvoice={isViewInvoice}
          text={
            payment.description
              ? <p>{generateDescription(payment)}</p>
              : (
                <div>
                  <p>
                    {`${currentReservation.guest.first_name} ${currentReservation.guest.last_name}`}
                  </p>
                  <p>
                    {payment.invoiceType === INVOICE_TYPES.PAYMENT && howDidWePay(payment)}
                  </p>
                </div>
              )
          }
          columnClasses={cx('zr-col-description', {
            'zr-width-flex-39 zr-width-flex-md-18': !isViewInvoice,
            'zr-width-flex-20':                     isViewInvoice,
          })}
        />
        <TableCell
          tableRowShowed={tableRowShowed}
          handleShow={handleShow}
          payment={payment}
          isViewInvoice={isViewInvoice}
          text={(
            <span>
              <span>{`${moment(payment.created_at).format('DD/MM/YYYY')}`}</span>
              {
                payment.creation_way === 'automated'
                  ? <span>{i18n.t('common.by_system')}</span>
                  : <span>{i18n.t('common.by_guest', { name: userName })}</span>
              }
            </span>
          )}
          columnClasses={cx('zr-col-date', {
            'zr-width-flex-19 zr-width-flex-md-18': !isViewInvoice,
            'zr-width-flex-20':                     isViewInvoice,
          })}
        />
        <TableCell
          tableRowShowed={tableRowShowed}
          handleShow={handleShow}
          payment={payment}
          isViewInvoice={isViewInvoice}
          text={payment.invoiceType === INVOICE_TYPES.CHARGE && `${+payment.amount} ${i18n.t('reservation.SAR')}`}
          columnClasses={cx('zr-col-charge', {
            'zr-width-flex-10 zr-width-flex-md-18': !isViewInvoice,
            'zr-width-flex-20':                     isViewInvoice,
          })}
        />
        <TableCell
          tableRowShowed={tableRowShowed}
          handleShow={handleShow}
          payment={payment}
          isViewInvoice={isViewInvoice}
          text={
            payment.invoiceType === INVOICE_TYPES.DISCOUNT
              && `${+payment.value} ${payment.kind === 'amount' ? i18n.t('reservation.SAR') : '%'}`
          }
          columnClasses={cx('zr-col-discount', {
            'zr-width-flex-11 zr-width-flex-md-18': !isViewInvoice,
            'zr-width-flex-20':                     isViewInvoice,
          })}
        />
        <TableCell
          tableRowShowed={tableRowShowed}
          handleShow={handleShow}
          payment={payment}
          isViewInvoice={isViewInvoice}
          text={
            (payment.invoiceType === INVOICE_TYPES.REFUND && `-${+payment.amount} ${i18n.t('reservation.SAR')}`)
            || (payment.invoiceType === INVOICE_TYPES.PAYMENT && `${+payment.amount} ${i18n.t('reservation.SAR')}`)
          }
          columnClasses={cx({
            'zr-col-payment zr-width-flex-21 zr-width-flex-md-28': !isViewInvoice,
            'zr-col zr-width-flex-20':                             isViewInvoice,
          })}
        />
        { (!isViewInvoice && !(payment.invoiceType === INVOICE_TYPES.CHARGE || payment.creation_way === 'automated')
            && (moment(payment.created_at).isAfter(lastCompletedAudit.created_at)))
          && (
            <TableRowActions
              handleResetForm={handleResetForm}
              handleShow={handleShow}
              invoiceId={currentReservation.invoice_id}
              paymentId={payment.id}
              paymentType={payment.invoiceType}
              refundsList={refunds}
            />
          )
        }
      </div>
      {
        !isViewInvoice && (
          <HistoryList
            currentReservation={currentReservation}
            versionsList={payment.versions}
            paymentType={payment.invoiceType}
            paymentID={payment.id}
            userName={userName}
            nightCount={nightCount}
            tableRowShowed={tableRowShowed}
            handleShow={handleShow}
            paymentsList={paymentsID}
            currentPayment={payment}
          />
        )
      }
    </div>
  );

};

const mapStateToProps = state => ({
  lastCompletedAudit: state.audit.lastCompleted,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(TableRow);
