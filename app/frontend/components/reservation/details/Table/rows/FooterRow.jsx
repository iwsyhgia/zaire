import React from 'react';
import i18n from 'i18next';
import cx from 'classnames';

const FooterRow = ({ text, payment, isViewInvoice }) => (
  <div className="zr-reservation-tfoot-row">
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-79 zr-width-flex-md-72': !isViewInvoice,
        'zr-width-flex-77':                     isViewInvoice,
      })}
    >
      {text}
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-11 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-23':                     isViewInvoice,
      })}
    >
      {`${+payment} ${i18n.t('reservation.SAR')}`}
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-10': !isViewInvoice,
      })}
    />
  </div>
);

export default FooterRow;
