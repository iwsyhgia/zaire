import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import cx from 'classnames';
import i18n from 'i18next';

import { howDidWePay, paymentForRefund } from '../../../../utils/payments';
import TableCell from './columns/Column';
import { INVOICE_TYPES, NO_PARAM } from '../../../../constants/constants';

const HistoryList = ({
  versionsList = [],
  paymentType,
  currentReservation,
  userName,
  tableRowShowed,
  loading,
  handleShow,
  paymentID,
  paymentsList,
  currentPayment,
}) => {
  if (loading) return null;

  return (
    <div className={
      cx('collapse multi-collapse', { show: (tableRowShowed.includes(paymentID) && versionsList.length > 0) })
    }
    >
      {[...versionsList].map((item, index) => (
        <div className="zr-reservation-tbody-line-content zr-reservation-tbody-line-content-collapse">
          <div className="zr-reservation-tbody-row">
            <TableCell
              text={
                item.description
                  ? (
                    <div>
                      <p>{item.description}</p>
                      <p>
                        {paymentType === INVOICE_TYPES.REFUND
                          && paymentForRefund(item, paymentsList)}
                      </p>
                    </div>
                  )
                  : (
                    <div>
                      <p>
                        {currentReservation.guest.first_name}
                        {currentReservation.guest.last_name}
                      </p>
                      <p>
                        {paymentType === INVOICE_TYPES.PAYMENT && howDidWePay(item)}
                      </p>
                    </div>
                  )
              }
              columnClasses="zr-col-description zr-width-flex-39 zr-width-flex-md-18"
            />
            <TableCell
              text={(
                <span>
                  {index > 0
                    ? (
                      <span>
                        <span>
                          {`${i18n.t('reservation.edited')} ${moment(item.updated_at).format('DD/MM/YYYY - hh:mm:ss')}`}
                        </span>
                        <span>{i18n.t('common.by_guest', { name: userName })}</span>
                      </span>
                    )
                    : (
                      <span>
                        <span>
                          {`${i18n.t('reservation.added')} ${moment(item.created_at).format('DD/MM/YYYY - hh:mm:ss')}`}
                        </span>
                        {
                          currentPayment.creation_way === 'automated'
                            ? <span>{i18n.t('common.by_system')}</span>
                            : <span>{i18n.t('common.by_guest', { name: userName })}</span>
                        }
                      </span>
                    )
                  }
                </span>
              )}
              columnClasses="zr-col-date zr-width-flex-19 zr-width-flex-md-18"
            />
            <TableCell
              text={paymentType === 'charge' && `${+item.amount} ${i18n.t('reservation.SAR')}`}
              columnClasses="zr-col-charge zr-width-flex-10 zr-width-flex-md-18"
            />
            <TableCell
              text={
                paymentType === 'discount'
                  && `${+item.value} ${item.kind === 'amount' ? i18n.t('reservation.SAR') : '%'}`
              }
              columnClasses="zr-col-discount zr-width-flex-11 zr-width-flex-md-18"
            />
            <TableCell
              text={
                (paymentType === 'refund' && `-${+item.amount} ${i18n.t('reservation.SAR')}`)
                || (paymentType === 'payment' && `${+item.amount} ${i18n.t('reservation.SAR')}`)
              }
              columnClasses="zr-col-payment zr-width-flex-21 zr-width-flex-md-28"
            />
          </div>
        </div>
      ))}
      <div className="zr-reservation-tbody-row">
        <button type="button" onClick={() => handleShow('hide', NO_PARAM, paymentID)} className="btn btn-link">
          {i18n.t('reservation.hide_details')}
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  loading: state.payments.loading,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryList);
