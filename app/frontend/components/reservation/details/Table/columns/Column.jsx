import React from 'react';
import cx from 'classnames';

const TableCell = ({
  text,
  columnClasses,
  isTitle,
  handleShow,
  payment,
  tableRowShowed,
  isViewInvoice,
}) => (
  <div
    className={cx(columnClasses, {
      'zr-reservation-tbody-col': !isTitle,
      'zr-reservation-thead-col': isTitle,
    })}
    onClick={() => {
      if (!isViewInvoice && !tableRowShowed.includes(payment.id)) {
        handleShow(payment.invoiceType, 'history', payment.id);
      }
    }}
    aria-hidden
  >
    {text}
  </div>
);

TableCell.defaultProps = { isTitle: false };

export default TableCell;
