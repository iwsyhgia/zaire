import React from 'react';
import { connect } from 'react-redux';

import { IconEdit, IconCancel } from '../../../../icons/ReservationIcons';
import DeleteInvoiceForm from '../../../modals/DeleteInvoice';
import WarningPaymentAction from '../../../modals/WarningPayment';
import { showReservationPopup as showReservationPopupAction } from '../../../../../actions/reservations';

const TableRowActions = ({
  isOffline,
  showReservationPopup,
  handleResetForm,
  handleShow,
  invoiceId,
  paymentId,
  paymentType,
  refundsList,
}) => {
  const isPaymentLinked = paymentType === 'payment' && !!refundsList.find(item => item.payment_id === paymentId);
  const showedComponent = isPaymentLinked
    ? (
      <WarningPaymentAction />
    )
    : (
      <DeleteInvoiceForm
        isOffline={isOffline}
        id={paymentId}
        invoiceId={invoiceId}
        handleShow={handleShow}
        paymentType={paymentType}
      />
    );

  return (
    <div className="zr-reservation-tbody-col zr-col-actions zr-width-flex-10">
      <button
        className="zr-btn-icon zr-btn-edit"
        type="button"
        onClick={() => {
          handleResetForm();
          handleShow(paymentType, 'edit', paymentId);
        }}
        disabled={isOffline}
      >
        <IconEdit width="16" height="16" className="icon icon-edit" />
      </button>
      <button
        className="zr-btn-icon zr-btn-cancel"
        type="button"
        onClick={() => {
          showReservationPopup({
            isShow: true,
            showedComponent,
          });
        }}
        disabled={isOffline}
      >
        <IconCancel width="14" height="16" className="icon icon-cancel" />
      </button>
    </div>
  );
};

const mapStateToProps = state => ({
  isOffline: state.common.isOffline,
});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(TableRowActions);
