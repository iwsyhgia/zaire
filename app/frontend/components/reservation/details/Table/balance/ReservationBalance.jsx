import React from 'react';
import i18n from 'i18next';
import FooterRow from '../rows/FooterRow';

const ReservationBalance = ({ generalPayments, isViewInvoice }) => (
  <div>
    <div className="zr-reservation-tfoot-section zr-section-grey">
      <FooterRow
        text={i18n.t('reservation.total_booking')}
        payment={generalPayments.booking_total}
        isViewInvoice={isViewInvoice}
      />
      <FooterRow
        text={i18n.t('reservation.total_charges')}
        payment={generalPayments.charges_total}
        isViewInvoice={isViewInvoice}
      />
      <FooterRow
        text={i18n.t('reservation.vat_tax')}
        payment={generalPayments.vat_total}
        isViewInvoice={isViewInvoice}
      />
      <FooterRow
        text={i18n.t('reservation.municipality_tax')}
        payment={generalPayments.municipal_total}
        isViewInvoice={isViewInvoice}
      />
    </div>
    <div className="zr-reservation-tfoot-section zr-section-grey">
      <FooterRow
        text={i18n.t('reservation.total_cahrges_with_taxes')}
        payment={generalPayments.charges_with_taxes_total}
        isViewInvoice={isViewInvoice}
      />
      <FooterRow
        text={i18n.t('reservation.total_paid')}
        payment={generalPayments.paid_total}
        isViewInvoice={isViewInvoice}
      />
      <FooterRow
        text={i18n.t('reservation.balance')}
        payment={generalPayments.amount}
        isViewInvoice={isViewInvoice}
      />
    </div>
  </div>
);

export default ReservationBalance;
