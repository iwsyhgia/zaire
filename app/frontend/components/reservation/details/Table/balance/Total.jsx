import React from 'react';
import i18n from 'i18next';
import cx from 'classnames';

const TableTotal = ({ generalPayments, isViewInvoice }) => (
  <div className="zr-reservation-tfoot-row">
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-58 zr-width-flex-md-36': !isViewInvoice,
        'zr-width-flex-40':                     isViewInvoice,
      })}
    >
      <span>{i18n.t('reservation.total')}</span>
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-10 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    >
      <span>{`${+generalPayments.charges_total} ${i18n.t('reservation.SAR')}`}</span>
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-11 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    >
      <span>{`${+generalPayments.discounts_total} ${i18n.t('reservation.SAR')}`}</span>
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-11 zr-width-flex-md-18': !isViewInvoice,
        'zr-width-flex-20':                     isViewInvoice,
      })}
    >
      <span>{`${+generalPayments.payments_total - +generalPayments.refunds_total} ${i18n.t('reservation.SAR')}`}</span>
    </div>
    <div
      className={cx('zr-reservation-tfoot-col', {
        'zr-width-flex-10': !isViewInvoice,
      })}
    />
  </div>
);

export default TableTotal;
