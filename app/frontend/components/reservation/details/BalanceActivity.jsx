import React from 'react';
import i18n from 'i18next';
import { SectionBlock } from '../../Wrappers';
import Payments from '../forms/PaymentForms';
import TableTitle from './Table/rows/Title';
import TableRow from './Table/rows/Row';
import TableTotal from './Table/balance/Total';
import ReservationBalance from './Table/balance/ReservationBalance';

export const BalanceActivity = ({
  currentReservation,
  generalPayments,
  payments,
  collapseState,
  handleShow,
  resetForm,
  submitInvoice,
  handleSelect,
  nightCount = 2, // TODO pass actual param
  userName,
  refundSelectsHandle,
  paymentSelectHandle,
  discountSelectHandle,
  paymentById,
  isEditInvoice,
  handleResetForm,
  resetPaymentsState,
}) => (
  <div className="zr-reservation-table-wrap">
    {
      generalPayments.charges_total !== '0.0'
      || generalPayments.payments_total !== '0.0'
      || generalPayments.discounts_total !== '0.0'
      || generalPayments.refunds_total !== '0.0'
        ? (
          <div>
            <SectionBlock title={<TableTitle isViewInvoice={false} />} className="zr-reservation-widget-table">
              <div className="zr-reservation-table">
                { payments && payments.map(payment => (
                  <TableRow
                    payment={payment}
                    currentReservation={currentReservation}
                    userName={userName}
                    nightCount={nightCount}
                    handleResetForm={handleResetForm}
                    handleShow={handleShow}
                    tableRowShowed={collapseState.tableRowShowed}
                    paymentsID={generalPayments.payments}
                    refunds={generalPayments.refunds}
                    isViewInvoice={false}
                  />
                ))}
              </div>
            </SectionBlock>
            <TableTotal isViewInvoice={false} generalPayments={generalPayments} />
          </div>
        ) : (
          <React.Fragment>
            <div
              className="zr-reservation-stub-section zr-section-grey"
            >
              {i18n.t('reservation.no_balance_activity')}
            </div>
          </React.Fragment>
        )
    }
    {
      currentReservation.invoice_id
      && (
        <Payments
          paymentsID={generalPayments.payments}
          collapseState={collapseState}
          handleShow={handleShow}
          resetForm={resetForm}
          submitInvoice={submitInvoice}
          refundSelectsHandle={refundSelectsHandle}
          paymentSelectHandle={paymentSelectHandle}
          discountSelectHandle={discountSelectHandle}
          handleSelect={handleSelect}
          paymentById={paymentById}
          isEditInvoice={isEditInvoice}
          resetPaymentsState={resetPaymentsState}
        />
      )
    }
    <ReservationBalance isViewInvoice={false} generalPayments={generalPayments} />
  </div>
);


export default BalanceActivity;
