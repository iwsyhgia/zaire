import React from 'react';
import { connect } from 'react-redux';
import Guest from './ReservationInfo/Guest';
import Stay from './ReservationInfo/Stay';
import Adults from './ReservationInfo/Adults';

const AsideInfo = (props) => {
  const { current: { id, guest, adults, ...rest }, countries, locale } = props;
  return (
    <div>
      {guest && <Guest guest={guest} id={id} countries={countries} locale={locale} />}
      <Stay info={rest} locale={locale} />
      <Adults adults={adults} />
    </div>
  );
};

const mapStateToProps = state => ({
  current:   state.reservations.current,
  countries: state.common.countries,
  locale:    state.common.locale,
});

export default connect(mapStateToProps)(AsideInfo);
