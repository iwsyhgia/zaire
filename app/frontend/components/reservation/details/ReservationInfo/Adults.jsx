import React from 'react';
import i18n from 'i18next';
import { hasValue } from '../../../../utils/helpers';
import { SectionBlock } from '../../../Wrappers';
import { IconGuests, IconAccount } from '../../../icons/ReservationIcons';

const Adults = ({ adults = [] }) => {

  const adultList = adults.length === 0
    ? (
      <li className="widget-list-item">
        <p className="widget-list-item-col text-col">{i18n.t('reservation.no_adults_checked_in')}</p>
      </li>
    )
    : adults.map((item) => {
      const personTitle = hasValue(item.title) ? i18n.t(`common.${item.title}`) : '';
      return (
        <li className="widget-list-item" key={item.personal_id}>
          <span className="widget-list-item-col icon-col">
            <IconAccount width="19" height="19" className="icon icon-person" />
          </span>
          <p className="widget-list-item-col text-col">
            <strong>{`${personTitle} ${item.first_name} ${item.last_name}`}</strong>
            <span>{item.personal_id}</span>
          </p>
        </li>
      );
    });

  const sectionTitle = (
    <div className="header-wrap">
      <div className="header-icon-col">
        <IconGuests width="16" height="12" className="icon icon-guests" />
      </div>
      <div className="header-inner-col">
        <h4 className="widget-title">{i18n.t('reservation.adult_details')}</h4>
      </div>
    </div>
  );

  return (
    <SectionBlock title={sectionTitle}>
      <ul className="widget-list">
        {adultList}
      </ul>
    </SectionBlock>
  );
};

export default Adults;
