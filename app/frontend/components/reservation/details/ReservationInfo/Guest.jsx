import React from 'react';
import moment from 'moment';
import i18n from 'i18next';
import { hasValue } from '../../../../utils/helpers';
import { SectionBlock } from '../../../Wrappers';
import { IconAccount, IconCalendar, IconPlace, IconPhone, IconEmail } from '../../../icons/ReservationIcons';

const Guest = ({ guest = {}, id, countries, locale }) => {

  const {
    title,
    first_name: firstName,
    last_name: lastName,
    birth_date: birthDate,
    country,
    city,
    phone_code: code,
    phone_number: phone,
    email,
  } = guest;

  const personTitle = hasValue(title) ? i18n.t(`common.${title}`) : '';
  const selectedCountry = countries.find(item => item.alpha3 === country);
  let countryName = country;
  if (selectedCountry) {
    countryName = locale === 'ar' && selectedCountry.name_ar ? selectedCountry.name_ar : selectedCountry.name;
  }

  const sectionTitle = (
    <div className="header-wrap">
      <div className="header-icon-col">
        <IconAccount width="14" height="15.4" className="icon icon-person" />
      </div>
      <div className="header-inner-col">
        <h4 className="widget-title">{`${personTitle} ${firstName} ${lastName}`}</h4>
        <span className="widget-id">{i18n.t('reservation.guest_number', { number: guest.id })}</span>
      </div>
    </div>
  );

  return (
    <SectionBlock title={sectionTitle}>
      <ul className="widget-list zr-guest-info">
        {birthDate && (
          <li className="widget-list-item">
            <span className="widget-list-item-col icon-col">
              <IconCalendar width="19" height="19" className="icon  icon-calendar" />
            </span>
            <p className="widget-list-item-col text-col">
              {`${i18n.t('reservation.birth_date')} ${moment(birthDate).format('DD MMM YYYY')}`}
            </p>
          </li>
        )}
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconPlace width="19" height="19" className="icon  icon-place" />
          </span>
          <p className="widget-list-item-col text-col">{`${countryName}, ${city}`}</p>
        </li>
        {phone && (
          <li className="widget-list-item">
            <span className="widget-list-item-col icon-col">
              <IconPhone width="19" height="19" className="icon  icon-phone" />
            </span>
            <p className="widget-list-item-col text-col">{`+${code} ${phone}`}</p>
          </li>
        )}
        {email && (
          <li className="widget-list-item">
            <span className="widget-list-item-col icon-col">
              <IconEmail width="19" height="19" className="icon  icon-email" />
            </span>
            <p className="widget-list-item-col text-col">{email}</p>
          </li>
        )}
      </ul>
    </SectionBlock>
  );
};

export default Guest;
