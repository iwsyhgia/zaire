import React from 'react';
import moment from 'moment';
import i18n from 'i18next';
import { SectionBlock } from '../../../Wrappers';
import {
  IconInventory,
  IconGuests,
  IconRates,
  IconRooms,
  IconCalendar,
  IconReciept,
  IconStayDetails,
} from '../../../icons/ReservationIcons';

const Stay = ({ info = {}, locale }) => {

  const {
    status,
    check_in_date: checkIn,
    check_out_date: checkOut,
    created_at: createdAt,
    number_of_adults: numberAdults,
    number_of_children: numberChildren,
    rate = 'non_refundable',
    taxes_multiplier: taxesMultiplier = 1.075,
    total_nights: totalNights,
    total_price: totalPrice = 100,
    price_per_night: perNight = 100,
    room: { number, room_type_name: roomType, room_type_name_ar: roomTypeAr } = {},
  } = info;

  const sectionTitle = (
    <div className="header-wrap">
      <div className="header-icon-col">
        <IconStayDetails width="16" height="17.7" className="icon icon-person" />
      </div>
      <div className="header-inner-col">
        <h4 className="widget-title">{i18n.t('reservation.stay_details')}</h4>
      </div>
      <div className="header-status-col">
        <span className={`header-status-msg status-${status}`}>
          {i18n.t(`common.${status}`)}
        </span>
      </div>
    </div>
  );

  const totalNightsTranslation = totalNights > 1 ? 'price_for_nights' : 'price_for_night';
  const numberAdultsTranslation = numberAdults > 1 ? 'adults' : 'adult';

  const roomTypeName = locale === 'ar' && !!roomTypeAr ? roomTypeAr : roomType;

  return (
    <SectionBlock title={sectionTitle}>
      <ul className="widget-list">
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconRooms width="19" height="19" className="icon icon-rooms" />
          </span>
          <p className="widget-list-item-col text-col">{`${number} ${roomTypeName}`}</p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconCalendar width="19" height="19" className="icon icon-calendar" />
          </span>
          <p
            className="widget-list-item-col text-col"
          >
            { `${moment(checkIn).format('DD MMM')} - ${moment(checkOut).format('DD MMM')}` }
          </p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconGuests width="19" height="19" className="icon icon-guests" />
          </span>
          <p className="widget-list-item-col text-col">
            {`${numberAdults} ${i18n.t(`reservation.${numberAdultsTranslation}`)},
            ${numberChildren} ${numberChildren === 1 ? i18n.t('reservation.child') : i18n.t('reservation.children')}`}
          </p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconRates width="19" height="19" className="icon icon-rates" />
          </span>
          <p className="widget-list-item-col text-col">
            {`${i18n.t('common.rate')}: ${i18n.t(`reservation.${rate}`)}`}
          </p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconReciept width="19" height="19" className="icon icon-reciept" />
          </span>
          <p className="widget-list-item-col text-col">
            {i18n.t(`reservation.${totalNightsTranslation}`,
              { total_price: totalPrice * taxesMultiplier, night_count: totalNights })}
          </p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconReciept width="19" height="19" className="icon icon-reciept" />
          </span>
          <p className="widget-list-item-col text-col">
            {i18n.t('reservation.price_per_night', { per_night: perNight * taxesMultiplier })}
          </p>
        </li>
        <li className="widget-list-item">
          <span className="widget-list-item-col icon-col">
            <IconInventory width="19" height="19" className="icon icon-inventory" />
          </span>
          <p className="widget-list-item-col text-col">
            {
              i18n.t('reservation.reserved_date',
                {
                  created_date: moment(createdAt).format('DD MMM'),
                  created_time: moment(createdAt).format('HH:mm'),
                })
            }
          </p>
        </li>
      </ul>
    </SectionBlock>
  );
};

export default Stay;
