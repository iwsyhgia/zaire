import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'i18next';

import ConfirmationPopup from '../../notifications/ConfirmationPopup';
import { showReservationPopup as showReservationPopupAction } from '../../../actions/reservations';
import {
  deletePayment as deletePaymentAction,
  deleteRefund as deleteRefundAction,
  deleteDiscount as deleteDiscountAction,
} from '../../../actions/payments';

class DeleteInvoiceForm extends Component {

  static propTypes = {
    isOffline:            PropTypes.bool.isRequired,
    showReservationPopup: PropTypes.func.isRequired,
    deletePayment:        PropTypes.func.isRequired,
    deleteRefund:         PropTypes.func.isRequired,
    deleteDiscount:       PropTypes.func.isRequired,
    handleShow:           PropTypes.func,
    id:                   PropTypes.number.isRequired,
    invoiceId:            PropTypes.number.isRequired,
    paymentType:          PropTypes.string.isRequired,
    current:              PropTypes.instanceOf(Object),
  };

  static defaultProps = {
    handleShow: () => {},
    current:    { },
  }

  componentDidMount = () => {
  }

  deleteInvoice = () => {
    const {
      id,
      invoiceId,
      paymentType,
      deletePayment,
      deleteRefund,
      deleteDiscount,
      handleShow,
      showReservationPopup,
      current,
      isOffline,
    } = this.props;

    if (isOffline) {
      return;
    }

    switch (paymentType) {
      case 'discount':
        deleteDiscount(id, { invoiceId, reservationId: current.id });
        break;
      case 'refund':
        deleteRefund(id, { invoiceId, reservationId: current.id });
        break;
      case 'payment':
        deletePayment(id, { invoiceId, reservationId: current.id });
        break;
      default:
        break;
    }

    showReservationPopup({ isShow: false });
    handleShow();
  }

  render() {
    const {
      showReservationPopup,
      isOffline,
    } = this.props;

    const confirmationAttr = {
      onSubmit: () => this.deleteInvoice(),
      onHide:   () => showReservationPopup({ isShow: false }),
      text:     i18n.t('payments.delete_payment'),
    };

    return (
      <ConfirmationPopup
        show
        isOffline={isOffline}
        submitText={i18n.t('common.yes')}
        cancelText={i18n.t('common.no')}
        onSubmit={confirmationAttr.onSubmit}
        onHide={confirmationAttr.onHide}
      >
        <p>{confirmationAttr.text}</p>
      </ConfirmationPopup>
    );
  }

}


const mapStateToProps = state => ({
  current:   state.reservations.current,
  isOffline: state.common.isOffline,
});

const mapDispatchToProps = dispatch => ({
  deletePayment:        (id, data) => { dispatch(deletePaymentAction(id, data)); },
  deleteRefund:         (id, data) => { dispatch(deleteRefundAction(id, data)); },
  deleteDiscount:       (id, data) => { dispatch(deleteDiscountAction(id, data)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteInvoiceForm);
