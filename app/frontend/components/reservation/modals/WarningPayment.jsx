import React from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import ConfirmationPopup from '../../notifications/ConfirmationPopup';
import { showReservationPopup as showReservationPopupAction } from '../../../actions/reservations';

const WarningPaymentAction = ({ showReservationPopup }) => (
  <ConfirmationPopup
    show
    isSubmit={false}
    cancelText={i18n.t('common.ok')}
    onHide={() => showReservationPopup({ isShow: false })}
  >
    <p>{i18n.t('payments.delete_payment_forbidden')}</p>
  </ConfirmationPopup>
);

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(WarningPaymentAction);
