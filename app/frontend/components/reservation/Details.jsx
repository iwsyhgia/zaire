import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Transition } from 'react-transition-group';
import i18n from 'i18next';
import { PageBlock } from '../Wrappers';
import PageActions from './buttons/PageActions';
import PaymentActions from './buttons/PaymentActions';
import { BalanceActivity } from './details/BalanceActivity';
import AsideInfo from './details/AsideInfo';
import ModalView from '../ModalView';
import ReservationPopup from './forms/ReservationPopup';
import Spinner from '../icons/Spinner';
import {
  toggleInvoiceCollapse,
  handleSubmitInvoices,
  handleScrollToEnd,
  resetStateForSelects,
} from '../../utils/payments';
import {
  getReservation as getReservationAction,
  getReservationByID as getReservationByIdAction,
} from '../../actions/reservations';
import { getLastCompletedAuditAction } from '../../actions/audit';
import {
  addPayment as addPaymentAction,
  editPayment as editPaymentAction,
  addDiscount as addDiscountAction,
  editDiscount as editDiscountAction,
  addRefund as addRefundAction,
  editRefund as editRefundAction,
  addCharge as addChargeAction,
  getInvoice as getInvoiceAction,
  getGeneralPayments as getGeneralPaymentsAction,
  resetPaymentsState as resetPaymentsStateAction,
} from '../../actions/payments';

class ReservationDetails extends Component {

  state = {
    chargeShowed:         false,
    discountShowed:       false,
    refundShowed:         false,
    paymentShowed:        false,
    tableRowShowed:       [],
    currentPaymentMethod: null,
    currentPaymentId:     null,
    currentKind:          null,
    isEditInvoice:        false,
  };

  componentDidMount() {
    const {
      match: { params: { id } },
      list,
      getReservation,
      getReservationByID,
    } = this.props;

    if (list.length === 0) {
      getReservationByID(id);
    } else {
      getReservation(id);
    }
  }

  componentDidUpdate() {
    const {
      getGeneralPayments,
      current,
      generalPayments,
      getLastCompletedAudit,
    } = this.props;

    if (current && current.invoice_id && !generalPayments.paymentsLoaded) {
      getGeneralPayments(current.invoice_id);
      getLastCompletedAudit();
    }
  }

  handleShow = (buttonType, actionType, paymentId) => {
    const {
      combinedInvoices,
      getInvoice,
      resetPaymentsState,
    } = this.props;

    const {
      tableRowShowed,
    } = this.state;

    let collapseState = {
      chargeShowed:   false,
      discountShowed: false,
      refundShowed:   false,
      paymentShowed:  false,
      tableRowShowed: [],
    };

    resetPaymentsState();
    this.handleResetForm();

    let historyPaymentsList = [];

    if (actionType === 'edit') {
      getInvoice(combinedInvoices, paymentId);
    } else {
      this.setState({ isEditInvoice: false });
    }

    switch (buttonType) {
      case 'charge':
        if (actionType !== 'history') {
          collapseState = { ...collapseState, chargeShowed: true };
        }
        break;
      case 'discount':
        collapseState = toggleInvoiceCollapse(actionType, collapseState, 'discountShowed');
        break;
      case 'refund':
        collapseState = toggleInvoiceCollapse(actionType, collapseState, 'refundShowed');
        break;
      case 'payment':
        collapseState = toggleInvoiceCollapse(actionType, collapseState, 'paymentShowed');
        break;
      case 'hide':
        historyPaymentsList = tableRowShowed.filter(item => item !== paymentId);
        collapseState = toggleInvoiceCollapse(actionType);
        break;
      default:
        break;
    }

    handleScrollToEnd(buttonType, actionType, combinedInvoices.length);

    if (buttonType && buttonType !== 'hide') {
      historyPaymentsList = actionType === 'edit' ? [...tableRowShowed] : [...tableRowShowed, paymentId];
    }

    if (!buttonType && !actionType && !paymentId) {
      historyPaymentsList = [...tableRowShowed];
    }

    this.setState({
      ...collapseState,
      isEditInvoice:  actionType === 'edit',
      tableRowShowed: [...historyPaymentsList],
    });
  }

  resetForm = (api) => {
    this.api = api;
  }

  handleSelect = (value, optionValue) => {
    let optionValues = {
      currentPaymentMethod: null,
      currentPaymentId:     null,
      currentKind:          null,
    };

    switch (optionValue) {
      case 'payment_method':
        optionValues = { ...optionValues, currentPaymentMethod: value };
        break;
      case 'SAR':
        optionValues = { ...optionValues, currentKind: value };
        break;
      case 'choose_payment':
        optionValues = { ...optionValues, currentPaymentId: value };
        break;
      default:
        break;
    }
    this.setState({ ...optionValues });
  }

  resetSelect = () => {
    const selectsState = resetStateForSelects();
    this.setState({ ...selectsState });
  }

  handleResetForm = () => {
    if (this.api) {
      this.api.reset();
    }
    this.resetSelect();
  }

  submitInvoice = (data, type) => {

    const {
      addCharge,
      addDiscount,
      addRefund,
      editRefund,
      addPayment,
      editPayment,
      editDiscount,
      paymentById,
      generalPayments,
      current: {
        id,
      },
    } = this.props;

    const { isEditInvoice } = this.state;

    const generalRequest = { ...data, invoiceId: generalPayments.id, reservation_id: id };
    switch (type) {
      case 'charge':
        addCharge({ ...data, invoiceId: generalPayments.id });
        break;
      case 'discount':
        handleSubmitInvoices(isEditInvoice, addDiscount, editDiscount, generalRequest, paymentById.id);
        break;
      case 'refund':
        handleSubmitInvoices(isEditInvoice, addRefund, editRefund, generalRequest, paymentById.id);
        break;
      case 'payment':
        handleSubmitInvoices(isEditInvoice, addPayment, editPayment, generalRequest, paymentById.id);
        break;
      default:
        break;
    }
    this.handleResetForm();
  }

  render() {
    const {
      current,
      match: { params: { id } },
      generalPayments,
      paymentById,
      generalPayments: {
        payments = [],
      },
      locale,
      countries,
      combinedInvoices,
      resetPaymentsState,
      firstName,
      lastName,
    } = this.props;

    if (Object.keys(current).length === 0) {
      return (
        <PageBlock
          title={`${i18n.t('reservation.reservation')} #${id}`}
          className="zr-reservation-page"
        >
          <div className="jumbotron">
            <h1 className="text-center display-3">{i18n.t('common.loading')}</h1>
          </div>
        </PageBlock>
      );
    }

    const {
      chargeShowed,
      discountShowed,
      refundShowed,
      paymentShowed,
      tableRowShowed,
      currentPaymentMethod,
      currentPaymentId,
      currentKind,
      isEditInvoice,
    } = this.state;

    return (
      <PageBlock
        title={i18n.t('reservation.reservation_number', { number: id })}
        className="zr-reservation-page"
        controls={(
          <PageActions
            reservationId={id}
            current={current}
            locale={locale}
            countries={countries}
            handleShow={this.handleShow}
          />
        )}
      >
        <div className="zr-inner-content-toolbar">
          <PaymentActions
            current={current}
            payments={payments}
            handleShow={this.handleShow}
            resetPaymentsState={resetPaymentsState}
            isEditInvoice={isEditInvoice}
            combinedInvoices={combinedInvoices}
          />
        </div>
        <div className="zr-reservation-page">
          <div className="zr-reservation-page-row">
            <div className="zr-reservation-page-col secondary-col">
              <AsideInfo />
            </div>
            <div className="zr-reservation-page-col main-col">
              <Transition
                in={generalPayments.paymentsLoaded}
                timeout={500}
                classNames="fade"
              >
                { (state) => {
                  switch (state) {
                    case 'entered':
                      return (
                        <BalanceActivity
                          currentReservation={current}
                          generalPayments={generalPayments}
                          payments={_.sortBy(combinedInvoices, 'created_at')}
                          collapseState={{
                            chargeShowed,
                            discountShowed,
                            refundShowed,
                            paymentShowed,
                            tableRowShowed,
                          }}
                          userName={`${firstName} ${lastName}`}
                          handleShow={this.handleShow}
                          resetForm={this.resetForm}
                          submitInvoice={this.submitInvoice}
                          refundSelectsHandle={currentPaymentId}
                          paymentSelectHandle={currentPaymentMethod}
                          discountSelectHandle={currentKind}
                          handleSelect={this.handleSelect}
                          paymentById={paymentById}
                          isEditInvoice={isEditInvoice}
                          handleResetForm={this.handleResetForm}
                          resetPaymentsState={resetPaymentsState}
                        />
                      );
                    default:
                      return <Spinner className="zr-spinner-grey" />;
                  }
                }}
              </Transition>
            </div>
          </div>
        </div>
        <ModalView />
        <ReservationPopup current={current} />
      </PageBlock>
    );
  }

}

const mapStateToProps = state => ({
  current:          state.reservations.current,
  list:             state.reservations.list,
  form:             state.reservations.form,
  countries:        state.common.countries,
  locale:           state.common.locale,
  paymentById:      state.payments.paymentById,
  generalPayments:  state.payments.general_payments,
  combinedInvoices: state.payments.combined_invoices,
  paymentsError:    state.payments.error,
  firstName:        state.profile.form.first_name.value,
  lastName:         state.profile.form.last_name.value,
});

const mapDispatchToProps = dispatch => ({
  addPayment:            (data) => { dispatch(addPaymentAction(data)); },
  editPayment:           (data) => { dispatch(editPaymentAction(data)); },
  addDiscount:           (data) => { dispatch(addDiscountAction(data)); },
  editDiscount:          (data) => { dispatch(editDiscountAction(data)); },
  addRefund:             (data) => { dispatch(addRefundAction(data)); },
  editRefund:            (data) => { dispatch(editRefundAction(data)); },
  addCharge:             (data) => { dispatch(addChargeAction(data)); },
  getInvoice:            (data, id) => { dispatch(getInvoiceAction(data, id)); },
  getGeneralPayments:    (invoiceId) => { dispatch(getGeneralPaymentsAction(invoiceId)); },
  getReservation:        (id) => { dispatch(getReservationAction(id)); },
  getReservationByID:    (id) => { dispatch(getReservationByIdAction(id)); },
  resetPaymentsState:    () => { dispatch(resetPaymentsStateAction()); },
  getLastCompletedAudit: () => { dispatch(getLastCompletedAuditAction()); },
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationDetails);
