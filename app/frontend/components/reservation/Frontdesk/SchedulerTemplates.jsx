import React from 'react';
import cx from 'classnames';
import i18n from 'i18next';
import IconArrowDown from '../../icons/IconArrowDown';
import { eventDayView } from '../../../templates/reservation';
import { isDNR } from '../../../utils/reservation/frontdesk';

const isDayView = viewType => (viewType === 0);
const isMonthView = viewType => (viewType === 2 || viewType === 6);

const dayReplacer = (p1, p2, viewType) => {
  const day = i18n.t(`common.${p1.trim().toLowerCase()}`);
  return isMonthView(viewType) ? `${day}<br>${p2}` : `${day} ${p2}`;
};

const getDayHeader = (formattedItem, viewType) => (
  formattedItem.replace(/([^\d]*)([0-9]+)/g, (match, p1, p2) => (dayReplacer(p1, p2, viewType)))
);

export const nonAgendaCellHeaderTemplateResolver = (
  schedulerData,
  item,
  formattedDateItems,
  style
) => {
  const datetime = schedulerData.localeMoment(item.time);
  const viewType = schedulerData.viewType;
  const isCurrentDate = datetime.isSame(new Date(), 'day');

  const currentStyle = (isCurrentDate)
    ? { backgroundColor: '#FFFFFF', color: '#4D4F5C' }
    : { backgroundColor: '#FFFFFF', color: '#4D4F5C' };
  return (
    <th
      key={item.time}
      className={cx('header3-text scheduler-header',
        { 'day-view': isDayView(viewType), disabled: !!item.disabled })}
      style={{ ...style, ...currentStyle }}
    >
      {
        formattedDateItems.map(formattedItem => (
          <div
            key={`${formattedItem.toString()}`}
            // eslint-disable-next-line
            dangerouslySetInnerHTML={{ // dangerouslySetInnerHTML is required due to module
              __html: getDayHeader(formattedItem, viewType),
            }}
          />
        ))
      }
    </th>
  );
};

export const slotItemTemplateResolver = (
  schedulerData,
  slot,
  slotClickedFunc = () => {},
  width
) => {
  const isRoot = slot.slotId.toString().includes('root');
  const isOpenRoot = isRoot
    && schedulerData.resources.filter(item => item.parent === slot.slotId).length > 0;
  return (
    <div
      style={{ width }}
      title={slot.slotName}
      className={cx('overflow-text header2-text resource-title', { root: isRoot, 'is-open': isOpenRoot })}
      role="presentation"
      onClick={() => { slotClickedFunc(schedulerData, slot); }}
    >
      {isRoot && <span className="mx-2 icon"><IconArrowDown width="10" height="10" /></span>}
      <span>
        {slot.slotName}
      </span>
    </div>
  );
};

export const eventItemTemplateResolver = searchEventID => (
  (
    schedulerData,
    event,
    bgColor,
    isStart,
    isEnd,
    mustAddCssClass,
    mustBeHeight,
    agendaMaxEventWidth = null
  ) => {

    const viewType = schedulerData.viewType;
    // const titleText = schedulerData.behaviors.getEventTextFunc(schedulerData, event);
    let divStyle = { height: mustBeHeight };
    if (agendaMaxEventWidth) divStyle = { ...divStyle, maxWidth: agendaMaxEventWidth };

    let eventContent = null;

    const eventTitle = isDNR(event.status) ? event.description : event.title;
    const dir = schedulerData.config.isRtl ? 'rtl' : 'ltr';
    const locale = schedulerData.config.isRtl ? 'ar-en' : 'en';

    eventContent = (
      <div className="d-flex" dir={dir} style={{ height: `${mustBeHeight}px` }} title={`#${event.id} ${eventTitle}`}>
        <div className="w-100">
          <span className="d-block text-truncate">{`#${event.id}`}</span>
          <span className="d-block font-weight-bold text-truncate">{eventTitle}</span>
        </div>
        { isDayView(viewType) && eventDayView(event, locale)}
      </div>
    );

    return (
      <div
        key={event.id}
        id={`reservation-${event.id}`}
        className={cx(`reservation-item status-${event.status} ${mustAddCssClass}`,
          { 'zr-searchable-item': event.id === searchEventID })}
        style={divStyle}
      >
        {eventContent}
      </div>
    );
  }
);

export const getRoomRate = (rate, mode) => (
  <div className="zr-room-rate">
    {rate}
    {!isMonthView(mode) && ` ${i18n.t('reservation.SAR')}`}
  </div>
);
