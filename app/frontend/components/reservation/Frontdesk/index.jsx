import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import PathToRegexp from 'path-to-regexp';
import FrontdeskCalendar from './FrontdeskCalendar';
import { forceFrontdeskUpdate, getFrontdeskRooms } from '../../../utils/reservation/frontdesk';
import { NO_ROOMS, FORCE_FRONTDESK_UPDATE } from '../../../constants/constants';
import { getRooms as getRoomsAction } from '../../../actions/roomsActions';
import { updateReservationsWith as updateReservationsWithAction } from '../../../actions/reservations';


class Frontdesk extends PureComponent {

  static propTypes = {
    locale: PropTypes.string.isRequired,
    rooms:  PropTypes.arrayOf(PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.object
    ])).isRequired,
    getRooms:               PropTypes.func.isRequired,
    updateReservationsWith: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    forceFrontdeskUpdate({ response: null }, false);

    const { getRooms } = props;
    getRooms();
  }

  componentDidMount() {
    if (window.addEventListener) {
      window.addEventListener('storage', this.listenLocalStorage, false);
    } else {
      // eslint-disable-next-line
      if (window.attachEvent) {
        window.attachEvent('onstorage', this.listenLocalStorage);
      }
    }
  }

  componentWillUnmount() {
    if (window.removeEventListener) {
      window.removeEventListener('storage', this.listenLocalStorage, false);
    } else {
      // eslint-disable-next-line
      if (window.detachEvent) {
        window.detachEvent('onstorage', this.listenLocalStorage);
      }
    }
  }

  listenLocalStorage = (data) => {
    if (data.key === FORCE_FRONTDESK_UPDATE) {

      const { url, newValue } = data;

      // if updates come from the current tab
      const localeRegex = PathToRegexp(`${window.location.origin}/(ar|en)?/:bar*`);
      const parsedPath = localeRegex.exec(url);
      const currentParsedPath = localeRegex.exec(window.location.href);
      if (parsedPath[parsedPath.length - 1] === currentParsedPath[currentParsedPath.length - 1]) return;

      const newValueData = JSON.parse(newValue);

      if (newValueData.need_update === true) {
        if (newValueData.response) {
          const { updateReservationsWith } = this.props;
          updateReservationsWith(newValueData.response);
        }
        // reset state
        forceFrontdeskUpdate({ response: null }, false);
      }
    }
  }

  render() {
    const { rooms = [], locale } = this.props;

    if (rooms.length === 0) {
      return (
        <div className="jumbotron">
          <h1 className="text-center display-3">{i18n.t('common.loading')}</h1>
        </div>
      );
    }

    if (rooms[0] === NO_ROOMS) {
      return (
        <div className="jumbotron">
          <h1 className="text-center display-3">{i18n.t('reservation.no_rooms_available')}</h1>
        </div>
      );
    }

    const groupedRooms = getFrontdeskRooms(rooms, locale);

    return (
      <FrontdeskCalendar rooms={groupedRooms} />
    );
  }

}

const mapStateToProps = state => ({
  rooms:  state.rooms.list,
  locale: state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  getRooms:               () => dispatch(getRoomsAction()),
  updateReservationsWith: data => dispatch(updateReservationsWithAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Frontdesk);
