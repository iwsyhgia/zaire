import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import CustomScheduler from './CustomScheduler';
import { STEPS, DEFAULT_DNR_STATUS, CUT_DNR_STATUS, CONFIRMED, CHECKED_IN } from '../../../constants/constants';
import { formatDate } from '../../../utils/helpers';
import {
  getReservations as getReservationsAction,
  getDNRReservation as getDNRReservationAction,
  showReservationPopup as showReservationPopupAction,
  updateReservationWithDnD as updateReservationAction,
  updateDNRReservationWithDnD as updateDNRAction,
} from '../../../actions/reservations';
import { getAvailableRooms as getAvailableRoomsAction } from '../../../actions/roomsActions';

class FrontdeskCalendar extends PureComponent {

  static propTypes = {
    isOffline:            PropTypes.bool.isRequired,
    locale:               PropTypes.string.isRequired,
    rooms:                PropTypes.arrayOf(PropTypes.object).isRequired,
    list:                 PropTypes.arrayOf(PropTypes.object).isRequired,
    auditTime:            PropTypes.string.isRequired,
    getReservations:      PropTypes.func.isRequired,
    getAvailableRooms:    PropTypes.func.isRequired,
    getDNRReservation:    PropTypes.func.isRequired,
    showReservationPopup: PropTypes.func.isRequired,
    updateReservation:    PropTypes.func.isRequired,
    updateDNRReservation: PropTypes.func.isRequired,
    lastCompletedAudit:   PropTypes.instanceOf(Object).isRequired,
  };

  // TODO: pass from and to as props, so when offline -> we switch to default view
  componentDidMount() {
    const { getReservations } = this.props;
    const from = moment().startOf('month').format('DD/MM/YYYY');
    const to = moment().endOf('month').format('DD/MM/YYYY');
    getReservations(from, to);
  }

  onChangeDate = (from, to) => {
    const { getReservations } = this.props;
    getReservations(moment(from).format('DD/MM/YYYY'), moment(to).format('DD/MM/YYYY'));
  }

  onCreateReservation = (resourceId, from, to) => {
    const { showReservationPopup, getAvailableRooms } = this.props;

    if (Number.isInteger(resourceId)) { // Temp fix: groupId = 'root-${id} for root rows
      getAvailableRooms(moment(from).format('DD/MM/YYYY'), moment(to).format('DD/MM/YYYY'));
      showReservationPopup({
        step:                   STEPS.ADD,
        isEdit:                 false,
        isFrontdeskReservation: true,
        roomID:                 resourceId,
        checkIn:                moment(from).format('DD/MM/YYYY'),
        checkOut:               moment(to).format('DD/MM/YYYY'),
      });
    }
  }

  onReservationClick = (item) => {
    switch (item.status) {
      case DEFAULT_DNR_STATUS:
      case CUT_DNR_STATUS:
        this.showEditDNRPopup(item);
        break;
      default:
        this.redirectToReservation(item);
    }
  }

  onReservationUpdate = (reservationId, start, end, roomId, errorCallback = () => {}) => {

    const { list, updateReservation, updateDNRReservation } = this.props;

    const reservation = list.find(item => item.id === reservationId);

    if (reservation) {

      const status = reservation.status;

      const updatedData = {
        check_in_date:  start ? formatDate(start) : formatDate(reservation.check_in_date),
        check_out_date: end ? formatDate(end) : formatDate(reservation.check_out_date),
        room_id:        roomId || reservation.room.id,
      };

      switch (status) {
        case DEFAULT_DNR_STATUS:
        case CUT_DNR_STATUS:
          updateDNRReservation(reservationId, {
            dnr: {
              description: reservation.description,
              ...updatedData,
            },
          }, errorCallback);
          break;
        case CONFIRMED:
        case CHECKED_IN:
          updateReservation(reservationId, {
            number_of_adults:   reservation.number_of_adults,
            number_of_children: reservation.number_of_children,
            ...updatedData,
          }, errorCallback);
          break;
        default:
          updateReservation(reservationId, {
            reservation: {
              ...reservation,
              guest_attributes: reservation.guest,
              ...updatedData,
            },
          }, errorCallback);
          break;
      }
    }
  }

  redirectToReservation = ({ id }) => {
    const { locale } = this.props;
    // history.push(`/reservations/${id}`);
    const lang = locale === 'ar' ? '/ar' : '';
    window.open(`${lang}/reservations/${id}`, '_blank');
  }

  showEditDNRPopup = ({
    resourceId, start, end, id, description, status,
  }) => {
    const { showReservationPopup, getAvailableRooms, getDNRReservation } = this.props;
    const checkIn = moment(start).format('DD/MM/YYYY');
    const checkOut = moment(end).format('DD/MM/YYYY');

    getAvailableRooms(checkIn, checkOut);
    getDNRReservation(id);
    showReservationPopup({
      id,
      description,
      checkIn,
      checkOut,
      step:    STEPS.EDIT,
      type:    'DNR',
      isEdit:  true,
      roomID:  resourceId,
      blocked: status === 'cut_dnr' || moment().isAfter(moment(end)),
    });
  }

  render() {
    const {
      rooms,
      list: events,
      locale,
      auditTime,
      isOffline,
      lastCompletedAudit,
    } = this.props;

    return (
      <CustomScheduler
        isOffline={isOffline}
        resources={rooms}
        events={events}
        locale={locale}
        auditTime={auditTime}
        lastCompletedAudit={lastCompletedAudit}
        onCreateReservation={this.onCreateReservation}
        onChangeDate={this.onChangeDate}
        onResourceClick={this.onRoomClick}
        onEventClick={this.onReservationClick}
        onEventUpdate={this.onReservationUpdate}
      />
    );
  }

}

const mapStateToProps = state => ({
  locale:             state.common.locale,
  list:               state.reservations.list,
  current:            state.reservations.current,
  auditTime:          state.hotel_settings.nightAudit.model.night_audit_time,
  isOffline:          state.common.isOffline,
  lastCompletedAudit: state.audit.lastCompleted,
});

const mapDispatchToProps = dispatch => ({
  getAvailableRooms:    (from, to) => { dispatch(getAvailableRoomsAction(from, to)); },
  getReservations:      (from, to) => { dispatch(getReservationsAction(from, to)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  getDNRReservation:    (id) => { dispatch(getDNRReservationAction(id)); },
  updateReservation:    (id, data, cb) => { dispatch(updateReservationAction(id, data, cb)); },
  updateDNRReservation: (id, data, cb) => { dispatch(updateDNRAction(id, data, cb)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FrontdeskCalendar));
