import moment from 'moment';
import i18n from 'i18next';
import { ViewTypes, CellUnits, DATE_FORMAT } from '../../../libs/customized-react-big-scheduler/src/index';
import { getRoomRate } from './SchedulerTemplates';

const schedulerSizes = {
  schedulerWidth:           '1330',
  schedulerMaxHeight:       0,
  tableHeaderHeight:        45,
  dayResourceTableWidth:    156,
  weekResourceTableWidth:   156,
  monthResourceTableWidth:  156,
  customResourceTableWidth: 156,
  dayCellWidth:             515,
  weekCellWidth:            148,
  monthCellWidth:           34,
  customCellWidth:          34,
  eventItemHeight:          39,
  eventItemLineHeight:      39,
};

export const schedulerConfig = {
  ...schedulerSizes,
  defaultEventBgColor:            '#45BEEB',
  selectedAreaColor:              'rgba(75, 160, 225, 0.1)',
  nonWorkingTimeHeadColor:        '#A3A6B4',
  nonWorkingTimeHeadBgColor:      '#F5F6FA',
  nonWorkingTimeBodyBgColor:      '#ffffff',
  creatable:                      true,
  movable:                        true,
  crossResourceMove:              true,
  checkConflict:                  false,
  startResizable:                 true,
  endResizable:                   true,
  eventItemPopoverEnabled:        false,
  calendarPopoverEnabled:         true,
  resourceName:                   '',
  nonAgendaDayCellHeaderFormat:   'ddd D',
  nonAgendaOtherCellHeaderFormat: 'ddd D',
  nonTimeDayView:                 true,
  minuteStep:                     60,
  dayMaxEvents:                   3,
  weekMaxEvents:                  3,
  monthMaxEvents:                 3,
  views:                          [
    { viewName: i18n.t('common.month'), viewType: ViewTypes.Month, showAgenda: false, isEventPerspective: false },
    { viewName: i18n.t('common.week'), viewType: ViewTypes.Week, showAgenda: false, isEventPerspective: false },
    { viewName: i18n.t('common.day'), viewType: ViewTypes.Day, showAgenda: false, isEventPerspective: false }
  ],
};

export const schedulerBehaviour = {
  getNonAgendaViewBodyCellBgColorFunc: (schedulerdata, slotId) => (
    slotId.toString().includes('root') ? '#8D93A8' : '#FFFFFF'
  ),

  getNonAgendaViewBodyCellContent: (schedulerData, slotId, header, item) => {
    if (item.root) {
      const cellTime = moment(header.time).format('YYYY-MM-DD');
      const rate = (item.rates && item.rates[cellTime]) || 220;
      return getRoomRate(rate, schedulerData.viewType);
    }

    return null;
  },

  getDateLabelFunc: (schedulerdata, viewType, startDate, endDate) => {
    const start = schedulerdata.localeMoment(startDate);
    const end = schedulerdata.localeMoment(endDate);
    let dateLabel = start.format('D MMM YYYY');

    switch (viewType) {
      case ViewTypes.Year:
      case ViewTypes.Quarter:
        dateLabel = start.format('YYYY');
        break;
      case ViewTypes.Day:
        dateLabel = start.format('D MMM YYYY');
        break;
      default:
        dateLabel = `${start.format('D MMM YYYY')}-${end.format('D MMM YYYY')}`;
        break;
    }

    return dateLabel;
  },

  getCustomDateFunc: (schedulerData, num, date = undefined) => {
    const { viewType } = schedulerData;
    let selectDate = schedulerData.startDate;
    if (date !== undefined) selectDate = date;

    // month
    let startDate = schedulerData.localeMoment(selectDate).add(1 * num, 'month').format(DATE_FORMAT);
    let endDate = schedulerData.localeMoment(startDate).add(1, 'month').add(-1, 'days').format(DATE_FORMAT);
    let cellUnit = CellUnits.Day;

    if (viewType === ViewTypes.Custom) { // week
      startDate = schedulerData.localeMoment(selectDate).add(1 * num, 'weeks').format(DATE_FORMAT);
      endDate = schedulerData.localeMoment(startDate).add(1, 'weeks').add(-1, 'days').format(DATE_FORMAT);
      cellUnit = CellUnits.Day;
    } else if (viewType === ViewTypes.Custom2) { // day
      startDate = schedulerData.localeMoment(selectDate).add(1 * num, 'days').format(DATE_FORMAT);
      endDate = schedulerData.localeMoment(startDate).add(1, 'days').format(DATE_FORMAT);
      cellUnit = CellUnits.Hour;
    }

    return {
      startDate,
      endDate,
      cellUnit,
    };
  },

  isNonWorkingTime: () => false,
};

export const getViews = () => (
  [
    {
      viewName:           i18n.t('common.month'),
      viewType:           ViewTypes.Custom1,
      showAgenda:         false,
      isEventPerspective: false,
    },
    {
      viewName:           i18n.t('common.week'),
      viewType:           ViewTypes.Custom,
      showAgenda:         false,
      isEventPerspective: false,
    },
    {
      viewName:           i18n.t('common.day'),
      viewType:           ViewTypes.Day,
      showAgenda:         false,
      isEventPerspective: false,
    }
  ]
);

export const getDateLabel = locale => ((schedulerdata, viewType, startDate, endDate) => {
  const start = moment(startDate).locale(locale).format('D MMM YYYY');
  const end = moment(endDate).locale(locale).format('D MMM YYYY');
  return viewType === ViewTypes.Day ? start : `${start}-${end}`;
});
