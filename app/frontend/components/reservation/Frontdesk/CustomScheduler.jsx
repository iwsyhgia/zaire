import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'lodash';
import cx from 'classnames';
import ReservationSearch from './CustomScheduler/ReservationSearch';
import Scheduler, { SchedulerData, ViewTypes } from '../../../libs/customized-react-big-scheduler/src/index';
import {
  nonAgendaCellHeaderTemplateResolver,
  slotItemTemplateResolver,
  eventItemTemplateResolver,
} from './SchedulerTemplates';
import withDragDropContext from '../../../containers/withDnDContext';
import { schedulerConfig, schedulerBehaviour, getViews, getDateLabel } from './SchedulerConfig';
import {
  getFrontdeskEvents,
  getVisibleRooms,
  getSearchableEvents,
} from '../../../utils/reservation/frontdesk';

let resizeWidthTimeout = null;
const pageContentPaddings = 40;
const widthError = 15;
class CustomScheduler extends Component {

  static defaultProps = {
    lastCompletedAudit: {},
  }

  static propTypes = {
    resources:           PropTypes.arrayOf(PropTypes.object).isRequired,
    events:              PropTypes.arrayOf(PropTypes.object).isRequired,
    locale:              PropTypes.string.isRequired,
    lastCompletedAudit:  PropTypes.instanceOf(Object),
    onCreateReservation: PropTypes.func.isRequired,
    onChangeDate:        PropTypes.func.isRequired,
    onEventClick:        PropTypes.func.isRequired,
    onEventUpdate:       PropTypes.func.isRequired,
  };

  // Temp vars to check time changes
  static viewModelStartDate = null;

  static viewModelEndDate = null;

  constructor(props) {
    super(props);
    const { lastCompletedAudit } = props;
    const currentLocale = props.locale === 'ar' ? 'ar-en' : 'en';

    const currentDate = moment().format('YYYY-MM-DD');
    const edgeDate = moment(lastCompletedAudit.time_frame_end).format('YYYY-MM-DD');

    const schedulerData = new SchedulerData(
      currentDate,
      ViewTypes.Custom1,
      false,
      false,
      {
        ...schedulerConfig,
        views:    getViews(),
        isRtl:    props.locale === 'ar',
        edgeDate: moment(edgeDate).isValid() ? moment(edgeDate) : null,
      },
      {
        ...schedulerBehaviour,
        getDateLabelFunc: getDateLabel(currentLocale),
      }
    );

    const container = document.querySelector('.zr-inner-content');
    const contentWidth = (container && (container.offsetWidth - pageContentPaddings)) || 0;

    const newEvents = getFrontdeskEvents(props.events, props.resources);
    const searchableEvents = getSearchableEvents(newEvents);

    schedulerData.setData({
      resources:   props.resources,
      events:      newEvents,
      customWidth: contentWidth,
    });

    this.state = {
      viewModel:       schedulerData,
      events:          [],
      hiddenRoomTypes: [],
      reservations:    searchableEvents,
      resources:       props.resources,
      searchEventID:   null,
    };

  }

  static getDerivedStateFromProps(props, state) {
    // TODO: check performance
    const { lastCompletedAudit } = props;
    const { viewModel } = state;

    if (viewModel) {

      const { startDate, endDate, config } = viewModel;
      const isSameTime = (startDate === CustomScheduler.viewModelStartDate)
        && (endDate === CustomScheduler.viewModelEndDate);

      const schedulerChanges = {};
      const stateUpdates = {};

      // Check audit date updates
      const lastAuditDate = moment(lastCompletedAudit.time_frame_end).format('YYYY-MM-DD');
      if (config.edgeDate && config.edgeDate.format('YYYY-MM-DD') !== lastAuditDate) {
        schedulerChanges.edgeDate = lastAuditDate;
      }

      // Check reservation updates
      if (!!props.events && !!state.events
        && (!_.isEqual(_.sortBy(props.events), _.sortBy(state.events)) || !isSameTime)
      ) {
        CustomScheduler.viewModelStartDate = startDate;
        CustomScheduler.viewModelEndDate = endDate;

        const newEvents = getFrontdeskEvents(props.events, props.resources);
        const searchableEvents = getSearchableEvents(newEvents);

        schedulerChanges.events = newEvents;
        stateUpdates.events = _.cloneDeep(props.events);
        stateUpdates.reservations = searchableEvents;
      }

      // Check room updates
      if (!!props.resources && !!state.resources
        && (!_.isEqual(_.sortBy(props.resources), _.sortBy(state.resources)) || !isSameTime)
      ) {
        CustomScheduler.viewModelStartDate = startDate;
        CustomScheduler.viewModelEndDate = endDate;

        schedulerChanges.resources = props.resources;
        stateUpdates.resources = [...props.resources];
      }

      if (Object.keys(schedulerChanges).length > 0) {
        viewModel.setData(schedulerChanges);
      }
      return {
        ...state,
        ...stateUpdates,
        viewModel,
      };
    }

    return { ...state };
  }

  componentDidMount() {
    window.addEventListener('resize', this.onWidthChange);
    // Fix width after page reload
    const headerContainer = document.getElementById('scheduler-header-timeline-col');
    const headerContent = document.getElementById('scheduler-header-timeline');
    if (headerContainer && headerContent && headerContent.offsetWidth + widthError < headerContainer.offsetWidth) {
      this.onWidthChange();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onWidthChange);
  }

  onWidthChange = () => {
    clearTimeout(resizeWidthTimeout);
    resizeWidthTimeout = setTimeout(() => {
      clearTimeout(resizeWidthTimeout);
      const { viewModel } = this.state;
      const contentWidth = document.getElementById('RBS-Scheduler-root').closest('.zr-inner-content').offsetWidth
        - pageContentPaddings;
      viewModel.setContentWidth(contentWidth);
      this.setState({
        viewModel,
      });
    }, 600); // wait sidebar animation
  }

  /* --- Header controls --- */
  prevClick = (schedulerData) => {
    const { onChangeDate } = this.props;
    schedulerData.prev();
    this.setState({
      viewModel: schedulerData,
    });
    onChangeDate(schedulerData.startDate, schedulerData.endDate);
  }

  nextClick = (schedulerData) => {
    const { onChangeDate } = this.props;
    schedulerData.next();
    this.setState({
      viewModel: schedulerData,
    });
    onChangeDate(schedulerData.startDate, schedulerData.endDate);
  }

  onViewChange = (schedulerData, view) => {
    const { onChangeDate } = this.props;
    schedulerData.setViewType(view.viewType, false, false);
    this.setState({
      viewModel: schedulerData,
    });
    onChangeDate(schedulerData.startDate, schedulerData.endDate);
  }

  onSelectDate = (schedulerData, date) => {
    const { onChangeDate } = this.props;
    schedulerData.setDate(date);
    this.setState({
      viewModel: schedulerData,
    });
    onChangeDate(schedulerData.startDate, schedulerData.endDate);
  }
  /* --- end of Header controls --- */

  eventClicked = (schedulerData, event) => {
    const { onEventClick } = this.props;
    onEventClick(event);
  };

  newEvent = (schedulerData, slotId, slotName, start, end) => {
    const { onCreateReservation } = this.props;
    onCreateReservation(slotId, start, end);
  }

  slotClickedFunc = (schedulerData, slot) => {
    if (slot.root) {
      const { hiddenRoomTypes } = this.state;
      const { resources } = this.props;
      const { visibleRooms, newHiddenRoomTypes } = getVisibleRooms(
        slot.slotId,
        resources,
        hiddenRoomTypes
      );

      schedulerData.setResources(visibleRooms);

      this.setState({
        viewModel:       schedulerData,
        hiddenRoomTypes: newHiddenRoomTypes,
      });
    }
  }

  /* --- DnD reservations --- */
  updateEventStart = (schedulerData, event, newStart) => {
    const { onEventUpdate, events } =  this.props;
    const oldEvent = events.find(item => item.id === event.id);
    onEventUpdate(event.id, newStart, null, null, () => {
      schedulerData.updateEventStart(event, `${oldEvent.check_in_date} 12:00:00`);
      this.setState({
        viewModel: schedulerData,
      });
    });
  }

  updateEventEnd = (schedulerData, event, newEnd) => {
    const { onEventUpdate, events } =  this.props;
    const oldEvent = events.find(item => item.id === event.id);
    onEventUpdate(event.id, null, newEnd, null, () => {
      schedulerData.updateEventEnd(event, `${oldEvent.check_out_date} 12:00:00`);
      this.setState({
        viewModel: schedulerData,
      });
    });
  }

  moveEvent = (schedulerData, event, slotId, slotName, start, end) => {
    const { onEventUpdate, events } =  this.props;
    // to prevent reservation jumping
    schedulerData.moveEvent(event, slotId, slotName, start, end);
    this.setState({
      viewModel: schedulerData,
    });

    const oldEvent = events.find(item => item.id === event.id);
    onEventUpdate(event.id, start, end, slotId, () => {
      schedulerData.moveEvent(
        event,
        oldEvent.room.id,
        oldEvent.room.number,
        `${oldEvent.check_in_date} 12:00:00`,
        `${oldEvent.check_out_date} 12:00:00`
      );
      this.setState({
        viewModel: schedulerData,
      });
    });
  }
  /* --- end of DnD reservations --- */

  /* --- Search reservations --- */
  onSearchReservation = (result) => {
    const { viewModel } = this.state;
    if (result.visibleRooms) {
      viewModel.setResources(result.visibleRooms);
      this.setState({
        ...result,
        viewModel,
      });
    } else {
      this.setState({
        ...result,
      });
    }
  }
  /* --- end of Search reservations --- */

  render() {
    const {
      viewModel,
      searchEventID,
      reservations,
      hiddenRoomTypes = [],
      resources,
    } = this.state;
    if (viewModel === null) return null;
    const { locale } = this.props;

    const centerCustomHeader = (
      <ReservationSearch
        locale={locale}
        reservations={reservations}
        resources={resources}
        hiddenRooms={hiddenRoomTypes}
        onChange={this.onSearchReservation}
      />
    );

    return (
      <div className={cx('zr-frontdesk', { 'zr-frontdesk-search': !!searchEventID })}>
        <Scheduler
          schedulerData={viewModel}
          prevClick={this.prevClick}
          nextClick={this.nextClick}
          onSelectDate={this.onSelectDate}
          onViewChange={this.onViewChange}
          eventItemClick={this.eventClicked}
          slotClickedFunc={this.slotClickedFunc}
          updateEventStart={this.updateEventStart}
          updateEventEnd={this.updateEventEnd}
          moveEvent={this.moveEvent}
          newEvent={this.newEvent}
          nonAgendaCellHeaderTemplateResolver={nonAgendaCellHeaderTemplateResolver}
          eventItemTemplateResolver={eventItemTemplateResolver(searchEventID)}
          slotItemTemplateResolver={slotItemTemplateResolver}
          resolveScrollbarSizeOnUpdate={false}
          centerCustomHeader={centerCustomHeader}
        />
      </div>
    );
  }

}

export default withDragDropContext(CustomScheduler);
