import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import AsyncSelect from 'react-select/lib/Async';
import { formatOptionLabel } from '../../../../templates/reservation';
import {
  findReservationCallback,
  getVisibleRooms,
  scrollToReservationWithID,
} from '../../../../utils/reservation/frontdesk';


class ReservationSearch extends Component {

  static propTypes = {
    reservations: PropTypes.arrayOf(PropTypes.object).isRequired,
    resources:    PropTypes.arrayOf(PropTypes.object).isRequired,
    hiddenRooms:  PropTypes.arrayOf(PropTypes.object).isRequired,
    locale:       PropTypes.string.isRequired,
    onChange:     PropTypes.func.isRequired,
  };

  loadOptions = (inputValue, callback) => {
    const { reservations } = this.props;
    setTimeout(() => {
      callback(findReservationCallback(inputValue, reservations));
    }, 1000);
  }

  showResult = (result, type) => {

    const { onChange } = this.props;

    if (type && type.action === 'clear') {
      onChange({
        searchEventID: null,
      });
    }

    if (result !== null) {

      let newState = {
        searchEventID: result.item.id,
      };

      // Check closed room types
      const { hiddenRooms, resources } = this.props;
      if (hiddenRooms.includes(result.item.roomType)) {
        const {
          visibleRooms,
          newHiddenRoomTypes,
        } = getVisibleRooms(result.item.roomType, resources, hiddenRooms, 'open');


        newState = {
          ...newState,
          visibleRooms,
          hiddenRoomTypes: newHiddenRoomTypes,
        };
      }

      onChange(newState);

      scrollToReservationWithID(result.item.id);
    }
  }

  render() {
    const { locale } = this.props;
    const momentLocale = locale === 'ar' ? 'ar-en' : 'en';

    return (
      <div className="zr-reseravion-search">
        <AsyncSelect
          id="reservation-search"
          name="search"
          placeholder={i18n.t('common.search')}
          noOptionsMessage={() => i18n.t('common.no_options')}
          loadingMessage={() => i18n.t('common.loading')}
          className="zr-search-select"
          classNamePrefix="zr-search-select"
          formatOptionLabel={item => formatOptionLabel(item, momentLocale)}
          loadOptions={this.loadOptions}
          onChange={this.showResult}
          isClearable
        />
      </div>
    );

  }

}

export default ReservationSearch;
