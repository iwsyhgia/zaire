import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'i18next';

import ConfirmationPopup from '../../../notifications/ConfirmationPopup';
import {
  cancelReservation as cancelReservationAction,
  showReservationPopup as showReservationPopupAction,
} from '../../../../actions/reservations';

class CancelReservationForm extends Component {

  static propTypes = {
    showReservationPopup: PropTypes.func.isRequired,
    cancelReservation:    PropTypes.func.isRequired,
    current:              PropTypes.instanceOf(Object).isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isCancelled: false,
    };
  }

  hidePopup = () => {
    const { showReservationPopup } = this.props;
    showReservationPopup({ isShow: false });
  }

  cancelReservation = () => {
    this.setState({
      isCancelled: true,
    });
  }

  submitCancellationFee = (id, withFee) => {
    const { cancelReservation } = this.props;
    cancelReservation({
      id,
      cancellation_fee: withFee,
    });
    this.hidePopup();
  }


  render() {
    const {
      current: { id, status } = {},
    } = this.props;

    const { isCancelled } = this.state;
    const show = true;

    const confirmationAttr = isCancelled
      ? {
        onSubmit: () => this.submitCancellationFee(id, status === 'confirmed'),
        onHide:   () => this.submitCancellationFee(id, false),
        text:     i18n.t('reservation.add_cancelation'),
      }
      : {
        onSubmit: () => this.cancelReservation(),
        onHide:   () => this.hidePopup(),
        text:     i18n.t('reservation.submit_cancellation'),
      };

    return (
      <ConfirmationPopup
        show={show}
        submitText={i18n.t('common.yes')}
        cancelText={i18n.t('common.no')}
        onSubmit={confirmationAttr.onSubmit}
        onHide={confirmationAttr.onHide}
      >
        <p>{confirmationAttr.text}</p>
      </ConfirmationPopup>
    );
  }

}


const mapStateToProps = state => ({
  current: state.reservations.current,
});

const mapDispatchToProps = dispatch => ({
  cancelReservation:    (data) => { dispatch(cancelReservationAction(data)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(CancelReservationForm);
