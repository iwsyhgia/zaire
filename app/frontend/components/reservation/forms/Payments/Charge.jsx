import React, { Fragment } from 'react';
import { Form, Text } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';

import { checkLiterals } from '../../../../utils/payments';
import { Button } from '../../../Wrappers';
import validateForm from './validator';

const ChargeForm = ({
  chargeShowed,
  handleShow,
  resetForm,
  submitCharge,
}) => (
  <div className={cx('collapse multi-collapse', { show: chargeShowed })}>
    <div className="zr-reservation-table-form">
      <Form
        name="add-charge-form"
        className="add-reservation-form"
        onSubmit={data => submitCharge(data, 'charge')}
      >
        {({ formState, formApi }) => (
          <Fragment>
            {chargeShowed && resetForm(formApi)}
            <div className="zr-payment-form">
              <h4>{i18n.t('reservation.add_charge')}</h4>
              <div className="zr-payment-form-line">
                <div className="zr-payment-form-line-col zr-width-flex-72">
                  <div className="zr-payment-form-fields">
                    <div className="zr-payment-form-field zr-width-flex-72">
                      <Text
                        field="description"
                        id="charge-description"
                        className={cx('form-control', {
                          'is-invalid': formState.errors.description,
                        })}
                        placeholder={i18n.t('reservation.description')}
                        validate={validateForm}
                      />
                      <div className="invalid-feedback">
                        {formState.errors.description}
                      </div>
                    </div>
                    <div className="zr-payment-form-field zr-width-flex-28">
                      <Text
                        field="amount"
                        type="number"
                        step="0.01"
                        onKeyDown={checkLiterals}
                        id="guest-first_name"
                        className={cx('form-control', {
                          'is-invalid': formState.errors.amount,
                        })}
                        placeholder={i18n.t('common.amount_sar')}
                        validate={validateForm}
                      />
                      <div className="invalid-feedback">
                        {formState.errors.amount}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="zr-payment-form-line-col zr-flex-element-to-end zr-width-flex-auto">
                  <div className="zr-payment-form-actions">
                    <div className="zr-payment-form-action">
                      <Button
                        type="button"
                        className="btn btn-secondary"
                        onClick={() => {
                          handleShow();
                          formApi.reset();
                        }}
                      >
                        {i18n.t('common.cancel')}
                      </Button>
                    </div>
                    <div className="zr-payment-form-action">
                      <Button
                        type="submit"
                        className="btn btn-primary zr-btn-horiz-pad-md"
                        onClick={() => resetForm(formApi)}
                      >
                        <span>{i18n.t('common.add')}</span>
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Fragment>
        )}
      </Form>
    </div>
  </div>
);

export default ChargeForm;
