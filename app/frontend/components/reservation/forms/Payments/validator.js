import i18n from 'i18next';

const validateForm = (errorValue) => {
  if (!errorValue || errorValue === 0) return i18n.t('common.field_required');
  if (+errorValue <= 0) return i18n.t('common.field_positive');
  if (errorValue.length > 50) return i18n.t('common.max_symbols');
  return null;
};


export default validateForm;
