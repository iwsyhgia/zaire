import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Form, Text } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';

import { checkLiterals } from '../../../../utils/payments';
import { Button } from '../../../Wrappers';
import { getUpdatedPaymentsFields } from '../../../../utils/forms';
import { CustomSelect } from '../../../controls/FormControls';
import { KIND_PAYMENT_OPTIONS } from '../../../../constants/constants';
import validateForm from './validator';

const PaymentForm = ({
  paymentShowed,
  handleShow,
  resetForm,
  submitPayment,
  paymentSelectHandle,
  handleSelect,
  paymentById,
  isEditInvoice,
  loading,
  success,
  resetPaymentsState,
}) => {
  if (loading && isEditInvoice) return null;
  const paymentForm = getUpdatedPaymentsFields(isEditInvoice ? paymentById : {});
  const { formAmount } = paymentForm;

  if (success) handleShow();
  return (
    <div className={cx('collapse multi-collapse', { show: paymentShowed })}>
      <div className="zr-reservation-table-form">
        <Form
          name="add-payment-form"
          className="add-reservation-form"
          onSubmit={data => submitPayment(data, 'payment')}
        >
          {({ formState, formApi }) => (
            <Fragment>
              {paymentShowed && resetForm(formApi)}
              <div className="zr-payment-form">
                <h4>{isEditInvoice ? i18n.t('reservation.edit_payment') : i18n.t('reservation.add_payment')}</h4>
                <div className="zr-payment-form-line">
                  <div className="zr-payment-form-line-col zr-width-flex-72">
                    <div className="zr-payment-form-fields">
                      <div className="zr-payment-form-field zr-width-flex-67">
                        <Text
                          field="amount"
                          key={formAmount && formAmount !== '' ? formAmount : 'payment'}
                          id="payment-price"
                          type="number"
                          step="0.01"
                          className={cx('form-control', {
                            'is-invalid': formState.errors.amount,
                          })}
                          onKeyDown={checkLiterals}
                          placeholder={i18n.t('common.amount_sar')}
                          validate={validateForm}
                          initialValue={paymentForm.formAmount}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.amount}
                        </div>
                      </div>
                      <div className="zr-payment-form-field zr-width-flex-33">
                        <CustomSelect
                          key={(paymentForm.formPMethod && paymentForm.formPMethod.value) || 'kind'}
                          field="payment_method"
                          className={cx('zr-payment-select form-control', {
                            'is-invalid': formState.errors.payment_method,
                          })}
                          classNamePrefix="zr-payment-select"
                          options={KIND_PAYMENT_OPTIONS()}
                          placeholder={i18n.t('common.payment_method')}
                          validate={validateForm}
                          initialValue={(paymentForm.formPMethod || {}).value}
                          optionValue={paymentSelectHandle || paymentForm.formPMethod}
                          onChange={value => handleSelect(value, 'payment_method')}
                          value={(paymentForm.formPMethod || {}).value}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.payment_method}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="zr-payment-form-line-col zr-flex-element-to-end zr-width-flex-auto">
                    <div className="zr-payment-form-actions">
                      <div className="zr-payment-form-action">
                        <Button
                          type="button"
                          className="btn btn-secondary"
                          onClick={() => {
                            formApi.reset();
                            resetPaymentsState();
                            handleShow();
                          }}
                        >
                          {i18n.t('common.cancel')}
                        </Button>
                      </div>
                      <div className="zr-payment-form-action">
                        <Button type="submit" className="btn btn-primary zr-btn-horiz-pad-md" onClick={() => resetForm(formApi)}>
                          <span>{isEditInvoice ? i18n.t('common.save') : i18n.t('common.add')}</span>
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          )}
        </Form>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  loading: state.payments.loading,
  success: state.payments.success,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentForm);
