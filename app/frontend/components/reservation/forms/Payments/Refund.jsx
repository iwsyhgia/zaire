import React, { Fragment } from 'react';
import { Form, Text } from 'informed';
import { connect } from 'react-redux';
import moment from 'moment';
import cx from 'classnames';
import i18n from 'i18next';

import { checkLiterals } from '../../../../utils/payments';
import { getUpdatedRefundsFields } from '../../../../utils/forms';
import { CustomSelect } from '../../../controls/FormControls';
import { Button } from '../../../Wrappers';
import validateForm from './validator';

const RefundForm = ({
  paymentsID,
  refundShowed,
  handleShow,
  resetForm,
  submitRefund,
  refundSelectsHandle,
  handleSelect,
  paymentById,
  isEditInvoice,
  loading,
  success,
  resetPaymentsState,
}) => {
  if (loading && isEditInvoice) return null;
  const refundForm = isEditInvoice
    ? getUpdatedRefundsFields(paymentById, paymentsID)
    : getUpdatedRefundsFields({}, []);
  const { formRefundAmount, formDescription } = refundForm;

  if (success) handleShow();
  return (
    <div className={cx('collapse multi-collapse', { show: refundShowed })}>
      <div className="zr-reservation-table-form">
        <Form
          name="add-refund-form"
          className="add-reservation-form"
          onSubmit={data => submitRefund(data, 'refund')}
        >
          {({ formState, formApi }) => (
            <Fragment>
              {refundShowed && resetForm(formApi)}
              <div className="zr-payment-form">
                <h4>{isEditInvoice ? i18n.t('reservation.edit_refund') : i18n.t('reservation.add_refund')}</h4>
                <div className="zr-payment-form-line">
                  <div className="zr-payment-form-line-col zr-width-flex-72">
                    <div className="zr-payment-form-fields">
                      <div className="zr-payment-form-field zr-width-flex-34">
                        <CustomSelect
                          key={(refundForm.formPaymentId && refundForm.formPaymentId.value) || 'kind'}
                          field="payment_id"
                          className={cx('zr-payment-select form-control', {
                            'is-invalid': formState.errors.payment_id,
                          })}
                          classNamePrefix="zr-payment-select"
                          options={paymentsID
                            && paymentsID.map(item => ({
                              value: item.id,
                              label: `${moment(item.created_at).format('DD/MM/YYYY')}
                                        - ${+item.amount} ${i18n.t('reservation.SAR')}`,
                            }))}
                          optionValue={refundSelectsHandle || refundForm.formPaymentId}
                          onChange={value => handleSelect(value, 'choose_payment')}
                          placeholder={i18n.t('common.choose_payment')}
                          validate={validateForm}
                          initialValue={(refundForm.formPaymentId || {}).value}
                          value={(refundForm.formPaymentId || {}).value}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.payment_id}
                        </div>
                      </div>
                      <div className="zr-payment-form-field zr-width-flex-38">
                        <Text
                          field="description"
                          key={formDescription && formDescription !== '' ? formDescription : 'description'}
                          id="discount-description"
                          className={cx('form-control', {
                            'is-invalid': formState.errors.description,
                          })}
                          placeholder={i18n.t('reservation.reason_for_refund')}
                          validate={validateForm}
                          initialValue={refundForm.formDescription}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.description}
                        </div>
                      </div>
                      <div className="zr-payment-form-field zr-width-flex-28">
                        <Text
                          field="amount"
                          key={formRefundAmount && formRefundAmount !== '' ? formRefundAmount : 'refund'}
                          type="number"
                          onKeyDown={checkLiterals}
                          step="0.01"
                          className={cx('form-control', {
                            'is-invalid': formState.errors.amount,
                          })}
                          placeholder={i18n.t('common.amount_sar')}
                          validate={validateForm}
                          initialValue={refundForm.formRefundAmount}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.amount}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="zr-payment-form-line-col zr-width-flex-auto zr-flex-element-to-end">
                    <div className="zr-payment-form-actions">
                      <div className="zr-payment-form-action">
                        <Button
                          type="button"
                          className="btn btn-secondary"
                          onClick={() => {
                            formApi.reset();
                            resetPaymentsState();
                            handleShow();
                          }}
                        >
                          {i18n.t('common.cancel')}
                        </Button>
                      </div>
                      <div className="zr-payment-form-action">
                        <Button
                          type="submit"
                          className="btn btn-primary zr-btn-horiz-pad-md"
                          onClick={() => resetForm(formApi)}
                        >
                          <span>{isEditInvoice ? i18n.t('common.save') : i18n.t('common.add')}</span>
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          )}
        </Form>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  loading: state.payments.loading,
  success: state.payments.success,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(RefundForm);
