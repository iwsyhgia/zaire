import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Form, Text } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';

import { checkLiterals } from '../../../../utils/payments';
import { getUpdatedDiscountsFields } from '../../../../utils/forms';
import { CustomSelect } from '../../../controls/FormControls';
import { Button } from '../../../Wrappers';
import { KIND_DISCOUNT_OPTIONS } from '../../../../constants/constants';
import validateForm from './validator';

const DiscountForm = ({
  discountShowed,
  handleShow,
  resetForm,
  submitDiscount,
  discountSelectHandle,
  handleSelect,
  paymentById,
  isEditInvoice,
  loading,
  success,
  resetPaymentsState,
}) => {
  if (loading && isEditInvoice) return null;
  const discountForm = getUpdatedDiscountsFields(isEditInvoice ? paymentById : {});
  let amountPlaceholder = i18n.t('common.amount');
  if (discountSelectHandle) {
    amountPlaceholder = discountSelectHandle.value === 'amount'
      ? i18n.t('common.amount_sar')
      : i18n.t('common.amount_percent');
  }
  const { formDiscountAmount, formDescription } = discountForm;
  if (success) handleShow();
  return (
    <div className={cx('collapse multi-collapse', { show: discountShowed })}>
      <div className="zr-reservation-table-form">
        <Form
          name="add-discount-form"
          className="add-reservation-form"
          onSubmit={data => submitDiscount(data, 'discount')}
        >
          {({ formState, formApi }) => (
            <Fragment>
              {discountShowed && resetForm(formApi)}
              <div className="zr-payment-form">
                <h4>{isEditInvoice ? i18n.t('reservation.edit_discount') : i18n.t('reservation.add_discount')}</h4>
                <div className="zr-payment-form-line">
                  <div className="zr-payment-form-line-col zr-width-flex-72">
                    <div className="zr-payment-form-fields">
                      <div className="zr-payment-form-field zr-width-flex-wide">
                        <Text
                          field="description"
                          key={formDescription}
                          id="discount-description"
                          className={cx('form-control', {
                            'is-invalid': formState.errors.description,
                          })}
                          placeholder={i18n.t('reservation.description')}
                          validate={validateForm}
                          initialValue={discountForm.formDescription}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.description}
                        </div>
                      </div>
                      <div className="zr-payment-form-field zr-width-flex-22 zr-miw-90p">
                        <CustomSelect
                          key={(discountForm.formValue && discountForm.formValue.value) || 'kind'}
                          field="kind"
                          className={cx('zr-payment-select form-control', {
                            'is-invalid': formState.errors.kind,
                          })}
                          classNamePrefix="zr-payment-select"
                          options={KIND_DISCOUNT_OPTIONS()}
                          optionValue={discountSelectHandle || discountForm.formValue}
                          onChange={value => handleSelect(value, 'SAR')}
                          placeholder={i18n.t('common.value')}
                          validate={validateForm}
                          initialValue={(discountForm.formValue || {}).value}
                          value={(discountForm.formValue || {}).value}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.kind}
                        </div>
                      </div>
                      <div className="zr-payment-form-field zr-width-flex-22">
                        <Text
                          field="value"
                          key={formDiscountAmount && formDiscountAmount !== '' ? formDiscountAmount : 'discount'}
                          type="number"
                          step="0.01"
                          className={cx('form-control', {
                            'is-invalid': formState.errors.value,
                          })}
                          onKeyDown={checkLiterals}
                          placeholder={amountPlaceholder}
                          validate={validateForm}
                          initialValue={discountForm.formDiscountAmount}
                        />
                        <div className="invalid-feedback">
                          {formState.errors.value}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="zr-payment-form-line-col zr-flex-element-to-end zr-width-flex-auto ">
                    <div className="zr-payment-form-actions">
                      <div className="zr-payment-form-action">
                        <Button
                          type="button"
                          className="btn btn-secondary"
                          onClick={() => {
                            formApi.reset();
                            resetPaymentsState();
                            handleShow();
                          }}
                        >
                          {i18n.t('common.cancel')}
                        </Button>
                      </div>
                      <div className="zr-payment-form-action">
                        <Button
                          type="submit"
                          className="btn btn-primary zr-btn-horiz-pad-md"
                          onClick={() => resetForm(formApi)}
                        >
                          <span>{isEditInvoice ? i18n.t('common.save') : i18n.t('common.add')}</span>
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          )}
        </Form>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  loading: state.payments.loading,
  success: state.payments.success,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(DiscountForm);
