import React from 'react';
import DiscountForm from './Payments/Discount';
import ChargeForm from './Payments/Charge';
import RefundForm from './Payments/Refund';
import PaymentForm from './Payments/Payment';

const Payments = ({
  paymentsID,
  collapseState: {
    chargeShowed,
    discountShowed,
    refundShowed,
    paymentShowed,
  },
  handleShow,
  resetForm,
  submitInvoice,
  refundSelectsHandle,
  paymentSelectHandle,
  discountSelectHandle,
  handleSelect,
  paymentById,
  isEditInvoice,
  resetPaymentsState,
}) => (
  <div>
    <ChargeForm
      chargeShowed={chargeShowed}
      handleShow={handleShow}
      resetForm={resetForm}
      submitCharge={submitInvoice}
    />
    <DiscountForm
      discountShowed={discountShowed}
      handleShow={handleShow}
      resetForm={resetForm}
      submitDiscount={submitInvoice}
      discountSelectHandle={discountSelectHandle}
      handleSelect={handleSelect}
      paymentById={paymentById}
      isEditInvoice={isEditInvoice}
      resetPaymentsState={resetPaymentsState}
    />
    <RefundForm
      paymentsID={paymentsID}
      refundShowed={refundShowed}
      handleShow={handleShow}
      resetForm={resetForm}
      submitRefund={submitInvoice}
      refundSelectsHandle={refundSelectsHandle}
      handleSelect={handleSelect}
      paymentById={paymentById}
      isEditInvoice={isEditInvoice}
      resetPaymentsState={resetPaymentsState}
    />
    <PaymentForm
      paymentShowed={paymentShowed}
      handleShow={handleShow}
      resetForm={resetForm}
      submitPayment={submitInvoice}
      paymentSelectHandle={paymentSelectHandle}
      handleSelect={handleSelect}
      paymentById={paymentById}
      isEditInvoice={isEditInvoice}
      resetPaymentsState={resetPaymentsState}
    />
  </div>
);

export default Payments;
