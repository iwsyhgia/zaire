import PropTypes from 'prop-types';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Form } from 'informed';
import i18n from 'i18next';
import PopupTitle from '../PopupTitle';
import Fields from './Fields';
import { Button } from '../../../Wrappers';
import ConfirmationPopup from '../../../notifications/ConfirmationPopup';
import {
  updateDNRReservation as updateDNRAction,
  deleteDNRReservation as deleteDNRAction,
} from '../../../../actions/reservations';
import { SecondaryButton, SubmitButton } from '../../../controls/Buttons';


class EditDNRForm extends Component {

  static propTypes = {
    updateDNRReservation: PropTypes.func.isRequired,
    deleteDNRReservation: PropTypes.func.isRequired,
    onCancel:             PropTypes.func.isRequired,
    form:                 PropTypes.instanceOf(Object),
    current:              PropTypes.instanceOf(Object).isRequired,
  };

  static defaultProps = {
    form: {},
  };

  state = {
    deleteConfirmation: false,
  }

  onSubmit = (data) => {
    const { onCancel, updateDNRReservation, form } = this.props;
    const { room_id: roomID, check_in_date: checkIn, check_out_date: checkOut, description } = data;

    updateDNRReservation(form.id, {
      dnr: {
        description,
        room_id:        roomID,
        check_in_date:  moment(checkIn).format('DD/MM/YYYY'),
        check_out_date: moment(checkOut).format('DD/MM/YYYY'),
      },
    });
    onCancel();
  }

  deleteDNR = () => {
    const { deleteDNRReservation, onCancel, form } = this.props;

    deleteDNRReservation(form.id);
    onCancel();
  }

  renderConfirmationPopup = () => {
    const { deleteConfirmation }  = this.state;

    return (
      <ConfirmationPopup
        show={deleteConfirmation}
        submitText={i18n.t('common.delete')}
        onHide={() => this.setState({ deleteConfirmation: false })}
        onSubmit={() => {
          this.deleteDNR();
          this.setState({ deleteConfirmation: false });
        }}
      >
        <p>{i18n.t('reservation.delete_dnr_confirmation')}</p>
      </ConfirmationPopup>
    );
  };

  render() {
    const { form, current, onCancel } = this.props;
    const { checkIn, checkOut, description, blocked } = form;

    const momentCheckIn  = checkIn ? moment(checkIn, 'DD/MM/YYYY') : moment(current.check_in_date);
    const momentCheckOut = checkOut ? moment(checkOut, 'DD/MM/YYYY') : moment(current.check_out_date);
    const totalNights    = momentCheckIn && momentCheckOut ? Math.ceil(momentCheckOut.diff(momentCheckIn, 'days', true)) : 0;
    const endDate = totalNights === 0 ? momentCheckOut.add(1, 'days') : momentCheckOut;

    const title = blocked ? i18n.t('reservation.dnr') : i18n.t('reservation.edit_dnr');

    const fields = {
      description,
      blocked,
      startDate:         momentCheckIn.valueOf(),
      endDate:           endDate.valueOf(),
      onStartDateChange: () => { },
      onEndDateChange:   () => { },
    };

    return (
      <Fragment>
        <Form name="dnr-reservation-form" className="dnr-reservation-form" onSubmit={this.onSubmit}>
          {({ formState, formApi }) => (
            <Fragment>
              <PopupTitle formState={formState} formApi={formApi} title={title} blocked={blocked} />
              <div className="plain-form p-0 pt-3">
                <Fields {...fields} formState={formState} />
              </div>
              <div className="popup-footer">
                <div className="popup-footer-col">
                  <Button type="button" className="btn-clear popup-footer-btn" onClick={() => onCancel()}>
                    {i18n.t('common.cancel')}
                  </Button>
                </div>
                { !blocked && (
                  <div className="popup-footer-col">
                    <div className="popup-footer-btn-row">
                      <div className="popup-footer-btn-col">
                        <SecondaryButton
                          className="popup-footer-btn"
                          onClick={() => this.setState({ deleteConfirmation: true })}
                        >
                          {i18n.t('common.delete')}
                        </SecondaryButton>
                      </div>
                      <div className="popup-footer-btn-col">
                        <SubmitButton className="popup-footer-btn">
                          {i18n.t('common.save')}
                        </SubmitButton>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </Fragment>
          )}
        </Form>
        {this.renderConfirmationPopup()}
      </Fragment>
    );
  }

}


const mapStateToProps = state => ({
  current: state.reservations.current,
  form:    state.reservations.form,
});

const mapDispatchToProps = dispatch => ({
  updateDNRReservation: (id, data) => { dispatch(updateDNRAction(id, data)); },
  deleteDNRReservation: (id, data) => { dispatch(deleteDNRAction(id, data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(EditDNRForm);
