import React from 'react';
import cx from 'classnames';
import { Text } from 'informed';
import i18n from 'i18next';
import { CustomDatePicker } from '../../../controls/FormControls';
import { isValidMoment, isCheckinDateDisabled } from '../../../../utils/reservation/form';
import { validateTextField } from '../NewReservation/validator';

const Fields = ({
  description,
  blocked,
  startDate,
  endDate,
  onStartDateChange = () => { },
  onEndDateChange = () => { },
  formState,
}) => (
  <div className="plain-form form-section">
    <div className="form-row mt-2 mb-3">
      <div className="col-4">
        <CustomDatePicker
          label={i18n.t('common.start_date')}
          field="check_in_date"
          initialValue={startDate}
          inputProps={{ disabled: isCheckinDateDisabled(startDate) || blocked }}
          isValidDate={isValidMoment}
          onChange={() => onStartDateChange}
        />
      </div>
      <div className="col-4 ml-3 mr-3">
        <CustomDatePicker
          label={i18n.t('common.end_date')}
          field="check_out_date"
          initialValue={endDate}
          inputProps={{ disabled: blocked }}
          isValidDate={isValidMoment}
          onChange={() => onEndDateChange}
        />
      </div>
    </div>
    <div className="form-row mt-4">
      <div className="col-12">
        <Text
          field="description"
          id="roomtype-description"
          disabled={blocked}
          className={cx('form-control', {
            'is-invalid': formState.errors.description,
          })}
          placeholder={i18n.t('reservation.description')}
          initialValue={description}
          validate={validateTextField}
        />
        <div className="invalid-feedback">
          {formState.errors.description}
        </div>
      </div>
    </div>
  </div>
);

export default Fields;
