import React, { Component, Fragment } from 'react';
import PropTypes   from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { Form } from 'informed';
import i18n from 'i18next';
import PopupTitle from '../PopupTitle';
import Fields from './Fields';
import { Button } from '../../../Wrappers';
import {
  addReservationDNR as addDNRAction,
  showReservationPopup as showReservationPopupAction,
} from '../../../../actions/reservations';


class AddDNRForm extends Component {

  static propTypes = {
    onCancel:             PropTypes.func.isRequired,
    addReservationDNR:    PropTypes.func.isRequired,
    showReservationPopup: PropTypes.func.isRequired,
    form:                 PropTypes.instanceOf(Object).isRequired,
  };

  onSubmit = (data) => {
    const { addReservationDNR } = this.props;
    const { room_id: roomID, check_in_date: checkIn, check_out_date: checkOut, description } = data;

    addReservationDNR({
      dnr: {
        description,
        room_id:        roomID,
        check_in_date:  moment(checkIn).format('DD/MM/YYYY'),
        check_out_date: moment(checkOut).format('DD/MM/YYYY'),
      },
    });
  }

  render() {
    const { form = {}, onCancel } = this.props;
    const { checkIn, checkOut, savedValues } = form;

    const momentCheckIn  = Number.isInteger(checkIn) ? moment(checkIn) : moment(checkIn, 'DD/MM/YYYY');
    const momentCheckOut = Number.isInteger(checkOut) ? moment(checkOut) : moment(checkOut, 'DD/MM/YYYY');
    const totalNights    = momentCheckIn && momentCheckOut
      ? Math.ceil(momentCheckOut.diff(momentCheckIn, 'days', true))
      : 0;

    const endDate = totalNights === 0 ? momentCheckOut.add(1, 'days') : momentCheckOut;

    const fields = {
      startDate:         momentCheckIn.valueOf(),
      endDate:           endDate.valueOf(),
      onStartDateChange: () => { },
      onEndDateChange:   () => { },
    };
    return (
      <Form
        name="dnr-reservation-form"
        className="dnr-reservation-form"
        onSubmit={this.onSubmit}
        initialValues={savedValues.dnr || {}}
      >
        {({ formState, formApi }) => (
          <Fragment>
            <PopupTitle formState={formState} formApi={formApi} />
            <div className="plain-form p-0 pt-3">
              <Fields {...fields} formState={formState} />
            </div>
            <div className="popup-footer">
              <div className="popup-footer-col">
                <Button type="button" className="btn-clear popup-footer-btn" onClick={() => onCancel()}>
                  {i18n.t('common.cancel')}
                </Button>
              </div>
              <div className="popup-footer-col">
                <Button type="submit" className="btn-primary popup-footer-btn">{i18n.t('common.add')}</Button>
              </div>
            </div>
          </Fragment>
        )}
      </Form>
    );
  }

}


const mapStateToProps = state => ({
  form: state.reservations.form,
});

const mapDispatchToProps = dispatch => ({
  addReservationDNR:    (data) => { dispatch(addDNRAction(data)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddDNRForm);
