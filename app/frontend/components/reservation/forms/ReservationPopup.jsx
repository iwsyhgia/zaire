import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import AddReservationForm from './NewReservation/AddReservationForm';
import EditReservationForm from './NewReservation/EditReservationForm';
import CheckinForm from './Checkin/CheckinForm';
import AddDNRForm from './DNR/AddDNRForm';
import EditDNRForm from './DNR/EditDNRForm';
import { STEPS } from '../../../constants/constants';
import {
  resetForm as resetFormAction,
  showReservationPopup as showReservationPopupAction,
} from '../../../actions/reservations';
import IconCloseModal from '../../icons/IconCloseModal';

class ReservationPopup extends Component {

  static propTypes = {
    step:                   PropTypes.number.isRequired,
    type:                   PropTypes.string.isRequired,
    resetForm:              PropTypes.func.isRequired,
    showReservationPopup:   PropTypes.func.isRequired,
    isEdit:                 PropTypes.bool,
    isFrontdeskReservation: PropTypes.bool,
    current:                PropTypes.instanceOf(Object),
  };

  static defaultProps = {
    isFrontdeskReservation: true,
    current:                {},
    isEdit:                 false,
  }

  componentDidMount() {
  }

  onCancel = () => {
    const { resetForm, showReservationPopup } = this.props;
    showReservationPopup({ step: STEPS.CLOSE });
    resetForm();
  }

  render() {
    const {
      step,
      type,
      isEdit,
      isFrontdeskReservation,
      current,
    } = this.props;

    let currentForm = null;

    if (step === STEPS.CLOSE) {
      return null;
    }

    if (type === 'DNR') {
      currentForm = isEdit
        ? <EditDNRForm onCancel={() => this.onCancel()} />
        : <AddDNRForm onCancel={() => this.onCancel()} />;
    } else {
      switch (step) {
        case STEPS.EDIT:
          currentForm = (
            <EditReservationForm
              isFrontdeskReservation={isFrontdeskReservation}
              current={current}
              isEdit={isEdit}
              onCancel={() => this.onCancel()}
            />
          );
          break;
        case STEPS.CHECKIN:
          currentForm = (
            <CheckinForm
              isFrontdeskReservation={isFrontdeskReservation}
              onCancel={() => this.onCancel()}
              isEdit={isEdit}
            />
          );
          break;
        default:
          currentForm = (
            <AddReservationForm
              isEdit={isEdit}
              isFrontdeskReservation={isFrontdeskReservation}
              onCancel={() => this.onCancel()}
            />
          );
          break;
      }
    }

    return (
      <div
        className={cx('reservation-popup modal fade', {
          'd-block show': step !== STEPS.CLOSE,
        })}
        tabIndex="-1"
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <button
              type="button"
              className="close close-modal"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => this.onCancel()}
            >
              <IconCloseModal width="14" height="14" className="icon icon-close" />
            </button>
            <div className="modal-body">
              {currentForm}
            </div>
          </div>
        </div>
      </div>
    );
  }

}


const mapStateToProps = state => ({
  step:                   state.reservations.form.step,
  type:                   state.reservations.form.type,
  isEdit:                 state.reservations.form.isEdit,
  isFrontdeskReservation: state.reservations.form.isFrontdeskReservation,
});

const mapDispatchToProps = dispatch => ({
  resetForm:            () => { dispatch(resetFormAction()); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ReservationPopup);
