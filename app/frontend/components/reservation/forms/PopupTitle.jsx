import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { RadioGroup, Radio } from 'informed';
import i18n from 'i18next';
import { CustomSelect } from '../../controls/FormControls';
import { isEmptyObject } from '../../../utils/helpers';
import { getRoomOption, getGroupedRoomOptions, getFieldsBackup } from '../../../utils/reservation/form';
import { formatGroupLabel } from '../../../templates/reservation';
import { NO_ROOMS, STEPS } from '../../../constants/constants';
import { showReservationPopup as showReservationPopupAction } from '../../../actions/reservations';
import { showError as showErrorAction } from '../../../actions/messageLogger';


class PopupTitle extends PureComponent {

  static propTypes = {
    step:                  PropTypes.number.isRequired,
    type:                  PropTypes.string.isRequired,
    loadingAvailableRooms: PropTypes.bool.isRequired,
    availableRooms:        PropTypes.arrayOf(PropTypes.object).isRequired,
    rooms:                 PropTypes.arrayOf(PropTypes.object).isRequired,
    showReservationPopup:  PropTypes.func.isRequired,
    roomID:                PropTypes.number.isRequired,
    roomOption:            PropTypes.instanceOf(Object),
    savedValues:           PropTypes.instanceOf(Object),
    current:               PropTypes.instanceOf(Object).isRequired,
    formApi:               PropTypes.instanceOf(Object).isRequired,
    locale:                PropTypes.string.isRequired,
    title:                 PropTypes.string,
    blocked:               PropTypes.string,
    showError:             PropTypes.func.isRequired,
  };

  static defaultProps = {
    roomOption:  null,
    title:       null,
    blocked:     false,
    savedValues: {},
  }

  onChangeType = () => {
    const { showReservationPopup, formApi, savedValues } = this.props;

    const values = formApi.getState().values;
    const type = formApi.getValue('reserveType');

    const bakupFields = getFieldsBackup(type, values);

    showReservationPopup({ type: formApi.getValue('reserveType'), savedValues: { ...savedValues, ...bakupFields } });
  }

  onChangeRoom = (roomOption) => {
    const { showReservationPopup } = this.props;
    showReservationPopup({ roomOption, roomID: roomOption.value });
  }

  render() {
    const {
      step,
      type,
      availableRooms,
      roomOption,
      roomID,
      current: { room = {} } = {},
      rooms,
      locale,
      loadingAvailableRooms,
      title,
      blocked,
      showError,
    } = this.props;

    let isRoomOccupied = false;

    const groupedRooms = (availableRooms.length === 1 && availableRooms[0] === NO_ROOMS)
      ? [{ key: NO_ROOMS, label: i18n.t('reservation.no_rooms_available') }]
      : getGroupedRoomOptions(type === 'DNR' ? rooms : availableRooms, locale);
    let currentRoomOption = null;
    if (room && !_.isEmpty(room)) {
      currentRoomOption = getRoomOption(room, locale);
    }

    let currentRoom = roomOption || (step !== STEPS.ADD && currentRoomOption)
      || availableRooms.filter(item => item.id === (roomID || room.id))
        .map(item => (getRoomOption(item, locale)))[0] || {};

    const bookedRoom = rooms.filter(item => item.id === roomID)[0] || {};

    // Check if current room is not in available list
    if (availableRooms.length > 0 && isEmptyObject(currentRoom)) {

      if (availableRooms[0] === NO_ROOMS) {
        // There is no available rooms. Show popup
        isRoomOccupied = true;

      } else {

        const roomsOfType = availableRooms
          .filter(item => item.room_type_id === bookedRoom.room_type_id);

        if (roomsOfType.length > 0) {
          const [roomSub] = roomsOfType;
          currentRoom = getRoomOption(roomSub, locale);
        } else {
          // There is no available rooms of the same type. Show popup
          isRoomOccupied = true;
        }
      }
    }

    const localizedName = (locale === 'ar' && !!bookedRoom.room_type_name_ar)
      ? bookedRoom.room_type_name_ar
      : bookedRoom.room_type_name;

    const showErrorPopup = type !== 'DNR' && !loadingAvailableRooms && isRoomOccupied
      && step === STEPS.ADD;

    if (showErrorPopup) {
      showError(i18n.t('reservation.change_room_error', { room: `${localizedName} ${bookedRoom.number}` }));
    }

    return (
      <Fragment>
        <div>
          <div className="popup-title">
            <div className="row">
              <div className="col-6 room-select">
                <CustomSelect
                  key={currentRoom.value || 'room0'}
                  field="room_id"
                  className="text-select"
                  classNamePrefix="text-select"
                  initialValue={currentRoom.value}
                  value={currentRoom.value}
                  optionValue={currentRoom}
                  options={groupedRooms}
                  isDisabled={blocked}
                  isOptionDisabled={option => option.key === NO_ROOMS}
                  formatGroupLabel={formatGroupLabel}
                  onChange={option => this.onChangeRoom(option)}
                />
              </div>
              {step === STEPS.ADD ? (
                <div className="col-6 form-inline">
                  <RadioGroup field="reserveType" initialValue={type} onChange={() => this.onChangeType()}>
                    <div className="">
                      <label className="form-check-label radio-control font-20" htmlFor="reserveType-direct">
                        <Radio
                          value="direct"
                          id="reserveType-direct"
                          className="form-check-input"
                          checked="checked"
                        />
                        <span className="marker" />
                        {i18n.t('reservation.new_reservation')}
                      </label>
                    </div>
                    <div className="mx-2">
                      <label className="form-check-label radio-control font-20" htmlFor="reserveType-dnr">
                        <Radio value="DNR" id="reserveType-dnr" className="form-check-input" />
                        <span className="marker" />
                        {i18n.t('reservation.dnr')}
                      </label>
                    </div>
                  </RadioGroup>
                </div>
              ) : (
                <div className="col-6 form-inline">
                  <div className="font-20">{title || i18n.t('reservation.edit_reservation')}</div>
                </div>
              )}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }

}


const mapStateToProps = state => ({
  step:                  state.reservations.form.step,
  type:                  state.reservations.form.type,
  roomID:                state.reservations.form.roomID,
  roomOption:            state.reservations.form.roomOption,
  savedValues:           state.reservations.form.savedValues,
  current:               state.reservations.current,
  availableRooms:        state.rooms.available_rooms,
  loadingAvailableRooms: state.rooms.loading_av_rooms,
  rooms:                 state.rooms.list,
  locale:                state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  showError:            (msg) => { dispatch(showErrorAction(msg)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(PopupTitle);
