import React, { Component } from 'react';
import PropTypes   from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Form } from 'informed';
import _ from 'lodash';
import i18n from 'i18next';
import Adults from './CheckinInfo/Adults';
import InfoBlock from './CheckinInfo/InfoBlock';
import { Button } from '../../../Wrappers';
import IconArrow from '../../../icons/IconArrow';
import { STEPS } from '../../../../constants/constants';
import {
  checkInReservation as checkInReservationAction,
  deleteReservation as deleteReservationAction,
  showReservationPopup as showReservationPopupAction,
  resetItemState as resetItemStateAction,
} from '../../../../actions/reservations';


class CheckinForm extends Component {

  static propTypes = {
    current:                PropTypes.instanceOf(Object).isRequired,
    history:                PropTypes.instanceOf(Object).isRequired,
    locale:                 PropTypes.string.isRequired,
    countries:              PropTypes.arrayOf(PropTypes.object).isRequired,
    onCancel:               PropTypes.func.isRequired,
    checkInReservation:     PropTypes.func.isRequired,
    showReservationPopup:   PropTypes.func.isRequired,
    deleteReservation:      PropTypes.func.isRequired,
    resetItemState:         PropTypes.func.isRequired,
    isFrontdeskReservation: PropTypes.bool,
    isEdit:                 PropTypes.bool.isRequired,
  };

  static defaultProps = {
    isFrontdeskReservation: true,
  }

  onSubmit = ({ adults }) => {
    const {
      checkInReservation,
      current,
      history,
      onCancel,
      locale,
    } = this.props;

    const isEqualId = !!(adults.length !== _.uniq(adults.map(item => item.personal_id)));
    const adultsFormData = adults.map(item => ({ ...item, reservation_id: current.id }));

    const payload = {
      id:                 current.id,
      room_id:            current.room.id,
      check_in_date:      current.check_in_date,
      check_out_date:     current.check_out_date,
      number_of_adults:   current.number_of_adults,
      number_of_children: current.number_of_children,
      adults_attributes:  [
        ...adultsFormData
      ],
    };

    checkInReservation(payload);
    if (!isEqualId) {
      const lang = locale === 'ar' ? '/ar' : '';
      history.push(`${lang}/reservations/${current.id}`);
      onCancel();
    }
  }

  onBack = () => {
    const { showReservationPopup } = this.props;
    showReservationPopup({ step: STEPS.EDIT });
  }

  defaultAdultsList = () => {
    const { current: { number_of_adults: numberOfAdults } } = this.props;
    const adultsList = [];
    for (let i = 0; i < numberOfAdults; i += 1) {
      adultsList.push({ guestKey: i });
    }

    return adultsList;
  }

  onDelete = () => {
    const {
      current: {
        id,
      },
      onCancel,
      deleteReservation,
      resetItemState,
    } = this.props;

    deleteReservation(id);
    resetItemState();
    onCancel();
  }

  render() {
    const {
      current = {},
      countries,
      onCancel,
      history,
      isEdit,
      isFrontdeskReservation,
      locale,
    } = this.props;
    const { id, room, number_of_adults: numberOfAdults, adults } = current;

    if (numberOfAdults === adults.length) {
      onCancel();
      const lang = locale === 'ar' ? '/ar' : '';
      history.push(`${lang}/reservations/${id}`);
    }

    const localizedName = (locale === 'ar' && !!room.room_type_name_ar) ? room.room_type_name_ar : room.room_type_name;


    return (
      <Form name="checkin-reservation-form" className="checkin-reservation-form" onSubmit={this.onSubmit}>
        {({ formState }) => (
          <div className="p-0">
            <div className="popup-title">
              <div className="row">
                <div className="col-6">
                  <div className="zr-reservation-room-name">{`${room.number} ${localizedName}`}</div>
                </div>
                <div className="col-6 form-inline">
                  <div className="font-20">{i18n.t('reservation.new_reservation')}</div>
                </div>
              </div>
            </div>
            <InfoBlock locale={locale} current={current} countries={countries} />
            <Adults adultsList={this.defaultAdultsList()} isEdit={isEdit} formState={formState} />
            <div className="popup-footer">
              <div className="popup-footer-col">
                <Button
                  type="button"
                  className="btn-link"
                  onClick={isFrontdeskReservation ? this.onDelete : onCancel}
                >
                  {i18n.t('common.cancel')}
                </Button>
              </div>
              <div className="popup-footer-col">
                <div className="popup-footer-btn-row">
                  {isFrontdeskReservation
                    && (
                    <div className="popup-footer-btn-col">
                      <Button
                        type="button"
                        className="btn-secondary popup-footer-btn btn-back"
                        onClick={() => this.onBack()}
                      >
                        <IconArrow width="15" className="icon icon-arrow-right" />
                        <span className="popup-footer-btn-text">{i18n.t('common.back')}</span>
                      </Button>
                    </div>
                    )
                  }
                  <div className="popup-footer-btn-col">
                    <Button type="submit" className="btn-primary popup-footer-btn">
                      <span className="popup-footer-btn-text">{i18n.t('common.check_in')}</span>
                      <IconArrow width="15" color="#FFFFFF" className="icon icon-arrow-right" />
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </Form>
    );
  }

}

const mapStateToProps = state => ({
  current:   state.reservations.current,
  countries: state.common.countries,
  locale:    state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  checkInReservation:   (data) => { dispatch(checkInReservationAction(data)); },
  deleteReservation:    (id) => { dispatch(deleteReservationAction(id)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  resetItemState:       () => { dispatch(resetItemStateAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckinForm));
