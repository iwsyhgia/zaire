import React from 'react';
import { Scope } from 'informed';
import i18n from 'i18next';
import Adult from './Adult';
import IconCloseModal from '../../../../icons/IconCloseModal';

const Adults = ({
  adultsList,
  resetAdults,
  isEdit,
  formApi,
  formState,
}) => {

  const getAdultsFromForm = formApi && formApi.getState().values.adults;
  const isEditable = isEdit && adultsList.length > 1;
  const adults = [...adultsList].map((adult, i) => (
    <div className="zr-edit-guest-adult" key={adult.personal_id || `key-${adult.guestKey}`}>
      <div className="form-row">
        <div className="col-6">
          <h4 className="zr-edit-guest-adult-title">{`${i + 1} ${i18n.t('reservation.adult')}`}</h4>
        </div>
        { isEditable
          && (
          <div className="col-6">
            <button
              type="button"
              className="close close-modal"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => {
                resetAdults({
                  pId:  adult.personal_id || getAdultsFromForm[i].personal_id,
                  gKey: adult.guestKey,
                },
                getAdultsFromForm);
              }}
            >
              <IconCloseModal width="14" height="14" className="icon icon-close" />
            </button>
          </div>
          )
        }
      </div>
      <Scope scope={`adults[${i}]`}>
        <Adult
          formState={formState}
          guest={adult}
          index={i}
          isEditable={isEditable}
        />
      </Scope>
    </div>
  ));
  const kk = [...adultsList]
    .map(adult => ((!!adult.personal_id && adult.personal_id)
      || (!!adult.guestKey && adult.guestKey)))
    .join();
  return (
    <div className="plain-form" key={kk}>
      {adults}
    </div>
  );
};

export default Adults;
