import React from 'react';
import moment from 'moment';
import i18n from 'i18next';
import { hasValue }  from '../../../../../utils/helpers';
import { ContentGreyBlock } from '../../../../Wrappers';

const InfoBlock = ({ current = {}, countries, locale }) => {

  const {
    guest: {
      id,
      title,
      first_name: firstName,
      last_name: lastName,
      birth_date: birthDate,
      country,
      city,
      phone_code: code,
      phone_number: phone,
      email,
    } = {},
    check_in_date: checkIn,
    check_out_date: checkOut,
    number_of_adults: numberAdults,
    number_of_children: numberChildren,
    rate = 'non_refundable', // TODO: get this value from BE after the development of Rate management
    taxes_multiplier: taxesMultiplier = 1.075, // TODO: and this
    total_nights: totalNights,
    total_price: totalPrice = 100, // TODO: and this
    price_per_night: perNight = 100, // TODO: and this
  } = current;

  const selectedCountry = countries.find(item => item.alpha3 === country);
  const localeCountryName = selectedCountry && (locale === 'ar' ? selectedCountry.name_ar : selectedCountry.name);
  const countryName = localeCountryName || country;
  const personTitle = hasValue(title) ? i18n.t(`common.${title}`) : '';
  const totalNightsTranslation = totalNights > 1 ? 'price_for_nights' : 'price_for_night';

  return (
    <ContentGreyBlock className="justify-content-start p-4">
      <div className="row m-0 w-100">
        <div className="col-6 zr-guest-info">
          <h3>{`${personTitle} ${firstName} ${lastName}`}</h3>
          <ul className="list-unstyled">
            {birthDate
              && <li>{`${i18n.t('reservation.birth_date')} ${moment(birthDate).format('DD MMM YYYY')}`}</li>}
            <li>{`${countryName}, ${city}`}</li>
            <li>{`#${id}`}</li>
            {phone && <li>{`+${code} ${phone}`}</li>}
            {email && <li>{email}</li>}
          </ul>
        </div>
        <div className="col-6">
          <h3>{i18n.t('reservation.stay_details')}</h3>
          <ul className="list-unstyled">
            <li>{`${moment(checkIn).format('DD MMM')} - ${moment(checkOut).format('DD MMM')}`}</li>
            <li>
              {`${numberAdults} ${i18n.t('reservation.adults')}, ${numberChildren} ${i18n.t('reservation.children')}`}
            </li>
            <li>{`${i18n.t(`reservation.${rate}`)} ${i18n.t('common.rate')}`}</li>
            <li>
              {i18n.t(`reservation.${totalNightsTranslation}`,
                { total_price: totalPrice * taxesMultiplier, night_count: totalNights })}
            </li>
            <li>{i18n.t('reservation.price_per_night', { per_night: perNight * taxesMultiplier })}</li>
          </ul>
        </div>
      </div>
    </ContentGreyBlock>
  );
};

export default InfoBlock;
