import React, { Fragment } from 'react';
import { Text } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';
import { PERSON_TITLE_OPTIONS } from '../../../../../constants/constants';
import { CustomSelect } from '../../../../controls/FormControls';
import { validateTextField } from '../validator';

const Adult = ({
  guest = {},
  formState,
  index,
}) => {

  const {
    first_name: firstName = '',
    last_name: lastName = '',
    personal_id: personalID = '',
    title = null,
  } = guest;

  const initialTitle = title ? PERSON_TITLE_OPTIONS().find(item => item.value === title) : '';
  const isErrorExist = formState.errors.adults && formState.errors.adults[index];

  return (
    <Fragment>
      <div className="row zr-row">
        <div className="col zr-width-flex-16">
          <CustomSelect
            field="title"
            className="plain-select"
            initialValue={initialTitle}
            classNamePrefix="plain-select"
            options={PERSON_TITLE_OPTIONS()}
            placeholder={i18n.t('reservation.title')}
          />
        </div>
        <div className="col zr-width-flex-41">
          <Text
            field="first_name"
            className={cx('form-control', {
              'is-invalid': isErrorExist && formState.errors.adults[index].first_name,
            })}
            initialValue={firstName}
            value={firstName}
            placeholder={i18n.t('reservation.first_name')}
            validate={validateTextField}
          />
          <div className="invalid-feedback">
            {isErrorExist && formState.errors.adults[index].first_name}
          </div>
        </div>
        <div className="col zr-width-flex-43">
          <Text
            field="last_name"
            className={cx('form-control', {
              'is-invalid': isErrorExist && formState.errors.adults[index].last_name,
            })}
            initialValue={lastName}
            placeholder={i18n.t('reservation.last_name')}
            validate={validateTextField}
          />
          <div className="invalid-feedback">
            {isErrorExist && formState.errors.adults[index].last_name}
          </div>
        </div>
      </div>
      <div className="row zr-row">
        <div className="col zr-width-flex-43">
          <Text
            field="personal_id"
            type="text"
            className={cx('form-control', {
              'is-invalid': isErrorExist && formState.errors.adults[index].personal_id,
            })}
            initialValue={personalID}
            placeholder={i18n.t('reservation.ID')}
            validate={validateTextField}
          />
          <div className="invalid-feedback">
            {isErrorExist && formState.errors.adults[index].personal_id}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Adult;
