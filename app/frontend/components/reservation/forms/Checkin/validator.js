import i18n from 'i18next';

export const validateTextField = (errorValue) => {
  if (!errorValue || errorValue === '') return i18n.t('common.field_required');
  return null;
};

export const validateEmailOrPhone = () => {};
