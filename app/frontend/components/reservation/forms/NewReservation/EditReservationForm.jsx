import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form } from 'informed';
import moment  from 'moment';
import i18n from 'i18next';

import PopupTitle from '../PopupTitle';
import Adults from '../Checkin/CheckinInfo/Adults';
import InfoBlock from './ReservationInfo/InfoBlock';
import StayDates from './ReservationInfo/StayDates';
import { Button } from '../../../Wrappers';
import { IncDec } from '../../../controls/FormControls';
import { STEPS } from '../../../../constants/constants';
import { formatDate, hasValue, isLastAdultEmpty } from '../../../../utils/helpers';
import { processDates } from '../../../../utils/reservation/form';
import AddReservationForm from './AddReservationForm';
import {
  addReservation as addReservationAction,
  updateReservation as updateReservationAction,
  showReservationPopup as showReservationPopupAction,
} from '../../../../actions/reservations';
import { getAvailableRooms as getAvailableRoomsAction } from '../../../../actions/roomsActions';
import { getGuests as getGuestsAction } from '../../../../actions/commonActions';

class EditReservationForm extends Component {

  static propTypes = {
    locale:                 PropTypes.string.isRequired,
    countries:              PropTypes.arrayOf(PropTypes.object).isRequired,
    current:                PropTypes.instanceOf(Object).isRequired,
    form:                   PropTypes.instanceOf(Object).isRequired,
    onCancel:               PropTypes.func.isRequired,
    updateReservation:      PropTypes.func.isRequired,
    getGuests:              PropTypes.func.isRequired,
    showReservationPopup:   PropTypes.func.isRequired,
    getAvailableRooms:      PropTypes.func.isRequired,
    isEdit:                 PropTypes.bool.isRequired,
    isFrontdeskReservation: PropTypes.bool.isRequired,
  };

  static adultChecker = 0;

  state = {
    guestsCount:   0,
    childrenCount: 0,
    adultsList:    [],
    isError:       false,
  }

  componentDidMount = () => {
    const { getGuests, current } = this.props;
    const adultsCount = current.status === 'confirmed' ? current.number_of_adults : current.adults.length;

    this.setState({
      guestsCount:   adultsCount,
      childrenCount: current.number_of_children,
      adultsList:    current.adults,
    });
    EditReservationForm.adultChecker = adultsCount;
    getGuests();
  }

  resetAdults = ({ pId, gKey }, formState) => {
    const { guestsCount, adultsList } = this.state;
    const unitedAdults = formState
      .map((adult, index) => ({ ...adult, guestKey: adultsList[index].guestKey }));
    const filteredAdultsList = unitedAdults
      .filter(item => (item.personal_id ? item.personal_id !== pId : item.guestKey !== gKey));

    this.setState({
      adultsList:  [...filteredAdultsList],
      guestsCount: guestsCount - 1,
      isError:     false,
    });
  }

  incGuests = (guests, formState) => {
    const { current: { status } } = this.props;
    const { adultsList } = this.state;

    if (status === 'confirmed') {
      this.setState({ guestsCount: guests + 1 });
    } else {
      const unitedAdults = formState
        .map((adult, index) => (
          !adult.personal_id ? { ...adult, guestKey: adultsList[index].guestKey } : adult
        ));
      EditReservationForm.adultChecker += 1;

      this.setState({
        guestsCount: guests + 1,
        isError:     false,
        adultsList:  [...unitedAdults, { guestKey: EditReservationForm.adultChecker }],
      });
    }
  }

  decGuests = (guests, formState) => {
    const { current: { status } } = this.props;
    const { adultsList } = this.state;

    if (status === 'confirmed') {
      this.setState({ guestsCount: guests - 1 });
    } else {
      const unitedAdults = formState
        .map((adult, index) => (
          !adult.personal_id ? { ...adult, guestKey: adultsList[index].guestKey } : adult
        ));

      if (isLastAdultEmpty(formState)) {
        this.setState({
          guestsCount: guests - 1,
          isError:     false,
          adultsList:  adultsList.slice(0, guests - 1),
        });
      } else {
        this.setState({
          isError:    true,
          adultsList: [...unitedAdults],
        });
      }
    }
  }

  incChildren = (children) => {
    this.setState({ childrenCount: children + 1 });
  }

  decChildren = (children) => {
    this.setState({ childrenCount: children - 1 });
  }

  onSubmit = (data) => {
    const { current, updateReservation } = this.props;
    const {
      guestsCount,
      childrenCount,
    } = this.state;

    const reservationData = {
      room_id:            data.room_id || current.room.id,
      check_in_date:      formatDate(data.check_in_date),
      check_out_date:     formatDate(data.check_out_date),
      number_of_adults:   guestsCount,
      number_of_children: childrenCount,
    };

    if (data.adults) {
      const submitedAdults = data.adults.filter(item => !!item).map((item) => {
        const adultData = {
          first_name:     item.first_name,
          last_name:      item.last_name,
          title:          item && item.title ? (item.title.value || item.title) : '',
          personal_id:    item.personal_id,
          reservation_id: current.id,
        };
        const existingAdult = current.adults
          .find(adult => adult.personal_id === item.personal_id);
        if (existingAdult) {
          return { ...adultData, id: existingAdult.id };
        }
        return { ...adultData };
      });

      const submitedIds = submitedAdults.filter(item => !!item && !!item.id).map(item => (item.id));

      const deletedAdults =  current.adults
        .filter(item => !submitedIds.includes(item.id))
        .map(item => ({ ...item, _destroy: true }));

      updateReservation(current.id, {
        ...reservationData,
        adults_attributes: [...submitedAdults, ...deletedAdults],
      });
    } else {
      updateReservation(current.id, reservationData);
    }
  }

  render() {
    const {
      current: {
        guest,
        status,
        check_in_date:  checkInDate,
        check_out_date: checkOutDate,
        total_nights: defaultTotalNights,
      },
      form: { checkIn, checkOut },
      countries,
      onCancel,
      showReservationPopup,
      getAvailableRooms,
      locale,
      isEdit,
      isFrontdeskReservation,
    } = this.props;

    const { guestsCount, childrenCount, adultsList, isError } = this.state;

    const selectedCountry = countries.find(item => item.alpha3 === guest.country);
    let countryName = guest.country;
    if (selectedCountry) {
      countryName = (locale === 'ar' ? selectedCountry.name_ar : selectedCountry.name);
    }
    const personTitle = hasValue(guest.title) ? i18n.t(`common.${guest.title}`) : '';

    const {
      momentCheckIn,
      momentCheckOut,
    } = processDates(checkIn, checkInDate, checkOut, checkOutDate);
    let endDate = momentCheckOut.clone();

    const totalNights = Math.ceil(momentCheckOut.diff(momentCheckIn, 'days', true)) || defaultTotalNights;

    const stayDetails = {
      startDate:         momentCheckIn,
      endDate:           momentCheckOut,
      onStartDateChange: (date) => {

        if (date.isSameOrAfter(endDate)) {
          endDate = date.clone().add(1, 'days');
        }
        const from = formatDate(date);
        const to = formatDate(endDate);

        getAvailableRooms(from, to);
        showReservationPopup({ checkIn: from, checkOut: to });
      },
      onEndDateChange: (date) => {
        getAvailableRooms(formatDate(momentCheckIn), formatDate(date));
        showReservationPopup({ checkOut: formatDate(date) });
      },
    };

    return (
      <div>
        {
          ['checked_in', 'confirmed'].includes(status)
            ? (
              <Form name="add-reservation-form" className="add-reservation-form" onSubmit={this.onSubmit}>
                {({ formState, formApi }) => (
                  <Fragment>
                    <PopupTitle formState={formState} formApi={formApi} />
                    <div className="plain-form zr-edit-guest-form">
                      <div className="row zr-edit-guest-info zr-guest-info">
                        <div className="col-4">
                          <h4>{`${personTitle} ${guest.first_name} ${guest.last_name}`}</h4>
                          <ul className="list-unstyled">
                            {guest.birth_date
                              && (
                              <li>
                                {`${i18n.t('reservation.birth_date')}
                                  ${moment(guest.birth_date).format('DD MMM YYYY')}`}
                              </li>
                              )
                            }
                            <li>{`${countryName}, ${guest.city}`}</li>
                            <li>{`#${guest.id}`}</li>
                            {guest.phone_number && <li>{`+${guest.phone_code} ${guest.phone_number}`}</li>}
                            {guest.email && <li>{guest.email}</li>}
                          </ul>
                        </div>
                        <div className="col-4">
                          <h4 className="zr-edit-guest-col-title">{i18n.t('common.booking')}</h4>
                          <StayDates
                            {...stayDetails}
                            formApi={formApi}
                            formState={formState}
                            withLabel={false}
                            direction="column"
                            status={status}
                          />
                        </div>
                        <div className="col-4">
                          <h4 className="zr-edit-guest-col-title">{i18n.t('common.guests')}</h4>
                          <div className="zr-edit-guest-field-wrap">
                            <IncDec
                              field="number_of_adults"
                              id="number_of_adults"
                              className="form-control"
                              placeholder=""
                              place="edit-form"
                              initialValue={guestsCount}
                              label={i18n.t('reservation.adults')}
                              IncButton={{
                                onClick: () => this.incGuests(
                                  guestsCount,
                                  formApi.getState().values.adults
                                ),
                              }}
                              DecButton={{
                                onClick: () => this.decGuests(
                                  guestsCount,
                                  formApi.getState().values.adults
                                ),
                                disabled: guestsCount < 2,
                              }}
                            />
                          </div>
                          <div className="zr-edit-guest-field-wrap">
                            <IncDec
                              field="number_of_children"
                              id="number_of_children"
                              className="form-control"
                              placeholder=""
                              place="edit-form"
                              initialValue={childrenCount}
                              label={i18n.t('reservation.children')}
                              IncButton={{ onClick: () => this.incChildren(childrenCount) }}
                              DecButton={{
                                onClick:  () => this.decChildren(childrenCount),
                                disabled: childrenCount < 1,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      {
                        isError && (
                          <div className="row adults-delete-error">
                            {i18n.t('rooms.delete_adult_by_cross')}
                          </div>
                        )
                      }
                      {
                        status === 'checked_in'
                          && (
                            <Adults
                              adultsList={adultsList}
                              resetAdults={this.resetAdults}
                              isEdit={isEdit}
                              formState={formState}
                              formApi={formApi}
                            />
                          )
                      }
                      <InfoBlock totalNights={totalNights} defaultRate={100} />
                    </div>
                    <div className="popup-footer">
                      <div className="popup-footer-col">
                        <Button type="button" className="btn-clear popup-footer-btn" onClick={() => onCancel()}>
                          {i18n.t('common.cancel')}
                        </Button>
                      </div>
                      <div className="popup-footer-col">
                        <Button type="submit" className="btn-primary popup-footer-btn">
                          <span>{i18n.t('common.save')}</span>
                        </Button>
                      </div>
                    </div>
                  </Fragment>
                )}
              </Form>
            ) : (
              <AddReservationForm
                isEdit={isEdit}
                isFrontdeskReservation={isFrontdeskReservation}
                onCancel={() => onCancel()}
              />
            )
        }
      </div>
    );
  }

}


const mapStateToProps = state => ({
  guests:         state.reservations.guests,
  form:           state.reservations.form,
  step:           state.reservations.form.step,
  current:        state.reservations.current,
  availableRooms: state.rooms.available_rooms,
  countries:      state.common.countries,
  locale:         state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  getGuests:      () => { dispatch(getGuestsAction()); },
  addReservation: (data, readyCheckin) => {
    dispatch(addReservationAction(data, readyCheckin));
  },
  updateReservation: (id, data, checkin) => {
    dispatch(updateReservationAction(id, data, checkin));
  },
  getAvailableRooms:    (from, to) => { dispatch(getAvailableRoomsAction(from, to)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(EditReservationForm);
