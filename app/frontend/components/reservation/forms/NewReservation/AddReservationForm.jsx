import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form } from 'informed';
import moment  from 'moment';
import _ from 'lodash';
import i18n from 'i18next';

import { STEPS, DEFAULT_DNR_STATUS } from '../../../../constants/constants';
import PopupTitle from '../PopupTitle';
import GuestFields from './ReservationInfo/GuestFields';
import StayDetailsFields from './ReservationInfo/StayDetailsFields';
import ExtraDataFields from './ReservationInfo/ExtraDataFields';
import InfoBlock from './ReservationInfo/InfoBlock';
import { Button } from '../../../Wrappers';
import { SubmitButton, SecondaryButton } from '../../../controls/Buttons';
import IconArrow from '../../../icons/IconArrow';
import { hasValue, formatDate } from '../../../../utils/helpers';
import { processDates } from '../../../../utils/reservation/form';
import { getAvailableRooms as getAvailableRoomsAction } from '../../../../actions/roomsActions';
import {
  addReservation as addReservationAction,
  updateReservation as updateReservationAction,
  deleteReservation as deleteReservationAction,
  showReservationPopup as showReservationPopupAction,
  resetItemState as resetItemStateAction,
} from '../../../../actions/reservations';
import {  getGuests as getGuestsAction  } from '../../../../actions/commonActions';

class AddReservationForm extends Component {

  static propTypes = {
    locale:                 PropTypes.string.isRequired,
    current:                PropTypes.instanceOf(Object).isRequired,
    form:                   PropTypes.instanceOf(Object).isRequired,
    onCancel:               PropTypes.func.isRequired,
    addReservation:         PropTypes.func.isRequired,
    updateReservation:      PropTypes.func.isRequired,
    getGuests:              PropTypes.func.isRequired,
    showReservationPopup:   PropTypes.func.isRequired,
    getAvailableRooms:      PropTypes.func.isRequired,
    deleteReservation:      PropTypes.func.isRequired,
    resetItemState:         PropTypes.func.isRequired,
    guests:                 PropTypes.arrayOf(Object).isRequired,
    countries:              PropTypes.arrayOf(Object).isRequired,
    step:                   PropTypes.number.isRequired,
    isEdit:                 PropTypes.bool.isRequired,
    isFrontdeskReservation: PropTypes.bool.isRequired,
  };

  state = {
    clickedButton: '',
  }

  componentDidMount = () => {
    const { getGuests } = this.props;

    getGuests();
  }

  guestHasAttribute = (attr, data) => {
    const { guests } = this.props;
    const reservation = this.processData(data);

    return !!_.filter(guests, {
      [attr]: reservation.guest_attributes[attr],
    }).length && reservation.guest_attributes[attr] !== '';
  }

  onDelete = () => {
    const {
      current: {
        id = '',
        status = '',
      },
      onCancel,
      resetItemState,
      deleteReservation,
    } = this.props;

    if (status !== DEFAULT_DNR_STATUS && id !== '') {
      deleteReservation(id);
      resetItemState();
    }
    onCancel();
  }

  onAddUnconfirmedReservation = (data) => {

    const {
      step,
      current,
      addReservation,
      updateReservation,
    } = this.props;

    const reservation = this.processData(data);

    if (step === STEPS.ADD) {
      if (!this.guestHasAttribute('email', data) && !this.guestHasAttribute('phone_number', data)) {
        addReservation({ reservation });
      } else {
        addReservation({
          ...reservation,
          guest_id:         reservation.guest_attributes.id,
          guest_attributes: { ...reservation.guest_attributes },
        });
      }

    } else {
      const guestId = reservation.guest_attributes.id || null;
      updateReservation(current.id, {
        reservation: {
          ...reservation,
          guest_id:         guestId,
          room_id:          data.room_id || current.room.id,
          guest_attributes: { ...reservation.guest_attributes, id: guestId },
        },
      });
    }
  }

  onCheckin = (data) => {

    const {
      step,
      current,
      addReservation,
      updateReservation,
    } = this.props;

    const reservation = this.processData(data);

    if (step === STEPS.ADD) {
      if (!this.guestHasAttribute('email', data) && !this.guestHasAttribute('phone_number', data)) {
        addReservation({ reservation }, true);
      } else {
        addReservation({
          ...reservation,
          guest_id:         reservation.guest_attributes.id,
          guest_attributes: { ...reservation.guest_attributes },
        }, true);
      }
    } else {
      const guestId = data.id || current.guest.id;
      updateReservation(current.id, {
        reservation: {
          ...reservation,
          room_id:          data.room_id || current.room.id,
          guest_id:         guestId, // TODO: check affects. Case: guest of existing reservation is changed.
          guest_attributes: { ...reservation.guest_attributes, id: guestId },
        },
      }, true);
    }
  }

  // TO DO
  onSubmit = (data) => {
    const { clickedButton } = this.state;

    switch (clickedButton) {
      case 'save':
        this.onAddUnconfirmedReservation(data);
        break;
      case 'checkin':
        this.onCheckin(data);
        break;
      default:
        break;
    }
  }

  // TO DO
  toggleButton = (btn) => {
    this.setState({ clickedButton: btn });
  }

  processData = (data) => {
    const { form: { roomID } } = this.props; // TODO: remove, Check initial value of CustomSelect
    const { locale } = this.props;

    const {
      reserveType,
      rate,
      email,
      payment_kind: payment,
      room_id: roomId = roomID,
      check_in_date: checkIn,
      check_out_date: checkOut,
      number_of_adults: numberOfAdults = 1,
      number_of_children: numberOfChildren = 0,
      phone_number: phoneNumber,
      birth_date: birthDate,
      country,
      title,
      ...rest
    } = data;

    moment.locale('en');

    const reservation = {
      rate,
      room_id:            roomId,
      number_of_adults:   numberOfAdults,
      number_of_children: numberOfChildren,
      payment_kind:       payment,
      guest_attributes:   {
        email:        email || '',
        phone_number: phoneNumber || '',
        birth_date:   hasValue(birthDate) ? formatDate(birthDate) : '',
        country:      country ? (country.value || country) : null,
        title:        title ? (title.value || title) : null,
        ...rest,
      },
      check_in_date:  formatDate(checkIn),
      check_out_date: formatDate(checkOut),
    };

    const currentLocale = locale === 'ar' ? 'ar-en' : 'en';
    moment.locale(currentLocale);

    return reservation;
  }

  render() {
    const {
      current = {},
      form: { checkIn, checkOut, savedValues = {} },
      countries,
      getAvailableRooms,
      showReservationPopup,
      step,
      guests,
      isEdit,
      locale,
      onCancel,
      isFrontdeskReservation,
    } = this.props;

    const {
      momentCheckIn,
      momentCheckOut,
    } = processDates(checkIn, current.check_in_date, checkOut, current.check_out_date);
    let endDate = momentCheckOut.clone();
    const today = moment();

    const totalNights = Math.ceil(momentCheckOut.diff(momentCheckIn, 'days', true)) || current.total_nights;

    const stayDetailsfields = {
      startDate:         momentCheckIn,
      endDate:           momentCheckOut,
      onStartDateChange: (date) => {

        if (date.isSameOrAfter(endDate)) {
          endDate = date.clone().add(1, 'days');
        }
        const from = formatDate(date);
        const to = formatDate(endDate);

        getAvailableRooms(from, to);
        showReservationPopup({ checkIn: from, checkOut: to });
      },
      onEndDateChange: (date) => {
        getAvailableRooms(formatDate(momentCheckIn), formatDate(date));
        showReservationPopup({ checkOut: formatDate(date) });
      },
    };

    const { direct: { guest = {}, ...rest } = {} } = savedValues;
    const backupFields = (savedValues.direct && { ...rest, ...guest }) || {};

    const currentFields = step === STEPS.ADD ? backupFields : current;
    const currentGuest = step === STEPS.ADD ? guest : current.guest;

    return (
      <Form
        name="add-reservation-form"
        className="add-reservation-form zr-add-reservation-form"
        onSubmit={this.onSubmit}
        initialValues={backupFields}
      >
        {({ formState, formApi }) => (
          <Fragment>
            <PopupTitle formState={formState} formApi={formApi} />
            <div className="plain-form">
              <GuestFields
                guest={currentGuest}
                guests={guests}
                countries={countries}
                formApi={formApi}
                formState={formState}
                isEdit={isEdit}
                locale={locale}
              />
              <StayDetailsFields stayDetails={stayDetailsfields} formApi={formApi} formState={formState} />
              <ExtraDataFields
                current={currentFields}
                formApi={formApi}
                formState={formState}
                isEdit={isEdit}
              />
              <InfoBlock totalNights={totalNights} defaultRate={100} />
            </div>
            <div className="popup-footer">
              <div className="popup-footer-col">
                <Button
                  type="button"
                  className="btn-clear popup-footer-btn"
                  onClick={isFrontdeskReservation ? this.onDelete : onCancel}
                >
                  {i18n.t('common.cancel')}
                </Button>
              </div>
              <div className="popup-footer-col">
                <div className="popup-footer-btn-row">
                  {
                    !isEdit
                      && (
                        <div className="popup-footer-btn-col zr-checkin-container">
                          <SecondaryButton
                            type="submit"
                            className="popup-footer-btn zr-checkin-btn"
                            disabled={!momentCheckIn.isSame(today, 'day')}
                            onClick={() => this.toggleButton('checkin')}
                          >
                            <span className="popup-footer-btn-text">{i18n.t('common.check_in')}</span>
                            <IconArrow width="15" className="icon icon-arrow-right" />
                          </SecondaryButton>
                          <span className="validation-tooltip">{i18n.t('reservation.only_current_date_check_in')}</span>
                        </div>
                      )
                  }
                  <div className="popup-footer-btn-col">
                    <SubmitButton
                      onClick={() => this.toggleButton('save')}
                      className="popup-footer-btn"
                    >
                      <span className="popup-footer-btn-text">
                        {isEdit ? i18n.t('common.save') : i18n.t('common.add')}
                      </span>
                      {!isEdit && <IconArrow color="#FFFFFF" width="15" className="icon icon-arrow-right" />}
                    </SubmitButton>
                  </div>
                </div>
              </div>
            </div>
          </Fragment>
        )}
      </Form>
    );
  }

}


const mapStateToProps = state => ({
  guests:    state.reservations.guests,
  form:      state.reservations.form,
  step:      state.reservations.form.step,
  current:   state.reservations.current,
  countries: state.common.countries,
  locale:    state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  getGuests:      () => { dispatch(getGuestsAction()); },
  addReservation: (data, readyCheckin) => {
    dispatch(addReservationAction(data, readyCheckin));
  },
  updateReservation: (id, data, checkin) => {
    dispatch(updateReservationAction(id, data, checkin));
  },
  getAvailableRooms:    (from, to) => { dispatch(getAvailableRoomsAction(from, to)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  deleteReservation:    (id) => { dispatch(deleteReservationAction(id)); },
  resetItemState:       () => { dispatch(resetItemStateAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddReservationForm);
