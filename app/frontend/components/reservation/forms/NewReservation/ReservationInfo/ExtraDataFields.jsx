import React from 'react';
import { RadioGroup, Radio } from 'informed';
import i18n from 'i18next';
import { IncDec } from '../../../../controls/FormControls';

const ExtraDataFields = ({ current, formApi }) => {
  const {
    number_of_adults: adultsNumber = 1,
    number_of_children: childrenNumber = 0,
    rate = 'refundable',
    payment = (current && current.payment_kind) || 'first_night',
  } = current;
  return (
    <div className="form-section zr-form-section-extra">
      <div className="row zr-row">
        <div className="col zr-width-flex-35">
          <h4 className="zr-add-reservation-subtitle">{i18n.t('common.guests')}</h4>
          <div className="zr-add-reservation-field-wrap">
            <IncDec
              field="number_of_adults"
              id="number_of_adults"
              className="form-control"
              placeholder=""
              value={adultsNumber}
              initialValue={adultsNumber}
              label={i18n.t('reservation.adults')}
            />
          </div>
          <div className="zr-add-reservation-field-wrap">
            <IncDec
              field="number_of_children"
              id="number_of_children"
              className="form-control"
              placeholder=""
              value={childrenNumber}
              initialValue={childrenNumber}
              label={i18n.t('reservation.children', { count: childrenNumber })}
            />
          </div>
        </div>
        <div className="col zr-width-flex-4" />
        <div className="col zr-width-flex-31">
          <h4 className="zr-add-reservation-subtitle">{i18n.t('common.rate')}</h4>
          <RadioGroup field="rate" initialValue={rate}>
            <div className="zr-add-reservation-field-wrap">
              <label className="form-check-label radio-control" htmlFor="rate-refundable">
                <Radio value="refundable" id="rate-refundable" className="form-check-input" checked="true" />
                <span className="marker" />
                {i18n.t('reservation.refundable_upper')}
              </label>
            </div>
            <div className="zr-add-reservation-field-wrap">
              <label className="form-check-label radio-control" htmlFor="rate-non-refundable">
                <Radio value="non_refundable" id="rate-non-refundable" className="form-check-input" />
                <span className="marker" />
                {i18n.t('reservation.non_refundable_upper')}
              </label>
            </div>
          </RadioGroup>
        </div>
        <div className="col zr-width-flex-4" />
        {formApi.getValue('rate') === 'refundable' && (
        <div className="col zr-width-flex-26">
          <h4 className="zr-add-reservation-subtitle">{i18n.t('common.payment')}</h4>
          <RadioGroup field="payment_kind" initialValue={payment}>
            <div className="zr-add-reservation-field-wrap">
              <label className="form-check-label radio-control" htmlFor="payment-first-night">
                <Radio value="first_night" id="payment-first-night" className="form-check-inputs" />
                <span className="marker" />
                {i18n.t('reservation.first_night')}
              </label>
            </div>
            <div className="zr-add-reservation-field-wrap">
              <label className="form-check-label radio-control" htmlFor="payment-full">
                <Radio value="full_stay" id="payment-full" className="form-check-input" />
                <span className="marker" />
                {i18n.t('reservation.full_stay')}
              </label>
            </div>
          </RadioGroup>
        </div>
        )}
      </div>
    </div>
  );
};

export default ExtraDataFields;
