import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import { Text } from 'informed';
import moment from 'moment';
import _ from 'lodash';
import i18n from 'i18next';
import { getUpdatedGuestField, updateGuestFields } from '../../../../../utils/forms';
import { formatGuestOption } from '../../../../../templates/reservation';
import { CustomSelect, CustomDatePicker, CustomAsyncSelect } from '../../../../controls/FormControls';
import { PERSON_TITLE_OPTIONS } from '../../../../../constants/constants';
import { validateTextField, validateEmailOrPhone } from '../validator';
import { showReservationPopup as showReservationPopupAction } from '../../../../../actions/reservations';

let isExist = false;

const guestFormFields = guest => ({
  first_name:   guest.first_name,
  last_name:    guest.last_name,
  city:         guest.city,
  country:      guest.country,
  email:        guest.email ? guest.email : '',
  phone_code:   parseInt(guest.phone_code, 10),
  phone_number: guest.phone_number ? guest.phone_number : '',
  id:           guest.id,
  title:        guest.title !== '' ? guest.title : null,
  birth_date:   guest.birth_date ? guest.birth_date : '',
});

const sortList = (inputValue, guests, type) => {
  if (inputValue) {
    return _.uniqBy(guests
      .filter(item => item[type])
      .filter(item => item[type].toLowerCase().includes(inputValue.toLowerCase()))
      .map(item => ({ value: item[type], label: item[type], item })), item => item.value);
  }
  return guests;
};

const GuestFields = ({
  guest = {},
  formApi,
  countries,
  guests,
  formState,
  locale,
  form: {
    formPhone,
    formEmail,
    formTitle,
    formCountry,
    formBDay,
    formLastName,
  },
  showReservationPopup,
}) => {

  const handleChangeEvent = (item, field, fieldName) => {
    let formData = {};
    if (item.item) {
      updateGuestFields(guestFormFields(item.item), formApi);
      formData = getUpdatedGuestField(item.item, countries, locale);
      isExist = true;

    } else {
      const itemValue = item.value || item;
      formApi.setValue(fieldName, itemValue);
      formApi.setValue('id', null);
      formData = {
        [field]: { value: itemValue, label: itemValue, isNew: true },
      };
    }
    showReservationPopup({ ...formData });
  };

  const handleBlurEvent = (inputValue, field, fieldName) => {
    const currentValue = formApi.getValue(fieldName);
    if (!isExist && inputValue !== '' && inputValue !== currentValue) {
      formApi.setValue(fieldName, inputValue);
      formApi.setValue('id', null);
      showReservationPopup({ [field]: { value: inputValue, label: inputValue, isNew: true } });
    }
    isExist = false;
  };


  const {
    title = '',
    first_name: firstName = '',
    last_name: lastName = '',
    birth_date: birthDate = '',
    email = '',
    phone_code: phoneCode = '',
    phone_number: phoneNumber = '',
    country = '',
    city = '',
    id = null,
  } = guest;

  const countriesOptions = countries.map(item => (
    {
      value: item.alpha3,
      label: (locale === 'ar') ? item.name_ar : item.name,
      code:  item.country_code,
    }
  ));

  const personTitles = PERSON_TITLE_OPTIONS();

  const initialTitle = personTitles.find(item => item.value === title) || '';
  const initialCountry = countriesOptions.find(item => item.value === country)
    || countriesOptions.find(item => item.value === 'SAU')
    || null;
  const initPhoneCode = phoneCode || (initialCountry ? initialCountry.code : '');
  const phoneCodeField = formApi.getValue('phone_code');

  return (
    <div className="form-section zr-form-section-guest">
      <h4 className="zr-add-reservation-subtitle">{i18n.t('reservation.guest')}</h4>
      <div className="row zr-row">
        <div className="col zr-col zr-width-flex-24">
          <CustomSelect
            field="title"
            className="plain-select"
            classNamePrefix="plain-select"
            initialValue={initialTitle}
            optionValue={formTitle}
            options={personTitles}
            placeholder={i18n.t('reservation.title')}
            onChange={item => showReservationPopup({ formTitle: item })}
          />
        </div>
        <div className="col zr-col zr-width-flex-38">
          <Text
            field="first_name"
            id="guest-first_name"
            className={cx('form-control', {
              'is-invalid': formState.errors.first_name,
            })}
            initialValue={firstName}
            placeholder={i18n.t('reservation.first_name')}
            validate={validateTextField}
          />
          <Text
            field="id"
            id="guest-id"
            type="hidden"
            initialValue={id}
          />
          <div className="invalid-feedback">
            {formState.errors.first_name}
          </div>
        </div>
        <div className="col zr-col zr-width-flex-38">
          <CustomAsyncSelect
            id="guest-last_name"
            field="last_name"
            placeholder={i18n.t('reservation.last_name')}
            className={cx('plain-select form-control country-field guest-search', {
              'is-invalid': formState.errors.last_name,
            })}
            classNamePrefix="plain-select"
            initialValue={lastName}
            name="last_name"
            isSearchable
            loadOptions={(inputValue, callback) => {
              setTimeout(() => {
                callback(sortList(inputValue, guests, 'last_name'));
              }, 1000);
            }}
            formatOptionLabel={formatGuestOption}
            optionValue={formLastName}
            isNewOption={formLastName && formLastName.isNew}
            onChange={(item) => {
              handleChangeEvent(item, 'formLastName', 'last_name');
            }}
            blurInputOnSelect
            onBlur={(event) => {
              const inputValue = event.target.value;
              handleBlurEvent(inputValue, 'formLastName', 'last_name');
            }}
            validate={validateTextField}
          />
          <div className="invalid-feedback">
            {formState.errors.last_name}
          </div>
        </div>
      </div>
      <div className="row zr-row">
        <div
          className={cx('col zr-col zr-width-flex-24 phone-code-field',
            { filled: (!!phoneCodeField && phoneCodeField.toString().length) })}
        >
          <Text
            field="phone_code"
            type="number"
            id="guest-phone_code"
            className={cx('form-control', {
              'is-invalid': formState.errors.phone_code,
            })}
            initialValue={initPhoneCode}
            placeholder={i18n.t('reservation.code')}
            validate={validateTextField}
          />
          <div className="invalid-feedback">
            {formState.errors.phone_code}
          </div>
          <span className="phone-code-prefix" />
        </div>
        <div className="col zr-col zr-width-flex-38">
          <CustomAsyncSelect
            id="guest-phone_number"
            field="phone_number"
            placeholder={i18n.t('reservation.phone')}
            className={cx('plain-select form-control country-field guest-search', {
              'is-invalid': formState.errors.phone_number && formState.errors.email,
            })}
            classNamePrefix="plain-select"
            initialValue={phoneNumber}
            name="phone_number"
            isSearchable
            loadOptions={(inputValue, callback) => {
              setTimeout(() => {
                callback(sortList(inputValue, guests, 'phone_number'));
              }, 1000);
            }}
            formatOptionLabel={formatGuestOption}
            optionValue={formPhone}
            isNewOption={formPhone && formPhone.isNew}
            onChange={(item) => {
              handleChangeEvent(item, 'formPhone', 'phone_number');
            }}
            blurInputOnSelect
            onBlur={(event) => {
              const inputValue = event.target.value;
              handleBlurEvent(inputValue, 'formPhone', 'phone_number');
            }}
            validate={() => validateEmailOrPhone(formState.values)}
          />
          <div className="invalid-feedback">
            {formState.errors.phone_number}
          </div>
        </div>
        <div className="col zr-col zr-width-flex-38">
          <CustomAsyncSelect
            id="guest-email"
            field="email"
            placeholder={i18n.t('reservation.email')}
            className={cx('plain-select form-control country-field guest-search', {
              'is-invalid': formState.errors.email && formState.errors.phone_number,
            })}
            classNamePrefix="plain-select"
            initialValue={email}
            name="email"
            isSearchable
            loadOptions={(inputValue, callback) => {
              setTimeout(() => {
                callback(sortList(inputValue, guests, 'email'));
              }, 1000);
            }}
            formatOptionLabel={formatGuestOption}
            optionValue={formEmail}
            isNewOption={formEmail && formEmail.isNew}
            onChange={(item) => {
              handleChangeEvent(item, 'formEmail', 'email');
            }}
            blurInputOnSelect
            onBlur={(event) => {
              const inputValue = event.target.value;
              handleBlurEvent(inputValue, 'formEmail', 'email');
            }}
            validate={() => validateEmailOrPhone(formState.values)}
          />
          <div className="invalid-feedback">
            {formState.errors.email}
          </div>
        </div>
      </div>
      <div className="row zr-row">
        <div className="col zr-col zr-width-flex-24">
          <CustomDatePicker
            field="birth_date"
            id="birth_date"
            initialValue={birthDate}
            value={formBDay ? moment(formBDay).format('DD MMM YYYY') : ''}
            dateFormat="DD MMM YYYY"
            onChange={(date) => { showReservationPopup({ formBDay: date }); }}
            inputProps={{ placeholder: i18n.t('reservation.birth_date') }}
            isValidDate={current => current.isBefore(moment().subtract(1, 'day'))}
          />
        </div>
        <div className="col zr-col zr-width-flex-38">
          <CustomSelect
            field="country"
            id="guest-country"
            className={cx('plain-select form-control country-field', {
              'is-invalid': formState.errors.country,
            })}
            classNamePrefix="plain-select"
            initialValue={initialCountry}
            optionValue={formCountry || initialCountry}
            options={countriesOptions}
            placeholder={i18n.t('reservation.country')}
            validate={validateTextField}
            onChange={(item) => {
              formApi.setValue('phone_code', item.code);
              showReservationPopup({ formCountry: item });
            }}
          />
          <div className="invalid-feedback">
            {formState.errors.country}
          </div>
        </div>
        <div className="col zr-col zr-width-flex-38">
          <Text
            field="city"
            id="guest-city"
            className="form-control"
            initialValue={city}
            placeholder={i18n.t('reservation.city')}
          />
          <div className="invalid-feedback">
            {formState.errors.city}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  form:   state.reservations.form,
  locale: state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(GuestFields);
