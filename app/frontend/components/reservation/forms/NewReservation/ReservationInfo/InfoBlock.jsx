import React from 'react';
import i18n from 'i18next';
import { ContentGreyBlock } from '../../../../Wrappers';

const InfoBlock = ({ defaultRate = 100, totalNights = 1 }) => {
  const totalPrice = defaultRate * totalNights;
  const taxesMultiplier = 1.075;
  const priceForTotalNightsTranslation = totalNights > 1 ? 'total_price_for_nights' : 'total_price_for_night';
  return (
    <ContentGreyBlock className="justify-content-center">
      <div className="p-4">
        <p className="text-right mb-2">
          <b className="pl-2 pr-2">{`${i18n.t('reservation.avr_price_per_night')}:`}</b>
          {i18n.t('common.money_amount', { amount: defaultRate * taxesMultiplier })}
        </p>
        <p className="text-right mb-0">
          <b className="pl-2 pr-2">
            {`${i18n.t(`reservation.${priceForTotalNightsTranslation}`, { night_count: totalNights })}:`}
          </b>
          {i18n.t('common.money_amount', { amount: totalPrice * taxesMultiplier })}
        </p>
      </div>
    </ContentGreyBlock>
  );
};

export default InfoBlock;
