import React from 'react';
import i18n from 'i18next';
import StayDates from './StayDates';

const StayDetailsFields = ({
  stayDetails,
  formState,
  formApi,
}) => (
  <div className="form-section zr-form-section-stay">
    <h4 className="zr-add-reservation-subtitle">{i18n.t('reservation.stay_details')}</h4>
    <div className="row zr-row">
      <StayDates {...stayDetails} formApi={formApi} formState={formState} />
    </div>
  </div>
);

export default StayDetailsFields;
