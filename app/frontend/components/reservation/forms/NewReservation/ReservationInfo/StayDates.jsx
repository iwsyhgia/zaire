import React, { Fragment } from 'react';
import moment from 'moment';
import i18n from 'i18next';
import { CustomDatePicker } from '../../../../controls/FormControls';
import { isValidMoment, isCheckinDateDisabled } from '../../../../../utils/reservation/form';

const StayDates = ({
  startDate,
  endDate,
  onStartDateChange = () => { },
  onEndDateChange = () => { },
  formState,
  formApi,
  withLabel = true,
  direction = 'row',
  status,
}) => {

  const { check_in_date: checkIn, check_out_date: checkOut } = formState.values;

  let checkInDate = moment.isMoment(checkIn) ? checkIn : moment(checkIn);
  let checkOutDate = moment.isMoment(checkOut) ? checkOut : moment(checkOut);

  if (!checkInDate.isValid()) {
    checkInDate = startDate;
  }

  if (!checkOutDate.isValid()) {
    checkOutDate = endDate;
  }

  const wrapedClass = (direction === 'row') ? 'col zr-width-flex-24' : 'zr-edit-guest-field-wrap';

  // TODO: refactor dates. There is multi strange render
  // + While dateFormat="DD MMM YYYY" to show set year insted of current one,
  // dates will be in 'DD MMM' format as soon as we have controlled value in format 'DD MMM'

  return (
    <Fragment>
      <div className={wrapedClass}>
        <CustomDatePicker
          field="check_in_date"
          label={withLabel ? i18n.t('reservation.check_in') : null}
          initialValue={startDate}
          value={checkInDate.format('DD MMM')}
          dateFormat="DD MMM YYYY"
          inputProps={{ disabled: isCheckinDateDisabled(checkInDate, status) }}
          onChange={(date) => {
            // TODO: remove formState dependence!
            if (date.isSameOrAfter(checkOutDate)) {
              const shiftedEndDate = date.clone().add(1, 'days');
              formApi.setValue('check_out_date', shiftedEndDate);
            }
            onStartDateChange(date);
          }}
          isValidDate={isValidMoment}
        />
      </div>
      <div className={wrapedClass}>
        <CustomDatePicker
          field="check_out_date"
          label={withLabel ? i18n.t('reservation.check_out') : null}
          initialValue={endDate}
          value={checkOutDate.format('DD MMM')}
          dateFormat="DD MMM YYYY"
          onChange={onEndDateChange}
          isValidDate={isValidMoment}
        />
      </div>
    </Fragment>
  );
};

export default StayDates;
