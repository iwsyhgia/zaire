import React from 'react';
import moment from 'moment';
import i18n from 'i18next';
import { hasValue }  from '../../../../utils/helpers';
import { ContentGreyBlock } from '../../../Wrappers';

const InfoBlock = ({ current = {}, countries, locale }) => {

  const {
    guest: {
      id,
      title,
      first_name: firstName,
      last_name: lastName,
      birth_date: birthDate,
      country,
      city,
      phone_code: code,
      phone_number: phone,
      email,
    } = {},
    room: {
      room_type_name: roomTypeName,
      number,
    } = {},
    id: reservationId,
    check_in_date: checkIn,
    check_out_date: checkOut,
    number_of_adults: numberAdults,
    number_of_children: numberChildren,
    rate = 'non_refundable', // TODO: get this value from BE after the development of Rate management
    taxes_multiplier: taxesMultiplier = 1.075, // TODO: and this
    total_nights: totalNights,
    total_price: totalPrice = 100, // TODO: and this
    price_per_night: perNight = 100, // TODO: and this
  } = current;

  const selectedCountry = countries.find(item => item.alpha3 === country);
  const localeCountryName = selectedCountry && (locale === 'ar' ? selectedCountry.name_ar : selectedCountry.name);
  const countryName = localeCountryName || country;
  const personTitle = hasValue(title) ? i18n.t(`common.${title}`) : '';
  const totalNightsTranslation = totalNights > 1 ? 'price_for_nights' : 'price_for_night';

  return (
    <ContentGreyBlock className="justify-content-start p-4 mb-3">
      <div className="row">
        <div className="row m-0 w-100">
          <div className="col-4 zr-guest-info">
            <h3>{`${personTitle} ${firstName} ${lastName}`}</h3>
          </div>
          <div className="col-8 zr-guest-info">
            <h3>{i18n.t('reservation.stay_details')}</h3>
          </div>
        </div>
        <div className="row m-0 w-100">
          <div className="col-4 zr-guest-info">
            <ul className="list-unstyled">
              {birthDate && <li>{`${i18n.t('reservation.birth_date')} ${moment(birthDate).format('DD MMM YYYY')}`}</li>}
              <li>{`${countryName}, ${city}`}</li>
              <li>{`#${id}`}</li>
              {phone && <li>{`+${code} ${phone}`}</li>}
              {email && <li>{email}</li>}
            </ul>
          </div>
          <div className="col-4">
            <ul className="list-unstyled">
              <li>{`#${reservationId}`}</li>
              <li>{`${moment(checkIn).format('DD MMM')} - ${moment(checkOut).format('DD MMM')}`}</li>
              <li>
                {`${numberAdults} ${i18n.t('reservation.adults')}, ${numberChildren} ${i18n.t('reservation.children')}`}
              </li>
            </ul>
          </div>
          <div className="col-4">
            <ul className="list-unstyled">
              <li>{`${totalNights} ${totalNights > 1 ? i18n.t('common.nights') : i18n.t('common.night')}`}</li>
              <li>{roomTypeName}</li>
              <li>{`${i18n.t('common.room')} #${number}`}</li>
            </ul>
          </div>
        </div>
      </div>
    </ContentGreyBlock>
  );
};

export default InfoBlock;
