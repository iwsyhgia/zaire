import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Form } from 'informed';
import i18n from 'i18next';
import _ from 'lodash';
import TableTitle from '../../details/Table/rows/Title';
import TableRow from '../../details/Table/rows/Row';
import TableTotal from '../../details/Table/balance/Total';
import { IconSend } from '../../../icons/ReservationIcons';
import ReservationBalance from '../../details/Table/balance/ReservationBalance';
import InfoBlock from './InfoBlock';
import { Button, SectionBlock } from '../../../Wrappers';
import { showReservationPopup as showReservationPopupAction } from '../../../../actions/reservations';


const ViewInvoice = ({
  current = {},
  countries,
  locale,
  combinedInvoices,
  generalPayments,
  firstName,
  lastName,
  showReservationPopup,
}) => (
  <Form name="view-invoice" className="checkin-reservation-form">
    <div className="popup-title">
      <div className="font-20">{i18n.t('reservation.invoice')}</div>
    </div>
    <div className="p-3">
      <InfoBlock isViewInvoice locale={locale} current={current} countries={countries} />
      <div>
        <SectionBlock title={<TableTitle isViewInvoice />} className="zr-reservation-widget-table">
          <div className="zr-reservation-table">
            { combinedInvoices && _.sortBy(combinedInvoices, 'created_at').map(payment => (
              <TableRow
                payment={payment}
                currentReservation={current}
                userName={`${firstName} ${lastName}`}
                isViewInvoice
              />
            ))}
          </div>
        </SectionBlock>
        <TableTotal isViewInvoice generalPayments={generalPayments} />
        <ReservationBalance isViewInvoice generalPayments={generalPayments} />
      </div>
      <div className="popup-footer px-0">
        <div className="popup-footer-col">
          <Button
            type="button"
            className="btn-clear popup-footer-btn"
            onClick={() => showReservationPopup({ isShow: false })}
          >
            {i18n.t('common.cancel')}
          </Button>
        </div>
        <div className="popup-footer-col">
          <div className="popup-footer-btn-row">
            <div className="popup-footer-btn-col">
              <Button
                type="button"
                className="btn-primary popup-footer-btn btn-back"
                onClick={() => window.print()}
              >
                <span className="popup-footer-btn-text">{i18n.t('common.print')}</span>
              </Button>
            </div>
            <div className="popup-footer-btn-col">
              <Button type="button" className="btn-primary zr-inner-header-btn zr-button-padding" onClick={() => { }}>
                <IconSend width="16" height="18" className="icon icon-send" />
                <span className="popup-footer-btn-text">{i18n.t('common.send')}</span>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Form>
);

const mapStateToProps = state => ({
  current:          state.reservations.current,
  countries:        state.common.countries,
  locale:           state.common.locale,
  combinedInvoices: state.payments.combined_invoices,
  generalPayments:  state.payments.general_payments,
  firstName:        state.profile.form.first_name.value,
  lastName:         state.profile.form.last_name.value,
});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ViewInvoice));
