import React from 'react';
import i18n from 'i18next';
import { PageBlock } from '../Wrappers';
import ReservationPopup from './forms/ReservationPopup';
import Frontdesk from './Frontdesk';

const Dashboard = () => (
  <PageBlock title={i18n.t('common.reservations')} className="zr-reservation-page">
    <Frontdesk />
    <ReservationPopup />
  </PageBlock>
);

export default Dashboard;
