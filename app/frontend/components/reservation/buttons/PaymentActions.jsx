import React from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import ReactTooltip from 'react-tooltip';
import { showReservationPopup as showReservationPopupAction } from '../../../actions/reservations';
import ViewInvoice from '../forms/ViewInvoice/ViewInvoice';
import { IconRates, IconReciept, IconDiscount, IconRefund, IconInvoice } from '../../icons/ReservationIcons';
import { SecondaryButton } from '../../controls/Buttons';

const PaymentActions = ({
  current: {
    status,
  },
  handleShow,
  payments,
  isEditInvoice,
  success,
  showReservationPopup,
  combinedInvoices,
}) => (
  <ul className="zr-inner-content-tools">
    <li className="zr-inner-content-tool">
      <SecondaryButton
        className="zr-inner-content-tool-btn"
        onClick={() => {
          handleShow('charge', 'add');
        }}
        disabled={status === 'no_show'}
      >
        <IconReciept width="15" height="15" className="icon icon-reciept" />
        <span className="zr-inner-content-tool-btn-text">{i18n.t('reservation.add_charge')}</span>
      </SecondaryButton>
    </li>
    <li className="zr-inner-content-tool">
      <SecondaryButton
        disabled={status === 'no_show' || (isEditInvoice && success)}
        className="zr-inner-content-tool-btn"
        onClick={() => {
          handleShow('discount', 'add');
        }}
      >
        <IconDiscount width="15" height="15" className="icon icon-discount" />
        <span className="zr-inner-content-tool-btn-text">{i18n.t('reservation.add_discount')}</span>
      </SecondaryButton>
    </li>
    <li className="zr-inner-content-tool">
      <SecondaryButton
        className="zr-inner-content-tool-btn"
        onClick={() => {
          handleShow('refund', 'add');
        }}
        disabled={status === 'no_show' || !payments.length || (isEditInvoice && success)}
      >
        <IconRefund width="15" height="15" className="icon icon-add-refund" />
        <span className="zr-inner-content-tool-btn-text">{i18n.t('reservation.add_refund')}</span>
      </SecondaryButton>
    </li>
    <li className="zr-inner-content-tool">
      <SecondaryButton
        disabled={status === 'no_show' || (isEditInvoice && success)}
        className="zr-inner-content-tool-btn"
        onClick={() => {
          handleShow('payment', 'add');
        }}
      >
        <IconRates width="15" height="15" className="icon icon-rates" />
        <span className="zr-inner-content-tool-btn-text">{i18n.t('reservation.add_payment')}</span>
      </SecondaryButton>
    </li>
    <li className="zr-inner-content-tool" data-tip={i18n.t('profile.errors.no_payments')}>
      <SecondaryButton
        className="zr-inner-content-tool-btn"
        disabled={!combinedInvoices.length}
        onClick={() => showReservationPopup({
          isShow:          true,
          showedComponent: <ViewInvoice />,
        })}
      >
        {!combinedInvoices.length && <ReactTooltip place="bottom" type="dark" effect="solid" />}
        <IconInvoice width="15" height="15" className="icon icon-invoice" />
        <span className="zr-inner-content-tool-btn-text">{i18n.t('reservation.view_invoice')}</span>
      </SecondaryButton>
    </li>
  </ul>
);

const mapStateToProps = state => ({
  success: state.payments.success,
});

const mapDispatchToProps = dispatch => ({
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentActions);
