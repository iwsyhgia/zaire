import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import i18n from 'i18next';
import { getUpdatedGuestField } from '../../../utils/forms';
import { STEPS } from '../../../constants/constants';
import { IconEdit, IconCancel, IconCheckIn, IconCheckOut, IconSend } from '../../icons/ReservationIcons';
import CancelReservation from '../forms/CancelReservation/CancelReservation';
import { PrimaryButton } from '../../controls/Buttons';
import {
  checkInReservation as checkInReservationAction,
  checkOutReservation as checkOutReservationAction,
  showReservationPopup as showReservationPopupAction,
} from '../../../actions/reservations';
import { getAvailableRooms as getAvailableRoomsAction } from '../../../actions/roomsActions';


const PageActions = ({
  reservationId,
  locale,
  countries,
  current: {
    status,
    guest,
    check_in_date: checkInDate,
    check_out_date: checkOutDate,
  },
  handleShow,
  getAvailableRooms,
  checkOutReservation,
  showReservationPopup,
  lastCompletedAudit,
}) => (
  <Fragment>
    {
      status === 'checked_in'
      && (
        <li className="zr-inner-header-tool">
          <PrimaryButton className="zr-inner-header-btn" onClick={() => checkOutReservation(reservationId)}>
            <IconCheckOut width="15" height="15" className="icon icon-checkout" />
            <span className="zr-inner-header-btn-text">{i18n.t('reservation.check_out')}</span>
          </PrimaryButton>
        </li>
      )
    }
    {
      ['unconfirmed', 'confirmed'].includes(status)
      && (
        <li className="zr-inner-header-tool">
          <PrimaryButton
            type="button"
            className="zr-inner-header-btn"
            disabled={!(moment(checkInDate).isSame(moment(lastCompletedAudit.time_frame_end), 'day'))}
            onClick={() => showReservationPopup({
              step:                   STEPS.CHECKIN,
              isFrontdeskReservation: false,
              isEdit:                 false,
            })}
          >
            <IconCheckIn width="15" height="15" className="icon icon-checkin" />
            <span className="zr-inner-header-btn-text">{i18n.t('common.check_in')}</span>
          </PrimaryButton>
          <span className="validation-tooltip">{i18n.t('reservation.only_current_date_check_in')}</span>
        </li>
      )
    }
    {
      ['unconfirmed', 'confirmed', 'checked_in'].includes(status)
      && (
        <li className="zr-inner-header-tool">
          <PrimaryButton
            className="zr-inner-header-btn"
            onClick={() => {
              const from = moment(checkInDate).format('DD/MM/YYYY');
              const to = moment(checkOutDate).format('DD/MM/YYYY');
              const formData = getUpdatedGuestField(guest, countries, locale);
              getAvailableRooms(from, to);
              showReservationPopup({ step: STEPS.EDIT, isFrontdeskReservation: false, isEdit: true, ...formData });
              handleShow();
            }}
          >
            <IconEdit width="16" height="16" className="icon icon-edit" />
            <span className="zr-inner-header-btn-text">{i18n.t('reservation.edit_reservation')}</span>
          </PrimaryButton>
        </li>
      )
    }
    {
      status === 'checked_in' ? (
        <li className="zr-inner-header-tool">
          <PrimaryButton className="zr-inner-header-btn" onClick={() => { }}>
            <IconSend width="16" height="18" className="icon icon-send" />
            <span className="zr-inner-header-btn-text">{i18n.t('reservation.send_email_sms')}</span>
          </PrimaryButton>
        </li>
      ) : (
        ['unconfirmed', 'confirmed'].includes(status)
        && (
          <li className="zr-inner-header-tool">
            <PrimaryButton
              className="zr-inner-header-btn"
              onClick={() => showReservationPopup({
                isShow:          true,
                showedComponent: <CancelReservation />,
              })}
            >
              <IconCancel width="14" height="16" className="icon icon-cancel" />
              <span className="zr-inner-header-btn-text">{i18n.t('reservation.cancel_reservation')}</span>
            </PrimaryButton>
          </li>
        )
      )
    }
  </Fragment>
);

const mapStateToProps = state => ({
  lastCompletedAudit: state.audit.lastCompleted,
});

const mapDispatchToProps = dispatch => ({
  checkOutReservation:  (id) => { dispatch(checkOutReservationAction(id)); },
  checkInReservation:   (data) => { dispatch(checkInReservationAction(data)); },
  showReservationPopup: (data) => { dispatch(showReservationPopupAction(data)); },
  getAvailableRooms:    (from, to) => { dispatch(getAvailableRoomsAction(from, to)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(PageActions);
