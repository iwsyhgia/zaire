import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import i18n from 'i18next';
import { ImagesBlock } from '../../../templates/room_types';
import { ImageControlsEdit } from '../../controls/HoverControls';
import ImageUploader from './ImageUploader';
import { updateImages as updateImagesAction } from '../../../actions/roomTypes';

class ImagesEditable extends Component {

  static propTypes = {
    images:       PropTypes.arrayOf(PropTypes.object).isRequired,
    updateImages: PropTypes.func.isRequired,
  };

  onRemove = (ind) => { // TODO: DIRTY -  hash of image for search
    const { updateImages, images } = this.props;
    const copy = [...images];
    const image = copy[ind];

    copy.splice(ind, 1);

    if (typeof image.id !== 'undefined') {
      image._destroy = true;
      copy.push(image);
    }

    updateImages([...copy]);
  }


  render() {

    const { images } = this.props;

    const actualImages = images.filter(item => !item._destroy);

    return (
      <Fragment>
        <ImagesBlock length={actualImages.length}>
          <div className="room-images-list units-list d-flex flex-wrap flex-grow-1 px-3">
            <Scrollbars
              style={{ height: 175 }}
              renderView={props => <div {...props} className="zr-scrollbar-container" />}
              renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
            >
              {
                actualImages
                  .map((item, ind) => ((typeof item.id === 'undefined')
                    ? { ...item, id: `{temp-${Date.now() + ind}-${Math.random()}` }
                    : item))
                  .map((item, ind) => (
                    <div
                      key={`image-${item.id}`}
                      className="d-inline-block units-list-item mb-2"
                    >
                      <img
                        src={item.base64 || item.path}
                        data-toggle="modal"
                        data-id={`image-${item.id}`}
                        alt="room"
                      />
                      <ImageControlsEdit onRemove={() => this.onRemove(ind)} />
                    </div>
                  ))
                }
            </Scrollbars>
          </div>
          <ImageUploader />
          <div className="pt-3" style={{ boxSizing: 'border-box' }} />
          <div className="small px-3">
            {i18n.t('rooms.image_resolution_invalid', { resolution: '300x300' })}
            <br />
            {i18n.t('rooms.image_size_invalid')}
            <br />
            {i18n.t('rooms.image_format_invalid')}
          </div>
        </ImagesBlock>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  locale:     state.common.locale,
  images:     state.room_types.current.images,
  roomTypeID: state.room_types.current.id,
  loading:    state.amenities.loading,
  error:      state.amenities.error,
});

const mapDispatchToProps = dispatch => ({
  updateImages: (data) => { dispatch(updateImagesAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ImagesEditable);
