import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import cx from 'classnames';
import { ImageView } from '../../controls/HoverControls';
import ImageLoader from '../../ImageLoader';
import { ImagesBlock } from '../../../templates/room_types';


class Images extends React.Component {

  static propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  state = {
    photoIndex: null,
    isClosed:   true,
  };

  openImage = (id) => {
    this.setState({ photoIndex: id, isClosed: false });
  }

  closeImage = () => {
    this.setState({ photoIndex: null, isClosed: true });
  }

  loadPhotos = () => {
    const { items } = this.props;
    const { photoIndex } = this.state;

    return [...items].map(image => (
      <div
        className={cx('carousel-item', { active: photoIndex === image.id })}
        key={`image-${image.id}`}
      >
        <img className="d-block w-100" src={image.base64 || image.path} alt="Room" />
      </div>
    ));
  };


  render() {

    const { items } = this.props;
    const { isClosed } = this.state;

    return (
      <ImagesBlock length={items.length} className="zr-room-type-images">
        <div className="room-images-list units-list d-flex flex-wrap flex-grow-1 px-3">
          <Scrollbars
            style={{ height: 175 }}
            renderView={props => <div {...props} className="zr-scrollbar-container" />}
            renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
          >
            {
              items.map((item, ind) => (
                <div
                  key={`image-${item.id}`}
                  className="d-inline-block units-list-item mb-2"
                  onClick={() => this.openImage(item.id)}
                  role="presentation"
                >
                  <img
                    src={item.path}
                    data-toggle="modal"
                    data-id={`image-${item.id}`}
                    alt="room"
                  />
                  <ImageView dataToggle="modal" onClick={() => this.openImage(item.id)} dataId={ind} />
                </div>
              ))
            }
          </Scrollbars>
        </div>
        {!isClosed && (
          <ImageLoader
            show={!isClosed}
            photos={this.loadPhotos()}
            closeImage={() => this.closeImage()}
          />
        )}
      </ImagesBlock>
    );
  }

}

export default Images;
