import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import i18n from 'i18next';
import IconPlus from '../../icons/IconPlus';
import ErrorPopup from '../../notifications/ErrorPopup';
import { MIN_IMAGE_SIZE, MAX_IMAGE_SIZE } from '../../../constants/constants';
import { updateImages as updateImagesAction } from '../../../actions/roomTypes';

class ImageUploader extends React.Component {

  static propTypes = {
    images:       PropTypes.arrayOf(PropTypes.object).isRequired,
    updateImages: PropTypes.func.isRequired,
  };

  state = {
    error: null,
  };

  uploadImage = (event) => {
    const { updateImages, images } = this.props;

    if (window.File && window.FileList && window.FileReader) {

      const allowedTypes = ['jpeg', 'jpg', 'png'];

      const files = event.target.files;
      const filesLength = files.length;
      const loadedImages = [];

      for (let i = 0; i < filesLength; i += 1) {
        const file = files[i];

        const fileType = file.type;
        const isTypeAllowed = allowedTypes.some(type => (fileType.indexOf(type) > -1));

        if (!isTypeAllowed) {
          this.setState({ error: i18n.t('rooms.image_format_invalid') });
          return;
        }

        const fileSize = file.size / 1024 / 1024; // in MB
        if (fileSize < MIN_IMAGE_SIZE || fileSize > MAX_IMAGE_SIZE) {
          this.setState({ error: i18n.t('rooms.image_size_invalid') });
          return;
        }

        if (fileType.match('image')) {

          const reader = new FileReader();

          reader.onload = (fileEvent) => {
            loadedImages.push({ base64: fileEvent.target.result });
            if (loadedImages.length === filesLength) {

              updateImages([...images, ...loadedImages]);

            }
          };
          reader.onerror = () => {
            // console.log('Error: ', error);
          };
          reader.readAsDataURL(file);

        }
      }
    } else {
      // console.log('Your browser does not support File API');
    }

    this.imageInput.value = '';
  }

  render() {
    const { error }  = this.state;

    return (
      <Fragment>
        <div className="zr-image-uploader">
          <label className="cursor-pointer">
            <input
              className="invisible"
              style={{ width: '20px' }}
              type="file"
              multiple="multiple"
              onChange={this.uploadImage}
              ref={(ref) => { this.imageInput = ref; }}
            />
            <IconPlus width="16" height="16" />
          </label>
        </div>
        <ErrorPopup
          show={!_.isEmpty(error)}
          onHide={() => this.setState({ error: null })}
        >
          <p>{error}</p>
        </ErrorPopup>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  images: state.room_types.current.images,
});

const mapDispatchToProps = dispatch => ({
  updateImages: (data) => { dispatch(updateImagesAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);
