import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import i18n from 'i18next';
import RoomTypeEditable from './RoomTypeEditable';
import { SecondaryButton } from '../controls/Buttons';
import ConfirmationPopup from '../notifications/ConfirmationPopup';
import {
  updateRoomType as updateRoomTypeAction,
  loadRoomType as loadRoomTypeAction,
  getRoomType as getRoomTypeAction,
  deleteRoomType as deleteRoomTypeAction,
  updateAmenities as updateAmenitiesAction,
  resetRoomTypeState as resetRoomTypeStateAction,
  resetErrors as resetErrorsAction,
} from '../../actions/roomTypes';


class EditRoomType extends Component {

  static propTypes = {
    current:            PropTypes.instanceOf(Object).isRequired,
    list:               PropTypes.arrayOf(Object).isRequired,
    updateRoomType:     PropTypes.func.isRequired,
    loadRoomType:       PropTypes.func.isRequired,
    getRoomType:        PropTypes.func.isRequired,
    deleteRoomType:     PropTypes.func.isRequired,
    resetRoomTypeState: PropTypes.func.isRequired,
    history:            PropTypes.instanceOf(Object).isRequired,
    match:              PropTypes.instanceOf(Object).isRequired,
    locale:             PropTypes.string.isRequired,
  };

  state = {
    isConfirmed: false,
  }

  componentDidMount() {
    const { loadRoomType, getRoomType, list, match: { params: { id } } } = this.props;

    if (list.length === 0) {
      loadRoomType(id);
    } else {
      getRoomType(id);
    }

  }

  onSubmit = (data) => {
    const { updateRoomType, current: { id } } = this.props;
    updateRoomType(id, data);
  }

  deleteRoom = () => {
    this.setState({
      isConfirmed: true,
    });
  }

  submitDelete = () => {
    const { deleteRoomType, current: { id } } = this.props;
    deleteRoomType(id);
    this.setState({ isConfirmed: false });
  }

  render() {
    const { isConfirmed } = this.state;
    const {
      current,
      resetRoomTypeState,
      history,
      locale,
    } = this.props;

    if (current.success === true || current.deleted === true) {
      resetRoomTypeState();
      const lang = locale === 'ar' ? '/ar' : '';
      history.push(`${lang}/room_types`);
    }

    const localizedName = (locale === 'ar' && !!current.name_ar)
      ? current.name_ar
      : current.name;

    return (
      <Fragment>
        <div className="page-sub-title d-flex justify-content-between mb-4">
          <h2 className="m-0">{`${i18n.t('rooms.edit_room_type')} ${localizedName}`}</h2>
          <SecondaryButton onClick={this.deleteRoom}>{i18n.t('rooms.delete_room_type')}</SecondaryButton>
        </div>
        <RoomTypeEditable
          isNew={false}
          onSubmit={this.onSubmit}
        />
        <ConfirmationPopup
          show={isConfirmed}
          submitText={i18n.t('common.delete')}
          onSubmit={this.submitDelete}
          onHide={() => this.setState({ isConfirmed: false })}
        >
          <p>{i18n.t('rooms.are_you_sure_you_want_to_delete_room_type', { name: localizedName })}</p>
        </ConfirmationPopup>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  list:    state.room_types.list,
  current: state.room_types.current,
  success: state.room_types.success,
  locale:  state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  updateRoomType:     (id, data) => { dispatch(updateRoomTypeAction(id, data)); },
  loadRoomType:       (id) => { dispatch(loadRoomTypeAction(id)); },
  getRoomType:        (id) => { dispatch(getRoomTypeAction(id)); },
  deleteRoomType:     (id) => { dispatch(deleteRoomTypeAction(id)); },
  updateAmenities:    (data) => { dispatch(updateAmenitiesAction(data)); },
  resetRoomTypeState: () => { dispatch(resetRoomTypeStateAction()); },
  resetErrors:        () => { dispatch(resetErrorsAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditRoomType));
