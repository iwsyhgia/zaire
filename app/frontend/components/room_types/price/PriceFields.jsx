import React from 'react';
import { connect } from 'react-redux';
import { Text, withFormState  } from 'informed';
import cx from 'classnames';
import i18n from 'i18next';
import { validateTextField } from './validator';

const PriceFieldsComponent = ({ current, formState }) => (
  <div className="content-block grey-block d-flex zr-content-check-actions">
    <div className="zr-payment-form-line-col zr-width-flex-25 mb-0">
      <div className="zr-payment-form-fields zr-width-flex-100">
        <div className="zr-payment-form-field zr-width-flex-50 zr-dynamic-price-element">
          {
            formState.values.kind === 'dynamic' && (
              <div className="zr-payment-form-field zr-width-flex-30">
                <p>{i18n.t('hotel.from')}</p>
              </div>
            )
          }
          <div className="zr-payment-form-field zr-width-flex-55">
            <Text
              field="lower_bound"
              id="lower_bound"
              type="number"
              className={cx('form-control zr-price-field-width', {
                'is-invalid': formState.errors.lower_bound,
              })}
              initialValue={current.rate ? current.rate.lower_bound : ''}
              validate={validateTextField}
            />
            <div className="invalid-feedback">
              {formState.errors.lower_bound}
            </div>
          </div>
          <div className="zr-payment-form-field zr-width-flex-15">
            <p>{i18n.t('reservation.SAR')}</p>
          </div>
        </div>
        {
          formState.values.kind === 'dynamic' && (
            <div className="zr-payment-form-field zr-width-flex-50 zr-dynamic-price-element ml-1">
              <div className="zr-payment-form-field zr-width-flex-15">
                <p>{i18n.t('hotel.to')}</p>
              </div>
              <div className="zr-payment-form-field zr-width-flex-55">
                <Text
                  field="upper_bound"
                  id="upper_bound"
                  type="number"
                  className={cx('form-control zr-price-field-width', {
                    'is-invalid': formState.errors.upper_bound,
                  })}
                  initialValue={current.rate ? current.rate.upper_bound : ''}
                  validate={() => validateTextField(formState.values)}
                />
                <div className="invalid-feedback">
                  {formState.errors.upper_bound}
                </div>
              </div>
              <div className="zr-payment-form-field zr-width-flex-5">
                <p>{i18n.t('reservation.SAR')}</p>
              </div>
            </div>
          )
        }
      </div>
    </div>
  </div>
);


const mapStateToProps = state => ({
  locale:  state.common.locale,
  current: state.room_types.current,
});

const mapDispatchToProps = () => ({});

const PriceFields = withFormState(PriceFieldsComponent);
export default connect(mapStateToProps, mapDispatchToProps)(PriceFields);
