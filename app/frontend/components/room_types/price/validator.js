import i18n from 'i18next';

export const validateTextField = (errorValue) => {
  if (!errorValue || errorValue === '') return i18n.t('common.field_required');
  if (!errorValue || +errorValue <= 0) return i18n.t('common.field_positive');
  if (errorValue.kind === 'dynamic' && errorValue.upper_bound === '') return i18n.t('common.field_required');
  if (errorValue.kind === 'dynamic' && +errorValue.lower_bound >= +errorValue.upper_bound) {
    return i18n.t('common.higher_more_lower');
  }
  return null;
};


export default validateTextField;
