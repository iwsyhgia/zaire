import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { RadioGroup, Radio } from 'informed';
import i18n from 'i18next';
import PriceFields from './PriceFields';

const Price = ({ current, isNew }) => {
  if (!isNew && !current.rate) return null;

  return (
    <div className="room-type-item">
      <div className="px-3 pt-3 pb-0">
        <div className="page-sub-title d-flex justify-content-start mb-3">
          <h3 className="mb-0">{i18n.t('rooms.standard_price_per_night')}</h3>
        </div>
        <div className="plain-form sms-email-form py-1">
          <RadioGroup field="kind" initialValue={current.rate ? current.rate.kind : 'fixed'}>
            <div className="switch-field mb-4">
              <Radio value="fixed" id="kind-fixed" className="form-check-input" checked="checked" />
              <label className="form-check-label radio-control zr-price-tabs-font" htmlFor="kind-fixed">
                {i18n.t('rooms.fixed_price')}
              </label>
              <Radio value="dynamic" id="kind-dynamic" className="form-check-input" />
              <label className="form-check-label radio-control zr-price-tabs-font" htmlFor="kind-dynamic">
                {i18n.t('rooms.dynamic_price')}
              </label>
            </div>
          </RadioGroup>
        </div>
      </div>
      <PriceFields />
    </div>
  );
};


const mapStateToProps = state => ({
  locale:  state.common.locale,
  current: state.room_types.current,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Price);
