import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import i18n from 'i18next';
import RoomTypeEditable from './RoomTypeEditable';
import {
  addRoomType as addRoomTypeAction,
  resetRoomTypeState as resetRoomTypeStateAction,
} from '../../actions/roomTypes';


class AddRoomType extends React.Component {

  static propTypes = {
    history:            PropTypes.string.isRequired,
    // Store
    locale:             PropTypes.string.isRequired,
    current:            PropTypes.instanceOf(Object).isRequired,
    addRoomType:        PropTypes.func.isRequired,
    resetRoomTypeState: PropTypes.func.isRequired,
  };

  onSubmit = (data) => {
    const { addRoomType } = this.props;

    addRoomType(data);
  }

  render() {
    const {
      current,
      resetRoomTypeState,
      history,
      locale,
    } = this.props;

    if (current.success === true) {
      resetRoomTypeState();
      const lang = locale === 'ar' ? '/ar' : '';
      history.push(`${lang}/room_types`);
    }

    const isNew = true;

    return (
      <Fragment>
        <div className="page-sub-title d-flex justify-content-start mb-4">
          <h2 className="mb-0">{i18n.t('rooms.new_room_type')}</h2>
        </div>
        <RoomTypeEditable
          isNew={isNew}
          onSubmit={this.onSubmit}
        />
      </Fragment>
    );
  }

}


const mapStateToProps = state => ({
  locale:  state.common.locale,
  current: state.room_types.current,
});

const mapDispatchToProps = dispatch => ({
  addRoomType:        (data) => { dispatch(addRoomTypeAction(data)); },
  resetRoomTypeState: () => { dispatch(resetRoomTypeStateAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddRoomType));
