import React, { Fragment } from 'react';
import { ContentGreyBlock } from "../../Wrappers";

export const SyncBooking = (props) => {

    return (<ContentGreyBlock className="p-3">
    <div className="form-row align-items-center">
      <div className="col-auto">
        <div className="form-check mb-2">                
          <label className="form-check-label">
          <input className="form-check-input" type="checkbox" onChange={props.onUpdateSync}/>
            Sync with Booking.com
          </label>
        </div>
      </div>
      <div className="col-auto">
        <div className="plain-form">
          <input type="text" className="form-control mb-2" placeholder="Select type" />
        </div>
      </div>
    </div>
  </ContentGreyBlock>);
}