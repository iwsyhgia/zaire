import React, { Fragment } from 'react';
import i18n from 'i18next';

const Fields = ({ fields = {} }) => {
  const {
    localizedName,
    maxOccupancy,
    size,
    unit,
    quantity,
  } = fields;
  return (
    <Fragment>
      <h3 className="mb-4">{localizedName}</h3>
      <dl className="features-list list-unstyled">
        {!!maxOccupancy && (
          <div className="row mb-2">
            <div className="col-sm-7">{i18n.t('rooms.room_type.max_occupancy')}</div>
            <div className="col-sm-5">
              {maxOccupancy}
              {' '}
              {(maxOccupancy > 1) ? i18n.t('common.persons') : i18n.t('common.person')}
            </div>
          </div>
        )}
        {!!size && +size > 0 && (
          <div className="row mb-2">
            <div className="col-sm-7">{i18n.t('rooms.room_type.size_measurement')}</div>
            <div className="col-sm-5">
              {size}
              {' '}
              {(unit) && i18n.t('common.meters')}
            </div>
          </div>
        )}
        {!!quantity && (
          <div className="row">
            <div className="col-sm-7">{i18n.t('rooms.room_type.quantity')}</div>
            <div className="col-sm-5">{quantity}</div>
          </div>
        )}
      </dl>
    </Fragment>
  );
};

export default Fields;
