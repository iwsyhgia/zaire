import React, { Fragment } from 'react';
import { Text, Select, Option } from 'informed';
import _ from 'lodash';
import cx from 'classnames';
import i18n from 'i18next';
import BiField from '../../controls/BiField';
import { PositiveNumber, CustomSelect } from '../../controls/FormControls';
import { MAX_OCCUPANCY_OPTIONS } from '../../../constants/constants';

class FieldsEditable extends React.Component {

  validatePresence = value => (
    !value || value.length === 0 ? i18n.t('common.field_required') : null
  );

  validateEnglishPresence = value => (
    !value || value.trim().length === 0 ? i18n.t('common.english_field_required') : null
  );

  validateOnlySpaces = value => (
    !!value && value.trim().length === 0 ? i18n.t('common.incorrect_field_value') : null
  );


  render() {
    const {
      initValues:{
        // eslint-disable-next-line
        name = '', name_ar:nameAr = '', size = '', unit = 'meters', max_occupancy = '', quantity = '',
      } = {},
    } = this.props;

    const { isNew, formState } = this.props;
    const initialMaxOccupancy = MAX_OCCUPANCY_OPTIONS().find(item => item.value === max_occupancy.toString()) || '';

    if (isNew === false && name === '') return null;

    const nameField = (
      <Fragment>
        <Text
          field="name"
          id="roomtype-name"
          dir="ltr"
          className={cx('form-control', {
            'is-invalid': _.has(formState, 'errors.name'),
            empty:        _.get(formState, 'values.name', '').length === 0,
          })}
          initialValue={name}
          maxLength="30"
          validate={this.validateEnglishPresence}
        />
        <label htmlFor="roomtype-name" className="required">
          {i18n.t('rooms.room_type.name_of_type')}
        </label>
      </Fragment>
    );

    const nameArField = (
      <Fragment>
        <Text
          field="name_ar"
          id="roomtype-name_ar"
          dir="rtl"
          className={cx('form-control', {
            empty: _.get(formState, 'values.name_ar', '').length === 0,
          })}
          initialValue={nameAr}
          maxLength="30"
          validate={this.validateOnlySpaces}
        />
        <label htmlFor="roomtype-name_ar" className="required">
          {i18n.t('rooms.room_type.name_of_type')}
        </label>
      </Fragment>
    );

    const isPlain = true;

    return (
      <div className="plain-form p-0 pt-3">

        <div className="form-row mt-2">
          <div className="col-sm-12">
            <div className="form-group live-placeholder">
              <BiField
                isFirstEmpty={_.get(formState, 'values.name', '').length === 0}
                FirstField={nameField}
                isSecondEmpty={_.get(formState, 'values.name_ar', '').length === 0}
                SecondField={nameArField}
                isPlain={isPlain}
              />
              {
                <div className="invalid-feedback">
                  {_.has(formState, 'errors.name') && `${formState.errors.name} `}
                  {_.has(formState, 'errors.name_ar') && `${formState.errors.name_ar} `}
                </div>
              }
            </div>
          </div>
        </div>

        <div className="form-row mt-4">
          <div className="col-sm-8">
            <div className="form-group live-placeholder plain-select__control">
              <CustomSelect
                field="max_occupancy"
                id="roomtype-max_occupancy"
                className={cx('plain-select required', {
                  'is-invalid': _.has(formState, 'errors.max_occupancy'),
                  empty:        _.get(formState, 'values.max_occupancy', '').length === 0,
                })}
                // eslint-disable-next-line
                initialValue={initialMaxOccupancy}
                validate={this.validatePresence}
                classNamePrefix="plain-select"
                options={MAX_OCCUPANCY_OPTIONS()}
                placeholder={i18n.t('rooms.room_type.max_occupancy')}
              />
              <label htmlFor="roomtype-max_occupancy" className="required">
                {i18n.t('rooms.room_type.max_occupancy')}
              </label>
              {
                _.has(formState, 'errors.max_occupancy')
                && (
                  <div className="invalid-feedback">
                    {formState.errors.max_occupancy}
                  </div>
                )
              }
            </div>
          </div>
        </div>

        <div className="form-row mt-4">
          <div className="col-sm-8">
            <div className="form-group live-placeholder">
              <PositiveNumber
                field="size"
                initialValue={size}
                id="roomtype-size"
                className={cx('form-control', {
                  'is-invalid': _.has(formState, 'errors.size'),
                  empty:        _.get(formState, 'values.size', '').length === 0,
                })}
              />
              <label htmlFor="roomtype-size">
                {i18n.t('rooms.room_type.size_measurement')}
              </label>
              {
                _.has(formState, 'errors.size')
                && (
                  <div className="invalid-feedback">
                    {formState.errors.size}
                  </div>
                )
              }
            </div>
          </div>
          <div className="col-sm-4">
            <div className="form-group live-placeholder">
              <Select field="unit" id="roomtype-unit" className="form-control" initialValue={unit}>
                <Option value="meters">{i18n.t('common.meters')}</Option>
                <Option value="feet">{i18n.t('common.feet')}</Option>
              </Select>
            </div>
          </div>
        </div>

        <div className="form-row mt-4">
          <div className="col-sm-8">
            <div className="form-group live-placeholder">
              <PositiveNumber
                field="quantity"
                type="number"
                id="roomtype-quantity"
                initialValue={quantity}
                className={cx('form-control', {
                  'is-invalid': _.has(formState, 'errors.quantity'),
                  empty:        _.get(formState, 'values.quantity', '').length === 0,
                })}
                validate={this.validatePresence}
              />
              <label htmlFor="roomtype-quantity" className="required">
                {i18n.t('rooms.room_type.quantity')}
              </label>
              {
                _.has(formState, 'errors.quantity')
                && (
                  <div className="invalid-feedback">
                    {formState.errors.quantity}
                  </div>
                )
              }
            </div>
          </div>
        </div>

      </div>
    );
  }

}


export default (FieldsEditable);
