import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import i18n from 'i18next';
import { LinkButton } from '../controls/Buttons';
import RoomType from './RoomType';
import { ignorecaseSort } from '../../utils/helpers';
import { NO_ITEMS } from '../../constants/constants';
import {
  getRoomTypes as getRoomTypesAction,
  resetRoomTypeState as resetRoomTypeStateAction,
} from '../../actions/roomTypes';

class ViewRoomTypes extends React.Component {

  static propTypes = {
    match: PropTypes.instanceOf(Object).isRequired,
    list:  PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.object)
    ]).isRequired,
    getRoomTypes:       PropTypes.func.isRequired,
    resetRoomTypeState: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { getRoomTypes, resetRoomTypeState } = this.props;
    getRoomTypes();
    resetRoomTypeState();
  }

  render() {
    const {
      match,
      list,
    } = this.props;

    if (list === NO_ITEMS) {
      return (
        <Fragment>
          <div className="page-sub-title d-flex justify-content-between mb-3">
            <h2 className="m-0">{i18n.t('common.room_types')}</h2>
            <LinkButton
              to={`${match.url}/add`}
              className="btn-primary"
            >
              {i18n.t('rooms.add_new_room_type')}
            </LinkButton>
          </div>
          <div className="jumbotron">
            <h2 className="text-center display-3">
              {i18n.t('rooms.no_room_types')}
            </h2>
          </div>
        </Fragment>
      );
    }

    if (!list.length) {
      return (
        <div className="jumbotron">
          <h2 className="text-center display-3">
            {i18n.t('common.loading')}
          </h2>
        </div>
      );
    }

    return (
      <Fragment>
        <div className="page-sub-title d-flex justify-content-between mb-3">
          <h2 className="m-0">{i18n.t('common.room_types')}</h2>
          <LinkButton
            to={`${match.url}/add`}
            className="btn-primary"
          >
            {i18n.t('rooms.add_new_room_type')}
          </LinkButton>
        </div>
        <Scrollbars
          autoHeight
          autoHeightMax="100%"
          renderView={props => <div {...props} className="zr-scrollbar-container" />}
          renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
        >
          <div className="zr-roomtype-list">
            {ignorecaseSort(list, 'name').map(item => <RoomType key={item.id} item={item} />)}
          </div>
        </Scrollbars>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  list: state.room_types.list,
});

const mapDispatchToProps = dispatch => ({
  getRoomTypes: (response) => {
    dispatch(getRoomTypesAction(response));
  },
  resetRoomTypeState: () => { dispatch(resetRoomTypeStateAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewRoomTypes);
