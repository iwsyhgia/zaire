import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import i18n from 'i18next';
import { ContentGreyBlock } from '../../containers/Wrappers';
import Fields from './fields/Fields';
import Amenities from './amenities/Amenities';
import Images from './images/Images';
import Rooms from './rooms/Rooms';
import { PrimaryButton, SecondaryButton } from '../controls/Buttons';
import { RoomTypeUnit } from '../../templates/room_types';
import ConfirmationPopup from '../notifications/ConfirmationPopup';
import { deleteRoomType as deleteRoomTypeAction } from '../../actions/roomTypes';

class RoomType extends Component {

  static propTypes = {
    item:           PropTypes.instanceOf(Object).isRequired,
    locale:         PropTypes.string.isRequired,
    deleteRoomType: PropTypes.func.isRequired,
    match:          PropTypes.instanceOf(Object).isRequired,
    history:        PropTypes.instanceOf(Object).isRequired,
  };

  state = {
    deleteID:    '',
    isConfirmed: false,
  }

  deleteRoom = (id) => {
    this.setState({
      deleteID:    id,
      isConfirmed: true,
    });
  }

  submitDelete = () => {
    const { deleteRoomType } = this.props;
    const { deleteID } = this.state;

    deleteRoomType(deleteID);

    this.setState({
      deleteID:    '',
      isConfirmed: false,
    });
  };

  renderFieldsBlock = () => {
    const {
      item: {
        id,
        quantity,
        size,
        unit,
        ...rest
      },
      locale,
      match,
      history,
    } = this.props;

    const fields = {
      localizedName: (locale === 'ar' && !!rest.name_ar) ? rest.name_ar : rest.name,
      maxOccupancy:  rest.max_occupancy,
      size,
      unit,
      quantity,
    };

    return (
      <ContentGreyBlock className="flex-column h-100 p-3">
        <Fields fields={fields} />
        <div className="d-flex">
          <PrimaryButton
            onClick={() => history.push(`${match.url}/edit/${id}`)}
            className="mt-auto align-self-start"
          >
            {i18n.t('rooms.edit_type')}
          </PrimaryButton>
          <SecondaryButton className="mx-3" onClick={() => this.deleteRoom(id)}>
            {i18n.t('common.delete')}
          </SecondaryButton>
        </div>
      </ContentGreyBlock>
    );
  }

  render() {
    const {
      item: {
        amenities,
        images,
        rooms,
        name,
        name_ar: nameAr,
      },
      locale,
    } = this.props;

    const {
      isConfirmed,
    } = this.state;

    const localizedName = (locale === 'ar' && !!nameAr) ? nameAr : name;

    // TODO: remove confirmation popup from here
    return (
      <Fragment>
        <RoomTypeUnit
          data={{
            fields:    this.renderFieldsBlock(),
            amenities: <Amenities items={amenities} />,
            images:    <Images items={images} />,
            rooms:     <Rooms items={rooms} />,
          }}
        />
        <ConfirmationPopup
          show={isConfirmed}
          submitText={i18n.t('common.delete')}
          onSubmit={this.submitDelete}
          onHide={() => this.setState({ deleteID: '', isConfirmed: false })}
        >
          <p>{i18n.t('rooms.are_you_sure_you_want_to_delete_room_type', { name: localizedName })}</p>
        </ConfirmationPopup>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  locale: state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  deleteRoomType: (id) => { dispatch(deleteRoomTypeAction(id)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RoomType));
