import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form } from 'informed';
import i18n from 'i18next';
import { ContentGreyBlock } from '../Wrappers';
import FieldsEditable from './fields/FieldsEditable';
import AmenitiesEditable from './amenities/AmenitiesEditable';
import ImagesEditable from './images/ImagesEditable';
import RoomsEditable from './rooms/RoomsEditable';
import { SubmitButton, SecondaryButton } from '../controls/Buttons';
import { RoomTypeUnit } from '../../templates/room_types';
import ConfirmationPopup from '../notifications/ConfirmationPopup';
import Price from './price/Price';


import { deleteRoomType as deleteRoomTypeAction } from '../../actions/roomTypes';

class RoomTypeEditable extends Component {

  static propTypes = {
    isNew:          PropTypes.bool, // TODO: refactor !!
    onSubmit:       PropTypes.func,
    history:        PropTypes.instanceOf(Object).isRequired,
    // ----- store
    current:        PropTypes.instanceOf(Object).isRequired,
    locale:         PropTypes.string.isRequired,
    deleteRoomType: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isNew:    true,
    onSubmit: () => {},
  };

  state = {
    deleteID:    '',
    isConfirmed: false,
  }

  // Event Handlers
  onSubmit = (data) => {
    const {
      size,
      max_occupancy: maxOccupancy,
      quantity,
      kind,
      lower_bound: lowerBound,
      upper_bound: upperBound,
    } = data;
    const { current: { images, rooms, amenityIds }, onSubmit } = this.props;

    const roomType = {
      ...data,
      size:              +size,
      max_occupancy:     maxOccupancy.value ? +maxOccupancy.value : +maxOccupancy,
      quantity:          +quantity,
      amenity_ids:       amenityIds,
      rooms_attributes:  rooms,
      images_attributes: images,
      rate_attributes:   {
        kind,
        lower_bound: lowerBound,
        upper_bound: upperBound,
      },
    };

    onSubmit({ room_type: roomType });
  }

  onCancel = (e) => {
    e.preventDefault();
    const { history, locale } = this.props;
    const lang = locale === 'ar' ? '/ar' : '';
    history.push(`${lang}/room_types`);
  }

  deleteRoom = (id) => {
    this.setState({
      deleteID:    id,
      isConfirmed: true,
    });
  }

  submitDelete = () => {
    const { deleteRoomType } = this.props;
    const { deleteID } = this.state;

    deleteRoomType(deleteID);

    this.setState({
      deleteID:    '',
      isConfirmed: false,
    });
  };

  renderEditableFields(formState) {
    const {
      current,
      isNew,
    } = this.props;

    return (
      <ContentGreyBlock className="flex-column h-100 p-3">
        <FieldsEditable
          formState={formState}
          isNew={isNew}
          initValues={{ ...current }}
        />
      </ContentGreyBlock>
    );
  }

  render() {
    const {
      current: {
        name = '',
        name_ar: nameAr = '',
      },
      locale,
      isNew,
    } = this.props;
    const {
      isConfirmed,

    } = this.state; // TODO: use store instead!!

    const localizedName = (locale === 'ar' && !!nameAr) ? nameAr : name;

    // TODO: remove confirmation popup from here
    return (
      <Fragment>
        <Form name="form-room-type" className="form-room-type" onSubmit={this.onSubmit}>
          {({ formState }) => (
            <div>
              <RoomTypeUnit
                data={{
                  fields:    this.renderEditableFields(formState),
                  amenities: <AmenitiesEditable />,
                  images:    <ImagesEditable />,
                  rooms:     <RoomsEditable />,
                }}
              />
              <Price isNew={isNew} />
              <div className="page-sub-title d-flex justify-content-end mt-3">
                <SecondaryButton className="btn btn-secondary ml-3 mr-3" onClick={this.onCancel}>
                  {i18n.t('common.cancel')}
                </SecondaryButton>
                <SubmitButton>{i18n.t('common.save')}</SubmitButton>
              </div>
            </div>
          )}
        </Form>
        <ConfirmationPopup
          show={isConfirmed}
          submitText={i18n.t('common.delete')}
          onSubmit={this.submitDelete}
          onHide={() => this.setState({ deleteID: '', isConfirmed: false })}
        >
          <p>{i18n.t('rooms.are_you_sure_you_want_to_delete_room_type', { name: localizedName })}</p>
        </ConfirmationPopup>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  locale:  state.common.locale,
  current: state.room_types.current,
});

const mapDispatchToProps = dispatch => ({
  deleteRoomType: (id) => { dispatch(deleteRoomTypeAction(id)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RoomTypeEditable));
