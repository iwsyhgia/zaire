import React from 'react';
import _ from 'lodash';
import { Scrollbars } from 'react-custom-scrollbars';
import { RoomsBlock } from '../../../templates/room_types';

const Rooms = ({ items = [] }) => (
  <RoomsBlock length={items.length} className="zr-room-type-rooms">
    <div className="room-number-list units-list prevent-hover d-flex flex-wrap flex-grow-1">
      <Scrollbars
        style={{ height: 175 }}
        renderView={props => <div {...props} className="zr-scrollbar-container" />}
        renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
      >
        {
          _.sortBy(items, 'number').map(item => (
            <div className="room-number-badge units-list-item" key={item.id}>
              <span>{item.number}</span>
            </div>
          ))
        }
      </Scrollbars>
    </div>
  </RoomsBlock>
);

export default Rooms;
