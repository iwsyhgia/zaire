import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
// eslint-disable-next-line
import { connect } from 'react-redux';
import { Overlay } from 'react-bootstrap';
import validator from 'validator';
import i18n from 'i18next';
import ValidationTooltip from '../../notifications/ValidationTooltip';
import CretableField from '../../controls/CreatableField';
import forwardRefWrapper from '../../../containers/forwardRefWrapper';
import { updateRoom as updateRoomAction } from '../../../actions/roomTypes';

const fieldRef = React.createRef();
class RoomField extends Component {

  static propTypes = {
    rooms:      PropTypes.arrayOf(Object).isRequired,
    onSubmit:   PropTypes.func.isRequired,
    updateRoom: PropTypes.func.isRequired,
  };

  constructor(...args) {
    super(...args);
    this.attachRef = target => this.setState({ target });
  }

  state = {
    showValidMsg: false,
    message:      '',
    editableRoom: null,
  }

  showWarning = (message) => {
    this.setState({
      showValidMsg: true,
      message,
    });

    setTimeout(() => {
      this.setState({ showValidMsg: false });
    }, 1500);
  }

  focus = (item = {}, isEdit = true) => {
    fieldRef.current.focus(item.number, isEdit);
    this.setState({
      editableRoom: item,
    });
  };

  validRoomNumber = (value = '') => {
    const arr = '0123456789';
    return (value.length === 0 || (value.length > 0 && arr.indexOf(value[value.length - 1]) > -1));
  }

  getValidRoom = (number, isEdit = true) => {

    if (number === '') {
      this.showWarning(i18n.t('rooms.errors.room_number_presence'));
      fieldRef.current.focus(number, isEdit);
      return null;
    }

    if (!validator.isInt(number, { min: 1, allow_leading_zeroes: true })) {
      this.showWarning(i18n.t('rooms.invalid_room_number'));
      fieldRef.current.focus(number, isEdit);
      return null;
    }

    const { editableRoom } = this.state;
    const { rooms } = this.props;
    const room = rooms.find(item => (Number(item.number) === Number(number)));

    if (room) {

      if (room._destroy) { // restore deleted room
        const { _destroy, ...rest } = room;
        return { ...rest };
      }

      this.showWarning(i18n.t('rooms.room_number_is_duplicated'));
      fieldRef.current.focus(number, isEdit);
      return null;
    }

    // update attached room
    if (isEdit && editableRoom && editableRoom.id) {
      return { ...editableRoom, number: Number(number) };
    }

    // create a new room == restore a deleted room
    if (!isEdit && room && room._destroy) {
      const { _destroy, ...rest } = room;
      return { ...rest };
    }

    // add new room
    return { number: Number(number) };
  }

  addRoomNumber = (newValue) => {

    const room = this.getValidRoom(newValue, false);

    if (room) {
      const { rooms, onSubmit } = this.props;
      const modifiedRooms = [...rooms];

      if (_.has(room, 'id')) { // restore deleted room
        const oldIndex = rooms.findIndex(item => item.id === room.id);
        modifiedRooms[oldIndex] = room;
      } else {
        modifiedRooms.push(room);
      }

      onSubmit([...modifiedRooms]);
    }
  }

  updateRoomNumber = (oldRoom, newRoomNumber) => {

    const room = this.getValidRoom(newRoomNumber, true);

    if (room) {
      const { rooms, onSubmit, updateRoom } = this.props;

      if (room.id) {
        updateRoom(room.id, room);
      } else {
        const { editableRoom: { number } = {} } = this.state;
        const index = rooms.findIndex(item => Number(item.number) === Number(number));
        const modifiedRooms = [...rooms];
        modifiedRooms[index] = room;

        onSubmit(modifiedRooms);
      }
    }
  }

  render() {
    const {
      showValidMsg,
      target,
      message,
    } = this.state;

    return (
      <div className="room-number-tools" ref={this.attachRef}>
        <CretableField
          placeholder={i18n.t('rooms.enter_room_number')}
          ref={fieldRef}
          onCreateItem={this.addRoomNumber}
          onUpdate={this.updateRoomNumber}
          validValue={this.validRoomNumber}
          type="number"
        />
        <Overlay
          show={showValidMsg}
          onHide={() => this.setState({ showValidMsg: false })}
          placement="bottom"
          container={this}
          target={target}
        >
          {({ placement, scheduleUpdate, arrowProps, ...props }) => (
            <div
              {...props}
              style={{
                ...props.style,
                width: '100%',
              }}
            >
              <ValidationTooltip>{message}</ValidationTooltip>
            </div>
          )}
        </Overlay>
      </div>
    );
  }

}


const mapDispatchToProps = dispatch => ({
  updateRoom: (id, data) => { dispatch(updateRoomAction(id, data)); },
});

export default connect(null, mapDispatchToProps, null, { withRef: true })(forwardRefWrapper(RoomField));
