import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Scrollbars } from 'react-custom-scrollbars';
import RoomField from './RoomField';
import { RoomsBlock } from '../../../templates/room_types';
import { RoomControls } from '../../controls/HoverControls';
import { updateRooms as updateRoomsAction, deleteRoom as deleteRoomAction } from '../../../actions/roomTypes';


class RoomsEditable extends React.Component {

  static propTypes = {
    rooms:       PropTypes.arrayOf(Object).isRequired,
    updateRooms: PropTypes.func.isRequired,
  };

  // Event handlers
  // Room should be removed from room type on 'Save' click
  removeRoomNumber = (number) => {
    const { rooms, updateRooms } = this.props;

    const index = rooms.findIndex(item => Number(item.number) === Number(number));

    if (index !== -1) {
      const modifiedRooms = [...rooms];
      const room = modifiedRooms[index];

      if (_.has(room, 'id')) { // a room is attached to the room type
        modifiedRooms[index]._destroy = true;
      } else { // a newly created room
        modifiedRooms.splice(index, 1);
      }

      updateRooms([...modifiedRooms]);
    }
  }

  editRoomNumber = (item) => {
    this.roomField.focus(item);
  }

  setFieldRef = (connectedComponent) => {
    this.roomField = (connectedComponent && connectedComponent.getWrappedInstance()) || null;
  }

  render() {
    const { rooms, updateRooms } = this.props;

    const actualRooms = rooms.filter(item => !item._destroy);

    return (
      <RoomsBlock length={actualRooms.length}>
        <div className="room-number-list units-list d-flex flex-wrap flex-grow-1">
          <Scrollbars
            style={{ height: 175 }}
            renderView={props => <div {...props} className="zr-scrollbar-container" />}
            renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
          >
            {
              _.sortBy(actualRooms, 'number')
                .map(item => (
                  <div className="room-number-badge units-list-item" key={item.number}>
                    <span>{item.number}</span>
                    <RoomControls
                      onRemove={() => this.removeRoomNumber(item.number)}
                      onEdit={() => this.editRoomNumber(item)}
                    />
                  </div>
                ))
            }
          </Scrollbars>
        </div>
        <RoomField
          ref={(connectedComponent) => { this.setFieldRef(connectedComponent); }}
          rooms={rooms}
          onSubmit={(data) => { updateRooms(data); }}
        />
      </RoomsBlock>
    );
  }

}

const mapStateToProps = state => ({
  rooms: state.room_types.current.rooms,
});
const mapDispatchToProps = dispatch => ({
  updateRooms: (data) => { dispatch(updateRoomsAction(data)); },
  deleteRoom:  (id) => { dispatch(deleteRoomAction(id)); },
});

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(RoomsEditable);
