import React from 'react';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import { AmenitiesBlock } from '../../../templates/room_types';

const Amenities = ({ items, locale }) => (
  <AmenitiesBlock length={items.length} className="zr-room-type-amenities">
    <ul className="view-amenities-list list-unstyled units-list px-3">
      <Scrollbars
        style={{ height: 175 }}
        renderView={props => <div {...props} className="zr-scrollbar-container" />}
        renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
      >
        {
          items.map(item => (
            <li key={item.id}>
              {(locale === 'ar' && !!item.name_ar) ? item.name_ar : item.name}
            </li>
          ))
        }
      </Scrollbars>
    </ul>
  </AmenitiesBlock>
);

const mapStateToProps = state => ({
  locale: state.common.locale,
});

export default connect(mapStateToProps, null)(Amenities);
