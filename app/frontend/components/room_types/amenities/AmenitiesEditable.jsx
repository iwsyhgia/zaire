import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import _ from 'lodash';
import i18n from 'i18next';
import ConfirmationPopup from '../../notifications/ConfirmationPopup';
import NewCustomAmenity from './NewCustomAmenity';
import { AmenityControls } from '../../controls/HoverControls';
import { AmenitiesBlock, AmenityCategoryTitle, AmenityItem } from '../../../templates/room_types';
import { rlmFix } from '../../../utils/helpers';
import {
  getAmenities as getAmenitiesAction,
  deleteAmenity as deleteAmenityAction,
} from '../../../actions/amenities';
import { updateAmenities as updateAmenitiesAction } from '../../../actions/roomTypes';

class AmenitiesEditable extends React.Component {

  static propTypes = {
    amenities:  PropTypes.arrayOf(PropTypes.object).isRequired,
    amenityIds: PropTypes.arrayOf(PropTypes.number),
    roomTypeID: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    getAmenities:    PropTypes.func.isRequired,
    updateAmenities: PropTypes.func.isRequired,
    deleteAmenity:   PropTypes.func.isRequired,
    locale:          PropTypes.string.isRequired,
  };

  static defaultProps ={
    roomTypeID: null,
    amenityIds: [],
  }

  state = {
    isConfirmed:      false,
    submitID:         '',
    closedCategories: [],
  }

  componentDidMount() {
    const { getAmenities } = this.props;
    getAmenities();
  }

  onCheckCategory = (event, key) => {
    const { closedCategories } = this.state;
    const isChecked = event.target.checked;

    if (isChecked) {
      closedCategories.push(key);
    } else {
      _.remove(closedCategories, item => item === key);
    }

    this.setState({
      closedCategories,
    });
  }

  onCheckAmenity = (event, id) => {
    const { amenityIds, updateAmenities } = this.props;
    const isChecked = event.target.checked;
    const checkedAmenities = [...amenityIds];

    if (isChecked) {
      checkedAmenities.push(id);
    } else {
      _.remove(checkedAmenities, item => item === id);
    }

    updateAmenities(checkedAmenities);
  }

  removeAmenity = (id) => {
    const { amenities, locale } = this.props;
    const amenity = amenities.find(item => item.id === id) || { name: '' };
    this.setState({
      isConfirmed:        true,
      submitID:           id,
      removedAmenityName: (locale === 'ar' && !!amenity.name_ar) ? amenity.name_ar : amenity.name,
    });
  }

  submitDelete = () => {
    const { roomTypeID, amenityIds, deleteAmenity } = this.props;
    const { submitID } = this.state;
    this.setState({
      isConfirmed: false,
    });

    const data = (!!roomTypeID && !amenityIds.includes(submitID))
      ? { room_type_id: roomTypeID } : {};

    deleteAmenity(submitID, data);
  }

  getAmenitiesList = () => {
    const { amenities, amenityIds = [], locale } = this.props;
    const { closedCategories } = this.state;

    const prosessedAmenities = amenities
      .map(item => ({
        ...item,
        checked:           amenityIds.includes(item.id),
        localizedCategory: locale === 'ar' && !!item.category_ar ? item.category_ar : item.category,
        localizedName:     locale === 'ar' && !!item.name_ar ? item.name_ar : item.name,
      }));

    const categories = Object.entries(_.groupBy(_.sortBy(prosessedAmenities, 'localizedCategory'),
      item => (item.localizedCategory)));

    const rlm = rlmFix(locale);

    return (
      <Scrollbars
        style={{ height: '90%' }}
        renderView={props => <div {...props} className="zr-scrollbar-container" />}
        renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
      >
        {
          categories.map(([key, value]) => (
            <Fragment key={key}>
              <AmenityCategoryTitle
                name={key}
                itemCount={value.length}
                isOpen={closedCategories.includes(key)}
                onClick={this.onCheckCategory}
                rlmFix={rlm}
              />
              {
                closedCategories.includes(key) && _.sortBy(value, 'localizedName').map(item => (
                  <AmenityItem
                    key={item.id}
                    name={item.localizedName}
                    id={item.id}
                    isChecked={!!item.checked}
                    onClick={this.onCheckAmenity}
                    controls={
                      item.type === 'custom'
                        ? <AmenityControls onRemove={() => { this.removeAmenity(item.id); }} />
                        : null
                    }
                  />
                ))
              }
            </Fragment>
          ))
        }
      </Scrollbars>
    );
  }

  render() {
    const { amenities } = this.props;
    const {
      isConfirmed,
      removedAmenityName,
    } = this.state;

    return (
      <Fragment>
        <AmenitiesBlock length={amenities.length}>
          <ul className="amenities-list units-list list-unstyled flex-grow-1">
            {this.getAmenitiesList()}
          </ul>
          <NewCustomAmenity />
        </AmenitiesBlock>
        <ConfirmationPopup
          show={isConfirmed}
          submitText={i18n.t('common.delete')}
          onSubmit={this.submitDelete}
          onHide={() => this.setState({ isConfirmed: false })}
        >
          <p>{i18n.t('rooms.are_you_sure_you_want_to_delete_amenity', { name: removedAmenityName })}</p>
        </ConfirmationPopup>
      </Fragment>
    );
  }

}


const mapStateToProps = state => ({
  amenities:  state.amenities.list,
  locale:     state.common.locale,
  amenityIds: state.room_types.current.amenityIds,
  roomTypeID: state.room_types.current.id,
  loading:    state.amenities.loading,
  error:      state.amenities.error,
});

const mapDispatchToProps = dispatch => ({
  getAmenities:    () => { dispatch(getAmenitiesAction()); },
  deleteAmenity:   (id, data) => { dispatch(deleteAmenityAction(id, data)); },
  updateAmenities: (data) => { dispatch(updateAmenitiesAction(data)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(AmenitiesEditable);
