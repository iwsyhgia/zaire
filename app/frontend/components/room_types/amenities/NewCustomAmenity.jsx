import React from 'react';
import { connect } from 'react-redux';
import { Overlay } from 'react-bootstrap';
import i18n from 'i18next';
import ValidationTooltip from '../../notifications/ValidationTooltip';
import CretableField from '../../controls/CreatableField';
import { addAmenity as addAmenityAction } from '../../../actions/amenities';

class NewCustomAmenity extends React.Component {

  constructor(...args) {
    super(...args);
    this.attachRef = target => this.setState({ target });
  }

  state = {
    showValidMsg: false,
    message:      '',
  }

  addAmenity = (customAmenity, customAmenityAr = '') => {
    const { addAmenity } = this.props;
    if (customAmenity === '') {
      this.setState({
        showValidMsg: true,
        message:      i18n.t('amenities.errors.cannot_be_blank'),
      });

      setTimeout(() => {
        this.setState({ showValidMsg: false });
      }, 1500);

      return;
    }

    addAmenity({ amenity: { name: customAmenity, name_ar: customAmenityAr } });
  }

  render() {
    const {
      showValidMsg,
      target,
      message,
    } = this.state;

    const isBiField = true;

    return (
      <div className="zr-custom-amenity" ref={this.attachRef}>
        <CretableField
          placeholder={i18n.t('rooms.make_custom_amenity')}
          onCreateItem={this.addAmenity}
          isBiField={isBiField}
        />
        <Overlay
          show={showValidMsg}
          onHide={() => this.setState({ showValidMsg: false })}
          placement="bottom"
          container={this}
          target={target}
        >
          {({ placement, scheduleUpdate, arrowProps, ...props }) => (
            <div
              {...props}
              style={{
                ...props.style,
                width: '100%',
              }}
            >
              <ValidationTooltip>{message}</ValidationTooltip>
            </div>
          )}
        </Overlay>
      </div>
    );
  }

}

const mapDispatchToProps = dispatch => ({
  addAmenity: (data) => { dispatch(addAmenityAction(data)); },
});

export default connect(null, mapDispatchToProps)(NewCustomAmenity);
