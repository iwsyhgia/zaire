import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export default ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    <Component {...props} />
    // localStorage.getItem('jwt')
    //     ? <Component {...props} />
    //     : <Redirect to={{ pathname: '/sign_in', state: { from: props.location } }} />
  )} />
)

