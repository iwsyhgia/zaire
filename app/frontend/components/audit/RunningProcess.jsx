import React from 'react';
import i18n from 'i18next';
import Spinner from './Spinner';

const RunningProcess = () => (
  <div className="zr-night-audit-content">
    <Spinner />
    <p className="description">
      { i18n.t('audit.night_audit_description') }
    </p>
  </div>
);

export default RunningProcess;
