import React from 'react';
import i18n from 'i18next';
import moment from 'moment';
import IconAudit from '../icons/IconAudit';


const Logo = (props) => {
  const { auditDate } = props;

  const formattedAuditDate = moment(auditDate).format('DD MMM YYYY');

  return (
    <div className="zr-night-audit-logo">
      <div className="logo">
        <IconAudit width="74" height="74" />
      </div>
      <h2 className="title">
        {i18n.t('audit.night_audit_title')}
      </h2>
      <p className="subtitle">
        {i18n.t('audit.night_audit_subtitle')}
      </p>
      <p className="date">
        {i18n.t('audit.night_audit_date', { date: `${formattedAuditDate}` })}
      </p>
    </div>
  );
};

export default Logo;
