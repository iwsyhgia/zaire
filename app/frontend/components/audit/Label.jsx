import React from 'react';
import i18n from 'i18next';

const Label = props => (
  <div className="zr-night-audit-label noselect" {...props}>
    <span className="zr-label-vertical-text">
      {i18n.t('audit.night_audit')}
    </span>
  </div>
);

export default Label;
