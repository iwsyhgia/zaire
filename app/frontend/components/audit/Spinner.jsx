import React from 'react';

const Spinner = () => (
  <div className="zr-night-audit-spinner-box">
    <div className="zr-night-audit-spinner" />
  </div>
);

export default Spinner;
