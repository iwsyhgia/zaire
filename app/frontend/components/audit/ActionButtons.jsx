import React from 'react';
import i18n from 'i18next';


const ActionButtons = (props) => {
  const {
    onSubmit,
    onCancel,
  } = props;

  return (
    <div className="zr-night-audit-action-group">
      <button
        className="btn btn-primary"
        type="button"
        onClick={() => onSubmit()}
      >
        {i18n.t('common.perform_now')}
      </button>
      <button
        className="btn btn-secondary"
        type="button"
        onClick={() => onCancel()}
      >
        {i18n.t('common.postpone')}
      </button>
    </div>
  );
};

export default ActionButtons;
