import React from 'react';
import i18n from 'i18next';

const ResultMessage = (props) => {

  const {
    onSubmit,
    message,
  } = props;

  return (
    <div className="zr-night-audit-content zr-night-audit-result">
      <p className="description">
        {message}
      </p>
      <div>
        <button
          className="btn btn-primary"
          type="button"
          onClick={() => onSubmit()}
        >
          {i18n.t('common.close')}
        </button>
      </div>
    </div>
  );
};

export default ResultMessage;
