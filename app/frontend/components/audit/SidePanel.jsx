import React from 'react';
import { connect } from 'react-redux';
import i18n from 'i18next';
import Logo from './Logo';
import RunningProcess from './RunningProcess';
import ActionButtons from './ActionButtons';
import ResultMessage from './ResultMessage';
import {
  runAudit as runAuditAction,
  postponeAudit as postponeAuditAction,
  auditPostRun as auditPostRunAction,
} from '../../actions/audit';


const SidePanel = (props) => {

  const {
    isRunning,
    isPerformed,
    runAudit,
    postponeAudit,
    auditPostRun,
    confirmationID,
    message,
    auditDate,
  } = props;

  const actionGroup = (isPerformed)
    ? (
      <ResultMessage
        onSubmit={() => auditPostRun()}
        message={i18n.t(`audit.${message}`)}
      />
    )
    : (
      <ActionButtons
        onSubmit={() => runAudit(confirmationID)}
        onCancel={postponeAudit}
      />
    );

  return (
    <div className="zr-night-audit-sidepanel">
      <Logo auditDate={auditDate} />
      {
        isRunning
          ? <RunningProcess />
          : actionGroup
      }
    </div>
  );
};

const mapStateToProps = state => ({
  isRunning:      state.audit.isRunning,
  isPerformed:    state.audit.isPerformed,
  confirmationID: state.audit.confirmationID,
  message:        state.audit.message,
  auditDate:      state.audit.auditDate,
});

const mapDispatchToProps = dispatch => ({
  runAudit:      (id) => { dispatch(runAuditAction(id)); },
  postponeAudit: () => { dispatch(postponeAuditAction()); },
  auditPostRun:  () => { dispatch(auditPostRunAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(SidePanel);
