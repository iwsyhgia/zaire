import React from 'react';
import ReactDOM from 'react-dom';
import { NavLink, Link, withRouter } from 'react-router-dom';
import cx from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import PathToRegexp from 'path-to-regexp';
import i18n from 'i18next';
import {
  IconReservations,
  IconInventory,
  IconRates,
  IconReport,
  IconGuests,
  IconReviews,
  IconHotelSettings,
  IconRooms,
  IconUsers,
  IconArrowDown,
} from '../../components/icons/MenuIcons';
// import './AppSidebar.scss';


class AppSidebar extends React.Component {

  componentDidMount() {
    const node = ReactDOM.findDOMNode(this.hotelSettingsCollapsed);
    node.classList.add('collapsed');
  }

  toggleHotelSettings = () => {
    const node = ReactDOM.findDOMNode(this.hotelSettingsDropdown);
    node.classList.remove('show');
    setTimeout(() => { node.previousSibling.classList.add('collapsed'); }, 1); // TODO: workaround to return bootstrap class
  }

  render() {
    const { location, locale } = this.props;
    const isHotelSettingsSub = location.pathname.indexOf('hotel_settings') > -1;

    const localeRegex = PathToRegexp('/(ar|en)?/:bar*');
    const parsedPath = localeRegex.exec(location.pathname);
    const prefix = parsedPath[1] ? `/${parsedPath[1]}` : '';


    return (
      <aside className="zr-sidebar bg-dark">
        <header className="zr-sidebar-header">
          <Link to={`/${locale}`} className="zr-sidebar-logo">
            <span className="zr-sidebar-logo-short">Z</span>
            <span className="zr-sidebar-logo-rest">AIRE</span>
          </Link>
        </header>
        <Scrollbars
          height="100%"
          autoHeightMax="100%"
          autoHide
          renderView={props => <div {...props} className="zr-scrollbar-container" />}
          renderTrackVertical={props => <div {...props} className="zr-scrollbar-track-vertical" />}
        >
          <section className="zr-sidebar-content">
            <nav className="zr-sidebar-nav">
              <ul className="zr-nav">
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/reservations`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconReservations className="zr-nav-item-icon icon-reservations" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.reservations')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/inventory`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconInventory className="zr-nav-item-icon icon-inventory" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.inventory')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/rates`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconRates className="zr-nav-item-icon icon-rates" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.rates')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/report`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconReport className="zr-nav-item-icon icon-report" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.booking_report')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/guests`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconGuests className="zr-nav-item-icon icon-guests" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.guests')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/reviews`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconReviews className="zr-nav-item-icon icon-reviews" width="20" height="20" />
                    <span className="zr-nav-item-name">
                      {i18n.t('common.guests_reviews')}
                    </span>
                  </NavLink>
                </li>
                <li className="zr-nav-item dropdown">
                  <a
                    className={cx('zr-nav-item-link dropdown-toggle', { active: isHotelSettingsSub })}
                    href="#hotel-settings-dropdown"
                    role="button"
                    data-toggle="collapse"
                    aria-expanded="false"
                    ref={(ref) => { this.hotelSettingsCollapsed = ref; }}
                  >
                    <IconHotelSettings className="zr-nav-item-icon icon-hotel-settings" width="20" height="20" />
                    <span className="zr-nav-item-name">{i18n.t('common.hotel_settings')}</span>
                    <IconArrowDown className="zr-nav-item-arrow" width="11" height="7" />
                  </a>
                  <ul
                    className={cx('collapse collapsed-dropdown zr-subnav')}
                    id="hotel-settings-dropdown"
                    ref={(ref) => { this.hotelSettingsDropdown = ref; }}
                  >
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/offline`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.offline_storage')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/images`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">
                          {i18n.t('common.hotel_images')}
                        </span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/sellable_items`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.sellable_items')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/checkin`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.check_in_check_out')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/discount`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.max_discount')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/night_audit`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.night_audit')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/loyalty`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('common.loyalty')}</span>
                      </NavLink>
                    </li>
                    <li className="zr-subnav-item">
                      <NavLink
                        to={`${prefix}/hotel_settings/name_logo`}
                        activeClassName="active"
                        className="zr-subnav-item-link"
                      >
                        <span className="zr-subnav-item-name">{i18n.t('hotel.name_and_logo_subtitle')}</span>
                      </NavLink>
                    </li>
                  </ul>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/room_types`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                    isActive={() => (window.location.pathname.indexOf('room_types') > -1)}
                  >
                    <IconRooms className="zr-nav-item-icon icon-rooms" width="20" height="20" />
                    <span className="zr-nav-item-name">{i18n.t('common.rooms')}</span>
                  </NavLink>
                </li>
                <li className="zr-nav-item">
                  <NavLink
                    onClick={this.toggleHotelSettings}
                    to={`${prefix}/users`}
                    activeClassName="active"
                    className="zr-nav-item-link"
                  >
                    <IconUsers className="zr-nav-item-icon icon-users" width="20" height="20" />
                    <span className="zr-nav-item-name">{i18n.t('common.users')}</span>
                  </NavLink>
                </li>
              </ul>
            </nav>
            <ul className="zr-sidebar-pointers">
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-checked_in" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.checked_in')}
                </span>
              </li>
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-checked_out" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.checked_out')}
                </span>
              </li>
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-confirmed" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.confirmed')}
                </span>
              </li>
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-unconfirmed" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.unconfirmed')}
                </span>
              </li>
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-no_show" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.no_show')}
                </span>
              </li>
              <li className="zr-sidebar-pointer">
                <span className="zr-sidebar-pointer-status status-dnr" />
                <span className="zr-sidebar-pointer-name">
                  {i18n.t('common.dnr')}
                </span>
              </li>
            </ul>
          </section>
        </Scrollbars>
      </aside>
    );
  }

}

export default withRouter(AppSidebar);
