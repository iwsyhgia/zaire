import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import _ from 'lodash';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Clock from 'react-live-clock';
import i18n from 'i18next';
import moment from 'moment';
import { IconEnglish, IconArabic } from '../../components/icons/FlagIcons';
import IconNotification from '../../components/icons/IconNotification';
import IconArrowDown from '../../components/icons/IconArrowDown';
import IconLogout from '../../components/icons/IconLogout';
import IconBurger from '../../components/icons/IconBurger';
import IconPerson from '../../components/icons/IconPerson';
import DefaultAvatar from '../../components/icons/DefaultAvatar';
import { getProfileAction } from '../../actions/profiles';
import { unregisterServiceWorker } from '../../service-worker/serviceWorkerCompanion';

class AppHeader extends React.Component {

  static propTypes = {
    isOffline:  PropTypes.bool.isRequired,
    locale:     PropTypes.string.isRequired,
    setLocale:  PropTypes.func.isRequired,
    loadCss:    PropTypes.func.isRequired,
    location:   PropTypes.instanceOf(Object).isRequired,
    history:    PropTypes.instanceOf(Object).isRequired,
    profile:    PropTypes.instanceOf(Object).isRequired,
    getProfile: PropTypes.func.isRequired,
  };


  componentDidMount() {
    const { getProfile, profile } = this.props;
    if (!profile.model.avatar) getProfile();
    axios.defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-TOKEN':     document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    };
  }

  onPersonalAccountClick = () => {
    const { history, locale } = this.props;
    const lang = locale === 'ar' ? '/ar' : '';
    history.push(`${lang}/account`);
  }

  onSignOut = () => {
    const { locale } = this.props;
    const url = locale ? `/${locale}/users/sign_in` : '/';
    axios.delete('/users/sign_out')
      .then(() => {
        document.location.replace(url);
        unregisterServiceWorker();
      });
  }

  changeLocale = (e, locale) => {
    e && e.preventDefault();

    const { setLocale, location, history, loadCss } = this.props;
    setLocale(locale);
    i18n.changeLanguage(locale);

    if (locale === 'ar') {
      moment.locale('ar-en');
      loadCss('rtl');
    } else {
      moment.locale('en');
      loadCss('ltr');
    }

    const path = location.pathname;

    window.localStorage.setItem('locale', locale);

    // TODO REFACTOR
    const en = path.indexOf('en');
    const ar = path.indexOf('ar');
    const isLocalized = en > -1 || ar > -1;

    if (!isLocalized) {
      history.replace(`/${locale}${path}`);
    } else {
      if (ar > -1 && locale === 'en') {
        history.replace(path.replace(/ar/gi, 'en'));
      }
      if (en > -1 && locale === 'ar') {
        history.replace(path.replace(/en/gi, 'ar'));
      }
    }
  }

  render() {
    const { locale, profile, isOffline } = this.props;
    const isTickingTime = true;
    const timeFormat = locale === 'ar' ? 'D MMMM YYYY' : 'MMMM D, YYYY';

    const path = _.has(profile, 'model.avatar.path') && profile.model.avatar.path;
    const firstName = (_.has(profile, 'model.first_name') && profile.model.first_name) || 'John';
    const lastName = (_.has(profile, 'model.last_name') && profile.model.last_name) || 'Doe';

    return (
      <header className="zr-header">
        <button
          type="button"
          className="zr-header-toggle"
          onClick={() => document.querySelector('.zr-sidebar').classList.toggle('opened')}
        >
          <IconBurger className="icon icon-burger" width="S20" height="14" />
        </button>
        <div className="zr-header-date">
          <p>
            {
              `${moment().format('DD MMM YYYY')}`
            }
          </p>
          <Clock format="k:mm" timezone="Asia/Riyadh" ticking={isTickingTime} />
        </div>
        <nav className="zr-header-navbar">
          <ul className="zr-header-navbar-list">
            <li className="zr-header-navbar-item zr-header-navbar-item-user dropdown">
              <a className="zr-header-navbar-link zr-header-navbar-link-user dropdown-toggle" href="#" id="account-dropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                <span>{ `${firstName} ${lastName}` }</span>
                <IconArrowDown className="icon icon-arrow-down" width="11" height="7" />
                {
                  path ? (
                    <img src={path} className="zr-header-navbar-avatar" width="38px" alt="avatar" />
                  ) : (
                    <span className="zr-header-navbar-avatar">
                      <DefaultAvatar width="30" height="30" className="align-avatar" />
                    </span>
                  )
                }
              </a>
              <div className="dropdown-menu" aria-labelledby="account-dropdown">
                <Link to="#page" onClick={this.onPersonalAccountClick} className="dropdown-item">
                  <IconPerson className="icon icon-person" width="15" height="16" />
                  <span>{ i18n.t('profile.page_title') }</span>
                </Link>
                <Link
                  to="#signout"
                  onClick={this.onSignOut}
                  className={`dropdown-item ${isOffline ? 'disabled' : ''}`}
                  disabled={isOffline}
                >
                  <IconLogout className="icon icon-logout" width="15" height="15" />
                  <span>{i18n.t('devise.sign_out')}</span>
                </Link>
              </div>
            </li>
            <li className="zr-header-navbar-item">
              <a className="zr-header-navbar-link zr-header-navbar-link-notification new" href="#">
                <IconNotification className="icon icon-notification" width="16" height="16" />
              </a>
            </li>
            <li className="zr-header-navbar-item zr-header-navbar-item-lang dropdown">
              <a className="zr-header-navbar-link zr-header-navbar-link-lang dropdown-toggle" href="#" id="language-dropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                {
                  (locale === 'en')
                    ? (
                      <span className="zr-header-navbar-link-inner">
                        <IconArabic className="icon icon-flag-ar" width="20" height="14" />
                        <span className="arabic-lang">عربي</span>
                      </span>
                    )
                    : (
                      <span className="zr-header-navbar-link-inner">
                        <IconEnglish className="icon icon-flag-en" width="20" height="14" />
                        <span className="local-name">English</span>
                      </span>
                    )
                }
                <IconArrowDown className="icon icon-arrow-down" width="11" height="7" />
              </a>
              <div className="language-menu dropdown-menu" aria-labelledby="language-dropdown">
                <a className="dropdown-item" href="#" onClick={(e) => this.changeLocale(e, 'ar')}>
                  <IconArabic className="icon icon-flag-ar" width="20" height="14" />
                  <span className="arabic-lang">عربي</span>
                </a>
                <a className="dropdown-item" href="#" onClick={(e) => this.changeLocale(e, 'en')}>
                  <IconEnglish className="icon icon-flag-en" width="20" height="14" />
                  <span className="local-name">English</span>
                </a>
              </div>
            </li>
            <li className="zr-header-navbar-item zr-header-navbar-item-offline-message">
              {
                isOffline && (
                  <span> You are offline. </span>
                )
              }
            </li>
          </ul>
        </nav>
      </header>
    );
  }

}


const mapStateToProps = state => ({
  profile: state.profile,
  locale:  state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  getProfile: () => dispatch(getProfileAction()),
});

export default (withRouter(connect(mapStateToProps, mapDispatchToProps)(AppHeader)));
