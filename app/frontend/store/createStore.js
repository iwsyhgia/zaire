import { applyMiddleware, compose, createStore as createReduxStore } from 'redux';
import thunk       from 'redux-thunk';
import logger from 'redux-logger';
import Ajax        from '../middleware/ajax';
import rootReducer from '../reducers/index';

const createStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = [thunk, Ajax, logger];

  // ======================================================
  // Store Enhancers
  // ======================================================
  let composeEnhancers = compose;

  if (__DEV__) {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createReduxStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(...middleware)),
  );

  return store;
};

export default createStore;
