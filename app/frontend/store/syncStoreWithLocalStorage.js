import { updateServiceWorkerStatus } from '../service-worker/serviceWorkerCompanion';

export const loadState = (key) => {
  try {
    const serializedState = localStorage.getItem(key);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (key, state) => {
  try {
    const serializedState = JSON.stringify(state);

    // caches.open(key).then((cache) => {
    //   cache.put(key, new Response(serializedState, { headers: { 'Content-Type': 'text/json' } }));
    // });

    localStorage.setItem(key, serializedState);
    if (key === 'offline-storage') {
      updateServiceWorkerStatus();
    }
  } catch (err) {
    // Ignore write errors.
  }
};
