import ACCOUNT_ACTION_HANDLERS from './handlers/profiles';

const ACTION_HANDLERS = { ...ACCOUNT_ACTION_HANDLERS };
const initialState = {
  model: {},
  form:  {
    avatar:        { expanded: false },
    first_name:    { expanded: false },
    last_name:     { expanded: false },
    phone_number:  { expanded: false },
    notifications: { expanded: false },
    password:      { old: undefined, new: undefined },
  },
  error:    null,
  cropShow: false,
  loading:  false,
  success:  false,
};

const accounts = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default accounts;
