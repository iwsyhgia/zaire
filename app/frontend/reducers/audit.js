import AUDIT_ACTION_HANDLERS from './handlers/audit';

const ACTION_HANDLERS = { ...AUDIT_ACTION_HANDLERS };
const initialState = {
  isReadyToStart: false,
  isRunning:      false,
  isPostponed:    false,
  isPerformed:    false,
  confirmationID: null,
  message:        null,
  isOffline:      false,
  lastCompleted:  {},
};

const audit = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default audit;
