import MESSAGE_ACTION_HANDLERS from './handlers/messageLogger';

const ACTION_HANDLERS = { ...MESSAGE_ACTION_HANDLERS };
const initialState = {
  activeError: null,
  list:        [],
  errors:      [],
};

const messageLogger = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default messageLogger;
