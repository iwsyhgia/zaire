import ROOMS_ACTION_HANDLERS from './handlers/roomsHandlers';

const ACTION_HANDLERS = { ...ROOMS_ACTION_HANDLERS };
const initialState = {
  available_rooms: [],
  loading:         false,
  list:            [],
  room:            {},
};

const roomsReducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default roomsReducer;
