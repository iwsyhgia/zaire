import COMMON_ACTION_HANDLERS from './handlers/commonHandlers';

const ACTION_HANDLERS = { ...COMMON_ACTION_HANDLERS };
const initialState = {
  locale:      '',
  audit_info:  {},
  isOffline:   false,
  countries:   JSON.parse(localStorage.getItem('countries')),
  currentUser: JSON.parse(localStorage.getItem('currentUser')),
};

const commonReducer = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default commonReducer;
