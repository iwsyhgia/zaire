import PAYMENTS_ACTION_HANDLERS from './handlers/payments';

const ACTION_HANDLERS = { ...PAYMENTS_ACTION_HANDLERS };
const initialState = {
  discount:         [],
  payment:          [],
  paymentById:      {},
  charge:           {},
  refund:           [],
  general_payments: {
    paymentsLoaded: false,
  },
  combined_invoices: [],
  error:             null,
  loading:           false,
};

const payments = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default payments;
