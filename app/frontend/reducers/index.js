import { combineReducers } from 'redux';
import rooms     from './roomsReducer';
import room_types from './roomTypes';
import hotel_settings from './hotelSettings';
import amenities from './amenities';
import common    from './commonReducer';
import reservations from './reservations';
import payments from './payments';
import audit from './audit';
import profile from './profiles';
import messageLogger from './messageLogger';
import rates from './rates';

const rootReducer = combineReducers({
  amenities,
  audit,
  common,
  hotel_settings,
  message_logger: messageLogger,
  payments,
  profile,
  rates,
  reservations,
  rooms,
  room_types,
});

export default rootReducer;
