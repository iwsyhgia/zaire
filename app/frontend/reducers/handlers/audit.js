import {
  INIT_AUDIT,
  RUN_AUDIT,
  COMPLETE_AUDIT,
  FAIL_AUDIT,
  POSTPONE_AUDIT,
  PUSH_AUDIT,
  RESET_AUDIT_STATE,
  SET_OFFLINE_STATE,
  GET_LAST_COMPLETED_AUDIT,
} from '../../constants/actionConstants';


const AUDIT_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.audit, isOffline: true });
    }
    return state;
  },

  //
  [INIT_AUDIT]: (state, { payload }) => (
    {
      ...state,
      isReadyToStart: true,
      confirmationID: payload.id,
      auditDate:      payload.date,
    }
  ),

  //
  [RUN_AUDIT.SUCCESS]: state => (
    {
      ...state,
      isRunning: true,
    }
  ),

  //
  [COMPLETE_AUDIT]: state => (
    {
      ...state,
      isPerformed: true,
      isRunning:   false,
      message:     'successfull_audit', // TODO: update 'lastCompleted' audit date
    }
  ),
  [FAIL_AUDIT]: state => (
    {
      ...state,
      isPerformed: true,
      isRunning:   false,
      message:     'failed_audit',
    }
  ),

  //
  [POSTPONE_AUDIT]: state => (
    {
      ...state,
      isPostponed: true,
    }
  ),
  [PUSH_AUDIT]: state => (
    {
      ...state,
      isPostponed: false,
    }
  ),

  //
  [RESET_AUDIT_STATE]: state => (
    {
      ...state,
      isReadyToStart: false,
      isRunning:      false,
      isPostponed:    false,
      isPerformed:    false,
      confirmationID: null,
      message:        null,
    }
  ),

  [GET_LAST_COMPLETED_AUDIT.PENDING]: state => (
    { ...state, loading: true, success: null, error: null }
  ),
  [GET_LAST_COMPLETED_AUDIT.ERROR]: (state, { payload }) => (
    { ...state, loading: false, success: false, error: payload.error }
  ),
  [GET_LAST_COMPLETED_AUDIT.SUCCESS]: (state, { payload }) => (
    { ...state, lastCompleted: payload.response }
  ),
};

export default AUDIT_ACTION_HANDLERS;
