import {
  SET_LOCALE,
  SET_OFFLINE_STATE,
  SET_ONLINE_STATE,
  GET_CURRENT_USER,
  GET_COUNTRIES,
  GET_AUDIT_INFO,
} from '../../constants/actionConstants';


let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};

const COMMON_ACTION_HANDLERS = {
  [SET_LOCALE]: (state, action) => ({ ...state, locale: action.payload }),

  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.common, isOffline: true });
    }
    return state;
  },

  [SET_ONLINE_STATE]: (state) => {
    if (state.isOffline) {
      return { ...state, isOffline: false };
    }

    return state;
  },

  [GET_CURRENT_USER.SUCCESS]: (state, action) => ({ ...state, currentUser: action.payload }),

  [GET_COUNTRIES.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      countries: payload.response,
    }
  ),

  [GET_AUDIT_INFO.PENDING]: pendingState,
  [GET_AUDIT_INFO.ERROR]:   errorState,
  [GET_AUDIT_INFO.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      audit_info: payload.response,
    }
  ),
};

export default COMMON_ACTION_HANDLERS;
