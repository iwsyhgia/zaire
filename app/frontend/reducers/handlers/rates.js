import {
  GET_PERIODS_LIST,
  GET_SPECIAL_PERIOD,
  GET_STANDART_PERIOD,
  UPDATE_PERIOD,
} from '../../constants/actionConstants';


let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};

const RATES_ACTION_HANDLERS = {
  [GET_PERIODS_LIST.PENDING]: (pendingState = state => (
    {
      ...state,
      loading: true,
      success: null,
      error:   null,
    }
  )),
  [GET_PERIODS_LIST.ERROR]: (errorState = (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  )),
  [GET_PERIODS_LIST.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...payload.response,
      ...successState,
    }
  ),

  [GET_SPECIAL_PERIOD.PENDING]: pendingState,
  [GET_SPECIAL_PERIOD.ERROR]:   errorState,
  [GET_SPECIAL_PERIOD.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...payload.response,
      ...successState,
    }
  ),

  [GET_STANDART_PERIOD.PENDING]: pendingState,
  [GET_STANDART_PERIOD.ERROR]:   errorState,
  [GET_STANDART_PERIOD.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      standart_period: payload.response,
      ...successState,
    }
  ),
  [UPDATE_PERIOD.PENDING]: pendingState,
  [UPDATE_PERIOD.ERROR]:   errorState,
  [UPDATE_PERIOD.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      standart_period: { ...payload.response },
      ...successState,
    }
  ),
};

export default RATES_ACTION_HANDLERS;
