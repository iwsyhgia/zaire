import {
  LOG_ERROR,
  READ_ERROR_MESSAGE,
  SHOW_ERROR_MESSAGE,
} from '../../constants/actionConstants';


const MESSAGE_ACTION_HANDLERS = {

  // called in middleware ajax
  [LOG_ERROR]: (state, { payload }) => (
    {
      ...state,
      activeError: payload,
      errors:      [...state.errors, payload],
      list:        [...state.list, payload],
    }
  ),

  [SHOW_ERROR_MESSAGE]: (state, { payload }) => (
    {
      ...state,
      activeError: payload,
    }
  ),
  // to close error popup
  [READ_ERROR_MESSAGE]: state => (
    {
      ...state,
      activeError: null,
    }
  ),

};

export default MESSAGE_ACTION_HANDLERS;
