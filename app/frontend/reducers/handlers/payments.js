import {
  ADD_DISCOUNT,
  DELETE_DISCOUNT,
  EDIT_DISCOUNT,
  ADD_PAYMENT,
  DELETE_PAYMENT,
  EDIT_PAYMENT,
  ADD_REFUND,
  EDIT_REFUND,
  DELETE_REFUND,
  ADD_CHARGE,
  GET_GENERAL_PAYMENTS,
  GET_INVOICE,
  RESET_STATE,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';

import { INVOICE_TYPES } from '../../constants/constants';

let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};

const PAYMENTS_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.payments, isOffline: true });
    }
    return state;
  },

  [ADD_DISCOUNT.PENDING]: (pendingState = state => (
    {
      ...state,
      loading: true,
      success: null,
      error:   null,
    }
  )),
  [ADD_DISCOUNT.ERROR]: (errorState = (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  )),
  [ADD_DISCOUNT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
      },
      combined_invoices: [
        ...state.combined_invoices,
        { ...payload.response, invoiceType: INVOICE_TYPES.DISCOUNT }
      ],
      discount: [...state.discount, payload.response],
    }
  ),

  [DELETE_DISCOUNT.PENDING]: pendingState,
  [DELETE_DISCOUNT.ERROR]:   errorState,
  [DELETE_DISCOUNT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        discounts: [
          ...state.general_payments.discounts.filter(item => item.id !== payload.response.id)
        ],
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id)
      ],
    }
  ),

  [EDIT_DISCOUNT.PENDING]: pendingState,
  [EDIT_DISCOUNT.ERROR]:   errorState,
  [EDIT_DISCOUNT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id),
        { ...payload.response, invoiceType: 'discount' }
      ],
    }
  ),

  [ADD_PAYMENT.PENDING]: pendingState,
  [ADD_PAYMENT.ERROR]:   errorState,
  [ADD_PAYMENT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        payments: [
          ...state.general_payments.payments,
          payload.response
        ],
      },
      combined_invoices: [
        ...state.combined_invoices,
        { ...payload.response, invoiceType: INVOICE_TYPES.PAYMENT }
      ],
      payment: [...state.payment, payload.response],
    }
  ),

  [DELETE_PAYMENT.PENDING]: pendingState,
  [DELETE_PAYMENT.ERROR]:   errorState,
  [DELETE_PAYMENT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        payments: [
          ...state.general_payments.payments.filter(item => item.id !== payload.response.id)
        ],
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id)
      ],
    }
  ),

  [EDIT_PAYMENT.PENDING]: pendingState,
  [EDIT_PAYMENT.ERROR]:   errorState,
  [EDIT_PAYMENT.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        payments: [
          ...state.general_payments.payments.filter(invoice => invoice.id !== payload.response.id),
          { ...payload.response }
        ],
        ...payload.response.invoice,
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id),
        { ...payload.response, invoiceType: INVOICE_TYPES.PAYMENT }
      ],
    }
  ),

  [ADD_REFUND.PENDING]: pendingState,
  [ADD_REFUND.ERROR]:   errorState,
  [ADD_REFUND.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        refunds: [
          ...state.general_payments.refunds,
          payload.response
        ],
      },
      combined_invoices: [
        ...state.combined_invoices,
        { ...payload.response, invoiceType: INVOICE_TYPES.REFUND }
      ],
      refund: [...state.refund, payload.response],
    }
  ),

  [DELETE_REFUND.PENDING]: pendingState,
  [DELETE_REFUND.ERROR]:   errorState,
  [DELETE_REFUND.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        refunds: [
          ...state.general_payments.refunds.filter(item => item.id !== payload.response.id)
        ],
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id)
      ],
    }
  ),

  [EDIT_REFUND.PENDING]: pendingState,
  [EDIT_REFUND.ERROR]:   errorState,
  [EDIT_REFUND.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      general_payments: {
        ...state.general_payments,
        ...payload.response.invoice,
        refunds: [
          ...state.general_payments.refunds.filter(invoice => invoice.id !== payload.response.id),
          { ...payload.response }
        ],
      },
      combined_invoices: [
        ...state.combined_invoices.filter(invoice => invoice.id !== payload.response.id),
        { ...payload.response, invoiceType: INVOICE_TYPES.REFUND }
      ],
    }
  ),

  [ADD_CHARGE.PENDING]: pendingState,
  [ADD_CHARGE.ERROR]:   errorState,
  [ADD_CHARGE.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      charge: payload.response,
    }
  ),

  [GET_INVOICE]: (state, { payload }) => (
    {
      ...state,
      paymentById: payload.paymentById,
    }
  ),

  [GET_GENERAL_PAYMENTS.PENDING]: pendingState,
  [GET_GENERAL_PAYMENTS.ERROR]:   errorState,
  [GET_GENERAL_PAYMENTS.SUCCESS]: (state, { payload }) => {
    const charges = payload.response.charges.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.CHARGE,
    }));
    const discounts = payload.response.discounts.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.DISCOUNT,
    }));
    const payments = payload.response.payments.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.PAYMENT,
    }));
    const refunds = payload.response.refunds.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.REFUND,
    }));
    return {
      ...state,
      ...successState,
      general_payments: {
        ...payload.response,
        paymentsLoaded: true,
      },
      combined_invoices: [
        ...charges,
        ...discounts,
        ...payments,
        ...refunds
      ],
    };
  },

  [GET_GENERAL_PAYMENTS.OFFLINE]: (state, { parentAjaxAction, offlineStorage }) => {
    const currentInvoice = offlineStorage.invoices.find(item => item.id === +parentAjaxAction.payload.data.id);
    const charges = currentInvoice.charges.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.CHARGE,
    }));
    const discounts = currentInvoice.discounts.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.DISCOUNT,
    }));
    const payments = currentInvoice.payments.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.PAYMENT,
    }));
    const refunds = currentInvoice.refunds.map(item => ({
      ...item,
      invoiceType: INVOICE_TYPES.REFUND,
    }));

    return {
      ...state,
      loading:          true,
      general_payments: {
        ...currentInvoice,
        paymentsLoaded: true,
      },
      combined_invoices: [
        ...charges,
        ...discounts,
        ...payments,
        ...refunds
      ],
    };
  },

  [RESET_STATE]: state => (
    {
      ...state,
      loading: false,
      success: null,
      error:   null,
    }
  ),
};

export default PAYMENTS_ACTION_HANDLERS;
