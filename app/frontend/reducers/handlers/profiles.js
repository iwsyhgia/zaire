import {
  GET_PROFILE,
  UPDATE_PROFILE,
  STORE_PROFILE,
  STORE_PROFILE_FORM,
  TOGGLE_PROFILE_FIELD,
  RESET_FORM,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';

const errorState   = { loading: false, success: false };
const pendingState = { loading: true, success: null, error: null };
const successState = { loading: false, success: true, error: null };

const errorHandler = (state, { payload: { error } }) => ({ ...state, ...errorState, error });
const pendingHandler = state => ({ ...state, ...pendingState });
const successHandler = (state, { payload: { response } }) => {
  const { form } = state;
  return {
    ...state,
    ...successState,
    validation: {},
    model:      response,
    form:       {
      avatar:        { ...form.avatar },
      password:      { ...form.password },
      first_name:    { ...form.first_name, value: response.first_name || '' },
      last_name:     { ...form.last_name, value: response.last_name || '' },
      phone_number:  { ...form.phone_number, value: response.phone_number || '' },
      notifications: { ...form.notifications, value: response.notifications },
    },
  };
};

const ACCOUNTS_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.profile, isOffline: true });
    }
    return state;
  },
  [STORE_PROFILE]:        (state, { payload }) => ({ ...state, ...payload }),
  [STORE_PROFILE_FORM]:   (state, { payload }) => ({ ...state, form: { ...state.form, ...payload } }),
  [TOGGLE_PROFILE_FIELD]: (state, { payload }) => {
    const { form } = state;
    const name = payload;
    return { ...state, form: { ...form, [name]: { ...form[name], expanded: !form[name].expanded } } };
  },
  [RESET_FORM]: (state) => {
    const { model } = state;
    return {
      ...state,
      cropShow:   false,
      validation: { },
      form:       {
        password:      { },
        avatar:        { },
        first_name:    { value: model.first_name },
        last_name:     { value: model.last_name },
        phone_number:  { value: model.phone_number },
        notifications: { value: model.notifications },
      },
    };
  },
  [GET_PROFILE.ERROR]:   errorHandler,
  [GET_PROFILE.PENDING]: pendingHandler,
  [GET_PROFILE.SUCCESS]: successHandler,

  [UPDATE_PROFILE.ERROR]:   errorHandler,
  [UPDATE_PROFILE.PENDING]: pendingHandler,
  [UPDATE_PROFILE.SUCCESS]: successHandler,
};

export default ACCOUNTS_ACTION_HANDLERS;
