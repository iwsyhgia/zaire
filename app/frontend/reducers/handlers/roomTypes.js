import {
  GET_ROOM_TYPES,
  ADD_ROOM_TYPE,
  UPDATE_ROOM_TYPE,
  DELETE_ROOM_TYPE,
  LOAD_ROOM_TYPE,
  UPDATE_ROOM,
  DELETE_ROOM,
  GET_ROOM_TYPE,
  UPDATE_AMENITIES,
  UPDATE_IMAGES,
  UPDATE_ROOMS,
  RESET_STATE,
  RESET_ERROR,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';
import { NO_ITEMS } from '../../constants/constants';

const pendingState = state => (
  {
    ...state,
    loading: true,
    success: null,
    error:   null,
  }
);
const errorState = (state, { payload }) => (
  {
    ...state,
    loading: false,
    success: false,
    error:   payload.error,
  }
);
const successState = {
  loading: false,
  success: true,
  error:   null,
};

const resetCurrent = {
  amenities: [],
  images:    [],
  rooms:     [],
  loading:   false,
  success:   null,
  error:     null,
};

const ROOM_TYPE_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.room_types, isOffline: true });
    }
    return state;
  },

  //
  [GET_ROOM_TYPES.PENDING]: pendingState,
  [GET_ROOM_TYPES.ERROR]:   errorState,
  [GET_ROOM_TYPES.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      list: payload.response.length ? payload.response : NO_ITEMS,
    }
  ),

  //
  [ADD_ROOM_TYPE.PENDING]: pendingState,
  [ADD_ROOM_TYPE.ERROR]:   errorState,
  [ADD_ROOM_TYPE.SUCCESS]: (state, { payload }) => {
    const initList = state.list === NO_ITEMS ? [] : [...state.list];
    return {
      ...state,
      ...successState,
      current: {
        ...state.current,
        success: true,
      },
      list: [...initList, payload.response],
    };
  },

  //
  [UPDATE_ROOM_TYPE.PENDING]: pendingState,
  [UPDATE_ROOM_TYPE.ERROR]:   errorState,
  [UPDATE_ROOM_TYPE.SUCCESS]: (state, { payload }) => {
    const updatedRoomTypes = [...state.list];
    const index = updatedRoomTypes.findIndex(item => item.id === payload.id);
    if (index > -1) {
      updatedRoomTypes[index] = payload.response;
    }
    return {
      ...state,
      ...successState,
      current: {
        ...state.current,
        success: true,
      },
      list: updatedRoomTypes,
    };
  },

  //
  [DELETE_ROOM_TYPE.PENDING]: pendingState,
  [DELETE_ROOM_TYPE.ERROR]:   errorState,
  [DELETE_ROOM_TYPE.SUCCESS]: (state, { parentAjaxAction: { payload } }) => {
    const updatedRoomTypes = [...state.list];
    const index = updatedRoomTypes.findIndex(item => item.id === payload.id);
    if (index > -1) {
      updatedRoomTypes.splice(index, 1);
    }
    return {
      ...state,
      ...successState,
      current: {
        ...state.current,
        success: null,
        deleted: true,
      },
      list: updatedRoomTypes.length ? updatedRoomTypes : NO_ITEMS,
    };
  },

  [LOAD_ROOM_TYPE.PENDING]: pendingState,
  [LOAD_ROOM_TYPE.ERROR]:   errorState,
  [LOAD_ROOM_TYPE.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      current: {
        ...payload.response,
        amenityIds: payload.response.amenities.map(item => item.id),
      },
    }
  ),

  //
  [UPDATE_ROOM.PENDING]: pendingState,
  [UPDATE_ROOM.ERROR]:   errorState,
  [UPDATE_ROOM.SUCCESS]: (state, { payload }) => {
    const updatedRooms = [...state.current.rooms];
    const index = updatedRooms.findIndex(item => item.id === payload.response.id);
    if (index > -1) {
      updatedRooms[index] = payload.response;
    }
    return {
      ...state,
      current: { ...state.current, rooms: updatedRooms },
    };
  },

  //
  [DELETE_ROOM.PENDING]: pendingState,
  [DELETE_ROOM.ERROR]:   errorState,
  [DELETE_ROOM.SUCCESS]: (state, { parentAjaxAction: { payload } }) => {
    const updatedRooms = [...state.current.rooms];
    const index = updatedRooms.findIndex(item => item.id === payload.id);
    if (index > -1) {
      updatedRooms.splice(index, 1);
    }
    return {
      ...state,
      current: { ...state.current, rooms: updatedRooms },
    };
  },

  //
  [GET_ROOM_TYPE]: (state, { payload }) => {
    const roomType = state.list.find(item => item.id === payload.room_type_id);

    return (
      {
        ...state,
        current: { ...roomType, amenityIds: roomType.amenities.map(item => item.id) },
      }
    );
  },

  [UPDATE_AMENITIES]: (state, { payload }) => (
    {
      ...state,
      current: { ...state.current, amenityIds: payload.amenities },
    }
  ),

  [UPDATE_IMAGES]: (state, { payload }) => (
    {
      ...state,
      current: { ...state.current, images: payload.images },
    }
  ),

  [UPDATE_ROOMS]: (state, { payload }) => (
    {
      ...state,
      current: { ...state.current, rooms: payload.rooms },
    }
  ),

  [RESET_STATE]: (state, { payload }) => (
    {
      ...state,
      current: { ...resetCurrent, ...payload },
    }
  ),

  [RESET_ERROR]: state => (
    {
      ...state,
      loading: false,
      success: null,
      error:   null,
    }
  ),
};

export default ROOM_TYPE_ACTION_HANDLERS;
