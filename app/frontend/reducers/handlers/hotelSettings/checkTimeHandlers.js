import {
  GET_CHECK_TIME,
  STORE_CHECK_TIME,
  UPDATE_CHECK_TIME,
} from '../../../constants/actionConstants';


const fetchCheckTime = (state, { payload }) => (
  {
    ...state,
    checkTime: {
      ...state.checkTime,
      real:       payload.response,
      model:      payload.response,
      validation: null,
      loading:    false,
      success:    true,
      error:      null,
    },
  }
);

const errorCheckTime = (state, { payload }) => (
  {
    ...state,
    checkTime: {
      ...state.checkTime,
      loading: false,
      success: false,
      error:   payload.error,
    },
  }
);

const CHECK_TIME_HANDLERS = {
  [GET_CHECK_TIME.SUCCESS]:    fetchCheckTime,
  [GET_CHECK_TIME.ERROR]:      errorCheckTime,
  [UPDATE_CHECK_TIME.SUCCESS]: fetchCheckTime,
  [UPDATE_CHECK_TIME.ERROR]:   errorCheckTime,

  [STORE_CHECK_TIME]: (state, { payload }) => (
    {
      ...state,
      checkTime: {
        ...state.checkTime,
        ...payload,
      },
    }
  ),

};

export default CHECK_TIME_HANDLERS;
