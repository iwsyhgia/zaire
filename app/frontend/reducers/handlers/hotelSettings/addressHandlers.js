import {
  GET_ADDRESS,
  UPDATE_ADDRESS,
  STORE_ADDRESS,
} from '../../../constants/actionConstants';

const fetchAddress = (state, { payload }) => ({
  ...state,
  address: {
    ...state.address,
    country_input: {
      value: payload.response.country,
      label: payload.response.country,
    },
    real:       { ...payload.response },
    model:      { ...payload.response },
    validation: null,
    loading:    false,
    success:    true,
    error:      null,
  },
});


const errorAddress = (state, { payload }) => (
  {
    ...state,
    address: {
      loading: false,
      success: false,
      error:   payload.error,
    },
  }
);

const ADDRESS_HANDLERS = {
  [GET_ADDRESS.SUCCESS]:    fetchAddress,
  [GET_ADDRESS.ERROR]:      errorAddress,
  [UPDATE_ADDRESS.SUCCESS]: fetchAddress,
  [UPDATE_ADDRESS.ERROR]:   errorAddress,

  [STORE_ADDRESS]: (state, { payload }) => (
    {
      ...state,
      address: {
        ...state.address,
        ...payload,
      },
    }
  ),
};


export default ADDRESS_HANDLERS;
