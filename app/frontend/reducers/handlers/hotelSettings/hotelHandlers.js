import {
  GET_HOTEL,
  UPDATE_HOTEL,
  STORE_HOTEL,
  STORE_HOTEL_SETTINGS,
} from '../../../constants/actionConstants';


const fetchHotel = (state, { payload }) => (
  {
    ...state,
    hotel: {
      ...state.hotel,
      ...payload.response,
      validation: null,
      loading:    false,
      success:    true,
      error:      null,
    },
  }
);

const errorHotel = (state, { payload }) => (
  {
    ...state,
    hotel: {
      loading: false,
      success: false,
      error:   payload.error,
    },
  }
);

const HOTEL_HANDLERS = {
  [GET_HOTEL.SUCCESS]:    fetchHotel,
  [GET_HOTEL.ERROR]:      errorHotel,
  [UPDATE_HOTEL.SUCCESS]: fetchHotel,
  [UPDATE_HOTEL.ERROR]:   errorHotel,

  [STORE_HOTEL]: (state, { payload }) => ({
    ...state,
    hotel: {
      ...state.hotel,
      ...payload,
    },
  }),

  [STORE_HOTEL_SETTINGS]: (state, { payload }) => ({
    ...state,
    ...payload,
  }),
};


export default HOTEL_HANDLERS;
