import { SET_OFFLINE_STATE } from '../../../constants/actionConstants';

const COMMON_HOTEL_SETTINGS_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.hotel_settings, isOffline: true });
    }
    return state;
  },
};

export default COMMON_HOTEL_SETTINGS_HANDLERS;
