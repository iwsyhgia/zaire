import i18n from 'i18next';
import {
  GET_LOGO,
  CREATE_LOGO,
  UPDATE_LOGO,
  DELETE_LOGO,
  STORE_LOGO,
} from '../../../constants/actionConstants';


const fetchLogo = (state, { payload }) => ({
  ...state,
  logo: {
    ...state.logo,
    model:        payload.response,
    file:         null,
    progressShow: false,
    loading:      false,
    success:      true,
    error:        null,
  },
});

const errorLogo = (state, { payload }) => (
  {
    ...state,
    logo: {
      ...state.logo,
      ...{ file: null, progressShow: false },
      loading: false,
      success: true,
      error:   payload.error || i18n.t('images.errors.size_invalid'),
    },
  }
);

const LOGO_HANDLERS = {
  [GET_LOGO.SUCCESS]:    fetchLogo,
  [CREATE_LOGO.SUCCESS]: fetchLogo,
  [UPDATE_LOGO.SUCCESS]: fetchLogo,
  [CREATE_LOGO.ERROR]:   errorLogo,
  [UPDATE_LOGO.ERROR]:   errorLogo,
  [DELETE_LOGO.ERROR]:   errorLogo,

  [DELETE_LOGO.SUCCESS]: state => (
    {
      ...state,
      logo: {
        ...state.logo,
        ...{ model: null },
        loading: false,
        success: true,
        error:   null,
      },
    }
  ),

  [STORE_LOGO]: (state, { payload }) => ({ ...state, logo: { ...state.logo, ...payload } }),
};


export default LOGO_HANDLERS;
