import { registerServiceWorker, unregisterServiceWorker } from '../../../service-worker/serviceWorkerCompanion';
import {
  UPDATE_OFFLINE_MODE,
  GET_OFFLINE_MODE,
} from '../../../constants/actionConstants';


const OFFLINE_HANDLERS = {
  [GET_OFFLINE_MODE.PENDING]: state => ({ ...state, loading: true, success: null, error: null }),
  [GET_OFFLINE_MODE.SUCCESS]: (state, { payload }) => ({
    ...state,
    loading: false,
    success: true,
    error:   null,
    offline: payload.response,
  }),
  [GET_OFFLINE_MODE.ERROR]: (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  ),

  [UPDATE_OFFLINE_MODE.PENDING]: state => ({ ...state, loading: true, success: null, error: null }),
  [UPDATE_OFFLINE_MODE.SUCCESS]: (state, { payload }) => {
    if (payload.response.active) {
      registerServiceWorker();
    } else {
      unregisterServiceWorker();
    }

    return ({
      ...state,
      loading: false,
      success: true,
      error:   null,
      offline: { ...payload.response },
    });
  },
  [UPDATE_OFFLINE_MODE.ERROR]: (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }),
};


export default OFFLINE_HANDLERS;
