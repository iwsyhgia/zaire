import {
  GET_AUDIT_TIME,
  STORE_AUDIT_TIME,
  UPDATE_AUDIT_TIME,
} from '../../../constants/actionConstants';


const fetchAuditTime = (state, { payload }) => (
  {
    ...state,
    nightAudit: {
      ...state.nightAudit,
      real:       payload.response,
      model:      payload.response,
      validation: null,
      loading:    false,
      success:    true,
      error:      null,
    },
  }
);

const errorAuditTime = (state, { payload }) => (
  {
    ...state,
    nightAudit: {
      ...state.nightAudit,
      loading: false,
      success: false,
      error:   payload.error,
    },
  }
);

const AUDIT_HANDLERS = {
  [GET_AUDIT_TIME.SUCCESS]:    fetchAuditTime,
  [GET_AUDIT_TIME.ERROR]:      errorAuditTime,
  [UPDATE_AUDIT_TIME.SUCCESS]: fetchAuditTime,
  [UPDATE_AUDIT_TIME.ERROR]:   errorAuditTime,

  [STORE_AUDIT_TIME]: (state, { payload }) => (
    {
      ...state,
      nightAudit: {
        ...state.nightAudit,
        ...payload,
      },
    }
  ),

};

export default AUDIT_HANDLERS;
