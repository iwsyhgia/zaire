import {
  GET_AMENITIES,
  ADD_AMENITY,
  DELETE_AMENITY,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';

let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};


const AMENITIES_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.amenities, isOffline: true });
    }
    return state;
  },

  //
  [GET_AMENITIES.PENDING]: (pendingState = state => (
    {
      ...state,
      loading: true,
      success: null,
      error:   null,
    }
  )),
  [GET_AMENITIES.ERROR]: (errorState = (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  )),
  [GET_AMENITIES.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      list: payload.response,
    }
  ),

  //
  [ADD_AMENITY.PENDING]: pendingState,
  [ADD_AMENITY.ERROR]:   errorState,
  [ADD_AMENITY.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      list: [...state.list, payload.response],
    }
  ),

  //
  [DELETE_AMENITY.PENDING]: pendingState,
  [DELETE_AMENITY.ERROR]:   errorState,
  [DELETE_AMENITY.SUCCESS]: (state, { parentAjaxAction: { payload } }) => {
    const updatedAmenities = [...state.list];
    const index = updatedAmenities.findIndex(item => item.id === payload.id);
    if (index > -1) {
      updatedAmenities.splice(index, 1);
    }
    return {
      ...state,
      ...successState,
      list: updatedAmenities,
    };
  },
};

export default AMENITIES_ACTION_HANDLERS;
