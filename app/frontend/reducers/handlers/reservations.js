import {
  GET_RESERVATIONS,
  ADD_RESERVATION,
  UPDATE_RESERVATION,
  DELETE_RESERVATION,
  GET_RESERVATION_BY_ID,
  GET_RESERVATION,
  UPDATE_RESERVATIONS_LIST,
  GET_DNR_RESERVATIONS,
  ADD_DNR_RESERVATION,
  GET_DNR_RESERVATION,
  DELETE_DNR_RESERVATION,
  UPDATE_DNR_RESERVATION,
  CHEKIN_RESERVATION,
  CHEKOUT_RESERVATION,
  ADD_ADULT,
  SHOW_RESERVATION_POPUP,
  CHANGE_RESERVATION_DATE_RANGE,
  RESET_STATE,
  RESET_CURRENT,
  RESET_FORM,
  GET_GUESTS,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';

import { CANCELLED } from '../../constants/constants';

let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};
const resetForm = {
  type:         'direct',
  step:         0,
  savedValues:  {},
  roomID:       null,
  roomOption:   null,
  checkIn:      null,
  checkOut:     null,
  formBDay:     null,
  formCountry:  null,
  formEmail:    null,
  formPhone:    null,
  formTitle:    null,
  formLastName: null,
};

const RESERVATION_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.reservations, isOffline: true });
    }
    return state;
  },

  [GET_RESERVATIONS.PENDING]: (pendingState = state => (
    {
      ...state,
      loading: true,
      success: null,
      error:   null,
    }
  )),
  [GET_RESERVATIONS.ERROR]: (errorState = (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  )),
  [GET_RESERVATIONS.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      list: payload.response,
    }
  ),

  //
  [ADD_RESERVATION.PENDING]: pendingState,
  [ADD_RESERVATION.ERROR]:   errorState,
  [ADD_RESERVATION.SUCCESS]: (state, { parentAjaxAction, payload }) => {
    const updatedForm = parentAjaxAction.payload.readyToCheckin
      ? { step: 3 }
      : { ...resetForm };
    const updatedCurrent = parentAjaxAction.payload.readyToCheckin
      ? { ...payload.response }
      : {};

    return {
      ...state,
      ...successState,
      list:         [...state.list, payload.response],
      current:      { ...updatedCurrent },
      readyCheckin: parentAjaxAction.payload.readyToCheckin,
      form:         { ...state.form, ...updatedForm },
    };
  },

  //
  [UPDATE_RESERVATION.PENDING]: pendingState,
  [UPDATE_RESERVATION.ERROR]:   errorState,
  [UPDATE_RESERVATION.SUCCESS]: (state, { parentAjaxAction, payload }) => {
    const loadedReservations = [...state.list];
    const index = loadedReservations.findIndex(item => item.id === parentAjaxAction.payload.id);
    if (index > -1) {
      loadedReservations[index] = payload.response;
    }
    const updatedForm = parentAjaxAction.payload.readyToCheckin
      ? { step: 3 }
      : { ...resetForm };
    const updatedCurrent = parentAjaxAction.payload.readyToCheckin
      ? { ...payload.response }
      : {};

    return {
      ...state,
      ...successState,
      list:    loadedReservations,
      current: { ...updatedCurrent },
      form:    { ...state.form, ...updatedForm },
    };
  },

  //
  [DELETE_RESERVATION.PENDING]: pendingState,
  [DELETE_RESERVATION.ERROR]:   errorState,
  [DELETE_RESERVATION.SUCCESS]: (state, { parentAjaxAction: { payload } }) => {
    const loadedReservations = [...state.list];
    const index = loadedReservations.findIndex(item => item.id === payload.id);
    if (index > -1) {
      loadedReservations.splice(index, 1);
    }
    return {
      ...state,
      ...successState,
      list: loadedReservations,
    };
  },

  //
  [GET_RESERVATION_BY_ID.PENDING]: pendingState,
  [GET_RESERVATION_BY_ID.ERROR]:   errorState,
  [GET_RESERVATION_BY_ID.OFFLINE]: (state, { parentAjaxAction }) => {
    const currentOfflineReservation = state.list.find(item => item.id === +parentAjaxAction.payload.data.id);
    return {
      ...state,
      ...successState,
      current: { ...currentOfflineReservation },
    };
  },
  [GET_RESERVATION_BY_ID.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      current: { ...payload.response },
    }
  ),

  // DNR
  [GET_DNR_RESERVATIONS.PENDING]: pendingState,
  [GET_DNR_RESERVATIONS.ERROR]:   errorState,
  [GET_DNR_RESERVATIONS.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      dnr: [...payload.response],
    }
  ),

  //
  [ADD_DNR_RESERVATION.PENDING]: pendingState,
  [ADD_DNR_RESERVATION.ERROR]:   errorState,
  [ADD_DNR_RESERVATION.SUCCESS]: (state, { payload }) => {
    const { dnr } = payload.response;
    const reservations  = payload.response.reservations || [];
    let reservationsList = state.list;

    if (reservations.length) {
      const updatedReservationsIds = reservations.map(r => r.id);
      reservationsList = reservationsList.filter(r => !updatedReservationsIds.includes(r.id));
    }

    return {
      ...state,
      ...successState,
      list: [...reservationsList, { ...dnr, status: 'default_dnr' }, ...reservations],
      form: { ...resetForm },
    };
  },

  [UPDATE_DNR_RESERVATION.PENDING]: pendingState,
  [UPDATE_DNR_RESERVATION.ERROR]:   errorState,
  [UPDATE_DNR_RESERVATION.SUCCESS]: (state, { payload }) => {
    const { dnr } = payload.response;
    let reservationsList = state.list;

    const index = reservationsList.findIndex(item => item.id === dnr.id);
    if (index > -1) {
      reservationsList[index] = { ...dnr, status: 'default_dnr' };
    }

    const reservations  = payload.response.reservations || [];

    if (reservations.length) {
      const updatedReservationsIds = reservations.map(r => r.id);
      reservationsList = reservationsList.filter(r => !updatedReservationsIds.includes(r.id));
    }

    return {
      ...state,
      ...successState,
      list: [...reservationsList, ...reservations],
      form: { ...resetForm },
    };
  },

  [GET_DNR_RESERVATION]: (state, { payload }) => (
    {
      ...state,
      current: [...state.list].find(item => item.id === payload.id) || {},
    }
  ),

  [DELETE_DNR_RESERVATION.PENDING]: pendingState,
  [DELETE_DNR_RESERVATION.ERROR]:   errorState,
  [DELETE_DNR_RESERVATION.SUCCESS]: (state, { payload }) => {
    const { dnr } = payload.response;
    const { id, check_out_date: checkOut } = dnr;
    const reservationsList = [...state.list];

    const index = reservationsList.findIndex(item => item.id === id);
    if (index !== -1) {
      if (checkOut) {
        // update dnr reservation
        reservationsList[index] = { ...dnr, status: 'cut_dnr' };
      } else {
        reservationsList.splice(index, 1);
      }
    }

    return {
      ...state,
      ...successState,
      list: [...reservationsList],
    }
  },

  //
  [CHEKIN_RESERVATION.PENDING]: pendingState,
  [CHEKIN_RESERVATION.ERROR]:   errorState,
  [CHEKIN_RESERVATION.SUCCESS]: (state, { payload }) => {
    const { id, status } = payload.response;
    const loadedReservations = [...state.list];
    const index = loadedReservations.findIndex(item => item.id === id);
    if (index > -1) {
      loadedReservations[index].status = status;
    }
    return {
      ...state,
      ...successState,
      list:    [...loadedReservations],
      form:    { ...resetForm },
      current: { ...payload.response },
    };
  },

  [CHEKOUT_RESERVATION.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      current: { ...payload.response },
    }
  ),

  //
  [ADD_ADULT.PENDING]: pendingState,
  [ADD_ADULT.ERROR]:   errorState,
  [ADD_ADULT.SUCCESS]: (state, { parentAjaxAction: { payload } }) => {
    const loadedReservations = [...state.list];
    const currentReservation = { ...state.current };
    const adult = { ...payload.data };

    const index = loadedReservations.findIndex(item => item.id === adult.reservation_id);
    if (index > -1) {
      loadedReservations[index].adults.push(adult);
    }
    if (currentReservation.id === adult.reservation_id
      && !currentReservation.adults.find(item => item.personal_id === adult.personal_id)
    ) {
      currentReservation.adults.push(adult);
    }

    return {
      ...state,
      ...successState,
      list:    loadedReservations,
      current: currentReservation,
    };
  },

  //
  [UPDATE_RESERVATIONS_LIST]: (state, { payload }) => {
    const loadedReservations = [...state.list];
    const id = payload.reservation.id;
    const index = loadedReservations.findIndex(item => item.id === id);
    if (index > -1) {
      if (payload.reservation.status === CANCELLED) {
        loadedReservations.splice(index, 1);
      } else {
        loadedReservations[index] = payload.reservation;
      }
    }
    return {
      ...state,
      list: loadedReservations,
    };
  },

  [GET_RESERVATION]: (state, { payload }) => {
    const loadedReservations = [...state.list];
    const reservation = loadedReservations.find(item => item.id === payload.id) || {};
    return {
      ...state,
      current: reservation,
    };
  },

  [SHOW_RESERVATION_POPUP]: (state, { payload }) => (
    {
      ...state,
      form: { ...state.form, ...payload },
    }
  ),

  [CHANGE_RESERVATION_DATE_RANGE]: (state, { payload }) => (
    {
      ...state,
      checkin_date:  payload.checkin_date,
      checkout_date: payload.checkout_date,
    }
  ),

  [GET_GUESTS.PENDING]: pendingState,
  [GET_GUESTS.ERROR]:   errorState,
  [GET_GUESTS.SUCCESS]: (state, { payload }) => (
    {
      ...state,
      ...successState,
      guests: [...payload.response],
    }
  ),

  [RESET_STATE]: state => (
    {
      ...state,
      loading:      false,
      success:      null,
      error:        null,
      readyCheckin: false,
    }
  ),

  [RESET_CURRENT]: state => (
    {
      ...state,
      loading: false,
      success: null,
      error:   null,
      current: {},
      form:    { ...resetForm },
    }
  ),

  [RESET_FORM]: state => (
    {
      ...state,
      form: { ...resetForm },
    }
  ),
};

export default RESERVATION_ACTION_HANDLERS;
