import {
  SET_ROOMS,
  SET_ROOM,
  SET_ROOM_TYPES,
  SET_ROOM_TYPE,
  SET_AMENITIES,
  GET_ROOMS,
  GET_AVAILABLE_ROOMS,
  SET_OFFLINE_STATE,
} from '../../constants/actionConstants';

import { NO_ROOMS } from '../../constants/constants';

let pendingState;
let errorState;
const successState = {
  loading: false,
  success: true,
  error:   null,
};

const ROOMS_ACTION_HANDLERS = {
  [SET_OFFLINE_STATE]: (state, action) => {
    if (!state.isOffline) {
      return ({ ...action.payload.rooms, isOffline: true });
    }
    return state;
  },

  [SET_ROOMS]:      (state, action) => ({ ...state, list: action.payload.response }),
  [SET_ROOM]:       (state, action) => ({ ...state, room: action.payload.response }),
  [SET_ROOM_TYPES]: (state, action) => ({ ...state, list: action.payload.response }),
  [SET_ROOM_TYPE]:  (state, action) => ({ ...state, room_type: action.payload.response }),
  [SET_AMENITIES]:  (state, action) => ({ ...state, amenities: action.payload.response }),

  //
  [GET_ROOMS.PENDING]: (pendingState = state => (
    {
      ...state,
      loading: true,
      success: null,
      error:   null,
    }
  )),
  [GET_ROOMS.ERROR]: (errorState = (state, { payload }) => (
    {
      ...state,
      loading: false,
      success: false,
      error:   payload.error,
    }
  )),
  [GET_ROOMS.SUCCESS]: (state, { payload }) => {
    const rooms = payload.response;
    if (rooms.length === 0) {
      rooms.push(NO_ROOMS);
    }
    return {
      ...state,
      ...successState,
      list: rooms,
    };
  },

  //
  [GET_AVAILABLE_ROOMS.PENDING]: state => (
    {
      ...state,
      loading_av_rooms: true,
      success_av_room:  null,
      error_av_rooms:   null,
    }
  ),
  [GET_AVAILABLE_ROOMS.ERROR]: (state, { payload }) => (
    {
      ...state,
      loading_av_rooms: false,
      success_av_room:  false,
      error_av_rooms:   payload.error,
    }
  ),
  [GET_AVAILABLE_ROOMS.SUCCESS]: (state, { payload }) => {
    const fixed = payload.response.reduce((result, item) => {
      // remove duplicated items
      if (result.some(element => element.id === item.id)) {
        return result;
      }
      return [...result, item];
    }, []);

    if (fixed.length === 0) {
      fixed.push(NO_ROOMS);
    }

    return {
      ...state,
      loading_av_rooms: false,
      success_av_room:  true,
      error_av_rooms:   null,
      available_rooms: fixed,
    };
  },

};

export default ROOMS_ACTION_HANDLERS;
