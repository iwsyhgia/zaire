import RESERVATION_ACTION_HANDLERS from './handlers/reservations';

const ACTION_HANDLERS = { ...RESERVATION_ACTION_HANDLERS };
const initialState = {
  current:       {},
  list:          [],
  dnr:           [],
  guests:        [],
  checkin_date:  null,
  checkout_date: null,
  loading:       false,
  success:       null,
  error:         null,
  readyCheckin:  false,
  form:          {
    type:        'direct', // DNR
    step:        0, // 1 - add, 2 - edit, 3 - checkin
    isEdit:      false,
    roomID:      null,
    roomOption:  null,
    checkIn:     null,
    checkOut:    null,
    description: '',
  },
};

const reservations = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default reservations;
