import ROOM_TYPE_ACTION_HANDLERS from './handlers/roomTypes';

const ACTION_HANDLERS = { ...ROOM_TYPE_ACTION_HANDLERS };
const initialState = {
  list:    [],
  current: {
    amenityIds: [],
    images:     [],
    rooms:      [],
    loading:    false,
    success:    null,
    error:      null,
  },
  amenities: [],
  loading:   false,
  success:   null,
  error:     null,
};

const room_types = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default room_types;
