import OFFLINE_HANDLERS from './handlers/hotelSettings/offlineHandlers';
import LOGO_HANDLERS from './handlers/hotelSettings/logoHandlers';
import HOTEL_HANDLERS from './handlers/hotelSettings/hotelHandlers';
import ADDRESS_HANDLERS from './handlers/hotelSettings/addressHandlers';
import CHECK_TIME_HANDLERS from './handlers/hotelSettings/checkTimeHandlers';
import AUDIT_HANDLERS from './handlers/hotelSettings/auditHandlers';
import COMMON_HOTEL_SETTINGS_HANDLERS from './handlers/hotelSettings/commonHotelSettingsHandlers';


const ACTION_HANDLERS = {
  ...OFFLINE_HANDLERS,
  ...LOGO_HANDLERS,
  ...HOTEL_HANDLERS,
  ...ADDRESS_HANDLERS,
  ...CHECK_TIME_HANDLERS,
  ...AUDIT_HANDLERS,
  ...COMMON_HOTEL_SETTINGS_HANDLERS,
};

const initialState = {
  offline: {
    active:            JSON.parse(localStorage.getItem('offlineMode')).active || false,
    days_before_today: null,
    days_after_today:  null,
  },

  logo: {
    model:        null,
    cropShow:     false,
    file:         null,
    progress:     0,
    progressShow: false,
    loadCanceled: false,
  },

  hotel: {
    name:       '',
    name_ar:    '',
    validation: null,
    error:      null,
    success:    null,
    loading:    true,
  },

  address: {
    country_input: { value: '', label: '' },

    real: {
      country:   '',
      address_1: '',
      address_2: '',
      city:      '',
      state:     '',
    },

    model: {
      country:   '',
      address_1: '',
      address_2: '',
      city:      '',
      state:     '',
    },

    validation: null,
  },

  checkTime: {
    real: {
    },

    model: {
    },

    validation:        null,
    errorPopupShow:    false,
    errorPopupMessage: '',
  },

  nightAudit: {
    real: JSON.parse(localStorage.getItem('nightAuditSettings')) || {
      night_audit_time:                  '5:00',
      night_audit_confirmation_required: false,
    },

    model: JSON.parse(localStorage.getItem('nightAuditSettings')) || {
      night_audit_time:                  '5:00',
      night_audit_confirmation_required: false,
    },

    validation:        null,
    errorPopupShow:    false,
    errorPopupMessage: '',
  },

  currentAudit: JSON.parse(localStorage.getItem('currentAudit')),

  loading: false,
  success: null,
  error:   null,
};

const hotelSettings = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default hotelSettings;
