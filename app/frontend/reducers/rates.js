import RATES_ACTION_HANDLERS from './handlers/rates';

const ACTION_HANDLERS = { ...RATES_ACTION_HANDLERS };
const initialState = {
  list:            [],
  error:           null,
  loading:         false,
  success:         false,
  standart_period: {},
};

const rates = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default rates;
