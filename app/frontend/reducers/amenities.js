import AMENITIES_ACTION_HANDLERS from './handlers/amenities';

const ACTION_HANDLERS = { ...AMENITIES_ACTION_HANDLERS };
const initialState = {
  list: [],
  isOffline: false,
  loading: false,
  success: null,
  error: null
};

const amenities = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default amenities;
