import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Error from '../../components/notifications/Error';
import { readError as readErrorAction } from '../../actions/messageLogger';


const ErrorModal = (props) => {

  const { activeError = null, readError } = props;

  return (
    <div
      className={cx('error-popup modal fade', { 'show d-block': activeError !== null })}
      tabIndex="-1"
      role="dialog"
      aria-hidden="true"
    >
      <Error onSubmit={readError}>
        {activeError && activeError.error && activeError.error.message}
      </Error>
    </div>
  );
};


const mapStateToProps = state => ({
  activeError: state.message_logger.activeError,
});
const mapDispatchToProps = dispatch => ({
  readError: () => { dispatch(readErrorAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModal);
