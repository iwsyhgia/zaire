import React from 'react';
// import I18n from 'i18n'; // eslint-disable-line
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Router as BrowserRouter } from 'react-router-dom';
import moment from 'moment';
import '../utils/customMoment';
import { ToastContainer, ToastStore } from 'react-toasts';
import AppHeader  from '../views/AppHeader/AppHeader';
import AppSidebar from '../views/AppSidebar/AppSidebar';
import AppContent from './AppContent';
import Audit from './Audit';
import ErrorModal from './ErrorModal';
import history from '../utils/history';
import { setLocale as setLocaleAction  } from '../actions/commonActions';
import {
  registerServiceWorker,
  unregisterServiceWorker,
  updateServiceWorkerStatus,
} from '../service-worker/serviceWorkerCompanion';
import '../styles/application.scss';

class App extends React.Component {

  static propTypes = {
    isOfflineModeActivated: PropTypes.bool.isRequired,
    currentUser:            PropTypes.instanceOf(Object).isRequired,
    isOffline:              PropTypes.bool.isRequired,
    locale:                 PropTypes.string.isRequired,
    setLocale:              PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.mainCssLink = document.getElementById('main-css');
    const rtlLink = document.getElementById('second-css');

    // save href to switch
    this.ltrCssHref = this.mainCssLink ? this.mainCssLink.href : null;
    this.rtlCssHref = rtlLink ? rtlLink.href : null;

    // remove second css link
    rtlLink.parentNode.removeChild(rtlLink);
  }

  componentDidMount() {
    this.setCurrentLocale();
    const { isOfflineModeActivated } = this.props;

    if (isOfflineModeActivated) {
      registerServiceWorker();
    } else {
      unregisterServiceWorker(); // unregister service worker when user switch OFF offline mode in hotel settings
    }
  }

  componentDidUpdate() {
    const { isOfflineModeActivated } = this.props;
    if (isOfflineModeActivated) {
      updateServiceWorkerStatus();
    }
  }

  setCurrentLocale = () => {
    const compProps = this.props;
    const locale = window.location.pathname.indexOf('/ar') === 0 ? 'ar' : 'en';
    compProps.setLocale(locale);

    if (locale === 'ar') {
      moment.locale('ar-en');
      this.loadCss('rtl');
    } else {
      moment.locale('en');
      this.loadCss('ltr');
    }

    window.localStorage.setItem('locale', locale);
  }

  loadCss = (mode) => {

    if (this.mainCssLink) {

      if (mode === 'ltr' && this.ltrCssHref) {
        this.mainCssLink.href = this.ltrCssHref;
      }

      if (mode === 'rtl' && this.rtlCssHref) {
        this.mainCssLink.href = this.rtlCssHref;
      }
    }

  }

  render() {
    const { locale, setLocale, isOffline, currentUser } = this.props;

    return (
      <BrowserRouter history={history}>
        <div className="zr-layout" dir={(locale === 'ar') ? 'rtl' : 'ltr'}>
          <ToastContainer store={ToastStore} position={ToastContainer.POSITION.TOP_RIGHT} />
          <AppSidebar />
          <div
            className="zr-overlay"
            role="presentation"
            onClick={() => document.querySelector('.zr-sidebar').classList.toggle('opened')}
          />
          <div className="zr-page">
            <AppHeader
              currentUser={currentUser}
              isOffline={isOffline}
              locale={locale}
              setLocale={setLocale}
              loadCss={this.loadCss}
            />
            <AppContent />
          </div>
          <Audit />
          <ErrorModal />
        </div>
      </BrowserRouter>
    );
  }

}


const mapStateToProps = state => (
  {
    ...state.common,
    isOfflineModeActivated: state.hotel_settings.offline.active,
  }
);

const mapDispatchToProps = dispatch => ({
  setLocale: (locale) => {
    dispatch(setLocaleAction(locale));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
