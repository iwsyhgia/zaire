import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Label from '../../components/audit/Label';
import SidePanel from '../../components/audit/SidePanel';
import { pushAudit as pushAuditAction } from '../../actions/audit';


const Panel = (props) => {

  const {
    isReadyToStart,
    isPostponed,
    pushAudit,
    locale,
  } = props;

  return (
    <Fragment>
      {
        isReadyToStart
          ? (
            <div className={cx('zr-night-audit', { 'zr-night-audit--is-postponed': isPostponed })}>
              <SidePanel locale={locale} />
              <Label onClick={() => pushAudit()} />
            </div>
          )
          : null
      }
    </Fragment>
  );
};

const mapStateToProps = state => ({
  isReadyToStart: state.audit.isReadyToStart,
  isPostponed:    state.audit.isPostponed,
  locale:         state.common.locale,
});

const mapDispatchToProps = dispatch => ({
  pushAudit: () => { dispatch(pushAuditAction()); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Panel);
