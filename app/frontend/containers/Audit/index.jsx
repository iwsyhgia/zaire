import React from 'react';
import { connect } from 'react-redux';
import { ActionCableProvider, ActionCableConsumer } from 'react-actioncable-provider';
import Panel from './Panel';
import { auditChannel } from '../../constants/constants';
import {
  initAudit as initAuditAction,
  completeAudit as completeAuditAction,
  failAudit as failAuditAction,
} from '../../actions/audit';


const Audit = (props) => {

  const { initAudit, completeAudit, failAudit } = props;

  const handleReceivedAuditStatus = (response) => {
    const { kind, confrimation_id: id, message, audit_date: auditDate } = response;

    switch (kind) {
      case 'audit_initiated':
        initAudit(id, auditDate);
        break;
      case 'audit_completed':
        setTimeout(() => {
          completeAudit(message);
        }, 1500);
        break;
      case 'audit_failed':
        setTimeout(() => {
          failAudit(message);
        }, 1500);
        break;
      case 'reservation_updated':
        break;
      default:
        break;
    }
  };

  return (
    <ActionCableProvider>
      <ActionCableConsumer
        channel={auditChannel}
        onInitialized={() => {}}
        onConnected={() => {}}
        onDisconnected={() => {}}
        onReceived={handleReceivedAuditStatus}
        onRejected={() => {}}
      />
      <Panel />
    </ActionCableProvider>
  );
};


const mapDispatchToProps = dispatch => ({
  initAudit:     (id, date) => { dispatch(initAuditAction(id, date)); },
  completeAudit: (message) => { dispatch(completeAuditAction(message)); },
  failAudit:     (message) => { dispatch(failAuditAction(message)); },
});

export default connect(null, mapDispatchToProps)(Audit);
