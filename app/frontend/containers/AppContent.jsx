import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import Stub from '../components/Stub';
import getAppRoutes from './routes';
import Home from '../components/Home';
import {
  initAudit as initAuditAction,
  pushAudit as pushAuditAction,
  getLastCompletedAuditAction,
} from '../actions/audit';

import '../styles/application.scss';

class AppContent extends React.Component {

  static propTypes = {
    history:               PropTypes.instanceOf(Object).isRequired,
    isPostponed:           PropTypes.bool.isRequired,
    initAudit:             PropTypes.func.isRequired,
    pushAudit:             PropTypes.func.isRequired,
    getLastCompletedAudit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const { getLastCompletedAudit } = props;
    getLastCompletedAudit();

    props.history.listen(() => {

      const { isPostponed, pushAudit } = this.props;
      if (isPostponed) {
        pushAudit();
      }
    });

  }

  componentDidMount() {
    const audit = JSON.parse(window.localStorage.getItem('currentAudit')) || {};
    const { status, confirmation_id: id, time_frame_begin: auditDate } = audit;
    if (status === 'waiting_for_confirmation') {
      const { initAudit } = this.props;
      initAudit(id, auditDate);
    }
  }

  render() {

    return (
      <main role="main" className="zr-main">
        <section className="main-content zr-main-content">
          <Switch>
            <Route exact path="/" component={Home} />
            {getAppRoutes()}
            <Route component={Stub} />
          </Switch>
        </section>
      </main>
    );
  }

}

const mapStateToProps = state => ({
  isPostponed: state.audit.isPostponed,
  locale:      state.common.locale,
});
const mapDispatchToProps = dispatch => ({
  initAudit:             (id, date) => { dispatch(initAuditAction(id, date)); },
  pushAudit:             () => { dispatch(pushAuditAction()); },
  getLastCompletedAudit: () => { dispatch(getLastCompletedAuditAction()); },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContent));
