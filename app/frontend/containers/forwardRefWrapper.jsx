import React from 'react';

function forwardRefWrapper(Component) {

  class ForwardRefWrapper extends React.Component {

    render() {
      const { forwardedRef, ...rest } = this.props;

      // Assign the custom prop "forwardedRef" as a ref
      return <Component ref={forwardedRef} {...rest} />;
    }

  }

  // Note the second param "ref" provided by React.forwardRef.
  // We can pass it along to ForwardRefWrapper as a regular prop, e.g. "forwardedRef"
  // And it can then be attached to the Component.
  // eslint-disable-next-line
  return React.forwardRef((props, ref) => <ForwardRefWrapper {...props} forwardedRef={ref} />);
}

export default forwardRefWrapper;
