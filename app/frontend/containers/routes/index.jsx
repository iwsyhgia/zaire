import React from 'react';
import AuthRoute from '../../components/AuthRoute';
import ReservationManagement from './reservations/ReservationManagement';
import RoomManagement from './room_types/RoomManagement';
import OfflineMode from '../../components/hotel/OfflineMode';
import CheckTime from '../../components/hotel/CheckTime';
import NightAudit from '../../components/hotel/NightAudit';
import NameAndLogo from '../../components/hotel/NameAndLogo';
import Profile from '../../components/profile/Profile';
import RatesManagement from './rates/RatesManagement';

const locales = ['en', 'ar', ''];
const routes = [
  {
    path:      '/reservations',
    component: ReservationManagement,
  },
  {
    path:      '/room_types',
    component: RoomManagement,
  },
  {
    path:      '/hotel_settings/offline',
    component: OfflineMode,
  },
  {
    path:      '/hotel_settings/checkin',
    component: CheckTime,
  },
  {
    path:      '/hotel_settings/night_audit',
    component: NightAudit,
  },
  {
    path:      '/hotel_settings/name_logo',
    component: NameAndLogo,
  },
  {
    path:      '/account',
    component: Profile,
  },
  {
    path:      '/rates',
    component: RatesManagement,
  }
];

const getAppRoutes = () => (
  routes.reduce((result, item) => {
    const routeMap = locales.map((locale) => {
      const currentPath = (locale) ? `/${locale}${item.path}` : item.path;
      return <AuthRoute key={currentPath} path={currentPath} component={item.component} />;
    });
    return [...result, ...routeMap];
  }, [])
);

export default getAppRoutes;
