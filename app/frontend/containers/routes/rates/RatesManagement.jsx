import React from 'react';
import { Route } from 'react-router-dom';
import Rates from '../../../components/rates/Rates';


const RatesManagement = ({ match }) => (
  <Route exact path={match.path} component={Rates} />
);

export default (RatesManagement);
