import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Dashboard from '../../../components/reservation/Dashboard';
import Details from '../../../components/reservation/Details';


const ReservationManagement = ({ match }) => (
  <Fragment>
    <Route path={`${match.path}/:id`} component={Details} />
    <Route exact path={`${match.path}`} component={Dashboard} />
  </Fragment>
);

export default (ReservationManagement);
