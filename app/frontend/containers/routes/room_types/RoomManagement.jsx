import React       from 'react';
import { Route } from 'react-router-dom';
import i18n from 'i18next';
import { PageBlock } from '../../Wrappers';
import AddRoomType from '../../../components/room_types/AddRoomType';
import EditRoomType from '../../../components/room_types/EditRoomType';
import ViewRoomTypes from '../../../components/room_types/ViewRoomTypes';


const RoomManagement = ({ match }) => (
  <PageBlock title={i18n.t('rooms.room_management')}>
    <Route path={`${match.path}/add`} component={AddRoomType} />
    <Route path={`${match.path}/edit/:id`} component={EditRoomType} />
    <Route exact path={match.path} component={ViewRoomTypes} />
  </PageBlock>
);

export default RoomManagement;
