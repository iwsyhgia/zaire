export const registerServiceWorker = () => {
  if (navigator.serviceWorker) {
    console.log('START REGISTERING SERVICE WORKER!'); // eslint-disable-line no-console
    navigator.serviceWorker
      .register('/serviceworker.js', { scope: '/' })
      .then((registration) => {
        registration.addEventListener('updatefound', () => {
          const newWorker = registration.installing || registration.waiting || registration.active;

          // "installing" - the install event has fired, but not yet complete
          // "installed"  - install complete
          // "activating" - the activate event has fired, but not yet complete
          // "activated"  - fully active
          // "redundant"  - discarded. Either failed install, or it's been
          //                replaced by a newer version

          newWorker.addEventListener('statechange', () => {
            const offlineDataLoaded = localStorage.getItem('offline-storage');
            const color = (newWorker.state === 'activated' && offlineDataLoaded) ? 'green' : 'white';
            document.getElementsByClassName('zr-sidebar-logo-short')[0].style.color = color;
          });
        });

        console.log('[Page] Service worker registered!', registration); // eslint-disable-line no-console
      }).catch((err) => {
        console.log('Service Worker Failed to Register', err); // eslint-disable-line no-console
      });
  }

};

export const unregisterServiceWorker = () => {
  if (navigator.serviceWorker) {
    navigator.serviceWorker
      .getRegistrations()
      .then((registrations) => {
        const serviceWorkerInstance = registrations.find(registration => (
          registration.scope === `${window.location.origin}/`
        ));

        if (serviceWorkerInstance) {
          serviceWorkerInstance.unregister().then(() => {
            document.getElementsByClassName('zr-sidebar-logo-short')[0].style.color = 'white';
            localStorage.removeItem('offline-storage');

            caches.keys().then(cacheNames => (
              Promise.all(
                cacheNames.map(cacheName => (
                  cacheName.startsWith('service-worker-cache') ? caches.delete(cacheName) : true
                ))
              )
            ));

          });
        }
      }).catch((err) => {
        console.log('Service Worker Failed to Unregister', err); // eslint-disable-line no-console
      });
  }
};

export const updateServiceWorkerStatus = () => {
  if (navigator.serviceWorker) {
    navigator.serviceWorker
      .getRegistrations()
      .then((registrations) => {
        const serviceWorkerInstance = registrations.find(registration => (
          registration.scope === `${window.location.origin}/`
        ));

        if (serviceWorkerInstance) {
          const offlineDataLoaded = localStorage.getItem('offline-storage');
          const color = (serviceWorkerInstance.active && offlineDataLoaded) ? 'green' : 'white';
          document.getElementsByClassName('zr-sidebar-logo-short')[0].style.color = color;
        }
        console.log('updatedServiceWorkerStatus: ', serviceWorkerInstance); // eslint-disable-line no-console
      });
  }
};

export const showServiceWorkersStatus = () => {
  if (navigator.serviceWorker) {
    navigator.serviceWorker.getRegistrations()
      .then((registrations) => {
        registrations.forEach((registration) => {
          const installing = registration.installing;
          const active     = registration.active;
          const waiting    = registration.waiting;
          const data       = `
            <tr>
              <td> ${registration.scope} </td>
              <td> ${(registration.installing || registration.wating || registration.active).state} </td>
              <td>
                <p>
                  <b>Installing</b> -
                    script: ${installing ? installing.scriptURL : ''}, -
                    state: ${installing ? installing.state : ''}
                </p>
                <p>
                  <b>Waiting</b> -
                    script: ${waiting ? waiting.scriptURL : ''}, -
                    state: ${waiting ? waiting.state : ''}
                </p>
                <p>
                  <b>Active</b> -
                    script: ${active ? active.scriptURL : ''}, -
                    state: ${active ? active.state : ''}
                </p>
              </td>
            </td>
          `;
          document.getElementById('serviceworkers-table-body').insertAdjacentHTML('beforeend', data);
        });
      });
  }
};

export const showCacheStorageStatus = () => {
  caches.keys().then((cacheNames) => {
    cacheNames.forEach((cacheName) => {
      caches.open(cacheName).then((cache) => {
        cache.keys().then((keys) => {
          const tds = keys.map(key => (`${key.url}</br>`));
          const data = `
            <tr>
              <td> ${cacheName} </td>
              <td> ${tds.join('')} </td>
            </td>
          `;
          document.getElementById('cache-storage-table-body').insertAdjacentHTML('beforeend', data);
        });
      });
    });
  });
};
