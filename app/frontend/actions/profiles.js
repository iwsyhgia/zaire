import { ToastStore } from 'react-toasts';
import i18n from 'i18next';
import {
  AJAX,
  GET_PROFILE,
  UPDATE_PROFILE,
  STORE_PROFILE,
  STORE_PROFILE_FORM,
  TOGGLE_PROFILE_FIELD,
  RESET_FORM,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';

export const API_PROFILE = `${WEB_API}profiles`;

export const resetProfileFormAction = () => ({ type: RESET_FORM });
export const storeProfileAction     = data => ({ payload: { ...data }, type: STORE_PROFILE });
export const storeProfileFormAction = data => ({ payload: { ...data }, type: STORE_PROFILE_FORM });
export const toggleProfileFieldAction = name => ({ payload: name, type: TOGGLE_PROFILE_FIELD });

export const getProfileAction = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    API_PROFILE,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_PROFILE,
    },
  });
};

export const updateProfileAction = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    API_PROFILE,
      method: 'PUT',
      meta:   {
        log: ['error', 'success'],
      },
      data,
      ...UPDATE_PROFILE,
    },
  }).then(() => {
    ToastStore.success(i18n.t('profile.updated_successfuly')); // TODO: remove Toast
  });
};
