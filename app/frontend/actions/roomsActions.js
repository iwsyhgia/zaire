import {
  AJAX,
  SET_ROOMS,
  SET_ROOM,
  SET_ROOM_TYPES,
  SET_ROOM_TYPE,
  SET_AMENITIES,
  GET_ROOMS,
  GET_AVAILABLE_ROOMS,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';

const API_ROOMS = `${WEB_API}rooms`;

export const setRooms = response => ({
  type:    SET_ROOMS,
  payload: { response },
});

export const setRoom = response => ({
  type:    SET_ROOM,
  payload: { response },
});

export const setRoomTypes = response => ({
  type:    SET_ROOM_TYPES,
  payload: { response },
});

export const setRoomType = response => ({
  type:    SET_ROOM_TYPE,
  payload: { response },
});

export const setAmenities = response => ({
  type:    SET_AMENITIES,
  payload: { response },
});

export const getRooms = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_ROOMS}/rooms`,
      ...GET_ROOMS,
    },
  });
};

export const getAvailableRooms = (from, to) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_ROOMS}/available_rooms?check_in_date=${from}&check_out_date=${to}`,
      ...GET_AVAILABLE_ROOMS,
    },
  });
};
