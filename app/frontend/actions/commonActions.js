import {
  AJAX,
  SET_LOCALE,
  SET_OFFLINE_STATE,
  SET_ONLINE_STATE,
  GET_CURRENT_USER,
  GET_COUNTRIES,
  GET_GUESTS,
  asyncAction,
  GET_AUDIT_INFO,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';

const GET_AUDIT_INFO_URL = `${WEB_API}audits/audits/last`;

export const setLocale = locale => ({
  type:    SET_LOCALE,
  payload: locale,
});

export const setOfflineState = offlineData => ({
  type:    SET_OFFLINE_STATE,
  payload: offlineData,
});

export const setOnlineState = () => ({
  type:    SET_ONLINE_STATE,
  payload: { isOffline: false },
});

export const getCurrentUser = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      method: 'GET',
      url:    '/users/sign_in',
      ...asyncAction(GET_CURRENT_USER),
    },
  });
};

export const getCountries = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      method: 'GET',
      url:    `${WEB_API}countries`,
      ...GET_COUNTRIES,
    },
  });
};

export const getGuests = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}guests`,
      method: 'GET',
      ...GET_GUESTS,
    },
  });
};

export const getAuditInfo = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    GET_AUDIT_INFO_URL,
      method: 'GET',
      ...GET_AUDIT_INFO,
    },
  });
};
