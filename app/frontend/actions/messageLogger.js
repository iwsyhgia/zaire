import { READ_ERROR_MESSAGE, SHOW_ERROR_MESSAGE } from '../constants/actionConstants';


export const readError = () => ({
  type: READ_ERROR_MESSAGE,
});

export const showError = message => ({
  type:    SHOW_ERROR_MESSAGE,
  payload: {
    error: {
      message,
    },
  },
});
