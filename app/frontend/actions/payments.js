import {
  AJAX,
  ADD_DISCOUNT,
  EDIT_DISCOUNT,
  DELETE_DISCOUNT,
  ADD_PAYMENT,
  EDIT_PAYMENT,
  DELETE_PAYMENT,
  ADD_REFUND,
  EDIT_REFUND,
  DELETE_REFUND,
  ADD_CHARGE,
  GET_GENERAL_PAYMENTS,
  GET_RESERVATION_BY_ID,
  GET_INVOICE,
  RESET_STATE,
} from '../constants/actionConstants';


import { WEB_API } from '../constants/constants';
import { forceFrontdeskUpdate } from '../utils/reservation/frontdesk';

const API_INVOICES = `${WEB_API}invoices`;

export const addDiscount = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/discounts`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_DISCOUNT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const deleteDiscount = (id, { invoiceId, reservationId }) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${invoiceId}/discounts/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      ...DELETE_DISCOUNT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${reservationId}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const editDiscount = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/discounts/${data.id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...EDIT_DISCOUNT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const addPayment = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/payments`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_PAYMENT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    }).then((response) => {
      forceFrontdeskUpdate(response);
    });
  });
};

export const deletePayment = (id, { invoiceId, reservationId }) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${invoiceId}/payments/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      ...DELETE_PAYMENT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${reservationId}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    }).then((response) => {
      forceFrontdeskUpdate(response);
    });
  });
};

export const editPayment = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/payments/${data.id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...EDIT_PAYMENT,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    }).then((response) => {
      forceFrontdeskUpdate(response);
    });
  });
};

export const addRefund = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/refunds`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_REFUND,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const deleteRefund = (id, { invoiceId, reservationId }) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${invoiceId}/refunds/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      ...DELETE_REFUND,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${reservationId}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const editRefund = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/refunds/${data.id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...EDIT_REFUND,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}reservations/${data.reservation_id}`,
        method: 'GET',
        ...GET_RESERVATION_BY_ID,
      },
    });
  });
};

export const addCharge = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${data.invoiceId}/charges`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_CHARGE,
    },
  }).then(() => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${API_INVOICES}/${data.invoiceId}`,
        method: 'GET',
        ...GET_GENERAL_PAYMENTS,
      },
    });
  });
};

export const getGeneralPayments = invoiceId => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_INVOICES}/${invoiceId}`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      data: {
        id: invoiceId,
      },
      ...GET_GENERAL_PAYMENTS,
    },
  });
};

export const getInvoice = (invoices, id) => ({
  type:    GET_INVOICE,
  payload: {
    paymentById: invoices.filter(invoice => invoice.id === id)[0],
  },
});

export const resetPaymentsState = () => ({
  type:    RESET_STATE,
  payload: {},
});
