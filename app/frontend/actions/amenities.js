import {
  AJAX,
  GET_AMENITIES,
  ADD_AMENITY,
  DELETE_AMENITY,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';


// Amenities
export const getAmenities = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:  `${WEB_API}rooms/amenities`,
      meta: {
        log: ['error'],
      },
      ...GET_AMENITIES,
    },
  });
};

export const addAmenity = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/amenities`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_AMENITY,
    },
  });
};

export const deleteAmenity = (id, data) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/amenities/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      ...DELETE_AMENITY,
    },
  });
};
