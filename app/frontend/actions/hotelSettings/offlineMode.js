import {
  AJAX,
  GET_OFFLINE_MODE,
  UPDATE_OFFLINE_MODE,
} from '../../constants/actionConstants';
import { WEB_API } from '../../constants/constants';


const API_HOTEL = `${WEB_API}hotels`;

export const getOfflineMode = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_HOTEL}/offline_mode`,
      ...GET_OFFLINE_MODE,
    },
  });
};

export const updateOfflineMode = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/offline_mode`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_OFFLINE_MODE,
    },
  });
};
