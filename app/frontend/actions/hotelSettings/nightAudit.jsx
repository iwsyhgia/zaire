import { ToastStore } from 'react-toasts';
import i18n from 'i18next';
import {
  AJAX,
  GET_AUDIT_TIME,
  STORE_AUDIT_TIME,
  UPDATE_AUDIT_TIME,
} from '../../constants/actionConstants';
import { WEB_API } from '../../constants/constants';


const API_HOTEL = `${WEB_API}hotels`;

const auditTimeSuccess = () => (
  ToastStore.success(i18n.t('hotel.audit_time_saved'))
);

export const getAuditTime = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_HOTEL}/configuration`,
      ...GET_AUDIT_TIME,
    },
  });
};

export const updateAuditTime = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/configuration`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_AUDIT_TIME,
    },
  }).then(() => auditTimeSuccess());
};

export const storeAuditTime = data => ({
  type:    STORE_AUDIT_TIME,
  payload: { ...data },
});
