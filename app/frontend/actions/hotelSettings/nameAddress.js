import { ToastStore } from 'react-toasts';
import i18n from 'i18next';
import {
  AJAX,
  GET_HOTEL,
  UPDATE_HOTEL,
  GET_ADDRESS,
  UPDATE_ADDRESS,
  STORE_HOTEL,
  STORE_ADDRESS,
  STORE_HOTEL_SETTINGS,
} from '../../constants/actionConstants';
import { WEB_API } from '../../constants/constants';

const API_HOTEL = `${WEB_API}hotels`;

export const getHotel = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${WEB_API}/hotel`,
      ...GET_HOTEL,
    },
  });
};

export const updateHotel = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}/hotel`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_HOTEL,
    },
  }).then(() => ToastStore.success(i18n.t('hotel.name_success')));
};

export const storeHotel = data => ({
  type:    STORE_HOTEL,
  payload: { ...data },
});

export const getAddress = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_HOTEL}/address`,
      ...GET_ADDRESS,
    },
  });
};

export const updateAddress = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/address`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_ADDRESS,
    },
  }).then(() => ToastStore.success(i18n.t('hotel.address_success')));
};

export const storeAddress = data => ({
  type:    STORE_ADDRESS,
  payload: { ...data },
});

export const storeHotelSettings = data => ({
  type:    STORE_HOTEL_SETTINGS,
  payload: { ...data },
});
