import { ToastStore } from 'react-toasts';
import i18n from 'i18next';
import {
  AJAX,
  GET_CHECK_TIME,
  STORE_CHECK_TIME,
  UPDATE_CHECK_TIME,
} from '../../constants/actionConstants';
import { WEB_API } from '../../constants/constants';


const API_HOTEL = `${WEB_API}hotels`;

const checkTimeSuccess = () => {
  ToastStore.success(i18n.t('hotel.check_time_saved'));
};

export const getCheckTime = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_HOTEL}/check_time`,
      ...GET_CHECK_TIME,
    },
  });
};

export const updateCheckTime = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/check_time`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_CHECK_TIME,
    },
  }).then(() => checkTimeSuccess());
};

export const storeCheckTime = data => ({
  type:    STORE_CHECK_TIME,
  payload: { ...data },
});
