import { ToastStore } from 'react-toasts';
import i18n from 'i18next';
import {
  AJAX,
  GET_LOGO,
  UPDATE_LOGO,
  CREATE_LOGO,
  DELETE_LOGO,
  STORE_LOGO,
} from '../../constants/actionConstants';
import { WEB_API } from '../../constants/constants';


const API_HOTEL = `${WEB_API}hotels`;

export const storeLogo = data => ({
  type:    STORE_LOGO,
  payload: { ...data },
});

export const getLogo = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url: `${API_HOTEL}/logo`,
      ...GET_LOGO,
    },
  }).catch(e => e);
};

const logoSuccess = () => {
  ToastStore.success(i18n.t('hotel.logo_load_success'));
};

export const createLogo = (data, config) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/logo`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      config,
      ...CREATE_LOGO,
    },
  }).then(logoSuccess); // TODO: remove toast
};

export const updateLogo = (data, config) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/logo`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      config,
      ...UPDATE_LOGO,
    },
  }).then(logoSuccess); // TODO: remove toast
};

export const deleteLogo = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_HOTEL}/logo`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      ...DELETE_LOGO,
    },
  }).then(() => ToastStore.success(i18n.t('hotel.logo_destroyed'))); // TODO: remove toast
};
