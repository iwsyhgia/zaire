import {
  AJAX,
  GET_ROOM_TYPES,
  GET_ROOM_TYPE,
  ADD_ROOM_TYPE,
  LOAD_ROOM_TYPE,
  UPDATE_ROOM_TYPE,
  DELETE_ROOM_TYPE,
  UPDATE_ROOM,
  DELETE_ROOM,
  UPDATE_AMENITIES,
  UPDATE_IMAGES,
  UPDATE_ROOMS,
  RESET_STATE,
  RESET_ERROR,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';

const API_ROOM_TYPE = `${WEB_API}rooms/room_types`;

export const getRoomTypes = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    API_ROOM_TYPE,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_ROOM_TYPES,
    },
  });
};

export const addRoomType = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    API_ROOM_TYPE,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_ROOM_TYPE,
    },
  });
};

export const updateRoomType = (id, data) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/room_types/${id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      ...UPDATE_ROOM_TYPE,
    },
  });
};

export const deleteRoomType = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/room_types/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      id,
      data: { room_type_id: id },
      ...DELETE_ROOM_TYPE,
    },
  });
};

export const loadRoomType = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/room_types/${+id}`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...LOAD_ROOM_TYPE,
    },
  });
};

export const updateRoom = (id, data) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/rooms/${id}`,
      method: 'PUT',
      data,
      id,
      ...UPDATE_ROOM,
    },
  });
};

export const deleteRoom = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}rooms/rooms/${id}`,
      method: 'DELETE',
      id,
      ...DELETE_ROOM,
    },
  });
};

// simple actions
export const getRoomType = id => ({
  type:    GET_ROOM_TYPE,
  payload: {
    room_type_id: +id,
  },
});

export const updateAmenities = data => ({
  type:    UPDATE_AMENITIES,
  payload: {
    amenities: data,
  },
});

export const updateImages = data => ({
  type:    UPDATE_IMAGES,
  payload: {
    images: data,
  },
});

export const updateRooms = data => ({
  type:    UPDATE_ROOMS,
  payload: {
    rooms: data,
  },
});

// TODO: Move to ?
export const resetRoomTypeState = () => ({
  type:    RESET_STATE,
  payload: {
    success: null,
    deleted: null,
  },
});

export const resetErrors = () => ({
  type: RESET_ERROR,
});
