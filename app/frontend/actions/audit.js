import {
  AJAX,
  INIT_AUDIT,
  RUN_AUDIT,
  COMPLETE_AUDIT,
  FAIL_AUDIT,
  POSTPONE_AUDIT,
  PUSH_AUDIT,
  RESET_AUDIT_STATE,
  GET_LAST_COMPLETED_AUDIT,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';

export const initAudit = (id, date) => ({
  type:    INIT_AUDIT,
  payload: {
    id,
    date,
  },
});

export const runAudit = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}audits/confirmations/${id}/confirm`,
      method: 'POST',
      ...RUN_AUDIT,
    },
  });
};

export const completeAudit = msg => ({
  type:    COMPLETE_AUDIT,
  payload: {
    message: msg,
  },
});

export const failAudit = msg => ({
  type:    FAIL_AUDIT,
  payload: {
    message: msg,
  },
});

export const postponeAudit = () => ({
  type: POSTPONE_AUDIT,
});

export const pushAudit = () => ({
  type: PUSH_AUDIT,
});

export const auditPostRun = () => ({
  type: RESET_AUDIT_STATE,
});

export const getLastCompletedAuditAction = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${WEB_API}audits/audits/last_completed`,
      method: 'GET',
      ...GET_LAST_COMPLETED_AUDIT,
    },
  });
};
