import {
  AJAX,
  GET_PERIODS_LIST,
  GET_SPECIAL_PERIOD,
  GET_STANDART_PERIOD,
  UPDATE_PERIOD,
} from '../constants/actionConstants';


import { WEB_API } from '../constants/constants';


const API_RATES = `${WEB_API}rates/periods`;

export const getStandartPeriod = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RATES}/standard`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_STANDART_PERIOD,
    },
  });
};

export const getSpecialPeriod = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RATES}/periods/${id}`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_SPECIAL_PERIOD,
    },
  });
};

export const getPeriods = () => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    API_RATES,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_PERIODS_LIST,
    },
  });
};

export const updatePeriod = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RATES}/${data.period_id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      ...UPDATE_PERIOD,
    },
  });
};
