import {
  AJAX,
  GET_RESERVATIONS,
  ADD_RESERVATION,
  UPDATE_RESERVATION,
  DELETE_RESERVATION,
  CANCEL_RESERVATION,
  GET_RESERVATION_BY_ID,
  UPDATE_RESERVATIONS_LIST,
  GET_RESERVATION,
  GET_DNR_RESERVATIONS,
  GET_DNR_RESERVATION,
  ADD_DNR_RESERVATION,
  UPDATE_DNR_RESERVATION,
  DELETE_DNR_RESERVATION,
  CHEKIN_RESERVATION,
  CHEKOUT_RESERVATION,
  SHOW_RESERVATION_POPUP,
  CHANGE_RESERVATION_DATE_RANGE,
  ADD_ADULT,
  RESET_STATE,
  RESET_CURRENT,
  RESET_FORM,
  GET_GENERAL_PAYMENTS,
} from '../constants/actionConstants';

import { WEB_API } from '../constants/constants';
import { forceFrontdeskUpdate } from '../utils/reservation/frontdesk';

const API_RESERVATIONS = `${WEB_API}reservations`;

export const getReservations = (from, to) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}?q[from_date]=${from}&q[to_date]=${to}`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      ...GET_RESERVATIONS,
    },
  });
};

export const addReservation = (data, readyToCheckin = false) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      readyToCheckin,
      data,
      ...ADD_RESERVATION,
    },
  });
};

export const updateReservation = (id, data, readyToCheckin = false) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      readyToCheckin,
      ...UPDATE_RESERVATION,
    },
  }).then((result) => {
    forceFrontdeskUpdate(result);
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${API_RESERVATIONS}/${+id}`,
        method: 'GET',
        meta:   {
          log: ['error'],
        },
        ...GET_RESERVATION_BY_ID,
      },
    }).then(({ response }) => {
      dispatch({
        type:    AJAX,
        payload: {
          url:    `${WEB_API}invoices/${response.invoice_id}`,
          method: 'GET',
          meta:   {
            log: ['error'],
          },
          ...GET_GENERAL_PAYMENTS,
        },
      });
    });
  });
};

export const updateReservationWithDnD = (id, data, errorCallback) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      readyToCheckin: false,
      ...UPDATE_RESERVATION,
    },
  }).then(() => {}, () => {
    errorCallback();
  });
};

export const deleteReservation = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      id,
      ...DELETE_RESERVATION,
    },
  });
};

export const getReservationByID = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${+id}`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      data: {
        id,
      },
      ...GET_RESERVATION_BY_ID,
    },
  }).then(({ response }) => {
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${WEB_API}invoices/${response.invoice_id}`,
        method: 'GET',
        meta:   {
          log: ['error'],
        },
        ...GET_GENERAL_PAYMENTS,
      },
    });
  });
};

export const getReservationDNR = (from, to) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/dnr`,
      method: 'GET',
      meta:   {
        log: ['error'],
      },
      data: {
        from_date: from,
        to_date:   to,
      },
      ...GET_DNR_RESERVATIONS,
    },
  });
};

export const addReservationDNR = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/dnr`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_DNR_RESERVATION,
    },
  });
};

export const deleteDNRReservation = (id, data) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/dnr/${id}`,
      method: 'DELETE',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      ...DELETE_DNR_RESERVATION,
    },
  });
};

export const updateDNRReservation = (id, data) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/dnr/${id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      ...UPDATE_DNR_RESERVATION,
    },
  });
};

export const updateDNRReservationWithDnD = (id, data, errorCallback) => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/dnr/${id}`,
      method: 'PUT',
      meta:   {
        log: ['error'],
      },
      data,
      id,
      ...UPDATE_DNR_RESERVATION,
    },
  }).then(() => {}, () => {
    errorCallback();
  });
};


export const checkInReservation = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${data.id}/check_in`,
      method: 'PATCH',
      meta:   {
        log: ['error'],
      },
      data,
      ...CHEKIN_RESERVATION,
    },
  }).then((result) => {
    forceFrontdeskUpdate(result);
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${API_RESERVATIONS}/${+data.id}`,
        method: 'GET',
        meta:   {
          log: ['error'],
        },
        ...GET_RESERVATION_BY_ID,
      },
    }).then(({ response }) => {
      dispatch({
        type:    AJAX,
        payload: {
          url:    `${WEB_API}invoices/${response.invoice_id}`,
          method: 'GET',
          meta:   {
            log: ['error'],
          },
          ...GET_GENERAL_PAYMENTS,
        },
      });
    });
  });
};

export const checkOutReservation = id => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${id}/check_out`,
      method: 'PATCH',
      meta:   {
        log: ['error'],
      },
      id,
      ...CHEKOUT_RESERVATION,
    },
  }).then((result) => {
    forceFrontdeskUpdate(result);
  });
};

export const cancelReservation = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/${data.id}/cancel`,
      method: 'PATCH',
      meta:   {
        log: ['error'],
      },
      data,
      ...CANCEL_RESERVATION,
    },
  }).then((result) => {
    forceFrontdeskUpdate(result);
    dispatch({
      type:    AJAX,
      payload: {
        url:    `${API_RESERVATIONS}/${+data.id}`,
        method: 'GET',
        meta:   {
          log: ['error'],
        },
        ...GET_RESERVATION_BY_ID,
      },
    }).then(({ response }) => {
      dispatch({
        type:    AJAX,
        payload: {
          url:    `${WEB_API}invoices/${response.invoice_id}`,
          method: 'GET',
          meta:   {
            log: ['error'],
          },
          ...GET_GENERAL_PAYMENTS,
        },
      });
    });
  });
};

export const updateReservationsWith = reservation => ({
  type:    UPDATE_RESERVATIONS_LIST,
  payload: {
    reservation,
  },
});

export const getDNRReservation = id => ({
  type:    GET_DNR_RESERVATION,
  payload: {
    id: +id,
  },
});

export const getReservation = id => ({
  type:    GET_RESERVATION,
  payload: {
    id: +id,
  },
});

export const showReservationPopup = data => ({
  type:    SHOW_RESERVATION_POPUP,
  payload: {
    ...data,
  },
});

export const changeDateRange = data => ({
  type:    CHANGE_RESERVATION_DATE_RANGE,
  payload: {
    ...data,
  },
});

export const addAdult = data => (dispatch) => {
  dispatch({
    type:    AJAX,
    payload: {
      url:    `${API_RESERVATIONS}/adults`,
      method: 'POST',
      meta:   {
        log: ['error'],
      },
      data,
      ...ADD_ADULT,
    },
  });
};

export const resetEventState = () => ({
  type:    RESET_STATE,
  payload: {},
});

export const resetItemState = () => ({
  type:    RESET_CURRENT,
  payload: {},
});

export const resetForm = () => ({
  type:    RESET_FORM,
  payload: {},
});
