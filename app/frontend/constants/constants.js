import i18n from 'i18next';

export const auditChannel = 'WebNotificationsChannel';

export const WEB_API = '/api/v1/';
export const API_HOTEL = `${WEB_API}hotels`;

export const AM_PM_HOURS_FORMAT = 'hh:mm A';
export const HOURS_24_FORMAT = 'HH:mm';

export const DEFAULT_DNR_STATUS = 'default_dnr';
export const CUT_DNR_STATUS = 'cut_dnr';
export const DNR_STATUSES = [DEFAULT_DNR_STATUS, CUT_DNR_STATUS];
export const CHECKED_IN = 'checked_in';
export const CHECKED_OUT = 'checked_out';
export const CONFIRMED = 'confirmed';
export const UNCONFIRMED = 'unconfirmed';
export const NOSHOW = 'no_show';
export const CANCELLED = 'cancelled';
export const REFUNDABLE = 'refundable';

export const STEPS = Object.freeze({
  CLOSE:   0,
  ADD:     1,
  EDIT:    2,
  CHECKIN: 3,
});

export const PERSON_TITLE_OPTIONS = () => ([
  { value: 'mr', label: i18n.t('common.mr') },
  { value: 'mrs', label: i18n.t('common.mrs') },
  { value: 'ms', label: i18n.t('common.ms') }
]);

export const KIND_DISCOUNT_OPTIONS = () => ([
  { value: 'percent', label: i18n.t('common.percent') },
  { value: 'amount', label: i18n.t('common.amount') }
]);

export const NITGH_AUDIT_TIME_OPTIONS = [
  { value: '03:00', label: '03:00' },
  { value: '04:00', label: '04:00' },
  { value: '05:00', label: '05:00' },
  { value: '06:00', label: '06:00' },
  { value: '07:00', label: '07:00' }
];

export const KIND_PAYMENT_OPTIONS = () => ([
  { value: 'cash', label: i18n.t('common.cash') },
  { value: 'card', label: i18n.t('common.card') }
]);

export const FULLNESS_TYPE_OPTIONS = [
  { value: 'full', label: i18n.t('common.full') },
  { value: 'partial', label: i18n.t('common.partial') },
  { value: 'one_night', label: i18n.t('common.one_night') }
];

export const MAX_OCCUPANCY_OPTIONS = () => ([
  { value: '1', label: `1 ${i18n.t('common.person')}` },
  { value: '2', label: `2 ${i18n.t('common.person')}` },
  { value: '3', label: `3 ${i18n.t('common.person')}` },
  { value: '4', label: `4 ${i18n.t('common.person')}` }
]);

export const INVOICE_TYPES = {
  DISCOUNT: 'discount',
  REFUND:   'refund',
  PAYMENT:  'payment',
  CHARGE:   'charge',
};

export const WEEK_DAYS = [
  { num: 1, short_name: 'mon', field: 'monday' },
  { num: 2, short_name: 'tue', field: 'tuesday' },
  { num: 3, short_name: 'wed', field: 'wednesday' },
  { num: 4, short_name: 'thu', field: 'thursday' },
  { num: 5, short_name: 'fri', field: 'friday' },
  { num: 6, short_name: 'sat', field: 'saturday' },
  { num: 7, short_name: 'sun', field: 'sunday' }
];

export const NO_ROOMS = 'NO_ROOMS';
export const NO_ITEMS = 'NO_ITEMS';
export const NO_PARAM = undefined;

// size in MB
export const MIN_IMAGE_SIZE = 0.001;
export const MAX_IMAGE_SIZE = 10;
export const E_KEY_NUMBER = 69;
export const MAX_SCREEN_WIDTH = 1200;


export const PHONE_FORMAT = /^[+]?[0-9]+$/;

export const FORCE_FRONTDESK_UPDATE = 'force_frontdesk_update';
