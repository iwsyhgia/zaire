export const asyncAction = type => ({
  PENDING: `${type}_PENDING`,
  SUCCESS: `${type}_SUCCESS`,
  ERROR:   `${type}_ERROR`,
  OFFLINE: `${type}_OFFLINE`,
});

// Common
export const AJAX              = 'AJAX';
export const INIT_APP          = 'INIT_APP';
export const SET_LOCALE        = 'SET_LOCALE';
export const SET_OFFLINE_STATE = 'SET_OFFLINE_STATE';
export const SET_ONLINE_STATE  = 'SET_ONLINE_STATE';
export const GET_COUNTRIES     = asyncAction('GET_COUNTRIES');

// User
export const GET_CURRENT_USER = asyncAction('GET_CURRENT_USER');

// Rooms
export const GET_ROOMS = asyncAction('GET_ROOMS');
export const GET_AVAILABLE_ROOMS = asyncAction('GET_AVAILABLE_ROOMS');

// Rates

export const GET_PERIODS_LIST = asyncAction('GET_PERIODS_LIST');
export const GET_SPECIAL_PERIOD = asyncAction('GET_SPECIAL_PERIOD');
export const GET_STANDART_PERIOD = asyncAction('GET_STANDART_PERIOD');
export const UPDATE_PERIOD = asyncAction('UPDATE_PERIOD');

// Room Types
export const SET_ROOMS      = 'SET_ROOMS';
export const SET_ROOM       = 'SET_ROOM';
export const UPDATE_ROOM    = asyncAction('UPDATE_ROOM');
export const DELETE_ROOM    = asyncAction('DELETE_ROOM');
export const SET_ROOM_TYPES = 'SET_ROOM_TYPES';
export const SET_ROOM_TYPE  = 'SET_ROOM_TYPE';
export const SET_AMENITIES  = 'SET_AMENITIES';

export const GET_ROOM_TYPES = asyncAction('GET_ROOM_TYPES');
export const ADD_ROOM_TYPE = asyncAction('ADD_ROOM_TYPE');
export const UPDATE_ROOM_TYPE = asyncAction('UPDATE_ROOM_TYPE');
export const DELETE_ROOM_TYPE = asyncAction('DELETE_ROOM_TYPE');
export const LOAD_ROOM_TYPE = asyncAction('LOAD_ROOM_TYPE');


export const GET_AMENITIES  = asyncAction('GET_AMENITIES');
export const ADD_AMENITY  = asyncAction('ADD_AMENITY');
export const DELETE_AMENITY  = asyncAction('DELETE_AMENITY');

export const GET_ROOM_TYPE = 'GET_ROOM_TYPE';

export const UPDATE_AMENITIES  = 'UPDATE_AMENITIES';
export const UPDATE_IMAGES  = 'UPDATE_IMAGES';
export const UPDATE_ROOMS  = 'UPDATE_ROOMS';

export const RESET_STATE  = 'RESET_STATE';
export const RESET_CURRENT = 'RESET_CURRENT';
export const RESET_FORM = 'RESET_FORM';
export const RESET_ERROR = 'RESET_ERRORS';

// Hotel settings
export const GET_OFFLINE_MODE = asyncAction('GET_OFFLINE_MODE');
export const UPDATE_OFFLINE_MODE = asyncAction('UPDATE_OFFLINE_MODE');

export const GET_CHECK_TIME = asyncAction('GET_CHECK_TIME');
export const UPDATE_CHECK_TIME = asyncAction('UPDATE_CHECK_TIME');
export const STORE_CHECK_TIME = 'STORE_CHECK_TIME';

export const GET_AUDIT_TIME    = asyncAction('GET_AUDIT_TIME');
export const UPDATE_AUDIT_TIME = asyncAction('UPDATE_AUDIT_TIME');
export const STORE_AUDIT_TIME  = 'STORE_AUDIT_TIME';

export const GET_LOGO = asyncAction('GET_LOGO');
export const CREATE_LOGO = asyncAction('CREATE_LOGO');
export const UPDATE_LOGO = asyncAction('UPDATE_LOGO');
export const DELETE_LOGO = asyncAction('DELETE_LOGO');
export const STORE_LOGO = 'STORE_LOGO';

export const GET_HOTEL = asyncAction('GET_HOTEL');
export const UPDATE_HOTEL = asyncAction('UPDATE_HOTEL');
export const STORE_HOTEL = 'STORE_HOTEL';

export const GET_ADDRESS = asyncAction('GET_ADDRESS');
export const UPDATE_ADDRESS = asyncAction('UPDATE_ADDRESS');
export const STORE_ADDRESS = 'STORE_ADDRESS';
export const STORE_HOTEL_SETTINGS = 'STORE_HOTEL_SETTINGS';

// Reservations
export const GET_RESERVATIONS = asyncAction('GET_RESERVATIONS');
export const ADD_RESERVATION = asyncAction('ADD_RESERVATION');
export const UPDATE_RESERVATION = asyncAction('UPDATE_RESERVATION');
export const DELETE_RESERVATION = asyncAction('DELETE_RESERVATION');
export const CANCEL_RESERVATION = asyncAction('CANCEL_RESERVATION');
export const CHEKIN_RESERVATION = asyncAction('CHEKIN_RESERVATION');
export const CHEKOUT_RESERVATION = asyncAction('CHEKOUT_RESERVATION');
export const GET_RESERVATION_BY_ID = asyncAction('GET_RESERVATION_BY_ID');
export const UPDATE_RESERVATIONS_LIST = 'UPDATE_RESERVATIONS_LIST';
export const CHANGE_RESERVATION_DATE_RANGE = 'CHANGE_RESERVATION_DATE_RANGE';
export const GET_RESERVATION = 'GET_RESERVATION';
export const SHOW_RESERVATION_POPUP = 'SHOW_RESERVATION_POPUP';
export const GET_GUESTS = asyncAction('GET_GUESTS');

export const GET_DNR_RESERVATIONS = asyncAction('GET_DNR_RESERVATIONS');
export const ADD_DNR_RESERVATION = asyncAction('ADD_DNR_RESERVATION');
export const UPDATE_DNR_RESERVATION = asyncAction('UPDATE_DNR_RESERVATION');
export const DELETE_DNR_RESERVATION = asyncAction('DELETE_DNR_RESERVATION');
export const GET_DNR_RESERVATION = asyncAction('GET_DNR_RESERVATION');
export const ADD_ADULT = asyncAction('ADD_ADULT');

// Payments
export const ADD_DISCOUNT = asyncAction('ADD_DISCOUNT');
export const EDIT_DISCOUNT = asyncAction('EDIT_DISCOUNT');
export const DELETE_DISCOUNT = asyncAction('DELETE_DISCOUNT');

export const ADD_PAYMENT = asyncAction('ADD_PAYMENT');
export const EDIT_PAYMENT = asyncAction('EDIT_PAYMENT');
export const DELETE_PAYMENT = asyncAction('DELETE_PAYMENT');

export const ADD_REFUND = asyncAction('ADD_REFUND');
export const EDIT_REFUND = asyncAction('EDIT_REFUND');
export const DELETE_REFUND = asyncAction('DELETE_REFUND');

export const GET_INVOICE = 'GET_INVOICE';

export const ADD_CHARGE = asyncAction('ADD_CHARGE');
export const GET_GENERAL_PAYMENTS = asyncAction('GET_GENERAL_PAYMENTS');

// Audit
export const INIT_AUDIT = 'INIT_AUDIT';
export const RUN_AUDIT = asyncAction('RUN_AUDIT');
export const COMPLETE_AUDIT = 'COMPLETE_AUDIT';
export const FAIL_AUDIT = 'FAIL_AUDIT';
export const POSTPONE_AUDIT = 'POSTPONE_AUDIT';
export const PUSH_AUDIT = 'PUSH_AUDIT';
export const RESET_AUDIT_STATE = 'RESET_AUDIT_STATE';
export const GET_AUDIT_INFO = asyncAction('GET_AUDIT_INFO');
export const GET_LAST_COMPLETED_AUDIT = asyncAction('GET_LAST_COMPLETED_AUDIT');

// Profiles
export const GET_PROFILE = asyncAction('GET_PROFILE');
export const UPDATE_PROFILE = asyncAction('UPDATE_PROFILE');
export const STORE_PROFILE_FORM = 'STORE_PROFILE_FORM';
export const STORE_PROFILE = 'STORE_PROFILE';
export const TOGGLE_PROFILE_FIELD = 'TOGGLE_PROFILE_FIELD';

// Message logger
export const LOG_ERROR = 'LOG_ERROR';
export const READ_ERROR_MESSAGE = 'READ_ERROR_MESSAGE';
export const SHOW_ERROR_MESSAGE = 'SHOW_ERROR_MESSAGE';
