import React, { Fragment } from 'react';
import moment from 'moment';
import cx from 'classnames';
import i18n from 'i18next';
import { IconReservations, IconGuests } from '../../components/icons/MenuIcons';
import { isDNR } from '../../utils/reservation/frontdesk';

/* ----- Add/edit reservation form ----- */
export const formatGroupLabel = data => (
  <div className="select-section">
    <span>{data.label}</span>
    <span className="badge">{data.options.length}</span>
  </div>
);

export const formatGuestOption = ({ item, label }) => (
  <div className="guest-option">
    {item
      ? (
        <Fragment>
          <p>
            <strong>
              {item.title ? `${i18n.t(`common.${item.title}`)} ` : ''}
              {`${item.first_name} ${item.last_name}`}
            </strong>
          </p>
          {item.phone_number && <p>{`+${item.phone_code} ${item.phone_number}`}</p>}
          {item.email && <p>{item.email}</p>}
        </Fragment>
      )
      : <div className="d-flex align-items-center h-100"><p>{label}</p></div>
    }
  </div>
);

/* ----- Search reservation ----- */
export const formatOptionLabel = ({ item, label }, locale) => (
  <div className="search-option">
    <div>
      <strong>
        <span>
          {`${moment(item.start).locale(locale).format('D MMM')}-${moment(item.end).locale(locale).format('D MMM')}`}
        </span>
        <span>{` #${item.id} `}</span>
      </strong>
    </div>
    <div>{label}</div>
  </div>
);

export const eventDayView = (event, locale) => (
  <Fragment>
    <div className={cx('rct-item-info', { 'mx-4': !isDNR(event.status) })}>
      <IconReservations width="14" height="14" />
      <span className="px-2">
        {`${moment(event.start).locale(locale).format('D MMM')} - ${moment(event.end).locale(locale).format('D MMM')}`}
      </span>
    </div>
    {
      !isDNR(event.status)
      && (
        <div className="rct-item-info">
          <IconGuests width="14" height="14" />
          <span className="px-2">
            {`  ${event.adults} `}
            {event.adults > 1 ? i18n.t('common.guests') : i18n.t('common.guest')}
          </span>
        </div>
      )
    }
  </Fragment>
);
