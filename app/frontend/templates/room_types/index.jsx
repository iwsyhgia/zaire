import React from 'react';
import cx from 'classnames';
import i18n from 'i18next';

export const RoomTypeUnit = ({
  data: {
    fields = null,
    amenities = null,
    images = null,
    rooms = null,
  } = {},
}) => (
  <div>
    <div className="room-type-item mb-4" style={{ paddingLeft: '1px', paddingBottom: '1px' }}>
      <div className="row no-gutters h-100">
        <div className="col-6">
          <div className="row no-gutters h-100">
            <div className="col-6">
              {fields}
            </div>
            <div className="col-6">
              {amenities}
            </div>
          </div>
        </div>
        <div className="col-6">
          <div className="row no-gutters h-100">
            <div className="col-6">
              {images}
            </div>
            <div className="col-6">
              {rooms}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

/* ----- Amenities ----- */
export const AmenitiesBlock = ({ children, className, length }) => (
  <div className={cx('zr-amenities-section d-flex flex-column h-100', className)}>
    <h3 className="mb-3 pt-3 pl-3 pr-3">
      {i18n.t('common.amenities')}
      <span className="color-gray-600">{` (${length})`}</span>
    </h3>
    {children}
  </div>
);

export const AmenityCategoryTitle = ({
  name,
  itemCount,
  isOpen,
  onClick,
  rlmFix,
}) => (
  <li className="units-list-item px-3 py-1">
    <div className="form-check">
      <label className="form-check-label">
        <input
          className="form-check-input"
          type="checkbox"
          checked={isOpen}
          onChange={event => onClick(event, name)}
        />
        <span className="category-check">{isOpen ? '-' : '+'}</span>
        {name}
        <span>
          {rlmFix}
          {` (${itemCount}) `}
        </span>
      </label>
    </div>
  </li>
);

export const AmenityItem = ({
  name,
  id,
  isChecked,
  onClick,
  controls = null,
}) => (
  <li className="units-list-item px-3 py-1">
    <div className="form-check">
      <label className="form-check-label">
        <input
          className="form-check-input"
          type="checkbox"
          checked={isChecked}
          onChange={event => onClick(event, id)}
        />
        {name}
      </label>
    </div>
    {controls}
  </li>
);

/* ----- Images ----- */
export const ImagesBlock = ({ children, className, length }) => (
  <div className={cx('zr-rooms-photos-section d-flex flex-column h-100', className)}>
    <h3 className="mb-3 pt-3 pl-3 pr-3">
      {i18n.t('common.photos')}
      <span className="color-gray-600">{` (${length})`}</span>
    </h3>
    {children}
  </div>
);

/* ----- Rooms ----- */
export const RoomsBlock = ({ children, className, length }) => (
  <div className={cx('zr-rooms-list-section d-flex flex-column h-100', className)}>
    <h3 className="mb-3 pt-3 pl-3 pr-3">
      {i18n.t('common.rooms')}
      <span className="color-gray-600">{` (${length})`}</span>
    </h3>
    {children}
  </div>
);
