function show(id) {
  const requestOptions = {
    method: 'GET',
    credentials: 'same-origin'
  };

  return fetch(`/api/v1/rooms/room_types/${id}`, requestOptions)
    .then(handleResponse)
}

function handleResponse(response) {
  if (response.status === 204) {
    return response;
  }
  return response.json().then((json) => {
    if (!response.ok) {
      return Promise.reject(json);
    }

    return json;
  });
}

export default { show };

