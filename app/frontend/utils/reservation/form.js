import moment from 'moment';
import { groupByProp, ignorecaseSort } from '../helpers';
import { CHECKED_IN } from '../../constants/constants';

export const getRoomOption = (room, locale) => {
  const isArabicTranslated = (locale === 'ar' && !!room.room_type_name_ar);
  const localizedName = isArabicTranslated ? room.room_type_name_ar : room.room_type_name;
  return {
    value:    room.id,
    label:    `${room.number} ${localizedName}`,
    number:   room.number,
    roomType: localizedName,
  };
};

export const getGroupedRoomOptions = (rooms, locale) => {

  // sort rooms first
  const roomOptions = ignorecaseSort(rooms, 'room_type_name') // TODO: check sort in arabic mode
    .map(item => (
      getRoomOption(item, locale)
    ));

  const groups = groupByProp(roomOptions, 'roomType');

  return Object.keys(groups).map(key => ({
    label:   key,
    options: groups[key].sort((a, b) => (a.number - b.number)),
  }));
};

/* ----- Dates ----- */
export const processDates = (checkIn, initCheckIn, checkOut, initCheckOut) => {
  const momentCheckIn  = checkIn ? moment(checkIn, 'DD/MM/YYYY') : moment(initCheckIn);
  const momentCheckOut = checkOut ? moment(checkOut, 'DD/MM/YYYY') : moment(initCheckOut);

  const totalNights    = momentCheckIn && momentCheckOut
    ? Math.ceil(momentCheckOut.diff(momentCheckIn, 'days', true))
    : 0;

  if (totalNights === 0) {
    momentCheckOut.add(1, 'days');
  }

  return {
    momentCheckIn,
    momentCheckOut,
  };
};

export const isValidMoment = dateMoment => (
  dateMoment.isAfter(moment().subtract(1, 'day'))
);

export const isCheckinDateDisabled = (date, status) => (
  moment(date).isBefore(moment().subtract(1, 'day'))
    || (status === CHECKED_IN && moment(date).isSameOrBefore(moment()))
);

/* ----- Fields backup ----- */
export const getFieldsBackup = (type, values) => {
  const formValues = {};
  if (type === 'DNR') {
    const {
      birth_date: BD,
      city,
      country,
      email,
      first_name: firstName,
      last_name: lastName,
      id,
      phone_code: code,
      phone_number: number,
      title,
      ...rest
    } = values;
    formValues.direct = {
      ...rest,
      reserveType: 'direct',
      guest:       {
        birth_date:   BD,
        city,
        country,
        email,
        first_name:   firstName,
        last_name:    lastName,
        id,
        phone_code:   code,
        phone_number: number,
        title,
      },
    };
  } else {
    const { check_in_date: checkIn, check_out_date: checkOut, ...rest } = values;
    formValues.dnr = {
      ...rest,
      reserveType:    'DNR',
      check_in_date:  moment(checkIn).valueOf(),
      check_out_date: moment(checkOut).valueOf(),
    };
  }

  return formValues;
};
