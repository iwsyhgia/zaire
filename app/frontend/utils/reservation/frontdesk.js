import _ from 'lodash';
import moment from 'moment';
import i18n from 'i18next';

import {
  FORCE_FRONTDESK_UPDATE,
  DNR_STATUSES,
  CHECKED_IN,
  CONFIRMED,
  UNCONFIRMED,
  REFUNDABLE,
} from '../../constants/constants';
import { ignorecaseSort, scrollTo } from '../helpers';

export const isDNR = status => DNR_STATUSES.includes(status);

const isMoveable = item => (
  moment(item.check_in_date).isSameOrAfter(moment(), 'day')
    && (isDNR(item.status) || item.status === UNCONFIRMED || item.status === CONFIRMED
      || (!(item.rate === REFUNDABLE) && item.status === CHECKED_IN))
);

const isStartResizeable = item => (
  moment(item.check_in_date).isSameOrAfter(moment(), 'day')
    && (item.status === UNCONFIRMED || (item.status === CONFIRMED && item.rate === REFUNDABLE) || isDNR(item.status))
);

const isEndResizeable = item => (
  moment(item.check_out_date).isSameOrAfter(moment(), 'day')
    && (item.status === UNCONFIRMED
      || (item.rate === REFUNDABLE && (item.status === CHECKED_IN || item.status === CONFIRMED))
      || isDNR(item.status))
);

const isMoveableWithinType = item => (
  !(item.rate === REFUNDABLE) && (item.status === CHECKED_IN || item.status === CONFIRMED)
);

const getGuestName = (guest) => {
  const { title, first_name: firstName, last_name: lastName } = guest;
  return `${title ? i18n.t(`common.${title}`) : ''} ${firstName} ${lastName}`;
};

export const getFrontdeskEvents = (events = [], resources = []) => {
  const roomsCounter = Object.entries(_.groupBy(resources, 'parent'))
    .reduce((result, [key, value]) => ({ ...result, [key]: value.length }), {});

  const newEvents = events
    .filter(item => !!item.room)
    .map((item) => {
      const roomTypeId = item.room ? `root-${item.room.room_type_id}` : 'NONE';
      const roomsOfType = roomsCounter[roomTypeId] || 1;
      const isWithinType = isMoveableWithinType(item);
      return {
        id:          item.id,
        resourceId:  item.room ? item.room.id : 0,
        start:       `${item.check_in_date} 12:00:00`, // format YYYY-MM-DD
        end:         `${item.check_out_date} 12:00:00`,
        title:       item.guest ? getGuestName(item.guest) : '',
        description: item.description || '',
        status:      item.status,
        roomType:    roomTypeId,
        adults:      item.number_of_adults || '',
        avgPrice:    item.average_price_per_night,
        email:       item.guest && item.guest.email ? item.guest.email : '',
        phone:       item.guest && item.guest.phone_number
          ? `+${item.guest.phone_code}${item.guest.phone_number}`
          : '',
        withinType:        isWithinType,
        movable:           isWithinType ? roomsOfType > 1 : isMoveable(item),
        crossResourceMove: isWithinType ? roomsOfType > 1 : isMoveable(item),
        startResizable:    isStartResizeable(item),
        endResizable:      isEndResizeable(item),
      };
    });

  return _.orderBy(newEvents, ['start'], ['asc']);
};

export const getVisibleRooms = (roomType, rooms, hiddenRoomTypes = [], action = 'toggle') => {

  const newHiddenRoomTypes = [...hiddenRoomTypes];

  const ind = newHiddenRoomTypes.indexOf(roomType);

  switch (action) {
    case 'open':
      if (ind !== -1) {
        newHiddenRoomTypes.splice(ind, 1);
      }
      break;
    case 'toggle':
    default:
      if (ind === -1) { // hide section
        newHiddenRoomTypes.push(roomType);
      } else { // show section
        newHiddenRoomTypes.splice(ind, 1);
      }
      break;
  }

  const visibleRooms = rooms.filter(item => !newHiddenRoomTypes.includes(item.parent));

  return {
    visibleRooms,
    newHiddenRoomTypes,
  };

};

export const getFrontdeskRooms = (rooms, locale) => {

  const groups = rooms
    .reduce((result, item) => {
      const groupID = `root-${item.room_type_id}`;
      const group = (typeof result[groupID] === 'undefined')
        ? {
          [groupID]: {
            id:    `root-${item.room_type_id}`,
            name:  (locale === 'ar' && !!item.room_type_name_ar) ? item.room_type_name_ar : item.room_type_name,
            rooms: [item],
          },
        }
        : {
          [groupID]: {
            ...result[groupID],
            rooms: [...result[groupID].rooms, item],
          },
        };
      return { ...result, ...group };
    }, {});

  return ignorecaseSort(Object.values(groups), 'name')
    .reduce((result, item) => {
      const groupRooms = item.rooms.map(room => ({
        id:     room.id,
        name:   room.number,
        root:   false,
        parent: item.id,
      })).sort((a, b) => (a.name - b.name));

      const parentRoot = {
        id:     item.id,
        name:   item.name,
        root:   true,
        parent: null,
        rates:  {}, // TODO: get rates in form {'2019-11-25': 200}
      };

      return [...result, parentRoot, ...groupRooms];
    }, []);
};

/* ----- Search reservation ----- */
export const findReservationCallback = (inputValue, reservations = []) => {
  if (inputValue) {
    const searchValue = inputValue.toUpperCase();
    return reservations
      .filter(item => item.id.toString().toUpperCase().includes(searchValue)
        || item.title.toUpperCase().includes(searchValue)
        || item.email.toUpperCase().includes(searchValue)
        || item.phone.toUpperCase().includes(searchValue))
      .map(item => ({ value: item.id, label: item.title, item }));
  }
  return [];
};

export const getSearchableEvents = events => (
  events.filter(item => !isDNR(item.status))
);

export const scrollToReservationWithID = (id) => {

  // Tricky hack to scroll to proper item after rerender
  let n = 0;
  const scrollInterval = setInterval(() => {
    const reservation = document.getElementById(`reservation-${id}`);
    if (reservation) {
      clearInterval(scrollInterval);
      const area = reservation.closest('.scheduler-body-table > tbody');
      const offset = reservation.getBoundingClientRect().top
        - area.getBoundingClientRect().top + area.scrollTop - 42;
      scrollTo(area, offset, 200);
    }
    if (n > 10) {
      clearInterval(scrollInterval);
    }
    n += 1;
  }, 150);
};

/* ----- Frontdesk state from props ----- */
export const getFrontdeskState = (props, state, isSameTime) => {

  const schedulerChanges = {};
  const stateUpdates = {};

  const { viewModel, groupedRooms = {} } = state;
  const { config } = viewModel;
  const { lastCompletedAudit } = props;

  // Check audit date updates
  const lastAuditDate = moment(lastCompletedAudit.time_frame_end).format('YYYY-MM-DD');
  if (config.edgeDate && config.edgeDate.format('YYYY-MM-DD') !== lastAuditDate) {
    schedulerChanges.edgeDate = lastAuditDate;
  }

  // Check reservation updates
  if (!!props.events && !!state.events
    && (!_.isEqual(_.sortBy(props.events), _.sortBy(state.events)) || !isSameTime)
  ) {
    const newEvents = getFrontdeskEvents(props.events, groupedRooms);
    const searchableEvents = getSearchableEvents(newEvents);

    schedulerChanges.events = newEvents;
    stateUpdates.events = _.cloneDeep(props.events);
    stateUpdates.reservations = searchableEvents;
  }

  // Check room updates
  if (!!props.resources && !!state.resources
    && (!_.isEqual(_.sortBy(props.resources), _.sortBy(state.resources)) || !isSameTime)
  ) {
    schedulerChanges.resources = props.resources;
    stateUpdates.resources = [...props.resources];
    const updatedRooms = _.groupBy(props.resources, 'parent');
    stateUpdates.groupedRooms = updatedRooms;

    if (props.events && props.events.length > 0) { // to update moveable prop
      const newEvents = getFrontdeskEvents(props.events, updatedRooms);
      schedulerChanges.events = newEvents;
    }
  }

  return { schedulerChanges, stateUpdates };
};

/* ----- LocalStorage ----- */
export const forceFrontdeskUpdate = (updates, isUpdateNeeded = true) => {
  window.localStorage.setItem(FORCE_FRONTDESK_UPDATE, JSON.stringify({ need_update: isUpdateNeeded, ...updates }));
};
