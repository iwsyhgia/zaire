import moment from 'moment';
import i18n from 'i18next';

export const getUpdatedGuestField = (guestItem, countries, locale) => {

  const userCountry = countries
    .find(country => country.alpha3 === guestItem.country || country.name === guestItem.country);

  const fCountry = userCountry
    ? {
      value: userCountry.alpha3,
      label: locale === 'ar' ? userCountry.name_ar : userCountry.name,
    }
    : null;

  const fTitle = guestItem.title && guestItem.title !== ''
    ? {
      value: guestItem.title,
      label: i18n.t(`common.${guestItem.title}`),
    }
    : null;

  const fBDay = guestItem.birth_date || '';

  const fPhone = {
    value: guestItem.phone_number ? guestItem.phone_number : '',
    label: guestItem.phone_number ? guestItem.phone_number : '',
  };

  const fEmail = {
    value: guestItem.email ? guestItem.email : '',
    label: guestItem.email ? guestItem.email : '',
  };

  const fLastName = {
    value: guestItem.last_name ? guestItem.last_name : '',
    label: guestItem.last_name ? guestItem.last_name : '',
  };

  return {
    formCountry:  fCountry,
    formTitle:    fTitle,
    formBDay:     fBDay,
    formPhone:    fPhone,
    formEmail:    fEmail,
    formLastName: fLastName,
  };
};

export const getUpdatedPaymentsFields = (payment) => {
  const fAmount = payment.amount ? payment.amount : '';

  const fPMethod = payment.payment_method ? {
    value: (payment || {}).payment_method,
    label: i18n.t(`common.${payment.payment_method}`),
  } : null;

  return {
    formAmount:  fAmount,
    formPMethod: fPMethod,
  };
};

export const getUpdatedRefundsFields = (refund, pIds) => {
  const [getPaymentId] = pIds.filter(item => item.id === (refund || {}).payment_id);

  const fPaymentId = refund.payment_id ? {
    value: (refund || {}).payment_id,
    label: `${moment(getPaymentId.created_at).format('DD/MM/YYYY')}
            - ${+getPaymentId.amount} ${i18n.t('reservation.SAR')}`,
  } : null;

  const fRefundAmount = refund.amount ? refund.amount : '';

  const fDescription = refund.description ? refund.description : '';

  return {
    formPaymentId:    fPaymentId,
    formRefundAmount: fRefundAmount,
    formDescription:  fDescription,
  };
};

export const getUpdatedDiscountsFields = (discount) => {
  const fDescription = discount.description ? discount.description : '';

  const fValue = discount.kind ? {
    value: (discount || {}).kind,
    label: i18n.t(`common.${discount.kind}`),
  } : null;

  const fDiscountAmount = discount.value ? discount.value : '';

  return {
    formDescription:    fDescription,
    formValue:          fValue,
    formDiscountAmount: fDiscountAmount,
  };
};

export const getAdultsTitle = (adult) => {
  const fTitle = adult.title && adult.title !== ''
    ? {
      value: adult.title,
      label: i18n.t(`common.${adult.title}`),
    }
    : null;
  return {
    formTitle: fTitle,
  };
};

export const updateGuestFields = (guestFields, formApi) => {
  Object.entries(guestFields).forEach(([key, value]) => (formApi.setValue(key, value)));
};
