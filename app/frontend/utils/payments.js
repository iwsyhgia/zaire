import moment from 'moment';
import i18n from 'i18next';
import { E_KEY_NUMBER, MAX_SCREEN_WIDTH, INVOICE_TYPES } from '../constants/constants';

export const howDidWePay = (payment) => {
  if (payment.payment_method === 'cash') {
    return i18n.t('reservation.paid_in_cash');
  }
  return i18n.t('reservation.paid_by_card');
};

export const generateDescription = (payment) => {
  if (payment.invoiceType === INVOICE_TYPES.CHARGE) {
    return `${i18n.t(`charges.${payment.kind}`)} ${i18n.t('charges.charge_for')} ${payment.description}`;
  }
  return payment.description;
};

export const paymentForRefund = (refund, paymentsList) => {
  const [getPaymentId] = paymentsList.filter(item => item.id === (refund || {}).payment_id);
  return i18n.t('reservation.payment_for_refund', {
    payment: ` ${moment((getPaymentId || {}).created_at).format('DD/MM/YYYY')} - ${+(getPaymentId || {}).amount}
      ${i18n.t('reservation.SAR')}`,
  });
};

export const toggleInvoiceCollapse = (
  type,
  collapseState,
  collapseProperty
) => {
  let result = { ...collapseState };
  if (type === 'edit' || type === 'add') {
    result = { ...collapseState, [collapseProperty]: true };
  } else {
    result = { ...collapseState };
  }

  return result;
};

export const handleSubmitInvoices = (isEdit, addInvoice, editInvoice, data, id) => {
  if (isEdit) {
    editInvoice({ ...data, id });
  } else {
    addInvoice(data);
  }
};

export const checkLiterals = (e) => {
  if (e.which === E_KEY_NUMBER) {
    e.preventDefault();
  }
};

export const handleScrollToEnd = (buttonType, actionType, paymentsCount) => {
  const el = document.querySelector('.zr-inner-content .zr-reservation-page');
  if (buttonType && buttonType !== 'hide' && actionType !== 'history' && paymentsCount) {
    el.scrollTop = el.scrollHeight;
  } else {
    el.scrollTop = el.clientWidth < MAX_SCREEN_WIDTH ? el.scrollHeight : el.scrollTop;
  }
};

export const resetStateForSelects = () => (
  {
    currentPaymentMethod: null,
    currentPaymentId:     null,
    currentKind:          null,
  }
);
