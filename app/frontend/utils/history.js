import { createBrowserHistory } from 'history';

export default createBrowserHistory({
  basename: '/',
});

export const historyEn = createBrowserHistory({
  basename: '/en',
});

export const historyAr = createBrowserHistory({
  basename: '/ar',
});
