import moment from 'moment';
import _ from 'lodash';
import cx from 'classnames';

/* Transform array of object into object with keys = values of 'prop' */
export const groupByProp = (options, prop) => (
  options.reduce((result, item) => {
    const groupID = item[prop];
    const group = (typeof result[groupID] === 'undefined')
      ? {
        [groupID]: [item],
      }
      : {
        [groupID]: [
          ...result[groupID],
          item
        ],
      };
    return { ...result, ...group };
  }, {})
);

export const hasValue = param => (!!param);

export const formatDate = date => moment(date).format('DD/MM/YYYY');

export const ignorecaseSort = (list, field) => {
  const sorted = _.sortBy(list, item => item[field] && item[field].toLowerCase());

  return _.concat(sorted.filter(item => Number.isNaN(parseInt(item[field], 10))),
    sorted.filter(item => !Number.isNaN(parseInt(item[field], 10))));
};

export const isEmptyObject = obj => !obj || Object.keys(obj).length === 0;

export const rlmFix = locale => (locale === 'ar' ? String.fromCharCode(8207) : null);

export const isLastAdultEmpty = formState => formState[formState.length - 1].title === ''
  && formState[formState.length - 1].first_name === ''
  && formState[formState.length - 1].last_name === ''
  && formState[formState.length - 1].personal_id === '';


export const cxInvalid = (classNames, model, name) => (
  cx(classNames, { 'is-invalid': _.get(model, `validation.${name}.isInvalid`) })
);

/* ----- Scroll ----- */
export const scrollTo = (element, to, duration) => {
  const start = element.scrollTop;
  const change = to - start;
  const increment = 10;
  let currentTime = 0;

  const animateScroll = () => {
    currentTime += increment;
    const val = Math.easeInOutQuad(currentTime, start, change, duration);
    // eslint-disable-next-line
    element.scrollTop = val;
    if (currentTime < duration) {
      setTimeout(animateScroll, increment);
    }
  };
  animateScroll();
};

// t = current time
// b = start value
// c = change in value
// d = duration
Math.easeInOutQuad = (t, b, c, d) => {
  // eslint-disable-next-line
  t /= d / 2;
  if (t < 1) return c / 2 * t * t + b;
  // eslint-disable-next-line
  t -= 1;
  return -c / 2 * (t * (t - 2) - 1) + b;
};
