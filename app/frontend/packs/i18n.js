import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

const enCommon = require('../../../config/locales/frontend/common.en.json');
const enApplication = require('../../../config/locales/frontend/application.en.json');
const arCommon = require('../../../config/locales/frontend/common.ar.json');
const arApplication = require('../../../config/locales/frontend/application.ar.json');

i18n
  .use(XHR)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: {
          ...enCommon,
          ...enApplication,
        },
      },
      ar: {
        translation: {
          ...arCommon,
          ...arApplication,
        },
      },
    },
    fallbackLng: ['en', 'ar'],

    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
