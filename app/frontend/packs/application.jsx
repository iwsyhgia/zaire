import 'babel-polyfill';
import 'bootstrap/dist/js/bootstrap.min.js';
import './devise';
import './i18n';

// const erb = require('./loaders/erb');

// import '../service-worker/serviceWorkerCompanion'; // Uncomment it to test Service Workers
import React        from 'react';
import ReactDOM     from 'react-dom';
import { Provider } from 'react-redux';
import throttle     from 'lodash/throttle'; // eslint-disable-line
import createStore  from '../store/createStore';
import App          from '../containers/App';

// import '../styles/application.scss';
// import '../styles/application-rtl.scss';

// import { setMomentLocale } from '../helpers';

// import { BrowserRouter, Route, Link } from "react-router-dom";
// setMomentLocale();

// Store Initialization
// ------------------------------------
const store = createStore(window.__INITIAL_STATE__);
const { saveState } = require('../store/syncStoreWithLocalStorage');

store.subscribe(throttle(() => {
  const currentState = store.getState();
  if (!currentState.isOffline) { saveState('redux-store', store.getState()); }
}, 1000));


// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('AppRoot');

let render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <App hotelid={MOUNT_NODE.dataset.hotelid} />
    </Provider>,
    MOUNT_NODE,
  );
};

// Development Tools
// ------------------------------------
if (__DEV__) {
  if (module.hot) {
    const renderApp = render;
    const renderError = (error) => {
      const RedBox = require('redbox-react').default;

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        console.error(e);
        renderError(e);
      }
    };

    // Setup hot module replacement
    module.hot.accept([
      './containers/App',
    ], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE);
        render();
      }));
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__ && !!MOUNT_NODE) render();
