document.addEventListener('DOMContentLoaded', () => {

  const passwordPromptTool = document.getElementById('password-prompt');
  const passwordRevealTool = document.getElementById('password-reveal');
  const passwordPromptTooltip = document.getElementById('password-prompt-tooltip');
  const passwordConfirmationPromptTooltip = document.getElementById('password-confirmation-prompt-tooltip');
  const passwordConfirmationRevealTool = document.getElementById('password-confirmation-reveal');
  const passwordConfirmationPromptTool = document.getElementById('password-confirmation-prompt');

  const revealPassword = (passwordField) => {
    const type = passwordField.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordField.setAttribute('type', type);
  };

  const showPrompt = (promptTooltip) => {
    promptTooltip.classList.add('show');
    promptTooltip.addEventListener('click', () => {
      promptTooltip.classList.remove('show');
    });
  };

  const closePrompt = (promptTooltip) => {
    if (promptTooltip.classList.contains('show')) {
      promptTooltip.classList.remove('show');
    }
  };

  // Events
  if (passwordRevealTool) {
    passwordRevealTool.addEventListener('click', () => {
      passwordRevealTool.classList.toggle('opened');

      const passwordField = document.getElementById('user_password');
      revealPassword(passwordField);
    }, false);
  }

  if (passwordPromptTool && passwordConfirmationPromptTooltip && passwordPromptTooltip) {
    passwordPromptTool.addEventListener('click', () => {
      closePrompt(passwordConfirmationPromptTooltip);
      showPrompt(passwordPromptTooltip);
    }, false);
  }

  if (passwordConfirmationRevealTool) {
    passwordConfirmationRevealTool.addEventListener('click', () => {
      passwordConfirmationRevealTool.classList.toggle('opened');

      const passwordField = document.getElementById('user_password_confirmation');
      revealPassword(passwordField);
    }, false);
  }

  if (passwordConfirmationPromptTool && passwordConfirmationPromptTooltip && passwordPromptTooltip) {
    passwordConfirmationPromptTool.addEventListener('click', () => {
      closePrompt(passwordPromptTooltip);
      showPrompt(passwordConfirmationPromptTooltip);
    }, false);
  }
});
