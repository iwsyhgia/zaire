import {
  showServiceWorkersStatus,
  showCacheStorageStatus,
} from '../service-worker/serviceWorkerCompanion';


document.addEventListener('DOMContentLoaded', () => {
  showServiceWorkersStatus();
  showCacheStorageStatus();
});
