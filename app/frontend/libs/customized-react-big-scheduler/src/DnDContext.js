/* eslint-disable */
import { DropTarget } from 'react-dnd'
import {getPos} from './Util'
import {DnDTypes} from './DnDTypes'
import {CellUnits, DATETIME_FORMAT} from './index'

export default class DnDContext {
    constructor(sources, DecoratedComponent) {
        this.sourceMap = new Map();
        sources.forEach((item) => {
            this.sourceMap.set(item.dndType, item);
        })
        this.DecoratedComponent = DecoratedComponent;
    }

    getDropSpec = () => {
        return {
            drop: (props, monitor, component) =>{
                const {schedulerData, resourceEvents} = props;
                const {cellUnit, localeMoment, disabledIndex, config} = schedulerData;
                const type = monitor.getItemType();
                const item = monitor.getItem();
                const pos = getPos(component.eventContainer);
                let cellWidth = schedulerData.getContentCellWidth();
                let initialStartTime = null, initialEndTime = null;
                const headerItems = (config.isRtl) ? resourceEvents.headerItems.slice(0).reverse(): resourceEvents.headerItems;
                const initialPoint = monitor.getInitialSourceClientOffset();
                let initialLeftIndex = Math.floor((initialPoint.x - pos.x)/cellWidth);
                if(type === DnDTypes.EVENT) {                   
                    initialStartTime = headerItems[initialLeftIndex].start;
                    initialEndTime = headerItems[initialLeftIndex].end;
                    if(cellUnit !== CellUnits.Hour)
                        initialEndTime = localeMoment(headerItems[initialLeftIndex].start).hour(23).minute(59).second(59).format(DATETIME_FORMAT);
                }
                const source = monitor.getSourceClientOffset();   
                let leftIndex = Math.floor((source.x - pos.x)/cellWidth);

                /* --- ZAIRE --- */
                // prevent drop on audited area
                if (
                  (config.isRtl && (resourceEvents.headerItems.length - disabledIndex <= leftIndex))
                  || (!config.isRtl && disabledIndex > leftIndex)
                ) {
                  let slot = schedulerData.getSlotById(item.resourceId);
                  return {
                    slotId: item.resourceId,
                    slotName: slot.name,
                    start: item.start,
                    end: item.start,
                    initialStart: item.start,
                    initialEnd: item.start
                  }
                }
                /* --- end ZAIRE --- */

                let startTime = headerItems[leftIndex].start;
                let endTime = headerItems[leftIndex].end;
                if(cellUnit !== CellUnits.Hour)
                    endTime = localeMoment(headerItems[leftIndex].start).hour(23).minute(59).second(59).format(DATETIME_FORMAT);

                return {
                    slotId: resourceEvents.slotId,
                    slotName: resourceEvents.slotName,
                    start: startTime,
                    end: endTime,
                    initialStart: initialStartTime,
                    initialEnd: initialEndTime
                };
            },

            canDrop: (props, monitor) => {
                const {schedulerData, resourceEvents} = props;
                const item = monitor.getItem();
                if(schedulerData._isResizing() || resourceEvents.root || (item.withinType && item.roomType !== resourceEvents.parent)) return false;
                const {config} = schedulerData;
                return config.movable && (item.movable == undefined || item.movable !== false);
            }
        }
    }

    getDropCollect = (connect, monitor) => {
        return {
            connectDropTarget: connect.dropTarget(),
            isOver: monitor.isOver()
        };
    }

    getDropTarget = () => {
        return DropTarget([...this.sourceMap.keys()], this.getDropSpec(), this.getDropCollect)(this.DecoratedComponent);
    }

    getDndSource = (dndType = DnDTypes.EVENT) => {
        return this.sourceMap.get(dndType);
    }
}
