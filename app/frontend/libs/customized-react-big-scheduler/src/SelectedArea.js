/* eslint-disable */
import React, {Component} from 'react'
import {PropTypes} from 'prop-types'

class SelectedArea extends Component {
    constructor(props){
        super(props);
    }

    static propTypes = {
        schedulerData: PropTypes.object.isRequired,
        left: PropTypes.number.isRequired,
        width: PropTypes.number.isRequired,
    }

    render() {
        const {left, width, schedulerData} = this.props;
        const {config} = schedulerData;
        const cellWidth = schedulerData.getContentCellWidth();
        const offset = (schedulerData.viewType === 0) ? 0 : (config.isRtl ? -cellWidth/2 : cellWidth/2);

        return (
            <div className="selected-area" style={{left: left, width: width, top: 0, bottom: 0, backgroundColor: config.selectedAreaColor, marginLeft:offset}}>
            </div>
        );
    }
}

export default SelectedArea
