import React from 'react';
import renderer from 'react-test-renderer';
import Switcher from '../../../components/controls/Switcher';

it('renders correctly', () => {
  const tree = renderer.create(<Switcher />).toJSON();
  expect(tree).toMatchSnapshot();
});
