import React from 'react';
import renderer from 'react-test-renderer';
import DefaultLogo from '../../../components/icons/DefaultLogo';

it('renders correctly', () => {
  const tree = renderer.create(<DefaultLogo />).toJSON();
  expect(tree).toMatchSnapshot();
});
