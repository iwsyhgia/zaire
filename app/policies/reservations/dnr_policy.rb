# frozen_string_literal: true

module Reservations
  class DNRPolicy < ReservationPolicy
    class Scope < Scope
      def resolve
        scope.merge(hotel.dnr)
      end
    end
  end
end
