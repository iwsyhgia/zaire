# frozen_string_literal: true

module Reservations
  class ReservationPolicy < ApplicationPolicy
    def create?
      user.present? && hotel.rooms.exists?(record.room_id)
    end

    def update?
      create?
    end

    def destroy?
      create?
    end

    def show?
      create?
    end

    def cancel?
      create?
    end

    def check_in?
      create?
    end

    def check_out?
      create?
    end
  end
end
