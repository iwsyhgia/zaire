# frozen_string_literal: true

module Reservations
  class DirectReservationPolicy < ReservationPolicy
    class Scope < Scope
      def resolve
        scope.merge(hotel.reservations)
      end
    end
  end
end
