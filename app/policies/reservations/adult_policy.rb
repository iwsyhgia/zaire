# frozen_string_literal: true

module Reservations
  class AdultPolicy < ApplicationPolicy
    def create?
      user.present? && hotel.reservations.exists?(record.reservation_id)
    end

    def update?
      create?
    end

    def destroy?
      create?
    end

    def show?
      create?
    end
  end
end
