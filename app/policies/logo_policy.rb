# frozen_string_literal: true

class LogoPolicy < ApplicationPolicy
  def update?
    user.present? && hotel.id == record.imageable_id
  end

  def show?
    update?
  end
end
