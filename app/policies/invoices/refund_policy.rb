# frozen_string_literal: true

module Invoices
  class RefundPolicy < ApplicationPolicy
    include Invoiceable

    def create?
      user.present? &&
        hotel.invoices.exists?(record.invoice_id) &&
        ::Invoices::Payment.exists?(invoice_id: hotel.invoices, id: record.payment_id)
    end

    def update?
      create? && not_audited
    end

    def destroy?
      create? && not_audited
    end

    def show?
      create?
    end

    class Scope < Scope
      def resolve
        scope.where(invoice_id: hotel.invoices)
      end
    end
  end
end
