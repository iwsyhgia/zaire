# frozen_string_literal: true

module Invoices
  module Invoiceable
    private

    def not_audited
      hotel.last_audit_time < record.created_at
    end
  end
end
