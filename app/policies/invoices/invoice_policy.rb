# frozen_string_literal: true

module Invoices
  class InvoicePolicy < ApplicationPolicy
    include Invoiceable

    def create?
      user.present? && hotel.invoices.exists?(record.id)
    end

    def update?
      create? && not_audited
    end

    def destroy?
      create? && not_audited
    end

    def show?
      create?
    end
  end
end
