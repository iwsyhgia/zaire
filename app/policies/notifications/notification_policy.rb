# frozen_string_literal: true

module Notifications
  class NotificationPolicy < ApplicationPolicy
    def index?
      user.present?
    end

    def read?
      record.recipient == user ||
        Guests::GuestPolicy::Scope.new(user, Guests::Guest.all).resolve.exist?(record.recipient)
    end

    class Scope < Scope
      def resolve
        scope.where(recipient: [user, Guests::GuestPolicy::Scope.new(user, Guests::Guest.all).resolve])
      end
    end
  end
end
