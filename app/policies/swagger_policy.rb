# frozen_string_literal: true

class SwaggerPolicy < ApplicationPolicy
  def index?
    user.present?
  end
end
