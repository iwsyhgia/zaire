# frozen_string_literal: true

module Audits
  class ConfirmationPolicy < ApplicationPolicy
    def confirm?
      record.user == user
    end

    class Scope < Scope
      def resolve
        scope.where(user: user)
      end
    end
  end
end
