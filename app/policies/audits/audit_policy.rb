# frozen_string_literal: true

module Audits
  class AuditPolicy < ApplicationPolicy
    def show?
      hotel == record.hotel
    end

    class Scope < Scope
      def resolve
        scope.where(hotel: hotel)
      end
    end
  end
end
