# frozen_string_literal: true

class ApplicationPolicy
  attr_reader :user, :record, :hotel

  def initialize(user, record)
    @user = user
    @hotel = user&.hotel
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    true
  end

  def edit?
    update?
  end

  def destroy?
    true
  end

  class Scope
    attr_reader :user, :hotel, :scope

    def initialize(user, scope)
      @user = user
      @hotel = user&.hotel
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
