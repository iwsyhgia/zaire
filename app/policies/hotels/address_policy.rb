# frozen_string_literal: true

module Hotels
  class AddressPolicy < ApplicationPolicy
    def show?
      user && record.hotel_id == hotel.id
    end

    def update?
      user && record.hotel_id == hotel.id
    end
  end
end
