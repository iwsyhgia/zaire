# frozen_string_literal: true

module Hotels
  class HotelPolicy < ApplicationPolicy
    def show?
      @user && @record.user_id == @user.id
    end

    def update?
      @user && @record.user_id == @user.id
    end
  end
end
