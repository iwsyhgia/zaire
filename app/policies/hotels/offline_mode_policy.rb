# frozen_string_literal: true

module Hotels
  class OfflineModePolicy < ApplicationPolicy
    def update?
      user.present? && record.hotel_id == hotel.id
    end

    def show?
      update?
    end
  end
end
