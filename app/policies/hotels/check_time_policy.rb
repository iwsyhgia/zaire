# frozen_string_literal: true

module Hotels
  class CheckTimePolicy < ApplicationPolicy
    def update?
      user.present? && record.hotel_id == hotel.id
    end
  end
end
