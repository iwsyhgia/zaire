# frozen_string_literal: true

module Guests
  class GuestPolicy < ApplicationPolicy
    class Scope < Scope
      def resolve
        scope.merge(hotel.guests.distinct)
      end
    end
  end
end
