# frozen_string_literal: true

module Profiles
  class ProfilePolicy < ApplicationPolicy
    def show?
      record.user_id == user.id
    end

    def update?
      record.user_id == user.id
    end
  end
end
