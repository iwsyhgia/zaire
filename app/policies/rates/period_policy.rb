# frozen_string_literal: true

module Rates
  class PeriodPolicy < ApplicationPolicy
    class Scope < Scope
      def resolve
        scope.where(hotel_id: hotel.id)
      end
    end
  end
end
