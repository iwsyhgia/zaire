# frozen_string_literal: true

module Rates
  class PeriodsRoomTypePolicy < ApplicationPolicy
    class Scope < Scope
      def resolve
        scope.joins(:period).where(rates_periods: { hotel_id: hotel.id })
      end
    end
  end
end
