# frozen_string_literal: true

module Rooms
  class AmenityPolicy < ApplicationPolicy
    def create?
      user.present? && record.hotel_id == hotel.id
    end

    def update?
      create?
    end

    def destroy?
      create?
    end

    def show?
      create? || record.hotel_id.nil?
    end

    class Scope < Scope
      def resolve
        scope.where(hotel_id: [hotel.id, nil])
      end
    end
  end
end
