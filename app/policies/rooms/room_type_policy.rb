# frozen_string_literal: true

module Rooms
  class RoomTypePolicy < ApplicationPolicy
    def create?
      user.present? && record.hotel_id == hotel.id
    end

    def update?
      create?
    end

    def destroy?
      create?
    end

    def show?
      create?
    end

    class Scope < Scope
      def resolve
        scope.merge(hotel.room_types)
      end
    end
  end
end
