# frozen_string_literal: true

module Rooms
  class RoomPolicy < ApplicationPolicy
    def create?
      user.present? && hotel.room_types.exists?(record.room_type_id)
    end

    def update?
      create?
    end

    def destroy?
      create?
    end

    def show?
      create?
    end

    class Scope < Scope
      def resolve
        scope.merge(hotel.rooms)
      end
    end
  end
end
