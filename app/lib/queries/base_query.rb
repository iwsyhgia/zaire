# frozen_string_literal: true

module Queries
  class BaseQuery
    include Singleton

    class << self
      delegate :call, to: :instance
    end
  end
end
