# frozen_string_literal: true

module Queries
  module Reservations
    class ReservationQuery < BaseQuery
      def initialize(relation = ::Reservations::Reservation.all)
        @reservations = relation.extending(Scopes)
      end
    end

    module Scopes
      def overlaps(check_in_date, check_out_date)
        check_in_date  = check_in_date.to_time&.to_date&.to_formatted_s(:db)
        check_out_date = check_out_date.to_time&.to_date&.to_formatted_s(:db)

        condition = '(check_in_date, check_out_date) OVERLAPS (?, ?)'
        where(condition, check_in_date, check_out_date)
      end
    end
  end
end
