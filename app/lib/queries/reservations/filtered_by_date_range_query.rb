# frozen_string_literal: true

module Queries
  module Reservations
    class FilteredByDateRangeQuery < ReservationQuery
      def call(from_date, to_date)
        # TODO: Is it equal to overlaps query?
        @reservations.
          where(check_in_date: from_date..to_date).
          or(@reservations.where(check_out_date: from_date..to_date))
      end
    end
  end
end
