# frozen_string_literal: true

module Queries
  module Reservations
    class OverlapsQuery < ReservationQuery
      def call(options)
        check_in_date  = options[:check_in_date].to_time&.to_date&.to_formatted_s(:db)
        check_out_date = options[:check_out_date].to_time&.to_date&.to_formatted_s(:db)

        @reservations.overlaps(check_in_date, check_out_date)
      end
    end
  end
end
