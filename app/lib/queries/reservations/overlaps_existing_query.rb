# frozen_string_literal: true

module Queries
  module Reservations
    class OverlapsExistingQuery < ReservationQuery
      def call(options)
        @reservations.
          where(room_id: options[:room_id]).
          where.not(id: options[:id]).
          where.not(status: :cancelled).
          overlaps(options[:check_in_date], options[:check_out_date])
      end
    end
  end
end
