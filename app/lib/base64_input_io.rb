# frozen_string_literal: true

class Base64InputIO < StringIO
  attr_accessor :filename

  def original_filename
    @filename ||= "#{SecureRandom.uuid}.png"
    @filename
  end
end
