module Swagger
  module V1
    module Models
      module Rates
        class PeriodsRoomTypeShow < Swagger::V1::Models::Record
          swagger_schema :PeriodsRoomTypeShow do
            key :title, 'Periods Room Type Show'
            property :id do
              key :type, :number
            end
            property :room_type_id do
              key :type, :number
            end
            property :discount_value do
              key :type, :string
            end
            property :rates do
              key :'$ref', :PeriodsRoomTypeRates
            end
          end
        end
      end
    end
  end
end
