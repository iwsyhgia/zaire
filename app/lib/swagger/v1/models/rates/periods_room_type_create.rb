module Swagger
  module V1
    module Models
      module Rates
        class PeriodsRoomTypeCreate < Swagger::V1::Models::Record
          swagger_schema :PeriodsRoomTypeCreate do
            key :title, 'Periods Room Type Create'
            key :required, %i[discount_value rates room_type_id]
            property :room_type_id do
              key :type, :number
            end
            property :discount_value do
              key :type, :string
            end
            property :rates do
              key :'$ref', :PeriodsRoomTypeRates
            end
          end
        end
      end
    end
  end
end
