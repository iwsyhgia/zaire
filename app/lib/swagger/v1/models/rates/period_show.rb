module Swagger
  module V1
    module Models
      module Rates
        class PeriodShow < Swagger::V1::Models::Record
          swagger_schema :PeriodShow do
            key :title, 'Period Show'
            key :required, %i[id name start_date end_date price_kind discount_kind period_kind]
            property :id do
              key :type, :number
            end
            property :name do
              key :type, :string
            end
            property :start_date do
              key :type, :date
            end
            property :end_date do
              key :type, :date
            end
            property :price_kind do
              key :type, :string
              key :enum, ::Rates::Period.price_kinds.keys
            end
            property :discount_kind do
              key :type, :string
              key :enum, ::Rates::Period.discount_kinds.keys
            end
            property :period_kind do
              key :type, :string
              key :enum, ::Rates::Period.period_kinds.keys
            end
            property :periods_room_types_attributes do
              key :type, :array
              items do
                key :'$ref', :PeriodRoomTypeCreate
                key :title, 'PeriodRoomTypeCreate'
              end
            end
          end
        end
      end
    end
  end
end
