module Swagger
  module V1
    module Models
      module Rates
        class RatesIndex < Swagger::V1::Models::Record
          swagger_schema :RatesIndex do
            key :title, 'RatesIndex'
          end
        end
      end
    end
  end
end
