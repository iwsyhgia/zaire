module Swagger
  module V1
    module Models
      module Rates
        class PeriodsRoomTypeRates < Swagger::V1::Models::Record
          swagger_schema :PeriodsRoomTypeRates do
            key :title, 'PeriodsRoomTypeRates'

            property :monday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :tuesday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :wednesday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :thursday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :friday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :saturday do
              key :'$ref', :PeriodsRoomTypeRate
            end
            property :sunday do
              key :'$ref', :PeriodsRoomTypeRate
            end
          end
        end
      end
    end
  end
end
