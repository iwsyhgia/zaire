module Swagger
  module V1
    module Models
      module Rates
        class PeriodsRoomTypeRate < Swagger::V1::Models::Record
          swagger_schema :PeriodsRoomTypeRate do
            key :title, 'PeriodsRoomTypeRate'

            property :lower_bound do
              key :type, :number
            end
            property :upper_bound do
              key :type, %i[null number]
            end
          end
        end
      end
    end
  end
end
