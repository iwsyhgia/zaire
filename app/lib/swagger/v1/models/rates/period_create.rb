module Swagger
  module V1
    module Models
      module Rates
        class PeriodCreate < Swagger::V1::Models::Record
          swagger_schema :PeriodCreate do
            key :title, 'Period Create'
            key :required, %i[name start_date end_date price_kind discount_kind period_kind]
            property :name do
              key :type, :string
            end
            property :start_date do
              key :type, :string
            end
            property :end_date do
              key :type, :string
            end
            property :price_kind do
              key :type, :string
              key :enum, ::Rates::Period.price_kinds.keys
            end
            property :discount_kind do
              key :type, :string
              key :enum, ::Rates::Period.discount_kinds.keys
            end
            property :period_kind do
              key :type, :string
              key :enum, ::Rates::Period.period_kinds.keys
            end
            property :periods_room_types_attributes do
              key :type, :array
              items do
                key :'$ref', :PeriodsRoomTypeCreate
                key :title, 'PeriodsRoomTypeCreate'
              end
            end
          end
        end
      end
    end
  end
end
