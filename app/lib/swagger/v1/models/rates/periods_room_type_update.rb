module Swagger
  module V1
    module Models
      module Rates
        class PeriodsRoomTypeUpdate < Swagger::V1::Models::Record
          swagger_schema :PeriodsRoomTypeUpdate do
            key :title, 'Periods Room Type Update'
            key :required, %i[id discount_value rates room_type_id]
            property :id do
              key :type, :number
            end
            property :room_type_id do
              key :type, :number
            end
            property :discount_value do
              key :type, :string
            end
            property :rates do
              key :'$ref', :PeriodsRoomTypeRates
            end
          end
        end
      end
    end
  end
end
