module Swagger
  module V1
    module Models
      class RoomCreateQuery < Swagger::V1::Models::Record
        swagger_schema :RoomCreateQuery do
          key :title, 'RoomCreateQuery'
          property :number do
            key :type, :number
          end
          property :room_type_id do
            key :type, %w[integer null]
          end
        end
      end
    end
  end
end
