module Swagger
  module V1
    module Models
      module Hotels
        class CheckTimeIndex < Swagger::V1::Models::Record
          swagger_schema :CheckTimeIndex do
            key :title, 'CheckTime'
            key :required, %i[fixed start_time end_time]
            property :fixed do
              key :type, :boolean
            end
            property :start_time do
              key :type, :string
            end
            property :end_time do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
