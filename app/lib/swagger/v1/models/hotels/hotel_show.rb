module Swagger
  module V1
    module Models
      module Hotels
        class HotelShow < Swagger::V1::Models::Record
          swagger_schema :HotelShow do
            key :title, 'Hotel'
            key :required, %i[name]

            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
