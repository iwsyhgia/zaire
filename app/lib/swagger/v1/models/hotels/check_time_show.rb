module Swagger
  module V1
    module Models
      module Hotels
        class CheckTimeShow < Swagger::V1::Models::Record
          swagger_schema :CheckTimeShow do
            key :title, 'CheckTime'
            key :required, %i[fixed start_time end_time]
            property :fixed do
              key :type, :boolean
              key :enum, [true, false]
            end
            property :start_time do
              key :type, :string
              key :description, <<-Q
                Required. time format - HH:MM
              Q
            end
            property :end_time do
              key :type, :string
              key :description, <<-Q
                Required if fixed = "false".
                Time format - HH:MM
              Q
            end
          end
        end
      end
    end
  end
end
