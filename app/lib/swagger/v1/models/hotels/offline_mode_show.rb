module Swagger
  module V1
    module Models
      module Hotels
        class OfflineModeShow < Swagger::V1::Models::Record
          swagger_schema :OfflineModeShow do
            key :title, 'OfflineMode'
            key :required, %i[active days_before_today days_after_today]
            property :active do
              key :type, :boolean
            end
            property :days_before_today do
              key :type, %i[number null]
            end
            property :days_after_today do
              key :type, %i[number null]
            end
          end
        end
      end
    end
  end
end
