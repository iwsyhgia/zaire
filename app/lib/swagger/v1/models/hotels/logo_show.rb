module Swagger
  module V1
    module Models
      module Hotels
        class LogoShow < Swagger::V1::Models::Record
          swagger_schema :LogoShow do
            key :required, %i[path]
            key :title, 'Logo'

            property :id do
              key :type, :number
            end
            property :path do
              key :type, :string
            end
            property :tag do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
