module Swagger
  module V1
    module Models
      module Hotels
        class LogoCreateQuery < Swagger::V1::Models::Record
          swagger_schema :LogoCreateQuery do
            key :required, %i[logo]

            property :logo do
              property :base64 do
                key :description,  <<-Q
                    Logo image in base64 format
                Q
                key :type, :string
              end
            end
          end
        end
      end
    end
  end
end
