module Swagger
  module V1
    module Models
      module Hotels
        class AddressShow < Swagger::V1::Models::Record
          swagger_schema :AddressShow do
            key :title, 'Address'
            key :required, %i[country city state address_1 fax email phone_number]

            property :country do
              key :type, :string
            end

            property :city do
              key :type, :string
            end

            property :state do
              key :type, :string
            end

            property :address_1 do
              key :type, :string
            end

            property :address_2 do
              key :type, :string
            end

            property :fax do
              key :type, :string
            end

            property :email do
              key :type, :string
            end

            property :phone_number do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
