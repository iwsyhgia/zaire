# frozen_string_literal: true

module Swagger
  module V1
    module Models
      module Hotels
        class ConfigurationShow < Swagger::V1::Models::Record
          swagger_schema :ConfigurationShow do
            key :title, 'Configuration'
            key :required, %i[night_audit_time night_audit_confirmation_required time_zone]

            property :night_audit_time do
              key :type, :string
              key :description, "ex: '03:00', only from 3am till 7am "
            end

            property :night_audit_confirmation_required do
              key :type, :boolean
            end

            property :time_zone do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
