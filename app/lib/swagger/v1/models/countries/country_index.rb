module Swagger
  module V1
    module Models
      module Countries
        class CountryIndex < Swagger::V1::Models::Record
          swagger_schema :CountryIndex do
            key :title, 'Country'
            key :required, %i[alpha3 name country_code]
            property :alpha3 do
              key :type, :string
            end
            property :name do
              key :type, :string
            end
            property :country_code do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
