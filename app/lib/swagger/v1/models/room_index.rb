module Swagger
  module V1
    module Models
      class RoomIndex < Swagger::V1::Models::Record
        swagger_schema :RoomIndex do
          key :title, 'Room'
          key :required, %i[id number]
          property :id do
            key :type, :number
          end
          property :number do
            key :type, :number
          end
          property :room_type_id do
            key :type, :number
          end
        end
      end
    end
  end
end
