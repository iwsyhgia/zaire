module Swagger
  module V1
    module Models
      module Rooms
        class RoomTypeIndex < Swagger::V1::Models::Record
          swagger_schema :RoomTypeIndex do
            key :title, 'RoomType'
            key :required, %i[id name size unit max_occupancy]
            property :id do
              key :type, :number
            end
            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
            property :size do
              key :type, :string
            end
            property :unit do
              key :type, :string
            end
            property :max_occupancy do
              key :type, :number
            end
            property :rate do
              key :'$ref', :RoomTypeRateShow
            end
            property :rooms do
              key :type, :array
              items do
                key :'$ref', :RoomIndex
                key :title, 'RoomIndex'
              end
            end
            property :amenities do
              key :type, :array
              items do
                key :'$ref', :AmenitiesIndex
                key :title, 'Amenity'
              end
            end
            property :images do
              key :type, :array
              items do
                property :id do
                  key :type, :number
                end
                property :path do
                  key :type, :string
                end
              end
            end
          end
        end
      end
    end
  end
end
