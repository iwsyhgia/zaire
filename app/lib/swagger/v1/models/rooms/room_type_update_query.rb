module Swagger
  module V1
    module Models
      module Rooms
        class RoomTypeUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :RoomTypeUpdateQuery do
            key :title, 'RoomTypeUpdateQuery'
            key :required, %i[name max_occupancy quantity]
            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
            property :size do
              key :type, :string
            end
            property :unit do
              key :type, :string
            end
            property :quantity do
              key :type, :number
            end
            property :max_occupancy do
              key :type, :number
            end
            property :rate_attributes do
              key :'$ref', :RoomTypeRateUpdate
              key :title, 'RoomTypeRateUpdate'
            end
            property :rooms_attributes do
              key :type, :array
              items do
                key :'$ref', :RoomCreateQuery
                key :title, 'RoomCreateQuery'
              end
            end
            property :amenity_ids do
              key :type, :array
              key :description, 'List of attached amenities'
              items do
                key :type, :integer
              end
            end
            property :images_attributes do
              key :type, :array
              items do
                property :base64 do
                  key :type, :string
                end
              end
            end
          end
        end
      end
    end
  end
end
