module Swagger
  module V1
    module Models
      module Rooms
        class RoomTypeRateUpdate < Swagger::V1::Models::Record
          swagger_schema :RoomTypeRateUpdate do
            key :title, 'RoomTypeRateUpdate'
            key :required, %i[kind lower_bound]

            property :kind do
              key :type, :string
              key :enum, %i[fixed dynamic]
            end
            property :lower_bound do
              key :type, :number
            end
            property :upper_bound do
              key :type, [:number, :null]
            end
          end
        end
      end
    end
  end
end
