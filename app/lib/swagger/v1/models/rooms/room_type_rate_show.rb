module Swagger
  module V1
    module Models
      module Rooms
        class RoomTypeRateShow < Swagger::V1::Models::Record
          swagger_schema :RoomTypeRateShow do
            key :title, 'RoomTypeRateShow'
            key :required, %i[id kind lower_bound upper_bound]
            property :id do
              key :type, :number
            end
            property :kind do
              key :type, :string
              key :enum, %i[fixed dynamic]
            end
            property :lower_bound do
              key :type, :string
            end
            property :upper_bound do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
