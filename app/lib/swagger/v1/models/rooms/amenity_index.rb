module Swagger
  module V1
    module Models
      module Rooms
        class AmenityIndex < Swagger::V1::Models::Record
          swagger_schema :AmenityIndex do
            key :title, 'Amenity'
            key :required, %i[id name]
            property :id do
              key :type, :number
            end
            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
