module Swagger
  module V1
    module Models
      module Rooms
        class AmenityIndex < Swagger::V1::Models::Record
          swagger_schema :AmenityIndex do
            key :title, 'Amenity'
            key :required, %i[id path]
            property :id do
              key :type, :number
            end
            property :path do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
