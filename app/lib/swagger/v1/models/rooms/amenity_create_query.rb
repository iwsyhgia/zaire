module Swagger
  module V1
    module Models
      module Rooms
        class AmenityCreateQuery < Swagger::V1::Models::Record
          swagger_schema :AmenityCreateQuery do
            key :title, 'AmenityCreateQuery'
            key :required, %i[name]
            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
