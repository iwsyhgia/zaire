module Swagger
  module V1
    module Models
      module Rooms
        class ImageCreateQuery < Swagger::V1::Models::Record
          swagger_schema :ImageCreateQuery do
            key :title, 'ImageCreateQuery'
            key :required, %i[base64]
            property :base64 do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
