module Swagger
  module V1
    module Models
      module Notifications
        class NotificationIndex < Swagger::V1::Models::Record
          swagger_schema :NotificationIndex do
            key :required, %i[id message_params kind read_at created_at updated_at]
            key :title, 'Notification'
            property :id do
              key :type, :number
            end
            property :message_params do
              key :type, :json
            end
            property :kind do
              key :type, :string
            end
            property :read_at do
              key :type, :datetime
            end
            property :created_at do
              key :type, :datetime
            end
            property :updated_at do
              key :type, :datetime
            end
          end
        end
      end
    end
  end
end
