module Swagger
  module V1
    module Models
      module Notifications
        class NotificationCreateQuery < Swagger::V1::Models::Record
          swagger_schema :NotificationCreateQuery do
            key :required, %i[kind reservation_id recipient_id notifier]
            key :title, 'NotificationCreateQuery'

            property :kind do
              key :type, :string
            end
            property :reservation_id do
              key :type, :number
            end
            property :recipient_id do
              key :type, :number
            end
            property :notifier do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
