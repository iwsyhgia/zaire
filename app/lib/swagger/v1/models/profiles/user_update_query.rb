module Swagger
  module V1
    module Models
      module Profiles
        class UserUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :UserUpdateQuery do
            key :title, 'UserUpdateQuery'
            key :required, %i[password current_password password_confirmation]
            property :password do
              key :type, :string
            end
            property :current_password do
              key :type, :string
            end
            property :password_confirmation do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
