module Swagger
  module V1
    module Models
      module Profiles
        class ProfileShow < Swagger::V1::Models::Record
          swagger_schema :ProfileShow do
            key :title, 'Profile'
            key :required, %i[
              id
              user_id
              email
              first_name
              last_name
              phone_number
              notifications

              created_at
              updated_at
            ]

            property :id do
              key :type, :number
            end
            property :user_id do
              key :type, :number
            end
            property :email do
              key :type, :string
            end
            property :first_name do
              key :type, :string
            end
            property :last_name do
              key :type, :string
            end
            property :phone_number do
              key :type, :string
            end
            property :notifications do
              key :enum, ::Profiles::Profile.notifications.keys
            end
            property :phone_number do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
