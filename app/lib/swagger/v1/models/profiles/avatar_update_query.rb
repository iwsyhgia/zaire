module Swagger
  module V1
    module Models
      module Profiles
        class AvatarUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :AvatarUpdateQuery do
            key :title, 'AvatarUpdateQuery'
            key :required, %i[base64]
            property :base64 do
              key :type, [:string, :null]
            end
            property :_destroy do
              key :type, :boolean
            end
          end
        end
      end
    end
  end
end
