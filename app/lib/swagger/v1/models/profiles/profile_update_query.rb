module Swagger
  module V1
    module Models
      module Profiles
        class ProfileUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :ProfileUpdateQuery do
            key :title, 'ProfileUpdateQuery'
            key :required, %i[
              first_name
              last_name
              phone_number
              notifications
            ]
            property :first_name do
              key :type, :string
            end
            property :last_name do
              key :type, :string
            end
            property :phone_number do
              key :type, :string
            end
            property :notifications do
              key :type, :string
              key :enum, ::Profiles::Profile.notifications.keys
            end

            property :user_attributes do
              key :'$ref', :UserUpdateQuery
              key :title, 'UserUpdateQuery'
            end

            property :avatar_attributes do
              key :'$ref', :AvatarUpdateQuery
              key :title, 'AvatarUpdateQuery'
            end
          end
        end
      end
    end
  end
end
