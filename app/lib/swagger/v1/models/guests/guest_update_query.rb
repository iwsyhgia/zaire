module Swagger
  module V1
    module Models
      module Guests
        class GuestUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :GuestUpdateQuery do
            key :title, 'GuestUpdateQuery'
            key :required, %i[
              id
              title
              first_name
              last_name
              email
              phone_code
              phone_number
              country
              city
            ]
            property :id do
              key :type, :number
            end
            property :first_name do
              key :type, :string
            end
            property :title do
              key :type, :string
            end
            property :last_name do
              key :type, :string
            end
            property :birth_date do
              key :type, :string
              key :description, 'ex. 29/11/2018'
              key :pattern, DATE_FORMATTER
            end
            property :email do
              key :type, :string
            end
            property :phone_code do
              key :type, :string
            end
            property :phone_number do
              key :type, :string
            end
            property :country do
              key :type, :string
            end
            property :city do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
