module Swagger
  module V1
    module Models
      module Guests
        class GuestSearchQuery < Swagger::V1::Models::Record
          swagger_schema :GuestSearchQuery do
            key :title, 'GuestSearchQuery'
            key :required, %i[
              g
              email_cont
              phone_number_cont
            ]
            property :g do
              key :description, 'to filter by first\last name'
              items do
                property :first_name_cont do
                  key :type, :string
                end
                property :last_name_cont do
                  key :type, :string
                end
              end
              key :type, :array
              key :maxItems, 1
            end
            property :email_cont do
              key :type, :string
            end
            property :phone_number_cont do
              key :type, :string
            end
          end
        end
      end
    end
  end
end

# TODO: discuss, it makes sense to rewrite to GET query with only params that user enters.
