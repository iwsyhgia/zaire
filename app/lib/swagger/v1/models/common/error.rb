module Swagger
  module V1
    module Models
      module Common
        class Error < Swagger::V1::Models::Record
          swagger_schema :Error do
            key :required, %i[message status]
            property :error do
              property :message do
                key :type, :string
              end
              property :details do
                key :type, :object
              end
            end
            property :status do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
