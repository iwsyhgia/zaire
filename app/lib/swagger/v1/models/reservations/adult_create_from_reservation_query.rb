module Swagger
  module V1
    module Models
      module Reservations
        class AdultCreateFromReservationQuery < Swagger::V1::Models::Record
          swagger_schema :AdultCreateFromReservationQuery do
            key :title, 'AdultCreateFromReservationQuery'
            key :required, %i[
              personal_id
              first_name
              last_name
            ]
            property :id do
              key :type, :string
              key :description, 'pass ID if you want to update an adult'
            end
            property :personal_id do
              key :type, :string
              key :description, 'only digits and letters'
            end
            property :first_name do
              key :type, :string
            end
            property :last_name do
              key :type, :string
            end
            property :title do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
