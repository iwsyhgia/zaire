module Swagger
  module V1
    module Models
      module Reservations
        class AffectedReservation < Swagger::V1::Models::Record
          swagger_schema :AffectedReservation do
            key :type, :object
            key :required, %i[id]

            property :id do
              key :required, %i[room_id]
              key :type, :object

              property :room_id do
                key :required, %i[id]

                property :id do
                  key :type, :number
                end
              end
            end
          end
        end
      end
    end
  end
end
