module Swagger
  module V1
    module Models
      module Reservations
        class DNRCreateQuery < Swagger::V1::Models::Record
          swagger_schema :DNRCreateQuery do
            key :title, 'DNRCreateQuery'
            key :required, %i[
              room_id
              check_in_date
              check_out_date
              description
            ]
            property :room_id do
              key :type, :number
            end
            property :check_in_date do
              key :type, :string
              key :description, 'ex. 29/11/2018'
              key :pattern, DATE_FORMATTER
            end
            property :check_out_date do
              key :type, :string
              key :description, 'ex. 29/11/2018'
              key :pattern, DATE_FORMATTER
            end
            property :description do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
