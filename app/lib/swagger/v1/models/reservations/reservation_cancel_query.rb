module Swagger
  module V1
    module Models
      module Reservations
        class ReservationCancelQuery < Swagger::V1::Models::Record
          swagger_schema :ReservationCancelQuery do
            key :title, 'ReservationCancelQuery'
            key :required, %i[
              cancellation_fee
            ]
            property :cancellation_fee do
              key :type, :boolean
            end
          end
        end
      end
    end
  end
end
