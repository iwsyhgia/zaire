module Swagger
  module V1
    module Models
      module Reservations
        class DNRIndex < Swagger::V1::Models::Record
          swagger_schema :DNRIndex do
            key :title, 'DNR'
            key :required, %i[
              id
              check_in_date
              check_out_date
              description
              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :check_in_date do
              key :type, :date
            end
            property :check_out_date do
              key :type, :date
            end
            property :description do
              key :type, :string
            end
            property :room do
              key :'$ref', :RoomShow
              key :title, 'Room'
            end
          end
        end
      end
    end
  end
end
