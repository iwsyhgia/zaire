module Swagger
  module V1
    module Models
      module Reservations
        class AdultCreateQuery < Swagger::V1::Models::Record
          swagger_schema :AdultCreateQuery do
            key :title, 'AdultCreateQuery'
            key :required, %i[
              reservation_id
              personal_id
              first_name
              last_name
            ]
            property :reservation_id do
              key :type, :number
            end
            property :personal_id do
              key :type, :number
            end
            property :first_name do
              key :type, :string
            end
            property :last_name do
              key :type, :string
            end
            property :title do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
