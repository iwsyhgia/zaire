module Swagger
  module V1
    module Models
      module Reservations
        class ReservationUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :ReservationUpdateQuery do
            key :title, 'ReservationUpdateQuery'
            key :required, %i[
              room_id
              check_in_date
              check_out_date
              number_of_adults
              number_of_children
            ]
            property :room_id do
              key :type, :number
            end
            property :check_in_date do
              key :type, :string
              key :description, 'ex. 29/11/2018'
              key :pattern, DATE_FORMATTER
            end
            property :check_out_date do
              key :type, :string
              key :description, 'ex. 29/11/2018'
              key :pattern, DATE_FORMATTER
            end
            property :number_of_adults do
              key :type, :number
            end
            property :number_of_children do
              key :type, :number
            end
            property :adults_attributes do
              key :type, :array
              items do
                key :'$ref', :AdultCreateQuery
                key :title, 'Adult'
              end
            end
          end
        end
      end
    end
  end
end
