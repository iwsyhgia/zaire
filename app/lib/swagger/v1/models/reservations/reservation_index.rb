module Swagger
  module V1
    module Models
      module Reservations
        class ReservationIndex < Swagger::V1::Models::Record
          swagger_schema :ReservationIndex do
            key :title, 'Reservation'
            key :required, %i[
              id
              check_in_date
              check_out_date
              total_nights
              average_price_per_night
              total_price
              status
              number_of_adults
              number_of_children
              source
              rate
              payment_kind
              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :check_in_date do
              key :type, :date
            end
            property :check_out_date do
              key :type, :date
            end
            property :total_nights do
              key :type, :number
              key :description, 'total_nights = check_out_date - check_in_date'
            end
            property :average_price_per_night do
              key :type, :number
              key :description, 'default rate is 100 SAR'
            end
            property :total_price do
              key :type, :number
              key :description, 'total_price = total_nights * default_rate | default_rate is 100 SAR'
            end
            property :number_of_adults do
              key :type, :number
            end
            property :number_of_children do
              key :type, :number
            end
            property :rate do
              key :type, :string
            end
            property :payment_kind do
              key :type, :string
            end
            property :average_price do
              key :type, :number
            end
            property :guest do
              key :'$ref', :GuestShow
              key :title, 'Guest'
            end
            property :room do
              key :'$ref', :RoomShow
              key :title, 'Room'
            end
          end
        end
      end
    end
  end
end
