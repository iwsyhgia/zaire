module Swagger
  module V1
    module Models
      module Invoices
        class InvoiceShow < Swagger::V1::Models::Record
          swagger_schema :InvoiceShow do
            key :title, 'Invoice'
            key :required, %i[
              id
              amount

              vat_tax
              municipal_tax

              charges_with_taxes_total
              sellable_items_total
              charges_total
              payments_total
              refunds_total
              vat_total
              municipal_total
              booking_total
              paid_total
              discounts_total

              created_at
              updated_at
            ]
            property :payments do
              key :type, :array
              items do
                key :title, 'Payments'
                key :'$ref', :PaymentShow
              end
            end
            property :refunds do
              key :type, :array
              items do
                key :title, 'Refunds'
                key :'$ref', :RefundShow
              end
            end
            property :discounts do
              key :type, :array
              items do
                key :title, 'Discounts'
                key :'$ref', :DiscountShow
              end
            end
            property :charges do
              key :type, :array
              items do
                key :title, 'Charges'
                key :'$ref', :ChargeShow
              end
            end
          end
        end
      end
    end
  end
end
