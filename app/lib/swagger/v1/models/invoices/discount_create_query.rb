module Swagger
  module V1
    module Models
      module Invoices
        class DiscountCreateQuery < Swagger::V1::Models::Record
          swagger_schema :DiscountCreateQuery do
            key :required, %i[value kind description]
            property :value do
              key :type, :number
            end
            property :kind do
              key :type, :string
              key :enum, ::Invoices::Discount.kinds.keys
            end
            property :description do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
