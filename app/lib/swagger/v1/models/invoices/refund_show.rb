module Swagger
  module V1
    module Models
      module Invoices
        class RefundShow < Swagger::V1::Models::Record
          swagger_schema :RefundShow do
            key :title, 'Refund'
            key :required, %i[
              id
              payment_id
              user_id

              amount
              description
              status
              creation_way

              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :payment_id do
              key :type, :number
            end
            property :user_id do
              key :type, :number
            end
            property :amount do
              key :type, :number
            end
            property :description do
              key :type, :string
            end
            property :status do
              key :type, :string
            end
            property :creation_way do
              key :type, :string
            end
            property :invoice do
              key :'$ref', :InvoiceShow
              key :title, 'Invoice'
            end
          end
        end
      end
    end
  end
end
