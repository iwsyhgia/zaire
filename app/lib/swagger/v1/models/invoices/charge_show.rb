module Swagger
  module V1
    module Models
      module Invoices
        class ChargeShow < Swagger::V1::Models::Record
          swagger_schema :ChargeShow do
            key :title, 'Charge'
            key :required, %i[
              id
              user_id

              amount
              description
              kind
              creation_way

              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :user_id do
              key :type, :number
            end
            property :amount do
              key :type, :number
            end
            property :description do
              key :type, :string
            end
            property :kind do
              key :type, :string
            end
            property :creation_way do
              key :type, :string
            end
            property :invoice do
              key :'$ref', :InvoiceShow
              key :title, 'Invoice'
            end
          end
        end
      end
    end
  end
end
