module Swagger
  module V1
    module Models
      module Invoices
        class ChargeUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :ChargeUpdateQuery do
            key :required, %i[amount description kind]
            property :amount do
              key :type, :number
            end
            property :description do
              key :type, :string
            end
            property :kind do
              key :type, :string
              key :enum, ::Invoices::Charge.kinds.keys
            end
          end
        end
      end
    end
  end
end
