module Swagger
  module V1
    module Models
      module Invoices
        class DiscountShow < Swagger::V1::Models::Record
          swagger_schema :DiscountShow do
            key :title, 'Discount'
            key :required, %i[
              id
              user_id

              value
              kind
              description
              status
              creation_way

              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :user_id do
              key :type, :number
            end
            property :value do
              key :type, :number
            end
            property :description do
              key :type, :string
            end
            property :kind do
              key :type, :string
            end
            property :status do
              key :type, :string
            end
            property :invoice do
              key :'$ref', :InvoiceShow
              key :title, 'Invoice'
            end
          end
        end
      end
    end
  end
end
