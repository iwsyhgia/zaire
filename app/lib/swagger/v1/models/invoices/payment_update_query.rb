module Swagger
  module V1
    module Models
      module Invoices
        class PaymentUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :PaymentUpdateQuery do
            key :required, %i[amount payment_method]
            property :amount do
              key :type, :number
            end
            property :payment_method do
              key :type, :string
              key :enum, ::Invoices::Payment.payment_methods.keys
            end
          end
        end
      end
    end
  end
end
