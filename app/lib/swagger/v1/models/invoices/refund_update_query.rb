module Swagger
  module V1
    module Models
      module Invoices
        class RefundUpdateQuery < Swagger::V1::Models::Record
          swagger_schema :RefundUpdateQuery do
            key :required, %i[payment_id amount description]
            property :payment_id do
              key :type, :number
            end
            property :amount do
              key :type, :number
            end
            property :description do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
