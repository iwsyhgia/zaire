module Swagger
  module V1
    module Models
      module Invoices
        class PaymentShow < Swagger::V1::Models::Record
          swagger_schema :PaymentShow do
            key :title, 'Payment'
            key :required, %i[
              id
              user_id

              amount
              status
              creation_way
              payment_method

              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :user_id do
              key :type, :number
            end
            property :amount do
              key :type, :number
            end
            property :status do
              key :type, :string
            end
            property :creation_way do
              key :type, :string
            end
            property :payment_method do
              key :type, :string
            end
            property :invoice do
              key :'$ref', :InvoiceShow
              key :title, 'Invoice'
            end
          end
        end
      end
    end
  end
end
