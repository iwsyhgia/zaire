module Swagger
  module V1
    module Models
      class RoomShow < Swagger::V1::Models::Record
        swagger_schema :RoomShow do
          key :title, 'Room'
          key :required, %i[id number room_type_id]
          property :id do
            key :type, :integer
          end
          property :number do
            key :type, :number
          end
          property :room_type_id do
            key :type, :number
          end
        end
      end
    end
  end
end
