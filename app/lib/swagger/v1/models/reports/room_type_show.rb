module Swagger
  module V1
    module Models
      module Reports
        class RoomTypeShow < Swagger::V1::Models::Record
          swagger_schema :ReportRoomTypeShow do
            key :title, 'RoomType'
            key :required, %i[id name name_ar]
            property :id do
              key :type, :number
            end
            property :name do
              key :type, :string
            end
            property :name_ar do
              key :type, :string
            end
          end
        end
      end
    end
  end
end
