module Swagger
  module V1
    module Models
      module Reports
        class InvoiceShow < Swagger::V1::Models::Record
          swagger_schema :ReportInvoiceShow do
            key :title, 'Invoice'
            key :required, %i[
              id

              stay_fees
              stay_fees_discount
              sellable_items
              sellable_items_discount
              tax_total
              charges_with_taxes_total
              paid_amount
              unpaid_amount

              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :stay_fees do
              key :type, :number
            end
            property :stay_fees_discount do
              key :type, :number
            end
            property :sellable_items do
              key :type, :number
            end
            property :sellable_items_discount do
              key :type, :number
            end
            property :tax_total do
              key :type, :number
            end
            property :charges_with_taxes_total do
              key :type, :number
            end
            property :paid_amount do
              key :type, :number
            end
            property :unpaid_amount do
              key :type, :number
            end
            property :updated_at do
              key :type, :date
            end
            property :created_at do
              key :type, :date
            end
          end
        end
      end
    end
  end
end
