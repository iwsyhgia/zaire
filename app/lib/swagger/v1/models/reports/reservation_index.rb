module Swagger
  module V1
    module Models
      module Reports
        class ReservationIndex < Swagger::V1::Models::Record
          swagger_schema :ReportReservationIndex do
            key :title, 'Reservation'
            key :required, %i[
              id
              status

              full_name
              check_in_date
              check_out_date
              total_nights
              source
              created_at
              updated_at
            ]
            property :id do
              key :type, :number
            end
            property :check_in_date do
              key :type, :date
            end
            property :check_out_date do
              key :type, :date
            end
            property :total_nights do
              key :type, :number
            end
            property :full_name do
              key :type, :string
              key :description, 'Full name of guest'
            end
            property :source do
              key :type, :string
            end
            property :updated_at do
              key :type, :date
            end
            property :created_at do
              key :type, :date
            end
            property :room do
              key :'$ref', :RoomShow
              key :title, 'Room'
            end
            property :invoice do
              key :'$ref', :ReportInvoiceShow
              key :title, 'Invoice'
            end
            property :room_type do
              key :'$ref', :ReportRoomTypeShow
              key :title, 'Room Type'
            end
          end
        end
      end
    end
  end
end
