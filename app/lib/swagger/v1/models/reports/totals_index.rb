module Swagger
  module V1
    module Models
      module Reports
        class TotalsIndex < Swagger::V1::Models::Record
          swagger_schema :TotalsIndex do
            key :title, 'Totals'
            key :required, %i[
              stay_fees
              stay_fees_discount
              sellable_items
              sellable_items_discount
              tax_total
              charges_with_taxes_total
              paid_total
              unpaid_total
            ]
            property :stay_fees do
              key :type, :number
            end
            property :stay_fees_discount do
              key :type, :number
            end
            property :sellable_items do
              key :type, :number
            end
            property :sellable_items_discount do
              key :type, :number
            end
            property :tax_total do
              key :type, :number
            end
            property :charges_with_taxes_total do
              key :type, :number
            end
            property :paid_total do
              key :type, :number
            end
            property :unpaid_total do
              key :type, :number
            end
          end
        end
      end
    end
  end
end
