module Swagger
  module V1
    module Shared
      module SecurityParameter
        def self.extended(base)
          base.security do
            key :authorization, []
          end
        end
      end
    end
  end
end
