module Swagger
  module V1
    module Shared
      module LocaleParameter
        def self.extended(base)
          base.parameter do
            key :in,          :header
            key :name,        'X-LANGUAGE'
            key :description, 'locale'
            key :type,        :string
            key :default,     'en'
            key :enum,        %w[en ar]
          end
        end
      end
    end
  end
end
