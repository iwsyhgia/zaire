module Swagger
  module V1
    module Responses
      module NotFoundError
        def self.extended(base)
          base.response 404 do
            key :description, 'Not Found'
            schema do
              key :'$ref', :Error
            end
          end
        end
      end
    end
  end
end
