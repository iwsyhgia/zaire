module Swagger
  module V1
    module Responses
      module DestroyedSuccess
        def self.extended(base)
          base.response 200 do
            key :description, 'Successfully destroyed'
            schema do
              property :id do
                key :type, :integer
                key :description, 'Id of destroyed object'
              end
            end
          end
        end
      end
    end
  end
end
