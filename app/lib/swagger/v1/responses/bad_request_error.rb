module Swagger
  module V1
    module Responses
      module BadRequestError
        def self.extended(base)
          base.response 400 do
            key :description, 'Bad request or validation errors.'
            schema do
              key :'$ref', :Error
            end
          end
        end
      end
    end
  end
end
