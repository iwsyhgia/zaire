module Swagger
  module V1
    module Responses
      module AuthenticationError
        def self.extended(base)
          base.response 401 do
            key :description, 'Unauthorized'
            schema do
              key :'$ref', :Error
            end
          end
        end
      end
    end
  end
end
