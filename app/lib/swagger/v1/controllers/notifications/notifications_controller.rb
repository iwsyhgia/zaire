module Swagger
  module V1
    module Controllers
      module Notifications
        class NotificationsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Notifications'.freeze

          swagger_path '/notifications' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'notifications_index'
              key :description, <<-Q
                Returns the list of notifications for current user.
                Also allows to filter by kind recipient and whether notifications are read.
                Default values:
                  recipient: Current user
              Q

              parameter 'q[kind_in]'.to_sym do
                key :type, :string
                key :name, 'q[kind_in]'.to_sym
                key :in, :query
                key :description,
                  "Set of needed notification kinds. Possible: #{::Notifications::Notification.kinds.keys.join(', ')}."
              end

              parameter 'q[is_read]'.to_sym do
                key :name, 'q[is_read]'.to_sym
                key :in, :query
                key :type, :boolean
                key :description, 'Whether or not notifications are read.'
              end

              parameter 'q[recipient_type]'.to_sym do
                key :type, :string
                key :name, 'q[recipient_type]'.to_sym
                key :in, :query
                key :description,
                  'Recipient type. Possible: User, Guests::Guest. Can\'t be used without recipient_id'
              end

              parameter 'q[recipient_id]'.to_sym do
                key :type, :integer
                key :name, 'q[recipient_id]'.to_sym
                key :in, :query
                key :description,
                  'Recipient id. Can\'t be used without recipient_type'
              end

              response 200 do
                key :description, 'Get list of all notifications for user'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :NotificationIndex
                  end
                end
              end
            end
          end

          swagger_path '/notifications' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'notifications_create'
              key :description, <<-Q
                Description:
                  Create new notification
                Policy:
                  Applicable only to current user or to the guests of current user hotel.
              Q

              parameter do
                schema do
                  property :notification do
                    key :'$ref', :NotificationCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns newly created notification'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :NotificationShow
                  end
                end
              end
            end
          end

          swagger_path '/notifications/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'notifications_show'
              key :description, <<-Q
                Description:
                  Returns a notification details.
                Policy:
                  Applicable only to current user or to the guests of current user hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Notification id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Notification details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :NotificationShow
                  end
                end
              end
            end
          end

          swagger_path '/notifications/preview' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'notifications_preview'
              key :description, <<-Q
                Description:
                  Returns a notification message.
                Policy:
                  Applicable only to current user or to the guests of current user hotel.
              Q

              parameter do
                schema do
                  property :notification do
                    key :'$ref', :NotificationCreateQuery
                  end
                end
                key :in, :body
              end
            end
          end

          swagger_path '/notifications/{id}/read' do
            operation :patch do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'notifications_read'
              key :description, <<-Q
                Description:
                  Marks notification as read.
                Policy:
                  Applicable only to current user or to the guests of current user hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Notification id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Returns the notification read.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :NotificationShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
