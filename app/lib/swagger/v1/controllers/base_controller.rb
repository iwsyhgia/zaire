module Swagger
  module V1
    module Controllers
      class BaseController
        include Swagger::Blocks
      end
    end
  end
end
