module Swagger
  module V1
    module Controllers
      module Profiles
        class ProfilesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Profiles'.freeze

          swagger_path '/profiles' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'profile_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns profile info.
                Policy:
                  Can see only for current user.
              Q

              response 200 do
                key :description, 'Profile details'
                schema do
                  property :profile do
                    key :'$ref', :ProfileShow
                  end
                end
              end
            end
          end

          swagger_path '/profiles' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'profile_update'
              key :description, <<-Q
                Description:
                  Updates profile address.
                Policy:
                  Can update only for current user.
              Q

              parameter do
                schema do
                  property :profile do
                    key :'$ref', :ProfileUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :profile do
                    key :'$ref', :ProfileShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
