module Swagger
  module V1
    module Controllers
      module Reports
        class ReservationsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Reports - [Reservations]'.freeze

          swagger_path '/reports/reservations' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_index'
              key :description, <<-Q
                Returns the list of reservations. Reservations per page - 100.
                Also allows to filter by statuses, sources, dates, pages, and room type name.
                Default values:
                  statuses: unconfirmed, confirmed, checked_in, checked_out, no_show
                  sources: direct
                  page: 1

                Note: doesn't show cancelled by default.
              Q

              parameter 'page'.to_sym do
                key :type, :number
                key :name, 'page'.to_sym
                key :in, :query
              end

              parameter 'q[status_in][]'.to_sym do
                key :type, :string
                key :name, 'q[status_in][]'.to_sym
                key :in, :query
                key :description,
                  "set of needed statuses. Possible: #{::Reservations::Reservation.statuses.keys.join(', ')}."
              end
              parameter 'q[source_in][]'.to_sym do
                key :type, :string
                key :name, 'q[source_in][]'.to_sym
                key :in, :query
                key :description,
                  "set of needed sources. Possible: #{::Reservations::Reservation::SOURCES.keys.join(', ')}."
              end
              parameter 'q[check_in_date_gteq]'.to_sym do
                key :type, :string
                key :description, 'ex. 29/11/2018'
                key :name, 'q[check_in_date_gteq]'.to_sym
                key :in, :query
                key :pattern, DATE_FORMATTER
              end
              parameter 'q[check_out_date_lteq]'.to_sym do
                key :type, :string
                key :description, 'ex. 29/11/2018'
                key :name, 'q[check_out_date_lteq]'.to_sym
                key :in, :query
                key :pattern, DATE_FORMATTER
              end
              parameter 'q[room_type_name_eq]'.to_sym do
                key :type, :string
                key :description, 'ex. 29/11/2018'
                key :name, 'q[room_type_name_eq]'.to_sym
                key :in, :query
                key :pattern, DATE_FORMATTER
              end

              response 200 do
                key :description, 'Get list of all reservations in accordance with the search query'
                schema do
                  property :reservations do
                    key :type, :array
                    items do
                      key :'$ref', :ReportReservationIndex
                    end
                  end
                  property :totals do
                    key :'$ref', :TotalsIndex
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
