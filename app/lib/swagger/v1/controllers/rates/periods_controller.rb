module Swagger
  module V1
    module Controllers
      module Rates
        class PeriodsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rates - [Periods]'.freeze

          swagger_path '/rates/periods' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'periods_index'
              key :description, <<-Q
                Returns the list of periods for current hotel.
              Q

              response 200 do
                key :description, 'Get list of all periods for hotel'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PeriodShow
                  end
                end
              end
            end
          end

          swagger_path '/rates/periods/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'periods_show'
              key :description, <<-Q
                Description:
                  Returns period details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Period id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get period details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PeriodShow
                  end
                end
              end
            end
          end

          swagger_path '/rates/periods/standard' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'periods_standard'
              key :description, <<-Q
                Returns the standard period.
              Q

              response 200 do
                key :description, 'Get standard period for a hotel.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PeriodShow
                  end
                end
              end
            end
          end


          swagger_path '/rates/periods/{period_id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'periods_update'
              key :description, <<-Q
                Description:
                  Updates a period
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :period_id
                key :in, :path
                key :description, 'period_id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :period do
                    key :'$ref', :PeriodUpdate
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated period.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PeriodShow
                  end
                end
              end
            end
          end

          swagger_path '/rates/periods' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'periods_create'
              key :description, <<-Q
                Description:
                  Creates a new period.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                schema do
                  property :period do
                    key :'$ref', :PeriodCreate
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the created period.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PeriodShow
                  end
                end
              end
            end
          end

          swagger_path '/rates/periods/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'periods_destroy'
              key :description, <<-Q
                Description:
                  Destroys a period
                Policy:
                  Accessible only within current hotel.
                  Can't be destroyed if period is standard.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Period id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
