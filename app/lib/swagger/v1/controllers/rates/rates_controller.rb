module Swagger
  module V1
    module Controllers
      module Rates
        class RatesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rates'.freeze

          swagger_path '/rates/rates' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'rates_index'
              key :description, <<-Q
                Returns the list of grouped by a room type rates for current hotel.
              Q

              parameter 'q[from_date]'.to_sym do
                key :description, 'ex. 29/11/2018'
                key :name, 'q[from_date]'.to_sym
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end
              parameter 'q[to_date]'.to_sym do
                key :description, 'ex. 29/11/2018'
                key :name, 'q[to_date]'.to_sym
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end

              response 200 do
                key :description, 'Get list of grouped by a room type rates for hotel'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RateIndex
                  end
                end
              end
            end
          end

        end
      end
    end
  end
end
