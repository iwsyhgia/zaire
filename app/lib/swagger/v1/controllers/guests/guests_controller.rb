module Swagger
  module V1
    module Controllers
      module Guests
        class GuestsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Guests'.freeze

          swagger_path '/guests' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'guests_index'
              key :description, <<-Q
                Description:
                  Returns the list of guests assoiciated within the current hotel.
                  Searches for guests by phone/email/name.
                Policy:
                  Can see only for current hotel.
                  Can see guests even if reservation has been deleted.
              Q

              parameter do
                schema do
                  property :q do
                    key :'$ref', :GuestSearchQuery
                  end
                end
                key :required, false
                key :in, :body
              end

              response 200 do
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :GuestIndex
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
