module Swagger
  module V1
    module Controllers
      module Reservations
        class AdultsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Reservations - [Adults]'.freeze

          swagger_path '/reservations/adults/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'adult_update'
              key :description, <<-Q
                Description:
                  Updates an adult
                Policy:
                  Can update adult only for current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :adult do
                    key :'$ref', :AdultCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated adult.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AdultShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/adults' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'adult_create'
              key :description, <<-Q
                Description:
                  Creates new adult
                Policy:
                  Connected to reservation.
                  Accessible only within hotel.
              Q

              parameter do
                schema do
                  property :adult do
                    key :'$ref', :AdultCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns an updated adult.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AdultShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/adults/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'adult_show'
              key :description, <<-Q
                Description:
                  Returns adult details.
                Policy:
                  Accessible only within hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Adult id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get adult details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AdultShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/adults/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'adult_destroy'
              key :description, <<-Q
                Description:
                  Destroys an adult.
                Policy:
                  Accessible only within hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Adult id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
