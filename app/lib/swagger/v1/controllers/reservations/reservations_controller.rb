module Swagger
  module V1
    module Controllers
      module Reservations
        class ReservationsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Reservations'.freeze

          swagger_path '/reservations' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_index'
              key :description, <<-Q
                Returns the list of reservations.
                Also allows to filter by dates and statuses.
                Default values:
                  from_date: <today - 15 days>
                  to_date: <today + 15 days>
                  statuses: unconfirmed, confirmed, checked_in, checked_out, no_show

                Note: doesn't show cancelled by default.
              Q

              parameter 'q[status_in]'.to_sym do
                key :type, :string
                key :name, 'q[status_in]'.to_sym
                key :in, :query
                key :description,
                  "set of needed statuses. Possible: #{::Reservations::Reservation.statuses.keys.join(', ')}."
              end
              parameter 'q[from_date]'.to_sym do
                key :description, 'ex. 29/11/2018'
                key :name, 'q[from_date]'.to_sym
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end
              parameter 'q[to_date]'.to_sym do
                key :description, 'ex. 29/11/2018'
                key :name, 'q[to_date]'.to_sym
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end

              response 200 do
                key :description, 'Get list of all Reservations'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationIndex
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_update'
              key :description, <<-Q
                Description:
                  Updates a reservation
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :reservation do
                    key :'$ref', :ReservationUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_create'
              key :description, <<-Q
                Description:
                  Create new reservation with Unconfirmed status
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                schema do
                  property :reservation do
                    key :'$ref', :ReservationCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_show'
              key :description, <<-Q
                Description:
                  Returns a reservation details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Reservation id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Reservation details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}/cancel' do
            operation :patch do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_cancel'
              key :description, <<-Q
                Description:
                  Cancel reservation
                Policy:
                  Only confirmed and unconfirmed reservations can be cancelled.
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Reservation id'
                key :required, true
                key :type, :integer
              end

              parameter do
                schema do
                  property :reservation do
                    key :'$ref', :ReservationCancelQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Cancel reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}/check_in' do
            operation :patch do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_check_in'
              key :description, <<-Q
                Description:
                  Saves adults and changes reservation status to checked-in.
                Policy:
                  Only confirmed and unconfirmed reservations can be checked-in.
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Reservation id'
                key :required, true
                key :type, :integer
              end

              parameter do
                schema do
                  property :reservation do
                    key :'$ref', :ReservationUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns checked in reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}/check_out' do
            operation :patch do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_check_out'
              key :description, <<-Q
                Description:
                  Changes reservation status to checked-out.
                Policy:
                  Only checked-in reservations can be checked-out.
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Reservation id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Check out reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ReservationShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'reservations_destroy'
              key :description, <<-Q
                Description:
                  Destroys a reservation
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Reservation id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
