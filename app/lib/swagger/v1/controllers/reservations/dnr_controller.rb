module Swagger
  module V1
    module Controllers
      module Reservations
        class DNRController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Reservations - [DNR]'.freeze

          swagger_path '/reservations/dnr' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'dnr_index'
              key :description, <<-Q
                Description:
                  Returns the list of reservations with status DNR.
                Policy:
                  Accessible only within current hotel.
              Q

              response 200 do
                key :description, 'Get list of all DNRs'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :DNRIndex
                  end
                end
              end
            end
          end

          swagger_path '/reservations/dnr/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'dnr_update'
              key :description, <<-Q
                Description:
                  Updates a DNR reservation
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :dnr do
                    key :'$ref', :DNRCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated reservation.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :DNRShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/dnr' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'dnr_create'
              key :description, <<-Q
                Description:
                  Creates new reservation with status DNR.
                  Moves affected reservations if it possible.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                schema do
                  property :dnr do
                    key :'$ref', :DNRCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns newly created reservation and affected reservations.'
                schema do
                  property :dnr do
                    key :'$ref', :DNRShow
                  end
                  property :reservations do
                    key :type, :array
                    items do
                      key :'$ref', :AffectedReservation
                    end
                    key :nullable, :true
                  end
                end
              end
            end
          end

          swagger_path '/reservations/dnr/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'dnr_show'
              key :description, <<-Q
                Description:
                  Returns a DNR reservation details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'DNR id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get DNR reservation details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :DNRShow
                  end
                end
              end
            end
          end

          swagger_path '/reservations/dnr/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'dnr_destroy'
              key :description, <<-Q
                Description:
                  Destroys a DNR reservation
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'DNR reservation id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
