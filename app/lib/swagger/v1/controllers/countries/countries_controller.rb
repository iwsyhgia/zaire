module Swagger
  module V1
    module Controllers
      module Countries
        class CountriesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Countries'.freeze

          swagger_path '/countries' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'countries_index'
              key :description, <<-Q
                Description:
                  Returns the list of countries
                  The list of codes: https://en.wikipedia.org/wiki/ISO_3166-1#Officially_assigned_code_elements
                Policy:
                  No restrictions
              Q

              response 200 do
                key :description, 'Get list of all Countries'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :CountryIndex
                  end
                end
              end
            end
          end

          swagger_path '/countries/{alpha3}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'countires_show'
              key :description, <<-Q
                Description:
                  Get country details by alpha3 code.
                  The list of codes: https://en.wikipedia.org/wiki/ISO_3166-1#Officially_assigned_code_elements
                Policy:
                  No restrictions
              Q

              parameter do
                key :name, :alpha3
                key :in, :path
                key :description, 'Three-letter country code, ex: BLR'
                key :required, true
                key :type, :string
              end

              response 200 do
                key :description, 'Returns country details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :CountryShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end

