module Swagger
  module V1
    module Controllers
      module Invoices
        class PaymentsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Invoices - [Payments]'.freeze

          swagger_path '/invoices/{invoice_id}/payments' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'payments_index'
              key :description, <<-Q
                Description:
                  Returns the list of payments for an invoice.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :description, 'Invoice id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get list of Payments'
                schema do
                  key :type, :array
                  items do
                    property :payments do
                      key :'$ref', :PaymentShow
                    end
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/payments/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'payments_show'
              key :description, <<-Q
                Description:
                  Returns a payment details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Payment id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Payment details'
                schema do
                  key :'$ref', :PaymentShow
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/payments/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'payments_update'
              key :description, <<-Q
                Description:
                  Updates a payment.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Payment id'
                key :required, true
                key :type, :integer
              end

              parameter do
                schema do
                  property :payment do
                    key :'$ref', :PaymentUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated payment.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PaymentShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/payments' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'payments_create'
              key :description, <<-Q
                Description:
                  Create a new payment.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :payment do
                    key :'$ref', :PaymentCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated payment.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :PaymentShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/payments/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'payments_destroy'
              key :description, <<-Q
                Description:
                  Destroys a payment.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Payment id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
