module Swagger
  module V1
    module Controllers
      module Invoices
        class RefundsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Invoices - [Refunds]'.freeze

          swagger_path '/invoices/{invoice_id}/refunds/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'refunds_show'
              key :description, <<-Q
                Description:
                  Returns a refund details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Refund id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Refund details'
                schema do
                  key :'$ref', :RefundShow
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/refunds/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'refunds_update'
              key :description, <<-Q
                Description:
                  Updates a refund.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :required, true
                key :in, :path
                key :description, 'Refund id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :refund do
                    key :'$ref', :RefundUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated refund.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RefundShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/refunds' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'refunds_create'
              key :description, <<-Q
                Description:
                  Create a new refund.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :refund do
                    key :'$ref', :RefundCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated refund.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RefundShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/refunds/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'refunds_destroy'
              key :description, <<-Q
                Description:
                  Destroys a refund
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Refund id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
