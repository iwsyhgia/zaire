module Swagger
  module V1
    module Controllers
      module Invoices
        class ChargesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Invoices - [Charges]'.freeze

          swagger_path '/invoices/{invoice_id}/charges' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'charges_index'
              key :description, <<-Q
                Description:
                  Returns the list of charges for an invoice.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :description, 'Invoice id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get list of Charges'
                schema do
                  key :type, :array
                  items do
                    property :charges do
                      key :'$ref', :ChargeShow
                    end
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/charges/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'charges_show'
              key :description, <<-Q
                Description:
                  Returns a charge details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Charge id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Charge details'
                schema do
                  key :'$ref', :ChargeShow
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/charges/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'charges_update'
              key :description, <<-Q
                Description:
                  Updates a charge.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :required, true
                key :in, :path
                key :description, 'Charge id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :charge do
                    key :'$ref', :ChargeUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated charge.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ChargeShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/charges' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'charges_create'
              key :description, <<-Q
                Description:
                  Create a new charge.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :charge do
                    key :'$ref', :ChargeCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated charge.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :ChargeShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/charges/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'charges_destroy'
              key :description, <<-Q
                Description:
                  Destroys a charge
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Charge id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
