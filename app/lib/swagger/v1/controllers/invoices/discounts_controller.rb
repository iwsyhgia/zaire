module Swagger
  module V1
    module Controllers
      module Invoices
        class DiscountsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Invoices - [Discounts]'.freeze

          swagger_path '/invoices/{invoice_id}/discounts/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'discounts_show'
              key :description, <<-Q
                Description:
                  Returns a discount details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Discount id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Discount details'
                schema do
                  key :'$ref', :DiscountShow
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/discounts/{id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'discounts_update'
              key :description, <<-Q
                Description:
                  Updates a discount.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :required, true
                key :description, 'Discount id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :discount do
                    key :'$ref', :DiscountUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated discount.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :DiscountShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/discounts' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'discounts_create'
              key :description, <<-Q
                Description:
                  Create a new discount.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :discount do
                    key :'$ref', :DiscountCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated discount.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :DiscountShow
                  end
                end
              end
            end
          end

          swagger_path '/invoices/{invoice_id}/discounts/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'discounts_destroy'
              key :description, <<-Q
                Description:
                  Destroys a discount.
                Policy:
                  Accessible only within current hotel and invoice.
              Q

              parameter do
                key :name, :invoice_id
                key :in, :path
                key :required, true
                key :description, 'Invoice id'
                key :type, :integer
              end

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Discount id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
