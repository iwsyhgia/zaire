module Swagger
  module V1
    module Controllers
      module Invoices
        class InvoicesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Invoices'.freeze

          swagger_path '/invoices/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'invoices_show'
              key :description, <<-Q
                Description:
                  Returns an invoice details that has a invoice.
                  Invoice ID can be retrieved from a reservation.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Invoice id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Invoice details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :InvoiceShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
