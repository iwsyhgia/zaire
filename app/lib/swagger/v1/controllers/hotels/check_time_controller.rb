module Swagger
  module V1
    module Controllers
      module Hotels
        class CheckTimeController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels - [Check Time]'.freeze

          swagger_path '/hotels/check_time' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'check_time_index'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Get hotel check in and check out time.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel check in/out details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :CheckTimeIndex
                  end
                end
              end
            end
          end

          swagger_path '/hotels/check_time/{check_time_id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :operationId, 'check_time_update'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Updates hotel check in OR check out time.
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                key :name, :check_time_id
                key :in, :path
                key :description, 'check_time_id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :check_time do
                    key :'$ref', :CheckTimeShow
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :check_time do
                    key :'$ref', :CheckTimeShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
