module Swagger
  module V1
    module Controllers
      module Hotels
        class HotelsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels'.freeze

          swagger_path '/hotel' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'hotel_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns hotel info.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel details'
                schema do
                  property :room do
                    key :'$ref', :HotelShow
                  end
                end
              end
            end
          end

          swagger_path '/hotel' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'hotel_update'
              key :description, <<-Q
                Description:
                  Updates hotel address.
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                schema do
                  property :hotel do
                    key :'$ref', :HotelShow
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :hotel do
                    key :'$ref', :HotelShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
