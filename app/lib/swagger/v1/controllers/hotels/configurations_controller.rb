# frozen_string_literal: true

module Swagger
  module V1
    module Controllers
      module Hotels
        class ConfigurationsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels - [Configuration]'.freeze

          swagger_path '/hotels/configuration' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'configuration_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns hotel configuration.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel configuration details'
                schema do
                  property :configuration do
                    key :'$ref', :ConfigurationShow
                  end
                end
              end
            end
          end

          swagger_path '/hotels/configuration' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'configuration_update'
              key :description, <<-Q
                Description:
                  Updates hotel configuration.
                Defaults:
                  night_audit_time: '03:00'
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                schema do
                  property :configuration do
                    key :'$ref', :ConfigurationShow
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :configuration do
                    key :'$ref', :ConfigurationShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
