module Swagger
  module V1
    module Controllers
      module Hotels
        class AddressesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels - [Address]'.freeze

          swagger_path '/hotels/address' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'address_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns hotel address.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel address details'
                schema do
                  property :address do
                    key :'$ref', :AddressShow
                  end
                end
              end
            end
          end

          swagger_path '/hotels/address' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'address_update'
              key :description, <<-Q
                Description:
                  Updates hotel address.
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                schema do
                  property :address do
                    key :'$ref', :AddressShow
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :address do
                    key :'$ref', :AddressShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
