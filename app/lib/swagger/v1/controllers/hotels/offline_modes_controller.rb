module Swagger
  module V1
    module Controllers
      module Hotels
        class OfflineModesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels - [Offline Mode]'.freeze

          swagger_path '/hotels/offline_mode' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'offline_mode_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns hotel offline mode settings.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel offline mode details'
                schema do
                  property :room do
                    key :'$ref', :OfflineModeShow
                  end
                end
              end
            end
          end

          swagger_path '/hotels/offline_mode' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'offline_mode_update'
              key :description, <<-Q
                Description:
                  Updates hotel offline mode settings.
                Defaults:
                  When user activates offline mode we set both days fields to 7 by default
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                schema do
                  property :offline_mode do
                    key :'$ref', :OfflineModeShow
                  end
                end
                key :in, :body
              end

              response 200 do
                schema do
                  property :room do
                    key :'$ref', :OfflineModeShow
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
