module Swagger
  module V1
    module Controllers
      module Hotels
        class LogosController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Hotels - [Logo]'.freeze

          swagger_path '/hotels/logo' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::AuthenticationError

              key :operationId, 'logo_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns hotel logo.
                Defaults:
                  If there is no logo, 404 will return.
                Policy:
                  Can see only for current hotel.
              Q

              response 200 do
                key :description, 'Hotel logo'
                schema do
                  property :logo do
                    key :'$ref', :LogoShow
                  end
                end
              end
            end
          end

          swagger_path '/hotels/logo' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'logo_update'
              key :description, <<-Q
                Description:
                  Updates hotel logo.
                Policy:
                  Can update only for current hotel.
              Q

              parameter do
                schema do
                  property :logo do
                    key :title, 'hotel_images'
                    key :'$ref', :LogoCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns logo as image.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :LogoShow
                  end
                end
              end
            end
          end

        end
      end
    end
  end
end
