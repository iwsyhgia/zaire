module Swagger
  module V1
    module Controllers
      module Rooms
        class RoomsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rooms'.freeze

          swagger_path '/rooms/rooms' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'rooms_index'
              key :description, <<-Q
                Description:
                  Returns the list of rooms
                Policy:
                  Accessible only within current hotel.
              Q

              response 200 do
                key :description, 'Get list of all Rooms'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomIndex
                  end
                end
              end
            end
          end

          swagger_path '/rooms/rooms/{room_id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'rooms_update'
              key :description, <<-Q
                Description:
                  Updates a room
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :room_id
                key :in, :path
                key :description, 'room_id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :room do
                    key :'$ref', :RoomCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns updated room'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/rooms' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'rooms_create'
              key :description, <<-Q
                Description:
                  Create new room
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                schema do
                  property :room do
                    key :'$ref', :RoomCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns newly created room'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/rooms/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'rooms_show'
              key :description, <<-Q
                Description:
                  Returns room details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Room id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Room details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/rooms/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'room_destroy'
              key :description, <<-Q
                Description:
                  Destroys a room
                Policy:
                  Accessible only within current hotel.
                  Room can be destroyed only if there no any active reservations.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Room id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
