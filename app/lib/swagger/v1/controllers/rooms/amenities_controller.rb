module Swagger
  module V1
    module Controllers
      module Rooms
        class AmenitiesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rooms - [Amenities]'.freeze

          swagger_path '/rooms/amenities' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'amenities_index'
              key :description, <<-Q
                Description:
                  Returns the list of Default+Custom amenities
                Policy:
                  Custom amenities accessible only within current hotel.
              Q

              response 200 do
                key :description, 'Get list of all amenities'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AmenitiesIndex
                  end
                end
              end
            end
          end

          swagger_path '/rooms/amenities/{amenity_id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :operationId, 'amenities_update'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns the list of Default+Custom amenities
                Policy:
                  Only custom amenities can be updated.
              Q

              parameter do
                key :name, :amenity_id
                key :in, :path
                key :description, 'amenity_id'
                key :type, :integer
              end
              parameter do
                schema do
                  property :amenity do
                    key :'$ref', :AmenityCreateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated amenity.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AmenityShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/amenities' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'amenities_create'
              key :description, <<-Q
                Description:
                  Creates custom amenity by name, will automatically attached to current hotel
                Policy:
                  Only custom amenities can be created.
              Q

              parameter do
                schema do
                  property :amenity do
                    key :'$ref', :AmenityCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated amenity.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AmenityShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/amenities/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :operationId, 'amenities_show'
              key :tags, [TAG_NAME]
              key :description, <<-Q
                Description:
                  Returns the amenity details.
                Policy:
                  Custom amenity accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Amenity id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Amenity details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :AmenityShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/amenities/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'amenities_destroy'
              key :description, <<-Q
                  Description:
                    Destroys a custom amenity
                  Policy:
                    Can be destroyed only if there are no any connection with room type.
                    Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Amenity id'
                key :required, true
                key :type, :integer
              end

              parameter do
                key :name, :body
                key :in, :body

                schema do
                  property :room_type_id do
                    key :description, 'Current Room Type id'
                    key :required, false
                    key :type, %w[integer null]
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end

