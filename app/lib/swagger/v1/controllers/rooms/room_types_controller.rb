module Swagger
  module V1
    module Controllers
      module Rooms
        class RoomTypesController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rooms - [Room Types]'.freeze

          swagger_path '/rooms/room_types' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'rooms_types_index'
              key :description, <<-Q
                Description:
                  Returns the list of room types
                Policy:
                  Accessible only within current hotel.
              Q

              response 200 do
                key :description, 'Get list of all Room Types'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomTypeIndex
                  end
                end
              end
            end
          end

          swagger_path '/rooms/room_types/{room_type_id}' do
            operation :put do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'room_types_update'
              key :description, <<-Q
                Description:
                  Updates a room type
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :room_type_id
                key :in, :path
                key :description, 'room_type_id'
                key :type, :integer
              end

              parameter do
                schema do
                  property :room_type do
                    key :'$ref', :RoomTypeUpdateQuery
                  end
                end
                key :in, :body
              end

              response 200 do
                key :description, 'Returns the updated room type.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomTypeShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/room_types' do
            operation :post do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::BadRequestError

              key :tags, [TAG_NAME]
              key :operationId, 'room_types_create'
              key :description, <<-Q
                Description:
                  Create new room type
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                schema do
                  property :room_type do
                    key :'$ref', :RoomTypeCreateQuery
                  end
                end
                key :in, :body
              end

              response 201 do
                key :description, 'Returns the updated room type.'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomTypeShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/room_types/{id}' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError

              key :tags, [TAG_NAME]
              key :operationId, 'room_types_show'
              key :description, <<-Q
                Description:
                  Returns a room type details.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Room type id'
                key :required, true
                key :type, :integer
              end

              response 200 do
                key :description, 'Get Room Type details'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomTypeShow
                  end
                end
              end
            end
          end

          swagger_path '/rooms/room_types/{id}' do
            operation :delete do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError
              extend ::Swagger::V1::Responses::NotFoundError
              extend ::Swagger::V1::Responses::DestroyedSuccess

              key :tags, [TAG_NAME]
              key :operationId, 'room_types_destroy'
              key :description, <<-Q
                Description:
                  Destroys a room type
                Policy:
                  Accessible only within current hotel.
                  Can't be destroyed if there any active reservations.
              Q

              parameter do
                key :name, :id
                key :in, :path
                key :description, 'Room type id'
                key :required, true
                key :type, :integer
              end
            end
          end
        end
      end
    end
  end
end
