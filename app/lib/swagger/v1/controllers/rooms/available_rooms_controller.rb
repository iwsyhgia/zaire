module Swagger
  module V1
    module Controllers
      module Rooms
        class AvailableRoomsController < Swagger::V1::Controllers::BaseController
          TAG_NAME = 'Rooms'.freeze

          swagger_path '/rooms/available_rooms' do
            operation :get do
              extend ::Swagger::V1::Shared::LocaleParameter
              extend ::Swagger::V1::Shared::SecurityParameter
              extend ::Swagger::V1::Responses::AuthenticationError

              key :tags, [TAG_NAME]
              key :operationId, 'available_rooms_index'
              key :description, <<-Q
                Description:
                  Returns the list of rooms available for reservations based on dates.
                Policy:
                  Accessible only within current hotel.
              Q

              parameter do
                key :description, 'ex. 29/11/2018'
                key :name, :check_in_date
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end
              parameter do
                key :description, 'ex. 29/11/2018'
                key :name, :check_out_date
                key :in, :query
                key :type, :string
                key :pattern, DATE_FORMATTER
              end

              response 200 do
                key :description, 'Get list of rooms available for reservations'
                schema do
                  key :type, :array
                  items do
                    key :'$ref', :RoomIndex
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
