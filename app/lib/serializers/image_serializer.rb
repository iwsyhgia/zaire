# frozen_string_literal: true

module Serializers
  class ImageSerializer < BaseSerializer
    attributes %I[id path tag]

    def path
      origin
    end

    def origin
      object&.file&.url
    end

    def medium
      object&.file_url(:medium)
    end

    def small
      object&.file_url(:small)
    end

    def tag
      object&.tag
    end
  end
end
