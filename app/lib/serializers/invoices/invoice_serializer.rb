# frozen_string_literal: true

module Serializers
  module Invoices
    class InvoiceSerializer < BaseSerializer
      attributes %i[
        id
        amount

        vat_tax
        municipal_tax

        charges_with_taxes_total
        sellable_items_total
        charges_total
        payments_total
        refunds_total
        vat_total
        municipal_total
        booking_total
        paid_total
        discounts_total

        created_at
        updated_at
      ]

      lazy_has_many :payments,  serializer: ::Serializers::Invoices::PaymentSerializer
      lazy_has_many :refunds,   serializer: ::Serializers::Invoices::RefundSerializer
      lazy_has_many :discounts, serializer: ::Serializers::Invoices::DiscountSerializer
      lazy_has_many :charges,   serializer: ::Serializers::Invoices::ChargeSerializer
    end
  end
end
