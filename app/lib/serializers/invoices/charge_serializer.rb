# frozen_string_literal: true

module Serializers
  module Invoices
    class ChargeSerializer < BaseSerializer
      attributes %i[
        id
        amount
        description
        user_id
        kind
        creation_way

        created_at
        updated_at
      ]

      lazy_belongs_to :invoice, serializer: ::Serializers::Invoices::InvoiceSerializer
    end
  end
end
