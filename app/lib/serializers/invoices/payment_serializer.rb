# frozen_string_literal: true

module Serializers
  module Invoices
    class PaymentSerializer < BaseSerializer
      lazy_relationship :user

      attributes %i[
        id
        amount
        creation_way
        payment_method
        status
        full_name

        created_at
        updated_at

        versions
      ]

      lazy_belongs_to :invoice, serializer: ::Serializers::Invoices::InvoiceSerializer

      def full_name
        lazy_user.present? ? lazy_user.email : nil # TODO: change email to full_name
      end

      attribute :versions, if: -> { instance_options[:leaf].blank? } do
        object.versions.map { |version| self.class.new(version.reify, leaf: true).attributes }
      end
    end
  end
end
