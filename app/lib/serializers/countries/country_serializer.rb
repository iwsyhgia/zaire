# frozen_string_literal: true

module Serializers
  module Countries
    class CountrySerializer < BaseSerializer
      attributes %i[
        alpha3
        name
        name_ar
        country_code
      ]

      attribute :name do
        object&.translation('en')
      end

      attribute :name_ar do
        object&.translation('ar')
      end
    end
  end
end
