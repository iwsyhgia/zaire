# frozen_string_literal: true

module Serializers
  class BaseSerializer < ActiveModel::Serializer
    include AmsLazyRelationships::Core
  end
end
