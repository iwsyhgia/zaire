# frozen_string_literal: true

module Serializers
  module Hotels
    class LogoSerializer < BaseSerializer
      attributes %i[logo]
    end
  end
end
