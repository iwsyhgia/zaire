# frozen_string_literal: true

module Serializers
  module Hotels
    class ConfigurationSerializer < BaseSerializer
      attributes %i[night_audit_time night_audit_confirmation_required]
    end
  end
end
