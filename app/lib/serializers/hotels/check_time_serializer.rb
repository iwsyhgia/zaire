# frozen_string_literal: true

module Serializers
  module Hotels
    class CheckTimeSerializer < BaseSerializer
      attributes %i[fixed start_time end_time check_type]
    end
  end
end
