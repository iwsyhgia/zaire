# frozen_string_literal: true

module Serializers
  module Hotels
    class HotelSerializer < BaseSerializer
      attributes %i[id name name_ar]
    end
  end
end
