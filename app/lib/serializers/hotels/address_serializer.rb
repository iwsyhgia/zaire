# frozen_string_literal: true

module Serializers
  module Hotels
    class AddressSerializer < BaseSerializer
      attributes %i[id country city state address_1 address_2 fax phone_number email]
    end
  end
end
