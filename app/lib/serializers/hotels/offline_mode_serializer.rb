# frozen_string_literal: true

module Serializers
  module Hotels
    class OfflineModeSerializer < BaseSerializer
      attributes %i[id active days_before_today days_after_today]
    end
  end
end
