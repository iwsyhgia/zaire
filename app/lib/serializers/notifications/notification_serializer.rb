# frozen_string_literal: true

module Serializers
  module Notifications
    class NotificationSerializer < BaseSerializer
      attributes %i[id message_params kind read_at created_at updated_at]
    end
  end
end
