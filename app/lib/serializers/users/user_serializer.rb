# frozen_string_literal: true

module Serializers
  module Users
    class UserSerializer < ActiveModel::Serializer
      attributes %I[id email role]
    end
  end
end
