# frozen_string_literal: true

module Serializers
  module Audits
    class CurrentAuditSerializer < BaseSerializer
      include Rails.application.routes.url_helpers

      attributes %i[
        id
        status
        time_frame_begin
        time_frame_end
        confirmation_id
        confirmation_path
        started_at
        updated_at
        created_at
      ]

      def confirmation_id
        object.confirmations.find_by(user: instance_options[:current_user])&.id
      end

      def confirmation_path
        return unless confirmation_id

        confirm_api_v1_audits_confirmation_path(id: confirmation_id)
      end
    end
  end
end
