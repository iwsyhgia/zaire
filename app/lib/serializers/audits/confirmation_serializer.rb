# frozen_string_literal: true

module Serializers
  module Audits
    class ConfirmationSerializer < BaseSerializer
      attributes %i[id user_id audit_id confirmed_at created_at updated_at]
    end
  end
end
