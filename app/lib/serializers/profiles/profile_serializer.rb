# frozen_string_literal: true

module Serializers
  module Profiles
    class ProfileSerializer < BaseSerializer
      lazy_relationship :user

      attributes %i[
        id
        user_id
        email
        first_name
        last_name
        phone_number
        notifications

        created_at
        updated_at
      ]

      lazy_has_one :avatar, serializer: ::Serializers::ImageSerializer

      def email
        lazy_user.email
      end
    end
  end
end
