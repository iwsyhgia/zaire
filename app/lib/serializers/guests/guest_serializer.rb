# frozen_string_literal: true

module Serializers
  module Guests
    class GuestSerializer < BaseSerializer
      attributes %i[
        id
        title
        first_name
        last_name
        email
        birth_date
        phone_code
        phone_number
        country
        city
        rank
      ]

      # TODO: Replace with actual data
      attribute(:rank) { 'Silver' }
      attribute(:loyalty_points) { 42 }
    end
  end
end
