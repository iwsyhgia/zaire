# frozen_string_literal: true

module Serializers
  module Reports
    class RoomTypeSerializer < BaseSerializer
      attributes %i[
        id
        name
        name_ar
      ]
    end
  end
end
