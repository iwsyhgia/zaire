# frozen_string_literal: true

module Serializers
  module Reports
    class ReservationSerializer < BaseSerializer
      attributes %i[
        id
        status

        full_name
        check_in_date
        check_out_date
        total_nights
        source
        created_at
        updated_at
      ]

      lazy_has_one    :invoice,   serializer: ::Serializers::Reports::InvoiceSerializer
      lazy_has_one    :room_type, serializer: ::Serializers::Reports::RoomTypeSerializer
      lazy_belongs_to :room,      serializer: ::Serializers::Rooms::RoomSerializer

      def full_name
        object.guest.slice(:first_name, :last_name).values.join(' ')
      end

      def source
        :direct
      end
    end
  end
end
