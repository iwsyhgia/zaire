# frozen_string_literal: true

module Serializers
  module Reports
    class InvoiceSerializer < BaseSerializer
      attributes %i[
        id

        stay_fees
        stay_fees_discount
        sellable_items
        sellable_items_discount
        tax_total
        charges_with_taxes_total
        paid_total
        unpaid_total

        created_at
        updated_at
      ]
    end
  end
end
