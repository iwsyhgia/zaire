# frozen_string_literal: true

module Serializers
  module Rates
    class PeriodSerializer < ActiveModel::Serializer
      attributes %i[
        id
        name
        start_date
        end_date
        price_kind
        discount_kind
        period_kind
      ]

      has_many :periods_room_types, serializer: ::Serializers::Rates::PeriodsRoomTypeSerializer
    end
  end
end
