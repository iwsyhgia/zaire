# frozen_string_literal: true

module Serializers
  module Rates
    class PeriodsRoomTypeSerializer < ActiveModel::Serializer
      attributes %i[id room_type_id period_id discount_value rates]
    end
  end
end
