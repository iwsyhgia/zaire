# frozen_string_literal: true

module Serializers
  module Rooms
    class RateSerializer < BaseSerializer
      attributes %i[
        id
        kind
        active
        lower_bound
      ]

      attribute :upper_bound, if: -> { object.dynamic? }
    end
  end
end
