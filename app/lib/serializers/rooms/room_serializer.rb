# frozen_string_literal: true

module Serializers
  module Rooms
    class RoomSerializer < BaseSerializer
      lazy_relationship :room_type

      attributes %i[id number room_type_id room_type_name room_type_name_ar]

      def room_type_name
        lazy_room_type&.name
      end

      def room_type_name_ar
        lazy_room_type&.name_ar
      end
    end
  end
end
