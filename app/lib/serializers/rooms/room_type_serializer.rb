# frozen_string_literal: true

module Serializers
  module Rooms
    class RoomTypeSerializer < BaseSerializer
      attributes %i[
        id
        name
        name_ar
        size
        unit
        quantity
        max_occupancy
      ]

      lazy_has_one :rate, serializer: ::Serializers::Rooms::RateSerializer
      lazy_has_many :rooms, serializer: ::Serializers::Rooms::RoomSerializer
      lazy_has_many :amenities, serializer: ::Serializers::Rooms::AmenitySerializer
      lazy_has_many :images, serializer: ::Serializers::ImageSerializer
    end
  end
end
