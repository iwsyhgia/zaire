# frozen_string_literal: true

module Serializers
  module Rooms
    class AmenitySerializer < BaseSerializer
      lazy_relationship :hotel

      attributes %i[id name name_ar type room_type_ids category category_ar]

      def type
        lazy_hotel.nil? ? :default : :custom
      end

      def category
        object.category || (object&.hotel.nil? ? 'Without category' : 'Custom')
      end

      def category_ar
        object.category_ar || (object&.hotel.nil? ? 'بدون فئة' : 'عرف')
      end
    end
  end
end
