# frozen_string_literal: true

module Serializers
  module Reservations
    class ReservationSerializer < BaseSerializer
      lazy_relationship :invoice

      attributes %i[
        id
        invoice_id
        check_in_date
        check_out_date
        total_nights
        average_price_per_night
        total_price
        status
        number_of_adults
        number_of_children
        total_nights
        status
        rate
        payment_kind
        source
        description
        type
        created_at
        updated_at
      ]

      lazy_has_many   :adults,  serializer: ::Serializers::Reservations::AdultSerializer
      lazy_belongs_to :guest,   serializer: ::Serializers::Guests::GuestSerializer
      lazy_belongs_to :room,    serializer: ::Serializers::Rooms::RoomSerializer

      def invoice_id
        lazy_invoice.present? ? lazy_invoice.id : nil
      end

      def average_price_per_night
        lazy_invoice.price_per_night
      end

      def total_price
        lazy_invoice.price_total
      end

      def source
        return 'direct' if object.dnr? || object.direct?
      end
    end
  end
end
