# frozen_string_literal: true

module Serializers
  module Reservations
    class AdultSerializer < BaseSerializer
      attributes %i[
        id
        reservation_id
        title
        first_name
        last_name
        personal_id
      ]
    end
  end
end
