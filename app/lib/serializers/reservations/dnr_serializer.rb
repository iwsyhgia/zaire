# frozen_string_literal: true

module Serializers
  module Reservations
    class DNRSerializer < BaseSerializer
      attributes %i[
        id
        check_in_date
        check_out_date
        description
        created_at
        updated_at
        type
      ]

      lazy_belongs_to :room, serializer: ::Serializers::Rooms::RoomSerializer
    end
  end
end
