module Schemas
  module Rates
    class Period < Base
      ID_MAX_SIZE = 25
      NAME_MAX_SIZE = 255

      define do
        optional(:id)

        optional(:start_date).value(:date)
        optional(:end_date).value(:date)
        optional(:name).value(:string, max_size?: NAME_MAX_SIZE)

        optional(:discount_kind).value(:string, included_in?: ::Rates::Period.discount_kinds.keys)
        optional(:price_kind).value(:string, included_in?: ::Rates::Period.price_kinds.keys)
        optional(:period_kind).value(:string, included_in?: ::Rates::Period.period_kinds.keys)

        optional(:periods_room_types_attributes).each do
          schema(PeriodsRoomType.new)
        end
      end
    end
  end
end
