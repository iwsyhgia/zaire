module Schemas
  module Rates
    class Rate < Base
      MAX_SIZE = 25

      define do
        required(:lower_bound).value(:string, max_size?: MAX_SIZE)
        optional(:upper_bound).value(:string, max_size?: MAX_SIZE)
      end
    end
  end
end
