module Schemas
  module Rates
    class PeriodsRoomType < Base
      RATE = Rate.new

      define do
        optional(:id).value(:integer, gt?: 0)
        optional(:room_type_id).value(:integer, gt?: 0)
        optional(:discount_value).value(:string)

        optional(:rates).schema do
          %i[monday tuesday wednesday thursday saturday sunday].each do |day_name|
            optional(day_name).schema(RATE)
          end
        end
      end
    end
  end
end
