module Schemas
  class Base < ::Dry::Schema::JSON
    def self.call(params)
      params = params.permit!.to_h if params.is_a?(::ActionController::Parameters)
      new.call(params)
    end

    def self.call!(params)
      result = call(params)

      raise InvalidError, result.errors if result.failure?

      result
    end

    def self.top_level_keys
      new.key_map.dump.reduce([]) { |memo, v| v.is_a?(Hash) ? memo + v.keys : memo << v }.map(&:to_sym)
    end
  end
end
