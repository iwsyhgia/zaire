module Schemas
  class InvalidError < StandardError
    attr_reader :errors

    def initialize(errors = nil)
      @errors = errors.to_h
      super('Invalid Schema')
    end
  end
end
