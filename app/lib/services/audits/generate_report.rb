# frozen_string_literal: true

module Services
  module Audits
    class GenerateReport < Services::Base
      attribute :audit, ::Audits::Audit

      def perform
        return error(I18n.t('general.errors.record_not_found')) unless audit

        file_path = Rails.root.join('tmp', "audit_report_#{audit.id}_#{DateTime.current.to_i}.pdf")
        file = PDFKit.new(report_html).to_file(file_path)
        save_as_attachment(file)

        success(200)
      rescue StandardError => e
        meta = { error: e.message }
        ::Audits::ErrorLog.create(audit: audit, meta: meta, step: 'send_report')
        ::Airbrake.notify(e.message, e) unless Rails.env.development? && Rails.env.test?
        error(e.message)
      end

      private

      def report_html
        ActionController::Base.new.render_to_string(
          'audits/audits/show',
          encoding: 'UTF-8',
          layout: 'pdf/application',
          locals: {
            audit_date:            audit.night,
            hotel_name:            hotel.name || "Hotel##{hotel.id}",
            occupied_rooms_count:  checked_in_reservations_count,
            available_rooms_count: available_rooms_count,
            check_ins_count:       transitions_to_state_count(:checked_in),
            check_outs_count:      transitions_to_state_count(:checked_out),
            no_shows_count:        transitions_to_state_count(:no_show),
            cancellations_count:   transitions_to_state_count(:cancelled),
            dnrs_count:            dnrs_count,
            room_revenue:          charges.where.not(kind: 'sellable_items').sum(:base_price),
            no_shows_revenue:      charges.no_show_fee.sum(:base_price),
            cancellations_revenue: charges.cancellation_fee.sum(:base_price),
            booking_revenue:       charges.sum(:base_price),
            room_vat:              charges.where.not(kind: 'sellable_items').sum(:vat_tax_amount),
            room_municipality_tax: charges.where.not(kind: 'sellable_items').sum(:municipal_tax_amount),
            sellable_items_vat:    charges.sellable_items.sum(:vat_tax_amount)
          }
        )
      end

      def save_as_attachment(file)
        attachment = Attachment.where(attachable: audit).first_or_initialize

        attachment.file = file
        attachment.save
      end

      def hotel
        @hotel = audit.hotel
      end

      def transitions_to_state_count(state)
        ::AasmLog.
          where(
            aasm_loggable_type: 'Reservations::Reservation',
            to_state: state,
            aasm_loggable_id: hotel.reservations.where(type: 'Reservations::DirectReservation')
          ).
          where('created_at >= ?', audit.time_frame_begin).
          select('aasm_loggable_id').
          distinct.
          count
      end

      def dnrs_count
        @dnrs_count ||= reservations.dnr.count
      end

      def checked_in_reservations_count
        @checked_in_reservations_count = reservations.checked_in.count
      end

      def confirmed_non_refundable_reservations_count
        @confirmed_non_refundable_reservations_count = reservations.confirmed.non_refundable.count
      end

      def available_rooms_count
        @available_rooms_count =
          hotel.rooms.count - checked_in_reservations_count - dnrs_count - confirmed_non_refundable_reservations_count
      end

      def reservations
        @reservations ||= hotel.reservations.for_audit(audit)
      end

      def invoices
        @invoices ||= ::Invoices::Invoice.where(to: reservations)
      end

      def charges
        @charges ||= ::Invoices::Charge.where(invoice: invoices)
        @charges.for_period(audit.time_frame_begin, audit.time_frame_end).or(@charges.where(audit_id: audit.id))
      end

      def payments
        @payments ||=
          ::Invoices::Payment.where(invoice: invoices).for_period(audit.time_frame_begin, audit.time_frame_end)
      end
    end
  end
end
