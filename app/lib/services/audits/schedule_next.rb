# frozen_string_literal: true

module Services
  module Audits
    # Schedules next night audit based on hotel settings
    #
    class ScheduleNext < Services::Base
      attribute :hotel, ::Hotels::Hotel

      def perform
        return error(I18n.t('general.errors.record_not_found')) unless hotel

        @prev_audit = hotel.audits.last
        attrs = { time_frame_begin: time_frame_begin, time_frame_end: time_frame_end }

        return success(next_audit) if hotel.audits.exists?(attrs)

        next_audit = hotel.transaction do
          next_audit = hotel.audits.create(attrs)
          result     = ::Services::Audits::Initiate.call_at(time_frame_end, audit: next_audit)

          raise ActiveRecord::Rollback unless result.is_a?(::ServiceJob)

          next_audit
        end

        raise StandardError, 'An error occured while scheduling next audit.' unless next_audit

        success(next_audit)
      rescue StandardError => e
        if @prev_audit
          meta = { error: e.message }
          ::Audits::ErrorLog.create(audit: @prev_audit, meta: meta, step: 'schedule_next_audit')
        end

        error(e.message)
      end

      private

      def time_frame_begin
        @prev_audit&.time_frame_end ? @prev_audit.time_frame_end : time_frame_end - 24.hours
      end

      def time_frame_end
        hours_to_add = hotel.configuration.night_audit_time_in_hours.hours
        (@prev_audit&.time_frame_end || Time.current).utc.tomorrow.beginning_of_day + hours_to_add
      end
    end
  end
end
