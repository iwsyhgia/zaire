# frozen_string_literal: true

module Services
  module Audits
    # Starts night audit
    #
    class Perform < Services::Base
      attribute :audit, ::Audits::Audit

      def perform
        return error(I18n.t('general.errors.record_not_found')) unless audit

        audit.update(started_at: Time.now, status: :running)
        update_reservations_status
        create_stay_charges
        send_report
        schedule_next_audit if audit == hotel.audits.last
        audit.completed!
        notify_success
        success(audit)
      rescue StandardError => e
        ::Airbrake.notify(e.message, e) unless Rails.env.development? && Rails.env.test?

        audit.failed!
        notify_fail

        raise e
      end

      private

      def hotel
        @hotel ||= audit.hotel
      end

      def send_report
        result = ::Services::Audits::GenerateReport.call(audit: audit)
        return unless result.success?

        ::AuditReportsMailer.audit_report(
          email: hotel.user.email,
          audit: audit,
          report_title: I18n.t('audits.report.file.name', time: I18n.l(Time.zone.now), hotel: audit.hotel.name)
        ).deliver_now
      end

      def update_reservations_status
        hotel.reservations.must_be_marked_as_no_show(audit.time_frame_end.to_date).find_each do |reservation|
          ::Services::Reservations::Statuses::NoShowService.call(reservation: reservation, audit: audit)
        end
      end

      def create_stay_charges
        hotel.reservations.waiting_for_new_charge(audit).find_each do |reservation|
          ::Services::Invoices::Charges::GenerateCharge.call(reservation: reservation, audit: audit)
        end
      end

      def schedule_next_audit
        ::Services::Audits::ScheduleNext.call(hotel: hotel)
      end

      def notify_success
        ::Services::Notifications::Send.call(
          recipients: [hotel.user],
          message_params: { message: I18n.t('audits.success_notification') },
          kind: :audit_completed
        )
      end

      def notify_fail
        ::Services::Notifications::Send.call(
          recipients: [hotel.user],
          message_params: { message: I18n.t('audits.fail_notification') },
          kind: :audit_failed
        )
      end
    end
  end
end
