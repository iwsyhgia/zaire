# frozen_string_literal: true

module Services
  module Audits
    # Starts night audit process or requests confirmations if it's needed
    #
    class Initiate < Services::Base
      attribute :audit, ::Audits::Audit, required: true

      include Rails.application.routes.url_helpers

      def perform
        return error(I18n.t('general.errors.record_not_found')) unless audit

        request_confirmation
        success(audit, 200)
      end

      private

      def hotel
        @hotel ||= audit.hotel
      end

      def confirmation_required?
        hotel.configuration.night_audit_confirmation_required
      end

      def request_confirmation
        ::Services::Audits::Perform.call_later(audit: audit) unless confirmation_required?

        # TODO: replace with receptionists when they are ready
        audit.waiting_for_confirmation!
        confirmation = audit.confirmations.create!(user: hotel.user)
        ::Services::Notifications::Send.call(
          recipients: [hotel.user],
          message_params: {
            message: I18n.t('audits.start_notification'),
            confrimation_id: confirmation.id,
            audit_date: audit.time_frame_begin.to_date,
            confirm_audit_path: confirm_api_v1_audits_confirmation_path(id: confirmation.id)
          },
          notifiers: [::Services::Notifications::Notifiers::ActionCable],
          kind: :audit_initiated
        )
      end
    end
  end
end
