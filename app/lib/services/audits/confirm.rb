# frozen_string_literal: true

module Services
  module Audits
    # Confirm night audit
    #
    class Confirm < Services::Base
      attribute :confirmation, ::Audits::Confirmation

      def perform
        return error(I18n.t('general.errors.record_not_found')) unless confirmation

        confirmation.update(confirmed_at: Time.now)
        unless audit.confirmations.exists?(confirmed_at: nil)
          audit.confirmed!
          Perform.call(audit: audit)
        end
        success(confirmation, 200)
      end

      private

      def audit
        confirmation.audit
      end
    end
  end
end
