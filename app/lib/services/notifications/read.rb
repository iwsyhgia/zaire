# frozen_string_literal: true

module Services
  module Notifications
    # Marks notification as read
    #
    class Read < Services::Base
      attribute :notification, ::Notifications::Notification

      def perform
        notification.update!(read_at: Time.now)
        success(notification)
      end
    end
  end
end
