# frozen_string_literal: true

module Services
  module Notifications
    module Notifiers
      # Notifier that sends messages via Sms
      #
      class Sms < Notifiers::Base
        attribute :notification, ::Notifications::Notification, required: true

        def perform
          return unless kind_exists?

          notification_service = "Services::Sms::#{notification.kind.classify}".constantize
          notification_service.call(reservation_id: notification.message_params['reservation_id'],
                                    phone_number: notification.recipient.phone_number_with_code)
        end

        private

        def kind_exists?
          ::Notifications::Notification.kinds.except(
            :audit_initiated,
            :audit_completed,
            :audit_failed,
            :reservation_invoice
          ).key?(notification.kind)
        end
      end
    end
  end
end
