# frozen_string_literal: true

module Services
  module Notifications
    module Notifiers
      # Notifier that sends messages via web-sockets
      #
      class ActionCable < Notifiers::Base
        attribute :notification, ::Notifications::Notification, required: true

        def perform
          WebNotificationsChannel.broadcast_to(
            notification.recipient,
            notification.message_params.merge(kind: notification.kind)
          )
        end
      end
    end
  end
end
