# frozen_string_literal: true

module Services
  module Notifications
    module Notifiers
      # Notifier that sends messages via Mandrill
      #
      class ActionMailer < Notifiers::Base
        attribute :notification, ::Notifications::Notification, required: true

        MAILERS = {
          dates_changed: ReservationMailer,
          room_changed: ReservationMailer,
          reservation_cancelled: ReservationMailer,
          reservation_invoice: ReservationMailer
        }.freeze

        def perform
          mailer.public_send(
            notification_kind,
            email: notification.recipient.email,
            message_params: notification.message_params
          )&.deliver_later
        end

        private

        def mailer # need to find some method to get all mailers ([All mailers].map ... respond_to?(notification.kind))
          MAILERS.include?(notification.kind.to_sym) ? MAILERS[notification.kind.to_sym] : NotificationsMailer
        end

        def notification_kind
          mailer == NotificationsMailer ? :default_notification : notification.kind
        end
      end
    end
  end
end
