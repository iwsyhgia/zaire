# frozen_string_literal: true

module Services
  module Notifications
    module Notifiers
      # Base class for notification transport (SMS WebSocket Email etc).
      # Subclasses must implement send method which is responsible for message sending.
      #
      class Base < Services::Base
        attribute :notification, ::Notifications::Notification, required: true

        def call_later
          ::NotificationJob.perform_later(self.class.name, attributes)
        end
      end
    end
  end
end
