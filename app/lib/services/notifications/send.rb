# frozen_string_literal: true

module Services
  module Notifications
    # Sends notifications to the user based on his settings
    #
    class Send < Services::Base
      attribute :recipients, Array, required: true
      attribute :message_params, Hash, required: true
      attribute :kind, String
      attribute :notifiers, nil, default: [::Services::Notifications::Notifiers::ActionCable]

      def perform
        recipients.each do |recipient|
          notification = ::Notifications::Notification.create!(
            recipient: recipient,
            message_params: message_params,
            kind: kind
          )
          notifiers.each do |notifier|
            configure_notifier_name(notifier).call(notification: notification)
          end
        end
      end

      private

      def configure_notifier_name(notifier)
        return notifier if notifier == ::Services::Notifications::Notifiers::ActionCable

        {
          email: ::Services::Notifications::Notifiers::ActionMailer,
          sms:   ::Services::Notifications::Notifiers::Sms
        }.fetch(notifier.to_sym)
      end
    end
  end
end
