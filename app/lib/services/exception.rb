# frozen_string_literal: true

module Services
  class Exception < StandardError; end
end
