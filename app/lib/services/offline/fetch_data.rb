# frozen_string_literal: true

module Services
  module Offline
    class FetchData < Services::Base
      attribute :hotel, ::Hotels::Hotel

      def perform
        return success({}) unless hotel.offline_mode.active

        start_date   = (Date.current - hotel.offline_mode.days_before_today).to_formatted_s(:db)
        end_date     = (Date.current + hotel.offline_mode.days_after_today).to_formatted_s(:db)
        reservations = hotel.reservations.filtered_by_date_range(start_date, end_date).not_cancelled
        invoices     = ::Invoices::Invoice.where(reservation: reservations)

        data = {
          invoices:     serialize(invoices, ::Serializers::Invoices::InvoiceSerializer),
          reservations: serialize(reservations, ::Serializers::Reservations::ReservationSerializer),
          room_types:   serialize(hotel.room_types, ::Serializers::Rooms::RoomTypeSerializer),
          rooms:        serialize(hotel.rooms, ::Serializers::Rooms::RoomSerializer),
          hotel:        {
            offline: serialize(hotel.offline_mode, ::Serializers::Hotels::OfflineModeSerializer)
          }
        }

        success(offline_storage: data)
      end

      def serialize(resources, each_serializer)
        options = { each_serializer: each_serializer }
        ActiveModelSerializers::SerializableResource.new(resources, options).as_json
      end
    end
  end
end
