# frozen_string_literal: true

module Services
  module Payments
    module Offline
      class Pay < Services::Base
        attribute :payment

        # TODO: before send payment object to this service calculate amount based on connected Charges and Discounts
        def call
          result = transaction_service.call do |_transaction|
            balance = ::Payments::Balance.lock.for(payment.to)
            amount  = payment.amount
            result  = true # because it's an offline successful payment
            balance.update(amount: balance.amount + amount)

            payment.update(status: :paid)

            result
          end

          payment.update(status: :failed) unless result.success?
          result
        end

        private

        def transaction_service
          ::Services::Payments::Transaction.new(
            source: payment.from,
            target: payment.to,
            payment: payment,
            transfer_kind: :offline_pay
          )
        end
      end
    end
  end
end
