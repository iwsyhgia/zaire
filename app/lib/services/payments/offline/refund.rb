# frozen_string_literal: true

module Services
  module Payments
    module Offline
      class Refund < Services::Base
        attribute :payment

        # TODO: before send payment object to this service calculate refund amount based on connected pending Refunds
        def call
          if payment.paid?

            result = transaction_service.call do |_transaction|
              balance = ::Payments::Balance.lock.for(payment.to)
              amount = payment.amount
              result = true
              balance.update(amount: balance.amount - amount)

              payment.update(status: :refunded)
              result
            end

            payment.update(status: :failed) unless result.success?

            result

          else
            error('Payment status is not able to refund.')
          end
        end

        private

        def transaction_service
          ::Services::Payments::Transaction.new(
            source: payment.to,
            target: payment.from,
            payment: payment,
            transfer_kind: :offline_refund
          )
        end
      end
    end
  end
end
