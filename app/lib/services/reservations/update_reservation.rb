# frozen_string_literal: true

module Services
  module Reservations
    class UpdateReservation < ::Services::Base
      attribute :reservation, ::Reservations::Reservation

      def perform
        build_reservation_was

        exception(I18n.t('reservations.errors.dates_cannot_be_narrowed')) if dates_cannot_be_narrowed?

        reservation.transaction do
          reservation.save!
          invoice.update!(current_rate: ::Services::Rates::DetermineRate.call(reservation: reservation).value)
          ::Services::Invoices::RecalculateInvoice.call(invoice: invoice)
        end

        add_refund if refund_needed?
        send_room_changed_notification if room_type_changed?
        send_dates_changed_notification if dates_changed?

        success(reservation)
      end

      private

      def refund_needed?
        reservation.refundable? &&
          reservation.status.in?(%w[checked_in confirmed]) &&
          @price_total_was > reservation.price_total
      end

      def dates_narrowed?
        @reservation_was.check_in_date < reservation.check_in_date ||
          @reservation_was.check_out_date > reservation.check_out_date
      end

      def dates_extended?
        @reservation_was.check_in_date > reservation.check_in_date ||
          @reservation_was.check_out_date < reservation.check_out_date
      end

      def room_type_changed?
        reservation.room_type.id != @reservation_was.room_type.id
      end

      def dates_changed?
        dates_narrowed? || dates_extended?
      end

      def dates_cannot_be_narrowed?
        dates_narrowed? && reservation.status.in?(%w[checked_in confirmed]) && reservation.non_refundable?
      end

      def send_room_changed_notification
        ::Services::Notifications::Send.call(
          recipients: [reservation.guest],
          kind: :room_changed,
          notifiers: %w[email sms],
          message_params: { reservation_id: reservation.id }
        )
      end

      def send_dates_changed_notification
        ::Services::Notifications::Send.call(
          recipients: [reservation.guest],
          kind: :dates_changed,
          notifiers: %w[email sms],
          message_params: { reservation_id: reservation.id }
        )
      end

      # Receptionist adds refund to return the difference between the paid and necessary amount.
      # It'll be done automatically for refund, but for the first phase we work only with manual adding.
      def add_refund; end

      def build_reservation_was
        @price_total_was = reservation.price_total
        @reservation_was = ::Reservations::Reservation.new(reservation.attributes.merge(reservation.changed_attributes))
      end

      def invoice
        @invoice ||= reservation.invoice
      end
    end
  end
end
