# frozen_string_literal: true

module Services
  module Reservations
    class Move < ::Services::Base
      attribute :dnr, ::Reservations::DNR

      def perform
        if affected_reservations.exists?
          exception(I18n.t('dnr.errors.checked_in_exists')) if affected_reservations.exists?(status: :checked_in)
          exception(I18n.t('dnr.errors.dnr_affected'))      if affected_reservations.exists?(type: 'Reservations::DNR')
          exception(I18n.t('dnr.errors.cannot_be_moved'))   if update_attributes.nil?

          @moved_reservations = ::Reservations::Reservation.update(update_attributes.keys, update_attributes.values)
        end

        success(result)
      end

      private

      def update_attributes
        @update_attributes ||= affected_reservations.each_with_object({}) do |reservation, attrs|
          room_ids = available_room_ids_for(reservation)
          break if room_ids.blank?

          attrs[reservation.id] = { room_id: room_ids.first }
        end
      end

      def available_room_ids_for(reservation)
        query_options = {
          status: ::Reservations::Reservation.statuses[:cancelled],
          check_in_date: reservation.check_in_date.to_s(:db),
          check_out_date: reservation.check_out_date.to_s(:db),
          reservation_id: reservation.id,
          dnr_id: dnr.id,
          dnr_room_id: dnr.room_id
        }

        condition = <<-SQL
          rooms.id != :dnr_room_id AND
          reservations.status = :status OR
          reservations.room_id IS NULL OR
          reservations.id = :dnr_id OR
          (NOT (check_in_date, check_out_date) OVERLAPS (:check_in_date, :check_out_date))
        SQL

        rooms.
          left_joins(:reservations).
          where(condition, **query_options).
          where(room_type: reservation.room_type).
          where.not(id: excluded_room_ids(reservation)).
          distinct.
          ids
      end

      def excluded_room_ids(reservation)
        ::Reservations::Reservation.
          overlaps(date_pair(reservation)).
          where.not(status: :cancelled, id: dnr.id).
          select(:room_id)
      end

      def affected_reservations
        @affected_reservations ||=
          ::Reservations::Reservation.
          overlaps(date_pair(dnr)).
          where.not(id: dnr.id).
          where(room_id: dnr.room_id).
          where(status: %i[unconfirmed confirmed checked_in checked_out default_dnr cut_dnr])
      end

      def date_pair(reservation)
        reservation.slice(:check_in_date, :check_out_date).transform_values { |v| v.to_s(:db) }
      end

      def rooms
        @rooms ||= ::Services::CurrentControllerService.get.current_hotel.rooms
      end

      def result
        @moved_reservations || []
      end
    end
  end
end
