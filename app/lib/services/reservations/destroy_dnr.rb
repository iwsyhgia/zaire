module Services
  module Reservations
    class DestroyDNR < ::Services::Base
      attribute :dnr, ::Reservations::DNR

      def perform
        return error(I18n.t('dnr.errors.already_deleted')) if dnr.deleted?
        return error(I18n.t('dnr.errors.cannot_be_deleted')) if cannot_be_deleted?

        if audit_start_date >= dnr.check_in_date
          change_dates
          success(::Serializers::Reservations::DNRSerializer.new(dnr))
        else
          dnr.destroy!
          success(id: dnr.id)
        end
      end

      private

      def change_dates
        dnr.check_out_date = last_audit.time_frame_end
        dnr.status = 'cut_dnr'
        dnr.save!
      end

      def audit_start_date
        last_audit&.time_frame_begin&.to_date || Date.yesterday
      end

      def last_audit
        dnr.hotel.audits.order(:created_at).completed.last
      end

      def cannot_be_deleted?
        Date.current >= dnr.check_out_date
      end
    end
  end
end
