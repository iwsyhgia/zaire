# frozen_string_literal: true

module Services
  module Reservations
    class CreateReservation < ::Services::Base
      attribute :reservation, ::Reservations::Reservation

      def perform
        reservation.transaction do
          current_rate = ::Services::Rates::DetermineRate.call(reservation: reservation).value
          reservation.build_invoice(from: reservation.guest, current_rate: current_rate)
          reservation.save!
          ::Services::Invoices::RecalculateInvoice.call(invoice: reservation.invoice)
        end

        success(reservation)
      end
    end
  end
end
