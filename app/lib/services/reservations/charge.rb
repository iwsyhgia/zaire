# frozen_string_literal: true

module Services
  module Reservations
    class Charge < Services::Base
      attribute :reservation, ::Reservations::Reservation
      attribute :amount
      attribute :description

      def perform
        add_charge
        update_charges_total
      end

      private

      def active_payment
        @active_payment ||=
          reservation.payments.pending.last ||
          reservation.payments.create(from: reservation.guest, to: reservation)
      end

      def add_charge
        active_payment.charges.create(description: description, amount: amount)
      end

      def update_charges_total
        active_payment.update(charges_total: active_payment.charges.sum(:amount))
      end
    end
  end
end
