# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class NoShowService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation
        attribute :audit, ::Audits::Audit, optional: true

        def perform
          return error(I18n.t('reservations.errors.already_no_show')) if reservation.no_show?
          return error(I18n.t('reservations.errors.cannot_be_no_showed')) unless reservation.may_no_show?

          result = ::Services::Invoices::Charges::GenerateCharge.call(reservation: reservation,
                                                                      kind: 'no_show_fee',
                                                                      audit: audit)
          update_reservation if result.success?

          raise StandardError, 'An error happen while generating charge or saving reservation.' if result.failure?

          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          ::Airbrake.notify(e.message, e) unless Rails.env.development? && Rails.env.test?
          error(e.message)
        end

        protected

        def update_reservation
          if must_be_cut?
            reservation.no_show! do
              new_check_out = reservation.check_in_date + 1.day
              reservation.update!(check_out_date: new_check_out)
            end
          else
            reservation.no_show!
          end
        end

        def must_be_cut?
          new_check_out = reservation.check_in_date + 1.day

          return false unless audit
          return false if audit.time_frame_end.to_date >= new_check_out && reservation.check_out_date == new_check_out
          return false if reservation.non_refundable? && reservation.confirmed?

          true
        end
      end
    end
  end
end
