# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class CheckOutService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation

        def perform
          return error(I18n.t('reservations.errors.already_checked_out')) if reservation.checked_out?
          return error(I18n.t('reservations.errors.cannot_be_checked_out')) unless can_check_out?

          reservation.check_out!
          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          error(e.message)
        end

        private

        def can_check_out?
          reservation.may_check_out?
        end
      end
    end
  end
end
