# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class ConfirmService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation

        def perform
          return error(I18n.t('reservations.errors.already_confirmed')) if reservation.confirmed?
          return error(I18n.t('reservations.errors.cannot_be_confirmed')) unless reservation.may_confirm?

          reservation.confirm!

          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          error(e.message)
        end
      end
    end
  end
end
