# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class UnconfirmService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation

        def perform
          return error(I18n.t('reservations.errors.already_unconfirmed')) if reservation.unconfirmed?
          return error(I18n.t('reservations.errors.cannot_be_unconfirmed')) unless reservation.may_unconfirm?

          reservation.unconfirm!

          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          error(e.message)
        end
      end
    end
  end
end
