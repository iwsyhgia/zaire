# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class CheckInService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation

        def perform
          return error(I18n.t('reservations.errors.already_checked_in')) if reservation.checked_in?
          return error(I18n.t('reservations.errors.cannot_be_checked_in')) unless can_check_in?
          return error(I18n.t('reservations.errors.only_current_date_check_in')) unless check_in_for_today?

          reservation.transaction do
            reservation.save! # update adults info
            reservation.check_in!
          end

          notify

          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          error(e.message)
        end

        private

        def notify
          WelcomeMailer.welcome_after_check_in(reservation)&.deliver_later
          ::Services::Notifications::Send.call(
            recipients: [reservation.guest],
            kind: :after_check_in,
            notifiers: ['sms'],
            message_params: { reservation_id: reservation.id }
          )
        end

        def can_check_in?
          !reservation.hotel.initiated_audit? && reservation.may_check_in?
        end

        def check_in_for_today?
          DateTime.current.ago(hotel.configuration.night_audit_time_in_seconds).to_date == reservation.check_in_date
        end

        def hotel
          @hotel = reservation.hotel
        end
      end
    end
  end
end
