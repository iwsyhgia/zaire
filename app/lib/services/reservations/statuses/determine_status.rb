# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class DetermineStatus < ::Services::Base
        attribute :reservation, ::Reservations::Reservation

        def perform
          ::Services::Reservations::Statuses::ConfirmService.call(reservation: reservation) if must_be_confirmed?
          ::Services::Reservations::Statuses::UnconfirmService.call(reservation: reservation) if must_be_unconfirmed?
        end

        private

        def must_be_unconfirmed?
          reservation.confirmed? && (
            current_balance < night_price && first_night_for_confirmation? ||
            current_balance < full_stay_price && full_stay_for_confirmation?
          )
        end

        def must_be_confirmed?
          reservation.unconfirmed? && (
            current_balance >= night_price && first_night_for_confirmation? ||
            current_balance >= full_stay_price && full_stay_for_confirmation?
          )
        end

        def full_stay_for_confirmation?
          reservation.refundable? && reservation.full_stay? || reservation.non_refundable?
        end

        def first_night_for_confirmation?
          reservation.refundable? && reservation.first_night?
        end

        def current_balance
          # initialy balance is equal to a negative full stay price
          @current_balance ||= reservation.invoice.amount + full_stay_price
        end

        def night_price
          reservation.price_per_night_with_taxes
        end

        def full_stay_price
          reservation.price_total_with_taxes
        end
      end
    end
  end
end
