# frozen_string_literal: true

module Services
  module Reservations
    module Statuses
      class CancelService < ::Services::Base
        attribute :reservation, ::Reservations::Reservation
        attribute :cancellation_fee

        def perform
          return error(I18n.t('reservations.errors.already_cancelled')) if reservation.cancelled?
          return error(I18n.t('reservations.errors.cannot_be_cancelled')) if cannot_be_cancelled?

          if cancellation_fee
            ::Services::Invoices::Charges::GenerateCharge.call(reservation: reservation, kind: 'cancellation_fee')
          end

          reservation.cancel!

          notify

          success(::Serializers::Reservations::ReservationSerializer.new(reservation))
        rescue StandardError => e
          error(e.message)
        end

        private

        def notify
          ::Services::Notifications::Send.call(
            recipients: [reservation.guest],
            kind: :reservation_cancelled,
            notifiers: %w[email sms],
            message_params: { reservation_id: reservation.id }
          )
        end

        def cannot_be_cancelled?
          reservation.dnr? || !reservation.may_cancel?
        end
      end
    end
  end
end
