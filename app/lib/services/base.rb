# frozen_string_literal: true

module Services
  class Result
    attr_accessor :value, :status

    def initialize(value, status)
      @value = value
      @status = status
    end

    def success?
      is_a? Success
    end

    def failure?
      is_a? Error
    end
  end

  class Success < Result; end
  class Error < Result; end

  class Base
    include Pundit
    include Virtus.model
    # prepend Services::Logging

    #  Shortcut
    #
    def self.call(*args, &block)
      new(*args, &block).call
    end

    def self.call_later(*args, &block)
      new(*args, &block).call_later
    end

    def self.call_at(*args, &block)
      time = args.shift
      new(*args, &block).call_at(time)
    end

    #  Here is the actual logic should run
    #
    def perform
      raise "You must override #perform in class #{self.class.name}"
    end

    #  Wrapper for performing the action
    #
    def call(*params, &block)
      perform(*params, &block)
    end

    #  Action scheduler wrapper
    def call_later
      ::ServiceJob.perform_later(self.class.name, attributes)
    end

    def call_at(time)
      ::ServiceJob.new(self.class.name, attributes).enqueue(wait_until: time)
    end

    protected

    def pundit_user
      CurrentControllerService.get.current_user
    end

    def current_hotel
      CurrentControllerService.get.current_hotel
    end

    # result(value = nil, status = nil)

    def success(value = nil, status = 200)
      ::Services::Success.new(value, status)
    end

    private

    def error(value = nil, status = 400)
      ::Services::Error.new(value, status)
    end

    def exception(message)
      raise ::Services::Exception, message
    end
  end
end
