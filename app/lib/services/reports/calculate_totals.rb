# frozen_string_literal: true

module Services
  module Reports
    class CalculateTotals < Services::Base
      attribute :reservations

      def perform
        invoice_attributes.each { |attr| totals[attr] = invoices.sum(&attr) }
        totals[:items_number] = reservations.size
        success(totals)
      rescue StandardError => e
        error(e.message)
      end

      def totals
        @totals ||= Hash[invoice_attributes.product([0])]
      end

      def invoices
        @invoices ||= reservations.includes(:invoice).map(&:invoice).compact
      end

      def invoice_attributes
        %i[
          stay_fees
          stay_fees_discount
          sellable_items
          sellable_items_discount
          tax_total
          charges_with_taxes_total
          paid_total
          unpaid_total
        ]
      end
    end
  end
end
