# frozen_string_literal: true

#  Service for remembering the controller instances
#
module Services
  class CurrentControllerService
    def self.controllers
      @controllers ||= {}
    end

    Finalizer = lambda do |thread_id|
      controllers.delete thread_id
    end

    def self.set(controller)
      ObjectSpace.define_finalizer Thread.current, Finalizer unless controllers.key?(thread_id)
      controllers[thread_id] = controller
    end

    def self.get
      controllers[thread_id]
    end

    def self.thread_id
      Thread.current.object_id
    end
  end
end
