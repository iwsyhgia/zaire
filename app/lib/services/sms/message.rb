# frozen_string_literal: true

module Services
  module Sms
    class Message < Services::Base
      attribute :phone_number
      attribute :reservation_id

      def perform
        return unless phone_number.present? && reservation_id.present?

        @reservation = ::Reservations::Reservation.find(reservation_id)
        return unless @reservation

        ::Services::Sms::Send.call_later(phone_number: phone_number, text: message_text)
      end

      private

      def message_text
        raise 'Please override this method'
      end
    end
  end
end
