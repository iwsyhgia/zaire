# frozen_string_literal: true

require 'net/http'

module Services
  module Sms
    class Send < Services::Base
      attribute :phone_number
      attribute :text

      ENDPOINT = 'https://www.safa-sms.com/api/sendsms.php'

      def perform
        return unless phone_number.present? && Flipper.enabled?(:feature_sms)

        response = Faraday.post(ENDPOINT,
          username: sms[:username],
          password: sms[:password],
          message: text,
          sender: sms[:username],
          numbers: phone_number)

        code = response.body

        if code == '100'
          success(I18n.t('sms.code_100'))
        else
          error(error_by_code(code))
        end
      end

      private

      def sms
        Rails.application.secrets.sms
      end

      def error_by_code(code)
        I18n.t("sms.errors.code_#{code}")
      end
    end
  end
end
