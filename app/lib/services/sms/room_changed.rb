# frozen_string_literal: true

module Services
  module Sms
    class RoomChanged < Sms::Message
      private

      def message_text
        I18n.t(
          'sms.messages.room_changed',
          reservation_id: @reservation.id,
          first_name: @reservation.guest.first_name,
          new_room_type: @reservation.room.room_type.name,
          total_stay_fee_amount: @reservation.price_total_with_taxes
        )
      end
    end
  end
end
