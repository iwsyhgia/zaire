# frozen_string_literal: true

module Services
  module Sms
    class DatesChanged < Sms::Message
      private

      def message_text
        I18n.t(
          'sms.messages.dates_changed',
          first_name: @reservation.guest.first_name,
          check_in_date: @reservation.check_in_date,
          check_out_date: @reservation.check_out_date,
          total_stay_fee_amount: @reservation.price_total_with_taxes
        )
      end
    end
  end
end
