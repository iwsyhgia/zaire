# frozen_string_literal: true

module Services
  module Sms
    class AfterCheckIn < Sms::Message
      private

      def message_text
        I18n.t(
          'sms.messages.after_check_in',
          first_name: @reservation.guest.first_name,
          hotel_name: @reservation.hotel.name
        )
      end
    end
  end
end
