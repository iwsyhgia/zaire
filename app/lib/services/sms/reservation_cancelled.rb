# frozen_string_literal: true

module Services
  module Sms
    class ReservationCancelled < Sms::Message
      private

      def message_text
        I18n.t(
          'sms.messages.reservation_cancelled',
          reservation_id: @reservation.id,
          first_name: @reservation.guest.first_name
        )
      end
    end
  end
end
