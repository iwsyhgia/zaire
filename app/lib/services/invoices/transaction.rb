# frozen_string_literal: true

# Wraps the block given into invoice transaction
#
module Services
  module Invoices
    class Transaction < Services::Base
      attribute :source
      attribute :target
      attribute :document
      attribute :kind

      def perform(block = Proc.new)
        @source_transaction = create!(source, target)
        commit!(block)
        confirm!(@source_transaction)

        success(@source_transaction)
      rescue StandardError => e
        rollback!(@source_transaction)
        error(e)
      end

      def amount
        @amount ||= @document.amount
      end

      # Creates transaction and invoice attempts
      #
      def create!(source, target)
        income_sign = source == @source ? -1 : 1
        ::Invoices::Transaction.create(
          source: source,
          amount: income_sign * amount,
          target: target,
          document: @document,
          kind: @kind,
          status: :pending
        )
      end

      # Commits the transaction
      #
      def commit!(block)
        ::ActiveRecord::Base.transaction do
          @target_transaction = create!(target, source) if target
          run!(block)
          lock!
          update_balance!

          confirm!(@target_transaction) if target
        end
      end

      # Runs the external block, updates transaction with the meta
      # Runs out of the DB transaction not to block a table for a long time
      #
      def run!(block = Proc.new)
        meta = block.call(@source_transaction)
        @source_transaction.update_attributes!(meta: meta)
      end

      def lock!
        (@source_balance = ::Invoices::Balance.lock.for(source))
        (@target_balance = ::Invoices::Balance.lock.for(target)) if @target
      end

      def confirm!(transaction)
        transaction.update_attributes!(status: :successful)
      end

      def rollback!(transaction)
        transaction.update_attributes!(status: :failed)
      end

      def update_balance!
        raise I18n.t('.transactions.errors.not_enough_money') if @source_balance.amount < amount

        @source_balance.update(amount: @source_balance.amount - amount)
        @target_balance&.update(amount: @target_balance.amount + amount) if @target

        @source_transaction.update(balance: @source_balance.amount)
        @target_transaction.update(balance: @target_balance.amount) if @target
      end
    end
  end
end
