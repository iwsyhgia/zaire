# frozen_string_literal: true

module Services
  module Invoices
    module Charges
      class ChargeAmountCalculator < Services::Base
        attribute :reservation, ::Reservations::DirectReservation
        attribute :kind, String, default: 'stay_fee'

        def perform
          return error unless reservation&.invoice

          amounts_hash =
            case kind
            when 'no_show_fee'
              calculated_no_show_fee
            when 'cancellation_fee'
              calculated_cancellation_fee
            when 'stay_fee'
              calculated_stay_fee
            end

          success(amounts_hash)
        end

        def calculated_no_show_fee
          nights_count   = reservation.confirmed? && reservation.non_refundable? ? reservation.total_nights : 1
          base_amount    = base_price * nights_count
          vat_tax_amount = base_amount.take_percent(reservation.invoice.vat_tax)

          {
            base_price:           base_amount,
            amount:               base_amount + vat_tax_amount,
            municipal_tax_amount: 0,
            vat_tax_amount:       vat_tax_amount
          }
        end

        def calculated_cancellation_fee
          vat_tax_amount = base_price.take_percent(reservation.invoice.vat_tax)

          {
            base_price:           base_price,
            amount:               base_price + vat_tax_amount,
            municipal_tax_amount: 0,
            vat_tax_amount:       vat_tax_amount
          }
        end

        def calculated_stay_fee
          vat_tax_amount       = base_price.take_percent(reservation.invoice.vat_tax)
          municipal_tax_amount = base_price.take_percent(reservation.invoice.municipal_tax)

          {
            base_price:           base_price,
            amount:               base_price + municipal_tax_amount + vat_tax_amount,
            municipal_tax_amount: municipal_tax_amount,
            vat_tax_amount:       vat_tax_amount
          }
        end

        def base_price
          reservation.invoice.current_rate
        end
      end
    end
  end
end
