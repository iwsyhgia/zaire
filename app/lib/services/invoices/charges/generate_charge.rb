# frozen_string_literal: true

module Services
  module Invoices
    module Charges
      class GenerateCharge < Services::Base
        attribute :reservation, ::Reservations::Reservation
        attribute :audit, ::Audits::Audit, optinal: true
        attribute :kind, String, default: 'stay_fee'

        def perform
          if kind != 'no_show_fee' && !reservation.nights_as_text.include?(night)
            return error(I18n.t('reservations.errors.no_such_night'))
          end

          if reservation.invoice.charges.where(description: night).exists?
            return error(I18n.t('reservations.errors.already_charged'))
          end

          charge = create_charge!
          raise StandardError, 'Error happen while saving charge and recalculating totals.' unless charge

          success(charge)
        rescue StandardError => e
          ::Airbrake.notify(e.message, e) unless Rails.env.development? && Rails.env.test?
          log_error(e)
        end

        private

        def night
          audit ? audit.night : reservation.nights_as_text.first
        end

        def create_charge!
          reservation.transaction do
            amounts = ::Services::Invoices::Charges::ChargeAmountCalculator.call(reservation: reservation, kind: kind).
                      value
            charge  = reservation.invoice.charges.create!(
              {
                description:  night,
                audit_id:     audit&.id,
                creation_way: 'automated',
                kind:         kind
              }.merge(amounts)
            )

            result = ::Services::Invoices::RecalculateInvoice.call(statement: charge)
            raise ActiveRecord::Rollback unless result.success?

            charge
          end
        end

        def log_error(error_obj)
          if audit
            meta = { error: error_obj.message, reservation_was: reservation.attributes }
            log = ::Audits::ErrorLog.create(
              audit: audit, auditable: reservation, meta: meta, step: 'create_stay_charges'
            )
          end

          error(log || error_obj)
        end
      end
    end
  end
end
