# frozen_string_literal: true

module Services
  module Invoices
    class RecalculateInvoice < Services::Base # rubocop:disable Metrics/ClassLength
      attribute :statement
      attribute :invoice

      def perform
        raise I18n.t('invoices.errors.discount_exceeded') if stay_fees_total < discounts_total

        build_update_attributes
        invoice.update!(**update_attributes)
        success(invoice)
      end

      protected

      def build_update_attributes # rubocop:disable Metrics/AbcSize
        update_attributes[:charges_with_taxes_total]   = charges_with_taxes_and_discounts_total - booking_total
        update_attributes[:sellable_items_total]       = sellable_items_total
        update_attributes[:discounts_total]            = discounts_total
        update_attributes[:payments_total]             = payments_total
        update_attributes[:booking_total]              = booking_total
        update_attributes[:charges_total]              = charges_total
        update_attributes[:refunds_total]              = refunds_total
        update_attributes[:amount]                     = amount

        update_attributes[:price_total]                = price_total
        update_attributes[:price_total_with_taxes]     = price_total_with_taxes
        update_attributes[:price_per_night]            = price_total / reservation.total_nights
        update_attributes[:price_per_night_with_taxes] = price_total_with_taxes / reservation.total_nights
        update_attributes[:paid_total]                 = paid_total
        update_attributes[:unpaid_total]               = price_total - paid_total
        update_attributes[:vat_total]                  = vat_total
        update_attributes[:municipal_total]            = municipal_total
        update_attributes[:sellable_items_total]       = charges.sellable_items.sum(:amount)
        update_attributes[:stay_fees_total]            = charges.stay_fee.sum(:amount)
      end

      # Total charges with taxes = Sum of all the charges + All the taxes - All discounts
      def charges_with_taxes_and_discounts_total
        @charges_with_taxes_and_discounts_total ||=
          (charges.sellable_items + invoice.discounts).sort_by(&:created_at).
          reduce(stay_fees_total_with_taxes) do |memo, statement|
            case statement
            when ::Invoices::Charge
              memo + statement.amount * taxes_multiplier
            when ::Invoices::Discount
              memo - (statement.percent? ? memo.take_percent(statement.value) : statement.value)
            end
          end
      end

      # Total booking = Sum of all the Stay fees charges (without sellable items) - All the Discounts (without taxes)
      def booking_total
        @booking_total ||= invoice.discounts.order(:created_at).reduce(stay_fees_total_with_taxes) do |memo, statement|
          memo - (statement.percent? ? memo.take_percent(statement.value) : statement.value)
        end
      end

      def amount
        @amount ||= payments_total - refunds_total - charges_with_taxes_and_discounts_total
      end

      def discounts_total
        @discounts_total ||=
          (sellable_items_total + stay_fees_total) * taxes_multiplier - charges_with_taxes_and_discounts_total
      end

      def payments_total
        @payments_total ||= invoice.payments.sum(:amount)
      end

      def refunds_total
        @refunds_total ||= invoice.refunds.sum(:amount)
      end

      # Total charges = Sum of all the charges (stay fees and sellable items, that were entered, without taxes)
      def charges_total
        @charges_total ||=
          sellable_items_total + charges.where(kind: %i[stay_fee cancellation_fee no_show_fee]).sum(:amount)
      end

      private

      def charges_with_taxes_total
        @charges_with_taxes_total ||= charges_total * taxes_multiplier
      end

      def stay_fees_total
        @stay_fees_total ||= reservation.price_total
      end

      def stay_fees_total_with_taxes
        @stay_fees_total_with_taxes ||= stay_fees_total * taxes_multiplier
      end

      def sellable_items_total
        @sellable_items_total ||= charges.sellable_items.sum(:amount)
      end

      def taxes_multiplier
        @taxes_multiplier ||= 1 + (invoice.vat_tax + invoice.municipal_tax) / 100
      end

      def paid_total
        payments_total - refunds_total
      end

      def vat_total
        (booking_total * invoice.vat_tax / total_percent).round(2)
      end

      def municipal_total
        (booking_total * invoice.municipal_tax / total_percent).round(2)
      end

      def total_percent
        100 + invoice.vat_tax + invoice.municipal_tax
      end

      def price_total
        price_per_night * reservation.total_nights
      end

      def price_total_with_taxes
        price_per_night * reservation.total_nights * taxes_multiplier
      end

      def price_per_night
        invoice.current_rate
      end

      def price_per_night_with_taxes
        price_per_night * taxes_multiplier
      end

      def charges
        @charges ||= invoice.charges
      end

      def invoice
        @invoice ||= statement.invoice
      end

      def reservation
        @reservation ||= invoice.reservation
      end

      def update_attributes
        @update_attributes ||= {}
      end
    end
  end
end
