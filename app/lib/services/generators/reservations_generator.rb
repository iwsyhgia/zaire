# frozen_string_literal: true

module Services
  module Generators
    class ReservationsGenerator < Services::Base
      attribute :params

      def perform
        return error('No rooms available') if available_rooms.empty?

        available_rooms.map do |room|
          @reservation = ::Reservations::DirectReservationGenerator.new(reservation_params.merge(room_id: room.id))
          @reservation.transaction do
            create_guest
            @reservation.save!
            ::Services::Invoices::RecalculateInvoice.call(invoice: @reservation.build_invoice(from: @reservation.guest))

            if @reservation.confirmed?
              payment = @reservation.invoice.payments.create!(
                creation_way: :manual,
                payment_method: :cash,
                amount: @reservation.price_total
              )
              ::Services::Invoices::RecalculateInvoice.call(statement: payment)
            end

            if @reservation.checked_in?
              @reservation.adults.create!(
                first_name:  Faker::Name.first_name,
                last_name:   Faker::Name.last_name,
                personal_id: 'PersonalID'
              )
            end

            unless @reservation.unconfirmed?
              @reservation.aasm_logs.create!(from_state: 'unconfirmed', to_state: @reservation.status)
            end
          end
        end
        success(200)
      rescue ::Services::Exception => e
        e.message
      end

      def create_guest
        @reservation.build_guest(
          birth_date: 18.years.ago.to_date,
          city:       'City',
          country:    'SAU',
          first_name: Faker::Name.first_name,
          last_name:  Faker::Name.last_name,
          email:      "email#{::Guests::Guest.maximum(:id).next}@example.com",
          phone_code: 966
        )
      end

      def available_rooms
        hotel.rooms.available_for(params[:check_in_date], params[:check_out_date]).take(params[:count])
      end

      def reservation_params
        params.except(:hotel_id, :count).merge(number_of_adults: 1)
      end

      def hotel
        @hotel = ::Hotels::Hotel.find(params[:hotel_id])
      end
    end
  end
end
