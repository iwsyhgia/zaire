# frozen_string_literal: true

module Services
  module Generators
    class HotelGenerator < Services::Base
      attribute :params

      def perform
        user = User.create(email: params[:user][:email],
                           password: 'Zaire123',
                           password_confirmation: 'Zaire123',
                           confirmed_at: DateTime.now)

        return error(user.errors.full_messages.join(', ')) unless user.id

        @hotel = Services::Hotels::Init.call(user: user).value

        create_room_types(room_types_params[:room_types_count], room_types_params[:rooms_count])

        @hotel.save!
        success(@hotel, 200)
      rescue Services::Exception => e
        error(e.message)
      end

      def create_room_types(room_types_count, rooms_count)
        room_types_count.times do
          @hotel.room_types.build(
            max_occupancy:    4,
            name:             Faker::Science.unique.element,
            quantity:         rooms_count,
            rooms_attributes: Array.new(rooms_count) { { number: ((1..10_000).to_a - @hotel.rooms.pluck(:number)).sample } }
          )
        end
      end

      def room_types_params
        {
          room_types_count: params[:room_types][:room_types_count].to_i,
          rooms_count: params[:room_types][:rooms_count].to_i
        }.compact
      end
    end
  end
end
