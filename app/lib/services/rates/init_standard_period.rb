# frozen_string_literal: true

module Services
  module Rates
    class InitStandardPeriod < Services::Base
      attribute :hotel, ::Hotels::Hotel

      def perform
        period = hotel.periods.find_or_create_by(period_kind: :standard, name: I18n.t('periods.standard_name'))

        hotel.room_types.includes(:rate).find_each do |room_type|
          ::Services::Rates::MapToStandardPeriod.call(room_type: room_type, hotel: hotel)
        end

        success(period)
      end
    end
  end
end
