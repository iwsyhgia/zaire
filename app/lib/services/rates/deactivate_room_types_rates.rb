# frozen_string_literal: true

module Services
  module Rates
    class DeactivateRoomTypesRates < Services::Base
      attribute :period, ::Reservations::Reservation

      def call
        return if period.special?

        room_type_ids = period.periods_room_types.reduce([]) do |memo, record|
          record.changed.include?('rates') ? memo << record.room_type.id : memo
        end

        rates = ::Rooms::Rate.where(room_type_id: room_type_ids, active: true).update(active: false)

        success(rates)
      end
    end
  end
end
