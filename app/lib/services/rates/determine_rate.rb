# frozen_string_literal: true

module Services
  module Rates
    class DetermineRate < Services::Base
      attribute :date, Date
      attribute :room_type, ::Rooms::RoomType
      attribute :reservation, ::Reservations::Reservation

      def call
        result = lower_bound if period.price_fixed? || count_booked_rooms.zero?
        result ||= upper_bound if period.price_dynamic? && count_booked_rooms == total_rooms

        result ||= (upper_bound - lower_bound) / count_available_rooms + lower_bound

        success(apply_discount(result))
      end

      private

      def apply_discount(result)
        return result unless reservation&.non_refundable?

        value = periods_room_type.discount_value
        period.discount_percent? ? result * (1 - value.to_d / 100) : result - value
      end

      def count_available_rooms
        total_rooms - count_booked_rooms
      end

      def count_booked_rooms
        current_hotel.
          reservations.
          where(room_types: { id: room_type.id }).
          where('(check_in_date, check_out_date) OVERLAPS (?, ?)', date, date.next_day).
          count
      end

      def total_rooms
        current_hotel.rooms.where(room_type: room_type).count
      end

      def lower_bound
        @lower_bound ||= periods_room_type.lower_bound(day_name)
      end

      def upper_bound
        @upper_bound ||= periods_room_type.upper_bound(day_name)
      end

      def day_name
        @day_name ||= date.strftime('%A').downcase
      end

      def periods_room_type
        @periods_room_type ||= period.periods_room_types.find_by(room_type: room_type)
      end

      def period
        @period ||= current_hotel.periods.for(date, date.next_day)
      end

      def date
        @date ||= reservation&.check_in_date
      end

      def room_type
        @room_type ||= reservation&.room_type
      end
    end
  end
end
