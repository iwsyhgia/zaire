# frozen_string_literal: true

module Services
  module Rates
    class DetermineRateForDate < Services::Base
      attribute :date, Date
      attribute :room_type_id, Integer

      def call
        return success(rate['lower_bound']) if period.price_fixed? || count_booked_rooms.zero?
        return success(rate['upper_bound']) if period.price_dynamic? && count_booked_rooms == total_rooms

        success((rate['upper_bound'] - rate['lower_bound']) / count_available_rooms + rate['lower_bound'])
      end

      def count_available_rooms
        total_rooms - count_booked_rooms
      end

      def count_booked_rooms
        current_hotel.
          reservations.
          where(room_types: { id: room_type_id }).
          where('(check_in_date, check_out_date) OVERLAPS (?, ?)', date, date + 1.day).
          count
      end

      def total_rooms
        current_hotel.rooms.where(room_type_id: room_type_id).count
      end

      def rate
        room_type_rate.rates[date.strftime('%A').downcase]
      end

      def room_type_rate
        @room_type_rate ||= period.periods_room_types.where(room_type_id: room_type_id).first
      end

      def period
        @period ||= special_period || standard_period
      end

      def standard_period
        current_hotel.periods.standard.first
      end

      def special_period
        current_hotel.
          periods.
          where(period_kind: :special).
          where('(start_date, end_date) OVERLAPS (?, ?)', date, date + 1.day).
          first
      end

      def room_type
        @room_type ||= current_hotel.room_types.find(room_type_id)
      end
    end
  end
end
