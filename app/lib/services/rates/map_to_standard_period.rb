# frozen_string_literal: true

module Services
  module Rates
    class MapToStandardPeriod < Services::Base
      attribute :room_type, ::Rooms::RoomType
      attribute :hotel, ::Hotels::Hotel

      def perform
        return unless room_type.rate.active

        period_room_type.assign_attributes(period: standard_period, rates: rates)
        period_room_type.save!

        success(period_room_type)
      end

      private

      def rates
        day_rate = { **rate.slice(:lower_bound), **(standard_period.price_dynamic? ? rate.slice(:upper_bound) : {}) }
        standard_period.covered_day_names.each_with_object({}) { |day_name, memo| memo[day_name] = day_rate }
      end

      def period_room_type
        @period_room_type ||=
          standard_period.periods_room_types.includes(:period).find_or_initialize_by(room_type: room_type)
      end

      def rate
        @rate ||= room_type.rate.attributes.symbolize_keys
      end

      def standard_period
        @standard_period ||= (hotel || current_hotel).standard_period
      end
    end
  end
end
