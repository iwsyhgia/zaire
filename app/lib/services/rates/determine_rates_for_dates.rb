# frozen_string_literal: true

module Services
  module Rates
    class DetermineRatesForDates < Services::Base
      attribute :start_date, Date
      attribute :end_date, Date

      def call
        result = {}
        (start_date..end_date).each do |date|
          room_types_rates = []
          current_hotel.room_types.find_each do |room_type|
            rate = ::Services::Rates::DetermineRate.call(room_type: room_type, date: date).value
            room_types_rates << { rate: rate, room_type: room_type.slice(:id, :name) }
          end
          result[date.to_s(:db)] = room_types_rates
        end

        success(result)
      end
    end
  end
end
