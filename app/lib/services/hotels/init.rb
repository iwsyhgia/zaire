# frozen_string_literal: true

# Creates Hotel instance for a new Hotelier with all needed dependencies

module Services
  module Hotels
    class Init < Services::Base
      attribute :user, User

      CHECK_IN_DEFAULT = {
        start: '14:00'
      }.freeze

      CHECK_OUT_DEFAULT = {
        start: '12:00',
        end: '14:00'
      }.freeze

      def perform
        ActiveRecord::Base.transaction do
          raise ActiveRecord::Rollback unless user.save

          hotel = ::Hotels::Hotel.new(user: @user)
          hotel.build_offline_mode
          hotel.build_address
          hotel.build_configuration
          hotel.build_check_in_time(fixed: true, start_time: CHECK_IN_DEFAULT[:start])
          hotel.build_check_out_time(fixed: false, start_time: CHECK_OUT_DEFAULT[:start],
                                     end_time: CHECK_OUT_DEFAULT[:end])

          raise ActiveRecord::Rollback unless hotel.save

          ::Services::Rates::InitStandardPeriod.call(hotel: hotel)
          ::Services::Audits::ScheduleNext.call(hotel: hotel)

          success(hotel, 200)
        end
      end
    end
  end
end
