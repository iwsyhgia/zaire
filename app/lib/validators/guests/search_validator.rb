# frozen_string_literal: true

module Validators
  module Guests
    class SearchValidator < ::Validators::Base
      LENGTH_RANGE = (0..100).freeze

      fields :email_cont, :phone_number_cont, :g, :m

      # Length
      validates :email_cont, length: { in: LENGTH_RANGE }, allow_blank: true
      validates :phone_number_cont, length: { in: LENGTH_RANGE }, allow_blank: true

      # Custom validation
      validate :validate_first_name_and_last_name

      def validate_first_name_and_last_name
        params = g&.first
        return if params.nil?

        validate_param_length(params, :first_name_cont)
        validate_param_length(params, :last_name_cont)
      end

      protected

      def validate_param_length(params, name)
        value = params[name]
        return unless value

        in_range = LENGTH_RANGE.include?(value.length)
        errors.add(name, I18n.t("guests.errors.search.#{name}.length")) unless in_range
      end
    end
  end
end
