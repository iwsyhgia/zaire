# frozen_string_literal: true

module Validators
  module Guests
    class GuestValidator < ::Validators::Base
      NAME_LENGTH_RANGE = (1..120).freeze
      EMAIL_FORMAT = URI::MailTo::EMAIL_REGEXP
      PHONE_FORMAT = /\A\d+\z/.freeze

      fields :id,
        :title,
        :first_name,
        :last_name,
        :email,
        :birth_date,
        :phone_code,
        :phone_number,
        :country,
        :city,
        :hotel_id

      # Presence
      validates :first_name, presence: true
      validates :last_name,  presence: true
      validates :country,    presence: true

      # email or phone must be present
      validates :email,        presence: true, if: ->(r) { r.phone_number.blank? }
      validates :phone_number, presence: true, if: ->(r) { r.email.blank? }
      validates :phone_code,   presence: true, if: ->(r) { r.email.blank? }

      # Dates
      validates_date :birth_date, before: -> { Date.current }, allow_blank: true

      # Numericality
      validates :phone_code,
        numericality: { only_integer: true, greater_than: 0 }, allow_nil: true

      # Format
      validates :email,
        format: { with: EMAIL_FORMAT, message: I18n.t('guests.errors.email_format') },
        allow_blank: true

      validates :phone_number,
        format: { with: PHONE_FORMAT, message: I18n.t('guests.errors.phone_format') },
        allow_blank: true

      # Length
      validates :first_name, length: { in: NAME_LENGTH_RANGE }
      validates :last_name, length: { in: NAME_LENGTH_RANGE }

      validate :validate_email_uniqueness, if: -> { email.present? }
      validate :validate_phone_number_uniqueness, if: -> { phone_number.present? }

      protected

      def validate_phone_number_uniqueness
        return unless guests.exists?(phone_number: phone_number)

        errors.add(:base, I18n.t('guests.errors.phone_number_taken', value: phone_number))
      end

      def validate_email_uniqueness
        return unless guests.exists?(email: email)

        errors.add(:base, I18n.t('guests.errors.email_taken', value: email))
      end

      def guests
        current_hotel.guests.without_deleted.where.not(guests: { id: id })
      end
    end
  end
end
