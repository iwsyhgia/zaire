# frozen_string_literal: true

module Validators
  module Hotels
    class LogoValidator < ::Validators::Base
      BASE64_FORMAT = %r{\Adata:([-\w]+\/[-\w\+\.]+)?;base64,(.*)}m.freeze
      IMAGE_FORMATS = %w[PNG JPG JPEG SVG].freeze
      MIN_BASE64_LENGTH = 100
      MIN_SIZE = 1.kilobyte
      MAX_SIZE = 10.megabyte

      fields :logo

      validates :logo, presence: true
      validates :logo, length: { minimum: MIN_BASE64_LENGTH }, allow_blank: true

      # Format
      validates :logo, format: { with: BASE64_FORMAT, message: I18n.t('images.errors.base64_invalid') }

      validate :validate_image_format, if: :image
      validate :validate_image_size, if: :image

      def validate_image_format
        return if IMAGE_FORMATS.include?(image.type)

        errors.add(:base, I18n.t('hotels.errors.format_invalid'))
      end

      def validate_image_size
        return if image.size > MIN_SIZE && image.size < MAX_SIZE

        message = 'images.errors.size_invalid'
        min = MIN_SIZE / 1.kilobyte
        max = MAX_SIZE / 1.megabyte
        errors.add(:base, I18n.t(message, min: min, max: max))
      end

      protected

      def image
        return unless logo

        img = Image.image_from_base64(logo)
        @image ||= img ? MiniMagick::Image.read(img) : nil
      end
    end
  end
end
