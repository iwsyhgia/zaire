# frozen_string_literal: true

module Validators
  module Hotels
    class HotelValidator < ::Validators::Base
      MAX_TITLE = 255

      fields :name, :name_ar

      validates :name, length: { maximum: MAX_TITLE }
      validates :name_ar, length: { maximum: MAX_TITLE }, allow_blank: true
    end
  end
end
