# frozen_string_literal: true

module Validators
  module Hotels
    class ConfigurationValidator < ::Validators::Base
      TIME_ZONES = ActiveSupport::TimeZone.all.map { |tz| tz.tzinfo.name }.freeze

      fields :night_audit_time, :night_audit_confirmation_required, :time_zone

      validates_time :night_audit_time, between: '03:00'..'07:00'
      validates :night_audit_confirmation_required, inclusion: { in: [true, false, 'true', 'false'] }
    end
  end
end
