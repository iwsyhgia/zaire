# frozen_string_literal: true

module Validators
  module Hotels
    class CheckTimeValidator < ::Validators::Base
      fields :check_in_time_attributes,
        :check_out_time_attributes

      validate :check_in_after_check_out
      validate :check_out_earlier_check_in
      validate :check_in, if: -> { errors.empty? }
      validate :check_out, if: -> { errors.empty? }

      private

      def check_in_after_check_out
        return if check_out_time_attributes[:fixed] &&
                  (check_out_time_attributes[:start_time] < check_in_time_attributes[:start_time])
        return if !check_out_time_attributes[:fixed] &&
                  (check_out_time_attributes[:end_time] < check_in_time_attributes[:start_time])

        errors.add(:base, I18n.t('hotels.errors.check_out_time_earlier'))
      end

      def check_out_earlier_check_in
        return if check_out_time_attributes[:fixed] &&
                  (check_in_time_attributes[:start_time] > check_out_time_attributes[:start_time])
        return if !check_out_time_attributes[:fixed] &&
                  (check_in_time_attributes[:start_time] > check_out_time_attributes[:end_time])

        errors.add(:base, I18n.t('hotels.errors.check_out_time_earlier'))
      end

      def check_in
        validator = ::Validators::Hotels::CheckInOutTimeValidator.new(check_in_time_attributes)
        return if validator.valid?

        errors.add(:check_in_time, validator.errors.full_messages.first)
      end

      def check_out
        validator = ::Validators::Hotels::CheckInOutTimeValidator.new(check_out_time_attributes)
        return if validator.valid?

        errors.add(:check_out_time, validator.errors.full_messages.first)
      end
    end
  end
end
