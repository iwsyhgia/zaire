# frozen_string_literal: true

module Validators
  module Hotels
    class OfflineModeValidator < ::Validators::Base
      MAX_DAYS = 15

      fields :id, :active, :days_before_today, :days_after_today

      validates :days_before_today, :days_after_today, presence: true, if: ->(r) { r.active }
      validates :days_before_today, numericality: { only_integer: true,
                                                    greater_than: 0,
                                                    less_than_or_equal_to: MAX_DAYS }, if: ->(r) { r.active }
      validates :days_after_today, numericality: { only_integer: true,
                                                   greater_than: 0,
                                                   less_than_or_equal_to: MAX_DAYS }, if: ->(r) { r.active }
    end
  end
end
