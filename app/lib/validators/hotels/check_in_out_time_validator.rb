# frozen_string_literal: true

module Validators
  module Hotels
    class CheckInOutTimeValidator < ::Validators::Base
      fields :fixed, :check_type, :start_time, :end_time

      VALID_TIME_REGEX = /\A\d{2}:\d{2}\z/.freeze
      validates :start_time, presence: true
      validates :end_time, presence: true, unless: ->(r) { r.fixed }
      validate  :time_start_end
      validates_format_of :start_time, with: VALID_TIME_REGEX, message: I18n.t('hotels.errors.invalid_format')
      validates_format_of :end_time,
        with: VALID_TIME_REGEX,
        message: I18n.t('hotels.errors.invalid_format'),
        unless: ->(r) { r.fixed }

      private

      def time_start_end
        return unless start_time && end_time

        errors.add(:base, I18n.t('hotels.errors.wrong_time_period')) if !fixed && start_time >= end_time
      end
    end
  end
end
