# frozen_string_literal: true

module Validators
  module Hotels
    class AddressValidator < ::Validators::Base
      MAX_LENGTH = 255
      EMAIL_FORMAT = URI::MailTo::EMAIL_REGEXP
      PHONE_FORMAT = /\A[+]?\d+\z/.freeze

      fields :country, :city, :state, :address_1, :address_2, :phone_number, :fax, :email

      validates :country, presence: true, length: { maximum: MAX_LENGTH }
      validates :city, presence: true, length: { maximum: MAX_LENGTH }
      validates :state, length: { maximum: MAX_LENGTH }
      validates :address_1, presence: true, length: { maximum: MAX_LENGTH }
      validates :address_2, length: { maximum: MAX_LENGTH }

      validates :phone_number, presence: true
      validates :email,        presence: true

      validates :phone_number, length: { maximum: MAX_LENGTH }
      validates :fax,          length: { maximum: MAX_LENGTH }
      validates :email,        length: { maximum: MAX_LENGTH }

      validates :email,
        format: { with: EMAIL_FORMAT, message: I18n.t('hotels.errors.email_invalid') }, allow_blank: true
      validates :phone_number,
        format: { with: PHONE_FORMAT, message: I18n.t('hotels.errors.phone_number_invalid') }, allow_blank: true
    end
  end
end
