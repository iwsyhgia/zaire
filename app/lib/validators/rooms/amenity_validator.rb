# frozen_string_literal: true

module Validators
  module Rooms
    class AmenityValidator < ::Validators::Base
      LENGTH_RANGE = (1..50).freeze
      fields :id, :name, :name_ar, :hotel_id, :_destroy

      validates :name, presence: true, unless: -> { id && _destroy }
      validates :name, length: { in: LENGTH_RANGE }, allow_blank: true
      validates :name_ar, length: { in: LENGTH_RANGE }, allow_blank: true

      validate :name_uniqueness, if: -> { errors.empty? && !_destroy }
      validate :name_ar_uniqueness, if: -> { errors.empty? && !_destroy }

      def name_uniqueness
        errors.add(:base, I18n.t('amenities.errors.taken', value: name)) unless uniq_by?(name: name)
      end

      def name_ar_uniqueness
        return unless name_ar.present?

        errors.add(:base, I18n.t('amenities.errors.taken', value: name_ar)) unless uniq_by?(name_ar: name_ar)
      end

      private

      def uniq_by?(conditions)
        !current_hotel.amenities.where.not(amenities: { id: id }).exists?(conditions)
      end
    end
  end
end
