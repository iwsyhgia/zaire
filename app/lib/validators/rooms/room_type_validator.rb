# frozen_string_literal: true

module Validators
  module Rooms
    class RoomTypeValidator < ::Validators::Base
      NAME_LENGTH_RANGE = (1..30).freeze
      UNIT_LENGTH_RANGE = (1..30).freeze
      SIZE_LENGTH_RANGE = (1..30).freeze
      MAX_COUNT = 1000

      fields :id,
        :name,
        :name_ar,
        :size,
        :unit,
        :quantity,
        :max_occupancy,
        :room_ids,
        :hotel_id,
        :amenity_ids,
        :image_ids,
        :rooms_attributes,
        :amenities_attributes,
        :images_attributes,
        :rate_attributes,
        :updated_at,
        :created_at

      # Presence
      validates :name, presence: true
      validates :max_occupancy, presence: true
      validates :quantity, presence: true

      # Length
      validates :name, length: { in: NAME_LENGTH_RANGE }
      validates :name_ar, length: { in: NAME_LENGTH_RANGE }, allow_blank: true
      validates :unit, length: { in: UNIT_LENGTH_RANGE }, allow_blank: true
      validates :size, length: { in: SIZE_LENGTH_RANGE }, allow_blank: true

      # Numericality
      validates :quantity, numericality: { only_integer: true,
                                           less_than_or_equal_to: MAX_COUNT,
                                           greater_than: 0 }
      validates :max_occupancy, numericality: { only_integer: true,
                                                less_than_or_equal_to: MAX_COUNT,
                                                greater_than: 0 },
                                allow_nil: true

      # Custom Validation
      validate :validate_quantity_attribute
      validate :validate_rooms_attributes
      validate :validate_amenities_attributes
      validate :validate_images_attributes
      validate :validate_associated_reservations
      validate :validate_rate
      validate :name_uniqueness, if: -> { errors.empty? }
      validate :name_ar_uniqueness, if: -> { errors.empty? }

      def validate_rate
        return unless rate_attributes

        validator = ::Validators::Rooms::RateValidator.new(rate_attributes)
        errors.add(:base, validator.errors.full_messages.first) unless validator.valid?
      end

      def validate_rooms_attributes
        (rooms_attributes || []).each do |room|
          validator = ::Validators::Rooms::RoomValidator.new(room)
          errors.add(:base, validator.errors.full_messages.first) unless validator.valid?
        end
      end

      def validate_amenities_attributes
        amenities_attributes&.each do |amenity|
          validator = ::Validators::Rooms::AmenityValidator.new(amenity)
          errors.add(:amenity, validator.errors.full_messages.first) unless validator.valid?
        end
      end

      def validate_images_attributes
        images_attributes&.each do |image|
          validator = ::Validators::ImageValidator.new(image)
          errors.add(:base, validator.errors.full_messages.first) unless validator.valid?
        end
      end

      def validate_quantity_attribute
        return if rooms_attributes.blank? || @quantity.blank?
        return if rooms_attributes.reject { |room| room.key?(:_destroy) }.length <= @quantity.to_i

        errors.add(:base, I18n.t('.room_types.errors.incorrect_rooms_count'))
      end

      def validate_associated_reservations
        return unless id
        return if ::Rooms::RoomType.find(id).max_occupancy == max_occupancy

        max_date = ::Reservations::Reservation.joins(room: :room_type).
                   where.not(status: ::Reservations::Reservation.statuses[:cancelled]).
                   where(room_types: { id: id }).maximum(:check_out_date)
        return if max_date.nil? || max_date <= Date.current

        errors.add(:base, I18n.t('.room_types.errors.upcoming_reservations'))
      end

      def name_uniqueness
        errors.add(:base, I18n.t('room_types.errors.taken', value: name)) unless uniq_by?(name: name)
      end

      def name_ar_uniqueness
        return unless name_ar.present?

        errors.add(:base, I18n.t('room_types.errors.taken', value: name_ar)) unless uniq_by?(name_ar: name_ar)
      end

      def uniq_by?(conditions)
        !current_hotel.room_types.where.not(room_types: { id: id }).exists?(conditions)
      end

      def to_hash
        rooms_attributes&.uniq!
        amenities_attributes&.uniq!
        super
      end
    end
  end
end
