# frozen_string_literal: true

module Validators
  module Rooms
    class DestroyRoomTypeValidator < ::Validators::Base
      fields :id, :hotel_id
      validate :validate_ability_be_destroyed

      def validate_ability_be_destroyed
        return unless current_hotel.reservations.
                      where('check_out_date >= ?', Date.current).
                      where.not(status: :cancelled).
                      where(room_types: { id: id }).exists?

        errors.add(:base, I18n.t('room_types.errors.cannot_be_destroyed'))
      end
    end
  end
end
