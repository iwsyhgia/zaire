# frozen_string_literal: true

module Validators
  module Rooms
    class RoomValidator < ::Validators::Base
      MAX_ROOM_NUMBER = 10_000

      fields :id, :room_type_id, :number, :updated_at, :created_at, :_destroy

      validates :number, presence: true, unless: -> { _destroy }
      validates :number,
        numericality: { only_integer: true, greater_than: 0, less_than_or_equal_to: MAX_ROOM_NUMBER },
        allow_blank: true, unless: -> { _destroy }

      validate :validate_number_uniqueness, if: -> { errors.empty? && !_destroy }
      validate :validate_ability_be_destroyed, if: -> { _destroy }

      def validate_number_uniqueness
        return unless current_hotel.rooms.where.not(rooms: { id: id }).exists?(number: number)

        errors.add(:base, I18n.t('rooms.errors.taken', value: number))
      end

      def validate_ability_be_destroyed
        return unless current_hotel.reservations.
                      where('check_out_date >= ?', Date.current).
                      where.not(status: :cancelled).
                      where(room_id: id).exists?

        errors.add(:base, I18n.t('rooms.errors.cannot_be_destroyed', value: number))
      end
    end
  end
end
