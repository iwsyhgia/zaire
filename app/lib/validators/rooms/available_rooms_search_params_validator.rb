# frozen_string_literal: true

module Validators
  module Rooms
    class AvailableRoomsSearchParamsValidator < ::Validators::Base
      fields :check_in_date, :check_out_date

      validates :check_in_date, presence: true
      validates :check_out_date, presence: true

      validates_date :check_in_date
      validates_date :check_out_date
    end
  end
end
