# frozen_string_literal: true

module Validators
  module Notifications
    class NotificationValidator < ::Validators::Base
      VALID_NOTIFIER_TYPES = %w[email sms].freeze

      fields :kind, :recipient_id, :reservation_id, :notifier

      validates :recipient_id, presence: true, numericality: true
      validates :reservation_id, presence: true, numericality: true

      validates :kind, presence: true
      validates :kind, inclusion: {
        in: ::Notifications::Notification.kinds.keys,
        message: I18n.t('notifications.errors.kind_invalid', values: ::Notifications::Notification.kinds.keys)
      }

      validates :notifier, presence: true
      validates :notifier, inclusion: {
        in: VALID_NOTIFIER_TYPES,
        message: I18n.t('notifications.errors.notifier_invalid', values: VALID_NOTIFIER_TYPES)
      }
    end
  end
end
