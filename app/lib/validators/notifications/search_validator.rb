# frozen_string_literal: true

module Validators
  module Notifications
    class SearchValidator < ::Validators::Base
      VALID_RECIPIENT_TYPE = %w[User Guests::Guest].freeze

      fields :kind_in, :is_read, :recipient_id, :recipient_type

      validates :kind_in, inclusion: {
        in: ::Notifications::Notification.kinds.keys, allow_nil: true,
        message: "Must be one of #{::Notifications::Notification.kinds.keys}"
      }
      validates :is_read, inclusion: {
        in: [true, false, 'true', 'false'], allow_nil: true,
        message: 'Invalid boolean value. Must be true or false'
      }
      validates :recipient_id, presence: true, numericality: true, if: :recipient_type
      validates :recipient_type, inclusion: {
        in: VALID_RECIPIENT_TYPE, allow_nil: true,
        message: "Must be one of #{VALID_RECIPIENT_TYPE}"
      }, presence: true, if: :recipient_id
    end
  end
end
