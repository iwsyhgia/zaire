# frozen_string_literal: true

module Validators
  class ImageValidator < ::Validators::Base
    BASE64_FORMAT = %r{\Adata:([-\w]+\/[-\w\+\.]+)?;base64,(.*)}m.freeze
    IMAGE_FORMATS = %w[PNG JPG JPEG SVG].freeze
    MIN_BASE64_LENGTH = 100
    MIN_SIZE = 1.kilobyte
    MAX_SIZE = 10.megabyte
    MIN_WIDTH = 300
    MIN_HEIGHT = 300

    fields :id, :base64, :_destroy, :file, :tag, :imageable

    validates :base64, presence: true, if: -> { id.blank? }
    validates :base64, length: { minimum: MIN_BASE64_LENGTH }, allow_blank: true

    # Format
    validates :base64,
      format: { with: BASE64_FORMAT, message: I18n.t('images.errors.base64_invalid') },
      if: -> { base64 }

    validate :validate_image_resolution, if: :image, unless: -> { _destroy || tag == 'logo' }
    validate :validate_image_format, if: :image, unless: -> { _destroy }
    validate :validate_image_size, if: :image, unless: -> { _destroy }

    def validate_image_resolution
      return if image[:width] >= MIN_WIDTH && image[:height] >= MIN_HEIGHT

      message = 'images.errors.resolution_invalid'
      resolution = "#{MIN_WIDTH}x#{MIN_HEIGHT}"
      errors.add :base, I18n.t(message, resolution: resolution)
    end

    def validate_image_format
      return if IMAGE_FORMATS.include?(image.type)

      errors.add(:base, I18n.t('images.errors.format_invalid'))
    end

    def validate_image_size
      return if image.size > MIN_SIZE && image.size < MAX_SIZE

      message = 'images.errors.size_invalid'
      min = MIN_SIZE / 1.kilobyte
      max = MAX_SIZE / 1.megabyte
      errors.add(:base, I18n.t(message, min: min, max: max))
    end

    protected

    def image
      return unless base64

      img = Image.image_from_base64(base64)
      @image ||= img ? MiniMagick::Image.read(img) : nil
    end
  end
end
