# frozen_string_literal: true

module Validators
  module Profiles
    class ProfileValidator < ::Validators::Base
      fields :id,
        :first_name,
        :last_name,
        :phone_number,
        :notifications,
        :avatar_attributes,
        :user_attributes

      LENGTH_RANGE = (0..42).freeze
      PHONE_FORMAT = /\A[+]?\d+\z/.freeze
      NOTIFICATION_KEYS = ::Profiles::Profile.notifications.keys

      with_options length: { in: LENGTH_RANGE }, allow_blank: true do
        validates :first_name
        validates :last_name
        validates :phone_number
      end

      validates :phone_number, allow_blank: true,
        format: { with: PHONE_FORMAT, message: I18n.t('profiles.errors.phone_format_invalid') }

      validates :notifications, inclusion: {
        in: NOTIFICATION_KEYS, allow_nil: true,
        message: I18n.t('profiles.errors.notifications_invalid', values: NOTIFICATION_KEYS.join(' '))
      }

      validate :validate_avatar_attributes
      validate :validate_user_attributes

      protected

      def validate_avatar_attributes
        return unless avatar_attributes
        return if avatar_attributes[:id].nil? && avatar_attributes[:_destroy]

        validator = ::Validators::ImageValidator.new(avatar_attributes)
        errors.add(:base, validator.errors.full_messages.first) unless validator.valid?
      end

      def validate_user_attributes
        return unless user_attributes

        valid = current_hotel.user.valid_password?(user_attributes[:current_password])
        errors.add(:base, I18n.t('profiles.errors.password_not_match')) unless valid
      end
    end
  end
end
