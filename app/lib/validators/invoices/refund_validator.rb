# frozen_string_literal: true

module Validators
  module Invoices
    class RefundValidator < ::Validators::Base
      MAX_AMOUNT = 1_000_000
      LENGTH_RANGE = (0..255).freeze
      CREATION_WAYS = ::Invoices::Refund.creation_ways.keys

      fields :id, :amount, :user_id, :payment_id, :description, :creation_way

      with_options presence: true do
        validates :amount
        validates :description
        validates :user_id
        validates :payment_id
      end

      validates :description, length: { in: LENGTH_RANGE }, allow_blank: true
      validates :payment_id, numericality: { only_integer: true }
      validates :amount, numericality: { greater_than: 0, less_than: MAX_AMOUNT }

      validate :validate_refunds_amount, if: -> { errors.empty? }

      validates :creation_way, inclusion: {
        in: CREATION_WAYS,
        message: I18n.t('refunds.errors.creation_way_invalid', valid_values: CREATION_WAYS)
      }

      protected

      def validate_refunds_amount
        invoice_ids = ::Services::CurrentControllerService.get.current_hotel.invoices.select(:invoice_id)
        refunds_amount = ::Invoices::Refund.
                         where(invoice_id: invoice_ids, payment_id: payment_id).
                         where.not(id: id).sum(:amount)
        payment = ::Invoices::Payment.find_by(invoice_id: invoice_ids, id: payment_id)
        return if payment && payment.amount >= refunds_amount + amount.to_d

        errors.add(:base, I18n.t('refunds.errors.invalid_refund_amount'))
      end
    end
  end
end
