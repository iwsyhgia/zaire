# frozen_string_literal: true

module Validators
  module Invoices
    class DiscountValidator < ::Validators::Base
      MAX_AMOUNT = 1_000_000
      MAX_PERCENT = 100
      LENGTH_RANGE = (0..255).freeze
      KINDS = ::Invoices::Discount.kinds.keys
      CREATION_WAYS = ::Invoices::Discount.creation_ways.keys

      fields :id, :value, :kind, :description, :user_id, :creation_way

      with_options presence: true, unless: -> { id } do
        validates :value
        validates :kind
        validates :user_id
        validates :description
      end

      validates :description, length: { in: LENGTH_RANGE }, allow_blank: true

      validates :value,
        numericality: { greater_than: 0, less_than: MAX_AMOUNT },
        if: -> { kind == 'amount' || @record&.kind == 'amount' }

      validates :value,
        numericality: { greater_than: 0, less_than: MAX_PERCENT },
        if: -> { kind == 'percent' || @record&.kind == 'percent' }

      validates :creation_way, inclusion: {
        in: CREATION_WAYS,
        message: I18n.t('discounts.errors.creation_way_invalid', valid_values: CREATION_WAYS)
      }

      validates :kind,
        inclusion: { in: KINDS, message: I18n.t('discounts.errors.kind_invalid', valid_values: KINDS) },
        allow_blank: -> { id }
    end
  end
end
