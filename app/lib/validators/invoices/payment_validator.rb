# frozen_string_literal: true

module Validators
  module Invoices
    class PaymentValidator < ::Validators::Base
      MAX_AMOUNT = 1_000_000
      CREATION_WAYS = ::Invoices::Payment.creation_ways.keys
      PAYMENT_METHODS = ::Invoices::Payment.payment_methods.keys

      fields :id, :creation_way, :amount, :user_id, :payment_method

      with_options presence: true do
        validates :amount
        validates :payment_method
        validates :user_id
      end

      validates :creation_way, inclusion: {
        in: CREATION_WAYS,
        message: I18n.t('payments.errors.creation_way_invalid', valid_values: CREATION_WAYS)
      }

      validates :payment_method, inclusion: {
        in: PAYMENT_METHODS,
        message: I18n.t('payments.errors.payment_method_invalid', valid_values: PAYMENT_METHODS)
      }
      validates :amount, numericality: { greater_than: 0, less_than: MAX_AMOUNT }
    end
  end
end
