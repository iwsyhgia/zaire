# frozen_string_literal: true

module Validators
  module Rates
    class PeriodsRoomTypeValidator < ::Validators::Base
      MAX_AMOUNT = 1_000_000
      MAX_PERCENT = 100

      fields(*Schemas::Rates::PeriodsRoomType.top_level_keys)

      with_options on: :create do
        validates :room_type_id, presence: true
        validates :discount_value, presence: true
        validates :rates, presence: true
      end

      with_options on: :update do
        validates :id, presence: true
      end

      with_options on: %i[create update] do
        validates :discount_value, numericality: { greater_than_or_equal_to: 0, less_than: MAX_AMOUNT },
          if: ->(r) { period.discount_amount? && r.discount_value.present? }

        validates :discount_value, numericality: { greater_than_or_equal_to: 0, less_than: MAX_PERCENT },
          if: ->(r) { period.discount_percent? && r.discount_value.present? }

        with_options if: ->(r) { r.rates.present? && errors.empty? } do
          validate :validate_day_names_correspond_dates
          validate :validate_discount_exceeded
          validate :validate_rates
        end
      end

      protected

      def validate_day_names_correspond_dates
        return if period.covered_day_names.each_with_object(rates.keys).all? do |day, day_names|
          day_names.include?(day)
        end

        errors.add(:base, I18n.t('periods.errors.day_names_invalid', values: period.covered_day_names.join(', ')))
      end

      def validate_discount_exceeded
        lowest_value = rates.min_by { |_day_name, value| value[:lower_bound] }
        return if lowest_value.second[:lower_bound].to_d > discount_value.to_d

        errors.add(:base, I18n.t('periods.errors.discount_exceeded'))
      end

      def validate_rates
        rates.each do |_day_name, attributes|
          validator = ::Validators::Rates::RateValidator.new(attributes.merge(kind: period.price_kind), nil, context)

          errors.add(:base, validator.errors.full_messages.first) if validator.invalid?(context)
        end
      end

      private

      def period
        @period ||= record.period
      end
    end
  end
end
