# frozen_string_literal: true

module Validators
  module Rates
    class PeriodValidator < ::Validators::Base
      DISCOUNT_KINDS = ::Rates::Period.discount_kinds.keys
      PRICE_KINDS = ::Rates::Period.price_kinds.keys
      NAME_LENGTH_RANGE = (0..30).freeze

      fields(*Schemas::Rates::Period.top_level_keys)

      with_options on: :create do
        validates :name, presence: true
        validates :start_date, presence: true
        validates :end_date, presence: true
        validates :price_kind, presence: true
        validates :discount_kind, presence: true
        validates :period_kind, presence: true
        validates :periods_room_types_attributes, presence: true

        validates_date :start_date, on_or_after: -> { Date.current }
        validates_date :end_date, after: ->(r) { r.start_date }

        validates :period_kind, inclusion: { in: %w[special], message: I18n.t('periods.errors.period_kind_invalid') }
      end

      with_options on: :update do
        validates :period_kind, absence: true
        validates :id, presence: true

        with_options if: ->(r) { r.start_date.present? || r.end_date.present? } do
          validates_date :start_date, on_or_after: -> { Date.current }
          validates_date :end_date, after: ->(r) { r.start_date }
        end
      end

      with_options on: %i[create update] do
        validates :name, length: { in: NAME_LENGTH_RANGE }
        validates :price_kind, inclusion: {
          in: PRICE_KINDS,
          message: I18n.t('periods.errors.price_kind_invalid', values: PRICE_KINDS.join(', '))
        }, allow_nil: true
        validates :discount_kind, inclusion: {
          in: DISCOUNT_KINDS,
          message: I18n.t('periods.errors.discount_kind_invalid', values: DISCOUNT_KINDS.join(', '))
        }, allow_nil: true

        validate :validate_period_cannot_be_overlapped, if: ->(r) { r.start_date.present? || r.end_date.present? }
        validate :validate_periods_room_types_attributes, if: -> { errors.empty? }
        validate :validate_period_name_uniqueness, if: -> { errors.empty? }
      end

      with_options on: :destroy do
        validates :id, presence: true
        validate :validate_standard_period_cannot_be_destroyed
      end

      protected

      def validate_period_name_uniqueness
        return unless current_hotel.periods.where.not(id: record&.id).exists?(name: name)

        errors.add(:base, I18n.t('periods.errors.name_taken'))
      end

      def validate_period_cannot_be_overlapped
        return unless current_hotel.periods.overlaps(start_date, end_date).where.not(id: record&.id).exists?

        errors.add(:base, I18n.t('periods.errors.cannot_be_overlapped'))
      end

      def validate_periods_room_types_attributes
        periods_room_types_attributes&.each do |attributes|
          options = [attributes, build_periods_room_type(attributes), context]
          validator = ::Validators::Rates::PeriodsRoomTypeValidator.new(*options)

          errors.add(:base, validator.errors.full_messages.first) && break if validator.invalid?(context)
        end
      end

      def validate_standard_period_cannot_be_destroyed
        return unless record.standard?

        errors.add(:base, I18n.t('periods.errors.cannot_be_destroyed'))
      end

      private

      def build_periods_room_type(attributes)
        ::Rates::PeriodsRoomType.new(attributes).tap { |r| r.period = build_period }
      end

      def build_period
        record&.tap { |r| r.assign_attributes(attributes_hash) } || ::Rates::Period.new(attributes_hash)
      end
    end
  end
end
