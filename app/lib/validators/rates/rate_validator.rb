# frozen_string_literal: true

module Validators
  module Rates
    class RateValidator < ::Validators::Base
      MIN_RATE = 0
      MAX_RATE = 100_000

      fields :lower_bound, :upper_bound, :kind

      with_options if: -> { kind == 'fixed' } do
        validates :lower_bound, presence: true
        validates :upper_bound, absence: true
        validates :lower_bound, numericality: { greater_than: MIN_RATE, less_than: MAX_RATE }
      end

      with_options if: -> { kind == 'dynamic' } do
        validates :lower_bound, presence: true
        validates :upper_bound, presence: true
        validates :lower_bound, numericality: { greater_than: MIN_RATE, less_than: MAX_RATE }
        validates :upper_bound, numericality: { greater_than_or_equal_to: ->(r) { r.lower_bound.to_d } }
      end
    end
  end
end
