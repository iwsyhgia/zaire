# frozen_string_literal: true

module Validators
  DEFAULT_LENGTH_RANGE = (1..50).freeze

  class Base
    include ActiveModel::Validations

    class_attribute :attributes, instance_writer: false
    attr_accessor :record
    attr_accessor :context

    def self.fields(*names)
      self.attributes ||= []
      self.attributes += Array(names)
      send :attr_accessor, *attributes
    end

    def initialize(params, record = nil, context = nil)
      @record = record
      @context = context
      params[:id] = record.id if record
      params = params.to_unsafe_h.with_indifferent_access if params.is_a? ActionController::Parameters
      params = params&.except(:format, :controller, :action)&.with_indifferent_access
      not_valid_params = params&.except(*attributes)&.keys
      raise ActionController::UnpermittedParameters, not_valid_params if not_valid_params.present?

      assign_attributes(params&.slice(*attributes))
    end

    def assign_attributes(new_attributes)
      new_attributes = new_attributes.to_unsafe_h if new_attributes.is_a? ActionController::Parameters
      attr_hash = Hash(new_attributes).deep_symbolize_keys
      attr_hash.each do |k, v|
        send("#{k}=", v) if respond_to?(k)
      end

      self
    end

    def attributes_hash
      hash = attributes.compact.each_with_object({}) do |attr, result|
        result[attr] = send(attr)
      end
      hash.compact
    end

    def current_hotel
      @current_hotel = ::Services::CurrentControllerService.get.current_hotel
    end

    def to_hash
      raise Validators::InvalidParameters, errors if invalid?(context)

      attributes_hash
    end

    def validate!
      super(context)
    rescue ActiveModel::ValidationError
      raise Validators::InvalidParameters, errors
    end
  end
end
