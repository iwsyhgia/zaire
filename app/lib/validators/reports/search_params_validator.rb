# frozen_string_literal: true

module Validators
  module Reports
    class SearchParamsValidator < ::Validators::Base
      SOURCES = ::Reservations::Reservation::SOURCES.keys.freeze
      STATUSES = ::Reservations::Reservation.statuses.keys.map(&:to_sym).freeze
      LENGTH_RANGE = (0..100).freeze

      fields :status_in, :source_in, :check_in_date_gteq, :check_out_date_lteq, :room_type_name_eq

      with_options allow_nil: true do
        validates :room_type_name_eq, length: { in: LENGTH_RANGE }

        validates_date :check_in_date_gteq, on_or_after: -> { Date.current }
        validates_date :check_out_date_lteq, after: ->(r) { r.check_in_date_gteq }
      end

      validate :validate_status_in, if: -> { status_in.present? }
      validate :validate_source_in, if: -> { source_in.present? }

      private

      def validate_status_in
        return if status_in.is_a?(Array) && status_in.all? { |s| STATUSES.include?(s.to_sym) }

        errors.add(:base, I18n.t('reservations.errors.status_invalid', values: STATUSES))
      end

      def validate_source_in
        return if source_in.is_a?(Array) && source_in.all? { |s| SOURCES.include?(s.to_sym) }

        errors.add(:base, I18n.t('reservations.errors.source_invalid', values: SOURCES))
      end
    end
  end
end
