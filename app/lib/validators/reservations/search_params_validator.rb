# frozen_string_literal: true

module Validators
  module Reservations
    class SearchParamsValidator < ::Validators::Base
      fields :from_date, :to_date, :status_in

      validates_date :from_date, allow_blank: true
      validates_date :to_date, allow_blank: true
      validate :statuses_are_correct

      def statuses_are_correct
        return if status_in.blank?
        return if status_in.all? { |s| ::Reservations::Reservation.statuses.key?(s) }

        errors.add(:status_in, I18n.t('reservations.errors.search_params.status_in'))
      end

      def initialize(params)
        params[:status_in] = params[:status_in].gsub(', ', ',').split(',') if params[:status_in]
        super(params)
      end

      def attributes_hash
        status_in ? super().merge(status_in: ::Reservations::Reservation.statuses.values_at(*status_in)) : super()
      end
    end
  end
end
