# frozen_string_literal: true

module Validators
  module Reservations
    class DNRValidator < ::Validators::Base
      LENGTH_RANGE = (0..250).freeze

      fields :id,
        :check_in_date,
        :check_out_date,
        :room_id,
        :description,
        :type

      # Presence
      validates :check_in_date, presence: true
      validates :check_out_date, presence: true
      validates :room_id, presence: true

      # Dates
      validates_date :check_in_date, on_or_after: -> { Date.current }, unless: ->(r) { r.record }

      validates_date :check_in_date,
        is_at: ->(r) { r.record.check_in_date },
        if: lambda { |r|
          errors.empty? && r.record&.dnr? && r.record&.check_in_date &&
            ((r.record.check_in_date + 1.day)..r.record&.check_out_date).cover?(Date.current)
        }

      validates_date :check_out_date, after: ->(r) { r.check_in_date }

      # Numericality
      validates :room_id, numericality: { only_integer: true }

      # Length
      validates :description, length: { in: LENGTH_RANGE }, allow_blank: true

      # Custom Validation
      validate :validate_room_existence

      def validate_room_existence
        return if ::Rooms::Room.exists?(room_id)

        errors.add(:base, I18n.t('rooms.errors.does_not_exist'))
      end
    end
  end
end
