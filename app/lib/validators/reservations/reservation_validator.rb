# frozen_string_literal: true

module Validators
  module Reservations
    class ReservationValidator < ::Validators::Base
      RATES = %w[refundable non_refundable].freeze
      PAYMENT_KINDS = %w[first_night full_stay].freeze
      LENGTH_RANGE = (0..100).freeze
      MAX_NUMBER = 42

      fields :id,
        :check_in_date,
        :check_out_date,
        :number_of_adults,
        :number_of_children,
        :room_id,
        :guest_id,
        :guest_attributes,
        :status,
        :type,
        :rate,
        :payment_kind,
        :adults_attributes

      # Presence
      validates :check_in_date,      presence: true
      validates :check_out_date,     presence: true
      validates :number_of_adults,   presence: true
      validates :number_of_children, presence: true
      validates :room_id,            presence: true
      validates :type,               presence: true
      validates :rate,               presence: true
      validates :payment_kind,       presence: true, if: -> { rate == 'refundable' }

      # Absence
      validates :payment_kind,       absence: true,  if: -> { rate == 'non_refundable' }

      # Inclusion
      validates :rate,         inclusion: { in: RATES }
      validates :payment_kind, inclusion: { in: PAYMENT_KINDS }, if: -> { rate == 'refundable' }

      # Numericality
      validates :room_id, numericality: { only_integer: true }
      validates :number_of_children,
        numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: MAX_NUMBER }
      validates :number_of_adults,
        numericality: { only_integer: true, greater_than: 0, less_than: MAX_NUMBER }

      # Dates
      validates_date :check_in_date, on_or_after: -> { Date.current }, if: -> { errors.empty? }
      validates_date :check_out_date, after: ->(r) { r.check_in_date }, if: -> { errors.empty? }

      # Custom Validation
      validate :validate_guest_attributes,         if: -> { errors.empty? }
      validate :validate_reservation_intersection, if: -> { errors.empty? }
      validate :validate_room_existence,           if: -> { errors.empty? }
      validate :validate_room_type,                if: -> { errors.empty? }
      validate :validate_room_occupancy,           if: -> { errors.empty? }

      def validate_guest_attributes
        validator = ::Validators::Guests::GuestValidator.new(guest_attributes)
        return if validator.valid?

        errors.add(:guest, validator.errors.full_messages.first)
      end

      def validate_reservation_intersection
        options = attributes_hash.slice(:id, :check_in_date, :check_out_date, :room_id)
        return if Queries::Reservations::OverlapsExistingQuery.call(options).empty?

        errors.add(:base, I18n.t('reservations.errors.overlaps'))
      end

      def validate_room_existence
        return if ::Rooms::Room.exists?(room_id)

        errors.add(:base, I18n.t('rooms.errors.does_not_exist'))
      end

      def validate_room_type
        return if ::Rooms::Room.find_by(id: room_id).room_type

        errors.add(:base, I18n.t('rooms.errors.not_associated'))
      end

      def validate_room_occupancy
        max_occupancy = ::Rooms::Room.find_by(id: room_id).room_type.max_occupancy
        return unless max_occupancy
        return if max_occupancy >= number_of_adults.to_i + number_of_children.to_i

        errors.add(:base, I18n.t('rooms.errors.capacity_exceeded'))
      end
    end
  end
end
