# frozen_string_literal: true

module Validators
  module Reservations
    class UpdateReservationValidator < ::Validators::Base
      MAX_NUMBER = 42

      fields :id,
        :room_id,
        :check_in_date,
        :check_out_date,
        :number_of_adults,
        :number_of_children,
        :adults_attributes

      # Presence
      validates :check_in_date,      presence: true
      validates :check_out_date,     presence: true
      validates :number_of_adults,   presence: true
      validates :number_of_children, presence: true

      # Numericality
      validates :number_of_children,
        numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than: MAX_NUMBER }
      validates :number_of_adults,
        numericality: { only_integer: true, greater_than: 0, less_than: MAX_NUMBER }

      # Dates
      validates_date :check_in_date,
        is_at: ->(r) { r.record.check_in_date_was },
        if: lambda { |r|
          errors.empty? && r.record.status.in?(%w[checked_in]) &&
            (r.record.check_in_date..r.record.check_out_date).cover?(Date.current)
        }

      validates_date :check_in_date,
        is_at: ->(r) { r.record.check_in_date_was },
        if: lambda { |r|
          errors.empty? && r.record.status.in?(%w[confirmed]) &&
            r.record.check_in_date < Date.current
        }

      validates_date :check_out_date, after: ->(r) { r.check_in_date || r.record&.check_in_date }

      # Custom Validation
      validate :validate_adults_attributes,        if: -> { errors.empty? && adults_attributes }
      validate :validate_adults_count,             if: -> { errors.empty? && adults_attributes }
      validate :validate_adults_uniqueness,        if: -> { errors.empty? && adults_attributes }
      validate :validate_reservation_intersection, if: -> { errors.empty? }
      validate :validate_room_occupancy,           if: -> { errors.empty? }

      def validate_reservation_intersection
        options = attributes_hash.slice(:id, :check_in_date, :check_out_date, :room_id)
        return if Queries::Reservations::OverlapsExistingQuery.call(options).empty?

        errors.add(:base, I18n.t('reservations.errors.overlaps'))
      end

      def validate_room_occupancy
        max_occupancy = @record.room.room_type.max_occupancy
        return unless max_occupancy
        return if max_occupancy >= number_of_adults.to_i + number_of_children.to_i

        errors.add(:base, I18n.t('rooms.errors.capacity_exceeded'))
      end

      def validate_adults_attributes
        adults_attributes.reject { |adult| adult.key?(:_destroy) }&.each do |adult|
          validator = ::Validators::Reservations::AdultValidator.new(adult.merge(reservation_id: @record.id))
          return if validator.valid?

          errors.add(:adults, validator.errors.full_messages.first)
        end
      end

      def validate_adults_count
        return if adults_attributes.reject { |adult| adult.key?(:_destroy) }.count == number_of_adults.to_i

        errors.add(:base, I18n.t('adults.errors.count_does_not_match'))
      end

      def validate_adults_uniqueness
        adults = adults_attributes.reject { |adult| adult.key?(:_destroy) }
        return if adults == adults.uniq { |adult| adult[:personal_id] }

        errors.add(:adults, I18n.t('adults.errors.personal_id_taken'))
      end
    end
  end
end
