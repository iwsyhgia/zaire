# frozen_string_literal: true

module Validators
  module Reservations
    class CancelReservationValidator < ::Validators::Base
      fields :cancellation_fee

      # Presence
      validates :cancellation_fee,
        inclusion: {
          in: [true, false, 'true', 'false', 0, 1],
          message: I18n.t('reservations.errors.cancellation_fee')
        }
    end
  end
end
