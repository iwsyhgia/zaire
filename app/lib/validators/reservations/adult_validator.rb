# frozen_string_literal: true

module Validators
  module Reservations
    class AdultValidator < ::Validators::Base
      NAME_LENGTH_RANGE = (0..100).freeze
      PERSONAL_ID_LENGTH_RANGE = (0..20).freeze
      PERSONAL_ID_FORMAT = /\A[\da-zA-Z]+\z/.freeze # only digits and letters

      fields :id,
        :title,
        :first_name,
        :last_name,
        :personal_id,
        :reservation_id

      # Presence
      validates :first_name, presence: true
      validates :last_name, presence: true
      validates :personal_id, presence: true

      # Length
      validates :first_name, length: { in: NAME_LENGTH_RANGE }
      validates :last_name, length: { in: NAME_LENGTH_RANGE }
      validates :personal_id, length: { in: PERSONAL_ID_LENGTH_RANGE }

      # Numericality
      validates :reservation_id, numericality: { only_integer: true }

      # Format
      validates :personal_id,
        format: { with: PERSONAL_ID_FORMAT, message: I18n.t('adults.errors.personal_id_format') }

      # Custom Validation
      validate :validate_reservation_existence
      validate :validate_personal_id_uniqueness
      validate :direct_only

      # when saving an adult as a nested attribute we don't need to do this check
      validate :validate_number_adults, if: -> { errors.empty? && @record }

      def direct_only
        return unless reservation

        errors.add(:base, I18n.t('reservations.errors.direct_only')) if reservation.dnr?
      end

      def validate_reservation_existence
        errors.add(:base, I18n.t('reservations.errors.does_not_exist')) unless reservation
      end

      def validate_personal_id_uniqueness
        return unless reservation
        return unless reservation.adults.where.not(reservations_adults: { id: id }).exists?(personal_id: personal_id)

        errors.add(:base, I18n.t('adults.errors.personal_id_taken', value: personal_id))
      end

      def validate_number_adults
        return unless reservation

        associated_adults_number = reservation.adults.count
        associated_adults_number += 1 unless id
        result = reservation.number_of_adults >= associated_adults_number

        errors.add(:base, I18n.t('adults.errors.wrong_number')) unless result
      end

      protected

      def reservation
        @reservation ||= ::Reservations::Reservation.find_by(id: reservation_id)
      end
    end
  end
end
