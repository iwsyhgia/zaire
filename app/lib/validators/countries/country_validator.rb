# frozen_string_literal: true

module Validators
  module Countries
    class CountryValidator < ::Validators::Base
      fields :alpha3

      validates :alpha3, length: { is: 3 }
      validates :alpha3, presence: true
    end
  end
end
