# frozen_string_literal: true

module Validators
  class InvalidParameters < StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
      message = I18n.t(
        :'errors.messages.model_invalid',
        errors: errors.full_messages.first,
        default: :'errors.messages.model_invalid'
      )
      super message
    end
  end
end
