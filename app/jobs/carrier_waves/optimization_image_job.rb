# frozen_string_literal: true

module CarrierWaves
  class OptimizationImageJob < ActiveJob::Base
    queue_as :carrier_wave_optimization

    def perform(model)
      @model = model
      @model.need_to_optimize = true
      @model.file.recreate_versions!
      @model.optimized_at = Time.current
      @model.save
    end
  end
end
