# frozen_string_literal: true

class ServiceJob < ApplicationJob
  queue_as :default

  def perform(service_class, **attributes)
    service_class = service_class.safe_constantize
    service = service_class.new(attributes)
    service.call
  end
end
