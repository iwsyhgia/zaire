# frozen_string_literal: true

class NotificationJob < ServiceJob
  queue_as :notifications

  def perform(service_class, **attributes)
    log = create_log!(attributes[:notification])
    super(service_class, attributes)
    confirm_log!(log)
  rescue StandardError => error
    rollback!(log, error)
  end

  private

  def create_log!(notification)
    ::Notifications::Log.create!(notification: notification, status: :pending)
  end

  def confirm_log!(log)
    log.update_attributes!(status: :success)
  end

  def rollback!(log, error)
    log.update_attributes!(status: :fail, meta: error.message)
  end
end
