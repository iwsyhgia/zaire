# frozen_string_literal: true

module Audits
  def self.table_name_prefix
    'audits_'
  end
end
