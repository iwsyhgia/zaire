# frozen_string_literal: true

# == Schema Information
#
# Table name: profiles
#
#  id            :bigint(8)        not null, primary key
#  deleted_at    :datetime
#  first_name    :string
#  last_name     :string
#  notifications :integer          default("on"), not null
#  phone_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :bigint(8)
#

module Profiles
  class Profile < ApplicationRecord
    self.table_name = 'profiles'
    acts_as_paranoid

    enum notifications: { on: 0, off: 1 }

    belongs_to :user
    has_one :avatar, as: :imageable, dependent: :destroy, class_name: 'Image'

    accepts_nested_attributes_for :user
    accepts_nested_attributes_for :avatar, allow_destroy: true
  end
end
