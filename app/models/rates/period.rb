# == Schema Information
#
# Table name: rates_periods
#
#  id            :bigint(8)        not null, primary key
#  deleted_at    :datetime
#  discount_kind :integer          default("amount"), not null
#  end_date      :date
#  name          :string           default(""), not null
#  period_kind   :integer          default("standard"), not null
#  price_kind    :integer          default("fixed"), not null
#  start_date    :date
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  hotel_id      :bigint(8)
#

module Rates
  class Period < ApplicationRecord
    acts_as_paranoid

    enum period_kind: { standard: 0, special: 1 }
    enum price_kind: { fixed: 0, dynamic: 1 }, _prefix: :price
    enum discount_kind: { amount: 0, percent: 1 }, _prefix: :discount

    belongs_to :hotel, class_name: '::Hotels::Hotel'
    has_many :periods_room_types, class_name: '::Rates::PeriodsRoomType', inverse_of: :period

    scope :standard_period, -> { standard.first }
    scope :special_periods, ->(start_date, end_date) { special.overlaps(start_date, end_date) }
    scope :overlaps, ->(start_date, end_date) { where('(start_date, end_date) OVERLAPS (?, ?)', start_date, end_date) }

    accepts_nested_attributes_for :periods_room_types

    def self.for(start_date, end_date)
      special_periods(start_date, end_date).first || standard.first
    end

    def covered_day_names
      return DateTime::DAYS_INTO_WEEK.keys if standard?

      (start_date...end_date).first(DateTime::DAYS_INTO_WEEK.keys.length).each_with_object([]) do |date, memo|
        memo[date.cwday] = date.strftime('%A').downcase.to_sym # %A - Monday, Tuesday, ...
      end.compact
    end
  end
end
