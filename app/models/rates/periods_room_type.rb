# == Schema Information
#
# Table name: rates_periods_room_types
#
#  id             :bigint(8)        not null, primary key
#  deleted_at     :datetime
#  discount_value :decimal(10, 2)   default(0.0)
#  rates          :jsonb
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  period_id      :bigint(8)
#  room_type_id   :bigint(8)
#

module Rates
  class PeriodsRoomType < ApplicationRecord
    acts_as_paranoid

    belongs_to :period, class_name: '::Rates::Period', dependent: :destroy, inverse_of: :periods_room_types
    belongs_to :room_type, class_name: '::Rooms::RoomType', dependent: :destroy

    def lower_bound(day_name)
      rates.dig(day_name, 'lower_bound').to_d
    end

    def upper_bound(day_name)
      rates.dig(day_name, 'upper_bound').to_d
    end
  end
end
