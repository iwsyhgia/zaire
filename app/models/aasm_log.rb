# frozen_string_literal: true

# == Schema Information
#
# Table name: aasm_logs
#
#  id                 :bigint(8)        not null, primary key
#  user_id            :bigint(8)
#  aasm_loggable_type :string
#  aasm_loggable_id   :bigint(8)
#  from_state         :string
#  to_state           :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  meta               :jsonb
#

class AasmLog < ApplicationRecord
  belongs_to :user
  belongs_to :aasm_loggable, polymorphic: true

  scope :transitions_to_state, ->(state) { where(to_state: state) }
end
