# frozen_string_literal: true

module Rates
  def self.table_name_prefix
    'rates_'
  end
end
