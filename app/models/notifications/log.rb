# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications_logs
#
#  id              :bigint(8)        not null, primary key
#  notification_id :bigint(8)
#  status          :integer
#  meta            :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

module Notifications
  class Log < ApplicationRecord
    enum status: { pending: 0, success: 1, fail: 2 }

    belongs_to :notification, class_name: '::Notifications::Notification'
  end
end
