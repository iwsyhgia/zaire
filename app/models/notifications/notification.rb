# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications
#
#  id             :bigint(8)        not null, primary key
#  kind           :integer
#  recipient_type :string
#  recipient_id   :bigint(8)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  read_at        :datetime
#  message_params :jsonb            not null
#

module Notifications
  class Notification < ApplicationRecord
    self.table_name = 'notifications'

    enum kind: {
      audit_initiated: 0,
      audit_completed: 1,
      audit_failed: 2,
      dates_changed: 3,
      room_changed: 4,
      reservation_cancelled: 5,
      reservation_invoice: 6,
      after_check_in: 7
    }

    belongs_to :recipient, polymorphic: true
    has_many :logs, class_name: '::Notifications::Log'
  end
end
