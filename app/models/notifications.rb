# frozen_string_literal: true

module Notifications
  def self.table_name_prefix
    'notifications_'
  end
end
