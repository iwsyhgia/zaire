# frozen_string_literal: true

module Guests
  def self.table_name_prefix
    'guests_'
  end
end
