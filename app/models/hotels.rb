# frozen_string_literal: true

module Hotels
  def self.table_name_prefix
    'hotels_'
  end
end
