# frozen_string_literal: true

module Reservations
  def self.table_name_prefix
    'reservations_'
  end
end
