# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_discounts
#
#  id           :bigint(8)        not null, primary key
#  creation_way :integer
#  deleted_at   :datetime
#  description  :text
#  kind         :integer          default("amount")
#  value        :decimal(10, 2)   default(0.0)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  invoice_id   :bigint(8)
#  user_id      :bigint(8)
#

module Invoices
  class Discount < ApplicationRecord
    has_paper_trail only: %i[value description kind user_id], on: %i[update]
    acts_as_paranoid

    enum kind: { amount: 0, percent: 1 }
    enum creation_way: { manual: 0, automated: 1 }

    belongs_to :user
    belongs_to :invoice, class_name: '::Invoices::Invoice'
  end
end
