# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_balances
#
#  id         :bigint(8)        not null, primary key
#  amount     :decimal(10, 2)   default(0.0)
#  deleted_at :datetime
#  department :integer
#  owner_type :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  owner_id   :bigint(8)
#

module Invoices
  class Balance < ApplicationRecord
    acts_as_paranoid
    enum department: { default: 0 }

    belongs_to :owner, polymorphic: true

    after_initialize :set_defaults, if: :new_record?

    validates :department, presence: true

    def self.for(owner, department = :default)
      find_or_create_by(owner: owner, department: department)
    end

    private

    def set_defaults
      self.amount ||= 0
      self.department ||= :default
    end
  end
end
