# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_refunds
#
#  id           :bigint(8)        not null, primary key
#  amount       :decimal(10, 2)   default(0.0)
#  creation_way :integer
#  deleted_at   :datetime
#  description  :text
#  status       :integer          default("pending")
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  invoice_id   :bigint(8)
#  payment_id   :bigint(8)
#  user_id      :bigint(8)
#

module Invoices
  class Refund < ApplicationRecord
    has_paper_trail only: %i[amount description payment_id user_id], on: %i[update]
    acts_as_paranoid

    enum status: { pending: 0, successful: 1, failed: 2 }
    enum creation_way: { manual: 0, automated: 1 }

    belongs_to :user
    belongs_to :invoice, class_name: '::Invoices::Invoice'
    belongs_to :payment, class_name: '::Invoices::Payment'

    has_many :transactions, as: :document, class_name: '::Invoices::Transaction'
  end
end
