# frozen_string_literal: true
# == Schema Information
#
# Table name: invoices
#
#  id                            :bigint(8)        not null, primary key
#  amount                        :decimal(10, 2)   default(0.0)
#  booking_total                 :decimal(10, 2)   default(0.0), not null
#  charges_total                 :decimal(10, 2)   default(0.0)
#  charges_with_taxes_total      :decimal(10, 2)   default(0.0)
#  current_rate                  :decimal(10, 2)   default(0.0)
#  deleted_at                    :datetime
#  discounts_total               :decimal(10, 2)   default(0.0)
#  from_type                     :string
#  municipal_tax                 :decimal(5, 2)    default(0.0)
#  municipal_total               :decimal(10, 2)   default(0.0)
#  paid_total                    :decimal(10, 2)   default(0.0)
#  payments_total                :decimal(10, 2)   default(0.0)
#  price_per_night               :decimal(10, 2)   default(0.0)
#  price_per_night_with_taxes    :decimal(10, 2)   default(0.0)
#  price_total                   :decimal(10, 2)   default(0.0)
#  price_total_with_taxes        :decimal(10, 2)   default(0.0)
#  refunds_total                 :decimal(10, 2)   default(0.0)
#  sellable_items_discount_total :decimal(10, 2)   default(0.0)
#  sellable_items_total          :decimal(10, 2)   default(0.0)
#  stay_fees_total               :decimal(10, 2)   default(0.0)
#  to_type                       :string
#  unpaid_total                  :decimal(10, 2)   default(0.0)
#  vat_tax                       :decimal(5, 2)    default(0.0)
#  vat_total                     :decimal(10, 2)   default(0.0)
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  from_id                       :bigint(8)
#  to_id                         :bigint(8)
#

module Invoices
  class Invoice < ApplicationRecord
    self.table_name = 'invoices'

    acts_as_paranoid

    belongs_to :from, polymorphic: true
    belongs_to :to, polymorphic: true

    belongs_to :reservation, foreign_key: :to_id, class_name: '::Reservations::Reservation', inverse_of: :invoice

    has_many :charges, class_name: '::Invoices::Charge', dependent: :destroy
    has_many :refunds, class_name: '::Invoices::Refund', dependent: :destroy
    has_many :discounts, class_name: '::Invoices::Discount', dependent: :destroy
    has_many :payments, class_name: '::Invoices::Payment', dependent: :destroy

    after_initialize :set_defaults

    def tax_total
      charges_total * (vat_tax + municipal_tax) / 100
    end

    def stay_fees
      charges_total
    end

    def stay_fees_discount
      discounts_total
    end

    def sellable_items
      0
    end

    def sellable_items_discount
      0
    end

    private

    def set_defaults
      return unless new_record?

      self.municipal_tax = 2.5
      self.vat_tax       = 5
    end
  end
end
