# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_payments
#
#  id                   :bigint(8)        not null, primary key
#  amount               :decimal(10, 2)   default(0.0)
#  amount_without_taxes :decimal(10, 2)   default(0.0)
#  creation_way         :integer          default("manual")
#  deleted_at           :datetime
#  payment_method       :integer
#  status               :integer          default("pending")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  invoice_id           :bigint(8)
#  user_id              :bigint(8)
#

module Invoices
  class Payment < ApplicationRecord
    has_paper_trail only: %i[amount payment_method kind user_id], on: %i[update]
    acts_as_paranoid

    enum status: { pending: 0, successful: 1, failed: 2 }
    enum payment_method: { card: 0, cash: 1 }
    enum creation_way: { manual: 0, automated: 1 }

    belongs_to :user
    belongs_to :invoice, class_name: '::Invoices::Invoice'

    has_many :refunds, class_name: '::Invoices::Refund'
    has_many :transactions, as: :document, class_name: '::Invoices::Transaction'

    scope :for_period, ->(start_date, end_date) { where('created_at >= ? AND created_at <= ?', start_date, end_date) }
  end
end
