# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_transactions
#
#  id            :bigint(8)        not null, primary key
#  amount        :decimal(10, 2)   default(0.0)
#  balance       :decimal(10, 2)   default(0.0)
#  deleted_at    :datetime
#  document_type :string
#  kind          :integer          default("online_payment")
#  meta          :jsonb
#  source_type   :string
#  status        :integer          default("pending")
#  target_type   :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  document_id   :bigint(8)
#  source_id     :bigint(8)
#  target_id     :bigint(8)
#

# This is our log records for transactions.
# Pair of such transactions (source and target transactions) are equal to invoice system transaction or any other kind
# of money transfer.
# Pay attention that only source transaction has meta field filled.

module Invoices
  class Transaction < ApplicationRecord
    acts_as_paranoid

    belongs_to :source, polymorphic: true
    belongs_to :target, polymorphic: true
    belongs_to :document, polymorphic: true
    belongs_to :invoice, class_name: '::Invoices::Invoice'

    enum status: { pending: 0, successful: 1, failed: 2 }
    enum kind: { online_payment: 0, online_refund: 1, offline_payment: 2, offline_refund: 3 }

    after_initialize :set_defaults, if: :new_record?

    validates :status, presence: true
    validates :kind, presence: true

    private

    def set_defaults
      self.balance ||= 0.0
    end
  end
end
