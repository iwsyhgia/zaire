# frozen_string_literal: true

# == Schema Information
#
# Table name: invoices_charges
#
#  id                   :bigint(8)        not null, primary key
#  amount               :decimal(10, 2)   default(0.0)
#  base_price           :decimal(10, 2)   default(0.0)
#  creation_way         :integer
#  deleted_at           :datetime
#  description          :text
#  kind                 :integer
#  municipal_tax_amount :decimal(10, 2)   default(0.0)
#  vat_tax_amount       :decimal(10, 2)   default(0.0)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  audit_id             :bigint(8)
#  invoice_id           :bigint(8)
#  user_id              :bigint(8)
#

module Invoices
  class Charge < ApplicationRecord
    has_paper_trail only: %i[amount description user_id], on: %i[update]
    acts_as_paranoid

    # TODO: make sellable_items singular
    enum kind: { stay_fee: 0, sellable_items: 1, cancellation_fee: 2, no_show_fee: 3 }
    enum creation_way: { manual: 0, automated: 1 }

    scope :for_period, ->(start_date, end_date) { where('created_at >= ? AND created_at <= ?', start_date, end_date) }

    belongs_to :user
    belongs_to :invoice, class_name: '::Invoices::Invoice'
    belongs_to :audit, class_name: '::Audits::Audit'
  end
end
