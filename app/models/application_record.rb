# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  after_initialize :set_defaults, if: :new_record?

  private

  def set_defaults; end
end
