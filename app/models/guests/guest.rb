# frozen_string_literal: true

# == Schema Information
#
# Table name: guests
#
#  id           :bigint(8)        not null, primary key
#  birth_date   :date
#  city         :string
#  country      :string
#  deleted_at   :datetime
#  email        :string
#  first_name   :string
#  last_name    :string
#  phone_code   :string
#  phone_number :string
#  title        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  hotel_id     :bigint(8)
#

module Guests
  class Guest < ApplicationRecord
    include Concerns::Balanceable
    include Concerns::Recipient

    self.table_name = 'guests'

    acts_as_paranoid

    has_many :reservations, class_name: '::Reservations::Reservation'
    has_many :invoice, as: :from, class_name: '::Invoices::Invoice'
    belongs_to :hotel, class_name: '::Hotels::Hotel'

    def phone_number_with_code
      return '' unless phone_code.present? && phone_number.present?

      phone_code + phone_number
    end
  end
end
