# frozen_string_literal: true

# == Schema Information
#
# Table name: images
#
#  id             :bigint(8)        not null, primary key
#  file           :string
#  imageable_type :string
#  imageable_id   :bigint(8)
#  tag            :string
#  optimized_at   :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#

class Image < ApplicationRecord
  include Concerns::PngOptimization

  acts_as_paranoid

  belongs_to :imageable, autosave: false, polymorphic: true, required: false

  mount_uploader :file, ImageUploader

  def base64=(base64)
    self.file = Image.image_from_base64(base64)
  end

  def aspect_ratio
    if CLASSES_FOR_COVER_IMAGE.include?(imageable.class) && tag == 'cover_image'
      5.0 / 1.0
    else
      1
    end
  end

  def self.image_from_base64(base64)
    index = base64.index(';base64,')
    return unless index

    data = base64[(index + 8)..-1]
    decoded = Base64.decode64(data.lstrip)
    Base64InputIO.new(decoded)
  end
end
