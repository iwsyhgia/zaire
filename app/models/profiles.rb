# frozen_string_literal: true

module Profiles
  def self.table_name_prefix
    'profiles_'
  end
end
