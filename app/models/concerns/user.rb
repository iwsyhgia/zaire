# frozen_string_literal: true

module Concerns
  module User
    extend ActiveSupport::Concern

    def generate_token
      payload = { payload: { id: id } }
      Knock::AuthToken.new(payload).token
    end

    def self.from_token_payload(payload)
      find(payload['id'])
    end
  end
end
