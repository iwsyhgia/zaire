# frozen_string_literal: true

module Concerns
  module Recipient
    extend ActiveSupport::Concern

    included do
      has_many :notifications, as: :recipient, class_name: '::Notifications::Notification'
    end
  end
end
