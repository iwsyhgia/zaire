# frozen_string_literal: true

class AASMLogger < AASM::Base
  def log_state_change!
    klass.class_eval do
      aasm with_klass: AASMLogger do
        after_all_transitions :log_state
      end

      def log_state(from_state: aasm.from_state, to_state: aasm.to_state, object: aasm.instance,
        user: ::Services::CurrentControllerService.get.try(:current_user))

        AasmLog.create!(
          user: user,
          aasm_loggable: self,
          from_state: from_state,
          to_state: to_state,
          meta: object.try(:aasm_state_log_meta) || {}
        )
      end
    end
  end
end
