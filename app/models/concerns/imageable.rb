# frozen_string_literal: true

require 'active_support/concern'

module Concerns
  module Imageable
    extend ActiveSupport::Concern

    included do
      #  Holds the image tags on class level
      #
      mattr_accessor :image_tags
      self.image_tags = []

      has_many :images, class_name: 'Image', as: :imageable, autosave: true, dependent: :destroy
    end

    class_methods do
      # rubocop:disable Naming/PredicateName
      def has_image(tag)
        image_tags << tag

        define_method tag do
          images.reject(&:marked_for_destruction?).detect { |i| i.tag.to_s == tag.to_s }
        end

        define_method "#{tag}_id" do
          send(tag).try(:id)
        end

        define_method "#{tag}=" do |value|
          old_image = Image.find_by(tag: tag)
          new_image = value
          new_image.tag = tag
          self.images = images - [old_image] + [new_image]
        end

        define_method "#{tag}_id=" do |value|
          image = value.blank? ? nil : Image.find_by(id: value, tag: tag)
          self.images += [image].compact
          (self.images.select { |i| i.tag == tag.to_s } - [image].compact).each(&:mark_for_destruction)
        end
      end
      # rubocop:enable Naming/PredicateName
    end
  end
end
