# frozen_string_literal: true

module AASMLoggable
  extend ActiveSupport::Concern

  included do
    has_many :aasm_logs, as: :aasm_loggable, class_name: 'AasmLog'

    def last_transition_to(state)
      aasm_logs.transitions_to_state(state).last
    end
  end
end
