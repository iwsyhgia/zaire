# frozen_string_literal: true

module Concerns
  module Balanceable
    extend ActiveSupport::Concern

    included do
      has_one :balance, as: :owner, class_name: '::Invoices::Balance'
    end
  end
end
