# frozen_string_literal: true

module Concerns
  module PngOptimization
    extend ActiveSupport::Concern

    included do
      attr_accessor :need_to_optimize
      after_commit :run_delay_optimization
    end

    def run_delay_optimization
      return if optimized_at.present?

      CarrierWaves::OptimizationImageJob.perform_later(self)
    end
  end
end
