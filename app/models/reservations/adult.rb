# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations_adults
#
#  id             :bigint(8)        not null, primary key
#  deleted_at     :datetime
#  first_name     :string
#  last_name      :string
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  personal_id    :string
#  reservation_id :bigint(8)
#

module Reservations
  class Adult < ApplicationRecord
    acts_as_paranoid

    belongs_to :reservation, class_name: '::Reservations::Reservation'
  end
end
