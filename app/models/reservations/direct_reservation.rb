# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id                 :bigint(8)        not null, primary key
#  check_in_date      :date
#  check_out_date     :date
#  deleted_at         :datetime
#  description        :string
#  number_of_adults   :integer
#  number_of_children :integer
#  payment_kind       :integer
#  rate               :integer          default("refundable"), not null
#  status             :integer          default("unconfirmed")
#  type               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  guest_id           :bigint(8)
#  room_id            :bigint(8)
#

module Reservations
  class DirectReservation < Reservation
  end
end
