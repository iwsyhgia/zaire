# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id                 :bigint(8)        not null, primary key
#  check_in_date      :date
#  check_out_date     :date
#  deleted_at         :datetime
#  description        :string
#  number_of_adults   :integer
#  number_of_children :integer
#  payment_kind       :integer
#  rate               :integer          default("refundable"), not null
#  status             :integer          default("unconfirmed")
#  type               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  guest_id           :bigint(8)
#  room_id            :bigint(8)
#

module Reservations
  class Reservation < ApplicationRecord
    SOURCES = { direct: 'Reservations::DirectReservation' }.freeze

    self.table_name = 'reservations'

    include Concerns::Balanceable
    include AASM
    include AASMLoggable

    acts_as_paranoid

    delegate :price_total, :price_total_with_taxes, :price_per_night, :price_per_night_with_taxes, to: :invoice

    enum rate: { refundable: 0, non_refundable: 1 }
    enum payment_kind: { full_stay: 0, first_night: 1 }
    enum status: {
      unconfirmed: 0,
      confirmed:   1,
      checked_in: 2,
      checked_out: 3,
      no_show: 4,
      cancelled: 5,
      default_dnr: 10,
      cut_dnr: 11
    }

    belongs_to :guest, class_name: '::Guests::Guest'
    belongs_to :room, class_name: '::Rooms::Room'
    has_one :hotel, through: :room, class_name: '::Hotels::Hotel'
    has_one :room_type, through: :room, class_name: '::Rooms::RoomType'

    has_many :adults, class_name: '::Reservations::Adult'
    has_one :invoice, as: :to, class_name: '::Invoices::Invoice', inverse_of: :reservation

    has_many :discounts, through: :invoice
    has_many :refunds, through: :invoice
    has_many :charges, through: :invoice
    has_many :payments, through: :invoice
    has_many :transactions, through: :invoice

    has_many :audit_error_logs, as: :auditable

    accepts_nested_attributes_for :guest, allow_destroy: false
    accepts_nested_attributes_for :adults, allow_destroy: true

    scope :dnr, -> { where(type: 'Reservations::DNR') }
    scope :direct, -> { where(type: 'Reservations::DirectReservation') }
    scope :filtered_by_date_range, lambda { |from_date, to_date|
      where(check_in_date: from_date..to_date).or(Reservations::Reservation.where(check_out_date: from_date..to_date))
    }

    scope :overlaps, Queries::Reservations::OverlapsQuery
    scope :must_be_marked_as_no_show, lambda { |date|
      reservations_condition = <<-SQL
        (status IN (:confirmed, :unconfirmed) AND rate = :refundable     AND check_in_date  < :date) OR
        (status = :unconfirmed                AND rate = :non_refundable AND check_in_date  < :date) OR
        (status = :confirmed                  AND rate = :non_refundable AND check_out_date < :date)
      SQL

      where(type: 'Reservations::DirectReservation').
        where(
          reservations_condition,
          date: date,
          confirmed: statuses[:confirmed],
          unconfirmed: statuses[:unconfirmed],
          refundable: rates[:refundable],
          non_refundable: rates[:non_refundable]
        )
    }

    scope :in_datetime_range,
      lambda { |time_frame_begin, time_frame_end, check_in_start_time = '00:00', check_out_end_time = '00:00'|
        condition = <<-SQL
          (check_in_date + time '#{check_in_start_time}',
           check_out_date + time '#{check_out_end_time}') OVERLAPS (?, ?)
        SQL

        where(condition, time_frame_begin, time_frame_end)
      }

    scope :for_audit, lambda { |audit|
      hotel = audit.hotel

      in_datetime_range(audit.time_frame_begin, audit.time_frame_end, hotel.check_in_time.start_time || '00:00',
        hotel.check_out_time.end_time || '00:00')
    }

    scope :waiting_for_new_charge, ->(audit) { checked_in.for_audit(audit) }
    scope :not_cancelled, -> { where.not(status: Reservations::Reservation.statuses[:cancelled]) }

    ransacker :status, formatter: ->(v) { statuses.merge!(::Reservations::DNR.statuses)[v] }
    ransacker :source, formatter: ->(v) { SOURCES[v.to_sym] } do |parent|
      parent.table[:type]
    end

    aasm with_klass: AASMLogger, column: :status, enum: true do
      log_state_change!

      state :unconfirmed, initial: true
      state :confirmed
      state :checked_in
      state :checked_out
      state :no_show
      state :cancelled
      state :default_dnr
      state :cut_dnr

      event :unconfirm do
        transitions from: %i[confirmed], to: :unconfirmed
      end

      event :confirm do
        transitions from: %i[unconfirmed], to: :confirmed
      end

      event :check_in do
        transitions from: %i[unconfirmed confirmed], to: :checked_in
      end

      event :check_out do
        transitions from: %i[checked_in], to: :checked_out
      end

      event :cancel do
        transitions from: %i[unconfirmed confirmed], to: :cancelled
      end

      event :no_show do
        transitions from: %i[unconfirmed confirmed], to: :no_show
      end
    end

    def dnr?
      type == 'Reservations::DNR'
    end

    def direct?
      type == 'Reservations::DirectReservation'
    end

    def total_nights
      (check_in_date..check_out_date).count - 1
    end

    def nights
      return [] unless check_in_date && check_out_date

      (check_in_date.tomorrow..check_out_date).map { |date| [date.yesterday, date] }
    end

    def nights_as_text
      nights.map do |night_dates|
        night_dates.map(&:to_s).join(' - ')
      end
    end
  end
end
