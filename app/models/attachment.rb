# frozen_string_literal: true

# == Schema Information
#
# Table name: attachments
#
#  id              :bigint(8)        not null, primary key
#  attachable_type :string
#  deleted_at      :datetime
#  file            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  attachable_id   :bigint(8)
#

class Attachment < ApplicationRecord
  acts_as_paranoid

  mount_uploader :file, AttachmentUploader

  belongs_to :attachable, autosave: false, polymorphic: true
  validates :file, presence: true

  def kind
    return 'pdf' if file.content_type.to_s.include?('pdf')
    return 'image' if file.content_type.to_s.include?('image')
    return 'doc' if %w[doc docm docx].include? extension
    return 'xls' if %w[xls xlsx].include? extension

    'doc'
  end

  def previewable?
    %w[pdf image].include?(kind)
  end

  def extension
    File.extname(file.current_path).split('.').last
  end
end
