# frozen_string_literal: true

# == Schema Information
#
# Table name: amenities_room_types
#
#  id           :bigint(8)        not null, primary key
#  room_type_id :bigint(8)
#  amenity_id   :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Rooms
  class AmenitiesRoomType < ApplicationRecord
    belongs_to :amenity, class_name: '::Rooms::Amenity'
    belongs_to :room_type, class_name: '::Rooms::RoomType'
  end
end
