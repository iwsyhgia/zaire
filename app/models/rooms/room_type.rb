# frozen_string_literal: true

# == Schema Information
#
# Table name: room_types
#
#  id            :bigint(8)        not null, primary key
#  deleted_at    :datetime
#  max_occupancy :integer          not null
#  name          :string           not null
#  name_ar       :string
#  quantity      :integer
#  size          :string
#  unit          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  hotel_id      :bigint(8)
#

module Rooms
  class RoomType < ApplicationRecord
    acts_as_paranoid

    after_initialize :set_defaults, if: :new_record?

    belongs_to :hotel, class_name: '::Hotels::Hotel'
    has_many :amenities_room_types, class_name: '::Rooms::AmenitiesRoomType', dependent: :destroy

    has_many :rooms, class_name: '::Rooms::Room', dependent: :destroy
    has_many :images, as: :imageable, dependent: :destroy, class_name: 'Image'
    has_many :amenities, through: :amenities_room_types, class_name: '::Rooms::Amenity'

    has_one :rate, class_name: '::Rooms::Rate', dependent: :destroy

    accepts_nested_attributes_for :rooms, allow_destroy: true
    accepts_nested_attributes_for :images, allow_destroy: true
    accepts_nested_attributes_for :amenities, allow_destroy: true
    accepts_nested_attributes_for :rate, update_only: true

    private

    def set_defaults
      return if rate.present?

      build_rate
    end
  end
end
