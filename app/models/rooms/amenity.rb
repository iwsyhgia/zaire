# frozen_string_literal: true

# == Schema Information
#
# Table name: amenities
#
#  id          :bigint(8)        not null, primary key
#  category    :string
#  category_ar :string
#  name        :string           not null
#  name_ar     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  hotel_id    :bigint(8)
#

module Rooms
  class Amenity < ApplicationRecord
    belongs_to :hotel, class_name: '::Hotels::Hotel'

    has_many :amenities_room_types, class_name: '::Rooms::AmenitiesRoomType'
    has_many :room_types, through: :amenities_room_types,
                          class_name: '::Rooms::RoomType', dependent: :destroy
  end
end
