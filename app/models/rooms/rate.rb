# frozen_string_literal: true

# == Schema Information
#
# Table name: rooms_rates
#
#  id           :bigint(8)        not null, primary key
#  active       :boolean          default(TRUE)
#  deleted_at   :datetime
#  kind         :integer          default("fixed")
#  lower_bound  :decimal(10, 2)   default(0.0)
#  upper_bound  :decimal(10, 2)   default(0.0)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  room_type_id :bigint(8)
#

module Rooms
  class Rate < ApplicationRecord
    self.table_name = 'rooms_rates'

    acts_as_paranoid

    enum kind: { fixed: 0, dynamic: 1 }

    belongs_to :room_type, class_name: '::Rooms::RoomType'
  end
end
