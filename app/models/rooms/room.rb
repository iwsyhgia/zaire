# frozen_string_literal: true

# == Schema Information
#
# Table name: rooms
#
#  id           :bigint(8)        not null, primary key
#  deleted_at   :datetime
#  number       :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  room_type_id :bigint(8)
#

module Rooms
  class Room < ApplicationRecord
    acts_as_paranoid

    belongs_to :room_type, class_name: '::Rooms::RoomType'
    has_one :hotel, through: :room_type, class_name: '::Hotels::Hotel'
    has_many :reservations, class_name: '::Reservations::Reservation'
    has_many :dnr, class_name: '::Reservations::DNR'

    scope :available_for, lambda { |check_in_date, check_out_date|
      check_in_date = DateUtils.date_for_db(check_in_date)
      check_out_date = DateUtils.date_for_db(check_out_date)

      overlapsing_reservations_condition = <<-SQL
        (reservations.check_in_date, reservations.check_out_date) OVERLAPS (?, ?)
      SQL

      room_ids =
        Reservations::Reservation.where(overlapsing_reservations_condition, check_in_date, check_out_date).
        where.not(status: 'cancelled').
        select(:room_id).
        distinct

      where.not(id: room_ids).where.not(room_type_id: nil)
    }
  end
end
