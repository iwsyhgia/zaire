# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels_configurations
#
#  id                                :bigint(8)        not null, primary key
#  deleted_at                        :datetime
#  night_audit_confirmation_required :boolean          default(TRUE)
#  night_audit_time                  :string           default("05:00")
#  time_zone                         :string           default("UTC")
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#  hotel_id                          :bigint(8)
#

# frozen_string_literal: true

module Hotels
  class Configuration < ApplicationRecord
    belongs_to :hotel, class_name: '::Hotels::Hotel'

    def night_audit_time_in_hours
      Time.parse(night_audit_time).utc.hour
    end

    def night_audit_time_in_seconds
      Time.parse(night_audit_time).utc.seconds_since_midnight.seconds
    end

    # def night_audit_parsed_time
    #   Time.new(night_audit_time).strftime('%H:%M')
    # end
  end
end
