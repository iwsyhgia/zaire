# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels_offline_modes
#
#  id                :bigint(8)        not null, primary key
#  hotel_id          :bigint(8)
#  active            :boolean          default(FALSE)
#  days_before_today :integer
#  days_after_today  :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deleted_at        :datetime
#

module Hotels
  class OfflineMode < ApplicationRecord
    DEFAULT_DAYS_COUNT = 7

    acts_as_paranoid

    belongs_to :hotel, class_name: '::Hotels::Hotel'

    before_save :set_defaults!, unless: ->(r) { r.active? }

    def set_defaults!
      return if days_before_today == DEFAULT_DAYS_COUNT && days_after_today == DEFAULT_DAYS_COUNT

      update(days_before_today: DEFAULT_DAYS_COUNT, days_after_today: DEFAULT_DAYS_COUNT)
    end
  end
end
