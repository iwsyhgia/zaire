# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels_addresses
#
#  id           :bigint(8)        not null, primary key
#  address_1    :string
#  address_2    :string
#  city         :string
#  country      :string
#  deleted_at   :datetime
#  email        :string
#  fax          :string
#  phone_number :string
#  state        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  hotel_id     :bigint(8)
#

module Hotels
  class Address < ApplicationRecord
    acts_as_paranoid

    belongs_to :hotel, class_name: '::Hotels::Hotel'
  end
end
