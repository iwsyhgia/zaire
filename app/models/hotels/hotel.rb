# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels
#
#  id         :bigint(8)        not null, primary key
#  deleted_at :datetime
#  name       :string
#  name_ar    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#

# frozen_string_literal: true

module Hotels
  class Hotel < ApplicationRecord
    self.table_name = 'hotels'
    include Concerns::Imageable
    has_image :logo

    acts_as_paranoid

    belongs_to :user

    has_one :configuration, class_name: '::Hotels::Configuration', dependent: :destroy
    has_one :offline_mode, class_name: '::Hotels::OfflineMode', dependent: :destroy
    has_one :address, class_name: '::Hotels::Address', dependent: :destroy
    has_one :check_in_time, -> { where(check_type: :check_in) },
      class_name: 'Hotels::CheckTime', dependent: :destroy
    has_one :check_out_time, -> { where(check_type: :check_out) },
      class_name: 'Hotels::CheckTime', dependent: :destroy
    has_many :room_types, class_name: '::Rooms::RoomType'
    has_many :rooms, class_name: '::Rooms::Room', through: :room_types
    has_many :amenities, class_name: '::Rooms::Amenity', dependent: :destroy

    has_many :reservations, class_name: '::Reservations::Reservation', through: :rooms
    has_many :audits, class_name: '::Audits::Audit'
    has_many :guests, -> { with_deleted }, class_name: '::Guests::Guest'

    has_many :dnr, class_name: '::Reservations::DNR', through: :rooms
    has_many :invoices, class_name: '::Invoices::Invoice', through: :reservations
    has_many :charges, class_name: '::Invoices::Charge', through: :invoices

    has_many :periods, class_name: '::Rates::Period'
    has_many :periods_room_types, through: :periods, class_name: '::Rates::PeriodsRoomType'

    accepts_nested_attributes_for :check_in_time
    accepts_nested_attributes_for :check_out_time

    has_one :standard_period, -> { standard }, class_name: '::Rates::Period'

    alias hotelier user

    def current_audit
      audits.where(status: %w[waiting_for_confirmation running]).order(:created_at).last
    end

    def last_audit_time
      audits.order(:created_at).completed.last&.started_at || Time.new(0)
    end

    def running_audit?
      current_audit.running?
    end

    def initiated_audit?
      current_audit&.status&.in?(%w[waiting_for_confirmation confirmed running])
    end
  end
end
