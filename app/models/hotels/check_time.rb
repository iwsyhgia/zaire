# frozen_string_literal: true

# == Schema Information
#
# Table name: hotels_check_times
#
#  id         :bigint(8)        not null, primary key
#  hotel_id   :bigint(8)
#  check_type :integer          default("check_in")
#  fixed      :boolean          default(TRUE)
#  start_time :string           not null
#  end_time   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Hotels
  class CheckTime < ApplicationRecord
    belongs_to :hotel, class_name: '::Hotels::Hotel'

    enum check_type: %w[check_in check_out]
  end
end
