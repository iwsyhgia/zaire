# frozen_string_literal: true

module Invoices
  def self.table_name_prefix
    'invoices_'
  end
end
