# frozen_string_literal: true

# == Schema Information
#
# Table name: audits_error_logs
#
#  id             :bigint(8)        not null, primary key
#  auditable_type :string
#  meta           :jsonb
#  step           :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  audit_id       :bigint(8)
#  auditable_id   :bigint(8)
#

module Audits
  class ErrorLog < ApplicationRecord
    enum step: { update_reservations_status: 0, create_stay_charges: 1, send_report: 2, schedule_next_audit: 3 }

    belongs_to :audit, class_name: '::Audits::Audit'
    belongs_to :auditable, polymorphic: true
  end
end
