# frozen_string_literal: true

# == Schema Information
#
# Table name: audits_confirmations
#
#  id           :bigint(8)        not null, primary key
#  user_id      :bigint(8)
#  audit_id     :bigint(8)
#  confirmed_at :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

module Audits
  class Confirmation < ApplicationRecord
    belongs_to :user
    belongs_to :audit

    scope :pending, -> { where(confirmed_at: nil) }
  end
end
