# frozen_string_literal: true

# == Schema Information
#
# Table name: audits
#
#  id               :bigint(8)        not null, primary key
#  started_at       :datetime
#  status           :integer
#  time_frame_begin :datetime
#  time_frame_end   :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  hotel_id         :bigint(8)
#

module Audits
  class Audit < ApplicationRecord
    self.table_name = 'audits'
    enum status: { scheduled: 0, waiting_for_confirmation: 1, confirmed: 2, running: 3, completed: 4, failed: 5 }

    belongs_to :hotel, class_name: '::Hotels::Hotel'
    has_many :confirmations
    has_many :error_logs, class_name: '::Audits::ErrorLog'
    has_many :charges, class_name: '::Invoices::Charge'
    has_one :pdf_report, class_name: '::Attachment', as: :attachable

    after_initialize :set_defaults, if: :new_record?

    def hours_coverage
      return unless time_frame_end && time_frame_begin

      ((time_frame_end - time_frame_begin) / 1.hour).round(2)
    end

    def night
      "#{time_frame_begin.to_date} - #{time_frame_end.to_date}"
    end

    private

    def set_defaults
      self.status ||= :scheduled
    end
  end
end
