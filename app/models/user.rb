# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  deleted_at             :datetime
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer          default("user"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class User < ApplicationRecord
  include Concerns::User
  include Concerns::Recipient
  PASSWORD_REGEXP = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/.freeze

  acts_as_paranoid

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :confirmable

  after_initialize :set_defaults, if: :new_record?

  has_one :hotel, class_name: '::Hotels::Hotel'
  has_one :profile, class_name: '::Profiles::Profile', dependent: :destroy
  has_many :audit_confirmations, class_name: '::Audits::Confirmation'

  enum role: %i[user admin]

  validate :password_complexity

  def password_complexity
    return if password.blank? || password =~ PASSWORD_REGEXP

    errors.add(:password, I18n.t('devise.passwords.errors.password_complexity'))
  end

  def email_required?
    new_record?
  end

  private

  def set_defaults
    build_profile
  end
end
