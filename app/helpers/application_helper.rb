# frozen_string_literal: true

module ApplicationHelper
  def plain_old_rails_page?
    devise_controller?
  end

  def current_audit_json
    return 'null' unless current_hotel.current_audit

    Serializers::Audits::CurrentAuditSerializer.new(current_hotel.current_audit, current_user: current_user).to_json
  end

  def night_audit_settings_json
    return 'null' unless current_hotel.configuration

    Serializers::Hotels::ConfigurationSerializer.new(current_hotel.configuration).to_json
  end

  def offline_mode_json
    return 'null' unless current_hotel.offline_mode

    Serializers::Hotels::OfflineModeSerializer.new(current_hotel.offline_mode).to_json
  end

  def countries_json
    ActiveModel::SerializableResource.new(
      ::ISO3166::Country.all,
      each_serializer: Serializers::Countries::CountrySerializer
    ).to_json
  end

  def current_user_json
    (current_user || {}).to_json
  end

  def arabic?
    return true if locale == :ar
  end

  def active_class_if(condition, defaults)
    condition ? defaults << ' active' : defaults
  end

  def hidden_if(condition)
    condition ? { style: 'display:none;' } : {}
  end
end
