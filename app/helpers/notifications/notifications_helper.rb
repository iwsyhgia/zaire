# frozen_string_literal: true

module Notifications
  module NotificationsHelper
    def notification_text
      notifier = validated_create_params[:notifiers].first

      if notifier == 'email'
        I18n.t("reservation_mailer.#{validated_create_params[:kind]}.message").html_safe
      elsif notifier == 'sms'
        I18n.t("sms.messages.#{validated_create_params[:kind]}").html_safe
      end
    end
  end
end
