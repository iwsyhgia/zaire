# frozen_string_literal: true

class Numeric
  def take_percent(percent)
    (to_d * percent.to_d / 100.0).round(2)
  end
end
