# frozen_string_literal: true

require 'colorize'

# rake db:remove_pii

namespace :db do
  desc 'Remove Personal information from database (change passwords)'
  task remove_pii: :environment do
    if Rails.env.production?
      puts "\nYou can't use this task at production environment. The task is terminated."
      exit
    end

    encrypted_password = User.new(password: 'Zaire123').encrypted_password

    print 'Updating passwords... '.green
    pii_sql = <<-SQL
      update users
        set encrypted_password = '#{encrypted_password}'
      ;

      update users
        set
          email = email || '.blocked'
        where
          email not like '%.blocked' AND
          email not like '%@sumatosoft.com'
      ;
    SQL

    ActiveRecord::Base.connection.execute(pii_sql)
    puts 'Done.'.green
  end
end
