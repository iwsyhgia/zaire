# frozen_string_literal: true

require 'csv'
require 'json'
require 'faker'

Rake::TaskManager.record_task_metadata = true

namespace :db do
  namespace :table do
    desc 'Delete records from database tables'
    task :reset, [:tables] => [:environment] do |_task, args|
      connection = ActiveRecord::Base.connection
      args.tables&.split&.each do |table_name|
        connection.execute "DELETE FROM #{table_name}"
        connection.reset_pk_sequence!(table_name)
      end
    end
  end

  namespace :seed do
    desc "Seeding the booking.com's amenities from db/data/amenities.csv file"
    task amenities: :environment do |task|
      task('db:table:reset').invoke('amenities')
      puts task.comment

      amenities = YAML.load_file Rails.root.join('db', 'data', 'amenities.yml')
      amenities['amenities_categories'].each do |category|
        category['amenities'].each do |amenity|
          ::Rooms::Amenity.create!(name: amenity['en'],
                                   name_ar: amenity['ar'],
                                   category: category['name'],
                                   category_ar: category['name_ar'])
        end
      end
    end

    desc 'Seeding users for development'
    task users: :environment do |task|
      task('db:table:reset').invoke('users')
      puts task.comment

      users = [
        { email: 'admin', password: 'Zaire123', role: :admin },
        { email: 'user', password: 'Zaire123', role: :user }
      ]
      users.each { |user| User.new(**user).save!(validate: false) }
      User.left_joins(:profile).where(profiles: { id: nil }).find_each(&:create_profile)
      User.first.confirm
    end

    desc 'Seeding guests without reservation for development'
    task guests: :environment do |task|
      puts task.comment

      10.times.each do
        ::Guests::Guest.create!(
          title: Faker::Name.prefix,
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          email: Faker::Internet.email,
          phone_number: Faker::PhoneNumber.phone_number,
          phone_code: Faker::PhoneNumber.area_code,
          country: ISO3166::Country.all.sample.name
        )
      end
    end

    desc 'Seeding room types for development'
    task room_types: :environment do |task|
      puts task.comment

      5.times do |i|
        ::Rooms::RoomType.create(
          hotel_id: ::Hotels::Hotel.first.id,
          name: %w[single double triple lux silver][i],
          max_occupancy: i + 1,
          quantity: 42,
          amenity_ids: ::Rooms::Amenity.ids.sample(5),
          room_ids: ::Rooms::Room.where(room_type_id: nil).ids.sample(5)
        )
      end
    end

    desc 'Seeding rooms for development'
    task rooms: :environment do |task|
      puts task.comment

      1.upto(15) do |i|
        ::Rooms::Room.create!(
          number: i,
          room_type_id: ::Rooms::RoomType.ids.sample
        )
      end
    end

    desc 'Seeding hotels for development'
    task hotels: :environment do |task|
      puts task.comment

      1.upto(2) do |i|
        hotel = ::Hotels::Hotel.create!(name: "zaire-#{i}", user_id: User.first.id)
        hotel.create_address
        hotel.create_offline_mode
        hotel.audits.create(status: :completed, time_frame_begin: Date.current, time_frame_end: Date.current.next_day)
        ::Services::Rates::InitStandardPeriod.call(hotel: hotel)
        hotel.create_configuration
      end
    end

    desc 'Seeding reservations for development'
    task reservations: :environment do |task|
      puts task.comment

      room_ids = ::Rooms::Room.all.ids
      1.upto(42) do |i|
        r = ::Reservations::DirectReservation.create(
          check_in_date: (Date.current + i.month).to_s(:db),
          check_out_date: (Date.current + i.month + 10.days).to_s(:db),
          status: 'unconfirmed',
          number_of_adults: rand(1..3),
          number_of_children: rand(1..6),
          room_id: room_ids.sample,
          guest_attributes: {
            title: Faker::Name.prefix,
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            email: Faker::Internet.email,
            phone_number: Faker::PhoneNumber.phone_number,
            phone_code: Faker::PhoneNumber.area_code,
            country: ISO3166::Country.all.sample.name
          }
        )
        r.create_invoice(to: r.guest)
      end
    end

    desc 'Seeding sample payments'
    task payments: :environment do |task|
      puts task.comment

      reservation = ::Reservations::Reservation.last
      guest = reservation.guest
      reservation.create_balance
      guest.create_balance
      Services::Reservations::Charge.call(reservation: reservation, amount: 2000, description: 'Stay fee')
      reservation.payments.last.discounts.create(value: 10, kind: :percent)
      reservation.payments.last.discounts.create(value: 100, kind: :amount)
    end
  end
end
