namespace :post_deploy do
  namespace :sprint_9 do
    desc 'Create standard periods for old hotels'
    task init_standard_periods: :environment do
      Hotels::Hotel.find_each do |hotel|
        hotel.periods.destroy_all
        ::Rooms::Rate.update(active: true)
        ::Services::Rates::InitStandardPeriod.call(hotel: hotel)
      end

      puts 'Done.'
    end
  end
end

# rake post_deploy:sprint_9:init_standard_periods
