# frozen_string_literal: true

module DateUtils
  module_function

  def date_for_db(date)
    case date
    when String
      Date.parse(date)
    else
      date
    end.to_formatted_s(:db)
  end
end
