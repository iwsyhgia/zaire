# frozen_string_literal: true

module AASM
  class InstanceBase
    attr_accessor :from_state, :to_state, :current_event, :instance
  end
end
